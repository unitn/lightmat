#
# Author: Alessandro Mazzalai
#
# Use "make all" to build the lightmat static library
#
# The library is generated in the "lib" folder
#

$(info =========================== Lightmat ===========================)

# Check OS
OS = $(shell uname)

ifeq ($(OS), Darwin)
$(info OS detected -> MAC OS X)
else ifeq ($(OS), Linux)
$(info OS detected -> LINUX)
else
$(error OS detected $(OS) is NOT SUPPORTED)
endif

$(info ================================================================)

# Current dir
CURRENT_DIR = $(shell pwd)

CC = clang++

LIB_DIR = lib
LIB = $(LIB_DIR)/liblm.a

all:
	(cd src; make -f make.unx CC=$(CC) OS=$(OS) ARCHFLAG=$(ARCHFLAG))
	mkdir -p $(LIB_DIR)
	ar rvs $(LIB) obj/*.o

dotest:
	cd src; make -f make.unx all

runtest:
	cd src; make -f make.unx lmtest-run

clean:
	cd src; make -f make.unx clean CC=$(CC)
	cd $(CURRENT_DIR); rm $(LIB)

debug:
	cd src; make -f make.unx OPT="-g -Wall" all

rowdebug:
	cd src; make -f make.unx OPT="-g -DROWMAJOR" all

