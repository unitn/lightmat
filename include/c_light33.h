//           -*- c++ -*-

#ifndef LIGHT33_C_H
#define LIGHT33_C_H
// <cd> light33lm_complex
//
// .SS Functionality
//
// light33 is a template for classes that implement matrices with 3x3
// elements. E.g. a matrix v with 3x3 elements of type double can be
// instanciated with:
//
// <code>light33&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHT33lm_complex_C_H

 class light3;
 class lightN;
 class lightN3;
 class lightN33;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light33lm_complex {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light33double();
  // Convert to double.
#else
   friend class  lightN33int;
#endif

#ifdef IN_LIGHT33double_C_H
   friend class  light33int;
#else
   friend class  light33double;
#endif

  #include "c_light33_auto.h"

  inline light33lm_complex();
  // Default constructor.

  inline light33lm_complex(const light33lm_complex&); 
  // Copy constructor.

  inline  light33lm_complex(const lm_complex e11, const lm_complex e12, const lm_complex e13, const lm_complex e21, const lm_complex e22, const lm_complex e23, const lm_complex e31, const lm_complex e32, const lm_complex e33);
  // Initialize elements with values.

  inline light33lm_complex(const lm_complex *);
  // Initialize elements with values from an array (in row major order).

  inline light33lm_complex(const lm_complex);
  // Initialize all elements with the same value.

  light33lm_complex& operator=(const light33lm_complex&);
    // Assignment.

  light33lm_complex& operator=(const lightN3lm_complex&);
  // Assignment from a lightN3 where N=3.

  light33lm_complex& operator=(const lightNNlm_complex&);
  // Assignment from a lightNN where NN=33.

  light33lm_complex& operator=(const lm_complex);
  // Assign one value to all elements.

  lm_complex operator()(const int x, const int y) const {
    limiterror((x<1) || (x>3) || (y<1) || (y>3));

#ifdef ROWMAJOR
    return elem[y-1+(x-1)*3];
#else
    return elem[x-1+(y-1)*3];
#endif
  };
  // Get the value of one element.


  lm_complex& operator()(const int x, const int y) {
    limiterror((x<1) || (x>3) || (y<1) || (y>3));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*3];
#else
    return elem[x-1+(y-1)*3];
#endif
  };
  // Get/Set the value of one element.

  light3lm_complex operator()(const int) const;
  // Get the value of one row.


  //
  // Set
  //

  inline light33lm_complex& Set (const int i0, const lm_complex val);
  inline light33lm_complex& Set (const int i0, const lightNlm_complex& arr);
  inline light33lm_complex& Set (const int i0, const light3lm_complex& arr);
  inline light33lm_complex& Set (const int i0, const light4lm_complex& arr);


  int operator==(const light33lm_complex&) const;
  // Equality.

  int operator!=(const light33lm_complex&) const;
  // Inequality.

  light33lm_complex& operator+=(const lm_complex);
  // Add a value to all elements.

  light33lm_complex& operator+=(const light33lm_complex&);
  // Elementwise addition.

  light33lm_complex& operator-=(const lm_complex);
  // Subtract a value from all elements.

  light33lm_complex& operator-=(const light33lm_complex&);
  // Elementwise subtraction.

  light33lm_complex& operator*=(const lm_complex);
  // Muliply all elements with a value.

  light33lm_complex& operator/=(const lm_complex);
  // Divide all elements with a value.

  light33lm_complex& reshape(const int, const int, const light3lm_complex&);
  // Convert the light3-vector to the given size and put the result in
  // this object. All three vector-columns of the light33 object will
  // get the value of the light3-argument. The first two arguments
  // (the size of the matrix) must both have the value 3 since it is a
  // light33 object.

  int dimension(const int x) const {
    if((x==1) || (x==2))
      return 3;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 3 and dimension(2) == 3).
 
  void Get(lm_complex *) const;
  // Get values of all elements and put them in an array (9 elements long,
  // row major order).

  void Set(const lm_complex *);
  // Set values of all elements from array (9 elements long, row major
  // order).

  void Set(const lm_complex e11, const lm_complex e12, const lm_complex e13,
	   const lm_complex e21, const lm_complex e22, const lm_complex e23,
	   const lm_complex e31, const lm_complex e32, const lm_complex e33);
  // Set the values of all the elements.

  void Get( lm_complex& e11, lm_complex& e12, lm_complex& e13,
	    lm_complex& e21, lm_complex& e22, lm_complex& e23,
	    lm_complex& e31, lm_complex& e32, lm_complex& e33) const;
  // Get the values of all the elements.

  light33lm_complex operator+() const;
  // Unary plus.

  light33lm_complex operator-() const;
  // Unary minus.

  friend inline light33lm_complex operator+(const light33lm_complex&, const light33lm_complex&);
  // Elementwise addition.

  friend inline light33lm_complex operator+(const light33lm_complex&, const lm_complex);
  // Addition to all elements.

  friend inline light33lm_complex operator+(const lm_complex, const light33lm_complex&);
  // Addition to all elements.

  friend inline light33lm_complex operator-(const light33lm_complex&, const light33lm_complex&);
  // Elementwise subtraction.

  friend inline light33lm_complex operator-(const light33lm_complex&, const lm_complex);
  // Subtraction from all elements.

  friend inline light33lm_complex operator-(const lm_complex, const light33lm_complex&);
  // Subtraction to all elements.

  friend inline light33lm_complex operator*(const light33lm_complex&, const light33lm_complex&);
  // Inner product.

  friend inline light33lm_complex Dot(const light33lm_complex&, const light33lm_complex&);
  // Inner product.

  friend inline light33lm_complex operator*(const light33lm_complex&, const lm_complex);
  // Multiply all elements.

  friend inline light33lm_complex operator*(const lm_complex, const light33lm_complex&);
  // Multiply all elements.

  friend inline light3lm_complex operator*(const light33lm_complex&, const light3lm_complex&);
  // Inner product.

  friend inline light3lm_complex operator*(const light3lm_complex&, const light33lm_complex&);
  // Inner product.

  friend inline light33lm_complex operator/(const light33lm_complex&, const lm_complex);
  // Divide all elements.

  friend inline light33lm_complex operator/(const lm_complex, const light33lm_complex&);
  // Divide with all elements.

  friend inline light33lm_complex pow(const light33lm_complex&, const light33lm_complex&);
  // Raise to the power of-function, elementwise.

  friend inline light33lm_complex pow(const light33lm_complex&, const lm_complex);
  // Raise to the power of-function, for all elements.

  friend inline light33lm_complex pow(const lm_complex, const light33lm_complex&);
  // Raise to the power of-function, for all elements.

  friend inline light33lm_complex ElemProduct(const light33lm_complex&, const light33lm_complex&);
  // Elementwise multiplication.

  friend inline light33lm_complex ElemQuotient(const light33lm_complex&, const light33lm_complex&);
  // Elementwise division.

  friend inline light33lm_complex Apply(const light33lm_complex&, lm_complex f(lm_complex));
  // Apply the function elementwise on all elements.

  friend inline light33lm_complex Apply(const light33lm_complex&, const light33lm_complex& s, lm_complex f(lm_complex, lm_complex));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline light33lm_complex Transpose(const light33lm_complex&);
  // Transpose matrix.

  friend inline light33lm_complex abs(const light33lm_complex&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light33int sign(const light33lm_complex&);
  // sign
#else
  friend inline light33lm_complex sign(const light33lm_complex&);
#endif
  friend inline light33int ifloor(const light33double&);
  // ifloor

  friend inline light33int iceil(const light33double&);
  // iceil

  friend inline light33int irint(const light33double&);
  // irint

  friend inline light33double sqrt(const light33double&);
  // sqrt

  friend inline light33double exp(const light33double&);
  // exp

  friend inline light33double log(const light33double&);
  // log

  friend inline light33double sin(const light33double&);
  // sin

  friend inline light33double cos(const light33double&);
  // cos

  friend inline light33double tan(const light33double&);
  // tan

  friend inline light33double asin(const light33double&);
  // asin

  friend inline light33double acos(const light33double&);
  // acos

  friend inline light33double atan(const light33double&);
  // atan

  friend inline light33double sinh(const light33double&);
  // sinh

  friend inline light33double cosh(const light33double&);
  // cosh

  friend inline light33double tanh(const light33double&);
  // tanh

  friend inline light33double asinh(const light33double&);
  // asinh

  friend inline light33double acosh(const light33double&);
  // acosh

  friend inline light33double atanh(const light33double&);
  // atanh

  friend inline light33lm_complex ifloor(const light33lm_complex&);
  // ifloor

  friend inline light33lm_complex iceil(const light33lm_complex&);
  // iceil

  friend inline light33lm_complex irint(const light33lm_complex&);
  // irint

  friend inline light33lm_complex sqrt(const light33lm_complex&);
  // sqrt

  friend inline light33lm_complex exp(const light33lm_complex&);
  // exp

  friend inline light33lm_complex log(const light33lm_complex&);
  // log

  friend inline light33lm_complex sin(const light33lm_complex&);
  // sin

  friend inline light33lm_complex cos(const light33lm_complex&);
  // cos

  friend inline light33lm_complex tan(const light33lm_complex&);
  // tan

  friend inline light33lm_complex asin(const light33lm_complex&);
  // asin

  friend inline light33lm_complex acos(const light33lm_complex&);
  // acos

  friend inline light33lm_complex atan(const light33lm_complex&);
  // atan

  friend inline light33lm_complex sinh(const light33lm_complex&);
  // sinh

  friend inline light33lm_complex cosh(const light33lm_complex&);
  // cosh

  friend inline light33lm_complex tanh(const light33lm_complex&);
  // tanh

  friend inline light33lm_complex asinh(const light33lm_complex&);
  // asinh

  friend inline light33lm_complex acosh(const light33lm_complex&);
  // acosh

  friend inline light33lm_complex atanh(const light33lm_complex&);
  // atanh



  //<ignore>
//    friend class light33int;
  friend class light3lm_complex;
  friend class lightN3lm_complex;
  friend class lightNNlm_complex;
  friend class lightN33lm_complex;
  friend class lightNNNlm_complex;
  friend class lightNNNNlm_complex;


  //    friend ostream & operator<<( ostream & ost,const  light33double & v);
  //</ignore>

protected:
  lm_complex elem[9];
  // The values of the nine elements.

  light33lm_complex(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.

};



typedef light33double double33;
typedef light33int int33;

//*********************************
typedef light33double doubleMat33;
extern  const  doubleMat33   zeroMat33;
//********************************

#undef IN_LIGHT33lm_complex_C_H

#endif
