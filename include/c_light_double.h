//
// routines which use BLAS
//
#ifndef LIGHTMAT_DONT_USE_BLAS
// These functions are overloaded variants of the ones in light_basic.h

lm_complex light_dot(const int n, const lm_complex *x, const lm_complex *y);

lm_complex light_dot(const int n, const lm_complex *x, const int ix, const lm_complex *y);

void light_gemv(const int m, const int n, const lm_complex *a, const lm_complex *x,
		lm_complex *y);

void light_gevm(const int m, const int n, const lm_complex *x, const lm_complex *a,
		lm_complex *y, const int iy = 1);

void light_gemm(const int m, const int n, const int k,
		const lm_complex *a, const lm_complex *b, lm_complex *c);

#endif // ! LIGHTMAT_DONT_USE_BLAS

//
// fractional part
//
inline lm_complex FractionalPart(const lm_complex s);
inline lightNlm_complex  FractionalPart(const lightNlm_complex&);
inline lightNNlm_complex  FractionalPart(const lightNNlm_complex&);
inline lightNNNlm_complex  FractionalPart(const lightNNNlm_complex&);
inline lightNNNNlm_complex  FractionalPart(const lightNNNNlm_complex&);

#ifndef C_LIGHT_DOUBLE
inline lm_complex FractionalPart(const int );
inline lightNint  FractionalPart(const lightNint&);
inline lightNNint  FractionalPart(const lightNNint&);
inline lightNNNint  FractionalPart(const lightNNNint&);
inline lightNNNNint  FractionalPart(const lightNNNNint&);
#endif

// IntegerPart
#ifndef C_LIGHT_DOUBLE
lightNint  IntegerPart(const lightNint&);
lightNNint  IntegerPart(const lightNNint&);
lightNNNint  IntegerPart(const lightNNNint&);
lightNNNNint  IntegerPart(const lightNNNNint&);
#endif

inline lm_complex IntegerPart(const lm_complex s); 
lightNlm_complex IntegerPart(const lightNlm_complex&);
lightNNlm_complex  IntegerPart(const lightNNlm_complex&);
lightNNNlm_complex  IntegerPart(const lightNNNlm_complex&);
lightNNNNlm_complex  IntegerPart(const lightNNNNlm_complex&);

//
// abs
//

lightNlm_complex abs(const lightNlm_complex& s);


lightN3lm_complex abs(const lightN3lm_complex& s);


lightNNlm_complex abs(const lightNNlm_complex& s);


lightNNNlm_complex abs(const lightNNNlm_complex& s);


lightNNNNlm_complex abs(const lightNNNNlm_complex& s);

// Mod

#ifndef C_LIGHT_DOUBLE
inline int Mod(const int m , const int n);
#endif

inline lm_complex Mod(const lm_complex m, const lm_complex n);
inline lm_complex Mod(const lm_complex m, const int n);
inline lm_complex Mod(const int m, const lm_complex n);

/*
lightNlm_complex Mod(const lightNint & i, const lightNlm_complex & d);
lightNlm_complex Mod(const lightNlm_complex & i, const lightNint & d);
lightNNlm_complex Mod(const lightNNint & i, const lightNNlm_complex & d);
lightNNlm_complex Mod(const lightNNlm_complex & i, const lightNNint & d);
lightNNNlm_complex Mod(const lightNNNlm_complex & i, const lightNNNint & d);
lightNNNlm_complex Mod(const lightNNNint & i, const lightNNNlm_complex & d);
lightNNNNlm_complex Mod(const lightNNNNlm_complex & i, const lightNNNNint & d);
lightNNNNlm_complex Mod(const lightNNNNint & i, const lightNNNNlm_complex & d);
*/




