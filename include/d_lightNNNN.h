//           -*- c++ -*-

#ifndef LIGHTNNNN_H
#define LIGHTNNNN_H
// <cd> lightNNNdouble
//
// .SS Functionality
//
// lightNNNN is a template for classes that implement tensors of rank 4
// with any number of elements. E.g. a lightNNNN s with 5x6x7x8 elements of
// type double can be instanciated with:
//
// <code>lightNNNN&lt;double&gt; s(5,6,7,8);</code>
//
// The size of the tensor can change during execution. It changes its
// size to whatever is needed when it is assigned a new value with an
// assignment operator.
//
// When a lightNNNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNNN object with 5x6x5x5 elements to one with 5x5x6x5 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHTNNNNdouble_H


#define LIGHTNNNN_SIZE1 4
#define LIGHTNNNN_SIZE2 4
#define LIGHTNNNN_SIZE3 4
#define LIGHTNNNN_SIZE4 4
#define LIGHTNNNN_SIZE (LIGHTNNNN_SIZE1*LIGHTNNNN_SIZE2*LIGHTNNNN_SIZE3*LIGHTNNNN_SIZE4)
// The default size.

 class lightN;
 class lightNN;
 class lightNNN;


class lightNNNNdouble {
public:

#ifdef IN_LIGHTNNNNdouble_H
  friend class lightNNNNint;
#else
  friend class lightNNNNdouble;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNNNint;
#endif

  #include "d_lightNNNN_auto.h"

  inline lightNNNNdouble();
  // Default constructor.

  inline lightNNNNdouble(const lightNNNNdouble&);
  // Copy constructor.

  inline lightNNNNdouble(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNNNdouble(const int, const int, const int, const int);
  // Construct a lightNNNN of given size.

  inline lightNNNNdouble(const int, const int, const int, const int, const double *);
  // Construct a lightNNNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNNNdouble(const int, const int, const int, const int, const double);
  // Construct a lightNNNN of given size and initialize all elements
  // with a value (the last argument).

  inline ~lightNNNNdouble();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNNNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNNNlm_complex();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNNNlm_complex();
  // Convert to complex.
#endif

  lightNNNNdouble& operator=(const lightNNNNdouble&);
  // Assignment.

  lightNNNNdouble& operator=(const double);
  // Assign one value to all elements.

  //
  // Operator ().
  //
  
  double operator()(const int x, const int y, const int z, const int v) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3) || (v<1) || (v>size4));
#ifdef ROWMAJOR
    return elem[(((x-1)*size2+(y-1))*size3+(z-1))*size4+v-1];
#else
    return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
#endif
  };
  // Get the value of one element.


  double& operator()(const int x, const int y, const int z, const int v) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3) || (v<1) || (v>size4));
#ifdef ROWMAJOR
    return elem[(((x-1)*size2+(y-1))*size3+(z-1))*size4+v-1];
#else
    return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
#endif
  };
  // Get/Set the value of one element.

  inline lightNdouble operator()(const int, const int, const int) const;
  // Get the value of a vector in the tensor.

  inline lightNNdouble operator()(const int, const int) const;
  // Get the value of a matrix in the tensor.

  inline lightNNNdouble operator()(const int) const;
  // Get the value of a tensor of rank 3 in the tensor.


  //
  // Set
  //

  inline lightNNNNdouble& Set (const int i0, const int i1, const int i2,
			    const double val);
  inline lightNNNNdouble& Set (const int i0, const int i1, const int i2,
			    const lightNdouble& arr);
  inline lightNNNNdouble& Set (const int i0, const int i1, const int i2,
			    const light3double& arr);
  inline lightNNNNdouble& Set (const int i0, const int i1, const int i2,
			    const light4double& arr);
  inline lightNNNNdouble& Set (const int i0, const int i1, const double val);
  inline lightNNNNdouble& Set (const int i0, const int i1, const lightNNdouble& arr);
  inline lightNNNNdouble& Set (const int i0, const int i1, const light33double& arr);
  inline lightNNNNdouble& Set (const int i0, const int i1, const light44double& arr);
  inline lightNNNNdouble& Set (const int i0, const double val);
  inline lightNNNNdouble& Set (const int i0, const lightNNNdouble& arr);

 
  int operator==(const lightNNNNdouble&) const;
  // Equality.

  int operator!=(const lightNNNNdouble&) const;
  // Inequality.
 
  lightNNNNdouble& operator+=(const double);
  // Add a value to all elements.

  lightNNNNdouble& operator+=(const lightNNNNdouble&);
  // Elementwise addition.

  lightNNNNdouble& operator-=(const double);
  // Subtract a value from all elements.

  lightNNNNdouble& operator-=(const lightNNNNdouble&);
  // Elementwise subtraction.

  lightNNNNdouble& operator*=(const double);
  // Mulitply all elements with a value.

  lightNNNNdouble& operator/=(const double);
  // Divide all elements with a value.

  lightNNNNdouble& SetShape(const int x=-1, const int y=-1, 
			 const int z=-1, const int v=-1);
  // Sets specified shape to the tensor. 

  double Extract(const lightNint&) const;
  // Extract an element using given index. 

  lightNNNNdouble& reshape(const int, const int, const int, const int, const lightNdouble&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c,d) will be set to s(a) in the lightNNNNdouble
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNNdouble& reshape(const int, const int, const int, const int, const lightNNdouble&);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c,d) will be set to s(a,b) in the
  // lightNNNN object. The values of the first two arguments must be
  // the same as the size of the lightNN-matrix. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNNdouble& reshape(const int, const int, const int, const int, const lightNNNdouble&);
  // Convert the lightNNN-tensor to the given size and put the result
  // in this object. (*this)(a,b,c,d) will be set to s(a,b,c) in the
  // lightNNNN object. The values of the first three arguments must be
  // the same as the size of the lightNNN-tensor. The program may dump
  // core or behave strangely if the arguments are incorrect.

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else if(x == 3)
      return size3;
    else if(x == 4)
      return size4;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(double *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const double *);
  // Set values of all elements from array (row major order).
 
  double * data() const;
  // Direct access to the stored data. Use the result with care. 

  lightNNNNdouble operator+() const;
  // Unary plus.

  lightNNNNdouble operator-() const;
  // Unary minus.


  friend inline lightNNNNdouble operator+(const lightNNNNdouble&, const lightNNNNdouble&);
  // Elementwise addition.

  friend inline lightNNNNdouble operator+(const lightNNNNdouble&, const double);
  // Addition to all elements.

  friend inline lightNNNNdouble operator+(const double, const lightNNNNdouble&);
  // Addition to all elements.

#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNNdouble operator+(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator+(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator+(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator+(const int, const lightNNNNdouble&);
#endif

  #ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator+(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator+(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNNdouble operator-(const lightNNNNdouble&, const lightNNNNdouble&);
  // Elementwise subtraction.

  friend inline lightNNNNdouble operator-(const lightNNNNdouble&, const double);
  // Subtraction from all elements.

  friend inline lightNNNNdouble operator-(const double, const lightNNNNdouble&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNNdouble operator-(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator-(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator-(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator-(const int, const lightNNNNdouble&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator-(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator-(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNNdouble operator*(const lightNNNNdouble&, const double);
  // Multiply all elements.

  friend inline lightNNNNdouble operator*(const double, const lightNNNNdouble&);
  // Multiply all elements.

#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNNdouble operator*(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator*(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator*(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator*(const int, const lightNNNNdouble&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator*(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator*(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNNdouble operator*(const lightNNNNdouble&, const lightNNdouble&);
  // Inner product.

  friend inline lightNNNNdouble operator*(const lightNNdouble&, const lightNNNNdouble&);
  // Inner product.

  friend inline lightNNNNdouble operator*(const lightNNNNdouble&, const light33double&);
  // Inner product.

  friend inline lightNNNNdouble operator*(const light33double&, const lightNNNNdouble&);
  // Inner product.

  friend inline lightNNNNdouble operator*(const lightNNNNdouble&, const light44double&);
  // Inner product.

  friend inline lightNNNNdouble operator*(const light44double&, const lightNNNNdouble&);
  // Inner product.

  friend inline lightNNNNdouble operator*(const lightNNNdouble&, const lightNNNdouble&);
  // Inner product.

  friend inline lightNNNNdouble operator/(const lightNNNNdouble&, const double);
  // Divide all elements.

  friend inline lightNNNNdouble operator/(const double, const lightNNNNdouble&);
  // Divide all elements.

#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNNdouble operator/(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator/(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator/(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator/(const int, const lightNNNNdouble&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator/(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator/(const double e, const lightNNNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNdouble arg(const lightNNNNlm_complex& a);
friend inline lightNNNNdouble re(const lightNNNNlm_complex& a);
friend inline lightNNNNdouble im(const lightNNNNlm_complex& a);
friend inline lightNNNNlm_complex conjugate(const lightNNNNlm_complex& a);
//#endif

  friend inline lightNNNNdouble pow(const lightNNNNdouble&, const lightNNNNdouble&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNNNdouble pow(const lightNNNNdouble&, const double);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNNdouble pow(const double, const lightNNNNdouble&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNNdouble ElemProduct(const lightNNNNdouble&, const lightNNNNdouble&);
  // Elementwise multiplication.

  friend inline lightNNNNdouble ElemQuotient(const lightNNNNdouble&, const lightNNNNdouble&);
  // Elementwise division.

  friend inline lightNNNNdouble Apply(const lightNNNNdouble&, double f(double));
  // Apply the function elementwise all elements.

  friend inline lightNNNNdouble Apply(const lightNNNNdouble&, const lightNNNNdouble&, double f(double, double));
  // Apply the function elementwise on all elements in the two tensors.

  friend inline lightNNNNdouble Transpose(const lightNNNNdouble&);
  // Transpose.

  
#ifdef IN_LIGHTNNNNlm_complex_H
#else
  friend inline lightNNNNdouble abs(const lightNNNNdouble&);
#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNNdouble abs(const lightNNNNlm_complex&);
#endif 
#endif


  // abs

#ifndef COMPLEX_TOOLS
  friend inline lightNNNNint sign(const lightNNNNint&);
  // sign

  friend inline lightNNNNint sign(const lightNNNNdouble&);
  // sign
#else
  friend inline lightNNNNlm_complex sign(const lightNNNNlm_complex&);
#endif


/// Added 2/2/98 

  friend inline lightNNNNdouble  FractionalPart(const lightNNNNdouble&);
  //  FractionalPart

  friend inline lightNNNNdouble  IntegerPart(const lightNNNNdouble&);
  //  IntegerPart
 
  //friend inline lightNNNNint  IntegerPart(const lightNNNNdouble&);
  //  IntegerPart

  friend inline lightNNNNdouble Mod (const  lightNNNNdouble&,const  lightNNNNdouble&);
  // Mod
 
  friend inline lightNNNNdouble Mod (const  lightNNNNdouble&,const double);
  // Mod

  friend inline lightNNNNdouble Mod (const double,const  lightNNNNdouble&);
  // Mod

  friend inline double LightMax (const lightNNNNdouble& );
  // Max

  friend inline double LightMin (const lightNNNNdouble& );
  // Min

  friend inline double findLightMax (const  double *,const  int );
  // Find Max
 
  friend inline double findLightMin (const  double *,const  int );
  // Find Min
 
  /// End Added 

  friend inline lightNNNNint ifloor(const lightNNNNdouble&);
  // ifloor

  friend inline lightNNNNint iceil(const lightNNNNdouble&);
  // iceil

  friend inline lightNNNNint irint(const lightNNNNdouble&);
  // irint

  friend inline lightNNNNdouble sqrt(const lightNNNNdouble&);
  // sqrt

  friend inline lightNNNNdouble exp(const lightNNNNdouble&);
  // exp

  friend inline lightNNNNdouble log(const lightNNNNdouble&);
  // log

  friend inline lightNNNNdouble sin(const lightNNNNdouble&);
  // sin

  friend inline lightNNNNdouble cos(const lightNNNNdouble&);
  // cos

  friend inline lightNNNNdouble tan(const lightNNNNdouble&);
  // tan

  friend inline lightNNNNdouble asin(const lightNNNNdouble&);
  // asin

  friend inline lightNNNNdouble acos(const lightNNNNdouble&);
  // acos

  friend inline lightNNNNdouble atan(const lightNNNNdouble&);
  // atan

  friend inline lightNNNNdouble sinh(const lightNNNNdouble&);
  // sinh

  friend inline lightNNNNdouble cosh(const lightNNNNdouble&);
  // cosh

  friend inline lightNNNNdouble tanh(const lightNNNNdouble&);
  // tanh

  friend inline lightNNNNdouble asinh(const lightNNNNdouble&);
  // asinh

  friend inline lightNNNNdouble acosh(const lightNNNNdouble&);
  // acosh

  friend inline lightNNNNdouble atanh(const lightNNNNdouble&);
  // atanh

  friend inline lightNNNNlm_complex ifloor(const lightNNNNlm_complex&);
  // ifloor

  friend inline lightNNNNlm_complex iceil(const lightNNNNlm_complex&);
  // iceil

  friend inline lightNNNNlm_complex irint(const lightNNNNlm_complex&);
  // irint

  friend inline lightNNNNlm_complex sqrt(const lightNNNNlm_complex&);
  // sqrt

  friend inline lightNNNNlm_complex exp(const lightNNNNlm_complex&);
  // exp

  friend inline lightNNNNlm_complex log(const lightNNNNlm_complex&);
  // log

  friend inline lightNNNNlm_complex sin(const lightNNNNlm_complex&);
  // sin

  friend inline lightNNNNlm_complex cos(const lightNNNNlm_complex&);
  // cos

  friend inline lightNNNNlm_complex tan(const lightNNNNlm_complex&);
  // tan

  friend inline lightNNNNlm_complex asin(const lightNNNNlm_complex&);
  // asin

  friend inline lightNNNNlm_complex acos(const lightNNNNlm_complex&);
  // acos

  friend inline lightNNNNlm_complex atan(const lightNNNNlm_complex&);
  // atan

  friend inline lightNNNNlm_complex sinh(const lightNNNNlm_complex&);
  // sinh

  friend inline lightNNNNlm_complex cosh(const lightNNNNlm_complex&);
  // cosh

  friend inline lightNNNNlm_complex tanh(const lightNNNNlm_complex&);
  // tanh

  friend inline lightNNNNlm_complex asinh(const lightNNNNlm_complex&);
  // asinh

  friend inline lightNNNNlm_complex acosh(const lightNNNNlm_complex&);
  // acosh

  friend inline lightNNNNlm_complex atanh(const lightNNNNlm_complex&);
  // atanh

#ifdef IN_LIGHTNNNNint_H
  friend inline lightNNNdouble light_mean     ( const  lightNNNNdouble& );
  friend inline lightNNNdouble light_standard_deviation( const  lightNNNNdouble& );
  friend inline lightNNNdouble light_variance ( const  lightNNNNdouble& );
  friend inline lightNNNdouble light_median   ( const  lightNNNNdouble& );
#else
  friend inline lightNNNdouble light_mean     ( const  lightNNNNdouble& );
  friend inline lightNNNdouble light_variance ( const  lightNNNNdouble& );
  friend inline lightNNNdouble light_standard_deviation( const  lightNNNNdouble& );
  friend inline lightNNNdouble light_median   ( const  lightNNNNdouble& );
#endif
  
  friend inline lightNNNdouble light_total    ( const  lightNNNNdouble& );

  friend inline lightNNNNdouble light_sort (const lightNNNNdouble arr1);
  
  friend inline lightNdouble light_flatten (const lightNNNNdouble s);
  friend inline lightNdouble light_flatten (const lightNNNNdouble s, int level);
  friend inline lightNNNNdouble light_flatten0 (const lightNNNNdouble s);
  friend inline lightNNNdouble light_flatten1 (const lightNNNNdouble s);
  friend inline lightNNdouble light_flatten2 (const lightNNNNdouble s);

  //<ignore>
#ifdef IN_LIGHTNNNNlm_complex_H  
  friend class lightNNNNint;
#endif  
  friend class lightNdouble;
  friend class lightNNdouble;
  friend class lightNNNdouble;

  friend inline lightNNNNdouble light_join (const lightNNNNdouble, const lightNNNNdouble);
  friend inline lightNNNNdouble light_drop (const lightNNNNdouble arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNNNdouble caNNNot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNNNdouble::lightNNNNdouble(const lightNNNNint& s1, const double e, const lightmat_plus_enum)

  double *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // Size of the first index.

  int size2;
  // Size of the second index.

  int size3;
  // Size of the third index.

  int size4;
  // Size of the fourth index.


protected:
  double sarea[LIGHTNNNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNNNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  lightNNNNdouble(const int, const int, const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // four arguments is the size of the created tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNNNdouble(const lightNNNNdouble&, const lightNNNNdouble&, const lightmat_plus_enum);
  lightNNNNdouble(const lightNNNNdouble&, const double, const lightmat_plus_enum);

#ifdef IN_LIGHTNNNNdouble_H
  lightNNNNdouble(const lightNNNNdouble&, const int, const lightmat_plus_enum);
  lightNNNNdouble(const lightNNNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNNNdouble(const lightNNNNdouble&, const lightNNNNdouble&, const lightmat_minus_enum);
  lightNNNNdouble(const double, const lightNNNNdouble&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNNNdouble_H
  lightNNNNdouble(const int, const lightNNNNdouble&, const lightmat_minus_enum);
  lightNNNNdouble(const double, const lightNNNNint&, const lightmat_minus_enum);
#endif

  lightNNNNdouble(const lightNNNNdouble&, const double, const lightmat_mult_enum);
#ifdef IN_LIGHTNNNNdouble_H
  lightNNNNdouble(const lightNNNNdouble&, const int, const lightmat_mult_enum);
  lightNNNNdouble(const lightNNNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNNNdouble(const lightNNNNdouble&, const lightNNdouble&, const lightmat_mult_enum);
  lightNNNNdouble(const lightNNdouble&, const lightNNNNdouble&, const lightmat_mult_enum);
  lightNNNNdouble(const lightNNNNdouble&, const light33double&, const lightmat_mult_enum);
  lightNNNNdouble(const light33double&, const lightNNNNdouble&, const lightmat_mult_enum);
  lightNNNNdouble(const lightNNNNdouble&, const light44double&, const lightmat_mult_enum);
  lightNNNNdouble(const light44double&, const lightNNNNdouble&, const lightmat_mult_enum);
  lightNNNNdouble(const lightNNNdouble&, const lightNNNdouble&, const lightmat_mult_enum);

  lightNNNNdouble(const double, const lightNNNNdouble&, const lightmat_div_enum);
#ifdef IN_LIGHTNNNNdouble_H
  lightNNNNdouble(const int, const lightNNNNdouble&, const lightmat_div_enum);
  lightNNNNdouble(const double, const lightNNNNint&, const lightmat_div_enum);
#endif

  lightNNNNdouble(const lightNNNNdouble&, const lightNNNNdouble&, const lightmat_pow_enum);
  lightNNNNdouble(const lightNNNNdouble&, const double, const lightmat_pow_enum);
  lightNNNNdouble(const double, const lightNNNNdouble&, const lightmat_pow_enum);
 

#ifdef IN_LIGHTNNNNlm_complex_H
 #else
  #ifdef IN_LIGHTNNNNdouble_H
   lightNNNNdouble(const lightNNNNdouble&, const lightmat_abs_enum);
   lightNNNNdouble(const lightNNNNlm_complex&, const lightmat_abs_enum);
  #else
  lightNNNNdouble(const lightNNNNdouble&, const lightmat_abs_enum);
  #endif
#endif


  lightNNNNdouble(const lightNNNNdouble&, const lightNNNNdouble&, const lightmat_eprod_enum);
  lightNNNNdouble(const lightNNNNdouble&, const lightNNNNdouble&, const lightmat_equot_enum);
  lightNNNNdouble(const lightNNNNdouble&, double f(double), const lightmat_apply_enum);
  lightNNNNdouble(const lightNNNNdouble&, const lightNNNNdouble&, double f(double, double), const lightmat_apply_enum);
  lightNNNNdouble(const lightNNNNdouble&, const lightmat_trans_enum);
};

typedef lightNNNNdouble doubleNNNN;
typedef lightNNNNint intNNNN;
typedef lightNNNNlm_complex lm_complexNNNN;

#undef IN_LIGHTNNNNdouble_H

#endif
