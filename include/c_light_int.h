//
// sign
//


inline int sign(int    x);	

#ifndef __GNUG__

inline  int sign(double x);	

#else

// GNU has this symbol in library. We do not inline it.
        int sign(double x);	
#endif

// See details in light_int.icc
// These functions are defined once there.


#ifndef COMPLEX_TOOLS

light3int sign(const light3lm_complex& s);	


light4int sign(const light4lm_complex& s);	


light33int sign(const light33lm_complex& s);	


light44int sign(const light44lm_complex& s);	


lightNint sign(const lightNlm_complex& s);	


lightN3int sign(const lightN3lm_complex& s);	


lightNNint sign(const lightNNlm_complex& s);	


lightNNNint sign(const lightNNNlm_complex& s);	


lightNNNNint sign(const lightNNNNlm_complex& s);	

//
// abs
//

lightNlm_complex abs(const lightNlm_complex& s);


lightN3lm_complex abs(const lightN3lm_complex& s);


lightNNlm_complex abs(const lightNNlm_complex& s);


lightNNNlm_complex abs(const lightNNNlm_complex& s);


lightNNNNlm_complex abs(const lightNNNNlm_complex& s);


#else
light3lm_complex sign(const light3lm_complex& s);	


light4lm_complex sign(const light4lm_complex& s);	


light33lm_complex sign(const light33lm_complex& s);	


light44lm_complex sign(const light44lm_complex& s);	


lightNlm_complex sign(const lightNlm_complex& s);	


lightN3lm_complex sign(const lightN3lm_complex& s);	


lightNNlm_complex sign(const lightNNlm_complex& s);	


lightNNNlm_complex sign(const lightNNNlm_complex& s);	


lightNNNNlm_complex sign(const lightNNNNlm_complex& s);	
#endif

//
// Mod
//


lightNlm_complex Mod(const lightNlm_complex& v, const lm_complex s );


lightNlm_complex Mod(const lm_complex v, const lightNlm_complex& s);


lightNlm_complex Mod(const lightNlm_complex&v1, const lightNlm_complex&v2);




lightNNlm_complex Mod(const lightNNlm_complex& v, const lm_complex s );


lightNNlm_complex Mod(const lm_complex v, const lightNNlm_complex& s);


lightNNlm_complex Mod(const lightNNlm_complex&v1, const lightNNlm_complex&v2);




lightNNNlm_complex Mod(const lightNNNlm_complex& v, const lm_complex s );


lightNNNlm_complex Mod(const lm_complex v, const lightNNNlm_complex& s);


lightNNNlm_complex Mod(const lightNNNlm_complex&v1, const lightNNNlm_complex&v2);




lightNNNNlm_complex Mod(const lightNNNNlm_complex& v, const lm_complex s );


lightNNNNlm_complex Mod(const lm_complex v, const lightNNNNlm_complex& s);


lightNNNNlm_complex Mod(const lightNNNNlm_complex&v1, const lightNNNNlm_complex&v2);



