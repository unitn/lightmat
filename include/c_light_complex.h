






//
// fractional part
//
inline lm_complex FractionalPart(const lm_complex s);
inline lightNlm_complex  FractionalPart(const lightNlm_complex&);
inline lightNNlm_complex  FractionalPart(const lightNNlm_complex&);

inline lightNNNlm_complex  FractionalPart(const lightNNNlm_complex&);

inline lightNNNNlm_complex  FractionalPart(const lightNNNNlm_complex&);


// IntegerPart
inline lm_complex IntegerPart(const lm_complex s);
lightNlm_complex  IntegerPart(const lightNlm_complex&);

lightNNlm_complex  IntegerPart(const lightNNlm_complex&);

lightNNNlm_complex  IntegerPart(const lightNNNlm_complex&);

lightNNNNlm_complex  IntegerPart(const lightNNNNlm_complex&);


// Mod

inline lm_complex Mod(const lm_complex m, const lm_complex n);


// abs, arg, im, re, conjugate 

inline lightNdouble abs(const lightNlm_complex& a);
inline lightNdouble arg(const lightNlm_complex& a);
inline lightNdouble re(const lightNlm_complex& a);
inline lightNdouble im(const lightNlm_complex& a);
inline lightNlm_complex conjugate(const lightNlm_complex& a);

inline lightNNdouble abs(const lightNNlm_complex& a);
inline lightNNdouble arg(const lightNNlm_complex& a);
inline lightNNdouble re(const lightNNlm_complex& a);
inline lightNNdouble im(const lightNNlm_complex& a);
inline lightNNlm_complex conjugate(const lightNNlm_complex& a);

inline lightNNNdouble abs(const lightNNNlm_complex& a);
inline lightNNNdouble arg(const lightNNNlm_complex& a);
inline lightNNNdouble re(const lightNNNlm_complex& a);
inline lightNNNdouble im(const lightNNNlm_complex& a);
inline lightNNNlm_complex conjugate(const lightNNNlm_complex& a);

inline lightNNNNdouble abs(const lightNNNNlm_complex& a);
inline lightNNNNdouble arg(const lightNNNNlm_complex& a);
inline lightNNNNdouble re(const lightNNNNlm_complex& a);
inline lightNNNNdouble im(const lightNNNNlm_complex& a);
inline lightNNNNlm_complex conjugate(const lightNNNNlm_complex& a);




