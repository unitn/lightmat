//           -*- c++ -*-

#ifndef LIGHTNN_H
#define LIGHTNN_H
// <cd> lightNNdouble
//
// .SS Functionality
//
// lightNN is a template for classes that implement matrices with any
// number of elements. E.g. a matrix v with 5x6 elements of type double
// can be instanciated with:
//
// <code>lightNN&lt;double&gt; v(5,6);</code>
//
// The number of rows and columns in the matrix can change during
// execution. It changes its size to whatever is needed when it is
// assigned a new value with an assignment operator.
//
// When a lightNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNN object with 5x5 elements to one with 5x6 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
// Vadim Engelson
//

#include <assert.h>

#define IN_LIGHTNNdouble_H


#define LIGHTNN_SIZE1 10
#define LIGHTNN_SIZE2 10
#define LIGHTNN_SIZE (LIGHTNN_SIZE1*LIGHTNN_SIZE2)
// The default size.

 class light3;
 class light4;
 class lightN;
 class light33;
 class light44;
 class lightN3;
 class lightNNN;


class lightNNdouble {
public:

#ifdef IN_LIGHTNNdouble_H
  friend class lightNNint;
#else
  friend class lightNNdouble;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNint;
#endif


  #include "d_lightNN_auto.h"

  inline lightNNdouble();
  // Default constructor.

  inline lightNNdouble(const lightNNdouble&);
  // Copy constructor.

  inline lightNNdouble(const light33double&);
  // Conversion.

  inline lightNNdouble(const light44double&);
  // Conversion.

  inline lightNNdouble(const lightN3double&);
  // Conversion.

  inline lightNNdouble(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNdouble(const int, const int);
  // Construct a lightNN of given size.

  inline lightNNdouble(const int, const int, const double *);
  // Construct a lightNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNdouble(const int, const int, const double);
  // Construct a lightNN of given size and initialize all elements
  // with a value (the third argument).

  inline ~lightNNdouble();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNlm_complex();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNlm_complex();
  // Convert to complex.
#endif



  lightNNdouble& operator=(const lightNNdouble&);
  // Assignment.

  lightNNdouble& operator=(const light33double&);
  // Assignment, change size to 3x3.

  lightNNdouble& operator=(const light44double&);
  // Assignment, change size to 4x4.

  lightNNdouble& operator=(const lightN3double&);
  // Assignment, change size to Nx3.

  lightNNdouble& operator=(const double);
  // Assign one value to all elements.

  double operator() (const int x, const int y) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2));
#ifdef ROWMAJOR
    return elem[y-1 + (x-1)*size2];
#else
    return elem[x-1 + (y-1)*size1];
#endif
  };
  // Get the value of one element.

  //
  // Set and Get
  //

  double& operator()(const int x, const int y) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2));
#ifdef ROWMAJOR
    return elem[y-1 + (x-1)*size2];
#else
    return elem[x-1 + (y-1)*size1];
#endif
  };
  // Get/Set the value of one element.

  //
  // Set
  //

  inline lightNNdouble& Set (const int i0, const double val);
  inline lightNNdouble& Set (const int i0, const lightNdouble& arr);
  inline lightNNdouble& Set (const int i0, const light3double& arr);
  inline lightNNdouble& Set (const int i0, const light4double& arr);


  double&  takeElementReference (const int x, const int y);
  // Same

  lightNdouble operator()(const int) const;
  // Get the value of one row.
 
  const lightNdouble& SetCol(const int n,const lightNdouble& x);
  // Put x into column n. Return x.

  const lightNdouble& SetRow(const int n,const lightNdouble& x);
  // Put x into row n. Return x.
  
  double SetCol(const int n,const double x);
  // Put x into all elements of  column n. Return x.

  double SetRow(const int n,const double x);
   // Put x into all elements of row n. Return x.

  const lightNNdouble& SetCols(const int n1,const int n2,const  lightNNdouble& x);
  // Put every  element of matrix x to the columns n1..n2. Return x. 
  // In this function and below:
  // n1=0 means to use  1. 
  // n2=0 means to use the largest possible value of matrix index.

  const lightNNdouble& SetRows(const int n1,const int n2,const  lightNNdouble& x);
  // Put every  element of matrix x to the rows n1..n2.  Return x. 
  
  double SetCols(const int n1, const int n2, const  double x);
  // Put x to very element of all columns n1..n2. Return x. 
  
  double SetRows(const int n1,const int n2,const  double x);
  // Put x to very eleme nt of all rows n1..n2. Return x. 
  
  lightNdouble  Col(const int n) const;
  // Get Column n.  

  lightNdouble  Row(const int n) const;
  // Get Row n.

// REMOVE LATER
  inline lightNNdouble   SubMatrix(const int r1, const int r2,const int c1, const int c2) const;
  // Get submatrix of rows r1..r2, columns c1..c2.

  inline const lightNNdouble& SetSubMatrix(const int r1,const int r2,const int c1,const int c2,
                  const  lightNNdouble& x);
  // Put matrix x into  submatrix  of rows r1..r2, columns c1..c2. Return x.

  inline  double SetSubMatrix(const int r1,const int r2,const int c1,const int c2,
                   const double x);
  // Put x into rows r1..r2, columns c1..c2. Return x.
  
  lightNNdouble Rows(const int x1,const int x2) const;
  // Get submstrix produced by rows x1..x2.
  
  
  lightNNdouble Cols(const int y1,const int y2) const;
  // Get submstrix produced by columns  y1..y2.
  
  int operator==(const lightNNdouble&) const;
  // Equality.

  int operator!=(const lightNNdouble&) const;
  // Inequality.
 
  lightNNdouble& operator+=(const double);
  // Add a value to all elements.

  lightNNdouble& operator+=(const lightNNdouble&);
  // Elementwise addition.

  lightNNdouble& operator-=(const double);
  // Subtract a value from all elements.

  lightNNdouble& operator-=(const lightNNdouble&);
  // Elementwise subtraction.

  lightNNdouble& operator*=(const double);
  // Muliply all elements with a value.

  lightNNdouble& operator/=(const double);
  // Divide all elements with a value.
 
  inline lightNNdouble& SetShape(const int x=-1, const int y=-1);
  // Sets specified shape for the matrix.

  double Extract(const lightNint&) const;
  // Extract an element using given index.   

  lightNNdouble& reshape(const int, const int, const lightNdouble&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. All vector-columns of the lightNN object will get
  // the value of the lightN-argument. The value of the first argument
  // must be the same as the number of elements in the lightN-vector.

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(double *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const double *);
  // Set values of all elements from array (row major order).
 
  double * data() const; 
  // Direct access to the stored data. Use the result with care.

  lightNNdouble operator+() const;
  // Unary plus.

  lightNNdouble operator-() const;
  // Unary minus.

  friend inline lightNNdouble operator+(const lightNNdouble&, const lightNNdouble&);
  // Elementwise addition.

  friend inline lightNNdouble operator+(const lightNNdouble&, const double);
  // Addition to all elements.

  friend inline lightNNdouble operator+(const double, const lightNNdouble&);
  // Addition to all elements.

#ifdef IN_LIGHTNNdouble_H
  friend inline lightNNdouble operator+(const lightNNdouble&, const int);
  friend inline lightNNdouble operator+(const lightNNint&, const double);
  friend inline lightNNdouble operator+(const double, const lightNNint&);
  friend inline lightNNdouble operator+(const int, const lightNNdouble&);
#endif

#ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNlm_complex operator+(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator+(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator+(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator+(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNNdouble operator-(const lightNNdouble&, const lightNNdouble&);
  // Elementwise subtraction.

  friend inline lightNNdouble operator-(const lightNNdouble&, const double);
  // Subtraction from all elements.

  friend inline lightNNdouble operator-(const double, const lightNNdouble&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNdouble_H
  friend inline lightNNdouble operator-(const lightNNdouble&, const int);
  friend inline lightNNdouble operator-(const lightNNint&, const double);
  friend inline lightNNdouble operator-(const double, const lightNNint&);
  friend inline lightNNdouble operator-(const int, const lightNNdouble&);
#endif

  #ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNlm_complex operator-(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator-(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator-(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator-(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNNdouble operator*(const lightNNdouble&, const lightNNdouble&);
  // Inner product.

  friend inline lightNNdouble operator*(const lightNNdouble&, const double);
  // Multiply all elements.

  friend inline lightNNdouble operator*(const double, const lightNNdouble& s);
  // Multiply all elements.

#ifdef IN_LIGHTNNdouble_H
  friend inline lightNNdouble operator*(const lightNNdouble&, const int);
  friend inline lightNNdouble operator*(const lightNNint&, const double);
  friend inline lightNNdouble operator*(const double, const lightNNint&);
  friend inline lightNNdouble operator*(const int, const lightNNdouble&);
#endif

#ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNlm_complex operator*(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator*(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator*(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator*(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNNdouble operator*(const lightNNdouble&, const light33double&);
  // Inner product.

  friend inline lightNNdouble operator*(const lightNNdouble&, const light44double&);
  // Inner product.

  friend inline lightNNdouble operator*(const lightN3double&, const lightNNdouble&);
  // Inner product.

  friend inline lightNNdouble operator*(const light33double&, const lightNNdouble&);
  // Inner product.

  friend inline lightNNdouble operator*(const light44double&, const lightNNdouble&);
  // Inner product.

  friend inline lightNNdouble operator*(const lightNdouble&, const lightNNNdouble&);
  // Inner product.

  friend inline lightNNdouble operator*(const lightNNNdouble&, const lightNdouble&);
  // Inner product.

  friend inline lightNNdouble operator*(const light3double&, const lightNNNdouble&);
  // Inner product.

  friend inline lightNNdouble operator*(const lightNNNdouble&, const light3double&);
  // Inner product.

  friend inline lightNNdouble operator*(const light4double&, const lightNNNdouble&);
  // Inner product.

  friend inline lightNNdouble operator*(const lightNNNdouble&, const light4double&);
  // Inner product.

  friend inline lightNNdouble operator/(const lightNNdouble&, const double);
  // Divide all elements.

  friend inline lightNNdouble operator/(const double, const lightNNdouble&);
  // Divide all elements.

#ifdef IN_LIGHTNNdouble_H
  friend inline lightNNdouble operator/(const lightNNdouble&, const int);
  friend inline lightNNdouble operator/(const lightNNint&, const double);
  friend inline lightNNdouble operator/(const double, const lightNNint&);
  friend inline lightNNdouble operator/(const int, const lightNNdouble&);
#endif

#ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNlm_complex operator/(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator/(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator/(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator/(const double e, const lightNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNdouble arg(const lightNNlm_complex& a);
friend inline lightNNdouble re(const lightNNlm_complex& a);
friend inline lightNNdouble im(const lightNNlm_complex& a);
friend inline lightNNlm_complex conjugate(const lightNNlm_complex& a);
//#endif

  friend inline lightNNdouble pow(const lightNNdouble&, const lightNNdouble&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNdouble pow(const lightNNdouble&, const double);
  // Raise to the power of-function, for all elements.

  friend inline lightNNdouble pow(const double, const lightNNdouble&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNdouble ElemProduct(const lightNNdouble&, const lightNNdouble&);
  // Elementwise multiplication.

  friend inline lightNNdouble ElemQuotient(const lightNNdouble&, const lightNNdouble&);
  // Elementwise division.

  friend inline lightNNdouble Apply(const lightNNdouble&, double f(double));
  // Apply the function elementwise all elements.

  friend inline lightNNdouble Apply(const lightNNdouble&, const lightNNdouble&, double f(double, double));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline lightNNdouble Transpose(const lightNNdouble&);
  // Transpose matrix.

  friend inline lightNNdouble Transpose(const lightN3double&);
  // Transpose matrix.

  friend inline lightNNdouble OuterProduct(const lightNdouble&, const lightNdouble&);
  // Outer product.


#ifdef IN_LIGHTNNlm_complex_H
#else
  friend inline lightNNdouble abs(const lightNNdouble&);
#ifdef IN_LIGHTNNdouble_H
  friend inline lightNNdouble abs(const lightNNlm_complex&);
#endif 
#endif


#ifndef COMPLEX_TOOLS
  friend inline lightNNint sign(const lightNNint&);
  // sign
#else
  // sign
  friend inline lightNNlm_complex sign(const lightNNlm_complex&);
#endif
  friend inline lightNNint sign(const lightNNdouble&);
  // sign

  /// Added 2/2/98 

  friend inline lightNNdouble  FractionalPart(const lightNNdouble&);
  //  FractionalPart

  friend inline lightNNdouble  IntegerPart(const lightNNdouble&);
  //  IntegerPart

  //friend inline lightNNint  IntegerPart(const lightNNdouble&);
  //  IntegerPart

  friend inline lightNNdouble Mod (const  lightNNdouble&,const  lightNNdouble&);
  // Mod
 
  friend inline lightNNdouble Mod (const  lightNNdouble&,const double);
  // Mod

  friend inline lightNNdouble Mod (const double,const  lightNNdouble&);
  // Mod

  friend inline double LightMax (const lightNNdouble& );
  // Max

  friend inline double LightMin (const lightNNdouble& );
  // Min

  friend inline double findLightMax (const  double *,const  int );
  // Find Max
 
  friend inline double findLightMin (const  double *,const  int );
  // Find Min
 
  /// End Added 



  friend inline lightNNint ifloor(const lightNNdouble&);
  // ifloor

  friend inline lightNNint iceil(const lightNNdouble&);
  // iceil

  friend inline lightNNint irint(const lightNNdouble&);
  // irint

  friend inline lightNNdouble sqrt(const lightNNdouble&);
  // sqrt

  friend inline lightNNdouble exp(const lightNNdouble&);
  // exp

  friend inline lightNNdouble log(const lightNNdouble&);
  // log

  friend inline lightNNdouble sin(const lightNNdouble&);
  // sin

  friend inline lightNNdouble cos(const lightNNdouble&);
  // cos

  friend inline lightNNdouble tan(const lightNNdouble&);
  // tan

  friend inline lightNNdouble asin(const lightNNdouble&);
  // asin

  friend inline lightNNdouble acos(const lightNNdouble&);
  // acos

  friend inline lightNNdouble atan(const lightNNdouble&);
  // atan

  friend inline lightNNdouble sinh(const lightNNdouble&);
  // sinh

  friend inline lightNNdouble cosh(const lightNNdouble&);
  // cosh

  friend inline lightNNdouble tanh(const lightNNdouble&);
  // tanh

  friend inline lightNNdouble asinh(const lightNNdouble&);
  // asinh

  friend inline lightNNdouble acosh(const lightNNdouble&);
  // acosh

  friend inline lightNNdouble atanh(const lightNNdouble&);
  // atanh
  
  friend inline lightNNlm_complex ifloor(const lightNNlm_complex&);
  // ifloor

  friend inline lightNNlm_complex iceil(const lightNNlm_complex&);
  // iceil

  friend inline lightNNlm_complex irint(const lightNNlm_complex&);
  // irint

  friend inline lightNNlm_complex sqrt(const lightNNlm_complex&);
  // sqrt

  friend inline lightNNlm_complex exp(const lightNNlm_complex&);
  // exp

  friend inline lightNNlm_complex log(const lightNNlm_complex&);
  // log

  friend inline lightNNlm_complex sin(const lightNNlm_complex&);
  // sin

  friend inline lightNNlm_complex cos(const lightNNlm_complex&);
  // cos

  friend inline lightNNlm_complex tan(const lightNNlm_complex&);
  // tan

  friend inline lightNNlm_complex asin(const lightNNlm_complex&);
  // asin

  friend inline lightNNlm_complex acos(const lightNNlm_complex&);
  // acos

  friend inline lightNNlm_complex atan(const lightNNlm_complex&);
  // atan

  friend inline lightNNlm_complex sinh(const lightNNlm_complex&);
  // sinh

  friend inline lightNNlm_complex cosh(const lightNNlm_complex&);
  // cosh

  friend inline lightNNlm_complex tanh(const lightNNlm_complex&);
  // tanh

  friend inline lightNNlm_complex asinh(const lightNNlm_complex&);
  // asinh

  friend inline lightNNlm_complex acosh(const lightNNlm_complex&);
  // acosh

  friend inline lightNNlm_complex atanh(const lightNNlm_complex&);
  // atanh



#ifdef IN_LIGHTNNint_H
  friend inline lightNdouble light_mean     ( const  lightNNdouble& );
  friend inline lightNdouble light_standard_deviation( const  lightNNdouble& );
  friend inline lightNdouble light_variance ( const  lightNNdouble& );
  friend inline lightNdouble light_median   ( const  lightNNdouble& );
#else
  friend inline lightNdouble light_mean     ( const  lightNNdouble& );
  friend inline lightNdouble light_variance ( const  lightNNdouble& );
  friend inline lightNdouble light_standard_deviation( const  lightNNdouble& );
  friend inline lightNdouble light_median   ( const  lightNNdouble& );
#endif
  
  friend inline lightNdouble light_total    ( const  lightNNdouble& );
 
 
  friend inline lightNNdouble light_sort (const lightNNdouble arr1);
  
  friend inline lightNdouble light_flatten (const lightNNdouble s);
  friend inline lightNdouble light_flatten (const lightNNdouble s, int level);
  friend inline lightNNdouble light_flatten0 (const lightNNdouble s);


  //<ignore>
#ifdef IN_LIGHTNNlm_complex_H  
  friend class lightNNint;
#endif  
  friend class light3double;
  friend class light4double;
  friend class lightNdouble;
  friend class light33double;
  friend class light44double;
  friend class lightN3double;
  friend class lightNNNdouble;
  friend class lightNNNNdouble;
  friend class lightN33double;

  friend inline lightNNdouble light_join (const lightNNdouble, const lightNNdouble);
  friend inline lightNNdouble light_drop (const lightNNdouble arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNdouble::lightNNdouble(const lightNNint& s1, const double e, const lightmat_plus_enum)
  double *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.


protected:
  double sarea[LIGHTNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNNdouble(const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // two arguments is the size of the created matrix. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNdouble(const lightNNdouble&, const lightNNdouble&, const lightmat_plus_enum);
  lightNNdouble(const lightNNdouble&, const double, const lightmat_plus_enum);

#ifdef IN_LIGHTNNdouble_H
  lightNNdouble(const lightNNdouble&, const int, const lightmat_plus_enum);
  lightNNdouble(const lightNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNdouble(const lightNNdouble&, const lightNNdouble&, const lightmat_minus_enum);
  lightNNdouble(const double, const lightNNdouble&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNdouble_H
  lightNNdouble(const int, const lightNNdouble&, const lightmat_minus_enum);
  lightNNdouble(const double, const lightNNint&, const lightmat_minus_enum);
#endif

  lightNNdouble(const lightNNdouble&, const lightNNdouble&, const lightmat_mult_enum);
  lightNNdouble(const lightNNdouble&, const double, const lightmat_mult_enum);

#ifdef IN_LIGHTNNdouble_H
  lightNNdouble(const lightNNdouble&, const int, const lightmat_mult_enum);
  lightNNdouble(const lightNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNdouble(const lightNNdouble&, const light33double&, const lightmat_mult_enum);
  lightNNdouble(const lightNNdouble&, const light44double&, const lightmat_mult_enum);
  lightNNdouble(const lightN3double&, const lightNNdouble&, const lightmat_mult_enum);
  lightNNdouble(const light33double&, const lightNNdouble&, const lightmat_mult_enum);
  lightNNdouble(const light44double&, const lightNNdouble&, const lightmat_mult_enum);
  lightNNdouble(const lightNdouble&, const lightNNNdouble&, const lightmat_mult_enum);
  lightNNdouble(const lightNNNdouble&, const lightNdouble&, const lightmat_mult_enum);
  lightNNdouble(const light3double&, const lightNNNdouble&, const lightmat_mult_enum);
  lightNNdouble(const lightNNNdouble&, const light3double&, const lightmat_mult_enum);
  lightNNdouble(const light4double&, const lightNNNdouble&, const lightmat_mult_enum);
  lightNNdouble(const lightNNNdouble&, const light4double&, const lightmat_mult_enum);

  lightNNdouble(const double, const lightNNdouble&, const lightmat_div_enum);
#ifdef IN_LIGHTNNdouble_H
  lightNNdouble(const int, const lightNNdouble&, const lightmat_div_enum);
  lightNNdouble(const double, const lightNNint&, const lightmat_div_enum);
#endif

  lightNNdouble(const lightNNdouble&, const lightNNdouble&, const lightmat_pow_enum);
  lightNNdouble(const lightNNdouble&, const double, const lightmat_pow_enum);
  lightNNdouble(const double, const lightNNdouble&, const lightmat_pow_enum);
  //lightNNdouble(const lightNNdouble&, const lightmat_abs_enum);

#ifdef IN_LIGHTNNlm_complex_H
 #else
  #ifdef IN_LIGHTNNdouble_H
   lightNNdouble(const lightNNdouble&, const lightmat_abs_enum);
   lightNNdouble(const lightNNlm_complex&, const lightmat_abs_enum);
  #else
  lightNNdouble(const lightNNdouble&, const lightmat_abs_enum);
  #endif
#endif

  lightNNdouble(const lightNNdouble&, const lightNNdouble&, const lightmat_eprod_enum);
  lightNNdouble(const lightNNdouble&, const lightNNdouble&, const lightmat_equot_enum);
  lightNNdouble(const lightNNdouble&, double f(double), const lightmat_apply_enum);
  lightNNdouble(const lightNNdouble&, const lightNNdouble&, double f(double, double), const lightmat_apply_enum);
  lightNNdouble(const lightN3double&, const lightmat_trans_enum);
  lightNNdouble(const lightNdouble&, const lightNdouble&, const lightmat_outer_enum);

};

typedef lightNNdouble doubleNN;
typedef lightNNint intNN;
typedef lightNNlm_complex lm_complexNN;

#undef IN_LIGHTNNdouble_H


#endif
