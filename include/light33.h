//           -*- c++ -*-

#ifndef LIGHT33_H
#define LIGHT33_H
// <cd> light33
//
// .SS Functionality
//
// light33 is a template for classes that implement matrices with 3x3
// elements. E.g. a matrix v with 3x3 elements of type double can be
// instanciated with:
//
// <code>light33&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHT33<T>_H

template<class T> class light3;
template<class T> class lightN;
template<class T> class lightN3;
template<class T> class lightN33;
template<class T> class lightNN;
template<class T> class lightNNN;
template<class T> class lightNNNN;

template<class T>
class light33 {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light33<double>();
  // Convert to double.
#else
   friend class  lightN33<int>;
#endif

#ifdef IN_LIGHT33<double>_H
   friend class  light33<int>;
#else
   friend class  light33<double>;
#endif

  #include "light33_auto.h"

  inline light33();
  // Default constructor.

  inline light33(const light33<T>&); 
  // Copy constructor.

  inline  light33(const T e11, const T e12, const T e13, const T e21, const T e22, const T e23, const T e31, const T e32, const T e33);
  // Initialize elements with values.

  inline light33(const T *);
  // Initialize elements with values from an array (in row major order).

  inline light33(const T);
  // Initialize all elements with the same value.

  light33<T>& operator=(const light33<T>&);
    // Assignment.

  light33<T>& operator=(const lightN3<T>&);
  // Assignment from a lightN3 where N=3.

  light33<T>& operator=(const lightNN<T>&);
  // Assignment from a lightNN where NN=33.

  light33<T>& operator=(const T);
  // Assign one value to all elements.

  T operator()(const int x, const int y) const {
    limiterror((x<1) || (x>3) || (y<1) || (y>3));

#ifdef ROWMAJOR
    return elem[y-1+(x-1)*3];
#else
    return elem[x-1+(y-1)*3];
#endif
  };
  // Get the value of one element.


  T& operator()(const int x, const int y) {
    limiterror((x<1) || (x>3) || (y<1) || (y>3));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*3];
#else
    return elem[x-1+(y-1)*3];
#endif
  };
  // Get/Set the value of one element.

  light3<T> operator()(const int) const;
  // Get the value of one row.


  //
  // Set
  //

  inline light33<T>& Set (const int i0, const T val);
  inline light33<T>& Set (const int i0, const lightN<T>& arr);
  inline light33<T>& Set (const int i0, const light3<T>& arr);
  inline light33<T>& Set (const int i0, const light4<T>& arr);


  int operator==(const light33<T>&) const;
  // Equality.

  int operator!=(const light33<T>&) const;
  // Inequality.

  light33<T>& operator+=(const T);
  // Add a value to all elements.

  light33<T>& operator+=(const light33<T>&);
  // Elementwise addition.

  light33<T>& operator-=(const T);
  // Subtract a value from all elements.

  light33<T>& operator-=(const light33<T>&);
  // Elementwise subtraction.

  light33<T>& operator*=(const T);
  // Muliply all elements with a value.

  light33<T>& operator/=(const T);
  // Divide all elements with a value.

  light33<T>& reshape(const int, const int, const light3<T>&);
  // Convert the light3-vector to the given size and put the result in
  // this object. All three vector-columns of the light33 object will
  // get the value of the light3-argument. The first two arguments
  // (the size of the matrix) must both have the value 3 since it is a
  // light33 object.

  int dimension(const int x) const {
    if((x==1) || (x==2))
      return 3;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 3 and dimension(2) == 3).
 
  void Get(T *) const;
  // Get values of all elements and put them in an array (9 elements long,
  // row major order).

  void Set(const T *);
  // Set values of all elements from array (9 elements long, row major
  // order).

  void Set(const T e11, const T e12, const T e13,
	   const T e21, const T e22, const T e23,
	   const T e31, const T e32, const T e33);
  // Set the values of all the elements.

  void Get( T& e11, T& e12, T& e13,
	    T& e21, T& e22, T& e23,
	    T& e31, T& e32, T& e33) const;
  // Get the values of all the elements.

  light33<T> operator+() const;
  // Unary plus.

  light33<T> operator-() const;
  // Unary minus.

  friend inline light33<T> operator+(const light33<T>&, const light33<T>&);
  // Elementwise addition.

  friend inline light33<T> operator+(const light33<T>&, const T);
  // Addition to all elements.

  friend inline light33<T> operator+(const T, const light33<T>&);
  // Addition to all elements.

  friend inline light33<T> operator-(const light33<T>&, const light33<T>&);
  // Elementwise subtraction.

  friend inline light33<T> operator-(const light33<T>&, const T);
  // Subtraction from all elements.

  friend inline light33<T> operator-(const T, const light33<T>&);
  // Subtraction to all elements.

  friend inline light33<T> operator*(const light33<T>&, const light33<T>&);
  // Inner product.

  friend inline light33<T> Dot(const light33<T>&, const light33<T>&);
  // Inner product.

  friend inline light33<T> operator*(const light33<T>&, const T);
  // Multiply all elements.

  friend inline light33<T> operator*(const T, const light33<T>&);
  // Multiply all elements.

  friend inline light3<T> operator*(const light33<T>&, const light3<T>&);
  // Inner product.

  friend inline light3<T> operator*(const light3<T>&, const light33<T>&);
  // Inner product.

  friend inline light33<T> operator/(const light33<T>&, const T);
  // Divide all elements.

  friend inline light33<T> operator/(const T, const light33<T>&);
  // Divide with all elements.

  friend inline light33<T> pow(const light33<T>&, const light33<T>&);
  // Raise to the power of-function, elementwise.

  friend inline light33<T> pow(const light33<T>&, const T);
  // Raise to the power of-function, for all elements.

  friend inline light33<T> pow(const T, const light33<T>&);
  // Raise to the power of-function, for all elements.

  friend inline light33<T> ElemProduct(const light33<T>&, const light33<T>&);
  // Elementwise multiplication.

  friend inline light33<T> ElemQuotient(const light33<T>&, const light33<T>&);
  // Elementwise division.

  friend inline light33<T> Apply(const light33<T>&, T f(T));
  // Apply the function elementwise on all elements.

  friend inline light33<T> Apply(const light33<T>&, const light33<T>& s, T f(T, T));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline light33<T> Transpose(const light33<T>&);
  // Transpose matrix.

  friend inline light33<T> abs(const light33<T>&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light33<int> sign(const light33<T>&);
  // sign
#else
  friend inline light33lm_complex sign(const light33lm_complex&);
#endif
  friend inline light33<int> ifloor(const light33<double>&);
  // ifloor

  friend inline light33<int> iceil(const light33<double>&);
  // iceil

  friend inline light33<int> irint(const light33<double>&);
  // irint

  friend inline light33<double> sqrt(const light33<double>&);
  // sqrt

  friend inline light33<double> exp(const light33<double>&);
  // exp

  friend inline light33<double> log(const light33<double>&);
  // log

  friend inline light33<double> sin(const light33<double>&);
  // sin

  friend inline light33<double> cos(const light33<double>&);
  // cos

  friend inline light33<double> tan(const light33<double>&);
  // tan

  friend inline light33<double> asin(const light33<double>&);
  // asin

  friend inline light33<double> acos(const light33<double>&);
  // acos

  friend inline light33<double> atan(const light33<double>&);
  // atan

  friend inline light33<double> sinh(const light33<double>&);
  // sinh

  friend inline light33<double> cosh(const light33<double>&);
  // cosh

  friend inline light33<double> tanh(const light33<double>&);
  // tanh

  friend inline light33<double> asinh(const light33<double>&);
  // asinh

  friend inline light33<double> acosh(const light33<double>&);
  // acosh

  friend inline light33<double> atanh(const light33<double>&);
  // atanh

  friend inline light33<lm_complex> ifloor(const light33<lm_complex>&);
  // ifloor

  friend inline light33<lm_complex> iceil(const light33<lm_complex>&);
  // iceil

  friend inline light33<lm_complex> irint(const light33<lm_complex>&);
  // irint

  friend inline light33<lm_complex> sqrt(const light33<lm_complex>&);
  // sqrt

  friend inline light33<lm_complex> exp(const light33<lm_complex>&);
  // exp

  friend inline light33<lm_complex> log(const light33<lm_complex>&);
  // log

  friend inline light33<lm_complex> sin(const light33<lm_complex>&);
  // sin

  friend inline light33<lm_complex> cos(const light33<lm_complex>&);
  // cos

  friend inline light33<lm_complex> tan(const light33<lm_complex>&);
  // tan

  friend inline light33<lm_complex> asin(const light33<lm_complex>&);
  // asin

  friend inline light33<lm_complex> acos(const light33<lm_complex>&);
  // acos

  friend inline light33<lm_complex> atan(const light33<lm_complex>&);
  // atan

  friend inline light33<lm_complex> sinh(const light33<lm_complex>&);
  // sinh

  friend inline light33<lm_complex> cosh(const light33<lm_complex>&);
  // cosh

  friend inline light33<lm_complex> tanh(const light33<lm_complex>&);
  // tanh

  friend inline light33<lm_complex> asinh(const light33<lm_complex>&);
  // asinh

  friend inline light33<lm_complex> acosh(const light33<lm_complex>&);
  // acosh

  friend inline light33<lm_complex> atanh(const light33<lm_complex>&);
  // atanh



  //<ignore>
//    friend class light33<int>;
  friend class light3<T>;
  friend class lightN3<T>;
  friend class lightNN<T>;
  friend class lightN33<T>;
  friend class lightNNN<T>;
  friend class lightNNNN<T>;


  //    friend ostream & operator<<( ostream & ost,const  light33<double> & v);
  //</ignore>

protected:
  T elem[9];
  // The values of the nine elements.

  light33(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.

};



typedef light33<double> double33;
typedef light33<int> int33;

//*********************************
typedef light33<double> doubleMat33;
extern  const  doubleMat33   zeroMat33;
//********************************

#undef IN_LIGHT33<T>_H

#endif
