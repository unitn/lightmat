//           -*- c++ -*-

extern "C" {
#include  "mathlink.h"
}
#include <assert.h>
#include "lightmat.h"
#include <stdio.h>
#include <strstream>
#if (defined (_MSC_VER) || defined  (__MINGW32__))
#include <float.h>
#endif

#if defined (_MSC_VER)
#include <tchar.h> 
#endif
using std::strstream;

/* This interface code is used for inclusion into
plotif.cc (or similar) code.

It provides interface between MathLink types and lightMath types
*/

/*
lmif_return(...)
should finish an interface  routine and takes the output
of a generated C++ function as the input parameter */


/* This is somewhat of a hack to fix the isnan function */
#ifdef macintosh
int isnan(double d)
{
    const unsigned char double_nan[8] = { 0x7F, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF };
    return d == *((double *)&(double_nan[0]));
}
#endif

/* In mathlink.h several functions changed their argument type when going over to MLINTERFACE==3  */
#if MLINTERFACE >=3
    #define int3long int 
#else
    #define int3long long 
#endif

#if MLINTERFACE >=4
#define MLDisownByteString MLReleaseByteString
#define MLDisownSymbol MLReleaseSymbol
#define MLDisownString MLReleaseString

#define MLGetIntegerList MLGetInteger32List
#define MLDisownIntegerList MLReleaseInteger32List

#define MLGetIntegerArray MLGetInteger32Array
#define MLDisownIntegerArray  MLReleaseInteger32Array
#define MLPutIntegerArray MLPutInteger32Array
#define MLGetRealList MLGetReal64List
#define MLDisownRealList MLReleaseReal64List
#define MLPutRealArray MLPutReal64Array

#define MLGetRealArray MLGetReal64Array
#define MLDisownRealArray MLReleaseReal64Array

#endif

#if MLINTERFACE >=4
#define DIMSTYPE int
#else
#define DIMSTYPE long
#endif

void safe_double(double * arr, int length)
{
    for(int i=0;i<length;i++)
    {
#if (defined (_MSC_VER) || defined  (__MINGW32__))

        if (_isnan(arr[i]))  arr[i]=0.9e99;
#else
        if (isnan(arr[i]))  arr[i]=0.9e99;
#endif
        if ((arr[i]>1e100)||(arr[i]<-1e100)) arr[i]=0.9e99;
        else 
            if(arr[i]!=0)
                if ((arr[i]<1e-100)&&(arr[i]>-1e-100)) arr[i]=0.9e-99;
    }
}


void lmif_return(const lm_complex  &  __result);

#ifdef LM_NN
void   lmif_return(const lightNNdouble & __result) 
{
    double *a;
    DIMSTYPE  *dims;
    long  d;

    d=2;
    dims=new DIMSTYPE[d];
    dims[0]=__result.dimension(1);
    dims[1]=__result.dimension(2);
    a=new double[dims[0]*dims[1]];
    __result.Get(a);
    safe_double(a,dims[0]*dims[1]);
    MLPutRealArray(stdlink,a,dims,NULL,d);
    delete [] dims;
    delete [] a;
}

void   lmif_return(const lightNNint & __result) 
{
    int   *a;
    DIMSTYPE  *dims;
    long  d;

    d=2;
    dims=new DIMSTYPE[d];    
    dims[0]=__result.dimension(1);
    dims[1]=__result.dimension(2);
    a=new int[dims[0]*dims[1]];
    __result.Get(a);
    MLPutIntegerArray(stdlink,a,dims,NULL,d);
    delete [] dims;
    delete [] a;
}

void   lmif_return(const lightNNlm_complex & __result) 
{
    MLPutFunction(stdlink,"List",__result.dimension(1));
    for (int j1=1;j1<=__result.dimension(1);j1++) {
        MLPutFunction(stdlink,"List",__result.dimension(2));
        for (int j2=1;j2<=__result.dimension(2);j2++) {
            const lm_complex  & tmp=__result(j1,j2);
            lmif_return(tmp);
        }
    }
}


#endif

#ifdef LM_NNN
void   lmif_return(const lightNNNdouble & __result) 
{
    double *a;
    DIMSTYPE  *dims;
    long  d;

    d=3;
    dims=new DIMSTYPE[d];
    dims[0]=__result.dimension(1);
    dims[1]=__result.dimension(2);
    dims[2]=__result.dimension(3);
    a=new double[dims[0]*dims[1]*dims[2]];
    __result.Get(a); 
    safe_double(a,dims[0]*dims[1]*dims[2]);
    MLPutRealArray(stdlink,a,dims,NULL,d);
    delete [] dims;
    delete [] a;
}
void   lmif_return(const lightNNNint & __result) 
{
    int   *a;
    DIMSTYPE  *dims;
    long  d;

    d=3;
    dims=new DIMSTYPE[d];
    dims[0]=__result.dimension(1);
    dims[1]=__result.dimension(2);
    dims[2]=__result.dimension(3);
    a=new int[dims[0]*dims[1]*dims[2]];
    __result.Get(a);
    MLPutIntegerArray(stdlink,a,dims,NULL,d);
    delete [] dims;
    delete [] a;
}

void   lmif_return(const lightNNNlm_complex & __result) 
{
    MLPutFunction(stdlink,"List",__result.dimension(1));
    for (int j1=1;j1<=__result.dimension(1);j1++) {
        MLPutFunction(stdlink,"List",__result.dimension(2));
        for (int j2=1;j2<=__result.dimension(2);j2++) {
            MLPutFunction(stdlink,"List",__result.dimension(3));
            for (int j3=1;j3<=__result.dimension(3);j3++) {
                const lm_complex  & tmp=__result(j1,j2,j3);
                lmif_return(tmp);
            }
        }
    }
}



#endif

#ifdef LM_NNNN
void   lmif_return(const lightNNNNdouble & __result) 
{
    double *a;
    DIMSTYPE  *dims;
    long  d;

    d=4;
    dims=new DIMSTYPE[d];
    dims[0]=__result.dimension(1);
    dims[1]=__result.dimension(2);
    dims[2]=__result.dimension(3);
    dims[3]=__result.dimension(4);
    a=new double[dims[0]*dims[1]*dims[2]*dims[3]];
    __result.Get(a);
    safe_double(a,dims[0]*dims[1]*dims[2]*dims[3]);
    MLPutRealArray(stdlink,a,dims,NULL,d);
    delete [] dims;
    delete [] a;
}

void   lmif_return(const lightNNNNint & __result) 
{
    int *a;
    DIMSTYPE  *dims;
    long  d;

    d=4;
    dims=new DIMSTYPE[d];
    dims[0]=__result.dimension(1);
    dims[1]=__result.dimension(2);
    dims[2]=__result.dimension(3);
    dims[3]=__result.dimension(4);
    a=new  int[dims[0]*dims[1]*dims[2]*dims[3]];
    __result.Get(a);
    MLPutIntegerArray(stdlink,a,dims,NULL,d);
    delete [] dims;
    delete [] a;
}

void   lmif_return(const lightNNNNlm_complex & __result) 
{
    MLPutFunction(stdlink,"List",__result.dimension(1));
    for (int j1=1;j1<=__result.dimension(1);j1++) {
        MLPutFunction(stdlink,"List",__result.dimension(2));
        for (int j2=1;j2<=__result.dimension(2);j2++) {
            MLPutFunction(stdlink,"List",__result.dimension(3));
            for (int j3=1;j3<=__result.dimension(3);j3++) {
                MLPutFunction(stdlink,"List",__result.dimension(4));
                for (int j4=1;j4<=__result.dimension(4);j4++) {
                    const lm_complex  & tmp=__result(j1,j2,j3,j4);
                    lmif_return(tmp);
                }
            }
        }
    }
}



#endif

#ifdef LM_N
void   lmif_return(const lightNdouble & __result) 
{
    double *a;
    a=new double[__result.dimension(1)];
    __result.Get(a);
    safe_double(a,__result.dimension(1));
    MLPutRealList(stdlink,a,__result.dimension(1));
    delete [] a;
}

void   lmif_return(const lightNint & __result) 
{
    int *a;
    a=new int[__result.dimension(1)];
    __result.Get(a);
    MLPutIntegerList(stdlink,a,__result.dimension(1));
    delete [] a;
}


void   lmif_return(const lightNlm_complex & __result) 
{
    MLPutFunction(stdlink,"List",__result.dimension(1));
    for (int j=1;j<=__result.dimension(1);j++) {
        const lm_complex  & tmp=__result(j);
        lmif_return(tmp);
    }
}

#endif

void lmif_return(const int &  __result)
{
    MLPutInteger(stdlink,  __result );  
}

void lmif_return(const double  &  __result)
{
    MLPutReal(stdlink,  __result );  
}

void lmif_return(const lm_complex  &  __result)
{
    lm_complex c;
    c=__result;
    safe_double(&(c.re),1); 
    safe_double(&(c.im),1); 
    MLPutFunction(stdlink,"Complex",2);
    MLPutReal(stdlink,c.re );  
    MLPutReal(stdlink,c.im );  
}


#ifdef ANSI_STRING
void lmif_return(const string  &  __result)
{
    MLPutByteString(stdlink, (unsigned char *) __result.data(), __result.length());  
}
#else
void lmif_return(const RWCString  &  __result)
{
    MLPutByteString(stdlink, (unsigned char *) __result.getdata(), __result.getlength());  
}
#endif

/* In case when nothing is returned - we return Null */ 
void lmif_return()
{
    MLPutSymbol(stdlink, "Null");  
}





#define get_lightNdouble(x) lightNdouble x;cb_get(x);
#define get_lightNNdouble(x) lightNNdouble x;cb_get(x);
#define get_lightNNNdouble(x) lightNNNdouble x;cb_get(x);
#define get_lightNNNNdouble(x) lightNNNNdouble x;cb_get(x);

#define get_lightNint(x) lightNint x;cb_get(x);
#define get_lightNNint(x) lightNNint x;cb_get(x);
#define get_lightNNNint(x) lightNNNint x;cb_get(x);
#define get_lightNNNNint(x) lightNNNNint x;cb_get(x);

#define get_complex(x)       lm_complex x;cb_get(x);
#define get_lightNcomplex(x) lightNlm_complex x;cb_get(x);
#define get_lightNNcomplex(x) lightNNlm_complex x;cb_get(x);
#define get_lightNNNcomplex(x) lightNNNlm_complex x;cb_get(x);
#define get_lightNNNNcomplex(x) lightNNNNlm_complex x;cb_get(x);
#define get_double(x) double x;cb_get(x);
#define get_int(x)    int x;cb_get(x);






// Z will mean bad character
#ifdef ANSI_STRING
#define get_string(x) \
    string x;\
{const unsigned char * x1;\
    int3long  xlen;\
    MLGetByteString(stdlink,&x1,&xlen,(long)'Z');\
    x=string((char *)x1);\
    MLDisownByteString(stdlink,x1,xlen);\
}
#else
#define get_string(x) \
    RWCString x;\
{const unsigned char * x1;\
    int3long xlen;\
    MLGetByteString(stdlink,&x1,&xlen,(long)'Z');\
    x.set(x1,xlen);\
    MLDisownByteString(stdlink,x1,xlen);\
}
#endif


double *if__a;int *if__ai;
DIMSTYPE *dims;
char ** heads;
#if MLINTERFACE >=4
int   if__d;
#else
long  if__d;
#endif

int   if__next;
char  *if__current_function=0;



// This fragment is used for INTERACTIVE variant (xxxml.exe) compilation.
//
#ifndef CALLBACK_BATCH
#if 0  /* WINDOWS_MATHLINK */

int PASCAL WinMain( HANDLE hinstCurrent, HANDLE hinstPrevious, LPSTR lpszCmdLine, int nCmdShow)
{
    char  buff[512];
    char FAR * buff_start = buff;
    char FAR * argv[32];
    char FAR * FAR * argv_end = argv + 32;

    if( !MLInitializeIcon( hinstCurrent, nCmdShow)) return 1;
    MLScanString( argv, &argv_end, &lpszCmdLine, &buff_start);
    return MLMain( argv_end - argv, argv);
}



#else
int main(int argc, char* argv[])
{
    return MLMain(argc, argv);
}
#endif
#endif


// This fragment is used for CALLBACK_BATCH (xxxcb.exe ) compilation.
//

MLENV ep = (MLENV)0;

extern "C" {

#if defined(MINGW)&&(__GNUC__ < 4)&&!defined(__CYGWIN__) /* only platform with the problem that I know of */
    static void deinit(...)
#else
    static void deinit( void)
#endif
    {
        if( ep) MLDeinitialize( ep);
    }

#if defined(MINGW)&&(__GNUC__ < 4)&&!defined(__CYGWIN__) /* old mingw is only platform with the problem that I know of */
    static void closelink(...)
#else
    static void closelink( void)
#endif

    {
        if( stdlink) MLClose( stdlink);
    }
}

static void init_and_openlink( int argc, char* argv[])
{
  int3long  err;
  ep =  MLInitialize( (MLParametersPointer)0);
    if( ep 
        == 
        (MLENV)0) 
        exit(1);
    atexit( deinit);
    stdlink = MLOpenArgv( ep, argv, argv + argc, &err);
    if(
       stdlink 
         ==
          (MLINK)
          0
          ) 
          exit(2)
          ;
    atexit( closelink);
}

///////////////////////////////////////////////////////
// This fragment is for all CALLBACK USES
///////////////////////////////////////////////////////
static int read_and_print_expression( int next);
void ML_IF_error();
void ML_IF_wait ();

void ML_IF_wait ()
{
    int pkt;
    MLEndPacket( stdlink);
    while( (pkt = MLNextPacket( stdlink), pkt) && pkt != RETURNPKT) {
        MLNewPacket( stdlink);
        if( MLError( stdlink)) ML_IF_error();
    }

}


static int read_and_print_expression(strstream & st, int next)
{
    kcharp_ct  s;
    int n;
    long i;
    int3long len;
    double r;
    static int indent;

    switch(next) {
        case MLTKSYM:
            MLGetSymbol( stdlink, &s);
            st<<" SYMBOL= "<<s<<endl; 
            MLDisownSymbol( stdlink, s);
            break;
        case MLTKSTR:
            MLGetString( stdlink, &s);
            st <<" STRING="<<"\""<<s<<"\""<<endl;
            MLDisownString( stdlink, s);
            break;
        case MLTKINT:
            MLGetInteger( stdlink, &n);
            st<<" INTEGER= "<<n<<endl;
            return n; 
            break;
        case MLTKREAL:

            MLGetReal( stdlink, &r);
            st<<" REAL= "<<r<<endl;
            break;
        case MLTKFUNC: {
            st<<" FUNC= "<<endl;
            indent += 3;
            st << endl;
            for (int j=1;j<=indent;j++) st <<" "; 
            if( MLGetArgCount( stdlink, &len) == 0){
                ML_IF_error();
            }else{
                int next2 = MLGetNext( stdlink);
                read_and_print_expression(st,next2);
                st<<"[";
                for( i = 1; i <= len; ++i){
                    next2=MLGetNext( stdlink);
                    read_and_print_expression(st,next2);
                    if( i != len) st<<", ";
                }
                st<< "]";
            }
            indent -= 3;
            break;}
        case MLTKERROR:
            st<<" ERROR "<<endl;
        default:
            st<<" UNKNOWN Mma TYPE="<<next<<endl;
            ML_IF_error();
    }
    return -1;
}

void ML_IF_error()
{
    if( MLError( stdlink)) {
        fprintf( stderr,
            "Error detected by Mathlink: %s in call to %s .\n",
            MLErrorMessage( stdlink),
            if__current_function );
    } else {
        fprintf( stderr, "Error detected by this program. in call to %s \n",
            if__current_function);
    }
    // exit( 1); // It is matter of taste probably...
}


void ML_IF_start(const char * fname,int argc){
    // This function starts attempt to call a callback function.
    // We also store the name of the callbeck function for future
    // error messages.
    /* Send EvaluatePacket[ FactorInteger[n]]. */
    MLPutFunction( stdlink, "EvaluatePacket", 1L);
    MLPutFunction( stdlink, fname , argc);
    
    if (if__current_function) delete [] if__current_function;
    
    if__current_function=new char[strlen(fname)+1];
    strncpy(if__current_function,fname,strlen(fname)+1);

};

void ML_Expected(const char typetext[],bool received=true){

    strstream st;
    if (if__current_function) 
        st<<"Returning from callback function "<<if__current_function<<endl;
    else
        st<<"In call to compiled function" << endl;

    if (received) {
        st<<"  Expected "<<typetext<<" received:\n";
        read_and_print_expression(st,if__next);
    }
    else
        st << typetext << endl;

    st << "***"<<endl;
    st.freeze();
    printf("%s",st.str());
    char * buf=new char[st.pcount()+1];
    strncpy(buf,st.str(),st.pcount());

    printf("%s",buf);


#ifdef _MSC_VER
    MessageBox(0,_T(buf),_T("Invalid arguments to compiled function."),MB_OK);
    DebugBreak();
#endif
    delete [] buf;
} 



void cb_get(){ 
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKSYM) {
        ML_Expected("Null");
    } else {
        kcharp_ct  s;
        MLGetSymbol( stdlink, &s);
        if (MLError(stdlink)) { 
            ML_IF_error(); MLClearError(stdlink); }
        MLDisownSymbol( stdlink, s);}
}

#ifdef ANSI_STRING
void  cb_get(string & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKSTR) {
        ML_Expected("String");
        x="-998";
    } else {
        const  unsigned char * x1;
        int3long xlen;
        MLGetByteString(stdlink,&x1,&xlen,(long)'Z');
        if (MLError(stdlink)) { 
            ML_IF_error();
            x="-997";
            MLClearError(stdlink); }
        x=string((char *)x1);
        MLDisownByteString(stdlink,x1,xlen);
    }
}
#else
void  cb_get(RWCString & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKSTR) {
        ML_Expected("String");
        x="-998";
    } else {
        const  unsigned char * x1;
        int3long xlen;
        MLGetByteString(stdlink,&x1,&xlen,(long)'Z');
        if (MLError(stdlink)) { 
            ML_IF_error();
            x="-997";
            MLClearError(stdlink); }
        x.set(x1,xlen);
        MLDisownByteString(stdlink,x1,xlen);
    }
}
#endif

void  cb_get(int & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKINT) {
        ML_Expected("Integer");
        x=-998;
    } else { 
        MLGetInteger(stdlink,&x);
        if (MLError(stdlink)) { 
            ML_IF_error();
            x=-997;
            MLClearError(stdlink);
        }
    }
}

void cb_get(lightNint & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("IntegerList");
        x.SetShape(1);x(1)=-998;
    } else { 
        MLGetIntegerList(stdlink,&if__ai,&if__d);
        if (MLError(stdlink)) { 
            ML_IF_error();
            x.SetShape(1);x(1)=-997;
            MLClearError(stdlink);}
        x.SetShape(if__d);
        x.Set(if__ai);
        MLDisownIntegerList(stdlink,if__ai,if__d);};
}

void cb_get(lightNNint & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("IntegerMatrix");
        x.SetShape(1,1);x(1,1)=-998;
    } else { 
        MLGetIntegerArray(stdlink,&if__ai,&dims,&heads,&if__d);
        if (MLError(stdlink)||(if__d!=2)) { 
            ML_IF_error();
            x.SetShape(1,1);x(1,1)=-997;
            MLClearError(stdlink);}
        x.SetShape(dims[0],dims[1] );
        x.Set(if__ai);
        MLDisownIntegerArray(stdlink,if__ai,dims,heads,if__d);
    }
} 

void cb_get(lightNNNint & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("IntegerTensor of rank 3");
        x.SetShape(1,1,1);x(1,1,1)=-998;
    } else { 
        MLGetIntegerArray(stdlink,&if__ai,&dims,&heads,&if__d);
        if (MLError(stdlink)||(if__d!=3) ) { 
            ML_IF_error();
            x.SetShape(1,1,1);x(1,1,1)=-997;
            MLClearError(stdlink);}
        x.SetShape(dims[0],dims[1],dims[2]  );
        x.Set(if__ai);
        MLDisownIntegerArray(stdlink,if__ai,dims,heads,if__d);
    };
}

void cb_get(lightNNNNint & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("IntegerTensor of rank 4");
        x.SetShape(1,1,1,1);x(1,1,1,1)=-998;
    } else { 
        MLGetIntegerArray(stdlink,&if__ai,&dims,&heads,&if__d);
        if (MLError(stdlink)||(if__d!=4) ) { 
            ML_IF_error();
            x.SetShape(1,1,1,1);x(1,1,1,1)=-997;
            MLClearError(stdlink);}
        x.SetShape(dims[0],dims[1],dims[2],dims[3]  );
        x.Set(if__ai);
        MLDisownIntegerArray(stdlink,if__ai,dims,heads,if__d);
    };
}




void  cb_get(double & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKREAL) {
        ML_Expected("Real");
        x=-998;
    } else { 
        MLGetReal(stdlink,&x);
        if (MLError(stdlink)) { 
            ML_IF_error();
            x=-997;
            MLClearError(stdlink);
        }
    }
}

void cb_get(lightNdouble & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("RealList");
        x.SetShape(1);x(1)=-998;
    } else { 
        MLGetRealList(stdlink,&if__a,&if__d);
        if (MLError(stdlink)) { 
            ML_IF_error();
            x.SetShape(1);x(1)=-997;
            MLClearError(stdlink);}
        x.SetShape(if__d);
        x.Set(if__a);
        MLDisownRealList(stdlink,if__a,if__d);};
}

void cb_get(lightNNdouble & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("RealMatrix");
        x.SetShape(1,1);x(1,1)=-998;
    } else { 
        MLGetRealArray(stdlink,&if__a,&dims,&heads,&if__d);
        if (MLError(stdlink)||(if__d!=2)) { 
            ML_IF_error();
            x.SetShape(1,1);x(1,1)=-997;
            MLClearError(stdlink);}
        x.SetShape(dims[0],dims[1] );
        x.Set(if__a);
        MLDisownRealArray(stdlink,if__a,dims,heads,if__d);
    }
} 

void cb_get(lightNNNdouble & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("RealTensor of rank 3");
        x.SetShape(1,1,1);x(1,1,1)=-998;
    } else { 
        MLGetRealArray(stdlink,&if__a,&dims,&heads,&if__d);
        if (MLError(stdlink)||(if__d!=3) ) { 
            ML_IF_error();
            x.SetShape(1,1,1);x(1,1,1)=-997;
            MLClearError(stdlink);}
        x.SetShape(dims[0],dims[1],dims[2]  );
        x.Set(if__a);
        MLDisownRealArray(stdlink,if__a,dims,heads,if__d);
    };
}

void cb_get(lightNNNNdouble & x) {
    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("RealTensor of rank 4");
        x.SetShape(1,1,1,1);x(1,1,1,1)=-998;
    } else { 
        MLGetRealArray(stdlink,&if__a,&dims,&heads,&if__d);
        if (MLError(stdlink)||(if__d!=4) ) { 
            ML_IF_error();
            x.SetShape(1,1,1,1);x(1,1,1,1)=-997;
            MLClearError(stdlink);}
        x.SetShape(dims[0],dims[1],dims[2],dims[3]  );
        x.Set(if__a);
        MLDisownRealArray(stdlink,if__a,dims,heads,if__d);
    };
}

void cb_get(lm_complex & x) {
    if__next=MLGetNext( stdlink);
    
    if (if__next==MLTKREAL) {
    	// For complex numbers that contain no im-part, with double re-part
    	double y;
        MLGetReal(stdlink,&y);
        if (MLError(stdlink)) { 
            ML_IF_error();
            y=-997;
            MLClearError(stdlink);
        }
        x.re=y;x.im=0;
    	return;
    	
    }
    
     if (if__next==MLTKINT) {
    	// For complex numbers that contain no im-part, with integer re-part
      	int y;
        MLGetInteger(stdlink,&y);
        if (MLError(stdlink)) { 
            ML_IF_error();
            y=-997;
            MLClearError(stdlink);
        }
        x.re=y;x.im=0;
    	return;
    	
    }
    
    
    if (if__next!=MLTKFUNC) {
        ML_Expected("A Complex number");
        x.re=-995;x.im=-996;
        return; 
    }
    long xtwo;
    int flag=MLCheckFunction(stdlink,"Complex",&xtwo);
    if (!flag) {
        ML_Expected("Complex[] data expected",false);
        x.re=-998;x.im=-997;
        return; 
    }
    if (xtwo!=2) {
        ML_Expected("Complex[] node with exactly two arguments",false);
        x.re=-998;x.im=-997;
        return;    
    }
    MLGetReal(stdlink,&x.re);
    MLGetReal(stdlink,&x.im);

}


void cb_get(lightNlm_complex & x){

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("A List of Complex");
        x.SetShape(1);
        x(1).re=-998;x(1).im=-997;
        return; 
    }
    long length1;
    int flag=MLCheckFunction(stdlink,"List",&length1);
    if (!flag) {
        ML_Expected("List[]  expected",false);
        x.SetShape(1);
        x(1).re=-998;x(1).im=-997;
        return; 
    }
    x.SetShape(length1);
    for (int i1=1;i1<=length1;i1++) {
        lm_complex y;
        cb_get(y);
        x(i1)=y;
    }


};
void cb_get(lightNNlm_complex & x){
    /*

    How lengths are evaluated.

    List[
    List[Complex,Complex]
    List[Complex,Complex,Complex,Complex]
    List[Complex,Complex]
    ]

    length1=3
    length2=2
    length2A= 4 (error!) , then 2

    List[
    List[ (*) Complex,Complex]
    List[Complex,Complex,Complex,Complex]
    List[Complex,Complex]
    ]

    (*) = here start the loops

    */

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("A List of Complex");
        x.SetShape(1,1);
        x(1,1).re=-998;x(1,1).im=-997;
        return; 
    }
    long length1;
    int flag1=MLCheckFunction(stdlink,"List",&length1);
    if (!flag1) {
        ML_Expected("List[]  expected",false);
        x.SetShape(1,1);
        x(1,1).re=-998;x(1,1).im=-997;
        return; 
    }

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("List[funcname[...]] expected ");
        x.SetShape(1,1);
        x(1,1).re=-998;x(1,1).im=-997;
        return; 
    }

    long length2;
    int flag2=MLCheckFunction(stdlink,"List",&length2);
    if (!flag2) {
        ML_Expected("List[List[]]  expected",false);
        x.SetShape(1,1);
        x(1,1).re=-998;x(1,1).im=-997;
        return; 
    }
    x.SetShape(length1,length2);
    for (int i1=1;i1<=length1;i1++) {
        if (i1>1) {
            long length2A;
            int flag2A=MLCheckFunction(stdlink,"List",&length2A);
            if (!flag2A) {
                ML_Expected("List[List[],...,List[*]]  expected",false);
                x.SetShape(1,1);
                x(1,1).re=-998;x(1,1).im=-997;
                return; 
            }
            if (length2!=length2A) {
                ML_Expected("Equal length sublists in List[List[],...,List[*]]  expected",false);
                x.SetShape(1,1);
                x(1,1).re=-998;x(1,1).im=-997;
                return; 
            }

        }
        for (int i2=1;i2<=length2;i2++) {

            lm_complex y;
            cb_get(y);
            x(i1,i2)=y;
        }
    }

};


void cb_get(lightNNNlm_complex & x){
    /*

    How lengths are evaluated.
    List[
    List[
    List[(*)Complex,Complex]
    List[Complex,Complex,Complex,Complex]
    List[Complex,Complex]
    ]
    List[
    List[Complex,Complex]
    List[Complex,Complex,Complex,Complex]
    List[Complex,Complex]
    ]
    ]

    length1=2
    length2=3
    length2A=3
    length3=2
    length3A= 4 (error!) , then 2




    */

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("A List of Complex");
        x.SetShape(1,1,1);
        x(1,1,1).re=-998;x(1,1,1).im=-997;
        return; 
    }
    long length1;
    int flag1=MLCheckFunction(stdlink,"List",&length1);
    if (!flag1) {
        ML_Expected("List[]  expected",false);
        x.SetShape(1,1,1);
        x(1,1,1).re=-998;x(1,1,1).im=-997;
        return; 
    }

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("List[funcname[...]] expected ");
        x.SetShape(1,1,1);
        x(1,1,1).re=-998;x(1,1,1).im=-997;
        return; 
    }

    long length2;
    int flag2=MLCheckFunction(stdlink,"List",&length2);
    if (!flag2) {
        ML_Expected("List[List[]]  expected",false);
        x.SetShape(1,1,1);
        x(1,1,1).re=-998;x(1,1,1).im=-997;
        return; 
    }

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("List[List[funcname[...]]] expected ");
        x.SetShape(1,1,1);
        x(1,1,1).re=-998;x(1,1,1).im=-997;
        return; 
    }

    long length3;
    int flag3=MLCheckFunction(stdlink,"List",&length3);
    if (!flag3) {
        ML_Expected("List[List[List[..]]]  expected",false);
        x.SetShape(1,1,1);
        x(1,1,1).re=-998;x(1,1,1).im=-997;
        return; 
    }



    x.SetShape(length1,length2,length3);
    for (int i1=1;i1<=length1;i1++) {
        if (i1>1) {
            long length2A;
            int flag2A=MLCheckFunction(stdlink,"List",&length2A);
            if (!flag2A) {
                ML_Expected("List[List[],...,List[*]]  expected",false);
                x.SetShape(1,1,1);
                x(1,1,1).re=-998;x(1,1,1).im=-997;
                return; 
            }
            if (length2!=length2A) {
                ML_Expected("Equal length sublists in List[List[],...,List[*]]  expected",false);
                x.SetShape(1,1,1);
                x(1,1,1).re=-998;x(1,1,1).im=-997;
                return; 
            }

        }
        for (int i2=1;i2<=length2;i2++) {

            if (i1>1 || i2>1) {
                long length3A;
                int flag3A=MLCheckFunction(stdlink,"List",&length3A);
                if (!flag3A) {
                    ML_Expected("List[List[List[],...,List[*]]]  expected",false);
                    x.SetShape(1,1,1);
                    x(1,1,1).re=-998;x(1,1,1).im=-997;
                    return; 
                }
                if (length3!=length3A) {
                    ML_Expected("Equal length sublists in List[List[],...,List[*]]  expected",false);
                    x.SetShape(1,1,1);
                    x(1,1,1).re=-998;x(1,1,1).im=-997;
                    return; 
                }

            }



            for (int i3=1;i3<=length3;i3++) {

                lm_complex y;
                cb_get(y);
                x(i1,i2,i3)=y;
            }
        }
    }

};


void cb_get(lightNNNNlm_complex & x){
    /*

    How lengths are evaluated.
    List[
    List[
    List[
    List[(*)Complex,Complex]
    List[Complex,Complex,Complex,Complex]
    List[Complex,Complex]
    ]
    List[
    List[Complex,Complex]
    List[Complex,Complex,Complex,Complex]
    List[Complex,Complex]
    ]
    ]
    ]

    length1=1
    length2=2
    length2A=2
    length3=3
    length3A= 3 
    length3=2
    length3A= 4 (error!) , then 2




    */

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("A List of Complex");
        x.SetShape(1,1,1,1);
        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
        return; 
    }
    long length1;
    int flag1=MLCheckFunction(stdlink,"List",&length1);
    if (!flag1) {
        ML_Expected("List[]  expected",false);
        x.SetShape(1,1,1,1);
        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
        return; 
    }

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("List[funcname[...]] expected ");
        x.SetShape(1,1,1,1);
        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
        return; 
    }

    long length2;
    int flag2=MLCheckFunction(stdlink,"List",&length2);
    if (!flag2) {
        ML_Expected("List[List[]]  expected",false);
        x.SetShape(1,1,1,1);
        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
        return; 
    }

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("List[List[funcname[...]]] expected ");
        x.SetShape(1,1,1,1);
        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
        return; 
    }

    long length3;
    int flag3=MLCheckFunction(stdlink,"List",&length3);
    if (!flag3) {
        ML_Expected("List[List[List[..]]]  expected",false);
        x.SetShape(1,1,1,1);
        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
        return; 
    }

    if__next=MLGetNext( stdlink);
    if (if__next!=MLTKFUNC) {
        ML_Expected("List[List[List[funcname[...]]]] expected ");
        x.SetShape(1,1,1,1);
        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
        return; 
    }

    long length4;
    int flag4=MLCheckFunction(stdlink,"List",&length4);
    if (!flag4) {
        ML_Expected("List[List[List[List[..]]]]  expected",false);
        x.SetShape(1,1,1,1);
        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
        return; 
    }


    x.SetShape(length1,length2,length3,length4);
    for (int i1=1;i1<=length1;i1++) {
        if (i1>1) {
            long length2A;
            int flag2A=MLCheckFunction(stdlink,"List",&length2A);
            if (!flag2A) {
                ML_Expected("List[List[],...,List[*]]  expected",false);
                x.SetShape(1,1,1,1);
                x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
                return; 
            }
            if (length2!=length2A) {
                ML_Expected("Equal length sublists in List[List[],...,List[*]]  expected",false);
                x.SetShape(1,1,1,1);
                x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
                return; 
            }

        }
        for (int i2=1;i2<=length2;i2++) {

            if (i1>1 || i2>1) {
                long length3A;
                int flag3A=MLCheckFunction(stdlink,"List",&length3A);
                if (!flag3A) {
                    ML_Expected("List[List[List[],...,List[*]]]  expected",false);
                    x.SetShape(1,1,1,1);
                    x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
                    return; 
                }
                if (length3!=length3A) {
                    ML_Expected("Equal length sublists in List[List[],...,List[*]]  expected",false);
                    x.SetShape(1,1,1,1);
                    x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
                    return; 
                }

            }


            for (int i3=1;i3<=length3;i3++) {

                if (i1>1 || i2>1 || i3>1) {
                    long length4A;
                    int flag4A=MLCheckFunction(stdlink,"List",&length4A);
                    if (!flag4A) {
                        ML_Expected("List[List[List[List[],...,List[*]]]]  expected",false);
                        x.SetShape(1,1,1,1);
                        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
                        return; 
                    }
                    if (length4!=length4A) {
                        ML_Expected("Equal length sublists in List[List[List[],...,List[*]]]]  expected",false);
                        x.SetShape(1,1,1,1);
                        x(1,1,1,1).re=-998;x(1,1,1,1).im=-997;
                        return; 
                    }

                }



                for (int i4=1;i4<=length4;i4++) {

                    lm_complex y;
                    cb_get(y);
                    x(i1,i2,i3,i4)=y;
                }
            }
        }
    }

};

