//           -*- c++ -*-

#ifndef LIGHTN_H
#define LIGHTN_H
// <cd> lightN
//
// .SS Functionality
//
// lightN is a template for classes that implement vectors with any
// number of elements. E.g. a vector v with 5 elements of type double
// can be instanciated with:
//
// <code>lightN&lt;double&gt; v(5);</code>
//
// The number of elements in the vector can change during
// execution. It changes its size to whatever is needed when it is
// assigned a new value with an assignment operator.
//
// When a lightN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightN object with four elements to one with five elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//
#define IN_LIGHTN<T>_H

#define LIGHTN_SIZE 10
// The default size.

template<class T> class light3;
template<class T> class light4;
template<class T> class lightN;
template<class T> class lightN3;
template<class T> class lightN33;
template<class T> class lightNN;
template<class T> class lightNNN;
template<class T> class lightNNNN;

template<class T>
class lightN {
public:

#ifdef IN_LIGHTNdouble_H
  friend class lightN<int>;
#else
  friend class lightN<double>;
#endif

#ifdef IN_LIGHTNint_H
#define TDtypeH  double
#define TDNtypeH lightNdouble
#else
#define TDtypeH T
#define TDNtypeH lightN<T>
#endif


#ifdef LIGHTMAT_TEMPLATES
  // friend class lightN<int>;
#endif

  #include "lightN_auto.h"

  inline lightN();
  // Default constructor.

  inline lightN(const lightN<T>&);
  // Copy constructor.
  
  inline lightN(const light3<T>&);
  // Conversion.

  inline lightN(const light4<T>&);
  // Conversion.

  inline lightN(const int);
  // Construct a lightN of given size.

  inline lightN(const int, const T *);
  // Construct a lightN of given size and initialize elements with values
  // from an array.

  inline lightN(const int, const T);
  // Construct a lightN of given size and initialize all elements
  // with a value (the second argument).

  inline ~lightN();
  // Destructor.


#ifdef CONV_INT_2_DOUBLE
  operator lightN<double>();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightN<lm_complex>();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightN<lm_complex>();
  // Convert to complex.
#endif


  lightN<T>& operator=(const lightN<T>&);
  // Assignment.

  lightN<T>& operator=(const light3<T>&);
  // Assignment, change size to 3 elements.

  lightN<T>& operator=(const light4<T>&);
  // Assignment, change size to 4 elements.

  lightN<T>& operator=(const T);
  // Assign one value to all elements.

  //
  // operator()
  //

  T operator() (const int x) const {
    limiterror((x<1) || (x>size1));
    return elem[x-1];
  };
  // Get the value of one element.

  T& operator()(const int x) {
    limiterror((x<1) || (x>size1));
    return elem[x-1];
  };
  // Get/Set the value of one element.
 
  inline T& takeElementReference(const int);
  // Same as above

  inline lightN<T>& SetShape(const int x=-1);
  // Sets the specified size. 

//REMOVE LATER
  lightN<T> SubVector(const int n1,const  int n2) const; 
  // Gets specified interval of the vector.

  const lightN<T>& SetSubVector(const int n1,const  int n2,const lightN<T>& x);
  // Puts all  elements of x into specified interval of  the current vector.
  // Returns the vector x.

  T SetSubVector(const int n1,const  int n2,const T x);
  // Puts the   element x into all elements of specified interval of  the current vector.
  // Returns x.

  int operator==(const lightN<T>&) const;
  // Equality.

  int operator!=(const lightN<T>&) const;
  // Inequality.
 
  lightN<T>& operator+=(const T);
  // Add a value to all elements.

  lightN<T>& operator+=(const lightN<T>&);
  // Elementwise addition.

  lightN<T>& operator-=(const T);
  // Subtract a value from all elements.

  lightN<T>& operator-=(const lightN<T>&);
  // Elementwise subtraction.

  lightN<T>& operator*=(const T);
  // Multiply all elements with a value.

  lightN<T>& operator/=(const T);
  // Divide all elements with a value.

#ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
#endif

  T Extract(const lightN<int>&) const;
  // Extract an element using given index.   

  int dimension(const int x = 1) const {
    if(x == 1)
      return size1;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == no. elements).

  #ifndef COMPLEX_TOOLS
  lightN<T>& normalize();
  // Normalize vector. Returns a reference to itself.
  #endif

  void Get(T *) const;
  // Get values of all elements and put them in an array.

  void Set(const T *);
  // Set values of all elements from array.

  T * data() const;
  // Direct access to the stored data. Use the result with care.

  lightN<T> operator+() const;
  // Unary plus.

  lightN<T> operator-() const;
  // Unary minus.


  //<ignore>
#ifdef IN_LIGHTNlm_complex_H
  friend class lightN<int>;
#endif
  friend class light3<T>;
  friend class light4<T>;
  friend class lightN3<T>;
  friend class lightNN<T>;
  friend class lightNNN<T>;
  friend class lightNNNN<T>;
  friend class lightN33<T>;
  //</ignore>

  friend inline lightN<T> operator+(const lightN<T>&, const lightN<T>&);
  // Elementwise addition.

  //--
  friend inline lightN<T> operator+(const lightN<T>&, const T);
  // Addition to all elements.

  friend inline lightN<T> operator+(const T, const lightN<T>&);
  // Addition to all elements.

#ifdef IN_LIGHTNdouble_H
  friend inline lightN<double> operator+(const lightN<double>&, const int);
  friend inline lightN<double> operator+(const lightN<int>&, const double);
  friend inline lightN<double> operator+(const double, const lightN<int>&);
  friend inline lightN<double> operator+(const int, const lightN<double>&);
#endif

#ifdef IN_LIGHTNlm_complex_H
friend inline lightNlm_complex operator+(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator+(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator+(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator+(const double e, const lightNlm_complex& s2);
#endif

  //--

  friend inline lightN<T> operator-(const lightN<T>&, const lightN<T>&);
  // Elementwise subtraction.

  friend inline lightN<T> operator-(const lightN<T>&, const T);
  // Subtraction from all elements.

  friend inline lightN<T> operator-(const T, const lightN<T>&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNdouble_H
  friend inline lightN<double> operator-(const lightN<double>&, const int);
  friend inline lightN<double> operator-(const lightN<int>&, const double);
  friend inline lightN<double> operator-(const double, const lightN<int>&);
  friend inline lightN<double> operator-(const int, const lightN<double>&);
#endif

  #ifdef IN_LIGHTNlm_complex_H
friend inline lightNlm_complex operator-(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator-(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator-(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator-(const double e, const lightNlm_complex& s2);
#endif


  friend inline T operator*(const lightN<T>&, const lightN<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const lightN<T>&, const T);
  // Multiply all elements.

  friend inline lightN<T> operator*(const T, const lightN<T>&);
  // Multiply all elements.

#ifdef IN_LIGHTNdouble_H
  friend inline lightN<double> operator*(const lightN<double>&, const int);
  friend inline lightN<double> operator*(const lightN<int>&, const double);
  friend inline lightN<double> operator*(const double, const lightN<int>&);
  friend inline lightN<double> operator*(const int, const lightN<double>&);
#endif

#ifdef IN_LIGHTNlm_complex_H
friend inline lightNlm_complex operator*(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator*(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator*(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator*(const double e, const lightNlm_complex& s2);
#endif

  friend inline lightN<T> operator*(const lightN<T>&, const lightNN<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const light3<T>&, const lightNN<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const light4<T>&, const lightNN<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const lightNN<T>&,const lightN<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const lightNN<T>&, const light3<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const lightNN<T>&, const light4<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const lightN3<T>&, const light3<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const lightN3<T>&, const lightN<T>&);
  // Inner product.

  friend inline lightN<T> operator/(const lightN<T>&, const T);
  // Divide all elements.

  friend inline lightN<T> operator/(const T, const lightN<T>&);
  // Divide all elements.

#ifdef IN_LIGHTNdouble_H
  friend inline lightN<double> operator/(const lightN<double>&, const int);
  friend inline lightN<double> operator/(const lightN<int>&, const double);
  friend inline lightN<double> operator/(const double, const lightN<int>&);
  friend inline lightN<double> operator/(const int, const lightN<double>&);
#endif

#ifdef IN_LIGHTNlm_complex_H
friend inline lightNlm_complex operator/(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator/(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator/(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator/(const double e, const lightNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNlm_complex_H
friend inline lightNdouble arg(const lightNlm_complex& a);
friend inline lightNdouble re(const lightNlm_complex& a);
friend inline lightNdouble im(const lightNlm_complex& a);
friend inline lightNlm_complex conjugate(const lightNlm_complex& a);
//#endif

  friend inline lightN<T> pow(const lightN<T>&, const lightN<T>&);
  // Raise to the power of-function, elementwise.

  friend inline lightN<T> pow(const lightN<T>&, const T);
  // Raise to the power of-function, for all elements.

  friend inline lightN<T> pow(const T, const lightN<T>&);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex pow(const lightNlm_complex& s1, const double e);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex pow( const double e,const lightNlm_complex& s1);
  // Raise to the power of-function, for all elements.

  friend inline lightN<T> ElemProduct(const lightN<T>&, const lightN<T>&);
  // Elementwise multiplication.

  friend inline lightN<T> ElemQuotient(const lightN<T>&, const lightN<T>&);
  // Elementwise division.

  friend inline lightN<T> Apply(const lightN<T>&, T f(T));
  // Apply the function elementwise all elements.

  friend inline lightN<T> Apply(const lightN<T>&, const lightN<T>&, T f(T, T));
  // Apply the function elementwise on all elements in the two vectors.


#ifdef IN_LIGHTNlm_complex_H
#else
  friend inline lightN<T> abs(const lightN<T>&);
#ifdef IN_LIGHTNdouble_H
  friend inline lightN<double> abs(const lightN<lm_complex>&);
#endif 
#endif

#ifndef COMPLEX_TOOLS
  // sign
  friend inline lightN<int> sign(const lightN<int>&);  
  friend inline lightN<int> sign(const lightN<double>&);
#else
  friend inline lightNlm_complex sign(const lightNlm_complex&);
#endif
  /// Added 2/2/98 

  friend inline lightN<T>  FractionalPart(const lightN<T>&);
  //  FractionalPart

  //old version
  //friend inline lightN<int>  IntegerPart(const lightN<int>&);
  //  IntegerPart

  //friend inline lightN<int>  IntegerPart(const lightN<double>&);
  //  IntegerPart

  //new version
  friend inline lightN<T>  IntegerPart(const lightN<T>&);
  //  IntegerPart

  friend inline lightN<T> Mod (const  lightN<T>&,const  lightN<T>&);
  // Mod
 
  friend inline lightN<T> Mod (const  lightN<T>&,const T);
  // Mod

  friend inline lightN<T> Mod (const T,const  lightN<T>&);
  // Mod

  friend inline T LightMax (const lightN<T>& );
  // Max

  friend inline T LightMin (const lightN<T>& );
  // Min

  friend inline T findLightMax (const T *,const int );
  // Find Max
 
  friend inline T findLightMin (const  T *,const  int );
  // Find Min
 
  /// End Added 

  friend inline lightN<int> ifloor(const lightN<double>&);
  // ifloor

  friend inline lightN<int> iceil(const lightN<double>&);
  // iceil

  friend inline lightN<int> irint(const lightN<double>&);
  // irint

  friend inline lightN<double> sqrt(const lightN<double>&);
  // sqrt

  friend inline lightN<double> exp(const lightN<double>&);
  // exp

  friend inline lightN<double> log(const lightN<double>&);
  // log

  friend inline lightN<double> sin(const lightN<double>&);
  // sin

  friend inline lightN<double> cos(const lightN<double>&);
  // cos

  friend inline lightN<double> tan(const lightN<double>&);
  // tan

  friend inline lightN<double> asin(const lightN<double>&);
  // asin

  friend inline lightN<double> acos(const lightN<double>&);
  // acos

  friend inline lightN<double> atan(const lightN<double>&);
  // atan

  friend inline lightN<double> sinh(const lightN<double>&);
  // sinh

  friend inline lightN<double> cosh(const lightN<double>&);
  // cosh

  friend inline lightN<double> tanh(const lightN<double>&);
  // tanh

  friend inline lightN<double> asinh(const lightN<double>&);
  // asinh

  friend inline lightN<double> acosh(const lightN<double>&);
  // acosh

  friend inline lightN<double> atanh(const lightN<double>&);
  // atanh

  friend inline lightN<lm_complex> sqrt(const lightN<lm_complex>&);
  // sqrt

  friend inline lightN<lm_complex> exp(const lightN<lm_complex>&);
  // exp

  friend inline lightN<lm_complex> ifloor(const lightN<lm_complex>&);
  // ifloor

  friend inline lightN<lm_complex> iceil(const lightN<lm_complex>&);
  // iceil

  friend inline lightN<lm_complex> irint(const lightN<lm_complex>&);
  // irint

  friend inline lightN<lm_complex> log(const lightN<lm_complex>&);
  // log

  friend inline lightN<lm_complex> sin(const lightN<lm_complex>&);
  // sin

  friend inline lightN<lm_complex> cos(const lightN<lm_complex>&);
  // cos

  friend inline lightN<lm_complex> tan(const lightN<lm_complex>&);
  // tan

  friend inline lightN<lm_complex> asin(const lightN<lm_complex>&);
  // asin

  friend inline lightN<lm_complex> acos(const lightN<lm_complex>&);
  // acos

  friend inline lightN<lm_complex> atan(const lightN<lm_complex>&);
  // atan

  friend inline lightN<lm_complex> sinh(const lightN<lm_complex>&);
  // sinh

  friend inline lightN<lm_complex> cosh(const lightN<lm_complex>&);
  // cosh

  friend inline lightN<lm_complex> tanh(const lightN<lm_complex>&);
  // tanh

  friend inline lightN<lm_complex> asinh(const lightN<lm_complex>&);
  // asinh

  friend inline lightN<lm_complex> acosh(const lightN<lm_complex>&);
  // acosh

  friend inline lightN<lm_complex> atanh(const lightN<lm_complex>&);
  // atanh


  friend inline lightN<T> Cross (const  lightN<T>&, const  lightN<T>&); 
  friend inline lightN<T> Cross (const  lightN<T>&, const  lightN<T>&, const  lightN<T>&); 
  friend inline lightN<T> Cross (const  lightN<T>&, const  lightN<T>&, const  lightN<T>&, const  lightN<T>&); 

  friend inline lightN<T> light_join (const lightN<T> arr1, const lightN<T> arr2);
  friend inline lightN<T> light_drop (const lightN<T> arr1, const R r);

#ifdef IN_LIGHTNint_H
  friend inline double light_mean     ( const  lightN<T>& );
  friend inline double light_standard_deviation( const  lightN<T>& );
  friend inline double light_variance ( const  lightN<T>& );
  friend inline double light_median   ( const  lightN<T>& );
#else
  friend inline T light_mean     ( const  lightN<T>& );
  friend inline T light_variance ( const  lightN<T>& );
  friend inline T light_standard_deviation( const  lightN<T>& );
  friend inline T light_median   ( const  lightN<T>& );
#endif
  
  friend inline T light_total    ( const  lightN<T>& );
 
 
  friend inline lightN<T> light_sort (const lightN<T> arr1);

  friend inline TDNtypeH light_quantile3L (const lightN<T> arr1, const lightN<double> q2, const lightNN<double> params);
  friend inline TDtypeH light_quantile3 (const lightN<T> arr1, double q,  const lightNN<double> params);
  friend inline TDNtypeH light_quantile2L (const lightN<T> arr1, const lightN<double> q2);
  friend inline TDtypeH light_quantile2 (const lightN<T> arr1, double q);

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNdouble::lightN(const lightNint& s1, const double e, const lightmat_plus_enum)

  T *elem;
  // elem always points at the area where the elements are.


protected:
  T sarea[LIGHTN_SIZE];
  // The values are stored here if they fit into LIGHTN_SIZE
  // else a memory area is allocated.

  int size1;
  // The size of the vector (number of elements).

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightN(const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // argument is the size of the created vector. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightN(const lightN<T>&, const lightN<T>&, const lightmat_plus_enum);
  lightN(const lightN<T>&, const T, const lightmat_plus_enum);
#ifdef IN_LIGHTNdouble_H
  lightN(const lightNdouble&, const int, const lightmat_plus_enum);
  lightN(const lightNint&, const double, const lightmat_plus_enum);
#endif

  lightN(const lightN<T>&, const lightN<T>&, const lightmat_minus_enum);
  lightN(const T, const lightN<T>&, const lightmat_minus_enum);
#ifdef IN_LIGHTNdouble_H
  lightN(const int, const lightNdouble&, const lightmat_minus_enum);
  lightN(const double, const lightNint&, const lightmat_minus_enum);
#endif

  lightN(const lightN<T>&, const lightN<T>&, const lightmat_mult_enum);
  lightN(const lightN<T>&, const T, const lightmat_mult_enum);
#ifdef IN_LIGHTNdouble_H
  lightN(const lightNdouble&, const int, const lightmat_mult_enum);
  lightN(const lightNint&, const double, const lightmat_mult_enum);
#endif

  lightN(const lightN<T>&, const lightNN<T>&, const lightmat_mult_enum);
  lightN(const lightNN<T>&, const lightN<T>&, const lightmat_mult_enum);
  lightN(const light3<T>&, const lightNN<T>&, const lightmat_mult_enum);
  lightN(const light4<T>&, const lightNN<T>&, const lightmat_mult_enum);
  lightN(const lightN3<T>&, const light3<T>&, const lightmat_mult_enum);
  lightN(const lightN3<T>&, const lightN<T>&, const lightmat_mult_enum);
  lightN(const lightNN<T>&, const light3<T>&, const lightmat_mult_enum);
  lightN(const lightNN<T>&, const light4<T>&, const lightmat_mult_enum);

  lightN(const T, const lightN<T>&, const lightmat_div_enum);
#ifdef IN_LIGHTNdouble_H
  lightN(const int, const lightNdouble&, const lightmat_div_enum);
  lightN(const double, const lightNint&, const lightmat_div_enum);
#endif

  lightN(const lightN<T>&, const lightN<T>&, const lightmat_pow_enum);
  lightN(const lightN<T>&, const T, const lightmat_pow_enum);
  lightN(const T, const lightN<T>&, const lightmat_pow_enum);

#ifdef IN_LIGHTNlm_complex_H
  lightNlm_complex(const lightNlm_complex& s1, const double e, const lightmat_pow_enum) ;
  lightNlm_complex(const double e,const lightNlm_complex& s1, const lightmat_pow_enum) ;
#endif

#ifdef IN_LIGHTNlm_complex_H
 #else
  #ifdef IN_LIGHTNdouble_H
   lightN(const lightN<T>&, const lightmat_abs_enum);
   lightN(const lightN<lm_complex>&, const lightmat_abs_enum);
  #else
  lightN(const lightN<T>&, const lightmat_abs_enum);
  #endif
#endif

  lightN(const lightN<T>&, const lightN<T>&, const lightmat_eprod_enum);
  lightN(const lightN<T>&, const lightN<T>&, const lightmat_equot_enum);
  lightN(const lightN<T>&, T f(T), const lightmat_apply_enum);
  lightN(const lightN<T>&, const lightN<T>&, T f(T, T), const lightmat_apply_enum);

};





 inline T findLightMax (const T *,const int );
// Find Max

 inline T findLightMin (const  T *,const  int );
// Find Min








typedef lightN<double> doubleN;
typedef lightN<int> intN;
typedef lightN<lm_complex> lm_complexN;

#undef TDtypeH  
#undef TDNtypeH 

#undef IN_LIGHTN<T>_H

#endif
