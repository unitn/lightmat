//           -*- c++ -*-

#ifndef LIGHT44_C_H
#define LIGHT44_C_H
// <cd> light44lm_complex
//
// .SS Functionality
//
// light44 is a template for classes that implement matrices with 4x4
// elements. E.g. a matrix v with 4x4 elements of type double can be
// instanciated with:
//
// <code>light44&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHT44lm_complex_C_H

 class light3;
 class light4;
 class lightN;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light44lm_complex {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light44double();
  // Convert to double.
#else
#endif
#ifdef LIGHTMAT_TEMPLATES
  //  friend class light44int;
#endif

#ifdef IN_LIGHT44double_C_H
   friend class  light44int;
#else
   friend class  light44double;
#endif

  #include "c_light44_auto.h"

  light44lm_complex();
  // Default constructor.

  light44lm_complex(const light44lm_complex&);
  // Copy constructor.

  inline 
  light44lm_complex(const lm_complex e11, const lm_complex e12, const lm_complex e13, const lm_complex e14,
	  const lm_complex e21, const lm_complex e22, const lm_complex e23, const lm_complex e24,
	  const lm_complex e31, const lm_complex e32, const lm_complex e33, const lm_complex e34,
	  const lm_complex e41, const lm_complex e42, const lm_complex e43, const lm_complex e44);
  // Initialize elements with values.

  light44lm_complex(const lm_complex *);
  // Initialize elements with values from an array (in row major order).

  light44lm_complex(const lm_complex);
  // Initialize all elements with the same value.

  light44lm_complex& operator=(const light44lm_complex&);
  // Assignment.

  light44lm_complex& operator=(const lightNNlm_complex&);
  // Assignment from a lightNN where NN=44.

  light44lm_complex& operator=(const lm_complex);
  // Assign one value to all elements.

  lm_complex operator() (const int x, const int y) const {
    limiterror((x<1) || (x>4) || (y<1) || (y>4));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*4];
#else
    return elem[x-1+(y-1)*4];
#endif
  };
  // Get the value of one element.

  lm_complex& operator()(const int x, const int y) {
    limiterror((x<1) || (x>4) || (y<1) || (y>4));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*4];
#else
    return elem[x-1+(y-1)*4];
#endif
  };
  // Get/Set the value of one element.

  light4lm_complex operator()(const int) const;
  // Get the value of one row.
 



  //
  // Set
  //

  inline light44lm_complex& Set (const int i0, const lm_complex val);
  inline light44lm_complex& Set (const int i0, const lightNlm_complex& arr);
  inline light44lm_complex& Set (const int i0, const light3lm_complex& arr);
  inline light44lm_complex& Set (const int i0, const light4lm_complex& arr);



  int operator==(const light44lm_complex&) const;
  // Equality.

  int operator!=(const light44lm_complex&) const;
  // Inequality.

  light44lm_complex& operator+=(const lm_complex);
  // Add a value to all elements.

  light44lm_complex& operator+=(const light44lm_complex&);
  // Elementwise addition.

  light44lm_complex& operator-=(const lm_complex);
  // Subtract a value from all elements.

  light44lm_complex& operator-=(const light44lm_complex&);
  // Elementwise subtraction

  light44lm_complex& operator*=(const lm_complex);
  // Muliply all elements with a value.

  light44lm_complex& operator/=(const lm_complex);
  // Divide all elements with a value.

  light44lm_complex& reshape(const int, const int, const light4lm_complex&);
  // Convert the light4-vector to the given size and put the result in
  // this object. All four vector-columns of the light44 object will
  // get the value of the light4-argument. The first two arguments
  // (the size of the matrix) must both have the value 4 since it is a
  // light44 object.

  int dimension(const int x) const {
    if((x==1) || (x==2))
      return 4;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 4 and dimension(2) == 4).

  void Get(lm_complex *) const;
  // Get values of all elements and put them in an array (9 elements long,
  // row major order).

  void Set(const lm_complex *data);
  // Set values of all elements from array (9 elements long, row major
  // order).

  light44lm_complex operator+() const;
  // Unary plus.

  light44lm_complex operator-() const;
  // Unary minus.

  friend inline light44lm_complex operator+(const light44lm_complex&, const light44lm_complex&);
  // Elementwise addition.

  friend inline light44lm_complex operator+(const light44lm_complex&, const lm_complex);
  // Addition to all elements.

  friend inline light44lm_complex operator+(const lm_complex, const light44lm_complex&);
  // Addition to all elements.

  friend inline light44lm_complex operator-(const light44lm_complex&, const light44lm_complex&);
  // Elementwise subtraction.

  friend inline light44lm_complex operator-(const light44lm_complex&, const lm_complex);
  // Subtraction from all elements.

  friend inline light44lm_complex operator-(const lm_complex, const light44lm_complex&);
  // Subtraction to all elements.

  friend inline light44lm_complex operator*(const light44lm_complex&, const light44lm_complex&);
  // Inner product.

  friend inline light44lm_complex operator*(const light44lm_complex&, const lm_complex);
  // Multiply all elements.

  friend inline light44lm_complex operator*(const lm_complex, const light44lm_complex&);
  // Multiply all elements.

  friend inline light4lm_complex operator*(const light44lm_complex&, const light4lm_complex&);
  // Inner product.

  friend inline light4lm_complex operator*(const light4lm_complex&, const light44lm_complex&);
  // Inner product.

  friend inline light44lm_complex operator/(const light44lm_complex&, const lm_complex);
  // Divide all elements.

  friend inline light44lm_complex operator/(const lm_complex, const light44lm_complex&);
  // Divide with all elements.

  friend inline light44lm_complex pow(const light44lm_complex&, const light44lm_complex&);
  // Raise to the power of-function, elementwise.

  friend inline light44lm_complex pow(const light44lm_complex&, const lm_complex);
  // Raise to the power of-function, for all elements.

  friend inline light44lm_complex pow(const lm_complex, const light44lm_complex&);
  // Raise to the power of-function, for all elements.

  friend inline light44lm_complex ElemProduct(const light44lm_complex&, const light44lm_complex&);
  // Elementwise multiplication.

  friend inline light44lm_complex ElemQuotient(const light44lm_complex&, const light44lm_complex&);
  // Elementwise division.

  friend inline light44lm_complex Apply(const light44lm_complex&, lm_complex f(lm_complex));
  // Apply the function elementwise on all elements.

  friend inline light44lm_complex Apply(const light44lm_complex&, const light44lm_complex&, lm_complex f(lm_complex, lm_complex));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline light44lm_complex Transpose(const light44lm_complex&);
  // Transpose matrix.

  friend inline light44lm_complex abs(const light44lm_complex&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light44int sign(const light44lm_complex&);
  // sign
#else
  friend inline light44lm_complex sign(const light44lm_complex&);
#endif
  friend inline light44int ifloor(const light44double&);
  // ifloor

  friend inline light44int iceil(const light44double&);
  // iceil

  friend inline light44int irint(const light44double&);
  // irint

  friend inline light44double sqrt(const light44double&);
  // sqrt

  friend inline light44double exp(const light44double&);
  // exp

  friend inline light44double log(const light44double&);
  // log

  friend inline light44double sin(const light44double&);
  // sin

  friend inline light44double cos(const light44double&);
  // cos

  friend inline light44double tan(const light44double&);
  // tan

  friend inline light44double asin(const light44double&);
  // asin

  friend inline light44double acos(const light44double&);
  // acos

  friend inline light44double atan(const light44double&);
  // atan

  friend inline light44double sinh(const light44double&);
  // sinh

  friend inline light44double cosh(const light44double&);
  // cosh

  friend inline light44double tanh(const light44double&);
  // tanh

  friend inline light44double asinh(const light44double&);
  // asinh

  friend inline light44double acosh(const light44double&);
  // acosh

  friend inline light44double atanh(const light44double&);
  // atanh

  friend inline light44lm_complex ifloor(const light44lm_complex&);
  // ifloor

  friend inline light44lm_complex iceil(const light44lm_complex&);
  // iceil

  friend inline light44lm_complex irint(const light44lm_complex&);
  // irint

  friend inline light44lm_complex sqrt(const light44lm_complex&);
  // sqrt

  friend inline light44lm_complex exp(const light44lm_complex&);
  // exp

  friend inline light44lm_complex log(const light44lm_complex&);
  // log

  friend inline light44lm_complex sin(const light44lm_complex&);
  // sin

  friend inline light44lm_complex cos(const light44lm_complex&);
  // cos

  friend inline light44lm_complex tan(const light44lm_complex&);
  // tan

  friend inline light44lm_complex asin(const light44lm_complex&);
  // asin

  friend inline light44lm_complex acos(const light44lm_complex&);
  // acos

  friend inline light44lm_complex atan(const light44lm_complex&);
  // atan

  friend inline light44lm_complex sinh(const light44lm_complex&);
  // sinh

  friend inline light44lm_complex cosh(const light44lm_complex&);
  // cosh

  friend inline light44lm_complex tanh(const light44lm_complex&);
  // tanh

  friend inline light44lm_complex asinh(const light44lm_complex&);
  // asinh

  friend inline light44lm_complex acosh(const light44lm_complex&);
  // acosh

  friend inline light44lm_complex atanh(const light44lm_complex&);
  // atanh


  //<ignore>
//    friend class light44int;
  friend class light4lm_complex;
  friend class lightN3lm_complex;
  friend class lightNNlm_complex;
  friend class lightNNNlm_complex;
  friend class lightNNNNlm_complex;
  //</ignore>

protected:
  lm_complex elem[16];
  // The values of the sixteen elements.

  light44lm_complex(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.


};

typedef light44double double44;
typedef light44int int44;

#undef IN_LIGHT44lm_complex_C_H

#endif
