//           -*- c++ -*-

#ifndef LIGHTN3_H
#define LIGHTN3_H
// <cd> lightN3
//
// .SS Functionality
//
// lightN3 is a template for classes that implement matrices with 3
// columns and any number of rows. E.g. a matrix v with 5x3 elements
// of type double can be instanciated with:
//
// <code>lightN3&lt;double&gt; v(5);</code>
//
// The number of rows in the matrix can change during execution. It
// changes its size to whatever is needed when it is assigned a new
// value with an assignment operator.
//
// When a lightN3 object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightN3 object with 5x3 elements to one with 7x3 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#define LIGHTN3_SIZE 10
// FOOFOO

template<class T> class lightN;
template<class T> class light33;
template<class T> class light44;
template<class T> class lightNN;

template<class T>
class lightN3 {
public:
  lightN3();
  // Default constructor.

  lightN3(const lightN3<T>&);
  // Copy constructor.

  lightN3(const light33<T>&);
  // Conversion.

  lightN3(const int);
  // Construct a lightN3 with a given number of rows.

  lightN3(const int, const T *);
  // Construct a lightN3 with a given number of rows and initialize the
  // elements with values from an array (in row major order).

  lightN3(const int, const T);
  // Construct a lightN3 with a given number of rows and initialize the
  // elements with a value (the second argument).

  ~lightN3();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightN3<double>();
  // Convert to double.
#else
  friend class lightN3<int>;
#endif
#ifdef LIGHTMAT_TEMPLATES
  // friend class lightN3<int>;
#endif
  lightN3<T>& operator=(const lightN3<T>&);
  // Assignment.
  
  lightN3<T>& operator=(const light33<T>&);
  // Assignment, change size to 3x3.

  lightN3<T>& operator=(const lightNN<T>&);
  // Assignment from a lightNN with 3 columns.

  lightN3<T>& operator=(const T);
  // Assign one value to all elements.

  T operator()(const int, const int) const;
  // Get the value of one element.

  T& operator()(const int, const int);
  // Get/Set the value of one element.

  //<ignore>
//FOOFOO    light3<T> operator()(const int) const;
  //</ignore>

  const light3<T>& operator()(const int) const;
  // Get the value of one row.

  light3<T>& operator()(const int);
  // Get/Set the value of one row.
 
  int operator==(const lightN3<T>&) const;
  // Equality.

  int operator!=(const lightN3<T>&) const;
  // Inequality.

  lightN3<T>& operator+=(const T);
  // Add a value to all elements.

  lightN3<T>& operator+=(const lightN3<T>&);
  // Elementwise addition.

  lightN3<T>& operator-=(const T);
  // Subtract a value from all elements.

  lightN3<T>& operator-=(const lightN3<T>&);
  // Elementwise subtraction.

  lightN3<T>& operator*=(const T);
  // Mulitply all elements with a value.

  lightN3<T>& operator/=(const T);
  // Divide all elements with a value.

  lightN3<T>& reshape(const int, const int, const lightN<T>&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. All vector-columns of the lightNN object will get
  // the value of the lightN-argument. The value of the first argument
  // must be the same as the number of elements in the lightN-vector
  // and the second argument must be 3. The program may dump core or
  // behave strangely if the arguments are incorrect.

  int dimension(const int) const;
  // Get size of some dimension (dimension(2) == 3 for lightN3).

  void Get(T *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const T *);
  // Set values of all elements from array (row major order).

  lightN3<T> operator+() const;
  // Unary plus.

  lightN3<T> operator-() const;
  // Unary minus.

  //<ignore>
  // friend class lightN3<int>;
  friend class light3<T>;
  friend class lightN<T>;
  friend class light33<T>;
  friend class lightNN<T>;
  //</ignore>

  friend inline lightN3<T> operator+(const lightN3<T>&, const lightN3<T>&);
  // Elementwise addition.

  friend inline lightN3<T> operator+(const lightN3<T>&, const T);
  // Addition to all elements.

  friend inline lightN3<T> operator+(const T, const lightN3<T>&);
  // Addition to all elements.

  friend inline lightN3<T> operator-(const lightN3<T>&, const lightN3<T>&);
  // Elementwise subtraction.

  friend inline lightN3<T> operator-(const lightN3<T>&, const T);
  // Subtraction from all elements.

  friend inline lightN3<T> operator-(const T, const lightN3<T>&);
  // Subtraction to all elements.

  friend inline lightN3<T> operator*(const lightN3<T>&, const lightN3<T>&);
  // Inner product.

  friend inline lightN3<T> operator*(const lightN3<T>&, const T);
  // Multiply all elements.

  friend inline lightN3<T> operator*(const T, const lightN3<T>&);
  // Multiply all elements.

  friend inline lightN3<T> operator*(const lightN3<T>&, const light33<T>&);
  // Inner product.

  friend inline lightN3<T> operator*(const light44<T>&, const lightN3<T>&);
  // Inner product.

  friend inline lightN3<T> operator*(const lightNN<T>&, const lightN3<T>&);
  // Inner product.

  friend inline lightN3<T> operator/(const lightN3<T>&, const T);
  // Divide all elements.

  friend inline lightN3<T> operator/(const T, const lightN3<T>&);
  // Divide all elements.

  friend inline lightN3<T> pow(const lightN3<T>&, const lightN3<T>&);
  // Raise to the power of-function, elementwise.

  friend inline lightN3<T> pow(const lightN3<T>&, const T);
  // Raise to the power of-function, for all elements.

  friend inline lightN3<T> pow(const T, const lightN3<T>&);
  // Raise to the power of-function, for all elements.

  friend inline lightN3<T> ElemProduct(const lightN3<T>&, const lightN3<T>&);
  // Elementwise multiplication.

  friend inline lightN3<T> ElemQuotient(const lightN3<T>&, const lightN3<T>&);
  // Elementwise division.

  friend inline lightN3<T> Apply(const lightN3<T>&, T f(T));
  // Apply the function elementwise all elements.

  friend inline lightN3<T> Apply(const lightN3<T>&, const lightN3<T>&, T f(T, T));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline lightN3<T> abs(const lightN3<T>&);
  // abs

  friend inline lightN3<int> sign(const lightN3<int>&);
  // sign

  friend inline lightN3<int> sign(const lightN3<double>&);
  // sign

  friend inline lightN3<int> ifloor(const lightN3<double>&);
  // ifloor

  friend inline lightN3<int> iceil(const lightN3<double>&);
  // iceil

  friend inline lightN3<int> irint(const lightN3<double>&);
  // irint

  friend inline lightN3<double> sqrt(const lightN3<double>&);
  // sqrt

  friend inline lightN3<double> exp(const lightN3<double>&);
  // exp

  friend inline lightN3<double> log(const lightN3<double>&);
  // log

  friend inline lightN3<double> sin(const lightN3<double>&);
  // sin

  friend inline lightN3<double> cos(const lightN3<double>&);
  // cos

  friend inline lightN3<double> tan(const lightN3<double>&);
  // tan

  friend inline lightN3<double> asin(const lightN3<double>&);
  // asin

  friend inline lightN3<double> acos(const lightN3<double>&);
  // acos

  friend inline lightN3<double> atan(const lightN3<double>&);
  // atan

  friend inline lightN3<double> sinh(const lightN3<double>&);
  // sinh

  friend inline lightN3<double> cosh(const lightN3<double>&);
  // cosh

  friend inline lightN3<double> tanh(const lightN3<double>&);
  // tanh

  friend inline lightN3<double> asinh(const lightN3<double>&);
  // asinh

  friend inline lightN3<double> acosh(const lightN3<double>&);
  // acosh

  friend inline lightN3<double> atanh(const lightN3<double>&);
  // atanh
 
  friend inline lightN3<lm_complex> ifloor(const lightN3<lm_complex>&);
  // ifloor

  friend inline lightN3<lm_complex> iceil(const lightN3<lm_complex>&);
  // iceil

  friend inline lightN3<lm_complex> irint(const lightN3<lm_complex>&);
  // irint

  friend inline lightN3<lm_complex> sign(const lightN3<lm_complex>&);

  friend inline lightN3<lm_complex> sqrt(const lightN3<lm_complex>&);
  // sqrt

  friend inline lightN3<lm_complex> exp(const lightN3<lm_complex>&);
  // exp

  friend inline lightN3<lm_complex> log(const lightN3<lm_complex>&);
  // log

  friend inline lightN3<lm_complex> sin(const lightN3<lm_complex>&);
  // sin

  friend inline lightN3<lm_complex> cos(const lightN3<lm_complex>&);
  // cos

  friend inline lightN3<lm_complex> tan(const lightN3<lm_complex>&);
  // tan

  friend inline lightN3<lm_complex> asin(const lightN3<lm_complex>&);
  // asin

  friend inline lightN3<lm_complex> acos(const lightN3<lm_complex>&);
  // acos

  friend inline lightN3<lm_complex> atan(const lightN3<lm_complex>&);
  // atan

  friend inline lightN3<lm_complex> sinh(const lightN3<lm_complex>&);
  // sinh

  friend inline lightN3<lm_complex> cosh(const lightN3<lm_complex>&);
  // cosh

  friend inline lightN3<lm_complex> tanh(const lightN3<lm_complex>&);
  // tanh

  friend inline lightN3<lm_complex> asinh(const lightN3<lm_complex>&);
  // asinh

  friend inline lightN3<lm_complex> acosh(const lightN3<lm_complex>&);
  // acosh

  friend inline lightN3<lm_complex> atanh(const lightN3<lm_complex>&);
  // atanh

protected:
//FOOFOO    light3<T> sarea[LIGHTN3_SIZE];

  light3<T> *elem;
  // The rows are stored here.

  int size;
  // The number of rows.

  int alloc_size;
  // The size of the allocated area (number of rows)

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  lightN3(const int x, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // argument is the number of rows in the matrix. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightN3(const lightN3<T>&, const lightN3<T>&, const lightmat_plus_enum);
  lightN3(const lightN3<T>&, const T, const lightmat_plus_enum);
  lightN3(const lightN3<T>&, const lightN3<T>&, const lightmat_minus_enum);
  lightN3(const T, const lightN3<T>&, const lightmat_minus_enum);
  lightN3(const lightN3<T>&, const lightmat_minus_enum);
  lightN3(const lightN3<T>&, const lightN3<T>&, const lightmat_mult_enum);
  lightN3(const lightN3<T>&, const T, const lightmat_mult_enum);
  lightN3(const lightNN<T>&, const lightN3<T>&, const lightmat_mult_enum);
  lightN3(const lightN3<T>&, const light33<T>&, const lightmat_mult_enum);
  lightN3(const light44<T>&, const lightN3<T>&, const lightmat_mult_enum);
  lightN3(const T, const lightN3<T>&, const lightmat_div_enum);
  lightN3(const lightN3<T>&, const lightN3<T>&, const lightmat_pow_enum);
  lightN3(const lightN3<T>&, const T, const lightmat_pow_enum);
  lightN3(const T, const lightN3<T>&, const lightmat_pow_enum);
  lightN3(const lightN3<T>&, const lightmat_abs_enum);
  lightN3(const lightN3<T>&, const lightN3<T>&, const lightmat_eprod_enum);
  lightN3(const lightN3<T>&, const lightN3<T>&, const lightmat_equot_enum);
  lightN3(const lightN3<T>&, T f(T), const lightmat_apply_enum);
  lightN3(const lightN3<T>&, const lightN3<T>&, T f(T, T), const lightmat_apply_enum);
};

typedef lightN3<double> doubleN3;
typedef lightN3<int> intN3;

#endif
