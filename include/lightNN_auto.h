inline lightNN<T>& Set (const int i0, const int i1, const T val);
inline lightNN<T>& Set (const R r0, const int i1, const lightN<T>& arr);
inline lightNN<T>& Set (const R r0, const int i1, const light3<T>& arr);
inline lightNN<T>& Set (const R r0, const int i1, const light4<T>& arr);
inline lightNN<T>& Set (const R r0, const int i1, const T val);
inline lightNN<T>& Set (const int i0, const R r1, const lightN<T>& arr);
inline lightNN<T>& Set (const int i0, const R r1, const light3<T>& arr);
inline lightNN<T>& Set (const int i0, const R r1, const light4<T>& arr);
inline lightNN<T>& Set (const int i0, const R r1, const T val);
inline lightNN<T>& Set (const R r0, const R r1, const lightNN<T>& arr);
inline lightNN<T>& Set (const R r0, const R r1, const light33<T>& arr);
inline lightNN<T>& Set (const R r0, const R r1, const light44<T>& arr);
inline lightNN<T>& Set (const R r0, const R r1, const T val);
inline lightN<T> operator() (const R r0, const int i1) const;
inline lightN<T> operator() (const int i0, const R r1) const;
inline lightNN<T> operator() (const R r0, const R r1) const;

#ifdef IN_LIGHTNNdouble_H
inline lightNNdouble(const double e, const lightNNint &s1, const lightmat_atan2_enum);
inline lightNNdouble(const double e, const lightNNdouble &s1, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const double e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const double e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const lightNNint &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const lightNNdouble &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const lightNNint &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const lightNNdouble &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const int e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const int e, const lightmat_atan2_enum);
inline lightNNdouble(const int e, const lightNNint &s1, const lightmat_atan2_enum);
inline lightNNdouble(const int e, const lightNNdouble &s1, const lightmat_atan2_enum);

#endif
