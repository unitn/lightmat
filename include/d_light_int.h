//
// sign
//


inline int sign(int    x);	

#ifndef __GNUG__

inline  int sign(double x);	

#else

// GNU has this symbol in library. We do not inline it.
        int sign(double x);	
#endif

// See details in light_int.icc
// These functions are defined once there.


#ifndef COMPLEX_TOOLS

light3int sign(const light3double& s);	


light4int sign(const light4double& s);	


light33int sign(const light33double& s);	


light44int sign(const light44double& s);	


lightNint sign(const lightNdouble& s);	


lightN3int sign(const lightN3double& s);	


lightNNint sign(const lightNNdouble& s);	


lightNNNint sign(const lightNNNdouble& s);	


lightNNNNint sign(const lightNNNNdouble& s);	

//
// abs
//

lightNdouble abs(const lightNdouble& s);


lightN3double abs(const lightN3double& s);


lightNNdouble abs(const lightNNdouble& s);


lightNNNdouble abs(const lightNNNdouble& s);


lightNNNNdouble abs(const lightNNNNdouble& s);


#else
light3lm_complex sign(const light3lm_complex& s);	


light4lm_complex sign(const light4lm_complex& s);	


light33lm_complex sign(const light33lm_complex& s);	


light44lm_complex sign(const light44lm_complex& s);	


lightNlm_complex sign(const lightNlm_complex& s);	


lightN3lm_complex sign(const lightN3lm_complex& s);	


lightNNlm_complex sign(const lightNNlm_complex& s);	


lightNNNlm_complex sign(const lightNNNlm_complex& s);	


lightNNNNlm_complex sign(const lightNNNNlm_complex& s);	
#endif

//
// Mod
//


lightNdouble Mod(const lightNdouble& v, const double s );


lightNdouble Mod(const double v, const lightNdouble& s);


lightNdouble Mod(const lightNdouble&v1, const lightNdouble&v2);




lightNNdouble Mod(const lightNNdouble& v, const double s );


lightNNdouble Mod(const double v, const lightNNdouble& s);


lightNNdouble Mod(const lightNNdouble&v1, const lightNNdouble&v2);




lightNNNdouble Mod(const lightNNNdouble& v, const double s );


lightNNNdouble Mod(const double v, const lightNNNdouble& s);


lightNNNdouble Mod(const lightNNNdouble&v1, const lightNNNdouble&v2);




lightNNNNdouble Mod(const lightNNNNdouble& v, const double s );


lightNNNNdouble Mod(const double v, const lightNNNNdouble& s);


lightNNNNdouble Mod(const lightNNNNdouble&v1, const lightNNNNdouble&v2);



