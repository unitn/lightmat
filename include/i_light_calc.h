//           -*- c++ -*-

#define IN_CALCint_I_H


//
// operator+
//

lightNint operator+(const lightNint& s1, const lightNint& s2);

//--

lightNint operator+(const lightNint& s1, const int e);


lightNint operator+(const int e, const lightNint& s2);

#ifdef IN_CALCdouble_I_H
lightNdouble operator+(const lightNdouble& s1, const int e);
lightNdouble operator+(const lightNint& s1, const double e);
lightNdouble operator+(const double e, const lightNint& s2);
lightNdouble operator+(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNlm_complex operator+(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator+(const lightNint& s1, const lm_complex e);
lightNlm_complex operator+(const lightNlm_complex& s1, const double e);
lightNlm_complex operator+(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator+(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator+(const int e, const lightNlm_complex& s2);
lightNlm_complex operator+(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator+(const double e, const lightNlm_complex& s2);
#endif

//--



lightN3int operator+(const lightN3int& s1, const lightN3int& s2);


lightN3int operator+(const lightN3int& s1, const int e);


lightN3int operator+(const int e, const lightN3int& s2);


lightNNint operator+(const lightNNint& s1, const lightNNint& s2);


lightNNint operator+(const lightNNint& s1, const int e);


lightNNint operator+(const int e, const lightNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNdouble operator+(const lightNNdouble& s1, const int e);
lightNNdouble operator+(const lightNNint& s1, const double e);
lightNNdouble operator+(const double e, const lightNNint& s2);
lightNNdouble operator+(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNlm_complex operator+(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator+(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator+(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator+(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator+(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator+(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator+(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator+(const double e, const lightNNlm_complex& s2);
#endif


lightNNNint operator+(const lightNNNint& s1, const lightNNNint& s2);


lightNNNint operator+(const lightNNNint& s1, const int e);


lightNNNint operator+(const int e, const lightNNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNNdouble operator+(const lightNNNdouble& s1, const int e);
lightNNNdouble operator+(const lightNNNint& s1, const double e);
lightNNNdouble operator+(const double e, const lightNNNint& s2);
lightNNNdouble operator+(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator+(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator+(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator+(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator+(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator+(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator+(const double e, const lightNNNlm_complex& s2);
#endif


lightNNNNint operator+(const lightNNNNint& s1, const lightNNNNint& s2);


lightNNNNint operator+(const lightNNNNint& s1, const int e);


lightNNNNint operator+(const int e, const lightNNNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNNNdouble operator+(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator+(const lightNNNNint& s1, const double e);
lightNNNNdouble operator+(const double e, const lightNNNNint& s2);
lightNNNNdouble operator+(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator+(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator+(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator+(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator+(const double e, const lightNNNNlm_complex& s2);
#endif

//
// operator-
//

lightNint operator-(const lightNint& s1, const lightNint& s2);


lightNint operator-(const lightNint& s1, const int e);


lightNint operator-(const int e, const lightNint& s2);

#ifdef IN_CALCdouble_I_H
lightNdouble operator-(const lightNdouble& s1, const int e);
lightNdouble operator-(const lightNint& s1, const double e);
lightNdouble operator-(const double e, const lightNint& s2);
lightNdouble operator-(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNlm_complex operator-(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator-(const lightNint& s1, const lm_complex e);
lightNlm_complex operator-(const lightNlm_complex& s1, const double e);
lightNlm_complex operator-(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator-(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator-(const int e, const lightNlm_complex& s2);
lightNlm_complex operator-(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator-(const double e, const lightNlm_complex& s2);
#endif


lightN3int operator-(const lightN3int& s1, const lightN3int& s2);


lightN3int operator-(const lightN3int& s1, const int e);


lightN3int operator-(const int e, const lightN3int& s2);


lightNNint operator-(const lightNNint& s1, const lightNNint& s2);


lightNNint operator-(const lightNNint& s1, const int e);


lightNNint operator-(const int e, const lightNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNdouble operator-(const lightNNdouble& s1, const int e);
lightNNdouble operator-(const lightNNint& s1, const double e);
lightNNdouble operator-(const double e, const lightNNint& s2);
lightNNdouble operator-(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNlm_complex operator-(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator-(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator-(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator-(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator-(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator-(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator-(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator-(const double e, const lightNNlm_complex& s2);
#endif


lightNNNint operator-(const lightNNNint& s1, const lightNNNint& s2);


lightNNNint operator-(const lightNNNint& s1, const int e);


lightNNNint operator-(const int e, const lightNNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNNdouble operator-(const lightNNNdouble& s1, const int e);
lightNNNdouble operator-(const lightNNNint& s1, const double e);
lightNNNdouble operator-(const double e, const lightNNNint& s2);
lightNNNdouble operator-(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator-(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator-(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator-(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator-(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator-(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator-(const double e, const lightNNNlm_complex& s2);

#endif



lightNNNNint operator-(const lightNNNNint& s1, const lightNNNNint& s2);


lightNNNNint operator-(const lightNNNNint& s1, const int e);


lightNNNNint operator-(const int e, const lightNNNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNNNdouble operator-(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator-(const lightNNNNint& s1, const double e);
lightNNNNdouble operator-(const double e, const lightNNNNint& s2);
lightNNNNdouble operator-(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator-(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator-(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator-(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator-(const double e, const lightNNNNlm_complex& s2);
#endif

//
// operator* (inner product)
//

lightNint operator*(const lightNint& s1, const int e);


lightNint operator*(const int e, const lightNint& s2);

#ifdef IN_CALCdouble_I_H
lightNdouble operator*(const lightNdouble& s1, const int e);
lightNdouble operator*(const lightNint& s1, const double e);
lightNdouble operator*(const double e, const lightNint& s2);
lightNdouble operator*(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNlm_complex operator*(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator*(const lightNint& s1, const lm_complex e);
lightNlm_complex operator*(const lightNlm_complex& s1, const double e);
lightNlm_complex operator*(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator*(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator*(const int e, const lightNlm_complex& s2);
lightNlm_complex operator*(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator*(const double e, const lightNlm_complex& s2);
#endif



lightNint operator*(const lightNint& s1, const lightNNint& s2);


lightNint operator*(const lightNNint& s1, const lightNint& s2);


lightNint operator*(const light3int& s1, const lightNNint& s2);


lightNint operator*(const light4int& s1, const lightNNint& s2);


lightNint operator*(const lightN3int& s1, const light3int& s2);


lightNint operator*(const lightN3int& s1, const lightNint& s2);


lightNint operator*(const lightNNint& s1, const light3int& s2);


lightNint operator*(const lightNNint& s1, const light4int& s2);


lightN3int operator*(const lightN3int& s1, const lightN3int& s2);


lightN3int operator*(const lightN3int& s1, const int e);


lightN3int operator*(const lightNNint& s1, const lightN3int& s2);


lightN3int operator*(const lightN3int& s1, const light33int& s2);


lightN3int operator*(const light44int& s1, const lightN3int& s2);


lightN3int operator*(const int e, const lightN3int& s2);


lightNNint operator*(const lightNNint& s1, const lightNNint& s2);


lightNNint operator*(const lightNNint& s1, const int e);


lightNNint operator*(const int e, const lightNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNdouble operator*(const lightNNdouble& s1, const int e);
lightNNdouble operator*(const lightNNint& s1, const double e);
lightNNdouble operator*(const double e, const lightNNint& s2);
lightNNdouble operator*(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNlm_complex operator*(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator*(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator*(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator*(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator*(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator*(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator*(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator*(const double e, const lightNNlm_complex& s2);
#endif


lightNNint operator*(const lightNNint& s1, const light33int& s2);


lightNNint operator*(const lightNNint& s1, const light44int& s2);


lightNNint operator*(const lightN3int& s1, const lightNNint& s2);


lightNNint operator*(const light33int& s1, const lightNNint& s2);


lightNNint operator*(const light44int& s1, const lightNNint& s2);


lightNNint operator*(const lightNint& s1, const lightNNNint& s2);


lightNNint operator*(const lightNNNint& s1, const lightNint& s2);


lightNNNint operator*(const lightNNNint& s1, const int e);


lightNNNint operator*(const int e, const lightNNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNNdouble operator*(const lightNNNdouble& s1, const int e);
lightNNNdouble operator*(const lightNNNint& s1, const double e);
lightNNNdouble operator*(const double e, const lightNNNint& s2);
lightNNNdouble operator*(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator*(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator*(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator*(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator*(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator*(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator*(const double e, const lightNNNlm_complex& s2);
#endif


lightNNNint operator*(const lightNNNint& s1, const lightNNint& s2);


lightNNNint operator*(const lightNNint& s1, const lightNNNint& s2);


lightNNNint operator*(const lightNNNNint& s1, const lightNint& s2);


lightNNNint operator*(const lightNint& s1, const lightNNNNint& s2);


lightNNNNint operator*(const lightNNNNint& s1, const int e);


lightNNNNint operator*(const int e, const lightNNNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNNNdouble operator*(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator*(const lightNNNNint& s1, const double e);
lightNNNNdouble operator*(const double e, const lightNNNNint& s2);
lightNNNNdouble operator*(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator*(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator*(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator*(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator*(const double e, const lightNNNNlm_complex& s2);
#endif


lightNNNNint operator*(const lightNNNNint& s1, const lightNNint& s2);


lightNNNNint operator*(const lightNNint& s1, const lightNNNNint& s2);


lightNNNNint operator*(const lightNNNint& s1, const lightNNNint& s2);

//
// operator/
//

lightNint operator/(const lightNint& s1, const int e);


lightNint operator/(const int e, const lightNint& s2);

#ifdef IN_CALCdouble_I_H
lightNdouble operator/(const lightNdouble& s1, const int e);
lightNdouble operator/(const lightNint& s1, const double e);
lightNdouble operator/(const double e, const lightNint& s2);
lightNdouble operator/(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNlm_complex operator/(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator/(const lightNint& s1, const lm_complex e);
lightNlm_complex operator/(const lightNlm_complex& s1, const double e);
lightNlm_complex operator/(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator/(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator/(const int e, const lightNlm_complex& s2);
lightNlm_complex operator/(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator/(const double e, const lightNlm_complex& s2);
#endif


lightN3int operator/(const lightN3int& s1, const int e);


lightN3int operator/(const int e, const lightN3int& s2);


lightNNint operator/(const lightNNint& s1, const int e);


lightNNint operator/(const int e, const lightNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNdouble operator/(const lightNNdouble& s1, const int e);
lightNNdouble operator/(const lightNNint& s1, const double e);
lightNNdouble operator/(const double e, const lightNNint& s2);
lightNNdouble operator/(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNlm_complex operator/(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator/(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator/(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator/(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator/(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator/(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator/(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator/(const double e, const lightNNlm_complex& s2);
#endif


lightNNNint operator/(const lightNNNint& s1, const int e);


lightNNNint operator/(const int e, const lightNNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNNdouble operator/(const lightNNNdouble& s1, const int e);
lightNNNdouble operator/(const lightNNNint& s1, const double e);
lightNNNdouble operator/(const double e, const lightNNNint& s2);
lightNNNdouble operator/(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_I_H
//lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator/(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator/(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator/(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator/(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator/(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator/(const double e, const lightNNNlm_complex& s2);
#endif


lightNNNNint operator/(const lightNNNNint& s1, const int e);


lightNNNNint operator/(const int e, const lightNNNNint& s2);

#ifdef IN_CALCdouble_I_H
lightNNNNdouble operator/(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator/(const lightNNNNint& s1, const double e);
lightNNNNdouble operator/(const double e, const lightNNNNint& s2);
lightNNNNdouble operator/(const int e, const lightNNNNdouble& s2);
#endif


#ifdef IN_CALClm_complex_I_H
//lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator/(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator/(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator/(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator/(const double e, const lightNNNNlm_complex& s2);
#endif


//
// ElemProduct
//

lightNint ElemProduct(const lightNint& s1, const lightNint& s2);


lightNNint ElemProduct(const lightNNint& s1, const lightNNint& s2);


lightN3int ElemProduct(const lightN3int& s1, const lightN3int& s2);


lightNNNint ElemProduct(const lightNNNint& s1, const lightNNNint& s2);


lightNNNNint ElemProduct(const lightNNNNint& s1, const lightNNNNint& s2);

//
// ElemQuotient
//

lightNint ElemQuotient(const lightNint& s1, const lightNint& s2);


lightNNint ElemQuotient(const lightNNint& s1, const lightNNint& s2);


lightN3int ElemQuotient(const lightN3int& s1, const lightN3int& s2);


lightNNNint ElemQuotient(const lightNNNint& s1, const lightNNNint& s2);


lightNNNNint ElemQuotient(const lightNNNNint& s1, const lightNNNNint& s2);

//
// Apply
//

lightNint Apply(const lightNint& s, int f(int));


lightNint Apply(const lightNint& s1, const lightNint& s2, int f(int, int));


lightNNint Apply(const lightNNint& s, int f(int));


lightNNint Apply(const lightNNint& s1, const lightNNint& s2, int f(int, int));


lightNNNint Apply(const lightNNNint& s, int f(int));


lightNNNint Apply(const lightNNNint& s1, const lightNNNint& s2, int f(int, int));


lightNNNNint Apply(const lightNNNNint& s, int f(int));


lightNNNNint Apply(const lightNNNNint& s1, const lightNNNNint& s2, int f(int, int));

//
// Transpose
//

lightNNint Transpose(const lightN3int& s);


lightNNint Transpose(const lightNNint& s);


lightNNNint Transpose(const lightNNNint& s);


lightNNNNint Transpose(const lightNNNNint& s);


lightNNint OuterProduct(const lightNint& s1, const lightNint& s2);

//
// pow
//

lightNint pow(const lightNint& s1, const lightNint& s2);


lightNint pow(const lightNint& s, const int e);


lightNint pow(const int e, const lightNint& s);

// Why this is needed for lightNlm_complex only ?
// This is because lightNlm_complex  accepts a (double) argument as
// constructor argument, via automatic conversion double=>integer.
// In constrast  lightNNlm_complex  accepts two (double) arguments as
// constructor argument, not just one,


lightNlm_complex pow(const lightNlm_complex& s1, const double e);

lightNlm_complex pow( const double e,const lightNlm_complex& s1);



lightN3int pow(const lightN3int& s1, const lightN3int& s2);


lightN3int pow(const lightN3int& s, const int e);


lightN3int pow(const int e, const lightN3int& s);


lightNNint pow(const lightNNint& s1, const lightNNint& s2);


lightNNint pow(const lightNNint& s, const int e);


lightNNint pow(const int e, const lightNNint& s);


lightNNNint pow(const lightNNNint& s1, const lightNNNint& s2);


lightNNNint pow(const lightNNNint& s, const int e);


lightNNNint pow(const int e, const lightNNNint& s);


lightNNNNint pow(const lightNNNNint& s1, const lightNNNNint& s2);


lightNNNNint pow(const lightNNNNint& s, const int e);


lightNNNNint pow(const int e, const lightNNNNint& s);



#include "i_light_calc_auto.h"

#undef IN_CALCint_I_H
