//           -*- c++ -*-

#define IN_CALClm_complex_C_H


//
// operator+
//

lightNlm_complex operator+(const lightNlm_complex& s1, const lightNlm_complex& s2);

//--

lightNlm_complex operator+(const lightNlm_complex& s1, const lm_complex e);


lightNlm_complex operator+(const lm_complex e, const lightNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNdouble operator+(const lightNdouble& s1, const int e);
lightNdouble operator+(const lightNint& s1, const double e);
lightNdouble operator+(const double e, const lightNint& s2);
lightNdouble operator+(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNlm_complex operator+(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator+(const lightNint& s1, const lm_complex e);
lightNlm_complex operator+(const lightNlm_complex& s1, const double e);
lightNlm_complex operator+(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator+(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator+(const int e, const lightNlm_complex& s2);
lightNlm_complex operator+(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator+(const double e, const lightNlm_complex& s2);
#endif

//--



lightN3lm_complex operator+(const lightN3lm_complex& s1, const lightN3lm_complex& s2);


lightN3lm_complex operator+(const lightN3lm_complex& s1, const lm_complex e);


lightN3lm_complex operator+(const lm_complex e, const lightN3lm_complex& s2);


lightNNlm_complex operator+(const lightNNlm_complex& s1, const lightNNlm_complex& s2);


lightNNlm_complex operator+(const lightNNlm_complex& s1, const lm_complex e);


lightNNlm_complex operator+(const lm_complex e, const lightNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNdouble operator+(const lightNNdouble& s1, const int e);
lightNNdouble operator+(const lightNNint& s1, const double e);
lightNNdouble operator+(const double e, const lightNNint& s2);
lightNNdouble operator+(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNlm_complex operator+(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator+(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator+(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator+(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator+(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator+(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator+(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator+(const double e, const lightNNlm_complex& s2);
#endif


lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const lightNNNlm_complex& s2);


lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const lm_complex e);


lightNNNlm_complex operator+(const lm_complex e, const lightNNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNNdouble operator+(const lightNNNdouble& s1, const int e);
lightNNNdouble operator+(const lightNNNint& s1, const double e);
lightNNNdouble operator+(const double e, const lightNNNint& s2);
lightNNNdouble operator+(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator+(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator+(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator+(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator+(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator+(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator+(const double e, const lightNNNlm_complex& s2);
#endif


lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const lightNNNNlm_complex& s2);


lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const lm_complex e);


lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNNNdouble operator+(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator+(const lightNNNNint& s1, const double e);
lightNNNNdouble operator+(const double e, const lightNNNNint& s2);
lightNNNNdouble operator+(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator+(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator+(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator+(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator+(const double e, const lightNNNNlm_complex& s2);
#endif

//
// operator-
//

lightNlm_complex operator-(const lightNlm_complex& s1, const lightNlm_complex& s2);


lightNlm_complex operator-(const lightNlm_complex& s1, const lm_complex e);


lightNlm_complex operator-(const lm_complex e, const lightNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNdouble operator-(const lightNdouble& s1, const int e);
lightNdouble operator-(const lightNint& s1, const double e);
lightNdouble operator-(const double e, const lightNint& s2);
lightNdouble operator-(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNlm_complex operator-(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator-(const lightNint& s1, const lm_complex e);
lightNlm_complex operator-(const lightNlm_complex& s1, const double e);
lightNlm_complex operator-(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator-(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator-(const int e, const lightNlm_complex& s2);
lightNlm_complex operator-(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator-(const double e, const lightNlm_complex& s2);
#endif


lightN3lm_complex operator-(const lightN3lm_complex& s1, const lightN3lm_complex& s2);


lightN3lm_complex operator-(const lightN3lm_complex& s1, const lm_complex e);


lightN3lm_complex operator-(const lm_complex e, const lightN3lm_complex& s2);


lightNNlm_complex operator-(const lightNNlm_complex& s1, const lightNNlm_complex& s2);


lightNNlm_complex operator-(const lightNNlm_complex& s1, const lm_complex e);


lightNNlm_complex operator-(const lm_complex e, const lightNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNdouble operator-(const lightNNdouble& s1, const int e);
lightNNdouble operator-(const lightNNint& s1, const double e);
lightNNdouble operator-(const double e, const lightNNint& s2);
lightNNdouble operator-(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNlm_complex operator-(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator-(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator-(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator-(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator-(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator-(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator-(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator-(const double e, const lightNNlm_complex& s2);
#endif


lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const lightNNNlm_complex& s2);


lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const lm_complex e);


lightNNNlm_complex operator-(const lm_complex e, const lightNNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNNdouble operator-(const lightNNNdouble& s1, const int e);
lightNNNdouble operator-(const lightNNNint& s1, const double e);
lightNNNdouble operator-(const double e, const lightNNNint& s2);
lightNNNdouble operator-(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator-(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator-(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator-(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator-(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator-(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator-(const double e, const lightNNNlm_complex& s2);

#endif



lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const lightNNNNlm_complex& s2);


lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const lm_complex e);


lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNNNdouble operator-(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator-(const lightNNNNint& s1, const double e);
lightNNNNdouble operator-(const double e, const lightNNNNint& s2);
lightNNNNdouble operator-(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator-(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator-(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator-(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator-(const double e, const lightNNNNlm_complex& s2);
#endif

//
// operator* (inner product)
//

lightNlm_complex operator*(const lightNlm_complex& s1, const lm_complex e);


lightNlm_complex operator*(const lm_complex e, const lightNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNdouble operator*(const lightNdouble& s1, const int e);
lightNdouble operator*(const lightNint& s1, const double e);
lightNdouble operator*(const double e, const lightNint& s2);
lightNdouble operator*(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNlm_complex operator*(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator*(const lightNint& s1, const lm_complex e);
lightNlm_complex operator*(const lightNlm_complex& s1, const double e);
lightNlm_complex operator*(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator*(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator*(const int e, const lightNlm_complex& s2);
lightNlm_complex operator*(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator*(const double e, const lightNlm_complex& s2);
#endif



lightNlm_complex operator*(const lightNlm_complex& s1, const lightNNlm_complex& s2);


lightNlm_complex operator*(const lightNNlm_complex& s1, const lightNlm_complex& s2);


lightNlm_complex operator*(const light3lm_complex& s1, const lightNNlm_complex& s2);


lightNlm_complex operator*(const light4lm_complex& s1, const lightNNlm_complex& s2);


lightNlm_complex operator*(const lightN3lm_complex& s1, const light3lm_complex& s2);


lightNlm_complex operator*(const lightN3lm_complex& s1, const lightNlm_complex& s2);


lightNlm_complex operator*(const lightNNlm_complex& s1, const light3lm_complex& s2);


lightNlm_complex operator*(const lightNNlm_complex& s1, const light4lm_complex& s2);


lightN3lm_complex operator*(const lightN3lm_complex& s1, const lightN3lm_complex& s2);


lightN3lm_complex operator*(const lightN3lm_complex& s1, const lm_complex e);


lightN3lm_complex operator*(const lightNNlm_complex& s1, const lightN3lm_complex& s2);


lightN3lm_complex operator*(const lightN3lm_complex& s1, const light33lm_complex& s2);


lightN3lm_complex operator*(const light44lm_complex& s1, const lightN3lm_complex& s2);


lightN3lm_complex operator*(const lm_complex e, const lightN3lm_complex& s2);


lightNNlm_complex operator*(const lightNNlm_complex& s1, const lightNNlm_complex& s2);


lightNNlm_complex operator*(const lightNNlm_complex& s1, const lm_complex e);


lightNNlm_complex operator*(const lm_complex e, const lightNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNdouble operator*(const lightNNdouble& s1, const int e);
lightNNdouble operator*(const lightNNint& s1, const double e);
lightNNdouble operator*(const double e, const lightNNint& s2);
lightNNdouble operator*(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNlm_complex operator*(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator*(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator*(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator*(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator*(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator*(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator*(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator*(const double e, const lightNNlm_complex& s2);
#endif


lightNNlm_complex operator*(const lightNNlm_complex& s1, const light33lm_complex& s2);


lightNNlm_complex operator*(const lightNNlm_complex& s1, const light44lm_complex& s2);


lightNNlm_complex operator*(const lightN3lm_complex& s1, const lightNNlm_complex& s2);


lightNNlm_complex operator*(const light33lm_complex& s1, const lightNNlm_complex& s2);


lightNNlm_complex operator*(const light44lm_complex& s1, const lightNNlm_complex& s2);


lightNNlm_complex operator*(const lightNlm_complex& s1, const lightNNNlm_complex& s2);


lightNNlm_complex operator*(const lightNNNlm_complex& s1, const lightNlm_complex& s2);


lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const lm_complex e);


lightNNNlm_complex operator*(const lm_complex e, const lightNNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNNdouble operator*(const lightNNNdouble& s1, const int e);
lightNNNdouble operator*(const lightNNNint& s1, const double e);
lightNNNdouble operator*(const double e, const lightNNNint& s2);
lightNNNdouble operator*(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator*(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator*(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator*(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator*(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator*(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator*(const double e, const lightNNNlm_complex& s2);
#endif


lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const lightNNlm_complex& s2);


lightNNNlm_complex operator*(const lightNNlm_complex& s1, const lightNNNlm_complex& s2);


lightNNNlm_complex operator*(const lightNNNNlm_complex& s1, const lightNlm_complex& s2);


lightNNNlm_complex operator*(const lightNlm_complex& s1, const lightNNNNlm_complex& s2);


lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const lm_complex e);


lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNNNdouble operator*(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator*(const lightNNNNint& s1, const double e);
lightNNNNdouble operator*(const double e, const lightNNNNint& s2);
lightNNNNdouble operator*(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator*(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator*(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator*(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator*(const double e, const lightNNNNlm_complex& s2);
#endif


lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const lightNNlm_complex& s2);


lightNNNNlm_complex operator*(const lightNNlm_complex& s1, const lightNNNNlm_complex& s2);


lightNNNNlm_complex operator*(const lightNNNlm_complex& s1, const lightNNNlm_complex& s2);

//
// operator/
//

lightNlm_complex operator/(const lightNlm_complex& s1, const lm_complex e);


lightNlm_complex operator/(const lm_complex e, const lightNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNdouble operator/(const lightNdouble& s1, const int e);
lightNdouble operator/(const lightNint& s1, const double e);
lightNdouble operator/(const double e, const lightNint& s2);
lightNdouble operator/(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNlm_complex operator/(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator/(const lightNint& s1, const lm_complex e);
lightNlm_complex operator/(const lightNlm_complex& s1, const double e);
lightNlm_complex operator/(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator/(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator/(const int e, const lightNlm_complex& s2);
lightNlm_complex operator/(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator/(const double e, const lightNlm_complex& s2);
#endif


lightN3lm_complex operator/(const lightN3lm_complex& s1, const lm_complex e);


lightN3lm_complex operator/(const lm_complex e, const lightN3lm_complex& s2);


lightNNlm_complex operator/(const lightNNlm_complex& s1, const lm_complex e);


lightNNlm_complex operator/(const lm_complex e, const lightNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNdouble operator/(const lightNNdouble& s1, const int e);
lightNNdouble operator/(const lightNNint& s1, const double e);
lightNNdouble operator/(const double e, const lightNNint& s2);
lightNNdouble operator/(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNlm_complex operator/(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator/(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator/(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator/(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator/(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator/(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator/(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator/(const double e, const lightNNlm_complex& s2);
#endif


lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const lm_complex e);


lightNNNlm_complex operator/(const lm_complex e, const lightNNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNNdouble operator/(const lightNNNdouble& s1, const int e);
lightNNNdouble operator/(const lightNNNint& s1, const double e);
lightNNNdouble operator/(const double e, const lightNNNint& s2);
lightNNNdouble operator/(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_C_H
//lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator/(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator/(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator/(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator/(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator/(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator/(const double e, const lightNNNlm_complex& s2);
#endif


lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const lm_complex e);


lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNlm_complex& s2);

#ifdef IN_CALCdouble_C_H
lightNNNNdouble operator/(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator/(const lightNNNNint& s1, const double e);
lightNNNNdouble operator/(const double e, const lightNNNNint& s2);
lightNNNNdouble operator/(const int e, const lightNNNNdouble& s2);
#endif


#ifdef IN_CALClm_complex_C_H
//lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator/(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator/(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator/(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator/(const double e, const lightNNNNlm_complex& s2);
#endif


//
// ElemProduct
//

lightNlm_complex ElemProduct(const lightNlm_complex& s1, const lightNlm_complex& s2);


lightNNlm_complex ElemProduct(const lightNNlm_complex& s1, const lightNNlm_complex& s2);


lightN3lm_complex ElemProduct(const lightN3lm_complex& s1, const lightN3lm_complex& s2);


lightNNNlm_complex ElemProduct(const lightNNNlm_complex& s1, const lightNNNlm_complex& s2);


lightNNNNlm_complex ElemProduct(const lightNNNNlm_complex& s1, const lightNNNNlm_complex& s2);

//
// ElemQuotient
//

lightNlm_complex ElemQuotient(const lightNlm_complex& s1, const lightNlm_complex& s2);


lightNNlm_complex ElemQuotient(const lightNNlm_complex& s1, const lightNNlm_complex& s2);


lightN3lm_complex ElemQuotient(const lightN3lm_complex& s1, const lightN3lm_complex& s2);


lightNNNlm_complex ElemQuotient(const lightNNNlm_complex& s1, const lightNNNlm_complex& s2);


lightNNNNlm_complex ElemQuotient(const lightNNNNlm_complex& s1, const lightNNNNlm_complex& s2);

//
// Apply
//

lightNlm_complex Apply(const lightNlm_complex& s, lm_complex f(lm_complex));


lightNlm_complex Apply(const lightNlm_complex& s1, const lightNlm_complex& s2, lm_complex f(lm_complex, lm_complex));


lightNNlm_complex Apply(const lightNNlm_complex& s, lm_complex f(lm_complex));


lightNNlm_complex Apply(const lightNNlm_complex& s1, const lightNNlm_complex& s2, lm_complex f(lm_complex, lm_complex));


lightNNNlm_complex Apply(const lightNNNlm_complex& s, lm_complex f(lm_complex));


lightNNNlm_complex Apply(const lightNNNlm_complex& s1, const lightNNNlm_complex& s2, lm_complex f(lm_complex, lm_complex));


lightNNNNlm_complex Apply(const lightNNNNlm_complex& s, lm_complex f(lm_complex));


lightNNNNlm_complex Apply(const lightNNNNlm_complex& s1, const lightNNNNlm_complex& s2, lm_complex f(lm_complex, lm_complex));

//
// Transpose
//

lightNNlm_complex Transpose(const lightN3lm_complex& s);


lightNNlm_complex Transpose(const lightNNlm_complex& s);


lightNNNlm_complex Transpose(const lightNNNlm_complex& s);


lightNNNNlm_complex Transpose(const lightNNNNlm_complex& s);


lightNNlm_complex OuterProduct(const lightNlm_complex& s1, const lightNlm_complex& s2);

//
// pow
//

lightNlm_complex pow(const lightNlm_complex& s1, const lightNlm_complex& s2);


lightNlm_complex pow(const lightNlm_complex& s, const lm_complex e);


lightNlm_complex pow(const lm_complex e, const lightNlm_complex& s);

// Why this is needed for lightNlm_complex only ?
// This is because lightNlm_complex  accepts a (double) argument as
// constructor argument, via automatic conversion double=>integer.
// In constrast  lightNNlm_complex  accepts two (double) arguments as
// constructor argument, not just one,


lightNlm_complex pow(const lightNlm_complex& s1, const double e);

lightNlm_complex pow( const double e,const lightNlm_complex& s1);



lightN3lm_complex pow(const lightN3lm_complex& s1, const lightN3lm_complex& s2);


lightN3lm_complex pow(const lightN3lm_complex& s, const lm_complex e);


lightN3lm_complex pow(const lm_complex e, const lightN3lm_complex& s);


lightNNlm_complex pow(const lightNNlm_complex& s1, const lightNNlm_complex& s2);


lightNNlm_complex pow(const lightNNlm_complex& s, const lm_complex e);


lightNNlm_complex pow(const lm_complex e, const lightNNlm_complex& s);


lightNNNlm_complex pow(const lightNNNlm_complex& s1, const lightNNNlm_complex& s2);


lightNNNlm_complex pow(const lightNNNlm_complex& s, const lm_complex e);


lightNNNlm_complex pow(const lm_complex e, const lightNNNlm_complex& s);


lightNNNNlm_complex pow(const lightNNNNlm_complex& s1, const lightNNNNlm_complex& s2);


lightNNNNlm_complex pow(const lightNNNNlm_complex& s, const lm_complex e);


lightNNNNlm_complex pow(const lm_complex e, const lightNNNNlm_complex& s);



#include "c_light_calc_auto.h"

#undef IN_CALClm_complex_C_H
