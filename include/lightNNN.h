//           -*- c++ -*-

#ifndef LIGHTNNN_H
#define LIGHTNNN_H
// <cd> lightNNN
//
// .SS Functionality
//
// lightNNN is a template for classes that implement tensors of rank 3
// with any number of elements. E.g. a lightNNN s with 5x6x7 elements of
// type double can be instanciated with:
//
// <code>lightNNN&lt;double&gt; s(5,6,7);</code>
//
// The size of the tensor can change during execution. It changes its
// size to whatever is needed when it is assigned a new value with an
// assignment operator.
//
// When a lightNNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNNN object with 5x6x5 elements to one with 5x5x6 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHTNNN<T>_H

#define LIGHTNNN_SIZE1 5
#define LIGHTNNN_SIZE2 5
#define LIGHTNNN_SIZE3 5
#define LIGHTNNN_SIZE (LIGHTNNN_SIZE1*LIGHTNNN_SIZE2*LIGHTNNN_SIZE3)
// The default size.

template<class T> class lightN;
template<class T> class lightNN;
template<class T> class lightN33;
template<class T> class lightNNNN;

template<class T>
class lightNNN {
public:

#ifdef IN_LIGHTNNNdouble_H
  friend class lightNNN<int>;
#else
  friend class lightNNN<double>;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNN<int>;
#endif

  #include "lightNNN_auto.h"

  inline lightNNN();
  // Default constructor.

  inline lightNNN(const lightNNN<T>&);
  // Copy constructor.

  inline lightNNN(const lightN33<T>&);
  // Conversion.

  inline lightNNN(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNN(const int, const int, const int);
  // Construct a lightNNN of given size.

  inline lightNNN(const int, const int, const int, const T *);
  // Construct a lightNNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNN(const int, const int, const int, const T);
  // Construct a lightNNN of given size and initialize all elements
  // with a value (the last argument).

  inline ~lightNNN();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNN<double>();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNN<lm_complex>();
  // Convert to complex.
  
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNN<lm_complex>();
  // Convert to complex.
#endif

  lightNNN<T>& operator=(const lightNNN<T>&);
  // Assignment.

  lightNNN<T>& operator=(const lightN33<T>&);
  // Assignment, change size to Nx3x3.

  lightNNN<T>& operator=(const T);
  // Assign one value to all elements.

  T operator() (const int x, const int y, const int z) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3));
#ifdef ROWMAJOR
    return elem[((x-1)*size2+(y-1))*size3+z-1];
#else
    return elem[((z-1)*size2+(y-1))*size1+x-1];
#endif
  };
  // Get the value of one element.


  T& operator()(const int x, const int y, const int z) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) || (z<1) || (z>size3));
#ifdef ROWMAJOR
    return elem[((x-1)*size2+(y-1))*size3+z-1];
#else
    return elem[((z-1)*size2+(y-1))*size1+x-1];
#endif
  };
  // Get/Set the value of one element.

  lightN<T> operator()(const int, const int) const;
  // Get the value of a vector in the tensor.

  lightNN<T> operator()(const int) const;
  // Get the value of a matrix in the tensor.




  //
  // Set
  //

  inline lightNNN<T>& Set (const int i0, const int i1, const T val);
  inline lightNNN<T>& Set (const int i0, const int i1, const lightN<T>& arr);
  inline lightNNN<T>& Set (const int i0, const int i1, const light3<T>& arr);
  inline lightNNN<T>& Set (const int i0, const int i1, const light4<T>& arr);

  inline lightNNN<T>& Set (const int i0, const T val);
  inline lightNNN<T>& Set (const int i0, const lightNN<T>& arr);
  inline lightNNN<T>& Set (const int i0, const light33<T>& arr);
  inline lightNNN<T>& Set (const int i0, const light44<T>& arr);


 
  int operator==(const lightNNN<T>&) const;
  // Equality.

  int operator!=(const lightNNN<T>&) const;
  // Inequality.
 
  lightNNN<T>& operator+=(const T);
  // Add a value to all elements.

  lightNNN<T>& operator+=(const lightNNN<T>&);
  // Elementwise addition.

  lightNNN<T>& operator-=(const T);
  // Subtract a value from all elements.

  lightNNN<T>& operator-=(const lightNNN<T>&);
  // Elementwise subtraction.

  lightNNN<T>& operator*=(const T);
  // Mulitply all elements with a value.

  lightNNN<T>& operator/=(const T);
  // Divide all elements with a value.

  T Extract(const lightN<int>&) const;
  // Extract an element using given index. 

  inline lightNNN<T>& reshape(const int, const int, const int, const lightN<T>&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c) will be set to s(a) in the lightNNN
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector. The program may dump
  // core or behave strangely if the arguments are incorrect.

  inline lightNNN<T>& reshape(const int, const int, const int, const lightNN<T>&);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c) will be set to s(a,b) in the
  // lightNNN object. The values of the first two arguments must be
  // the same as the size of the lightNN-matrix. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNN<T>& SetShape(const int x=-1, const int y=-1, const int z=-1); 
  // Set new shape for the tensor.

  const  lightNN<T>& SetMatrix1(const int n,const lightNN<T>& x);
  // Assigns Tensor[n,_,_]=Matrix

  const  lightN<T>& SetVector12(const int n,const int m,const lightN<T>& x);
  // Assigns Tensor[n,m,_]=Vector 

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else if(x == 3)
      return size3;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(T *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const T *);
  // Set values of all elements from array (row major order).
 
  T * data() const;
  // Direct access to the stored data. Use the result with care.

  lightNNN<T> operator+() const;
  // Unary plus.

  lightNNN<T> operator-() const;
  // Unary minus.

  friend inline lightNNN<T> operator+(const lightNNN<T>&, const lightNNN<T>&);
  // Elementwise addition.

  friend inline lightNNN<T> operator+(const lightNNN<T>&, const T);
  // Addition to all elements.

  friend inline lightNNN<T> operator+(const T, const lightNNN<T>&);
  // Addition to all elements.

#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNN<double> operator+(const lightNNN<double>&, const int);
  friend inline lightNNN<double> operator+(const lightNNN<int>&, const double);
  friend inline lightNNN<double> operator+(const double, const lightNNN<int>&);
  friend inline lightNNN<double> operator+(const int, const lightNNN<double>&);
#endif

  #ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator+(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator+(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator+(const double e, const lightNNNlm_complex& s2);
#endif

  friend inline lightNNN<T> operator-(const lightNNN<T>&, const lightNNN<T>&);
  // Elementwise subtraction.

  friend inline lightNNN<T> operator-(const lightNNN<T>&, const T);
  // Subtraction from all elements.

  friend inline lightNNN<T> operator-(const T, const lightNNN<T>&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNN<double> operator-(const lightNNN<double>&, const int);
  friend inline lightNNN<double> operator-(const lightNNN<int>&, const double);
  friend inline lightNNN<double> operator-(const double, const lightNNN<int>&);
  friend inline lightNNN<double> operator-(const int, const lightNNN<double>&);
#endif

  #ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator-(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator-(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator-(const double e, const lightNNNlm_complex& s2);

#endif

  friend inline lightNNN<T> operator*(const lightNNN<T>&, const T);
  // Multiply all elements.

  friend inline lightNNN<T> operator*(const T, const lightNNN<T>&);
  // Multiply all elements.

#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNN<double> operator*(const lightNNN<double>&, const int);
  friend inline lightNNN<double> operator*(const lightNNN<int>&, const double);
  friend inline lightNNN<double> operator*(const double, const lightNNN<int>&);
  friend inline lightNNN<double> operator*(const int, const lightNNN<double>&);
#endif

#ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator*(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator*(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator*(const double e, const lightNNNlm_complex& s2);
#endif

  friend inline lightNNN<T> operator*(const lightNNN<T>&, const lightNN<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const lightNN<T>&, const lightNNN<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const lightNNNN<T>&, const lightN<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const lightN<T>&,	const lightNNNN<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const lightNNN<T>&, const light33<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const light33<T>&, const lightNNN<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const lightNNNN<T>&, const light3<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const light3<T>&, const lightNNNN<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const lightNNN<T>&, const light44<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const light44<T>&, const lightNNN<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const lightNNNN<T>&, const light4<T>&);
  // Inner product.

  friend inline lightNNN<T> operator*(const light4<T>&, const lightNNNN<T>&);
  // Inner product.

  friend inline lightNNN<T> operator/(const lightNNN<T>&, const T);
  // Divide all elements.

  friend inline lightNNN<T> operator/(const T, const lightNNN<T>&);
  // Divide all elements.

#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNN<double> operator/(const lightNNN<double>&, const int);
  friend inline lightNNN<double> operator/(const lightNNN<int>&, const double);
  friend inline lightNNN<double> operator/(const double, const lightNNN<int>&);
  friend inline lightNNN<double> operator/(const int, const lightNNN<double>&);
#endif

#ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator/(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator/(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator/(const double e, const lightNNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNdouble arg(const lightNNNlm_complex& a);
friend inline lightNNNdouble re(const lightNNNlm_complex& a);
friend inline lightNNNdouble im(const lightNNNlm_complex& a);
friend inline lightNNNlm_complex conjugate(const lightNNNlm_complex& a);
//#endif

  friend inline lightNNN<T> pow(const lightNNN<T>&, const lightNNN<T>&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNN<T> pow(const lightNNN<T>&, const T);
  // Raise to the power of-function, for all elements.

  friend inline lightNNN<T> pow(const T, const lightNNN<T>&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNN<T> ElemProduct(const lightNNN<T>&, const lightNNN<T>&);
  // Elementwise multiplication.

  friend inline lightNNN<T> ElemQuotient(const lightNNN<T>&, const lightNNN<T>&);
  // Elementwise division.

  friend inline lightNNN<T> Apply(const lightNNN<T>&, T f(T));
  // Apply the function elementwise all elements.

  friend inline lightNNN<T> Apply(const lightNNN<T>&, const lightNNN<T>&, T f(T, T));
  // Apply the function elementwise on all elements in the two tensors.

  friend inline lightNNN<T> Transpose(const lightNNN<T>&);
  // Transpose.

#ifdef IN_LIGHTNNNlm_complex_H
#else
  friend inline lightNNN<T> abs(const lightNNN<T>&);
#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNN<double> abs(const lightNNN<lm_complex>&);
#endif 
#endif


#ifndef COMPLEX_TOOLS
  // sign
  friend inline lightNNN<int> sign(const lightNNN<int>&);
  // sign
  friend inline lightNNN<int> sign(const lightNNN<double>&);
  // abs
#else
  friend inline lightNNNlm_complex sign(const lightNNNlm_complex&);
#endif

 /// Added 2/2/98 

  friend inline lightNNN<T>  FractionalPart(const lightNNN<T>&);
  //  FractionalPart

  friend inline lightNNN<T>  IntegerPart(const lightNNN<T>&);
  //  IntegerPart

  //friend inline lightNNN<int>  IntegerPart(const lightNNN<double>&);
  //  IntegerPart

  friend inline lightNNN<T> Mod (const  lightNNN<T>&,const  lightNNN<T>&);
  // Mod
 
  friend inline lightNNN<T> Mod (const  lightNNN<T>&,const T);
  // Mod

  friend inline lightNNN<T> Mod (const T,const  lightNNN<T>&);
  // Mod

  friend inline T LightMax (const lightNNN<T>& );
  // Max

  friend inline T LightMin (const lightNNN<T>& );
  // Min

  friend inline T findLightMax (const  T *,const  int );
  // Find Max
 
  friend inline T findLightMin (const  T *,const  int );
  // Find Min
 
  /// End Added 



  friend inline lightNNN<int> ifloor(const lightNNN<double>&);
  // ifloor

  friend inline lightNNN<int> iceil(const lightNNN<double>&);
  // iceil

  friend inline lightNNN<int> irint(const lightNNN<double>&);
  // irint

  friend inline lightNNN<double> sqrt(const lightNNN<double>&);
  // sqrt

  friend inline lightNNN<double> exp(const lightNNN<double>&);
  // exp

  friend inline lightNNN<double> log(const lightNNN<double>&);
  // log

  friend inline lightNNN<double> sin(const lightNNN<double>&);
  // sin

  friend inline lightNNN<double> cos(const lightNNN<double>&);
  // cos

  friend inline lightNNN<double> tan(const lightNNN<double>&);
  // tan

  friend inline lightNNN<double> asin(const lightNNN<double>&);
  // asin

  friend inline lightNNN<double> acos(const lightNNN<double>&);
  // acos

  friend inline lightNNN<double> atan(const lightNNN<double>&);
  // atan

  friend inline lightNNN<double> sinh(const lightNNN<double>&);
  // sinh

  friend inline lightNNN<double> cosh(const lightNNN<double>&);
  // cosh

  friend inline lightNNN<double> tanh(const lightNNN<double>&);
  // tanh

  friend inline lightNNN<double> asinh(const lightNNN<double>&);
  // asinh

  friend inline lightNNN<double> acosh(const lightNNN<double>&);
  // acosh

  friend inline lightNNN<double> atanh(const lightNNN<double>&);
  // atanh

   friend inline lightNNN<lm_complex> ifloor(const lightNNN<lm_complex>&);
  // ifloor

  friend inline lightNNN<lm_complex> iceil(const lightNNN<lm_complex>&);
  // iceil

  friend inline lightNNN<lm_complex> irint(const lightNNN<lm_complex>&);
  // irint

  friend inline lightNNN<lm_complex> sqrt(const lightNNN<lm_complex>&);
  // sqrt

  friend inline lightNNN<lm_complex> exp(const lightNNN<lm_complex>&);
  // exp

  friend inline lightNNN<lm_complex> log(const lightNNN<lm_complex>&);
  // log

  friend inline lightNNN<lm_complex> sin(const lightNNN<lm_complex>&);
  // sin

  friend inline lightNNN<lm_complex> cos(const lightNNN<lm_complex>&);
  // cos

  friend inline lightNNN<lm_complex> tan(const lightNNN<lm_complex>&);
  // tan

  friend inline lightNNN<lm_complex> asin(const lightNNN<lm_complex>&);
  // asin

  friend inline lightNNN<lm_complex> acos(const lightNNN<lm_complex>&);
  // acos

  friend inline lightNNN<lm_complex> atan(const lightNNN<lm_complex>&);
  // atan

  friend inline lightNNN<lm_complex> sinh(const lightNNN<lm_complex>&);
  // sinh

  friend inline lightNNN<lm_complex> cosh(const lightNNN<lm_complex>&);
  // cosh

  friend inline lightNNN<lm_complex> tanh(const lightNNN<lm_complex>&);
  // tanh

  friend inline lightNNN<lm_complex> asinh(const lightNNN<lm_complex>&);
  // asinh

  friend inline lightNNN<lm_complex> acosh(const lightNNN<lm_complex>&);
  // acosh

  friend inline lightNNN<lm_complex> atanh(const lightNNN<lm_complex>&);
  // atanh

#ifdef IN_LIGHTNNNint_H
  friend inline lightNN<double> light_mean     ( const  lightNNN<T>& );
  friend inline lightNN<double> light_standard_deviation( const  lightNNN<T>& );
  friend inline lightNN<double> light_variance ( const  lightNNN<T>& );
  friend inline lightNN<double> light_median   ( const  lightNNN<T>& );
#else
  friend inline lightNN<T> light_mean     ( const  lightNNN<T>& );
  friend inline lightNN<T> light_variance ( const  lightNNN<T>& );
  friend inline lightNN<T> light_standard_deviation( const  lightNNN<T>& );
  friend inline lightNN<T> light_median   ( const  lightNNN<T>& );
#endif
  
  friend inline lightNN<T> light_total    ( const  lightNNN<T>& );
  
  friend inline lightNNN<T> light_sort (const lightNNN<T> arr1);
  
  friend inline lightN<T> light_flatten (const lightNNN<T> s);
  friend inline lightN<T> light_flatten (const lightNNN<T> s, int level);
  friend inline lightNNN<T> light_flatten0 (const lightNNN<T> s);
  friend inline lightNN<T> light_flatten1 (const lightNNN<T> s);

  //<ignore>
#ifdef IN_LIGHTNNNlm_complex_H
  friend class lightNNN<int>;
#endif

  friend class lightN<T>;
  friend class lightNN<T>;
  friend class lightN33<T>;
  friend class lightNNNN<T>;

  friend inline lightNNN<T> light_join (const lightNNN<T>, const lightNNN<T>);
  friend inline lightNNN<T> light_drop (const lightNNN<T> arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNNdouble::lightNNN(const lightNNNint& s1, const double e, const lightmat_plus_enum)

  T *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // Size of the first index.

  int size2;
  // Size of the second index.

  int size3;
  // Size of the third index.

protected:
  T sarea[LIGHTNNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNNN(const int, const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // three arguments is the size of the created tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNN(const lightNNN<T>&, const lightNNN<T>&, const lightmat_plus_enum);
  lightNNN(const lightNNN<T>&, const T, const lightmat_plus_enum);

#ifdef IN_LIGHTNNNdouble_H
  lightNNN(const lightNNNdouble&, const int, const lightmat_plus_enum);
  lightNNN(const lightNNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNN(const lightNNN<T>&, const lightNNN<T>&, const lightmat_minus_enum);
  lightNNN(const T, const lightNNN<T>&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNNdouble_H
  lightNNN(const int, const lightNNNdouble&, const lightmat_minus_enum);
  lightNNN(const double, const lightNNNint&, const lightmat_minus_enum);
#endif

  lightNNN(const lightNNN<T>&, const T, const lightmat_mult_enum);

#ifdef IN_LIGHTNNNdouble_H
  lightNNN(const lightNNNdouble&, const int, const lightmat_mult_enum);
  lightNNN(const lightNNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNN(const lightNNN<T>&, const lightNN<T>&, const lightmat_mult_enum);
  lightNNN(const lightNN<T>&, const lightNNN<T>&, const lightmat_mult_enum);
  lightNNN(const lightNNNN<T>&, const lightN<T>&, const lightmat_mult_enum);
  lightNNN(const lightN<T>&, const lightNNNN<T>&, const lightmat_mult_enum);
  lightNNN(const lightNNN<T>&, const light33<T>&, const lightmat_mult_enum);
  lightNNN(const light33<T>&, const lightNNN<T>&, const lightmat_mult_enum);
  lightNNN(const lightNNNN<T>&, const light3<T>&, const lightmat_mult_enum);
  lightNNN(const light3<T>&, const lightNNNN<T>&, const lightmat_mult_enum);
  lightNNN(const lightNNN<T>&, const light44<T>&, const lightmat_mult_enum);
  lightNNN(const light44<T>&, const lightNNN<T>&, const lightmat_mult_enum);
  lightNNN(const lightNNNN<T>&, const light4<T>&, const lightmat_mult_enum);
  lightNNN(const light4<T>&, const lightNNNN<T>&, const lightmat_mult_enum);
  lightNNN(const T, const lightNNN<T>&, const lightmat_div_enum);

#ifdef IN_LIGHTNNNdouble_H
  lightNNN(const int, const lightNNNdouble&, const lightmat_div_enum);
  lightNNN(const double, const lightNNNint&, const lightmat_div_enum);
#endif

  lightNNN(const lightNNN<T>&, const lightNNN<T>&, const lightmat_pow_enum);
  lightNNN(const lightNNN<T>&, const T, const lightmat_pow_enum);
  lightNNN(const T, const lightNNN<T>&, const lightmat_pow_enum);

#ifdef IN_LIGHTNNNlm_complex_H
 #else
  #ifdef IN_LIGHTNNNdouble_H
   lightNNN(const lightNNN<T>&, const lightmat_abs_enum);
   lightNNN(const lightNNN<lm_complex>&, const lightmat_abs_enum);
  #else
  lightNNN(const lightNNN<T>&, const lightmat_abs_enum);
  #endif
#endif


  lightNNN(const lightNNN<T>&, const lightNNN<T>&, const lightmat_eprod_enum);
  lightNNN(const lightNNN<T>&, const lightNNN<T>&, const lightmat_equot_enum);
  lightNNN(const lightNNN<T>&, T f(T), const lightmat_apply_enum);
  lightNNN(const lightNNN<T>&, const lightNNN<T>&, T f(T, T), const lightmat_apply_enum);
  lightNNN(const lightNNN<T>&, const lightmat_trans_enum);
};

typedef lightNNN<double> doubleNNN;
typedef lightNNN<int> intNNN;
typedef lightNNN<lm_complex> lm_complexNNN;

#undef IN_LIGHTNNN<T>_H

#endif
