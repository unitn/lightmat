//           -*- c++ -*-

#ifndef LIGHT3_C_H
#define LIGHT3_C_H
// <cd> light3lm_complex
//
// .SS Functionality
//
// light3 is a template for classes that implement vectors with 3
// elements. E.g. a vector v with 3 elements of type double can be
// instanciated with:
//
// <code>light3&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#define IN_LIGHT3lm_complex_C_H

 class light4;

 class lightN;
 class light33;
 class lightN3;
 class lightN33;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light3lm_complex {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light3double();
  // Convert to double.
#else
  friend class lightN3int;
  friend class lightN33int;
#endif
#ifdef LIGHTMAT_TEMPLATES
  //  friend class light3int; 
  friend class lightN3int;
  friend class lightN33int;
#endif

#ifdef IN_LIGHT3double_C_H
  friend class light3int; 
#else
  friend class light3double; 
#endif

  #include "c_light3_auto.h"

  light3lm_complex();
  // Default constructor.

  light3lm_complex(const light3lm_complex&);
  // Copy constructor.

  light3lm_complex(const lm_complex, const lm_complex, const lm_complex);
  // Initialize elements with values.

  light3lm_complex(const lm_complex *);
  // Initialize elements with values from an array.

  light3lm_complex(const lm_complex);
  // Initialize all elements with the same value.

  light3lm_complex& operator=(const light3lm_complex&);
  // Assignment.

  light3lm_complex& operator=(const lightNlm_complex&);
  // Assignment from a lightN where N=3.

  light3lm_complex& operator=(const lm_complex);
  // Assign one value to all elements.

  lm_complex operator()(const int x) const {
    limiterror((x<1) || (x>3));
    return elem[x-1];
  };
  // Get the value of one element.

  lm_complex& operator()(const int x) {
    limiterror((x<1) || (x>3));
    return elem[x-1];
  };
  // Get/Set the value of one element.


  int operator==(const light3lm_complex&) const;
  // Equality.

  int operator!=(const light3lm_complex&) const;
  // Inequality.

  light3lm_complex& operator+=(const lm_complex);
  // Add a value to all elements.

  light3lm_complex& operator+=(const light3lm_complex&);
  // Elementwise addition.

  light3lm_complex& operator-=(const lm_complex);
  // Subtract a value from all elements.

  light3lm_complex& operator-=(const light3lm_complex&);
  // Elementwise subtraction.

  light3lm_complex& operator*=(const lm_complex);
  // Muliply all elements with a value.

  light3lm_complex& operator/=(const lm_complex);
  // Divide all elements with a value.

  #ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
  #endif
  int dimension(const int x = 1) const {
    if (x == 1)
      return 3;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 3). If this was a lightNlm_complex
  // then dimension(1) would return the number of elements.
 
  #ifndef COMPLEX_TOOLS
  light3lm_complex& normalize();
  // Normalize vector.
  #endif

  void Get(lm_complex *) const;
  // Get values of all elements and put them in an array (3 elements long).

  void Set(const lm_complex *);
  // Set values of all elements from array (3 elements long).

  void Get( lm_complex&, lm_complex&, lm_complex&) const;
  // Get the value of the three elements.

  void Set(const lm_complex, const lm_complex, const lm_complex);
  // Set the value of the three elements.

  light3lm_complex operator+() const;
  // Unary plus.

  light3lm_complex operator-() const;
  // Unary minus.

  friend inline light3lm_complex operator+(const light3lm_complex&, const light3lm_complex&);
  // Elementwise addition.

  friend inline light3lm_complex operator+(const light3lm_complex&, const lm_complex);
  // Addition to all elements.

  friend inline light3lm_complex operator+(const lm_complex, const light3lm_complex&);
  // Addition to all elements.

  friend inline light3lm_complex operator-(const light3lm_complex&, const light3lm_complex&);
  // Elementwise subtraction.

  friend inline light3lm_complex operator-(const light3lm_complex&, const lm_complex);
  // Subtraction from all elements.

  friend inline light3lm_complex operator-(const lm_complex, const light3lm_complex&);
  // Subtraction to all elements.

  friend inline lm_complex operator*(const light3lm_complex&, const light3lm_complex&);
  // Inner product.

  friend inline lm_complex Dot(const light3lm_complex&, const light3lm_complex&);
  // Inner product.

  friend inline light3lm_complex operator*(const light3lm_complex&, const lm_complex);
  // Multiply all elements.

  friend inline light3lm_complex operator*(const lm_complex, const light3lm_complex&);
  // Multiply all elements.

  friend inline light3lm_complex operator*(const light3lm_complex&, const light33lm_complex&);
  // Inner product.

  friend inline light3lm_complex operator*(const light33lm_complex&, const light3lm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const light3lm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline light3lm_complex Dot(const light3lm_complex&, const light33lm_complex&);
  // Inner product.

  friend inline light3lm_complex Dot(const light33lm_complex&, const light3lm_complex&);
  // Inner product.

  friend inline light3lm_complex operator/(const light3lm_complex&, const lm_complex);
  // Divide all elements.

  friend inline light3lm_complex operator/(const lm_complex, const light3lm_complex&);
  // Divide with all elements.

  friend inline light3lm_complex pow(const light3lm_complex&, const light3lm_complex&);
  // Raise to the power of-function, elementwise.

  friend inline light3lm_complex pow(const light3lm_complex&, const lm_complex);
  // Raise to the power of-function, for all elements.

  friend inline light3lm_complex pow(const lm_complex, const light3lm_complex&);
  // Raise to the power of-function, for all elements.

  friend inline light3lm_complex ElemProduct(const light3lm_complex&, const light3lm_complex&);
  // Elementwise multiplication.

  friend inline light3lm_complex ElemQuotient(const light3lm_complex&, const light3lm_complex&);
  // Elementwise division.

  friend inline light3lm_complex Apply(const light3lm_complex&, lm_complex f(lm_complex));
  // Apply the function elementwise on all three elements.

  friend inline light3lm_complex Apply(const light3lm_complex&, const light3lm_complex&, lm_complex f(lm_complex, lm_complex));
  // Apply the function elementwise on all three elements in the two
  // vecors.

  friend inline light3lm_complex Cross(const light3lm_complex&, const light3lm_complex&);
  // Cross product.

  friend inline light33lm_complex OuterProduct(const light3lm_complex&, const light3lm_complex&);
  // Outer product.

  friend  inline  light3lm_complex abs(const light3lm_complex&);
  // abs

#ifndef COMPLEX_TOOLS
  friend  inline  light3int sign(const light3int&);
  // sign

  friend  inline  light3int sign(const light3double&);
  // sign
#else //for complex numbers
  friend  inline  light3lm_complex sign(const light3lm_complex&);

#endif
  friend  inline  light3int ifloor(const light3double&);
  // ifloor

  friend  inline  light3int iceil(const light3double&);
  // iceil

  friend  inline  light3int irint(const light3double&);
  // irint

  friend  inline  light3double sqrt(const light3double&);
  // sqrt

  friend  inline  light3double exp(const light3double&);
  // exp

  friend  inline  light3double log(const light3double&);
  // log

  friend  inline  light3double sin(const light3double&);
  // sin

  friend  inline  light3double cos(const light3double&);
  // cos

  friend  inline  light3double tan(const light3double&);
  // tan

  friend  inline  light3double asin(const light3double&);
  // asin

  friend  inline  light3double acos(const light3double&);
  // acos

  friend  inline  light3double atan(const light3double&);
  // atan

  friend  inline  light3double sinh(const light3double&);
  // sinh

  friend  inline  light3double cosh(const light3double&);
  // cosh

  friend  inline  light3double tanh(const light3double&);
  // tanh

  friend  inline  light3double asinh(const light3double&);
  // asinh

  friend  inline  light3double acosh(const light3double&);
  // acosh

  friend  inline  light3double atanh(const light3double&);
  // atanh

  friend  inline  light3lm_complex ifloor(const light3lm_complex&);
  // ifloor

  friend  inline  light3lm_complex iceil(const light3lm_complex&);
  // iceil

  friend  inline  light3lm_complex irint(const light3lm_complex&);
  // irint

  friend  inline  light3lm_complex sqrt(const light3lm_complex&);
  // sqrt

  friend  inline  light3lm_complex exp(const light3lm_complex&);
  // exp

  friend  inline  light3lm_complex log(const light3lm_complex&);
  // log

  friend  inline  light3lm_complex sin(const light3lm_complex&);
  // sin

  friend  inline  light3lm_complex cos(const light3lm_complex&);
  // cos

  friend  inline  light3lm_complex tan(const light3lm_complex&);
  // tan

  friend  inline  light3lm_complex asin(const light3lm_complex&);
  // asin

  friend  inline  light3lm_complex acos(const light3lm_complex&);
  // acos

  friend  inline  light3lm_complex atan(const light3lm_complex&);
  // atan

  friend  inline  light3lm_complex sinh(const light3lm_complex&);
  // sinh

  friend  inline  light3lm_complex cosh(const light3lm_complex&);
  // cosh

  friend  inline  light3lm_complex tanh(const light3lm_complex&);
  // tanh

  friend  inline  light3lm_complex asinh(const light3lm_complex&);
  // asinh

  friend  inline  light3lm_complex acosh(const light3lm_complex&);
  // acosh

  friend  inline  light3lm_complex atanh(const light3lm_complex&);
  // atanh
  //<ignore>
  // friend class light3int;
  friend class lightNlm_complex;
  friend class light33lm_complex;
  friend class lightN3lm_complex;
  friend class lightNNlm_complex;
  friend class lightNNNlm_complex;
  friend class lightNNNNlm_complex;
  
#ifndef COMPLEX_TOOLS
  friend  inline  lightN3int sign(const lightN3int&);
  friend  inline  lightN3int sign(const lightN3double&);
#else
  friend  inline  lightN3lm_complex sign(const lightN3lm_complex&);
#endif
  friend  inline  lightN3int ifloor(const lightN3double&);
  friend  inline  lightN3int iceil(const lightN3double&);
  friend  inline  lightN3int irint(const lightN3double&);

  friend inline lightNlm_complex light_join (const lightNlm_complex arr1, const lightNlm_complex arr2);

  //</ignore>

protected:
  lm_complex elem[3];
  // The values of the three elements.

  light3lm_complex(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The size of the vector (number of elements).


};

//#include "c_light3.icc"


inline  light3double atanh(const light3double&);
inline  light3double atanh(const light3double&);

//*******************************
typedef light3double doubleVec3;
typedef light3int int3;
extern const doubleVec3 zeroVec3;
//*******************************

#undef IN_LIGHT3lm_complex_C_H

#endif
