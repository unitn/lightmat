inline lightNNN<T>& Set (const int i0, const int i1, const int i2, const T val);
inline lightNNN<T>& Set (const R r0, const int i1, const int i2, const lightN<T>& arr);
inline lightNNN<T>& Set (const R r0, const int i1, const int i2, const light3<T>& arr);
inline lightNNN<T>& Set (const R r0, const int i1, const int i2, const light4<T>& arr);
inline lightNNN<T>& Set (const R r0, const int i1, const int i2, const T val);
inline lightNNN<T>& Set (const int i0, const R r1, const int i2, const lightN<T>& arr);
inline lightNNN<T>& Set (const int i0, const R r1, const int i2, const light3<T>& arr);
inline lightNNN<T>& Set (const int i0, const R r1, const int i2, const light4<T>& arr);
inline lightNNN<T>& Set (const int i0, const R r1, const int i2, const T val);
inline lightNNN<T>& Set (const R r0, const R r1, const int i2, const lightNN<T>& arr);
inline lightNNN<T>& Set (const R r0, const R r1, const int i2, const light33<T>& arr);
inline lightNNN<T>& Set (const R r0, const R r1, const int i2, const light44<T>& arr);
inline lightNNN<T>& Set (const R r0, const R r1, const int i2, const T val);
inline lightNNN<T>& Set (const int i0, const int i1, const R r2, const lightN<T>& arr);
inline lightNNN<T>& Set (const int i0, const int i1, const R r2, const light3<T>& arr);
inline lightNNN<T>& Set (const int i0, const int i1, const R r2, const light4<T>& arr);
inline lightNNN<T>& Set (const int i0, const int i1, const R r2, const T val);
inline lightNNN<T>& Set (const R r0, const int i1, const R r2, const lightNN<T>& arr);
inline lightNNN<T>& Set (const R r0, const int i1, const R r2, const light33<T>& arr);
inline lightNNN<T>& Set (const R r0, const int i1, const R r2, const light44<T>& arr);
inline lightNNN<T>& Set (const R r0, const int i1, const R r2, const T val);
inline lightNNN<T>& Set (const int i0, const R r1, const R r2, const lightNN<T>& arr);
inline lightNNN<T>& Set (const int i0, const R r1, const R r2, const light33<T>& arr);
inline lightNNN<T>& Set (const int i0, const R r1, const R r2, const light44<T>& arr);
inline lightNNN<T>& Set (const int i0, const R r1, const R r2, const T val);
inline lightNNN<T>& Set (const R r0, const R r1, const R r2, const lightNNN<T>& arr);
inline lightNNN<T>& Set (const R r0, const R r1, const R r2, const T val);
inline lightN<T> operator() (const R r0, const int i1, const int i2) const;
inline lightN<T> operator() (const int i0, const R r1, const int i2) const;
inline lightNN<T> operator() (const R r0, const R r1, const int i2) const;
inline lightN<T> operator() (const int i0, const int i1, const R r2) const;
inline lightNN<T> operator() (const R r0, const int i1, const R r2) const;
inline lightNN<T> operator() (const int i0, const R r1, const R r2) const;
inline lightNNN<T> operator() (const R r0, const R r1, const R r2) const;

#ifdef IN_LIGHTNNNdouble_H
inline lightNNNdouble(const double e, const lightNNNint &s1, const lightmat_atan2_enum);
inline lightNNNdouble(const double e, const lightNNNdouble &s1, const lightmat_atan2_enum);
inline lightNNNdouble(const lightNNNint &s1, const double e, const lightmat_atan2_enum);
inline lightNNNdouble(const lightNNNdouble &s1, const double e, const lightmat_atan2_enum);
inline lightNNNdouble(const lightNNNint &s1, const lightNNNint &s2, const lightmat_atan2_enum);
inline lightNNNdouble(const lightNNNint &s1, const lightNNNdouble &s2, const lightmat_atan2_enum);
inline lightNNNdouble(const lightNNNdouble &s1, const lightNNNint &s2, const lightmat_atan2_enum);
inline lightNNNdouble(const lightNNNdouble &s1, const lightNNNdouble &s2, const lightmat_atan2_enum);
inline lightNNNdouble(const lightNNNint &s1, const int e, const lightmat_atan2_enum);
inline lightNNNdouble(const lightNNNdouble &s1, const int e, const lightmat_atan2_enum);
inline lightNNNdouble(const int e, const lightNNNint &s1, const lightmat_atan2_enum);
inline lightNNNdouble(const int e, const lightNNNdouble &s1, const lightmat_atan2_enum);

#endif
