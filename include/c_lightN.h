//           -*- c++ -*-

#ifndef LIGHTN_C_H
#define LIGHTN_C_H
// <cd> lightNlm_complex
//
// .SS Functionality
//
// lightN is a template for classes that implement vectors with any
// number of elements. E.g. a vector v with 5 elements of type double
// can be instanciated with:
//
// <code>lightN&lt;double&gt; v(5);</code>
//
// The number of elements in the vector can change during
// execution. It changes its size to whatever is needed when it is
// assigned a new value with an assignment operator.
//
// When a lightN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightN object with four elements to one with five elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//
#define IN_LIGHTNlm_complex_C_H

#define LIGHTN_SIZE 10
// The default size.

 class light3;
 class light4;
 class lightN;
 class lightN3;
 class lightN33;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class lightNlm_complex {
public:

#ifdef IN_LIGHTNdouble_C_H
  friend class lightNint;
#else
  friend class lightNdouble;
#endif

#ifdef IN_LIGHTNint_C_H
#define TDtypeH  double
#define TDNtypeH lightNdouble
#else
#define TDtypeH lm_complex
#define TDNtypeH lightNlm_complex
#endif


#ifdef LIGHTMAT_TEMPLATES
  // friend class lightNint;
#endif

  #include "c_lightN_auto.h"

  inline lightNlm_complex();
  // Default constructor.

  inline lightNlm_complex(const lightNlm_complex&);
  // Copy constructor.
  
  inline lightNlm_complex(const light3lm_complex&);
  // Conversion.

  inline lightNlm_complex(const light4lm_complex&);
  // Conversion.

  inline lightNlm_complex(const int);
  // Construct a lightN of given size.

  inline lightNlm_complex(const int, const lm_complex *);
  // Construct a lightN of given size and initialize elements with values
  // from an array.

  inline lightNlm_complex(const int, const lm_complex);
  // Construct a lightN of given size and initialize all elements
  // with a value (the second argument).

  inline ~lightNlm_complex();
  // Destructor.


#ifdef CONV_INT_2_DOUBLE
  operator lightNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNlm_complex();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNlm_complex();
  // Convert to complex.
#endif


  lightNlm_complex& operator=(const lightNlm_complex&);
  // Assignment.

  lightNlm_complex& operator=(const light3lm_complex&);
  // Assignment, change size to 3 elements.

  lightNlm_complex& operator=(const light4lm_complex&);
  // Assignment, change size to 4 elements.

  lightNlm_complex& operator=(const lm_complex);
  // Assign one value to all elements.

  //
  // operator()
  //

  lm_complex operator() (const int x) const {
    limiterror((x<1) || (x>size1));
    return elem[x-1];
  };
  // Get the value of one element.

  lm_complex& operator()(const int x) {
    limiterror((x<1) || (x>size1));
    return elem[x-1];
  };
  // Get/Set the value of one element.
 
  inline lm_complex& takeElementReference(const int);
  // Same as above

  inline lightNlm_complex& SetShape(const int x=-1);
  // Sets the specified size. 

//REMOVE LATER
  lightNlm_complex SubVector(const int n1,const  int n2) const; 
  // Gets specified interval of the vector.

  const lightNlm_complex& SetSubVector(const int n1,const  int n2,const lightNlm_complex& x);
  // Puts all  elements of x into specified interval of  the current vector.
  // Returns the vector x.

  lm_complex SetSubVector(const int n1,const  int n2,const lm_complex x);
  // Puts the   element x into all elements of specified interval of  the current vector.
  // Returns x.

  int operator==(const lightNlm_complex&) const;
  // Equality.

  int operator!=(const lightNlm_complex&) const;
  // Inequality.
 
  lightNlm_complex& operator+=(const lm_complex);
  // Add a value to all elements.

  lightNlm_complex& operator+=(const lightNlm_complex&);
  // Elementwise addition.

  lightNlm_complex& operator-=(const lm_complex);
  // Subtract a value from all elements.

  lightNlm_complex& operator-=(const lightNlm_complex&);
  // Elementwise subtraction.

  lightNlm_complex& operator*=(const lm_complex);
  // Multiply all elements with a value.

  lightNlm_complex& operator/=(const lm_complex);
  // Divide all elements with a value.

#ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
#endif

  lm_complex Extract(const lightNint&) const;
  // Extract an element using given index.   

  int dimension(const int x = 1) const {
    if(x == 1)
      return size1;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == no. elements).

  #ifndef COMPLEX_TOOLS
  lightNlm_complex& normalize();
  // Normalize vector. Returns a reference to itself.
  #endif

  void Get(lm_complex *) const;
  // Get values of all elements and put them in an array.

  void Set(const lm_complex *);
  // Set values of all elements from array.

  lm_complex * data() const;
  // Direct access to the stored data. Use the result with care.

  lightNlm_complex operator+() const;
  // Unary plus.

  lightNlm_complex operator-() const;
  // Unary minus.


  //<ignore>
#ifdef IN_LIGHTNlm_complex_C_H
  friend class lightNint;
#endif
  friend class light3lm_complex;
  friend class light4lm_complex;
  friend class lightN3lm_complex;
  friend class lightNNlm_complex;
  friend class lightNNNlm_complex;
  friend class lightNNNNlm_complex;
  friend class lightN33lm_complex;
  //</ignore>

  friend inline lightNlm_complex operator+(const lightNlm_complex&, const lightNlm_complex&);
  // Elementwise addition.

  //--
  friend inline lightNlm_complex operator+(const lightNlm_complex&, const lm_complex);
  // Addition to all elements.

  friend inline lightNlm_complex operator+(const lm_complex, const lightNlm_complex&);
  // Addition to all elements.

#ifdef IN_LIGHTNdouble_C_H
  friend inline lightNdouble operator+(const lightNdouble&, const int);
  friend inline lightNdouble operator+(const lightNint&, const double);
  friend inline lightNdouble operator+(const double, const lightNint&);
  friend inline lightNdouble operator+(const int, const lightNdouble&);
#endif

#ifdef IN_LIGHTNlm_complex_C_H
friend inline lightNlm_complex operator+(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator+(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator+(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator+(const double e, const lightNlm_complex& s2);
#endif

  //--

  friend inline lightNlm_complex operator-(const lightNlm_complex&, const lightNlm_complex&);
  // Elementwise subtraction.

  friend inline lightNlm_complex operator-(const lightNlm_complex&, const lm_complex);
  // Subtraction from all elements.

  friend inline lightNlm_complex operator-(const lm_complex, const lightNlm_complex&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNdouble_C_H
  friend inline lightNdouble operator-(const lightNdouble&, const int);
  friend inline lightNdouble operator-(const lightNint&, const double);
  friend inline lightNdouble operator-(const double, const lightNint&);
  friend inline lightNdouble operator-(const int, const lightNdouble&);
#endif

  #ifdef IN_LIGHTNlm_complex_C_H
friend inline lightNlm_complex operator-(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator-(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator-(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator-(const double e, const lightNlm_complex& s2);
#endif


  friend inline lm_complex operator*(const lightNlm_complex&, const lightNlm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const lightNlm_complex&, const lm_complex);
  // Multiply all elements.

  friend inline lightNlm_complex operator*(const lm_complex, const lightNlm_complex&);
  // Multiply all elements.

#ifdef IN_LIGHTNdouble_C_H
  friend inline lightNdouble operator*(const lightNdouble&, const int);
  friend inline lightNdouble operator*(const lightNint&, const double);
  friend inline lightNdouble operator*(const double, const lightNint&);
  friend inline lightNdouble operator*(const int, const lightNdouble&);
#endif

#ifdef IN_LIGHTNlm_complex_C_H
friend inline lightNlm_complex operator*(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator*(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator*(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator*(const double e, const lightNlm_complex& s2);
#endif

  friend inline lightNlm_complex operator*(const lightNlm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const light3lm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const light4lm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const lightNNlm_complex&,const lightNlm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const lightNNlm_complex&, const light3lm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const lightNNlm_complex&, const light4lm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const lightN3lm_complex&, const light3lm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const lightN3lm_complex&, const lightNlm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator/(const lightNlm_complex&, const lm_complex);
  // Divide all elements.

  friend inline lightNlm_complex operator/(const lm_complex, const lightNlm_complex&);
  // Divide all elements.

#ifdef IN_LIGHTNdouble_C_H
  friend inline lightNdouble operator/(const lightNdouble&, const int);
  friend inline lightNdouble operator/(const lightNint&, const double);
  friend inline lightNdouble operator/(const double, const lightNint&);
  friend inline lightNdouble operator/(const int, const lightNdouble&);
#endif

#ifdef IN_LIGHTNlm_complex_C_H
friend inline lightNlm_complex operator/(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator/(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator/(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator/(const double e, const lightNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNlm_complex_C_H
friend inline lightNdouble arg(const lightNlm_complex& a);
friend inline lightNdouble re(const lightNlm_complex& a);
friend inline lightNdouble im(const lightNlm_complex& a);
friend inline lightNlm_complex conjugate(const lightNlm_complex& a);
//#endif

  friend inline lightNlm_complex pow(const lightNlm_complex&, const lightNlm_complex&);
  // Raise to the power of-function, elementwise.

  friend inline lightNlm_complex pow(const lightNlm_complex&, const lm_complex);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex pow(const lm_complex, const lightNlm_complex&);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex pow(const lightNlm_complex& s1, const double e);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex pow( const double e,const lightNlm_complex& s1);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex ElemProduct(const lightNlm_complex&, const lightNlm_complex&);
  // Elementwise multiplication.

  friend inline lightNlm_complex ElemQuotient(const lightNlm_complex&, const lightNlm_complex&);
  // Elementwise division.

  friend inline lightNlm_complex Apply(const lightNlm_complex&, lm_complex f(lm_complex));
  // Apply the function elementwise all elements.

  friend inline lightNlm_complex Apply(const lightNlm_complex&, const lightNlm_complex&, lm_complex f(lm_complex, lm_complex));
  // Apply the function elementwise on all elements in the two vectors.


#ifdef IN_LIGHTNlm_complex_C_H
#else
  friend inline lightNlm_complex abs(const lightNlm_complex&);
#ifdef IN_LIGHTNdouble_C_H
  friend inline lightNdouble abs(const lightNlm_complex&);
#endif 
#endif

#ifndef COMPLEX_TOOLS
  // sign
  friend inline lightNint sign(const lightNint&);  
  friend inline lightNint sign(const lightNdouble&);
#else
  friend inline lightNlm_complex sign(const lightNlm_complex&);
#endif
  /// Added 2/2/98 

  friend inline lightNlm_complex  FractionalPart(const lightNlm_complex&);
  //  FractionalPart

  //old version
  //friend inline lightNint  IntegerPart(const lightNint&);
  //  IntegerPart

  //friend inline lightNint  IntegerPart(const lightNdouble&);
  //  IntegerPart

  //new version
  friend inline lightNlm_complex  IntegerPart(const lightNlm_complex&);
  //  IntegerPart

  friend inline lightNlm_complex Mod (const  lightNlm_complex&,const  lightNlm_complex&);
  // Mod
 
  friend inline lightNlm_complex Mod (const  lightNlm_complex&,const lm_complex);
  // Mod

  friend inline lightNlm_complex Mod (const lm_complex,const  lightNlm_complex&);
  // Mod

  friend inline lm_complex LightMax (const lightNlm_complex& );
  // Max

  friend inline lm_complex LightMin (const lightNlm_complex& );
  // Min

  friend inline lm_complex findLightMax (const lm_complex *,const int );
  // Find Max
 
  friend inline lm_complex findLightMin (const  lm_complex *,const  int );
  // Find Min
 
  /// End Added 

  friend inline lightNint ifloor(const lightNdouble&);
  // ifloor

  friend inline lightNint iceil(const lightNdouble&);
  // iceil

  friend inline lightNint irint(const lightNdouble&);
  // irint

  friend inline lightNdouble sqrt(const lightNdouble&);
  // sqrt

  friend inline lightNdouble exp(const lightNdouble&);
  // exp

  friend inline lightNdouble log(const lightNdouble&);
  // log

  friend inline lightNdouble sin(const lightNdouble&);
  // sin

  friend inline lightNdouble cos(const lightNdouble&);
  // cos

  friend inline lightNdouble tan(const lightNdouble&);
  // tan

  friend inline lightNdouble asin(const lightNdouble&);
  // asin

  friend inline lightNdouble acos(const lightNdouble&);
  // acos

  friend inline lightNdouble atan(const lightNdouble&);
  // atan

  friend inline lightNdouble sinh(const lightNdouble&);
  // sinh

  friend inline lightNdouble cosh(const lightNdouble&);
  // cosh

  friend inline lightNdouble tanh(const lightNdouble&);
  // tanh

  friend inline lightNdouble asinh(const lightNdouble&);
  // asinh

  friend inline lightNdouble acosh(const lightNdouble&);
  // acosh

  friend inline lightNdouble atanh(const lightNdouble&);
  // atanh

  friend inline lightNlm_complex sqrt(const lightNlm_complex&);
  // sqrt

  friend inline lightNlm_complex exp(const lightNlm_complex&);
  // exp

  friend inline lightNlm_complex ifloor(const lightNlm_complex&);
  // ifloor

  friend inline lightNlm_complex iceil(const lightNlm_complex&);
  // iceil

  friend inline lightNlm_complex irint(const lightNlm_complex&);
  // irint

  friend inline lightNlm_complex log(const lightNlm_complex&);
  // log

  friend inline lightNlm_complex sin(const lightNlm_complex&);
  // sin

  friend inline lightNlm_complex cos(const lightNlm_complex&);
  // cos

  friend inline lightNlm_complex tan(const lightNlm_complex&);
  // tan

  friend inline lightNlm_complex asin(const lightNlm_complex&);
  // asin

  friend inline lightNlm_complex acos(const lightNlm_complex&);
  // acos

  friend inline lightNlm_complex atan(const lightNlm_complex&);
  // atan

  friend inline lightNlm_complex sinh(const lightNlm_complex&);
  // sinh

  friend inline lightNlm_complex cosh(const lightNlm_complex&);
  // cosh

  friend inline lightNlm_complex tanh(const lightNlm_complex&);
  // tanh

  friend inline lightNlm_complex asinh(const lightNlm_complex&);
  // asinh

  friend inline lightNlm_complex acosh(const lightNlm_complex&);
  // acosh

  friend inline lightNlm_complex atanh(const lightNlm_complex&);
  // atanh


  friend inline lightNlm_complex Cross (const  lightNlm_complex&, const  lightNlm_complex&); 
  friend inline lightNlm_complex Cross (const  lightNlm_complex&, const  lightNlm_complex&, const  lightNlm_complex&); 
  friend inline lightNlm_complex Cross (const  lightNlm_complex&, const  lightNlm_complex&, const  lightNlm_complex&, const  lightNlm_complex&); 

  friend inline lightNlm_complex light_join (const lightNlm_complex arr1, const lightNlm_complex arr2);
  friend inline lightNlm_complex light_drop (const lightNlm_complex arr1, const R r);

#ifdef IN_LIGHTNint_C_H
  friend inline double light_mean     ( const  lightNlm_complex& );
  friend inline double light_standard_deviation( const  lightNlm_complex& );
  friend inline double light_variance ( const  lightNlm_complex& );
  friend inline double light_median   ( const  lightNlm_complex& );
#else
  friend inline lm_complex light_mean     ( const  lightNlm_complex& );
  friend inline lm_complex light_variance ( const  lightNlm_complex& );
  friend inline lm_complex light_standard_deviation( const  lightNlm_complex& );
  friend inline lm_complex light_median   ( const  lightNlm_complex& );
#endif
  
  friend inline lm_complex light_total    ( const  lightNlm_complex& );
 
 
  friend inline lightNlm_complex light_sort (const lightNlm_complex arr1);

  friend inline TDNtypeH light_quantile3L (const lightNlm_complex arr1, const lightNdouble q2, const lightNNdouble params);
  friend inline TDtypeH light_quantile3 (const lightNlm_complex arr1, double q,  const lightNNdouble params);
  friend inline TDNtypeH light_quantile2L (const lightNlm_complex arr1, const lightNdouble q2);
  friend inline TDtypeH light_quantile2 (const lightNlm_complex arr1, double q);

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNdouble::lightNlm_complex(const lightNint& s1, const double e, const lightmat_plus_enum)

  lm_complex *elem;
  // elem always points at the area where the elements are.


protected:
  lm_complex sarea[LIGHTN_SIZE];
  // The values are stored here if they fit into LIGHTN_SIZE
  // else a memory area is allocated.

  int size1;
  // The size of the vector (number of elements).

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNlm_complex(const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // argument is the size of the created vector. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNlm_complex(const lightNlm_complex&, const lightNlm_complex&, const lightmat_plus_enum);
  lightNlm_complex(const lightNlm_complex&, const lm_complex, const lightmat_plus_enum);
#ifdef IN_LIGHTNdouble_C_H
  lightNlm_complex(const lightNdouble&, const int, const lightmat_plus_enum);
  lightNlm_complex(const lightNint&, const double, const lightmat_plus_enum);
#endif

  lightNlm_complex(const lightNlm_complex&, const lightNlm_complex&, const lightmat_minus_enum);
  lightNlm_complex(const lm_complex, const lightNlm_complex&, const lightmat_minus_enum);
#ifdef IN_LIGHTNdouble_C_H
  lightNlm_complex(const int, const lightNdouble&, const lightmat_minus_enum);
  lightNlm_complex(const double, const lightNint&, const lightmat_minus_enum);
#endif

  lightNlm_complex(const lightNlm_complex&, const lightNlm_complex&, const lightmat_mult_enum);
  lightNlm_complex(const lightNlm_complex&, const lm_complex, const lightmat_mult_enum);
#ifdef IN_LIGHTNdouble_C_H
  lightNlm_complex(const lightNdouble&, const int, const lightmat_mult_enum);
  lightNlm_complex(const lightNint&, const double, const lightmat_mult_enum);
#endif

  lightNlm_complex(const lightNlm_complex&, const lightNNlm_complex&, const lightmat_mult_enum);
  lightNlm_complex(const lightNNlm_complex&, const lightNlm_complex&, const lightmat_mult_enum);
  lightNlm_complex(const light3lm_complex&, const lightNNlm_complex&, const lightmat_mult_enum);
  lightNlm_complex(const light4lm_complex&, const lightNNlm_complex&, const lightmat_mult_enum);
  lightNlm_complex(const lightN3lm_complex&, const light3lm_complex&, const lightmat_mult_enum);
  lightNlm_complex(const lightN3lm_complex&, const lightNlm_complex&, const lightmat_mult_enum);
  lightNlm_complex(const lightNNlm_complex&, const light3lm_complex&, const lightmat_mult_enum);
  lightNlm_complex(const lightNNlm_complex&, const light4lm_complex&, const lightmat_mult_enum);

  lightNlm_complex(const lm_complex, const lightNlm_complex&, const lightmat_div_enum);
#ifdef IN_LIGHTNdouble_C_H
  lightNlm_complex(const int, const lightNdouble&, const lightmat_div_enum);
  lightNlm_complex(const double, const lightNint&, const lightmat_div_enum);
#endif

  lightNlm_complex(const lightNlm_complex&, const lightNlm_complex&, const lightmat_pow_enum);
  lightNlm_complex(const lightNlm_complex&, const lm_complex, const lightmat_pow_enum);
  lightNlm_complex(const lm_complex, const lightNlm_complex&, const lightmat_pow_enum);

#ifdef IN_LIGHTNlm_complex_C_H
  lightNlm_complex(const lightNlm_complex& s1, const double e, const lightmat_pow_enum) ;
  lightNlm_complex(const double e,const lightNlm_complex& s1, const lightmat_pow_enum) ;
#endif

#ifdef IN_LIGHTNlm_complex_C_H
 #else
  #ifdef IN_LIGHTNdouble_C_H
   lightNlm_complex(const lightNlm_complex&, const lightmat_abs_enum);
   lightNlm_complex(const lightNlm_complex&, const lightmat_abs_enum);
  #else
  lightNlm_complex(const lightNlm_complex&, const lightmat_abs_enum);
  #endif
#endif

  lightNlm_complex(const lightNlm_complex&, const lightNlm_complex&, const lightmat_eprod_enum);
  lightNlm_complex(const lightNlm_complex&, const lightNlm_complex&, const lightmat_equot_enum);
  lightNlm_complex(const lightNlm_complex&, lm_complex f(lm_complex), const lightmat_apply_enum);
  lightNlm_complex(const lightNlm_complex&, const lightNlm_complex&, lm_complex f(lm_complex, lm_complex), const lightmat_apply_enum);

};





 inline lm_complex findLightMax (const lm_complex *,const int );
// Find Max

 inline lm_complex findLightMin (const  lm_complex *,const  int );
// Find Min








typedef lightNdouble doubleN;
typedef lightNint intN;
typedef lightNlm_complex lm_complexN;

#undef TDtypeH  
#undef TDNtypeH 

#undef IN_LIGHTNlm_complex_C_H

#endif
