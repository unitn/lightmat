//           -*- c++ -*-

#ifndef LIGHTNNNN_H
#define LIGHTNNNN_H
// <cd> lightNNN
//
// .SS Functionality
//
// lightNNNN is a template for classes that implement tensors of rank 4
// with any number of elements. E.g. a lightNNNN s with 5x6x7x8 elements of
// type double can be instanciated with:
//
// <code>lightNNNN&lt;double&gt; s(5,6,7,8);</code>
//
// The size of the tensor can change during execution. It changes its
// size to whatever is needed when it is assigned a new value with an
// assignment operator.
//
// When a lightNNNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNNN object with 5x6x5x5 elements to one with 5x5x6x5 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHTNNNN<T>_H


#define LIGHTNNNN_SIZE1 4
#define LIGHTNNNN_SIZE2 4
#define LIGHTNNNN_SIZE3 4
#define LIGHTNNNN_SIZE4 4
#define LIGHTNNNN_SIZE (LIGHTNNNN_SIZE1*LIGHTNNNN_SIZE2*LIGHTNNNN_SIZE3*LIGHTNNNN_SIZE4)
// The default size.

template<class T> class lightN;
template<class T> class lightNN;
template<class T> class lightNNN;

template<class T>
class lightNNNN {
public:

#ifdef IN_LIGHTNNNNdouble_H
  friend class lightNNNN<int>;
#else
  friend class lightNNNN<double>;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNNN<int>;
#endif

  #include "lightNNNN_auto.h"

  inline lightNNNN();
  // Default constructor.

  inline lightNNNN(const lightNNNN<T>&);
  // Copy constructor.

  inline lightNNNN(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNNN(const int, const int, const int, const int);
  // Construct a lightNNNN of given size.

  inline lightNNNN(const int, const int, const int, const int, const T *);
  // Construct a lightNNNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNNN(const int, const int, const int, const int, const T);
  // Construct a lightNNNN of given size and initialize all elements
  // with a value (the last argument).

  inline ~lightNNNN();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNNN<double>();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNNN<lm_complex>();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNNN<lm_complex>();
  // Convert to complex.
#endif

  lightNNNN<T>& operator=(const lightNNNN<T>&);
  // Assignment.

  lightNNNN<T>& operator=(const T);
  // Assign one value to all elements.

  //
  // Operator ().
  //
  
  T operator()(const int x, const int y, const int z, const int v) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3) || (v<1) || (v>size4));
#ifdef ROWMAJOR
    return elem[(((x-1)*size2+(y-1))*size3+(z-1))*size4+v-1];
#else
    return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
#endif
  };
  // Get the value of one element.


  T& operator()(const int x, const int y, const int z, const int v) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3) || (v<1) || (v>size4));
#ifdef ROWMAJOR
    return elem[(((x-1)*size2+(y-1))*size3+(z-1))*size4+v-1];
#else
    return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
#endif
  };
  // Get/Set the value of one element.

  inline lightN<T> operator()(const int, const int, const int) const;
  // Get the value of a vector in the tensor.

  inline lightNN<T> operator()(const int, const int) const;
  // Get the value of a matrix in the tensor.

  inline lightNNN<T> operator()(const int) const;
  // Get the value of a tensor of rank 3 in the tensor.


  //
  // Set
  //

  inline lightNNNN<T>& Set (const int i0, const int i1, const int i2,
			    const T val);
  inline lightNNNN<T>& Set (const int i0, const int i1, const int i2,
			    const lightN<T>& arr);
  inline lightNNNN<T>& Set (const int i0, const int i1, const int i2,
			    const light3<T>& arr);
  inline lightNNNN<T>& Set (const int i0, const int i1, const int i2,
			    const light4<T>& arr);
  inline lightNNNN<T>& Set (const int i0, const int i1, const T val);
  inline lightNNNN<T>& Set (const int i0, const int i1, const lightNN<T>& arr);
  inline lightNNNN<T>& Set (const int i0, const int i1, const light33<T>& arr);
  inline lightNNNN<T>& Set (const int i0, const int i1, const light44<T>& arr);
  inline lightNNNN<T>& Set (const int i0, const T val);
  inline lightNNNN<T>& Set (const int i0, const lightNNN<T>& arr);

 
  int operator==(const lightNNNN<T>&) const;
  // Equality.

  int operator!=(const lightNNNN<T>&) const;
  // Inequality.
 
  lightNNNN<T>& operator+=(const T);
  // Add a value to all elements.

  lightNNNN<T>& operator+=(const lightNNNN<T>&);
  // Elementwise addition.

  lightNNNN<T>& operator-=(const T);
  // Subtract a value from all elements.

  lightNNNN<T>& operator-=(const lightNNNN<T>&);
  // Elementwise subtraction.

  lightNNNN<T>& operator*=(const T);
  // Mulitply all elements with a value.

  lightNNNN<T>& operator/=(const T);
  // Divide all elements with a value.

  lightNNNN<T>& SetShape(const int x=-1, const int y=-1, 
			 const int z=-1, const int v=-1);
  // Sets specified shape to the tensor. 

  T Extract(const lightN<int>&) const;
  // Extract an element using given index. 

  lightNNNN<T>& reshape(const int, const int, const int, const int, const lightN<T>&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c,d) will be set to s(a) in the lightNNNN
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNN<T>& reshape(const int, const int, const int, const int, const lightNN<T>&);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c,d) will be set to s(a,b) in the
  // lightNNNN object. The values of the first two arguments must be
  // the same as the size of the lightNN-matrix. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNN<T>& reshape(const int, const int, const int, const int, const lightNNN<T>&);
  // Convert the lightNNN-tensor to the given size and put the result
  // in this object. (*this)(a,b,c,d) will be set to s(a,b,c) in the
  // lightNNNN object. The values of the first three arguments must be
  // the same as the size of the lightNNN-tensor. The program may dump
  // core or behave strangely if the arguments are incorrect.

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else if(x == 3)
      return size3;
    else if(x == 4)
      return size4;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(T *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const T *);
  // Set values of all elements from array (row major order).
 
  T * data() const;
  // Direct access to the stored data. Use the result with care. 

  lightNNNN<T> operator+() const;
  // Unary plus.

  lightNNNN<T> operator-() const;
  // Unary minus.


  friend inline lightNNNN<T> operator+(const lightNNNN<T>&, const lightNNNN<T>&);
  // Elementwise addition.

  friend inline lightNNNN<T> operator+(const lightNNNN<T>&, const T);
  // Addition to all elements.

  friend inline lightNNNN<T> operator+(const T, const lightNNNN<T>&);
  // Addition to all elements.

#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNN<double> operator+(const lightNNNN<double>&, const int);
  friend inline lightNNNN<double> operator+(const lightNNNN<int>&, const double);
  friend inline lightNNNN<double> operator+(const double, const lightNNNN<int>&);
  friend inline lightNNNN<double> operator+(const int, const lightNNNN<double>&);
#endif

  #ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator+(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator+(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNN<T> operator-(const lightNNNN<T>&, const lightNNNN<T>&);
  // Elementwise subtraction.

  friend inline lightNNNN<T> operator-(const lightNNNN<T>&, const T);
  // Subtraction from all elements.

  friend inline lightNNNN<T> operator-(const T, const lightNNNN<T>&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNN<double> operator-(const lightNNNN<double>&, const int);
  friend inline lightNNNN<double> operator-(const lightNNNN<int>&, const double);
  friend inline lightNNNN<double> operator-(const double, const lightNNNN<int>&);
  friend inline lightNNNN<double> operator-(const int, const lightNNNN<double>&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator-(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator-(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNN<T> operator*(const lightNNNN<T>&, const T);
  // Multiply all elements.

  friend inline lightNNNN<T> operator*(const T, const lightNNNN<T>&);
  // Multiply all elements.

#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNN<double> operator*(const lightNNNN<double>&, const int);
  friend inline lightNNNN<double> operator*(const lightNNNN<int>&, const double);
  friend inline lightNNNN<double> operator*(const double, const lightNNNN<int>&);
  friend inline lightNNNN<double> operator*(const int, const lightNNNN<double>&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator*(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator*(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNN<T> operator*(const lightNNNN<T>&, const lightNN<T>&);
  // Inner product.

  friend inline lightNNNN<T> operator*(const lightNN<T>&, const lightNNNN<T>&);
  // Inner product.

  friend inline lightNNNN<T> operator*(const lightNNNN<T>&, const light33<T>&);
  // Inner product.

  friend inline lightNNNN<T> operator*(const light33<T>&, const lightNNNN<T>&);
  // Inner product.

  friend inline lightNNNN<T> operator*(const lightNNNN<T>&, const light44<T>&);
  // Inner product.

  friend inline lightNNNN<T> operator*(const light44<T>&, const lightNNNN<T>&);
  // Inner product.

  friend inline lightNNNN<T> operator*(const lightNNN<T>&, const lightNNN<T>&);
  // Inner product.

  friend inline lightNNNN<T> operator/(const lightNNNN<T>&, const T);
  // Divide all elements.

  friend inline lightNNNN<T> operator/(const T, const lightNNNN<T>&);
  // Divide all elements.

#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNN<double> operator/(const lightNNNN<double>&, const int);
  friend inline lightNNNN<double> operator/(const lightNNNN<int>&, const double);
  friend inline lightNNNN<double> operator/(const double, const lightNNNN<int>&);
  friend inline lightNNNN<double> operator/(const int, const lightNNNN<double>&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator/(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator/(const double e, const lightNNNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNNNlm_complex_H
friend inline lightNNNNdouble arg(const lightNNNNlm_complex& a);
friend inline lightNNNNdouble re(const lightNNNNlm_complex& a);
friend inline lightNNNNdouble im(const lightNNNNlm_complex& a);
friend inline lightNNNNlm_complex conjugate(const lightNNNNlm_complex& a);
//#endif

  friend inline lightNNNN<T> pow(const lightNNNN<T>&, const lightNNNN<T>&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNNN<T> pow(const lightNNNN<T>&, const T);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNN<T> pow(const T, const lightNNNN<T>&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNN<T> ElemProduct(const lightNNNN<T>&, const lightNNNN<T>&);
  // Elementwise multiplication.

  friend inline lightNNNN<T> ElemQuotient(const lightNNNN<T>&, const lightNNNN<T>&);
  // Elementwise division.

  friend inline lightNNNN<T> Apply(const lightNNNN<T>&, T f(T));
  // Apply the function elementwise all elements.

  friend inline lightNNNN<T> Apply(const lightNNNN<T>&, const lightNNNN<T>&, T f(T, T));
  // Apply the function elementwise on all elements in the two tensors.

  friend inline lightNNNN<T> Transpose(const lightNNNN<T>&);
  // Transpose.

  
#ifdef IN_LIGHTNNNNlm_complex_H
#else
  friend inline lightNNNN<T> abs(const lightNNNN<T>&);
#ifdef IN_LIGHTNNNNdouble_H
  friend inline lightNNNN<double> abs(const lightNNNN<lm_complex>&);
#endif 
#endif


  // abs

#ifndef COMPLEX_TOOLS
  friend inline lightNNNN<int> sign(const lightNNNN<int>&);
  // sign

  friend inline lightNNNN<int> sign(const lightNNNN<double>&);
  // sign
#else
  friend inline lightNNNNlm_complex sign(const lightNNNNlm_complex&);
#endif


/// Added 2/2/98 

  friend inline lightNNNN<T>  FractionalPart(const lightNNNN<T>&);
  //  FractionalPart

  friend inline lightNNNN<T>  IntegerPart(const lightNNNN<T>&);
  //  IntegerPart
 
  //friend inline lightNNNN<int>  IntegerPart(const lightNNNN<double>&);
  //  IntegerPart

  friend inline lightNNNN<T> Mod (const  lightNNNN<T>&,const  lightNNNN<T>&);
  // Mod
 
  friend inline lightNNNN<T> Mod (const  lightNNNN<T>&,const T);
  // Mod

  friend inline lightNNNN<T> Mod (const T,const  lightNNNN<T>&);
  // Mod

  friend inline T LightMax (const lightNNNN<T>& );
  // Max

  friend inline T LightMin (const lightNNNN<T>& );
  // Min

  friend inline T findLightMax (const  T *,const  int );
  // Find Max
 
  friend inline T findLightMin (const  T *,const  int );
  // Find Min
 
  /// End Added 

  friend inline lightNNNN<int> ifloor(const lightNNNN<double>&);
  // ifloor

  friend inline lightNNNN<int> iceil(const lightNNNN<double>&);
  // iceil

  friend inline lightNNNN<int> irint(const lightNNNN<double>&);
  // irint

  friend inline lightNNNN<double> sqrt(const lightNNNN<double>&);
  // sqrt

  friend inline lightNNNN<double> exp(const lightNNNN<double>&);
  // exp

  friend inline lightNNNN<double> log(const lightNNNN<double>&);
  // log

  friend inline lightNNNN<double> sin(const lightNNNN<double>&);
  // sin

  friend inline lightNNNN<double> cos(const lightNNNN<double>&);
  // cos

  friend inline lightNNNN<double> tan(const lightNNNN<double>&);
  // tan

  friend inline lightNNNN<double> asin(const lightNNNN<double>&);
  // asin

  friend inline lightNNNN<double> acos(const lightNNNN<double>&);
  // acos

  friend inline lightNNNN<double> atan(const lightNNNN<double>&);
  // atan

  friend inline lightNNNN<double> sinh(const lightNNNN<double>&);
  // sinh

  friend inline lightNNNN<double> cosh(const lightNNNN<double>&);
  // cosh

  friend inline lightNNNN<double> tanh(const lightNNNN<double>&);
  // tanh

  friend inline lightNNNN<double> asinh(const lightNNNN<double>&);
  // asinh

  friend inline lightNNNN<double> acosh(const lightNNNN<double>&);
  // acosh

  friend inline lightNNNN<double> atanh(const lightNNNN<double>&);
  // atanh

  friend inline lightNNNN<lm_complex> ifloor(const lightNNNN<lm_complex>&);
  // ifloor

  friend inline lightNNNN<lm_complex> iceil(const lightNNNN<lm_complex>&);
  // iceil

  friend inline lightNNNN<lm_complex> irint(const lightNNNN<lm_complex>&);
  // irint

  friend inline lightNNNN<lm_complex> sqrt(const lightNNNN<lm_complex>&);
  // sqrt

  friend inline lightNNNN<lm_complex> exp(const lightNNNN<lm_complex>&);
  // exp

  friend inline lightNNNN<lm_complex> log(const lightNNNN<lm_complex>&);
  // log

  friend inline lightNNNN<lm_complex> sin(const lightNNNN<lm_complex>&);
  // sin

  friend inline lightNNNN<lm_complex> cos(const lightNNNN<lm_complex>&);
  // cos

  friend inline lightNNNN<lm_complex> tan(const lightNNNN<lm_complex>&);
  // tan

  friend inline lightNNNN<lm_complex> asin(const lightNNNN<lm_complex>&);
  // asin

  friend inline lightNNNN<lm_complex> acos(const lightNNNN<lm_complex>&);
  // acos

  friend inline lightNNNN<lm_complex> atan(const lightNNNN<lm_complex>&);
  // atan

  friend inline lightNNNN<lm_complex> sinh(const lightNNNN<lm_complex>&);
  // sinh

  friend inline lightNNNN<lm_complex> cosh(const lightNNNN<lm_complex>&);
  // cosh

  friend inline lightNNNN<lm_complex> tanh(const lightNNNN<lm_complex>&);
  // tanh

  friend inline lightNNNN<lm_complex> asinh(const lightNNNN<lm_complex>&);
  // asinh

  friend inline lightNNNN<lm_complex> acosh(const lightNNNN<lm_complex>&);
  // acosh

  friend inline lightNNNN<lm_complex> atanh(const lightNNNN<lm_complex>&);
  // atanh

#ifdef IN_LIGHTNNNNint_H
  friend inline lightNNN<double> light_mean     ( const  lightNNNN<T>& );
  friend inline lightNNN<double> light_standard_deviation( const  lightNNNN<T>& );
  friend inline lightNNN<double> light_variance ( const  lightNNNN<T>& );
  friend inline lightNNN<double> light_median   ( const  lightNNNN<T>& );
#else
  friend inline lightNNN<T> light_mean     ( const  lightNNNN<T>& );
  friend inline lightNNN<T> light_variance ( const  lightNNNN<T>& );
  friend inline lightNNN<T> light_standard_deviation( const  lightNNNN<T>& );
  friend inline lightNNN<T> light_median   ( const  lightNNNN<T>& );
#endif
  
  friend inline lightNNN<T> light_total    ( const  lightNNNN<T>& );

  friend inline lightNNNN<T> light_sort (const lightNNNN<T> arr1);
  
  friend inline lightN<T> light_flatten (const lightNNNN<T> s);
  friend inline lightN<T> light_flatten (const lightNNNN<T> s, int level);
  friend inline lightNNNN<T> light_flatten0 (const lightNNNN<T> s);
  friend inline lightNNN<T> light_flatten1 (const lightNNNN<T> s);
  friend inline lightNN<T> light_flatten2 (const lightNNNN<T> s);

  //<ignore>
#ifdef IN_LIGHTNNNNlm_complex_H  
  friend class lightNNNN<int>;
#endif  
  friend class lightN<T>;
  friend class lightNN<T>;
  friend class lightNNN<T>;

  friend inline lightNNNN<T> light_join (const lightNNNN<T>, const lightNNNN<T>);
  friend inline lightNNNN<T> light_drop (const lightNNNN<T> arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNNNdouble caNNNot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNNNdouble::lightNNNN(const lightNNNNint& s1, const double e, const lightmat_plus_enum)

  T *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // Size of the first index.

  int size2;
  // Size of the second index.

  int size3;
  // Size of the third index.

  int size4;
  // Size of the fourth index.


protected:
  T sarea[LIGHTNNNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNNNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  lightNNNN(const int, const int, const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // four arguments is the size of the created tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNNN(const lightNNNN<T>&, const lightNNNN<T>&, const lightmat_plus_enum);
  lightNNNN(const lightNNNN<T>&, const T, const lightmat_plus_enum);

#ifdef IN_LIGHTNNNNdouble_H
  lightNNNN(const lightNNNNdouble&, const int, const lightmat_plus_enum);
  lightNNNN(const lightNNNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNNN(const lightNNNN<T>&, const lightNNNN<T>&, const lightmat_minus_enum);
  lightNNNN(const T, const lightNNNN<T>&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNNNdouble_H
  lightNNNN(const int, const lightNNNNdouble&, const lightmat_minus_enum);
  lightNNNN(const double, const lightNNNNint&, const lightmat_minus_enum);
#endif

  lightNNNN(const lightNNNN<T>&, const T, const lightmat_mult_enum);
#ifdef IN_LIGHTNNNNdouble_H
  lightNNNN(const lightNNNNdouble&, const int, const lightmat_mult_enum);
  lightNNNN(const lightNNNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNNN(const lightNNNN<T>&, const lightNN<T>&, const lightmat_mult_enum);
  lightNNNN(const lightNN<T>&, const lightNNNN<T>&, const lightmat_mult_enum);
  lightNNNN(const lightNNNN<T>&, const light33<T>&, const lightmat_mult_enum);
  lightNNNN(const light33<T>&, const lightNNNN<T>&, const lightmat_mult_enum);
  lightNNNN(const lightNNNN<T>&, const light44<T>&, const lightmat_mult_enum);
  lightNNNN(const light44<T>&, const lightNNNN<T>&, const lightmat_mult_enum);
  lightNNNN(const lightNNN<T>&, const lightNNN<T>&, const lightmat_mult_enum);

  lightNNNN(const T, const lightNNNN<T>&, const lightmat_div_enum);
#ifdef IN_LIGHTNNNNdouble_H
  lightNNNN(const int, const lightNNNNdouble&, const lightmat_div_enum);
  lightNNNN(const double, const lightNNNNint&, const lightmat_div_enum);
#endif

  lightNNNN(const lightNNNN<T>&, const lightNNNN<T>&, const lightmat_pow_enum);
  lightNNNN(const lightNNNN<T>&, const T, const lightmat_pow_enum);
  lightNNNN(const T, const lightNNNN<T>&, const lightmat_pow_enum);
 

#ifdef IN_LIGHTNNNNlm_complex_H
 #else
  #ifdef IN_LIGHTNNNNdouble_H
   lightNNNN(const lightNNNN<T>&, const lightmat_abs_enum);
   lightNNNN(const lightNNNN<lm_complex>&, const lightmat_abs_enum);
  #else
  lightNNNN(const lightNNNN<T>&, const lightmat_abs_enum);
  #endif
#endif


  lightNNNN(const lightNNNN<T>&, const lightNNNN<T>&, const lightmat_eprod_enum);
  lightNNNN(const lightNNNN<T>&, const lightNNNN<T>&, const lightmat_equot_enum);
  lightNNNN(const lightNNNN<T>&, T f(T), const lightmat_apply_enum);
  lightNNNN(const lightNNNN<T>&, const lightNNNN<T>&, T f(T, T), const lightmat_apply_enum);
  lightNNNN(const lightNNNN<T>&, const lightmat_trans_enum);
};

typedef lightNNNN<double> doubleNNNN;
typedef lightNNNN<int> intNNNN;
typedef lightNNNN<lm_complex> lm_complexNNNN;

#undef IN_LIGHTNNNN<T>_H

#endif
