//
// sqrt
//
light3<T> sqrt(const light3<T>& s);

light4<T> sqrt(const light4<T>& s);

light33<T> sqrt(const light33<T>& s);

light44<T> sqrt(const light44<T>& s);

lightN<T> sqrt(const lightN<T>& s);

lightNN<T> sqrt(const lightNN<T>& s);

lightN3<T> sqrt(const lightN3<T>& s);

lightNNN<T> sqrt(const lightNNN<T>& s);

lightNNNN<T> sqrt(const lightNNNN<T>& s);


//
// exp
//
light3<T> exp(const light3<T>& s);

light4<T> exp(const light4<T>& s);

light33<T> exp(const light33<T>& s);

light44<T> exp(const light44<T>& s);

lightN<T> exp(const lightN<T>& s);

lightNN<T> exp(const lightNN<T>& s);

lightN3<T> exp(const lightN3<T>& s);

lightNNN<T> exp(const lightNNN<T>& s);

lightNNNN<T> exp(const lightNNNN<T>& s);

//
// log
//
light3<T> log(const light3<T>& s);

light4<T> log(const light4<T>& s);

light33<T> log(const light33<T>& s);

light44<T> log(const light44<T>& s);

lightN<T> log(const lightN<T>& s);

lightNN<T> log(const lightNN<T>& s);

lightN3<T> log(const lightN3<T>& s);

lightNNN<T> log(const lightNNN<T>& s);

lightNNNN<T> log(const lightNNNN<T>& s);


//
// sin
//
light3<T> sin(const light3<T>& s);

light4<T> sin(const light4<T>& s);

light33<T> sin(const light33<T>& s);

light44<T> sin(const light44<T>& s);

lightN<T> sin(const lightN<T>& s);

lightNN<T> sin(const lightNN<T>& s);

lightN3<T> sin(const lightN3<T>& s);

lightNNN<T> sin(const lightNNN<T>& s);

lightNNNN<T> sin(const lightNNNN<T>& s);

//
// cos
//
light3<T> cos(const light3<T>& s);

light4<T> cos(const light4<T>& s);

light33<T> cos(const light33<T>& s);

light44<T> cos(const light44<T>& s);

lightN<T> cos(const lightN<T>& s);

lightNN<T> cos(const lightNN<T>& s);

lightN3<T> cos(const lightN3<T>& s);

lightNNN<T> cos(const lightNNN<T>& s);

lightNNNN<T> cos(const lightNNNN<T>& s);


//
// tan
//
light3<T> tan(const light3<T>& s);

light4<T> tan(const light4<T>& s);

light33<T> tan(const light33<T>& s);

light44<T> tan(const light44<T>& s);

lightN<T> tan(const lightN<T>& s);

lightNN<T> tan(const lightNN<T>& s);

lightN3<T> tan(const lightN3<T>& s);

lightNNN<T> tan(const lightNNN<T>& s);

lightNNNN<T> tan(const lightNNNN<T>& s);

//
// asin
//
light3<T> asin(const light3<T>& s);

light4<T> asin(const light4<T>& s);

light33<T> asin(const light33<T>& s);

light44<T> asin(const light44<T>& s);

lightN<T> asin(const lightN<T>& s);

lightNN<T> asin(const lightNN<T>& s);

lightN3<T> asin(const lightN3<T>& s);

lightNNN<T> asin(const lightNNN<T>& s);

lightNNNN<T> asin(const lightNNNN<T>& s);


//
// acos
//
light3<T> acos(const light3<T>& s);

light4<T> acos(const light4<T>& s);

light33<T> acos(const light33<T>& s);

light44<T> acos(const light44<T>& s);

lightN<T> acos(const lightN<T>& s);

lightNN<T> acos(const lightNN<T>& s);

lightN3<T> acos(const lightN3<T>& s);

lightNNN<T> acos(const lightNNN<T>& s);

lightNNNN<T> acos(const lightNNNN<T>& s);

//
// atan
//
light3<T> atan(const light3<T>& s);

light4<T> atan(const light4<T>& s);

light33<T> atan(const light33<T>& s);

light44<T> atan(const light44<T>& s);

lightN<T> atan(const lightN<T>& s);

lightNN<T> atan(const lightNN<T>& s);

lightN3<T> atan(const lightN3<T>& s);

lightNNN<T> atan(const lightNNN<T>& s);

lightNNNN<T> atan(const lightNNNN<T>& s);

//
// sinh
//
light3<T> sinh(const light3<T>& s);

light4<T> sinh(const light4<T>& s);

light33<T> sinh(const light33<T>& s);

light44<T> sinh(const light44<T>& s);

lightN<T> sinh(const lightN<T>& s);

lightNN<T> sinh(const lightNN<T>& s);

lightN3<T> sinh(const lightN3<T>& s);

lightNNN<T> sinh(const lightNNN<T>& s);

lightNNNN<T> sinh(const lightNNNN<T>& s);

//
// cosh
//
light3<T> cosh(const light3<T>& s);

light4<T> cosh(const light4<T>& s);

light33<T> cosh(const light33<T>& s);

light44<T> cosh(const light44<T>& s);

lightN<T> cosh(const lightN<T>& s);

lightNN<T> cosh(const lightNN<T>& s);

lightN3<T> cosh(const lightN3<T>& s);

lightNNN<T> cosh(const lightNNN<T>& s);

lightNNNN<T> cosh(const lightNNNN<T>& s);

//
// tanh
//
light3<T> tanh(const light3<T>& s);

light4<T> tanh(const light4<T>& s);

light33<T> tanh(const light33<T>& s);

light44<T> tanh(const light44<T>& s);

lightN<T> tanh(const lightN<T>& s);

lightNN<T> tanh(const lightNN<T>& s);

lightN3<T> tanh(const lightN3<T>& s);

lightNNN<T> tanh(const lightNNN<T>& s);

lightNNNN<T> tanh(const lightNNNN<T>& s);

//
// asinh
//
light3<T> asinh(const light3<T>& s);

light4<T> asinh(const light4<T>& s);

light33<T> asinh(const light33<T>& s);

light44<T> asinh(const light44<T>& s);

lightN<T> asinh(const lightN<T>& s);

lightNN<T> asinh(const lightNN<T>& s);

lightN3<T> asinh(const lightN3<T>& s);

lightNNN<T> asinh(const lightNNN<T>& s);

lightNNNN<T> asinh(const lightNNNN<T>& s);

//
// acosh
//
light3<T> acosh(const light3<T>& s);

light4<T> acosh(const light4<T>& s);

light33<T> acosh(const light33<T>& s);

light44<T> acosh(const light44<T>& s);

lightN<T> acosh(const lightN<T>& s);

lightNN<T> acosh(const lightNN<T>& s);

lightN3<T> acosh(const lightN3<T>& s);

lightNNN<T> acosh(const lightNNN<T>& s);

lightNNNN<T> acosh(const lightNNNN<T>& s);

//
// atanh
//
light3<T> atanh(const light3<T>& s);

light4<T> atanh(const light4<T>& s);

light33<T> atanh(const light33<T>& s);

light44<T> atanh(const light44<T>& s);

lightN<T> atanh(const lightN<T>& s);

lightNN<T> atanh(const lightNN<T>& s);

lightN3<T> atanh(const lightN3<T>& s);

lightNNN<T> atanh(const lightNNN<T>& s);

lightNNNN<T> atanh(const lightNNNN<T>& s);

