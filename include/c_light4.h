//           -*- c++ -*-

#ifndef LIGHT4_C_H
#define LIGHT4_C_H
// <cd> light4lm_complex
//
// .SS Functionality
//
// light4 is a template for classes that implement vectors with 4
// elements. E.g. a vector v with 4 elements of type double can be
// instanciated with:
//
// <code>light4&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#define IN_LIGHT4lm_complex_C_H

 class lightN;
 class light44;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light4lm_complex {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light4double();
  // Convert to double.
#else
#endif
#ifdef LIGHTMAT_TEMPLATES
  friend class light4int;
#endif

#ifdef IN_LIGHT4double_C_H
  friend class light4int;
#else
  friend class light4double;
#endif

  #include "c_light4_auto.h"

  light4lm_complex();
  // Default constructor.

  light4lm_complex(const light4lm_complex&);
  // Copy constructor.
  
  light4lm_complex(const lm_complex, const lm_complex, const lm_complex, const lm_complex);
  // Initialize elements with values.

  light4lm_complex(const lm_complex *);
  // Initialize elements with values from an array.

  light4lm_complex(const lm_complex);
  // Initialize all elements with the same value.

  light4lm_complex& operator=(const light4lm_complex& s);
  // Assignment.
  
  light4lm_complex& operator=(const lightNlm_complex& s);
  // Assignment from a lightN where N=3.

  light4lm_complex& operator=(const lm_complex);
  // Assign one value to all elements.

  lm_complex operator()(const int x) const {
    limiterror((x<1) || (x>4));
    return elem[x-1];
  };
  // Get the value of one element.

  lm_complex& operator()(const int x) {
    limiterror((x<1) || (x>4));
    return elem[x-1];
  };
  // Get/Set the value of one element.
  int operator==(const light4lm_complex&) const;
  // Equality.

  int operator!=(const light4lm_complex&) const;
  // Inequality.

  light4lm_complex& operator+=(const lm_complex);
  // Add a value to all elements.

  light4lm_complex& operator+=(const light4lm_complex&);
  // Elementwise addition.

  light4lm_complex& operator-=(const lm_complex);
  // Subtract a value from all elements.

  light4lm_complex& operator-=(const light4lm_complex&);
  // Elementwise subtraction.

  light4lm_complex& operator*=(const lm_complex);
  // Muliply all elements with a value.

  light4lm_complex& operator/=(const lm_complex);
  // Divide all elements with a value.
 
  #ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
  #endif
  int dimension(const int x = 1) const {
    if(x == 1)
      return 4;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 4). If this was a lightNlm_complex
  // then dimension(1) would return the number of elements.
 
  #ifndef COMPLEX_TOOLS
  light4lm_complex& normalize();
  // Normalize vector.
  #endif

  void Get(lm_complex *) const;
  // Get values of all elements and put them in an array (4 elements long).

  void Set(const lm_complex *);
  // Set values of all elements from array (4 elements long).
 
  light4lm_complex operator+() const;
  // Unary plus.

  light4lm_complex operator-() const;
  // Unary minus.

  friend inline light4lm_complex operator+(const light4lm_complex&, const light4lm_complex&);
  // Elementwise addition.

  friend inline light4lm_complex operator+(const light4lm_complex&, const lm_complex);
  // Addition to all elements.

  friend inline light4lm_complex operator+(const lm_complex, const light4lm_complex&);
  // Addition to all elements.

  friend inline light4lm_complex operator-(const light4lm_complex&, const light4lm_complex&);
  // Elementwise subtraction.

  friend inline light4lm_complex operator-(const light4lm_complex&, const lm_complex);
  // Subtraction from all elements.

  friend inline light4lm_complex operator-(const lm_complex, const light4lm_complex&);
  // Subtraction to all elements.

  friend inline lm_complex operator*(const light4lm_complex&, const light4lm_complex&);
  // Inner product.

  friend inline light4lm_complex operator*(const light4lm_complex&, const lm_complex);
  // Multiply all elements.

  friend inline light4lm_complex operator*(const lm_complex, const light4lm_complex&);
  // Multiply all elements.

  friend inline light4lm_complex operator*(const light4lm_complex&, const light44lm_complex&);
  // Inner product.

  friend inline light4lm_complex operator*(const light44lm_complex&, const light4lm_complex&);
  // Inner product.

  friend inline lightNlm_complex operator*(const light4lm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline light4lm_complex operator/(const light4lm_complex&, const lm_complex);
  // Divide all elements.

  friend inline light4lm_complex operator/(const lm_complex, const light4lm_complex&);
  // Divide with all elements.

  friend inline light4lm_complex pow(const light4lm_complex&, const light4lm_complex&);
  // Raise to the power of-function, elementwise.

  friend inline light4lm_complex pow(const light4lm_complex&, const lm_complex);
  // Raise to the power of-function, for all elements.

  friend inline light4lm_complex pow(const lm_complex, const light4lm_complex&);
  // Raise to the power of-function, for all elements.

  friend inline light4lm_complex ElemProduct(const light4lm_complex&, const light4lm_complex&);
  // Elementwise multiplication.

  friend inline light4lm_complex ElemQuotient(const light4lm_complex&, const light4lm_complex&);
  // Elementwise division.

  friend inline light4lm_complex Apply(const light4lm_complex&, lm_complex f(lm_complex));
  // Apply the function elementwise on all four elements.

  friend inline light4lm_complex Apply(const light4lm_complex&, const light4lm_complex& ,lm_complex f(lm_complex, lm_complex));
  // Apply the function elementwise on all four elements in the two
  // vecors.

  friend inline light44lm_complex OuterProduct(const light4lm_complex&, const light4lm_complex&);
  // Outer product.

  friend inline light4lm_complex abs(const light4lm_complex&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light4int sign(const light4lm_complex&);
  // sign
#else
  friend inline light4lm_complex sign(const light4lm_complex&);
#endif
  friend inline light4int ifloor(const light4double&);
  // ifloor

  friend inline light4int iceil(const light4double&);
  // iceil

  friend inline light4int irint(const light4double&);
  // irint

  friend inline light4double sqrt(const light4double&);
  // sqrt

  friend inline light4double exp(const light4double&);
  // exp

  friend inline light4double log(const light4double&);
  // log

  friend inline light4double sin(const light4double&);
  // sin

  friend inline light4double cos(const light4double&);
  // cos

  friend inline light4double tan(const light4double&);
  // tan

  friend inline light4double asin(const light4double&);
  // asin

  friend inline light4double acos(const light4double&);
  // acos

  friend inline light4double atan(const light4double&);
  // atan

  friend inline light4double sinh(const light4double&);
  // sinh

  friend inline light4double cosh(const light4double&);
  // cosh

  friend inline light4double tanh(const light4double&);
  // tanh

  friend inline light4double asinh(const light4double&);
  // asinh

  friend inline light4double acosh(const light4double&);
  // acosh

  friend inline light4double atanh(const light4double&);
  // atanh

  friend inline light4lm_complex ifloor(const light4lm_complex&);
  // ifloor

  friend inline light4lm_complex iceil(const light4lm_complex&);
  // iceil

  friend inline light4lm_complex irint(const light4lm_complex&);
  // irint

  friend inline light4lm_complex sqrt(const light4lm_complex&);
  // sqrt

  friend inline light4lm_complex exp(const light4lm_complex&);
  // exp

  friend inline light4lm_complex log(const light4lm_complex&);
  // log

  friend inline light4lm_complex sin(const light4lm_complex&);
  // sin

  friend inline light4lm_complex cos(const light4lm_complex&);
  // cos

  friend inline light4lm_complex tan(const light4lm_complex&);
  // tan

  friend inline light4lm_complex asin(const light4lm_complex&);
  // asin

  friend inline light4lm_complex acos(const light4lm_complex&);
  // acos

  friend inline light4lm_complex atan(const light4lm_complex&);
  // atan

  friend inline light4lm_complex sinh(const light4lm_complex&);
  // sinh

  friend inline light4lm_complex cosh(const light4lm_complex&);
  // cosh

  friend inline light4lm_complex tanh(const light4lm_complex&);
  // tanh

  friend inline light4lm_complex asinh(const light4lm_complex&);
  // asinh

  friend inline light4lm_complex acosh(const light4lm_complex&);
  // acosh

  friend inline light4lm_complex atanh(const light4lm_complex&);
  // atanh


  //<ignore>
//    friend class light4int;
  friend class lightNlm_complex;
  friend class light44lm_complex;
  friend class lightNNlm_complex;
  friend class lightNNNlm_complex;
  friend class lightNNNNlm_complex;

  friend inline lightNlm_complex light_join (const lightNlm_complex arr1, const lightNlm_complex arr2);

  //</ignore>

protected:
  lm_complex elem[4];
  // The values of the four elements.

  light4lm_complex(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The size of the vector (number of elements).


};

typedef light4double double4;
typedef light4int int4;

#undef IN_LIGHT4lm_complex_C_H

#endif
