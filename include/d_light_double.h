//
// routines which use BLAS
//
#ifndef LIGHTMAT_DONT_USE_BLAS
// These functions are overloaded variants of the ones in light_basic.h

double light_dot(const int n, const double *x, const double *y);

double light_dot(const int n, const double *x, const int ix, const double *y);

void light_gemv(const int m, const int n, const double *a, const double *x,
		double *y);

void light_gevm(const int m, const int n, const double *x, const double *a,
		double *y, const int iy = 1);

void light_gemm(const int m, const int n, const int k,
		const double *a, const double *b, double *c);

#endif // ! LIGHTMAT_DONT_USE_BLAS

//
// fractional part
//
inline double FractionalPart(const double s);
inline lightNdouble  FractionalPart(const lightNdouble&);
inline lightNNdouble  FractionalPart(const lightNNdouble&);
inline lightNNNdouble  FractionalPart(const lightNNNdouble&);
inline lightNNNNdouble  FractionalPart(const lightNNNNdouble&);

#ifndef C_LIGHT_DOUBLE
inline double FractionalPart(const int );
inline lightNint  FractionalPart(const lightNint&);
inline lightNNint  FractionalPart(const lightNNint&);
inline lightNNNint  FractionalPart(const lightNNNint&);
inline lightNNNNint  FractionalPart(const lightNNNNint&);
#endif

// IntegerPart
#ifndef C_LIGHT_DOUBLE
lightNint  IntegerPart(const lightNint&);
lightNNint  IntegerPart(const lightNNint&);
lightNNNint  IntegerPart(const lightNNNint&);
lightNNNNint  IntegerPart(const lightNNNNint&);
#endif

inline double IntegerPart(const double s); 
lightNdouble IntegerPart(const lightNdouble&);
lightNNdouble  IntegerPart(const lightNNdouble&);
lightNNNdouble  IntegerPart(const lightNNNdouble&);
lightNNNNdouble  IntegerPart(const lightNNNNdouble&);

//
// abs
//

lightNdouble abs(const lightNdouble& s);


lightN3double abs(const lightN3double& s);


lightNNdouble abs(const lightNNdouble& s);


lightNNNdouble abs(const lightNNNdouble& s);


lightNNNNdouble abs(const lightNNNNdouble& s);

// Mod

#ifndef C_LIGHT_DOUBLE
inline int Mod(const int m , const int n);
#endif

inline double Mod(const double m, const double n);
inline double Mod(const double m, const int n);
inline double Mod(const int m, const double n);

/*
lightNdouble Mod(const lightNint & i, const lightNdouble & d);
lightNdouble Mod(const lightNdouble & i, const lightNint & d);
lightNNdouble Mod(const lightNNint & i, const lightNNdouble & d);
lightNNdouble Mod(const lightNNdouble & i, const lightNNint & d);
lightNNNdouble Mod(const lightNNNdouble & i, const lightNNNint & d);
lightNNNdouble Mod(const lightNNNint & i, const lightNNNdouble & d);
lightNNNNdouble Mod(const lightNNNNdouble & i, const lightNNNNint & d);
lightNNNNdouble Mod(const lightNNNNint & i, const lightNNNNdouble & d);
*/




