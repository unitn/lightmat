//           -*- c++ -*-

#ifndef LIGHTN_I_H
#define LIGHTN_I_H
// <cd> lightNint
//
// .SS Functionality
//
// lightN is a template for classes that implement vectors with any
// number of elements. E.g. a vector v with 5 elements of type double
// can be instanciated with:
//
// <code>lightN&lt;double&gt; v(5);</code>
//
// The number of elements in the vector can change during
// execution. It changes its size to whatever is needed when it is
// assigned a new value with an assignment operator.
//
// When a lightN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightN object with four elements to one with five elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//
#define IN_LIGHTNint_I_H

#define LIGHTN_SIZE 10
// The default size.

 class light3;
 class light4;
 class lightN;
 class lightN3;
 class lightN33;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class lightNint {
public:

#ifdef IN_LIGHTNdouble_I_H
  friend class lightNint;
#else
  friend class lightNdouble;
#endif

#ifdef IN_LIGHTNint_I_H
#define TDtypeH  double
#define TDNtypeH lightNdouble
#else
#define TDtypeH int
#define TDNtypeH lightNint
#endif


#ifdef LIGHTMAT_TEMPLATES
  // friend class lightNint;
#endif

  #include "i_lightN_auto.h"

  inline lightNint();
  // Default constructor.

  inline lightNint(const lightNint&);
  // Copy constructor.
  
  inline lightNint(const light3int&);
  // Conversion.

  inline lightNint(const light4int&);
  // Conversion.

  inline lightNint(const int);
  // Construct a lightN of given size.

  inline lightNint(const int, const int *);
  // Construct a lightN of given size and initialize elements with values
  // from an array.

  inline lightNint(const int, const int);
  // Construct a lightN of given size and initialize all elements
  // with a value (the second argument).

  inline ~lightNint();
  // Destructor.


#ifdef CONV_INT_2_DOUBLE
  operator lightNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNlm_complex();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNlm_complex();
  // Convert to complex.
#endif


  lightNint& operator=(const lightNint&);
  // Assignment.

  lightNint& operator=(const light3int&);
  // Assignment, change size to 3 elements.

  lightNint& operator=(const light4int&);
  // Assignment, change size to 4 elements.

  lightNint& operator=(const int);
  // Assign one value to all elements.

  //
  // operator()
  //

  int operator() (const int x) const {
    limiterror((x<1) || (x>size1));
    return elem[x-1];
  };
  // Get the value of one element.

  int& operator()(const int x) {
    limiterror((x<1) || (x>size1));
    return elem[x-1];
  };
  // Get/Set the value of one element.
 
  inline int& takeElementReference(const int);
  // Same as above

  inline lightNint& SetShape(const int x=-1);
  // Sets the specified size. 

//REMOVE LATER
  lightNint SubVector(const int n1,const  int n2) const; 
  // Gets specified interval of the vector.

  const lightNint& SetSubVector(const int n1,const  int n2,const lightNint& x);
  // Puts all  elements of x into specified interval of  the current vector.
  // Returns the vector x.

  int SetSubVector(const int n1,const  int n2,const int x);
  // Puts the   element x into all elements of specified interval of  the current vector.
  // Returns x.

  int operator==(const lightNint&) const;
  // Equality.

  int operator!=(const lightNint&) const;
  // Inequality.
 
  lightNint& operator+=(const int);
  // Add a value to all elements.

  lightNint& operator+=(const lightNint&);
  // Elementwise addition.

  lightNint& operator-=(const int);
  // Subtract a value from all elements.

  lightNint& operator-=(const lightNint&);
  // Elementwise subtraction.

  lightNint& operator*=(const int);
  // Multiply all elements with a value.

  lightNint& operator/=(const int);
  // Divide all elements with a value.

#ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
#endif

  int Extract(const lightNint&) const;
  // Extract an element using given index.   

  int dimension(const int x = 1) const {
    if(x == 1)
      return size1;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == no. elements).

  #ifndef COMPLEX_TOOLS
  lightNint& normalize();
  // Normalize vector. Returns a reference to itself.
  #endif

  void Get(int *) const;
  // Get values of all elements and put them in an array.

  void Set(const int *);
  // Set values of all elements from array.

  int * data() const;
  // Direct access to the stored data. Use the result with care.

  lightNint operator+() const;
  // Unary plus.

  lightNint operator-() const;
  // Unary minus.


  //<ignore>
#ifdef IN_LIGHTNlm_complex_I_H
  friend class lightNint;
#endif
  friend class light3int;
  friend class light4int;
  friend class lightN3int;
  friend class lightNNint;
  friend class lightNNNint;
  friend class lightNNNNint;
  friend class lightN33int;
  //</ignore>

  friend inline lightNint operator+(const lightNint&, const lightNint&);
  // Elementwise addition.

  //--
  friend inline lightNint operator+(const lightNint&, const int);
  // Addition to all elements.

  friend inline lightNint operator+(const int, const lightNint&);
  // Addition to all elements.

#ifdef IN_LIGHTNdouble_I_H
  friend inline lightNdouble operator+(const lightNdouble&, const int);
  friend inline lightNdouble operator+(const lightNint&, const double);
  friend inline lightNdouble operator+(const double, const lightNint&);
  friend inline lightNdouble operator+(const int, const lightNdouble&);
#endif

#ifdef IN_LIGHTNlm_complex_I_H
friend inline lightNlm_complex operator+(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator+(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator+(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator+(const double e, const lightNlm_complex& s2);
#endif

  //--

  friend inline lightNint operator-(const lightNint&, const lightNint&);
  // Elementwise subtraction.

  friend inline lightNint operator-(const lightNint&, const int);
  // Subtraction from all elements.

  friend inline lightNint operator-(const int, const lightNint&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNdouble_I_H
  friend inline lightNdouble operator-(const lightNdouble&, const int);
  friend inline lightNdouble operator-(const lightNint&, const double);
  friend inline lightNdouble operator-(const double, const lightNint&);
  friend inline lightNdouble operator-(const int, const lightNdouble&);
#endif

  #ifdef IN_LIGHTNlm_complex_I_H
friend inline lightNlm_complex operator-(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator-(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator-(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator-(const double e, const lightNlm_complex& s2);
#endif


  friend inline int operator*(const lightNint&, const lightNint&);
  // Inner product.

  friend inline lightNint operator*(const lightNint&, const int);
  // Multiply all elements.

  friend inline lightNint operator*(const int, const lightNint&);
  // Multiply all elements.

#ifdef IN_LIGHTNdouble_I_H
  friend inline lightNdouble operator*(const lightNdouble&, const int);
  friend inline lightNdouble operator*(const lightNint&, const double);
  friend inline lightNdouble operator*(const double, const lightNint&);
  friend inline lightNdouble operator*(const int, const lightNdouble&);
#endif

#ifdef IN_LIGHTNlm_complex_I_H
friend inline lightNlm_complex operator*(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator*(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator*(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator*(const double e, const lightNlm_complex& s2);
#endif

  friend inline lightNint operator*(const lightNint&, const lightNNint&);
  // Inner product.

  friend inline lightNint operator*(const light3int&, const lightNNint&);
  // Inner product.

  friend inline lightNint operator*(const light4int&, const lightNNint&);
  // Inner product.

  friend inline lightNint operator*(const lightNNint&,const lightNint&);
  // Inner product.

  friend inline lightNint operator*(const lightNNint&, const light3int&);
  // Inner product.

  friend inline lightNint operator*(const lightNNint&, const light4int&);
  // Inner product.

  friend inline lightNint operator*(const lightN3int&, const light3int&);
  // Inner product.

  friend inline lightNint operator*(const lightN3int&, const lightNint&);
  // Inner product.

  friend inline lightNint operator/(const lightNint&, const int);
  // Divide all elements.

  friend inline lightNint operator/(const int, const lightNint&);
  // Divide all elements.

#ifdef IN_LIGHTNdouble_I_H
  friend inline lightNdouble operator/(const lightNdouble&, const int);
  friend inline lightNdouble operator/(const lightNint&, const double);
  friend inline lightNdouble operator/(const double, const lightNint&);
  friend inline lightNdouble operator/(const int, const lightNdouble&);
#endif

#ifdef IN_LIGHTNlm_complex_I_H
friend inline lightNlm_complex operator/(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator/(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator/(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator/(const double e, const lightNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNlm_complex_I_H
friend inline lightNdouble arg(const lightNlm_complex& a);
friend inline lightNdouble re(const lightNlm_complex& a);
friend inline lightNdouble im(const lightNlm_complex& a);
friend inline lightNlm_complex conjugate(const lightNlm_complex& a);
//#endif

  friend inline lightNint pow(const lightNint&, const lightNint&);
  // Raise to the power of-function, elementwise.

  friend inline lightNint pow(const lightNint&, const int);
  // Raise to the power of-function, for all elements.

  friend inline lightNint pow(const int, const lightNint&);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex pow(const lightNlm_complex& s1, const double e);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex pow( const double e,const lightNlm_complex& s1);
  // Raise to the power of-function, for all elements.

  friend inline lightNint ElemProduct(const lightNint&, const lightNint&);
  // Elementwise multiplication.

  friend inline lightNint ElemQuotient(const lightNint&, const lightNint&);
  // Elementwise division.

  friend inline lightNint Apply(const lightNint&, int f(int));
  // Apply the function elementwise all elements.

  friend inline lightNint Apply(const lightNint&, const lightNint&, int f(int, int));
  // Apply the function elementwise on all elements in the two vectors.


#ifdef IN_LIGHTNlm_complex_I_H
#else
  friend inline lightNint abs(const lightNint&);
#ifdef IN_LIGHTNdouble_I_H
  friend inline lightNdouble abs(const lightNlm_complex&);
#endif 
#endif

#ifndef COMPLEX_TOOLS
  // sign
  friend inline lightNint sign(const lightNint&);  
  friend inline lightNint sign(const lightNdouble&);
#else
  friend inline lightNlm_complex sign(const lightNlm_complex&);
#endif
  /// Added 2/2/98 

  friend inline lightNint  FractionalPart(const lightNint&);
  //  FractionalPart

  //old version
  //friend inline lightNint  IntegerPart(const lightNint&);
  //  IntegerPart

  //friend inline lightNint  IntegerPart(const lightNdouble&);
  //  IntegerPart

  //new version
  friend inline lightNint  IntegerPart(const lightNint&);
  //  IntegerPart

  friend inline lightNint Mod (const  lightNint&,const  lightNint&);
  // Mod
 
  friend inline lightNint Mod (const  lightNint&,const int);
  // Mod

  friend inline lightNint Mod (const int,const  lightNint&);
  // Mod

  friend inline int LightMax (const lightNint& );
  // Max

  friend inline int LightMin (const lightNint& );
  // Min

  friend inline int findLightMax (const int *,const int );
  // Find Max
 
  friend inline int findLightMin (const  int *,const  int );
  // Find Min
 
  /// End Added 

  friend inline lightNint ifloor(const lightNdouble&);
  // ifloor

  friend inline lightNint iceil(const lightNdouble&);
  // iceil

  friend inline lightNint irint(const lightNdouble&);
  // irint

  friend inline lightNdouble sqrt(const lightNdouble&);
  // sqrt

  friend inline lightNdouble exp(const lightNdouble&);
  // exp

  friend inline lightNdouble log(const lightNdouble&);
  // log

  friend inline lightNdouble sin(const lightNdouble&);
  // sin

  friend inline lightNdouble cos(const lightNdouble&);
  // cos

  friend inline lightNdouble tan(const lightNdouble&);
  // tan

  friend inline lightNdouble asin(const lightNdouble&);
  // asin

  friend inline lightNdouble acos(const lightNdouble&);
  // acos

  friend inline lightNdouble atan(const lightNdouble&);
  // atan

  friend inline lightNdouble sinh(const lightNdouble&);
  // sinh

  friend inline lightNdouble cosh(const lightNdouble&);
  // cosh

  friend inline lightNdouble tanh(const lightNdouble&);
  // tanh

  friend inline lightNdouble asinh(const lightNdouble&);
  // asinh

  friend inline lightNdouble acosh(const lightNdouble&);
  // acosh

  friend inline lightNdouble atanh(const lightNdouble&);
  // atanh

  friend inline lightNlm_complex sqrt(const lightNlm_complex&);
  // sqrt

  friend inline lightNlm_complex exp(const lightNlm_complex&);
  // exp

  friend inline lightNlm_complex ifloor(const lightNlm_complex&);
  // ifloor

  friend inline lightNlm_complex iceil(const lightNlm_complex&);
  // iceil

  friend inline lightNlm_complex irint(const lightNlm_complex&);
  // irint

  friend inline lightNlm_complex log(const lightNlm_complex&);
  // log

  friend inline lightNlm_complex sin(const lightNlm_complex&);
  // sin

  friend inline lightNlm_complex cos(const lightNlm_complex&);
  // cos

  friend inline lightNlm_complex tan(const lightNlm_complex&);
  // tan

  friend inline lightNlm_complex asin(const lightNlm_complex&);
  // asin

  friend inline lightNlm_complex acos(const lightNlm_complex&);
  // acos

  friend inline lightNlm_complex atan(const lightNlm_complex&);
  // atan

  friend inline lightNlm_complex sinh(const lightNlm_complex&);
  // sinh

  friend inline lightNlm_complex cosh(const lightNlm_complex&);
  // cosh

  friend inline lightNlm_complex tanh(const lightNlm_complex&);
  // tanh

  friend inline lightNlm_complex asinh(const lightNlm_complex&);
  // asinh

  friend inline lightNlm_complex acosh(const lightNlm_complex&);
  // acosh

  friend inline lightNlm_complex atanh(const lightNlm_complex&);
  // atanh


  friend inline lightNint Cross (const  lightNint&, const  lightNint&); 
  friend inline lightNint Cross (const  lightNint&, const  lightNint&, const  lightNint&); 
  friend inline lightNint Cross (const  lightNint&, const  lightNint&, const  lightNint&, const  lightNint&); 

  friend inline lightNint light_join (const lightNint arr1, const lightNint arr2);
  friend inline lightNint light_drop (const lightNint arr1, const R r);

#ifdef IN_LIGHTNint_I_H
  friend inline double light_mean     ( const  lightNint& );
  friend inline double light_standard_deviation( const  lightNint& );
  friend inline double light_variance ( const  lightNint& );
  friend inline double light_median   ( const  lightNint& );
#else
  friend inline int light_mean     ( const  lightNint& );
  friend inline int light_variance ( const  lightNint& );
  friend inline int light_standard_deviation( const  lightNint& );
  friend inline int light_median   ( const  lightNint& );
#endif
  
  friend inline int light_total    ( const  lightNint& );
 
 
  friend inline lightNint light_sort (const lightNint arr1);

  friend inline TDNtypeH light_quantile3L (const lightNint arr1, const lightNdouble q2, const lightNNdouble params);
  friend inline TDtypeH light_quantile3 (const lightNint arr1, double q,  const lightNNdouble params);
  friend inline TDNtypeH light_quantile2L (const lightNint arr1, const lightNdouble q2);
  friend inline TDtypeH light_quantile2 (const lightNint arr1, double q);

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNdouble::lightNint(const lightNint& s1, const double e, const lightmat_plus_enum)

  int *elem;
  // elem always points at the area where the elements are.


protected:
  int sarea[LIGHTN_SIZE];
  // The values are stored here if they fit into LIGHTN_SIZE
  // else a memory area is allocated.

  int size1;
  // The size of the vector (number of elements).

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNint(const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // argument is the size of the created vector. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNint(const lightNint&, const lightNint&, const lightmat_plus_enum);
  lightNint(const lightNint&, const int, const lightmat_plus_enum);
#ifdef IN_LIGHTNdouble_I_H
  lightNint(const lightNdouble&, const int, const lightmat_plus_enum);
  lightNint(const lightNint&, const double, const lightmat_plus_enum);
#endif

  lightNint(const lightNint&, const lightNint&, const lightmat_minus_enum);
  lightNint(const int, const lightNint&, const lightmat_minus_enum);
#ifdef IN_LIGHTNdouble_I_H
  lightNint(const int, const lightNdouble&, const lightmat_minus_enum);
  lightNint(const double, const lightNint&, const lightmat_minus_enum);
#endif

  lightNint(const lightNint&, const lightNint&, const lightmat_mult_enum);
  lightNint(const lightNint&, const int, const lightmat_mult_enum);
#ifdef IN_LIGHTNdouble_I_H
  lightNint(const lightNdouble&, const int, const lightmat_mult_enum);
  lightNint(const lightNint&, const double, const lightmat_mult_enum);
#endif

  lightNint(const lightNint&, const lightNNint&, const lightmat_mult_enum);
  lightNint(const lightNNint&, const lightNint&, const lightmat_mult_enum);
  lightNint(const light3int&, const lightNNint&, const lightmat_mult_enum);
  lightNint(const light4int&, const lightNNint&, const lightmat_mult_enum);
  lightNint(const lightN3int&, const light3int&, const lightmat_mult_enum);
  lightNint(const lightN3int&, const lightNint&, const lightmat_mult_enum);
  lightNint(const lightNNint&, const light3int&, const lightmat_mult_enum);
  lightNint(const lightNNint&, const light4int&, const lightmat_mult_enum);

  lightNint(const int, const lightNint&, const lightmat_div_enum);
#ifdef IN_LIGHTNdouble_I_H
  lightNint(const int, const lightNdouble&, const lightmat_div_enum);
  lightNint(const double, const lightNint&, const lightmat_div_enum);
#endif

  lightNint(const lightNint&, const lightNint&, const lightmat_pow_enum);
  lightNint(const lightNint&, const int, const lightmat_pow_enum);
  lightNint(const int, const lightNint&, const lightmat_pow_enum);

#ifdef IN_LIGHTNlm_complex_I_H
  lightNlm_complex(const lightNlm_complex& s1, const double e, const lightmat_pow_enum) ;
  lightNlm_complex(const double e,const lightNlm_complex& s1, const lightmat_pow_enum) ;
#endif

#ifdef IN_LIGHTNlm_complex_I_H
 #else
  #ifdef IN_LIGHTNdouble_I_H
   lightNint(const lightNint&, const lightmat_abs_enum);
   lightNint(const lightNlm_complex&, const lightmat_abs_enum);
  #else
  lightNint(const lightNint&, const lightmat_abs_enum);
  #endif
#endif

  lightNint(const lightNint&, const lightNint&, const lightmat_eprod_enum);
  lightNint(const lightNint&, const lightNint&, const lightmat_equot_enum);
  lightNint(const lightNint&, int f(int), const lightmat_apply_enum);
  lightNint(const lightNint&, const lightNint&, int f(int, int), const lightmat_apply_enum);

};





 inline int findLightMax (const int *,const int );
// Find Max

 inline int findLightMin (const  int *,const  int );
// Find Min








typedef lightNdouble doubleN;
typedef lightNint intN;
typedef lightNlm_complex lm_complexN;

#undef TDtypeH  
#undef TDNtypeH 

#undef IN_LIGHTNint_I_H

#endif
