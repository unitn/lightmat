//           -*- c++ -*-

#ifndef LIGHT4_H
#define LIGHT4_H
// <cd> light4double
//
// .SS Functionality
//
// light4 is a template for classes that implement vectors with 4
// elements. E.g. a vector v with 4 elements of type double can be
// instanciated with:
//
// <code>light4&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#define IN_LIGHT4double_H

 class lightN;
 class light44;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light4double {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light4double();
  // Convert to double.
#else
#endif
#ifdef LIGHTMAT_TEMPLATES
  friend class light4int;
#endif

#ifdef IN_LIGHT4double_H
  friend class light4int;
#else
  friend class light4double;
#endif

  #include "d_light4_auto.h"

  light4double();
  // Default constructor.

  light4double(const light4double&);
  // Copy constructor.
  
  light4double(const double, const double, const double, const double);
  // Initialize elements with values.

  light4double(const double *);
  // Initialize elements with values from an array.

  light4double(const double);
  // Initialize all elements with the same value.

  light4double& operator=(const light4double& s);
  // Assignment.
  
  light4double& operator=(const lightNdouble& s);
  // Assignment from a lightN where N=3.

  light4double& operator=(const double);
  // Assign one value to all elements.

  double operator()(const int x) const {
    limiterror((x<1) || (x>4));
    return elem[x-1];
  };
  // Get the value of one element.

  double& operator()(const int x) {
    limiterror((x<1) || (x>4));
    return elem[x-1];
  };
  // Get/Set the value of one element.
  int operator==(const light4double&) const;
  // Equality.

  int operator!=(const light4double&) const;
  // Inequality.

  light4double& operator+=(const double);
  // Add a value to all elements.

  light4double& operator+=(const light4double&);
  // Elementwise addition.

  light4double& operator-=(const double);
  // Subtract a value from all elements.

  light4double& operator-=(const light4double&);
  // Elementwise subtraction.

  light4double& operator*=(const double);
  // Muliply all elements with a value.

  light4double& operator/=(const double);
  // Divide all elements with a value.
 
  #ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
  #endif
  int dimension(const int x = 1) const {
    if(x == 1)
      return 4;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 4). If this was a lightNdouble
  // then dimension(1) would return the number of elements.
 
  #ifndef COMPLEX_TOOLS
  light4double& normalize();
  // Normalize vector.
  #endif

  void Get(double *) const;
  // Get values of all elements and put them in an array (4 elements long).

  void Set(const double *);
  // Set values of all elements from array (4 elements long).
 
  light4double operator+() const;
  // Unary plus.

  light4double operator-() const;
  // Unary minus.

  friend inline light4double operator+(const light4double&, const light4double&);
  // Elementwise addition.

  friend inline light4double operator+(const light4double&, const double);
  // Addition to all elements.

  friend inline light4double operator+(const double, const light4double&);
  // Addition to all elements.

  friend inline light4double operator-(const light4double&, const light4double&);
  // Elementwise subtraction.

  friend inline light4double operator-(const light4double&, const double);
  // Subtraction from all elements.

  friend inline light4double operator-(const double, const light4double&);
  // Subtraction to all elements.

  friend inline double operator*(const light4double&, const light4double&);
  // Inner product.

  friend inline light4double operator*(const light4double&, const double);
  // Multiply all elements.

  friend inline light4double operator*(const double, const light4double&);
  // Multiply all elements.

  friend inline light4double operator*(const light4double&, const light44double&);
  // Inner product.

  friend inline light4double operator*(const light44double&, const light4double&);
  // Inner product.

  friend inline lightNdouble operator*(const light4double&, const lightNNdouble&);
  // Inner product.

  friend inline light4double operator/(const light4double&, const double);
  // Divide all elements.

  friend inline light4double operator/(const double, const light4double&);
  // Divide with all elements.

  friend inline light4double pow(const light4double&, const light4double&);
  // Raise to the power of-function, elementwise.

  friend inline light4double pow(const light4double&, const double);
  // Raise to the power of-function, for all elements.

  friend inline light4double pow(const double, const light4double&);
  // Raise to the power of-function, for all elements.

  friend inline light4double ElemProduct(const light4double&, const light4double&);
  // Elementwise multiplication.

  friend inline light4double ElemQuotient(const light4double&, const light4double&);
  // Elementwise division.

  friend inline light4double Apply(const light4double&, double f(double));
  // Apply the function elementwise on all four elements.

  friend inline light4double Apply(const light4double&, const light4double& ,double f(double, double));
  // Apply the function elementwise on all four elements in the two
  // vecors.

  friend inline light44double OuterProduct(const light4double&, const light4double&);
  // Outer product.

  friend inline light4double abs(const light4double&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light4int sign(const light4double&);
  // sign
#else
  friend inline light4lm_complex sign(const light4lm_complex&);
#endif
  friend inline light4int ifloor(const light4double&);
  // ifloor

  friend inline light4int iceil(const light4double&);
  // iceil

  friend inline light4int irint(const light4double&);
  // irint

  friend inline light4double sqrt(const light4double&);
  // sqrt

  friend inline light4double exp(const light4double&);
  // exp

  friend inline light4double log(const light4double&);
  // log

  friend inline light4double sin(const light4double&);
  // sin

  friend inline light4double cos(const light4double&);
  // cos

  friend inline light4double tan(const light4double&);
  // tan

  friend inline light4double asin(const light4double&);
  // asin

  friend inline light4double acos(const light4double&);
  // acos

  friend inline light4double atan(const light4double&);
  // atan

  friend inline light4double sinh(const light4double&);
  // sinh

  friend inline light4double cosh(const light4double&);
  // cosh

  friend inline light4double tanh(const light4double&);
  // tanh

  friend inline light4double asinh(const light4double&);
  // asinh

  friend inline light4double acosh(const light4double&);
  // acosh

  friend inline light4double atanh(const light4double&);
  // atanh

  friend inline light4lm_complex ifloor(const light4lm_complex&);
  // ifloor

  friend inline light4lm_complex iceil(const light4lm_complex&);
  // iceil

  friend inline light4lm_complex irint(const light4lm_complex&);
  // irint

  friend inline light4lm_complex sqrt(const light4lm_complex&);
  // sqrt

  friend inline light4lm_complex exp(const light4lm_complex&);
  // exp

  friend inline light4lm_complex log(const light4lm_complex&);
  // log

  friend inline light4lm_complex sin(const light4lm_complex&);
  // sin

  friend inline light4lm_complex cos(const light4lm_complex&);
  // cos

  friend inline light4lm_complex tan(const light4lm_complex&);
  // tan

  friend inline light4lm_complex asin(const light4lm_complex&);
  // asin

  friend inline light4lm_complex acos(const light4lm_complex&);
  // acos

  friend inline light4lm_complex atan(const light4lm_complex&);
  // atan

  friend inline light4lm_complex sinh(const light4lm_complex&);
  // sinh

  friend inline light4lm_complex cosh(const light4lm_complex&);
  // cosh

  friend inline light4lm_complex tanh(const light4lm_complex&);
  // tanh

  friend inline light4lm_complex asinh(const light4lm_complex&);
  // asinh

  friend inline light4lm_complex acosh(const light4lm_complex&);
  // acosh

  friend inline light4lm_complex atanh(const light4lm_complex&);
  // atanh


  //<ignore>
//    friend class light4int;
  friend class lightNdouble;
  friend class light44double;
  friend class lightNNdouble;
  friend class lightNNNdouble;
  friend class lightNNNNdouble;

  friend inline lightNdouble light_join (const lightNdouble arr1, const lightNdouble arr2);

  //</ignore>

protected:
  double elem[4];
  // The values of the four elements.

  light4double(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The size of the vector (number of elements).


};

typedef light4double double4;
typedef light4int int4;

#undef IN_LIGHT4double_H

#endif
