//
// routines which use BLAS
//
#ifndef LIGHTMAT_DONT_USE_BLAS
// These functions are overloaded variants of the ones in light_basic.h

int light_dot(const int n, const int *x, const int *y);

int light_dot(const int n, const int *x, const int ix, const int *y);

void light_gemv(const int m, const int n, const int *a, const int *x,
		int *y);

void light_gevm(const int m, const int n, const int *x, const int *a,
		int *y, const int iy = 1);

void light_gemm(const int m, const int n, const int k,
		const int *a, const int *b, int *c);

#endif // ! LIGHTMAT_DONT_USE_BLAS

//
// fractional part
//
inline int FractionalPart(const int s);
inline lightNint  FractionalPart(const lightNint&);
inline lightNNint  FractionalPart(const lightNNint&);
inline lightNNNint  FractionalPart(const lightNNNint&);
inline lightNNNNint  FractionalPart(const lightNNNNint&);

#ifndef C_LIGHT_DOUBLE
inline int FractionalPart(const int );
inline lightNint  FractionalPart(const lightNint&);
inline lightNNint  FractionalPart(const lightNNint&);
inline lightNNNint  FractionalPart(const lightNNNint&);
inline lightNNNNint  FractionalPart(const lightNNNNint&);
#endif

// IntegerPart
#ifndef C_LIGHT_DOUBLE
lightNint  IntegerPart(const lightNint&);
lightNNint  IntegerPart(const lightNNint&);
lightNNNint  IntegerPart(const lightNNNint&);
lightNNNNint  IntegerPart(const lightNNNNint&);
#endif

inline int IntegerPart(const int s); 
lightNint IntegerPart(const lightNint&);
lightNNint  IntegerPart(const lightNNint&);
lightNNNint  IntegerPart(const lightNNNint&);
lightNNNNint  IntegerPart(const lightNNNNint&);

//
// abs
//

lightNint abs(const lightNint& s);


lightN3int abs(const lightN3int& s);


lightNNint abs(const lightNNint& s);


lightNNNint abs(const lightNNNint& s);


lightNNNNint abs(const lightNNNNint& s);

// Mod

#ifndef C_LIGHT_DOUBLE
inline int Mod(const int m , const int n);
#endif

inline int Mod(const int m, const int n);
inline int Mod(const int m, const int n);
inline int Mod(const int m, const int n);

/*
lightNint Mod(const lightNint & i, const lightNint & d);
lightNint Mod(const lightNint & i, const lightNint & d);
lightNNint Mod(const lightNNint & i, const lightNNint & d);
lightNNint Mod(const lightNNint & i, const lightNNint & d);
lightNNNint Mod(const lightNNNint & i, const lightNNNint & d);
lightNNNint Mod(const lightNNNint & i, const lightNNNint & d);
lightNNNNint Mod(const lightNNNNint & i, const lightNNNNint & d);
lightNNNNint Mod(const lightNNNNint & i, const lightNNNNint & d);
*/




