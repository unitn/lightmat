//           -*- c++ -*-

#ifndef LIGHT44_I_H
#define LIGHT44_I_H
// <cd> light44int
//
// .SS Functionality
//
// light44 is a template for classes that implement matrices with 4x4
// elements. E.g. a matrix v with 4x4 elements of type double can be
// instanciated with:
//
// <code>light44&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHT44int_I_H

 class light3;
 class light4;
 class lightN;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light44int {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light44double();
  // Convert to double.
#else
#endif
#ifdef LIGHTMAT_TEMPLATES
  //  friend class light44int;
#endif

#ifdef IN_LIGHT44double_I_H
   friend class  light44int;
#else
   friend class  light44double;
#endif

  #include "i_light44_auto.h"

  light44int();
  // Default constructor.

  light44int(const light44int&);
  // Copy constructor.

  inline 
  light44int(const int e11, const int e12, const int e13, const int e14,
	  const int e21, const int e22, const int e23, const int e24,
	  const int e31, const int e32, const int e33, const int e34,
	  const int e41, const int e42, const int e43, const int e44);
  // Initialize elements with values.

  light44int(const int *);
  // Initialize elements with values from an array (in row major order).

  light44int(const int);
  // Initialize all elements with the same value.

  light44int& operator=(const light44int&);
  // Assignment.

  light44int& operator=(const lightNNint&);
  // Assignment from a lightNN where NN=44.

  light44int& operator=(const int);
  // Assign one value to all elements.

  int operator() (const int x, const int y) const {
    limiterror((x<1) || (x>4) || (y<1) || (y>4));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*4];
#else
    return elem[x-1+(y-1)*4];
#endif
  };
  // Get the value of one element.

  int& operator()(const int x, const int y) {
    limiterror((x<1) || (x>4) || (y<1) || (y>4));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*4];
#else
    return elem[x-1+(y-1)*4];
#endif
  };
  // Get/Set the value of one element.

  light4int operator()(const int) const;
  // Get the value of one row.
 



  //
  // Set
  //

  inline light44int& Set (const int i0, const int val);
  inline light44int& Set (const int i0, const lightNint& arr);
  inline light44int& Set (const int i0, const light3int& arr);
  inline light44int& Set (const int i0, const light4int& arr);



  int operator==(const light44int&) const;
  // Equality.

  int operator!=(const light44int&) const;
  // Inequality.

  light44int& operator+=(const int);
  // Add a value to all elements.

  light44int& operator+=(const light44int&);
  // Elementwise addition.

  light44int& operator-=(const int);
  // Subtract a value from all elements.

  light44int& operator-=(const light44int&);
  // Elementwise subtraction

  light44int& operator*=(const int);
  // Muliply all elements with a value.

  light44int& operator/=(const int);
  // Divide all elements with a value.

  light44int& reshape(const int, const int, const light4int&);
  // Convert the light4-vector to the given size and put the result in
  // this object. All four vector-columns of the light44 object will
  // get the value of the light4-argument. The first two arguments
  // (the size of the matrix) must both have the value 4 since it is a
  // light44 object.

  int dimension(const int x) const {
    if((x==1) || (x==2))
      return 4;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 4 and dimension(2) == 4).

  void Get(int *) const;
  // Get values of all elements and put them in an array (9 elements long,
  // row major order).

  void Set(const int *data);
  // Set values of all elements from array (9 elements long, row major
  // order).

  light44int operator+() const;
  // Unary plus.

  light44int operator-() const;
  // Unary minus.

  friend inline light44int operator+(const light44int&, const light44int&);
  // Elementwise addition.

  friend inline light44int operator+(const light44int&, const int);
  // Addition to all elements.

  friend inline light44int operator+(const int, const light44int&);
  // Addition to all elements.

  friend inline light44int operator-(const light44int&, const light44int&);
  // Elementwise subtraction.

  friend inline light44int operator-(const light44int&, const int);
  // Subtraction from all elements.

  friend inline light44int operator-(const int, const light44int&);
  // Subtraction to all elements.

  friend inline light44int operator*(const light44int&, const light44int&);
  // Inner product.

  friend inline light44int operator*(const light44int&, const int);
  // Multiply all elements.

  friend inline light44int operator*(const int, const light44int&);
  // Multiply all elements.

  friend inline light4int operator*(const light44int&, const light4int&);
  // Inner product.

  friend inline light4int operator*(const light4int&, const light44int&);
  // Inner product.

  friend inline light44int operator/(const light44int&, const int);
  // Divide all elements.

  friend inline light44int operator/(const int, const light44int&);
  // Divide with all elements.

  friend inline light44int pow(const light44int&, const light44int&);
  // Raise to the power of-function, elementwise.

  friend inline light44int pow(const light44int&, const int);
  // Raise to the power of-function, for all elements.

  friend inline light44int pow(const int, const light44int&);
  // Raise to the power of-function, for all elements.

  friend inline light44int ElemProduct(const light44int&, const light44int&);
  // Elementwise multiplication.

  friend inline light44int ElemQuotient(const light44int&, const light44int&);
  // Elementwise division.

  friend inline light44int Apply(const light44int&, int f(int));
  // Apply the function elementwise on all elements.

  friend inline light44int Apply(const light44int&, const light44int&, int f(int, int));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline light44int Transpose(const light44int&);
  // Transpose matrix.

  friend inline light44int abs(const light44int&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light44int sign(const light44int&);
  // sign
#else
  friend inline light44lm_complex sign(const light44lm_complex&);
#endif
  friend inline light44int ifloor(const light44double&);
  // ifloor

  friend inline light44int iceil(const light44double&);
  // iceil

  friend inline light44int irint(const light44double&);
  // irint

  friend inline light44double sqrt(const light44double&);
  // sqrt

  friend inline light44double exp(const light44double&);
  // exp

  friend inline light44double log(const light44double&);
  // log

  friend inline light44double sin(const light44double&);
  // sin

  friend inline light44double cos(const light44double&);
  // cos

  friend inline light44double tan(const light44double&);
  // tan

  friend inline light44double asin(const light44double&);
  // asin

  friend inline light44double acos(const light44double&);
  // acos

  friend inline light44double atan(const light44double&);
  // atan

  friend inline light44double sinh(const light44double&);
  // sinh

  friend inline light44double cosh(const light44double&);
  // cosh

  friend inline light44double tanh(const light44double&);
  // tanh

  friend inline light44double asinh(const light44double&);
  // asinh

  friend inline light44double acosh(const light44double&);
  // acosh

  friend inline light44double atanh(const light44double&);
  // atanh

  friend inline light44lm_complex ifloor(const light44lm_complex&);
  // ifloor

  friend inline light44lm_complex iceil(const light44lm_complex&);
  // iceil

  friend inline light44lm_complex irint(const light44lm_complex&);
  // irint

  friend inline light44lm_complex sqrt(const light44lm_complex&);
  // sqrt

  friend inline light44lm_complex exp(const light44lm_complex&);
  // exp

  friend inline light44lm_complex log(const light44lm_complex&);
  // log

  friend inline light44lm_complex sin(const light44lm_complex&);
  // sin

  friend inline light44lm_complex cos(const light44lm_complex&);
  // cos

  friend inline light44lm_complex tan(const light44lm_complex&);
  // tan

  friend inline light44lm_complex asin(const light44lm_complex&);
  // asin

  friend inline light44lm_complex acos(const light44lm_complex&);
  // acos

  friend inline light44lm_complex atan(const light44lm_complex&);
  // atan

  friend inline light44lm_complex sinh(const light44lm_complex&);
  // sinh

  friend inline light44lm_complex cosh(const light44lm_complex&);
  // cosh

  friend inline light44lm_complex tanh(const light44lm_complex&);
  // tanh

  friend inline light44lm_complex asinh(const light44lm_complex&);
  // asinh

  friend inline light44lm_complex acosh(const light44lm_complex&);
  // acosh

  friend inline light44lm_complex atanh(const light44lm_complex&);
  // atanh


  //<ignore>
//    friend class light44int;
  friend class light4int;
  friend class lightN3int;
  friend class lightNNint;
  friend class lightNNNint;
  friend class lightNNNNint;
  //</ignore>

protected:
  int elem[16];
  // The values of the sixteen elements.

  light44int(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.


};

typedef light44double double44;
typedef light44int int44;

#undef IN_LIGHT44int_I_H

#endif
