//
// sqrt
//
light3lm_complex sqrt(const light3lm_complex& s);

light4lm_complex sqrt(const light4lm_complex& s);

light33lm_complex sqrt(const light33lm_complex& s);

light44lm_complex sqrt(const light44lm_complex& s);

lightNlm_complex sqrt(const lightNlm_complex& s);

lightNNlm_complex sqrt(const lightNNlm_complex& s);

lightN3lm_complex sqrt(const lightN3lm_complex& s);

lightNNNlm_complex sqrt(const lightNNNlm_complex& s);

lightNNNNlm_complex sqrt(const lightNNNNlm_complex& s);


//
// exp
//
light3lm_complex exp(const light3lm_complex& s);

light4lm_complex exp(const light4lm_complex& s);

light33lm_complex exp(const light33lm_complex& s);

light44lm_complex exp(const light44lm_complex& s);

lightNlm_complex exp(const lightNlm_complex& s);

lightNNlm_complex exp(const lightNNlm_complex& s);

lightN3lm_complex exp(const lightN3lm_complex& s);

lightNNNlm_complex exp(const lightNNNlm_complex& s);

lightNNNNlm_complex exp(const lightNNNNlm_complex& s);

//
// log
//
light3lm_complex log(const light3lm_complex& s);

light4lm_complex log(const light4lm_complex& s);

light33lm_complex log(const light33lm_complex& s);

light44lm_complex log(const light44lm_complex& s);

lightNlm_complex log(const lightNlm_complex& s);

lightNNlm_complex log(const lightNNlm_complex& s);

lightN3lm_complex log(const lightN3lm_complex& s);

lightNNNlm_complex log(const lightNNNlm_complex& s);

lightNNNNlm_complex log(const lightNNNNlm_complex& s);


//
// sin
//
light3lm_complex sin(const light3lm_complex& s);

light4lm_complex sin(const light4lm_complex& s);

light33lm_complex sin(const light33lm_complex& s);

light44lm_complex sin(const light44lm_complex& s);

lightNlm_complex sin(const lightNlm_complex& s);

lightNNlm_complex sin(const lightNNlm_complex& s);

lightN3lm_complex sin(const lightN3lm_complex& s);

lightNNNlm_complex sin(const lightNNNlm_complex& s);

lightNNNNlm_complex sin(const lightNNNNlm_complex& s);

//
// cos
//
light3lm_complex cos(const light3lm_complex& s);

light4lm_complex cos(const light4lm_complex& s);

light33lm_complex cos(const light33lm_complex& s);

light44lm_complex cos(const light44lm_complex& s);

lightNlm_complex cos(const lightNlm_complex& s);

lightNNlm_complex cos(const lightNNlm_complex& s);

lightN3lm_complex cos(const lightN3lm_complex& s);

lightNNNlm_complex cos(const lightNNNlm_complex& s);

lightNNNNlm_complex cos(const lightNNNNlm_complex& s);


//
// tan
//
light3lm_complex tan(const light3lm_complex& s);

light4lm_complex tan(const light4lm_complex& s);

light33lm_complex tan(const light33lm_complex& s);

light44lm_complex tan(const light44lm_complex& s);

lightNlm_complex tan(const lightNlm_complex& s);

lightNNlm_complex tan(const lightNNlm_complex& s);

lightN3lm_complex tan(const lightN3lm_complex& s);

lightNNNlm_complex tan(const lightNNNlm_complex& s);

lightNNNNlm_complex tan(const lightNNNNlm_complex& s);

//
// asin
//
light3lm_complex asin(const light3lm_complex& s);

light4lm_complex asin(const light4lm_complex& s);

light33lm_complex asin(const light33lm_complex& s);

light44lm_complex asin(const light44lm_complex& s);

lightNlm_complex asin(const lightNlm_complex& s);

lightNNlm_complex asin(const lightNNlm_complex& s);

lightN3lm_complex asin(const lightN3lm_complex& s);

lightNNNlm_complex asin(const lightNNNlm_complex& s);

lightNNNNlm_complex asin(const lightNNNNlm_complex& s);


//
// acos
//
light3lm_complex acos(const light3lm_complex& s);

light4lm_complex acos(const light4lm_complex& s);

light33lm_complex acos(const light33lm_complex& s);

light44lm_complex acos(const light44lm_complex& s);

lightNlm_complex acos(const lightNlm_complex& s);

lightNNlm_complex acos(const lightNNlm_complex& s);

lightN3lm_complex acos(const lightN3lm_complex& s);

lightNNNlm_complex acos(const lightNNNlm_complex& s);

lightNNNNlm_complex acos(const lightNNNNlm_complex& s);

//
// atan
//
light3lm_complex atan(const light3lm_complex& s);

light4lm_complex atan(const light4lm_complex& s);

light33lm_complex atan(const light33lm_complex& s);

light44lm_complex atan(const light44lm_complex& s);

lightNlm_complex atan(const lightNlm_complex& s);

lightNNlm_complex atan(const lightNNlm_complex& s);

lightN3lm_complex atan(const lightN3lm_complex& s);

lightNNNlm_complex atan(const lightNNNlm_complex& s);

lightNNNNlm_complex atan(const lightNNNNlm_complex& s);

//
// sinh
//
light3lm_complex sinh(const light3lm_complex& s);

light4lm_complex sinh(const light4lm_complex& s);

light33lm_complex sinh(const light33lm_complex& s);

light44lm_complex sinh(const light44lm_complex& s);

lightNlm_complex sinh(const lightNlm_complex& s);

lightNNlm_complex sinh(const lightNNlm_complex& s);

lightN3lm_complex sinh(const lightN3lm_complex& s);

lightNNNlm_complex sinh(const lightNNNlm_complex& s);

lightNNNNlm_complex sinh(const lightNNNNlm_complex& s);

//
// cosh
//
light3lm_complex cosh(const light3lm_complex& s);

light4lm_complex cosh(const light4lm_complex& s);

light33lm_complex cosh(const light33lm_complex& s);

light44lm_complex cosh(const light44lm_complex& s);

lightNlm_complex cosh(const lightNlm_complex& s);

lightNNlm_complex cosh(const lightNNlm_complex& s);

lightN3lm_complex cosh(const lightN3lm_complex& s);

lightNNNlm_complex cosh(const lightNNNlm_complex& s);

lightNNNNlm_complex cosh(const lightNNNNlm_complex& s);

//
// tanh
//
light3lm_complex tanh(const light3lm_complex& s);

light4lm_complex tanh(const light4lm_complex& s);

light33lm_complex tanh(const light33lm_complex& s);

light44lm_complex tanh(const light44lm_complex& s);

lightNlm_complex tanh(const lightNlm_complex& s);

lightNNlm_complex tanh(const lightNNlm_complex& s);

lightN3lm_complex tanh(const lightN3lm_complex& s);

lightNNNlm_complex tanh(const lightNNNlm_complex& s);

lightNNNNlm_complex tanh(const lightNNNNlm_complex& s);

//
// asinh
//
light3lm_complex asinh(const light3lm_complex& s);

light4lm_complex asinh(const light4lm_complex& s);

light33lm_complex asinh(const light33lm_complex& s);

light44lm_complex asinh(const light44lm_complex& s);

lightNlm_complex asinh(const lightNlm_complex& s);

lightNNlm_complex asinh(const lightNNlm_complex& s);

lightN3lm_complex asinh(const lightN3lm_complex& s);

lightNNNlm_complex asinh(const lightNNNlm_complex& s);

lightNNNNlm_complex asinh(const lightNNNNlm_complex& s);

//
// acosh
//
light3lm_complex acosh(const light3lm_complex& s);

light4lm_complex acosh(const light4lm_complex& s);

light33lm_complex acosh(const light33lm_complex& s);

light44lm_complex acosh(const light44lm_complex& s);

lightNlm_complex acosh(const lightNlm_complex& s);

lightNNlm_complex acosh(const lightNNlm_complex& s);

lightN3lm_complex acosh(const lightN3lm_complex& s);

lightNNNlm_complex acosh(const lightNNNlm_complex& s);

lightNNNNlm_complex acosh(const lightNNNNlm_complex& s);

//
// atanh
//
light3lm_complex atanh(const light3lm_complex& s);

light4lm_complex atanh(const light4lm_complex& s);

light33lm_complex atanh(const light33lm_complex& s);

light44lm_complex atanh(const light44lm_complex& s);

lightNlm_complex atanh(const lightNlm_complex& s);

lightNNlm_complex atanh(const lightNNlm_complex& s);

lightN3lm_complex atanh(const lightN3lm_complex& s);

lightNNNlm_complex atanh(const lightNNNlm_complex& s);

lightNNNNlm_complex atanh(const lightNNNNlm_complex& s);

