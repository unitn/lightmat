//
// sqrt
//
light3double sqrt(const light3double& s);

light4double sqrt(const light4double& s);

light33double sqrt(const light33double& s);

light44double sqrt(const light44double& s);

lightNdouble sqrt(const lightNdouble& s);

lightNNdouble sqrt(const lightNNdouble& s);

lightN3double sqrt(const lightN3double& s);

lightNNNdouble sqrt(const lightNNNdouble& s);

lightNNNNdouble sqrt(const lightNNNNdouble& s);


//
// exp
//
light3double exp(const light3double& s);

light4double exp(const light4double& s);

light33double exp(const light33double& s);

light44double exp(const light44double& s);

lightNdouble exp(const lightNdouble& s);

lightNNdouble exp(const lightNNdouble& s);

lightN3double exp(const lightN3double& s);

lightNNNdouble exp(const lightNNNdouble& s);

lightNNNNdouble exp(const lightNNNNdouble& s);

//
// log
//
light3double log(const light3double& s);

light4double log(const light4double& s);

light33double log(const light33double& s);

light44double log(const light44double& s);

lightNdouble log(const lightNdouble& s);

lightNNdouble log(const lightNNdouble& s);

lightN3double log(const lightN3double& s);

lightNNNdouble log(const lightNNNdouble& s);

lightNNNNdouble log(const lightNNNNdouble& s);


//
// sin
//
light3double sin(const light3double& s);

light4double sin(const light4double& s);

light33double sin(const light33double& s);

light44double sin(const light44double& s);

lightNdouble sin(const lightNdouble& s);

lightNNdouble sin(const lightNNdouble& s);

lightN3double sin(const lightN3double& s);

lightNNNdouble sin(const lightNNNdouble& s);

lightNNNNdouble sin(const lightNNNNdouble& s);

//
// cos
//
light3double cos(const light3double& s);

light4double cos(const light4double& s);

light33double cos(const light33double& s);

light44double cos(const light44double& s);

lightNdouble cos(const lightNdouble& s);

lightNNdouble cos(const lightNNdouble& s);

lightN3double cos(const lightN3double& s);

lightNNNdouble cos(const lightNNNdouble& s);

lightNNNNdouble cos(const lightNNNNdouble& s);


//
// tan
//
light3double tan(const light3double& s);

light4double tan(const light4double& s);

light33double tan(const light33double& s);

light44double tan(const light44double& s);

lightNdouble tan(const lightNdouble& s);

lightNNdouble tan(const lightNNdouble& s);

lightN3double tan(const lightN3double& s);

lightNNNdouble tan(const lightNNNdouble& s);

lightNNNNdouble tan(const lightNNNNdouble& s);

//
// asin
//
light3double asin(const light3double& s);

light4double asin(const light4double& s);

light33double asin(const light33double& s);

light44double asin(const light44double& s);

lightNdouble asin(const lightNdouble& s);

lightNNdouble asin(const lightNNdouble& s);

lightN3double asin(const lightN3double& s);

lightNNNdouble asin(const lightNNNdouble& s);

lightNNNNdouble asin(const lightNNNNdouble& s);


//
// acos
//
light3double acos(const light3double& s);

light4double acos(const light4double& s);

light33double acos(const light33double& s);

light44double acos(const light44double& s);

lightNdouble acos(const lightNdouble& s);

lightNNdouble acos(const lightNNdouble& s);

lightN3double acos(const lightN3double& s);

lightNNNdouble acos(const lightNNNdouble& s);

lightNNNNdouble acos(const lightNNNNdouble& s);

//
// atan
//
light3double atan(const light3double& s);

light4double atan(const light4double& s);

light33double atan(const light33double& s);

light44double atan(const light44double& s);

lightNdouble atan(const lightNdouble& s);

lightNNdouble atan(const lightNNdouble& s);

lightN3double atan(const lightN3double& s);

lightNNNdouble atan(const lightNNNdouble& s);

lightNNNNdouble atan(const lightNNNNdouble& s);

//
// sinh
//
light3double sinh(const light3double& s);

light4double sinh(const light4double& s);

light33double sinh(const light33double& s);

light44double sinh(const light44double& s);

lightNdouble sinh(const lightNdouble& s);

lightNNdouble sinh(const lightNNdouble& s);

lightN3double sinh(const lightN3double& s);

lightNNNdouble sinh(const lightNNNdouble& s);

lightNNNNdouble sinh(const lightNNNNdouble& s);

//
// cosh
//
light3double cosh(const light3double& s);

light4double cosh(const light4double& s);

light33double cosh(const light33double& s);

light44double cosh(const light44double& s);

lightNdouble cosh(const lightNdouble& s);

lightNNdouble cosh(const lightNNdouble& s);

lightN3double cosh(const lightN3double& s);

lightNNNdouble cosh(const lightNNNdouble& s);

lightNNNNdouble cosh(const lightNNNNdouble& s);

//
// tanh
//
light3double tanh(const light3double& s);

light4double tanh(const light4double& s);

light33double tanh(const light33double& s);

light44double tanh(const light44double& s);

lightNdouble tanh(const lightNdouble& s);

lightNNdouble tanh(const lightNNdouble& s);

lightN3double tanh(const lightN3double& s);

lightNNNdouble tanh(const lightNNNdouble& s);

lightNNNNdouble tanh(const lightNNNNdouble& s);

//
// asinh
//
light3double asinh(const light3double& s);

light4double asinh(const light4double& s);

light33double asinh(const light33double& s);

light44double asinh(const light44double& s);

lightNdouble asinh(const lightNdouble& s);

lightNNdouble asinh(const lightNNdouble& s);

lightN3double asinh(const lightN3double& s);

lightNNNdouble asinh(const lightNNNdouble& s);

lightNNNNdouble asinh(const lightNNNNdouble& s);

//
// acosh
//
light3double acosh(const light3double& s);

light4double acosh(const light4double& s);

light33double acosh(const light33double& s);

light44double acosh(const light44double& s);

lightNdouble acosh(const lightNdouble& s);

lightNNdouble acosh(const lightNNdouble& s);

lightN3double acosh(const lightN3double& s);

lightNNNdouble acosh(const lightNNNdouble& s);

lightNNNNdouble acosh(const lightNNNNdouble& s);

//
// atanh
//
light3double atanh(const light3double& s);

light4double atanh(const light4double& s);

light33double atanh(const light33double& s);

light44double atanh(const light44double& s);

lightNdouble atanh(const lightNdouble& s);

lightNNdouble atanh(const lightNNdouble& s);

lightN3double atanh(const lightN3double& s);

lightNNNdouble atanh(const lightNNNdouble& s);

lightNNNNdouble atanh(const lightNNNNdouble& s);

