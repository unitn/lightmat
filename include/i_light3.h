//           -*- c++ -*-

#ifndef LIGHT3_I_H
#define LIGHT3_I_H
// <cd> light3int
//
// .SS Functionality
//
// light3 is a template for classes that implement vectors with 3
// elements. E.g. a vector v with 3 elements of type double can be
// instanciated with:
//
// <code>light3&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#define IN_LIGHT3int_I_H

 class light4;

 class lightN;
 class light33;
 class lightN3;
 class lightN33;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light3int {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light3double();
  // Convert to double.
#else
  friend class lightN3int;
  friend class lightN33int;
#endif
#ifdef LIGHTMAT_TEMPLATES
  //  friend class light3int; 
  friend class lightN3int;
  friend class lightN33int;
#endif

#ifdef IN_LIGHT3double_I_H
  friend class light3int; 
#else
  friend class light3double; 
#endif

  #include "i_light3_auto.h"

  light3int();
  // Default constructor.

  light3int(const light3int&);
  // Copy constructor.

  light3int(const int, const int, const int);
  // Initialize elements with values.

  light3int(const int *);
  // Initialize elements with values from an array.

  light3int(const int);
  // Initialize all elements with the same value.

  light3int& operator=(const light3int&);
  // Assignment.

  light3int& operator=(const lightNint&);
  // Assignment from a lightN where N=3.

  light3int& operator=(const int);
  // Assign one value to all elements.

  int operator()(const int x) const {
    limiterror((x<1) || (x>3));
    return elem[x-1];
  };
  // Get the value of one element.

  int& operator()(const int x) {
    limiterror((x<1) || (x>3));
    return elem[x-1];
  };
  // Get/Set the value of one element.


  int operator==(const light3int&) const;
  // Equality.

  int operator!=(const light3int&) const;
  // Inequality.

  light3int& operator+=(const int);
  // Add a value to all elements.

  light3int& operator+=(const light3int&);
  // Elementwise addition.

  light3int& operator-=(const int);
  // Subtract a value from all elements.

  light3int& operator-=(const light3int&);
  // Elementwise subtraction.

  light3int& operator*=(const int);
  // Muliply all elements with a value.

  light3int& operator/=(const int);
  // Divide all elements with a value.

  #ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
  #endif
  int dimension(const int x = 1) const {
    if (x == 1)
      return 3;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 3). If this was a lightNint
  // then dimension(1) would return the number of elements.
 
  #ifndef COMPLEX_TOOLS
  light3int& normalize();
  // Normalize vector.
  #endif

  void Get(int *) const;
  // Get values of all elements and put them in an array (3 elements long).

  void Set(const int *);
  // Set values of all elements from array (3 elements long).

  void Get( int&, int&, int&) const;
  // Get the value of the three elements.

  void Set(const int, const int, const int);
  // Set the value of the three elements.

  light3int operator+() const;
  // Unary plus.

  light3int operator-() const;
  // Unary minus.

  friend inline light3int operator+(const light3int&, const light3int&);
  // Elementwise addition.

  friend inline light3int operator+(const light3int&, const int);
  // Addition to all elements.

  friend inline light3int operator+(const int, const light3int&);
  // Addition to all elements.

  friend inline light3int operator-(const light3int&, const light3int&);
  // Elementwise subtraction.

  friend inline light3int operator-(const light3int&, const int);
  // Subtraction from all elements.

  friend inline light3int operator-(const int, const light3int&);
  // Subtraction to all elements.

  friend inline int operator*(const light3int&, const light3int&);
  // Inner product.

  friend inline int Dot(const light3int&, const light3int&);
  // Inner product.

  friend inline light3int operator*(const light3int&, const int);
  // Multiply all elements.

  friend inline light3int operator*(const int, const light3int&);
  // Multiply all elements.

  friend inline light3int operator*(const light3int&, const light33int&);
  // Inner product.

  friend inline light3int operator*(const light33int&, const light3int&);
  // Inner product.

  friend inline lightNint operator*(const light3int&, const lightNNint&);
  // Inner product.

  friend inline light3int Dot(const light3int&, const light33int&);
  // Inner product.

  friend inline light3int Dot(const light33int&, const light3int&);
  // Inner product.

  friend inline light3int operator/(const light3int&, const int);
  // Divide all elements.

  friend inline light3int operator/(const int, const light3int&);
  // Divide with all elements.

  friend inline light3int pow(const light3int&, const light3int&);
  // Raise to the power of-function, elementwise.

  friend inline light3int pow(const light3int&, const int);
  // Raise to the power of-function, for all elements.

  friend inline light3int pow(const int, const light3int&);
  // Raise to the power of-function, for all elements.

  friend inline light3int ElemProduct(const light3int&, const light3int&);
  // Elementwise multiplication.

  friend inline light3int ElemQuotient(const light3int&, const light3int&);
  // Elementwise division.

  friend inline light3int Apply(const light3int&, int f(int));
  // Apply the function elementwise on all three elements.

  friend inline light3int Apply(const light3int&, const light3int&, int f(int, int));
  // Apply the function elementwise on all three elements in the two
  // vecors.

  friend inline light3int Cross(const light3int&, const light3int&);
  // Cross product.

  friend inline light33int OuterProduct(const light3int&, const light3int&);
  // Outer product.

  friend  inline  light3int abs(const light3int&);
  // abs

#ifndef COMPLEX_TOOLS
  friend  inline  light3int sign(const light3int&);
  // sign

  friend  inline  light3int sign(const light3double&);
  // sign
#else //for complex numbers
  friend  inline  light3lm_complex sign(const light3lm_complex&);

#endif
  friend  inline  light3int ifloor(const light3double&);
  // ifloor

  friend  inline  light3int iceil(const light3double&);
  // iceil

  friend  inline  light3int irint(const light3double&);
  // irint

  friend  inline  light3double sqrt(const light3double&);
  // sqrt

  friend  inline  light3double exp(const light3double&);
  // exp

  friend  inline  light3double log(const light3double&);
  // log

  friend  inline  light3double sin(const light3double&);
  // sin

  friend  inline  light3double cos(const light3double&);
  // cos

  friend  inline  light3double tan(const light3double&);
  // tan

  friend  inline  light3double asin(const light3double&);
  // asin

  friend  inline  light3double acos(const light3double&);
  // acos

  friend  inline  light3double atan(const light3double&);
  // atan

  friend  inline  light3double sinh(const light3double&);
  // sinh

  friend  inline  light3double cosh(const light3double&);
  // cosh

  friend  inline  light3double tanh(const light3double&);
  // tanh

  friend  inline  light3double asinh(const light3double&);
  // asinh

  friend  inline  light3double acosh(const light3double&);
  // acosh

  friend  inline  light3double atanh(const light3double&);
  // atanh

  friend  inline  light3lm_complex ifloor(const light3lm_complex&);
  // ifloor

  friend  inline  light3lm_complex iceil(const light3lm_complex&);
  // iceil

  friend  inline  light3lm_complex irint(const light3lm_complex&);
  // irint

  friend  inline  light3lm_complex sqrt(const light3lm_complex&);
  // sqrt

  friend  inline  light3lm_complex exp(const light3lm_complex&);
  // exp

  friend  inline  light3lm_complex log(const light3lm_complex&);
  // log

  friend  inline  light3lm_complex sin(const light3lm_complex&);
  // sin

  friend  inline  light3lm_complex cos(const light3lm_complex&);
  // cos

  friend  inline  light3lm_complex tan(const light3lm_complex&);
  // tan

  friend  inline  light3lm_complex asin(const light3lm_complex&);
  // asin

  friend  inline  light3lm_complex acos(const light3lm_complex&);
  // acos

  friend  inline  light3lm_complex atan(const light3lm_complex&);
  // atan

  friend  inline  light3lm_complex sinh(const light3lm_complex&);
  // sinh

  friend  inline  light3lm_complex cosh(const light3lm_complex&);
  // cosh

  friend  inline  light3lm_complex tanh(const light3lm_complex&);
  // tanh

  friend  inline  light3lm_complex asinh(const light3lm_complex&);
  // asinh

  friend  inline  light3lm_complex acosh(const light3lm_complex&);
  // acosh

  friend  inline  light3lm_complex atanh(const light3lm_complex&);
  // atanh
  //<ignore>
  // friend class light3int;
  friend class lightNint;
  friend class light33int;
  friend class lightN3int;
  friend class lightNNint;
  friend class lightNNNint;
  friend class lightNNNNint;
  
#ifndef COMPLEX_TOOLS
  friend  inline  lightN3int sign(const lightN3int&);
  friend  inline  lightN3int sign(const lightN3double&);
#else
  friend  inline  lightN3lm_complex sign(const lightN3lm_complex&);
#endif
  friend  inline  lightN3int ifloor(const lightN3double&);
  friend  inline  lightN3int iceil(const lightN3double&);
  friend  inline  lightN3int irint(const lightN3double&);

  friend inline lightNint light_join (const lightNint arr1, const lightNint arr2);

  //</ignore>

protected:
  int elem[3];
  // The values of the three elements.

  light3int(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The size of the vector (number of elements).


};

//#include "i_light3.icc"


inline  light3double atanh(const light3double&);
inline  light3double atanh(const light3double&);

//*******************************
typedef light3double doubleVec3;
typedef light3int int3;
extern const doubleVec3 zeroVec3;
//*******************************

#undef IN_LIGHT3int_I_H

#endif
