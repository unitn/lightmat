inline lightNNdouble& Set (const int i0, const int i1, const double val);
inline lightNNdouble& Set (const R r0, const int i1, const lightNdouble& arr);
inline lightNNdouble& Set (const R r0, const int i1, const light3double& arr);
inline lightNNdouble& Set (const R r0, const int i1, const light4double& arr);
inline lightNNdouble& Set (const R r0, const int i1, const double val);
inline lightNNdouble& Set (const int i0, const R r1, const lightNdouble& arr);
inline lightNNdouble& Set (const int i0, const R r1, const light3double& arr);
inline lightNNdouble& Set (const int i0, const R r1, const light4double& arr);
inline lightNNdouble& Set (const int i0, const R r1, const double val);
inline lightNNdouble& Set (const R r0, const R r1, const lightNNdouble& arr);
inline lightNNdouble& Set (const R r0, const R r1, const light33double& arr);
inline lightNNdouble& Set (const R r0, const R r1, const light44double& arr);
inline lightNNdouble& Set (const R r0, const R r1, const double val);
inline lightNdouble operator() (const R r0, const int i1) const;
inline lightNdouble operator() (const int i0, const R r1) const;
inline lightNNdouble operator() (const R r0, const R r1) const;

#ifdef IN_LIGHTNNdouble_H
inline lightNNdouble(const double e, const lightNNint &s1, const lightmat_atan2_enum);
inline lightNNdouble(const double e, const lightNNdouble &s1, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const double e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const double e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const lightNNint &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const lightNNdouble &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const lightNNint &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const lightNNdouble &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const int e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const int e, const lightmat_atan2_enum);
inline lightNNdouble(const int e, const lightNNint &s1, const lightmat_atan2_enum);
inline lightNNdouble(const int e, const lightNNdouble &s1, const lightmat_atan2_enum);

#endif
