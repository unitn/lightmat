//           -*- c++ -*-

#ifndef LIGHTN33_I_H
#define LIGHTN33_I_H
// <cd> lightN33int
//
// .SS Functionality
//
// lightN33 is a template for classes that implement tensors
// of size Nx3x3
//
// <code>lightN33&lt;double&gt; v(5);</code>
//
// The value of the first index can change during execution. It
// changes its size to whatever is needed when it is assigned a new
// value with an assignment operator.
//
// When a lightN33 object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightN33 object with 5x3x3 elements to one with 7x3x3 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#define LIGHTN33_SIZE 10


class lightN33int {
public:
  lightN33int();
  // Default constructor.

  lightN33int(const lightN33int&);
  // Copy constructor.

  lightN33int(const int n);
  // Construct a lightN33 with size nx3x3

  lightN33int(const int n, const int *);
  // Construct a lightN33 with size nx3x3 and initialize the
  // elements with values from an array (in row major order).
  
  lightN33int(const int n, const int);
  // Construct a lightN33 with size nx3x3 and initialize the
  // elements with a value (the second argument).

  ~lightN33int();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightN33double();
  // Convert to double
#else
  friend class lightN33int;  
#endif

  lightN33int& operator=(const lightN33int&);
  // Assignment.

  lightN33int& operator=(const lightNNNint&);
  // Assignment from a lightNNN with size Nx3x3.

  lightN33int& operator=(const int);
  // Assign one value to all elements.

  int operator()(const int, const int, const int) const;
  // Get the value of one element.

  int& operator()(const int, const int, const int);
  // Get/Set the value of one element.

  light3int operator()(const int, const int) const;
  // Get the value of a vector.

  const light33int& operator()(const int) const;
  // Get one 3x3 matrix.
  
  light33int& operator()(const int);
  // Get/Set one 3x3 matrix.
 
  int operator==(const lightN33int&) const;
  // Equality.

  int operator!=(const lightN33int&) const;
  // Inequality.

  lightN33int& operator+=(const int);
  // Add a value to all elements.

  lightN33int& operator+=(const lightN33int&);
  // Elementwise addition.

  lightN33int& operator-=(const int);
  // Subtract a value from all elements.

  lightN33int& operator-=(const lightN33int&);
  // Elementwise subtraction.

  lightN33int& operator*=(const int);
  // Mulitply all elements with a value.

  lightN33int& operator/=(const int);
  // Divide all elements with a value.

  lightN33int& reshape(const int, const int, const int, const lightNint& s);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c) will be set to s(a) in the lightN33int
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector and the second and third
  // argument must be 3. The program may dump core or behave strangely
  // if the arguments are incorrect.

  lightN33int& reshape(const int, const int, const int, const lightNNint& s);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c) will be set to s(a,b) in the
  // lightN33 object. The value of the first argument must be the same
  // as the number of rows in the lightNN-matrix and the second and
  // third argument must be 3. The number of columns in the
  // lightNN-matrix must also be 3. The program may dump core or
  // behave strangely if the arguments are incorrect.

  int dimension(const int) const;
  // Get size of some dimension (dimension(2) == 3 and dimension(3) ==
  // 3 for lightN3).

  void Get(int *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const int *);
  // Set values of all elements from array (row major order).

  lightN3int operator+() const;
  // Unary plus.

  lightN3int operator-() const;
  // Unary minus.

  friend inline lightN33int Apply(const lightN33int&, int f(int));
  // Apply the function elementwise all elements.

  friend inline lightN33int Apply(const lightN33int&, const lightN33int&, int f(int, int));
  // Apply the function elementwise on all elements in the two tensors.

  //<ignore>
  friend class light3int;
  friend class lightNint;
  friend class lightN3int;
  friend class lightNNint;
  friend class lightNNNint;
  //</ignore>
  
protected:
//FOOFOO    light33int sarea[LIGHTN33_SIZE];

  light33int *elem;
  // The matrixes are stored here.

  int size;
  // The size of the tensor.

  int alloc_size;
  // The size of the allocated area.

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  lightN33int(const int x, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // argument is the size of the tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  lightN33int(const lightN33int&, int f(int), const lightmat_apply_enum);
  lightN33int(const lightN33int&, const lightN33int&, int f(int, int), const lightmat_apply_enum);
  lightN33int(const lightN33int&, const lightmat_trans_enum);
};

typedef lightN33double doubleN33;
typedef lightN33int intN33;

#endif
