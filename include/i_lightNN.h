//           -*- c++ -*-

#ifndef LIGHTNN_I_H
#define LIGHTNN_I_H
// <cd> lightNNint
//
// .SS Functionality
//
// lightNN is a template for classes that implement matrices with any
// number of elements. E.g. a matrix v with 5x6 elements of type double
// can be instanciated with:
//
// <code>lightNN&lt;double&gt; v(5,6);</code>
//
// The number of rows and columns in the matrix can change during
// execution. It changes its size to whatever is needed when it is
// assigned a new value with an assignment operator.
//
// When a lightNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNN object with 5x5 elements to one with 5x6 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
// Vadim Engelson
//

#include <assert.h>

#define IN_LIGHTNNint_I_H


#define LIGHTNN_SIZE1 10
#define LIGHTNN_SIZE2 10
#define LIGHTNN_SIZE (LIGHTNN_SIZE1*LIGHTNN_SIZE2)
// The default size.

 class light3;
 class light4;
 class lightN;
 class light33;
 class light44;
 class lightN3;
 class lightNNN;


class lightNNint {
public:

#ifdef IN_LIGHTNNdouble_I_H
  friend class lightNNint;
#else
  friend class lightNNdouble;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNint;
#endif


  #include "i_lightNN_auto.h"

  inline lightNNint();
  // Default constructor.

  inline lightNNint(const lightNNint&);
  // Copy constructor.

  inline lightNNint(const light33int&);
  // Conversion.

  inline lightNNint(const light44int&);
  // Conversion.

  inline lightNNint(const lightN3int&);
  // Conversion.

  inline lightNNint(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNint(const int, const int);
  // Construct a lightNN of given size.

  inline lightNNint(const int, const int, const int *);
  // Construct a lightNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNint(const int, const int, const int);
  // Construct a lightNN of given size and initialize all elements
  // with a value (the third argument).

  inline ~lightNNint();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNlm_complex();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNlm_complex();
  // Convert to complex.
#endif



  lightNNint& operator=(const lightNNint&);
  // Assignment.

  lightNNint& operator=(const light33int&);
  // Assignment, change size to 3x3.

  lightNNint& operator=(const light44int&);
  // Assignment, change size to 4x4.

  lightNNint& operator=(const lightN3int&);
  // Assignment, change size to Nx3.

  lightNNint& operator=(const int);
  // Assign one value to all elements.

  int operator() (const int x, const int y) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2));
#ifdef ROWMAJOR
    return elem[y-1 + (x-1)*size2];
#else
    return elem[x-1 + (y-1)*size1];
#endif
  };
  // Get the value of one element.

  //
  // Set and Get
  //

  int& operator()(const int x, const int y) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2));
#ifdef ROWMAJOR
    return elem[y-1 + (x-1)*size2];
#else
    return elem[x-1 + (y-1)*size1];
#endif
  };
  // Get/Set the value of one element.

  //
  // Set
  //

  inline lightNNint& Set (const int i0, const int val);
  inline lightNNint& Set (const int i0, const lightNint& arr);
  inline lightNNint& Set (const int i0, const light3int& arr);
  inline lightNNint& Set (const int i0, const light4int& arr);


  int&  takeElementReference (const int x, const int y);
  // Same

  lightNint operator()(const int) const;
  // Get the value of one row.
 
  const lightNint& SetCol(const int n,const lightNint& x);
  // Put x into column n. Return x.

  const lightNint& SetRow(const int n,const lightNint& x);
  // Put x into row n. Return x.
  
  int SetCol(const int n,const int x);
  // Put x into all elements of  column n. Return x.

  int SetRow(const int n,const int x);
   // Put x into all elements of row n. Return x.

  const lightNNint& SetCols(const int n1,const int n2,const  lightNNint& x);
  // Put every  element of matrix x to the columns n1..n2. Return x. 
  // In this function and below:
  // n1=0 means to use  1. 
  // n2=0 means to use the largest possible value of matrix index.

  const lightNNint& SetRows(const int n1,const int n2,const  lightNNint& x);
  // Put every  element of matrix x to the rows n1..n2.  Return x. 
  
  int SetCols(const int n1, const int n2, const  int x);
  // Put x to very element of all columns n1..n2. Return x. 
  
  int SetRows(const int n1,const int n2,const  int x);
  // Put x to very eleme nt of all rows n1..n2. Return x. 
  
  lightNint  Col(const int n) const;
  // Get Column n.  

  lightNint  Row(const int n) const;
  // Get Row n.

// REMOVE LATER
  inline lightNNint   SubMatrix(const int r1, const int r2,const int c1, const int c2) const;
  // Get submatrix of rows r1..r2, columns c1..c2.

  inline const lightNNint& SetSubMatrix(const int r1,const int r2,const int c1,const int c2,
                  const  lightNNint& x);
  // Put matrix x into  submatrix  of rows r1..r2, columns c1..c2. Return x.

  inline  int SetSubMatrix(const int r1,const int r2,const int c1,const int c2,
                   const int x);
  // Put x into rows r1..r2, columns c1..c2. Return x.
  
  lightNNint Rows(const int x1,const int x2) const;
  // Get submstrix produced by rows x1..x2.
  
  
  lightNNint Cols(const int y1,const int y2) const;
  // Get submstrix produced by columns  y1..y2.
  
  int operator==(const lightNNint&) const;
  // Equality.

  int operator!=(const lightNNint&) const;
  // Inequality.
 
  lightNNint& operator+=(const int);
  // Add a value to all elements.

  lightNNint& operator+=(const lightNNint&);
  // Elementwise addition.

  lightNNint& operator-=(const int);
  // Subtract a value from all elements.

  lightNNint& operator-=(const lightNNint&);
  // Elementwise subtraction.

  lightNNint& operator*=(const int);
  // Muliply all elements with a value.

  lightNNint& operator/=(const int);
  // Divide all elements with a value.
 
  inline lightNNint& SetShape(const int x=-1, const int y=-1);
  // Sets specified shape for the matrix.

  int Extract(const lightNint&) const;
  // Extract an element using given index.   

  lightNNint& reshape(const int, const int, const lightNint&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. All vector-columns of the lightNN object will get
  // the value of the lightN-argument. The value of the first argument
  // must be the same as the number of elements in the lightN-vector.

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(int *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const int *);
  // Set values of all elements from array (row major order).
 
  int * data() const; 
  // Direct access to the stored data. Use the result with care.

  lightNNint operator+() const;
  // Unary plus.

  lightNNint operator-() const;
  // Unary minus.

  friend inline lightNNint operator+(const lightNNint&, const lightNNint&);
  // Elementwise addition.

  friend inline lightNNint operator+(const lightNNint&, const int);
  // Addition to all elements.

  friend inline lightNNint operator+(const int, const lightNNint&);
  // Addition to all elements.

#ifdef IN_LIGHTNNdouble_I_H
  friend inline lightNNdouble operator+(const lightNNdouble&, const int);
  friend inline lightNNdouble operator+(const lightNNint&, const double);
  friend inline lightNNdouble operator+(const double, const lightNNint&);
  friend inline lightNNdouble operator+(const int, const lightNNdouble&);
#endif

#ifdef IN_LIGHTNNlm_complex_I_H
friend inline lightNNlm_complex operator+(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator+(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator+(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator+(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNNint operator-(const lightNNint&, const lightNNint&);
  // Elementwise subtraction.

  friend inline lightNNint operator-(const lightNNint&, const int);
  // Subtraction from all elements.

  friend inline lightNNint operator-(const int, const lightNNint&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNdouble_I_H
  friend inline lightNNdouble operator-(const lightNNdouble&, const int);
  friend inline lightNNdouble operator-(const lightNNint&, const double);
  friend inline lightNNdouble operator-(const double, const lightNNint&);
  friend inline lightNNdouble operator-(const int, const lightNNdouble&);
#endif

  #ifdef IN_LIGHTNNlm_complex_I_H
friend inline lightNNlm_complex operator-(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator-(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator-(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator-(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNNint operator*(const lightNNint&, const lightNNint&);
  // Inner product.

  friend inline lightNNint operator*(const lightNNint&, const int);
  // Multiply all elements.

  friend inline lightNNint operator*(const int, const lightNNint& s);
  // Multiply all elements.

#ifdef IN_LIGHTNNdouble_I_H
  friend inline lightNNdouble operator*(const lightNNdouble&, const int);
  friend inline lightNNdouble operator*(const lightNNint&, const double);
  friend inline lightNNdouble operator*(const double, const lightNNint&);
  friend inline lightNNdouble operator*(const int, const lightNNdouble&);
#endif

#ifdef IN_LIGHTNNlm_complex_I_H
friend inline lightNNlm_complex operator*(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator*(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator*(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator*(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNNint operator*(const lightNNint&, const light33int&);
  // Inner product.

  friend inline lightNNint operator*(const lightNNint&, const light44int&);
  // Inner product.

  friend inline lightNNint operator*(const lightN3int&, const lightNNint&);
  // Inner product.

  friend inline lightNNint operator*(const light33int&, const lightNNint&);
  // Inner product.

  friend inline lightNNint operator*(const light44int&, const lightNNint&);
  // Inner product.

  friend inline lightNNint operator*(const lightNint&, const lightNNNint&);
  // Inner product.

  friend inline lightNNint operator*(const lightNNNint&, const lightNint&);
  // Inner product.

  friend inline lightNNint operator*(const light3int&, const lightNNNint&);
  // Inner product.

  friend inline lightNNint operator*(const lightNNNint&, const light3int&);
  // Inner product.

  friend inline lightNNint operator*(const light4int&, const lightNNNint&);
  // Inner product.

  friend inline lightNNint operator*(const lightNNNint&, const light4int&);
  // Inner product.

  friend inline lightNNint operator/(const lightNNint&, const int);
  // Divide all elements.

  friend inline lightNNint operator/(const int, const lightNNint&);
  // Divide all elements.

#ifdef IN_LIGHTNNdouble_I_H
  friend inline lightNNdouble operator/(const lightNNdouble&, const int);
  friend inline lightNNdouble operator/(const lightNNint&, const double);
  friend inline lightNNdouble operator/(const double, const lightNNint&);
  friend inline lightNNdouble operator/(const int, const lightNNdouble&);
#endif

#ifdef IN_LIGHTNNlm_complex_I_H
friend inline lightNNlm_complex operator/(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator/(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator/(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator/(const double e, const lightNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNlm_complex_I_H
friend inline lightNNdouble arg(const lightNNlm_complex& a);
friend inline lightNNdouble re(const lightNNlm_complex& a);
friend inline lightNNdouble im(const lightNNlm_complex& a);
friend inline lightNNlm_complex conjugate(const lightNNlm_complex& a);
//#endif

  friend inline lightNNint pow(const lightNNint&, const lightNNint&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNint pow(const lightNNint&, const int);
  // Raise to the power of-function, for all elements.

  friend inline lightNNint pow(const int, const lightNNint&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNint ElemProduct(const lightNNint&, const lightNNint&);
  // Elementwise multiplication.

  friend inline lightNNint ElemQuotient(const lightNNint&, const lightNNint&);
  // Elementwise division.

  friend inline lightNNint Apply(const lightNNint&, int f(int));
  // Apply the function elementwise all elements.

  friend inline lightNNint Apply(const lightNNint&, const lightNNint&, int f(int, int));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline lightNNint Transpose(const lightNNint&);
  // Transpose matrix.

  friend inline lightNNint Transpose(const lightN3int&);
  // Transpose matrix.

  friend inline lightNNint OuterProduct(const lightNint&, const lightNint&);
  // Outer product.


#ifdef IN_LIGHTNNlm_complex_I_H
#else
  friend inline lightNNint abs(const lightNNint&);
#ifdef IN_LIGHTNNdouble_I_H
  friend inline lightNNdouble abs(const lightNNlm_complex&);
#endif 
#endif


#ifndef COMPLEX_TOOLS
  friend inline lightNNint sign(const lightNNint&);
  // sign
#else
  // sign
  friend inline lightNNlm_complex sign(const lightNNlm_complex&);
#endif
  friend inline lightNNint sign(const lightNNdouble&);
  // sign

  /// Added 2/2/98 

  friend inline lightNNint  FractionalPart(const lightNNint&);
  //  FractionalPart

  friend inline lightNNint  IntegerPart(const lightNNint&);
  //  IntegerPart

  //friend inline lightNNint  IntegerPart(const lightNNdouble&);
  //  IntegerPart

  friend inline lightNNint Mod (const  lightNNint&,const  lightNNint&);
  // Mod
 
  friend inline lightNNint Mod (const  lightNNint&,const int);
  // Mod

  friend inline lightNNint Mod (const int,const  lightNNint&);
  // Mod

  friend inline int LightMax (const lightNNint& );
  // Max

  friend inline int LightMin (const lightNNint& );
  // Min

  friend inline int findLightMax (const  int *,const  int );
  // Find Max
 
  friend inline int findLightMin (const  int *,const  int );
  // Find Min
 
  /// End Added 



  friend inline lightNNint ifloor(const lightNNdouble&);
  // ifloor

  friend inline lightNNint iceil(const lightNNdouble&);
  // iceil

  friend inline lightNNint irint(const lightNNdouble&);
  // irint

  friend inline lightNNdouble sqrt(const lightNNdouble&);
  // sqrt

  friend inline lightNNdouble exp(const lightNNdouble&);
  // exp

  friend inline lightNNdouble log(const lightNNdouble&);
  // log

  friend inline lightNNdouble sin(const lightNNdouble&);
  // sin

  friend inline lightNNdouble cos(const lightNNdouble&);
  // cos

  friend inline lightNNdouble tan(const lightNNdouble&);
  // tan

  friend inline lightNNdouble asin(const lightNNdouble&);
  // asin

  friend inline lightNNdouble acos(const lightNNdouble&);
  // acos

  friend inline lightNNdouble atan(const lightNNdouble&);
  // atan

  friend inline lightNNdouble sinh(const lightNNdouble&);
  // sinh

  friend inline lightNNdouble cosh(const lightNNdouble&);
  // cosh

  friend inline lightNNdouble tanh(const lightNNdouble&);
  // tanh

  friend inline lightNNdouble asinh(const lightNNdouble&);
  // asinh

  friend inline lightNNdouble acosh(const lightNNdouble&);
  // acosh

  friend inline lightNNdouble atanh(const lightNNdouble&);
  // atanh
  
  friend inline lightNNlm_complex ifloor(const lightNNlm_complex&);
  // ifloor

  friend inline lightNNlm_complex iceil(const lightNNlm_complex&);
  // iceil

  friend inline lightNNlm_complex irint(const lightNNlm_complex&);
  // irint

  friend inline lightNNlm_complex sqrt(const lightNNlm_complex&);
  // sqrt

  friend inline lightNNlm_complex exp(const lightNNlm_complex&);
  // exp

  friend inline lightNNlm_complex log(const lightNNlm_complex&);
  // log

  friend inline lightNNlm_complex sin(const lightNNlm_complex&);
  // sin

  friend inline lightNNlm_complex cos(const lightNNlm_complex&);
  // cos

  friend inline lightNNlm_complex tan(const lightNNlm_complex&);
  // tan

  friend inline lightNNlm_complex asin(const lightNNlm_complex&);
  // asin

  friend inline lightNNlm_complex acos(const lightNNlm_complex&);
  // acos

  friend inline lightNNlm_complex atan(const lightNNlm_complex&);
  // atan

  friend inline lightNNlm_complex sinh(const lightNNlm_complex&);
  // sinh

  friend inline lightNNlm_complex cosh(const lightNNlm_complex&);
  // cosh

  friend inline lightNNlm_complex tanh(const lightNNlm_complex&);
  // tanh

  friend inline lightNNlm_complex asinh(const lightNNlm_complex&);
  // asinh

  friend inline lightNNlm_complex acosh(const lightNNlm_complex&);
  // acosh

  friend inline lightNNlm_complex atanh(const lightNNlm_complex&);
  // atanh



#ifdef IN_LIGHTNNint_I_H
  friend inline lightNdouble light_mean     ( const  lightNNint& );
  friend inline lightNdouble light_standard_deviation( const  lightNNint& );
  friend inline lightNdouble light_variance ( const  lightNNint& );
  friend inline lightNdouble light_median   ( const  lightNNint& );
#else
  friend inline lightNint light_mean     ( const  lightNNint& );
  friend inline lightNint light_variance ( const  lightNNint& );
  friend inline lightNint light_standard_deviation( const  lightNNint& );
  friend inline lightNint light_median   ( const  lightNNint& );
#endif
  
  friend inline lightNint light_total    ( const  lightNNint& );
 
 
  friend inline lightNNint light_sort (const lightNNint arr1);
  
  friend inline lightNint light_flatten (const lightNNint s);
  friend inline lightNint light_flatten (const lightNNint s, int level);
  friend inline lightNNint light_flatten0 (const lightNNint s);


  //<ignore>
#ifdef IN_LIGHTNNlm_complex_I_H  
  friend class lightNNint;
#endif  
  friend class light3int;
  friend class light4int;
  friend class lightNint;
  friend class light33int;
  friend class light44int;
  friend class lightN3int;
  friend class lightNNNint;
  friend class lightNNNNint;
  friend class lightN33int;

  friend inline lightNNint light_join (const lightNNint, const lightNNint);
  friend inline lightNNint light_drop (const lightNNint arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNdouble::lightNNint(const lightNNint& s1, const double e, const lightmat_plus_enum)
  int *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.


protected:
  int sarea[LIGHTNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNNint(const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // two arguments is the size of the created matrix. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNint(const lightNNint&, const lightNNint&, const lightmat_plus_enum);
  lightNNint(const lightNNint&, const int, const lightmat_plus_enum);

#ifdef IN_LIGHTNNdouble_I_H
  lightNNint(const lightNNdouble&, const int, const lightmat_plus_enum);
  lightNNint(const lightNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNint(const lightNNint&, const lightNNint&, const lightmat_minus_enum);
  lightNNint(const int, const lightNNint&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNdouble_I_H
  lightNNint(const int, const lightNNdouble&, const lightmat_minus_enum);
  lightNNint(const double, const lightNNint&, const lightmat_minus_enum);
#endif

  lightNNint(const lightNNint&, const lightNNint&, const lightmat_mult_enum);
  lightNNint(const lightNNint&, const int, const lightmat_mult_enum);

#ifdef IN_LIGHTNNdouble_I_H
  lightNNint(const lightNNdouble&, const int, const lightmat_mult_enum);
  lightNNint(const lightNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNint(const lightNNint&, const light33int&, const lightmat_mult_enum);
  lightNNint(const lightNNint&, const light44int&, const lightmat_mult_enum);
  lightNNint(const lightN3int&, const lightNNint&, const lightmat_mult_enum);
  lightNNint(const light33int&, const lightNNint&, const lightmat_mult_enum);
  lightNNint(const light44int&, const lightNNint&, const lightmat_mult_enum);
  lightNNint(const lightNint&, const lightNNNint&, const lightmat_mult_enum);
  lightNNint(const lightNNNint&, const lightNint&, const lightmat_mult_enum);
  lightNNint(const light3int&, const lightNNNint&, const lightmat_mult_enum);
  lightNNint(const lightNNNint&, const light3int&, const lightmat_mult_enum);
  lightNNint(const light4int&, const lightNNNint&, const lightmat_mult_enum);
  lightNNint(const lightNNNint&, const light4int&, const lightmat_mult_enum);

  lightNNint(const int, const lightNNint&, const lightmat_div_enum);
#ifdef IN_LIGHTNNdouble_I_H
  lightNNint(const int, const lightNNdouble&, const lightmat_div_enum);
  lightNNint(const double, const lightNNint&, const lightmat_div_enum);
#endif

  lightNNint(const lightNNint&, const lightNNint&, const lightmat_pow_enum);
  lightNNint(const lightNNint&, const int, const lightmat_pow_enum);
  lightNNint(const int, const lightNNint&, const lightmat_pow_enum);
  //lightNNint(const lightNNint&, const lightmat_abs_enum);

#ifdef IN_LIGHTNNlm_complex_I_H
 #else
  #ifdef IN_LIGHTNNdouble_I_H
   lightNNint(const lightNNint&, const lightmat_abs_enum);
   lightNNint(const lightNNlm_complex&, const lightmat_abs_enum);
  #else
  lightNNint(const lightNNint&, const lightmat_abs_enum);
  #endif
#endif

  lightNNint(const lightNNint&, const lightNNint&, const lightmat_eprod_enum);
  lightNNint(const lightNNint&, const lightNNint&, const lightmat_equot_enum);
  lightNNint(const lightNNint&, int f(int), const lightmat_apply_enum);
  lightNNint(const lightNNint&, const lightNNint&, int f(int, int), const lightmat_apply_enum);
  lightNNint(const lightN3int&, const lightmat_trans_enum);
  lightNNint(const lightNint&, const lightNint&, const lightmat_outer_enum);

};

typedef lightNNdouble doubleNN;
typedef lightNNint intNN;
typedef lightNNlm_complex lm_complexNN;

#undef IN_LIGHTNNint_I_H


#endif
