//           -*- c++ -*-


#ifdef LIGHTMAT_TEMPLATES
// First some basic stuff for calculation
#include "../icc/light_basic.icc"
// and the inlined functions

//CONV_DOUBLE + CONV_SELF replaced by CONV_INT_2_DOUBLE
//#define CONV_DOUBLE
//#define CONV_SELF
#define CONV_INT_2_DOUBLE
#define CONV_INT_2_COMPLEX

#include "../icc/light3.icc"
#include "../icc/light4.icc"
#include "../icc/light33.icc"
#include "../icc/light44.icc"
#include "../icc/lightN.icc"
#include "../icc/lightN3.icc"
#include "../icc/lightNN.icc"
#include "../icc/lightN33.icc"
#include "../icc/lightNNN.icc"
#include "../icc/lightNNNN.icc"
//#undef CONV_DOUBLE
//#undef CONV_SELF
#undef CONV_INT_2_DOUBLE
#undef CONV_INT_2_COMPLEX
// and last some special files which didn't fit in the other files:
// Functions for objects with used elements of type double
#include "../icc/light_double.icc"

// Functions for objects with returned elements of type int
#include "../icc/light_int.icc"

// Functions that call other funtions that do the actual work
#include "../icc/light_calc.icc"

// Functions that do conversions between different element types
#include "../icc/light_conversion.icc"

// Functions that create strings and output them
#include "../icc/light_output.icc"

// Set of functions used for vector transformations.
#include "../icc/transf.icc"




// END OF LIGHTMAT_TEMPLATES
#else
// START OF NOT LIGHTMAT_TEMPLATES


#include "../icc/lm_complex.icc"
// First some basic stuff for calculation
#include "../icc/d_light_basic.icc"
#include "../icc/i_light_basic.icc"
#define COMPLEX_TOOLS
#include "../icc/c_light_basic.icc"
#undef COMPLEX_TOOLS
// and the inlined functions

#define CONV_DOUBLE_2_COMPLEX
#include "../icc/d_light3.icc"
#include "../icc/d_light4.icc"
#include "../icc/d_light33.icc"
#include "../icc/d_light44.icc"
#include "../icc/d_lightN.icc"
#include "../icc/d_lightN3.icc"
#include "../icc/d_lightNN.icc"
#include "../icc/d_lightN33.icc"
#include "../icc/d_lightNNN.icc"
#include "../icc/d_lightNNNN.icc"
#undef CONV_DOUBLE_2_COMPLEX

#define CONV_INT_2_DOUBLE
#define CONV_INT_2_COMPLEX
//#define CONV_DOUBLE
//#define CONV_SELF
#include "../icc/i_light3.icc"
#include "../icc/i_light4.icc"
#include "../icc/i_light33.icc"
#include "../icc/i_light44.icc"
#include "../icc/i_lightN.icc"
#include "../icc/i_lightN3.icc"
#include "../icc/i_lightNN.icc"
#include "../icc/i_lightN33.icc"
#include "../icc/i_lightNNN.icc"
#include "../icc/i_lightNNNN.icc"
//#undef CONV_DOUBLE
//#undef CONV_SELF
#undef CONV_INT_2_DOUBLE
#undef CONV_INT_2_COMPLEX

#define COMPLEX_TOOLS

#include "../icc/c_light3.icc"
#include "../icc/c_light4.icc"
#include "../icc/c_light33.icc"
#include "../icc/c_light44.icc"
#include "../icc/c_lightN.icc"
#include "../icc/c_lightN3.icc"
#include "../icc/c_lightNN.icc"
#include "../icc/c_lightN33.icc"
#include "../icc/c_lightNNN.icc"
#include "../icc/c_lightNNNN.icc"

#undef COMPLEX_TOOLS


// and last some special files which didn't fit in the other files:
// Functions for objects with used elements of type double
#include "../icc/d_light_double.icc"
#include "../icc/d_light_double_complex.icc"
// Functions for objects with returned elements of type int
#include "../icc/i_light_int.icc"
#include "../icc/d_light_int.icc"

// Functions that call other funtions that do the actual work
#include "../icc/d_light_calc.icc"
#include "../icc/i_light_calc.icc"

// Functions that do conversions between different element types
#include "../icc/d_light_conversion.icc"

// Functions that create strings and output them
#include "../icc/d_light_output.icc"
#include "../icc/i_light_output.icc"

// Set of functions used for vector transformations.
#include "../icc/transf.icc"

//Functions for complex numbers
#define COMPLEX_TOOLS

#include "../icc/c_light_int.icc"
#include "../icc/c_light_double_complex.icc"
#include "../icc/c_light_complex.icc"
#include "../icc/c_light_calc.icc"
#include "../icc/c_light_output.icc"

#undef COMPLEX_TOOLS

#endif /* NOT LIGHTMAT_TEMPLATES */
