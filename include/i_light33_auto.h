inline light33int& Set (const int i0, const int i1, const int val);
inline light33int& Set (const R r0, const int i1, const lightNint& arr);
inline light33int& Set (const R r0, const int i1, const light3int& arr);
inline light33int& Set (const R r0, const int i1, const light4int& arr);
inline light33int& Set (const R r0, const int i1, const int val);
inline light33int& Set (const int i0, const R r1, const lightNint& arr);
inline light33int& Set (const int i0, const R r1, const light3int& arr);
inline light33int& Set (const int i0, const R r1, const light4int& arr);
inline light33int& Set (const int i0, const R r1, const int val);
inline light33int& Set (const R r0, const R r1, const lightNNint& arr);
inline light33int& Set (const R r0, const R r1, const light33int& arr);
inline light33int& Set (const R r0, const R r1, const light44int& arr);
inline light33int& Set (const R r0, const R r1, const int val);
inline lightNint operator() (const R r0, const int i1) const;
inline lightNint operator() (const int i0, const R r1) const;
inline lightNNint operator() (const R r0, const R r1) const;
friend inline light33double atan2 (const double e, const light33int &s1);
friend inline light33double atan2 (const double e, const light33double &s1);
friend inline light33double atan2 (const light33int &s1, const double e);
friend inline light33double atan2 (const light33double &s1, const double e);
friend inline light33double atan2 (const light33int &s1, const light33int &s2);
friend inline light33double atan2 (const light33int &s1, const light33double &s2);
friend inline light33double atan2 (const light33double &s1, const light33int &s2);
friend inline light33double atan2 (const light33double &s1, const light33double &s2);
friend inline light33double atan2 (const light33int &s1, const int e);
friend inline light33double atan2 (const light33double &s1, const int e);
friend inline light33double atan2 (const int e, const light33int &s1);
friend inline light33double atan2 (const int e, const light33double &s1);

#ifdef IN_LIGHT33double_I_H
inline light33double(const double e, const light33int &s1, const lightmat_atan2_enum);
inline light33double(const double e, const light33double &s1, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const double e, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const double e, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const light33int &s2, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const light33double &s2, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const light33int &s2, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const light33double &s2, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const int e, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const int e, const lightmat_atan2_enum);
inline light33double(const int e, const light33int &s1, const lightmat_atan2_enum);
inline light33double(const int e, const light33double &s1, const lightmat_atan2_enum);

#endif
