//           -*- c++ -*-

#ifndef LIGHTNNN_H
#define LIGHTNNN_H
// <cd> lightNNNdouble
//
// .SS Functionality
//
// lightNNN is a template for classes that implement tensors of rank 3
// with any number of elements. E.g. a lightNNN s with 5x6x7 elements of
// type double can be instanciated with:
//
// <code>lightNNN&lt;double&gt; s(5,6,7);</code>
//
// The size of the tensor can change during execution. It changes its
// size to whatever is needed when it is assigned a new value with an
// assignment operator.
//
// When a lightNNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNNN object with 5x6x5 elements to one with 5x5x6 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHTNNNdouble_H

#define LIGHTNNN_SIZE1 5
#define LIGHTNNN_SIZE2 5
#define LIGHTNNN_SIZE3 5
#define LIGHTNNN_SIZE (LIGHTNNN_SIZE1*LIGHTNNN_SIZE2*LIGHTNNN_SIZE3)
// The default size.

 class lightN;
 class lightNN;
 class lightN33;
 class lightNNNN;


class lightNNNdouble {
public:

#ifdef IN_LIGHTNNNdouble_H
  friend class lightNNNint;
#else
  friend class lightNNNdouble;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNNint;
#endif

  #include "d_lightNNN_auto.h"

  inline lightNNNdouble();
  // Default constructor.

  inline lightNNNdouble(const lightNNNdouble&);
  // Copy constructor.

  inline lightNNNdouble(const lightN33double&);
  // Conversion.

  inline lightNNNdouble(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNNdouble(const int, const int, const int);
  // Construct a lightNNN of given size.

  inline lightNNNdouble(const int, const int, const int, const double *);
  // Construct a lightNNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNNdouble(const int, const int, const int, const double);
  // Construct a lightNNN of given size and initialize all elements
  // with a value (the last argument).

  inline ~lightNNNdouble();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNNlm_complex();
  // Convert to complex.
  
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNNlm_complex();
  // Convert to complex.
#endif

  lightNNNdouble& operator=(const lightNNNdouble&);
  // Assignment.

  lightNNNdouble& operator=(const lightN33double&);
  // Assignment, change size to Nx3x3.

  lightNNNdouble& operator=(const double);
  // Assign one value to all elements.

  double operator() (const int x, const int y, const int z) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3));
#ifdef ROWMAJOR
    return elem[((x-1)*size2+(y-1))*size3+z-1];
#else
    return elem[((z-1)*size2+(y-1))*size1+x-1];
#endif
  };
  // Get the value of one element.


  double& operator()(const int x, const int y, const int z) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) || (z<1) || (z>size3));
#ifdef ROWMAJOR
    return elem[((x-1)*size2+(y-1))*size3+z-1];
#else
    return elem[((z-1)*size2+(y-1))*size1+x-1];
#endif
  };
  // Get/Set the value of one element.

  lightNdouble operator()(const int, const int) const;
  // Get the value of a vector in the tensor.

  lightNNdouble operator()(const int) const;
  // Get the value of a matrix in the tensor.




  //
  // Set
  //

  inline lightNNNdouble& Set (const int i0, const int i1, const double val);
  inline lightNNNdouble& Set (const int i0, const int i1, const lightNdouble& arr);
  inline lightNNNdouble& Set (const int i0, const int i1, const light3double& arr);
  inline lightNNNdouble& Set (const int i0, const int i1, const light4double& arr);

  inline lightNNNdouble& Set (const int i0, const double val);
  inline lightNNNdouble& Set (const int i0, const lightNNdouble& arr);
  inline lightNNNdouble& Set (const int i0, const light33double& arr);
  inline lightNNNdouble& Set (const int i0, const light44double& arr);


 
  int operator==(const lightNNNdouble&) const;
  // Equality.

  int operator!=(const lightNNNdouble&) const;
  // Inequality.
 
  lightNNNdouble& operator+=(const double);
  // Add a value to all elements.

  lightNNNdouble& operator+=(const lightNNNdouble&);
  // Elementwise addition.

  lightNNNdouble& operator-=(const double);
  // Subtract a value from all elements.

  lightNNNdouble& operator-=(const lightNNNdouble&);
  // Elementwise subtraction.

  lightNNNdouble& operator*=(const double);
  // Mulitply all elements with a value.

  lightNNNdouble& operator/=(const double);
  // Divide all elements with a value.

  double Extract(const lightNint&) const;
  // Extract an element using given index. 

  inline lightNNNdouble& reshape(const int, const int, const int, const lightNdouble&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c) will be set to s(a) in the lightNNNdouble
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector. The program may dump
  // core or behave strangely if the arguments are incorrect.

  inline lightNNNdouble& reshape(const int, const int, const int, const lightNNdouble&);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c) will be set to s(a,b) in the
  // lightNNN object. The values of the first two arguments must be
  // the same as the size of the lightNN-matrix. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNdouble& SetShape(const int x=-1, const int y=-1, const int z=-1); 
  // Set new shape for the tensor.

  const  lightNNdouble& SetMatrix1(const int n,const lightNNdouble& x);
  // Assigns Tensor[n,_,_]=Matrix

  const  lightNdouble& SetVector12(const int n,const int m,const lightNdouble& x);
  // Assigns Tensor[n,m,_]=Vector 

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else if(x == 3)
      return size3;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(double *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const double *);
  // Set values of all elements from array (row major order).
 
  double * data() const;
  // Direct access to the stored data. Use the result with care.

  lightNNNdouble operator+() const;
  // Unary plus.

  lightNNNdouble operator-() const;
  // Unary minus.

  friend inline lightNNNdouble operator+(const lightNNNdouble&, const lightNNNdouble&);
  // Elementwise addition.

  friend inline lightNNNdouble operator+(const lightNNNdouble&, const double);
  // Addition to all elements.

  friend inline lightNNNdouble operator+(const double, const lightNNNdouble&);
  // Addition to all elements.

#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNNdouble operator+(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator+(const lightNNNint&, const double);
  friend inline lightNNNdouble operator+(const double, const lightNNNint&);
  friend inline lightNNNdouble operator+(const int, const lightNNNdouble&);
#endif

  #ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator+(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator+(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator+(const double e, const lightNNNlm_complex& s2);
#endif

  friend inline lightNNNdouble operator-(const lightNNNdouble&, const lightNNNdouble&);
  // Elementwise subtraction.

  friend inline lightNNNdouble operator-(const lightNNNdouble&, const double);
  // Subtraction from all elements.

  friend inline lightNNNdouble operator-(const double, const lightNNNdouble&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNNdouble operator-(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator-(const lightNNNint&, const double);
  friend inline lightNNNdouble operator-(const double, const lightNNNint&);
  friend inline lightNNNdouble operator-(const int, const lightNNNdouble&);
#endif

  #ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator-(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator-(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator-(const double e, const lightNNNlm_complex& s2);

#endif

  friend inline lightNNNdouble operator*(const lightNNNdouble&, const double);
  // Multiply all elements.

  friend inline lightNNNdouble operator*(const double, const lightNNNdouble&);
  // Multiply all elements.

#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNNdouble operator*(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator*(const lightNNNint&, const double);
  friend inline lightNNNdouble operator*(const double, const lightNNNint&);
  friend inline lightNNNdouble operator*(const int, const lightNNNdouble&);
#endif

#ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator*(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator*(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator*(const double e, const lightNNNlm_complex& s2);
#endif

  friend inline lightNNNdouble operator*(const lightNNNdouble&, const lightNNdouble&);
  // Inner product.

  friend inline lightNNNdouble operator*(const lightNNdouble&, const lightNNNdouble&);
  // Inner product.

  friend inline lightNNNdouble operator*(const lightNNNNdouble&, const lightNdouble&);
  // Inner product.

  friend inline lightNNNdouble operator*(const lightNdouble&,	const lightNNNNdouble&);
  // Inner product.

  friend inline lightNNNdouble operator*(const lightNNNdouble&, const light33double&);
  // Inner product.

  friend inline lightNNNdouble operator*(const light33double&, const lightNNNdouble&);
  // Inner product.

  friend inline lightNNNdouble operator*(const lightNNNNdouble&, const light3double&);
  // Inner product.

  friend inline lightNNNdouble operator*(const light3double&, const lightNNNNdouble&);
  // Inner product.

  friend inline lightNNNdouble operator*(const lightNNNdouble&, const light44double&);
  // Inner product.

  friend inline lightNNNdouble operator*(const light44double&, const lightNNNdouble&);
  // Inner product.

  friend inline lightNNNdouble operator*(const lightNNNNdouble&, const light4double&);
  // Inner product.

  friend inline lightNNNdouble operator*(const light4double&, const lightNNNNdouble&);
  // Inner product.

  friend inline lightNNNdouble operator/(const lightNNNdouble&, const double);
  // Divide all elements.

  friend inline lightNNNdouble operator/(const double, const lightNNNdouble&);
  // Divide all elements.

#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNNdouble operator/(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator/(const lightNNNint&, const double);
  friend inline lightNNNdouble operator/(const double, const lightNNNint&);
  friend inline lightNNNdouble operator/(const int, const lightNNNdouble&);
#endif

#ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator/(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator/(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator/(const double e, const lightNNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNNlm_complex_H
friend inline lightNNNdouble arg(const lightNNNlm_complex& a);
friend inline lightNNNdouble re(const lightNNNlm_complex& a);
friend inline lightNNNdouble im(const lightNNNlm_complex& a);
friend inline lightNNNlm_complex conjugate(const lightNNNlm_complex& a);
//#endif

  friend inline lightNNNdouble pow(const lightNNNdouble&, const lightNNNdouble&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNNdouble pow(const lightNNNdouble&, const double);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNdouble pow(const double, const lightNNNdouble&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNdouble ElemProduct(const lightNNNdouble&, const lightNNNdouble&);
  // Elementwise multiplication.

  friend inline lightNNNdouble ElemQuotient(const lightNNNdouble&, const lightNNNdouble&);
  // Elementwise division.

  friend inline lightNNNdouble Apply(const lightNNNdouble&, double f(double));
  // Apply the function elementwise all elements.

  friend inline lightNNNdouble Apply(const lightNNNdouble&, const lightNNNdouble&, double f(double, double));
  // Apply the function elementwise on all elements in the two tensors.

  friend inline lightNNNdouble Transpose(const lightNNNdouble&);
  // Transpose.

#ifdef IN_LIGHTNNNlm_complex_H
#else
  friend inline lightNNNdouble abs(const lightNNNdouble&);
#ifdef IN_LIGHTNNNdouble_H
  friend inline lightNNNdouble abs(const lightNNNlm_complex&);
#endif 
#endif


#ifndef COMPLEX_TOOLS
  // sign
  friend inline lightNNNint sign(const lightNNNint&);
  // sign
  friend inline lightNNNint sign(const lightNNNdouble&);
  // abs
#else
  friend inline lightNNNlm_complex sign(const lightNNNlm_complex&);
#endif

 /// Added 2/2/98 

  friend inline lightNNNdouble  FractionalPart(const lightNNNdouble&);
  //  FractionalPart

  friend inline lightNNNdouble  IntegerPart(const lightNNNdouble&);
  //  IntegerPart

  //friend inline lightNNNint  IntegerPart(const lightNNNdouble&);
  //  IntegerPart

  friend inline lightNNNdouble Mod (const  lightNNNdouble&,const  lightNNNdouble&);
  // Mod
 
  friend inline lightNNNdouble Mod (const  lightNNNdouble&,const double);
  // Mod

  friend inline lightNNNdouble Mod (const double,const  lightNNNdouble&);
  // Mod

  friend inline double LightMax (const lightNNNdouble& );
  // Max

  friend inline double LightMin (const lightNNNdouble& );
  // Min

  friend inline double findLightMax (const  double *,const  int );
  // Find Max
 
  friend inline double findLightMin (const  double *,const  int );
  // Find Min
 
  /// End Added 



  friend inline lightNNNint ifloor(const lightNNNdouble&);
  // ifloor

  friend inline lightNNNint iceil(const lightNNNdouble&);
  // iceil

  friend inline lightNNNint irint(const lightNNNdouble&);
  // irint

  friend inline lightNNNdouble sqrt(const lightNNNdouble&);
  // sqrt

  friend inline lightNNNdouble exp(const lightNNNdouble&);
  // exp

  friend inline lightNNNdouble log(const lightNNNdouble&);
  // log

  friend inline lightNNNdouble sin(const lightNNNdouble&);
  // sin

  friend inline lightNNNdouble cos(const lightNNNdouble&);
  // cos

  friend inline lightNNNdouble tan(const lightNNNdouble&);
  // tan

  friend inline lightNNNdouble asin(const lightNNNdouble&);
  // asin

  friend inline lightNNNdouble acos(const lightNNNdouble&);
  // acos

  friend inline lightNNNdouble atan(const lightNNNdouble&);
  // atan

  friend inline lightNNNdouble sinh(const lightNNNdouble&);
  // sinh

  friend inline lightNNNdouble cosh(const lightNNNdouble&);
  // cosh

  friend inline lightNNNdouble tanh(const lightNNNdouble&);
  // tanh

  friend inline lightNNNdouble asinh(const lightNNNdouble&);
  // asinh

  friend inline lightNNNdouble acosh(const lightNNNdouble&);
  // acosh

  friend inline lightNNNdouble atanh(const lightNNNdouble&);
  // atanh

   friend inline lightNNNlm_complex ifloor(const lightNNNlm_complex&);
  // ifloor

  friend inline lightNNNlm_complex iceil(const lightNNNlm_complex&);
  // iceil

  friend inline lightNNNlm_complex irint(const lightNNNlm_complex&);
  // irint

  friend inline lightNNNlm_complex sqrt(const lightNNNlm_complex&);
  // sqrt

  friend inline lightNNNlm_complex exp(const lightNNNlm_complex&);
  // exp

  friend inline lightNNNlm_complex log(const lightNNNlm_complex&);
  // log

  friend inline lightNNNlm_complex sin(const lightNNNlm_complex&);
  // sin

  friend inline lightNNNlm_complex cos(const lightNNNlm_complex&);
  // cos

  friend inline lightNNNlm_complex tan(const lightNNNlm_complex&);
  // tan

  friend inline lightNNNlm_complex asin(const lightNNNlm_complex&);
  // asin

  friend inline lightNNNlm_complex acos(const lightNNNlm_complex&);
  // acos

  friend inline lightNNNlm_complex atan(const lightNNNlm_complex&);
  // atan

  friend inline lightNNNlm_complex sinh(const lightNNNlm_complex&);
  // sinh

  friend inline lightNNNlm_complex cosh(const lightNNNlm_complex&);
  // cosh

  friend inline lightNNNlm_complex tanh(const lightNNNlm_complex&);
  // tanh

  friend inline lightNNNlm_complex asinh(const lightNNNlm_complex&);
  // asinh

  friend inline lightNNNlm_complex acosh(const lightNNNlm_complex&);
  // acosh

  friend inline lightNNNlm_complex atanh(const lightNNNlm_complex&);
  // atanh

#ifdef IN_LIGHTNNNint_H
  friend inline lightNNdouble light_mean     ( const  lightNNNdouble& );
  friend inline lightNNdouble light_standard_deviation( const  lightNNNdouble& );
  friend inline lightNNdouble light_variance ( const  lightNNNdouble& );
  friend inline lightNNdouble light_median   ( const  lightNNNdouble& );
#else
  friend inline lightNNdouble light_mean     ( const  lightNNNdouble& );
  friend inline lightNNdouble light_variance ( const  lightNNNdouble& );
  friend inline lightNNdouble light_standard_deviation( const  lightNNNdouble& );
  friend inline lightNNdouble light_median   ( const  lightNNNdouble& );
#endif
  
  friend inline lightNNdouble light_total    ( const  lightNNNdouble& );
  
  friend inline lightNNNdouble light_sort (const lightNNNdouble arr1);
  
  friend inline lightNdouble light_flatten (const lightNNNdouble s);
  friend inline lightNdouble light_flatten (const lightNNNdouble s, int level);
  friend inline lightNNNdouble light_flatten0 (const lightNNNdouble s);
  friend inline lightNNdouble light_flatten1 (const lightNNNdouble s);

  //<ignore>
#ifdef IN_LIGHTNNNlm_complex_H
  friend class lightNNNint;
#endif

  friend class lightNdouble;
  friend class lightNNdouble;
  friend class lightN33double;
  friend class lightNNNNdouble;

  friend inline lightNNNdouble light_join (const lightNNNdouble, const lightNNNdouble);
  friend inline lightNNNdouble light_drop (const lightNNNdouble arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNNdouble::lightNNNdouble(const lightNNNint& s1, const double e, const lightmat_plus_enum)

  double *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // Size of the first index.

  int size2;
  // Size of the second index.

  int size3;
  // Size of the third index.

protected:
  double sarea[LIGHTNNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNNNdouble(const int, const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // three arguments is the size of the created tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNNdouble(const lightNNNdouble&, const lightNNNdouble&, const lightmat_plus_enum);
  lightNNNdouble(const lightNNNdouble&, const double, const lightmat_plus_enum);

#ifdef IN_LIGHTNNNdouble_H
  lightNNNdouble(const lightNNNdouble&, const int, const lightmat_plus_enum);
  lightNNNdouble(const lightNNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNNdouble(const lightNNNdouble&, const lightNNNdouble&, const lightmat_minus_enum);
  lightNNNdouble(const double, const lightNNNdouble&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNNdouble_H
  lightNNNdouble(const int, const lightNNNdouble&, const lightmat_minus_enum);
  lightNNNdouble(const double, const lightNNNint&, const lightmat_minus_enum);
#endif

  lightNNNdouble(const lightNNNdouble&, const double, const lightmat_mult_enum);

#ifdef IN_LIGHTNNNdouble_H
  lightNNNdouble(const lightNNNdouble&, const int, const lightmat_mult_enum);
  lightNNNdouble(const lightNNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNNdouble(const lightNNNdouble&, const lightNNdouble&, const lightmat_mult_enum);
  lightNNNdouble(const lightNNdouble&, const lightNNNdouble&, const lightmat_mult_enum);
  lightNNNdouble(const lightNNNNdouble&, const lightNdouble&, const lightmat_mult_enum);
  lightNNNdouble(const lightNdouble&, const lightNNNNdouble&, const lightmat_mult_enum);
  lightNNNdouble(const lightNNNdouble&, const light33double&, const lightmat_mult_enum);
  lightNNNdouble(const light33double&, const lightNNNdouble&, const lightmat_mult_enum);
  lightNNNdouble(const lightNNNNdouble&, const light3double&, const lightmat_mult_enum);
  lightNNNdouble(const light3double&, const lightNNNNdouble&, const lightmat_mult_enum);
  lightNNNdouble(const lightNNNdouble&, const light44double&, const lightmat_mult_enum);
  lightNNNdouble(const light44double&, const lightNNNdouble&, const lightmat_mult_enum);
  lightNNNdouble(const lightNNNNdouble&, const light4double&, const lightmat_mult_enum);
  lightNNNdouble(const light4double&, const lightNNNNdouble&, const lightmat_mult_enum);
  lightNNNdouble(const double, const lightNNNdouble&, const lightmat_div_enum);

#ifdef IN_LIGHTNNNdouble_H
  lightNNNdouble(const int, const lightNNNdouble&, const lightmat_div_enum);
  lightNNNdouble(const double, const lightNNNint&, const lightmat_div_enum);
#endif

  lightNNNdouble(const lightNNNdouble&, const lightNNNdouble&, const lightmat_pow_enum);
  lightNNNdouble(const lightNNNdouble&, const double, const lightmat_pow_enum);
  lightNNNdouble(const double, const lightNNNdouble&, const lightmat_pow_enum);

#ifdef IN_LIGHTNNNlm_complex_H
 #else
  #ifdef IN_LIGHTNNNdouble_H
   lightNNNdouble(const lightNNNdouble&, const lightmat_abs_enum);
   lightNNNdouble(const lightNNNlm_complex&, const lightmat_abs_enum);
  #else
  lightNNNdouble(const lightNNNdouble&, const lightmat_abs_enum);
  #endif
#endif


  lightNNNdouble(const lightNNNdouble&, const lightNNNdouble&, const lightmat_eprod_enum);
  lightNNNdouble(const lightNNNdouble&, const lightNNNdouble&, const lightmat_equot_enum);
  lightNNNdouble(const lightNNNdouble&, double f(double), const lightmat_apply_enum);
  lightNNNdouble(const lightNNNdouble&, const lightNNNdouble&, double f(double, double), const lightmat_apply_enum);
  lightNNNdouble(const lightNNNdouble&, const lightmat_trans_enum);
};

typedef lightNNNdouble doubleNNN;
typedef lightNNNint intNNN;
typedef lightNNNlm_complex lm_complexNNN;

#undef IN_LIGHTNNNdouble_H

#endif
