inline lightNNint& Set (const int i0, const int i1, const int val);
inline lightNNint& Set (const R r0, const int i1, const lightNint& arr);
inline lightNNint& Set (const R r0, const int i1, const light3int& arr);
inline lightNNint& Set (const R r0, const int i1, const light4int& arr);
inline lightNNint& Set (const R r0, const int i1, const int val);
inline lightNNint& Set (const int i0, const R r1, const lightNint& arr);
inline lightNNint& Set (const int i0, const R r1, const light3int& arr);
inline lightNNint& Set (const int i0, const R r1, const light4int& arr);
inline lightNNint& Set (const int i0, const R r1, const int val);
inline lightNNint& Set (const R r0, const R r1, const lightNNint& arr);
inline lightNNint& Set (const R r0, const R r1, const light33int& arr);
inline lightNNint& Set (const R r0, const R r1, const light44int& arr);
inline lightNNint& Set (const R r0, const R r1, const int val);
inline lightNint operator() (const R r0, const int i1) const;
inline lightNint operator() (const int i0, const R r1) const;
inline lightNNint operator() (const R r0, const R r1) const;

#ifdef IN_LIGHTNNdouble_I_H
inline lightNNdouble(const double e, const lightNNint &s1, const lightmat_atan2_enum);
inline lightNNdouble(const double e, const lightNNdouble &s1, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const double e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const double e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const lightNNint &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const lightNNdouble &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const lightNNint &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const lightNNdouble &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const int e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const int e, const lightmat_atan2_enum);
inline lightNNdouble(const int e, const lightNNint &s1, const lightmat_atan2_enum);
inline lightNNdouble(const int e, const lightNNdouble &s1, const lightmat_atan2_enum);

#endif
