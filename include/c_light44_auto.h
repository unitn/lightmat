inline light44lm_complex& Set (const int i0, const int i1, const lm_complex val);
inline light44lm_complex& Set (const R r0, const int i1, const lightNlm_complex& arr);
inline light44lm_complex& Set (const R r0, const int i1, const light3lm_complex& arr);
inline light44lm_complex& Set (const R r0, const int i1, const light4lm_complex& arr);
inline light44lm_complex& Set (const R r0, const int i1, const lm_complex val);
inline light44lm_complex& Set (const int i0, const R r1, const lightNlm_complex& arr);
inline light44lm_complex& Set (const int i0, const R r1, const light3lm_complex& arr);
inline light44lm_complex& Set (const int i0, const R r1, const light4lm_complex& arr);
inline light44lm_complex& Set (const int i0, const R r1, const lm_complex val);
inline light44lm_complex& Set (const R r0, const R r1, const lightNNlm_complex& arr);
inline light44lm_complex& Set (const R r0, const R r1, const light33lm_complex& arr);
inline light44lm_complex& Set (const R r0, const R r1, const light44lm_complex& arr);
inline light44lm_complex& Set (const R r0, const R r1, const lm_complex val);
inline lightNlm_complex operator() (const R r0, const int i1) const;
inline lightNlm_complex operator() (const int i0, const R r1) const;
inline lightNNlm_complex operator() (const R r0, const R r1) const;
friend inline light44double atan2 (const double e, const light44int &s1);
friend inline light44double atan2 (const double e, const light44double &s1);
friend inline light44double atan2 (const light44int &s1, const double e);
friend inline light44double atan2 (const light44double &s1, const double e);
friend inline light44double atan2 (const light44int &s1, const light44int &s2);
friend inline light44double atan2 (const light44int &s1, const light44double &s2);
friend inline light44double atan2 (const light44double &s1, const light44int &s2);
friend inline light44double atan2 (const light44double &s1, const light44double &s2);
friend inline light44double atan2 (const light44int &s1, const int e);
friend inline light44double atan2 (const light44double &s1, const int e);
friend inline light44double atan2 (const int e, const light44int &s1);
friend inline light44double atan2 (const int e, const light44double &s1);

#ifdef IN_LIGHT44double_C_H
inline light44double(const double e, const light44int &s1, const lightmat_atan2_enum);
inline light44double(const double e, const light44double &s1, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const double e, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const double e, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const light44int &s2, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const light44double &s2, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const light44int &s2, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const light44double &s2, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const int e, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const int e, const lightmat_atan2_enum);
inline light44double(const int e, const light44int &s1, const lightmat_atan2_enum);
inline light44double(const int e, const light44double &s1, const lightmat_atan2_enum);

#endif
