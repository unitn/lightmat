//           -*- c++ -*-

#ifndef _TOSTR_H_
#define _TOSTR_H_
#include "rw/cstring.h"


RWCString ToStr(const int val);
// Convert int to string. All leading and trailing spaces are elliminated.

RWCString ToStr(const double val);
// Convert double to string. All leading and trailing spaces are elliminated.
// All trailing zeros are elliminated.

RWCString ToStr(const lm_complex val);
// Convert lm_complex to string. All leading and trailing spaces are elliminated.
// All trailing zeros are elliminated.

RWCString ToStrS(const int val);
// Convert int to string. All leading and trailing spaces are elliminated
// except a space representing a plus sign.

RWCString ToStrS(const double val);
// Convert double to string. All leading and trailing spaces are elliminated
// except a space representing a plus sign.
// All trailing zeros are elliminated.


RWCString ToStrA(const double val);
// This is for variables that should be a multiple of pi.
// Convert double to string. All leading and trailing spaces are elliminated.
// All trailing zeros are elliminated.

RWCString ToStrSA(const double val);
// This is for variables that should be a multiple of pi.
// Convert double to string. All leading and trailing spaces are elliminated
// except a space representing a plus sign.
// All trailing zeros are elliminated.

#endif
