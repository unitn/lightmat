//           -*- c++ -*-

#if defined(_MSC_VER) || defined (MWCC)
#include "rw\cstring.h"
#else
#include <rw/cstring.h>
#endif


/* 
  ************************************************************* 
  HERE IS AREA FOR PORTABILITY of inlined Mathematical FUNCTIONs
  **************************************************************
 */
/* use   
      inline foo(ftype fie);
   in transf.h
   and adequate
      inline foo(ftype fie) { body };
   in transf.icc
*/

inline double abs(const lm_complex x);
inline lm_complex sign(const lm_complex & x );
inline lm_complex pow(lm_complex  a, lm_complex b);
inline lm_complex pow(int  a, lm_complex b);
inline lm_complex pow(double  a, lm_complex b);
inline lm_complex pow(lm_complex  a, int b);
inline lm_complex pow(lm_complex  a, double b);
inline lm_complex asin (lm_complex d);
inline lm_complex asinh (lm_complex d);
inline lm_complex acos (lm_complex d);
inline lm_complex acosh (lm_complex d);
inline lm_complex atan (lm_complex d);
inline lm_complex atanh (lm_complex d);
inline lm_complex exp(lm_complex d); 
inline lm_complex sqrt(lm_complex d); 
inline lm_complex log(lm_complex d); 
inline lm_complex sin(lm_complex d); 
inline lm_complex cos(lm_complex d); 
inline lm_complex tan(lm_complex d); 
inline lm_complex cot(lm_complex d); 
inline lm_complex cosh(lm_complex d); 
inline lm_complex sinh(lm_complex d); 
inline lm_complex tanh(lm_complex d); 
inline lm_complex coth(lm_complex d); 


// Added 060613, needed for all compilers,

inline double pow(int , double);


#ifdef __alpha
// DEC c++ compiler requires this.
inline double abs(const double x);
inline double pow(double  a, int b);
inline int  pow(int  a, int b);
#endif


#ifdef __GNUG__
// Gnu compiler starting from version 3.0
// produces messages about ambiguity for
// these cases.

// but not for __clang__=4  when  _GLIBCXX_RELEASE=8
#ifndef __clang__
inline double pow(double a, int b);
#endif

inline double pow(int a, int b) ;
#endif

#ifdef __GNUG__
inline int  abs(const int x);
#ifndef __clang__

#if __GNUC__ < 6
inline double  abs(const double x);
#endif

#endif
#endif





#ifdef __SUNPRO_CC

#if (__SUNPRO_CC >= 0x540)
#define EXPLICIT_INT_ARGS
#endif

#if (__SUNPRO_CC <= 0x510)
inline double pow(double d, int i); 

#endif

inline double abs(const double x);
inline double pow(int d, int e);

#endif



#ifdef MWCC
inline double abs(const double x);
inline double pow(double d, int i); 
inline double pow(int d, int i);
#endif

#if defined(_MSC_VER)
#if (_MSC_VER<1300)
inline double abs(const double x);
inline double pow(double , int);
inline double pow(int , int);
#else
#define EXPLICIT_INT_ARGS




#endif
inline double asinh (double);
inline double acosh (double);
inline double atanh (double);
inline double rint (double);

#endif

#if defined(__MINGW32__) && (__GNUC__ < 4) 
// *** inline double pow(double , int);
inline double pow(int , int);
inline double asinh (double);
inline double acosh (double);
inline double atanh (double);
inline double rint (double);
#endif

#ifdef EXPLICIT_INT_ARGS
inline double sqrt(int d); 
inline double sin(int d); 
#endif



// Sun CC wanted  these wrappers for C/C++ linkage compatibility
// in some version before Solaris 8
// These wrapper functions are used everywhere in  d_light_double.icc etc.
inline double sqrt_wrap_double(double x) ;
inline double exp_wrap_double(double x) ;
inline double exp_wrap_double(double x) ;
inline double log_wrap_double(double x) ;
inline double sin_wrap_double(double x);
inline double cos_wrap_double(double x);
inline double tan_wrap_double(double x) ;
inline double asin_wrap_double(double x) ;
inline double acos_wrap_double(double x) ;
inline double atan_wrap_double(double x);
inline double sinh_wrap_double(double x) ;
inline double cosh_wrap_double(double x) ;
inline double tanh_wrap_double(double x) ;
inline double asinh_wrap_double(double x);
inline double acosh_wrap_double(double x) ;
inline double atanh_wrap_double(double x) ;

inline lm_complex sqrt_wrap_lm_complex(lm_complex x) ;
inline lm_complex exp_wrap_lm_complex(lm_complex x) ;
inline lm_complex log_wrap_lm_complex(lm_complex x) ;
inline lm_complex sin_wrap_lm_complex(lm_complex x);
inline lm_complex cos_wrap_lm_complex(lm_complex x);
inline lm_complex tan_wrap_lm_complex(lm_complex x) ;
inline lm_complex asin_wrap_lm_complex(lm_complex x) ;
inline lm_complex acos_wrap_lm_complex(lm_complex x) ;
inline lm_complex atan_wrap_lm_complex(lm_complex x);
inline lm_complex sinh_wrap_lm_complex(lm_complex x) ;
inline lm_complex cosh_wrap_lm_complex(lm_complex x) ;
inline lm_complex tanh_wrap_lm_complex(lm_complex x) ;
inline lm_complex asinh_wrap_lm_complex(lm_complex x);
inline lm_complex acosh_wrap_lm_complex(lm_complex x) ;
inline lm_complex atanh_wrap_lm_complex(lm_complex x) ;

inline int irint (double x);
inline int ifloor (double x);
inline int iceil (double x) ;

inline lm_complex irint (lm_complex x);
inline lm_complex ifloor (lm_complex x);
inline lm_complex iceil (lm_complex x) ;

/* ********************** END OF PORTABILITY ISSUES ********* */

#ifdef LM_33
typedef double33  doubleMat33;
#endif

#define SetArray3(v,x,y,z) v.Set(x,y,z)

#define Det(v) v.det()

#define AbsVector(v) v.length()


#ifdef  LM_N
inline lightNdouble make_lightN (int size,double v1=0,
                         double v2=0,double v3=0,double v4=0,
                         double v5=0,double v6=0,double v7=0,
                         double v8=0,double v9=0,double v10=0);



inline lightNint make_lightN (int size,int v1=0,
                         int v2=0,int v3=0,int v4=0,
                         int v5=0,int v6=0,int v7=0,
                         int v8=0,int v9=0,int v10=0);

inline lightNlm_complex make_lightN (int size,lm_complex v1=0,
                         lm_complex v2=0,lm_complex v3=0,lm_complex v4=0,
                         lm_complex v5=0,lm_complex v6=0,lm_complex v7=0,
                         lm_complex v8=0,lm_complex v9=0,lm_complex v10=0);

#endif
#ifdef  LM_NN
inline lightNNint make_lightN (int size,lightNint v1=0,
                         lightNint v2=0,lightNint v3=0,lightNint v4=0,
                         lightNint v5=0,lightNint v6=0,lightNint v7=0,
                         lightNint v8=0,lightNint v9=0,lightNint v10=0);

inline lightNNdouble make_lightN (int size,lightNdouble v1=0,
                         lightNdouble v2=0,lightNdouble v3=0,lightNdouble v4=0,
                         lightNdouble v5=0,lightNdouble v6=0,lightNdouble v7=0,
                         lightNdouble v8=0,lightNdouble v9=0,lightNdouble v10=0);



inline lightNNlm_complex make_lightN (int size,lightNlm_complex v1=0,
                         lightNlm_complex v2=0,lightNlm_complex v3=0,lightNlm_complex v4=0,
                         lightNlm_complex v5=0,lightNlm_complex v6=0,lightNlm_complex v7=0,
                         lightNlm_complex v8=0,lightNlm_complex v9=0,lightNlm_complex v10=0);




#endif

#ifdef  LM_NNN
inline lightNNNint make_lightN (int size,lightNNint v1= lightmat_dummy,
                         lightNNint v2= lightmat_dummy,lightNNint v3= lightmat_dummy,lightNNint v4= lightmat_dummy,
                         lightNNint v5= lightmat_dummy,lightNNint v6= lightmat_dummy,lightNNint v7= lightmat_dummy,
                         lightNNint v8= lightmat_dummy,lightNNint v9= lightmat_dummy,lightNNint v10= lightmat_dummy);

inline lightNNNdouble make_lightN (int size,lightNNdouble v1= lightmat_dummy,
                         lightNNdouble v2= lightmat_dummy,lightNNdouble v3= lightmat_dummy,lightNNdouble v4= lightmat_dummy,
                         lightNNdouble v5= lightmat_dummy,lightNNdouble v6= lightmat_dummy,lightNNdouble v7= lightmat_dummy,
                         lightNNdouble v8= lightmat_dummy,lightNNdouble v9= lightmat_dummy,lightNNdouble v10= lightmat_dummy);

inline lightNNNlm_complex make_lightN (int size,lightNNlm_complex v1= lightmat_dummy,
                         lightNNlm_complex v2= lightmat_dummy,lightNNlm_complex v3= lightmat_dummy,lightNNlm_complex v4= lightmat_dummy,
                         lightNNlm_complex v5= lightmat_dummy,lightNNlm_complex v6= lightmat_dummy,lightNNlm_complex v7= lightmat_dummy,
                         lightNNlm_complex v8= lightmat_dummy,lightNNlm_complex v9= lightmat_dummy,lightNNlm_complex v10= lightmat_dummy);


#endif

#ifdef  LM_NNNN
inline lightNNNNint make_lightN (int size,lightNNNint v1= lightmat_dummy,
                         lightNNNint v2= lightmat_dummy,lightNNNint v3= lightmat_dummy,lightNNNint v4= lightmat_dummy,
                         lightNNNint v5= lightmat_dummy,lightNNNint v6= lightmat_dummy,lightNNNint v7= lightmat_dummy,
                         lightNNNint v8= lightmat_dummy,lightNNNint v9= lightmat_dummy,lightNNNint v10= lightmat_dummy);

inline lightNNNNdouble make_lightN (int size,lightNNNdouble v1= lightmat_dummy,
                         lightNNNdouble v2= lightmat_dummy,lightNNNdouble v3= lightmat_dummy,lightNNNdouble v4= lightmat_dummy,
                         lightNNNdouble v5= lightmat_dummy,lightNNNdouble v6= lightmat_dummy,lightNNNdouble v7= lightmat_dummy,
                         lightNNNdouble v8= lightmat_dummy,lightNNNdouble v9= lightmat_dummy,lightNNNdouble v10= lightmat_dummy);

inline lightNNNNlm_complex make_lightN (int size,lightNNNlm_complex v1= lightmat_dummy,
                         lightNNNlm_complex v2= lightmat_dummy,lightNNNlm_complex v3= lightmat_dummy,lightNNNlm_complex v4= lightmat_dummy,
                         lightNNNlm_complex v5= lightmat_dummy,lightNNNlm_complex v6= lightmat_dummy,lightNNNlm_complex v7= lightmat_dummy,
                         lightNNNlm_complex v8= lightmat_dummy,lightNNNlm_complex v9= lightmat_dummy,lightNNNlm_complex v10= lightmat_dummy);

#endif




#ifdef LM_33
void RotSym(// Input parameters 
            const double axx,
            const double axy,
            const double axz,
            const double ayy,
            const double ayz,
            const double azz,
            const doubleMat33& A,
            // Output parameters 
            double& bxx,
            double& bxy,
            double& bxz,
            double& byy,
            double& byz,
            double& bzz);


 doubleVec3 Max(const doubleVec3& a, const doubleVec3& b);
 doubleVec3 Min(const doubleVec3& a, const doubleVec3& b);

RWCString ToStr(const doubleVec3& val) ;
RWCString ToStr(const doubleMat33& M) ;

// These removed in version 11 Jan 2002 due to conflict
// with 2.95.2 bug
// extern const doubleVec3 zeroVec3;
// A zero vector 

// extern const doubleMat33 zeroMat33;
// A zero matrix 

// extern const doubleMat33 IMat33;
// A identity matrix 


///////////////////////////////////////////////////////////////////////
//
// Definitions of functions used for coordinate transformations etc.
//


// Create a symmetric matrix 
inline doubleMat33 Asym(const double axx, 
                        const double axy, 
                        const double axz,
                        const double ayy, 
                        const double ayz, 
                        const double azz); 

// Create a transformation matrix from base system to local system.
// We use 1,2,3 rotation scheme.
inline doubleMat33 A123(const doubleVec3& phi); 

// Create an inverse transformation matrix from base system to local system.
// We use 1,2,3 rotation scheme.
inline doubleMat33 invA123(const doubleVec3& phi); 


// Create a transformation matrix from base system to local system.
// We use 3,2,1 rotation scheme.
inline doubleMat33 A321(const doubleVec3& phi); 

// Create a transformation matrix from base system to local system.
// We use only rotation around the axis number 3 (z-axis).
inline doubleMat33 A3(const double phi_3); 

// Create a transformation matrix from base system to local system.
// We use only rotation around the axis number 2 (y-axis).
inline doubleMat33 A2(const double phi_2); 

// Create a transformation matrix from base system to local system.
// We use only rotation around the axis number 1 (x-axis).
inline doubleMat33 A1(const double phi_1); 


inline doubleMat33 U123(const doubleVec3& phi); 

inline doubleMat33 U321(const doubleVec3& phi); 

inline doubleMat33 H321(const doubleVec3& phi); 

inline doubleMat33 H123(const doubleVec3& phi); 

inline doubleMat33 invU123(const doubleVec3& phi); 

inline doubleMat33 invH321(const doubleVec3& phi); 

inline doubleMat33 invU321(const doubleVec3& phi); 

inline doubleMat33 invH123(const doubleVec3& phi); 

inline doubleMat33 vU123(const doubleVec3& phi,
                         const doubleVec3& vphi); 

inline doubleMat33 vH123(const doubleVec3& phi,
                         const doubleVec3& vphi); 

inline doubleMat33 vH321(const doubleVec3& phi,
                         const doubleVec3& vphi); 

inline doubleMat33 vU321(const doubleVec3& phi,
                         const doubleVec3& vphi); 

inline doubleVec3 Omega_U123(const doubleVec3& phi,
                             const doubleVec3& vphi);

inline doubleVec3 Omega_H123(const doubleVec3& phi,
                             const doubleVec3& vphi);

inline doubleVec3 vOmega_U123(const doubleVec3& phi,
                              const doubleVec3& vphi,
                              const doubleVec3& aphi);

inline doubleVec3 vOmega_H123(const doubleVec3& phi,
                              const doubleVec3& vphi,
                              const doubleVec3& aphi);

inline doubleVec3 Omega_H321(const doubleVec3& phi,
                             const doubleVec3& vphi);

inline doubleVec3 Omega_U321(const doubleVec3& phi,
                             const doubleVec3& vphi);

inline doubleVec3 vOmega_U321(const doubleVec3& phi,
                              const doubleVec3& vphi,
                              const doubleVec3& aphi);

inline doubleVec3 vOmega_H321(const doubleVec3& phi,
                              const doubleVec3& vphi,
                              const doubleVec3& aphi);

inline doubleMat33 Acyl(const doubleVec3& RC);


inline doubleMat33 Acyl(const double& RC_2);
  

inline doubleVec3 RC2R(const doubleVec3& RC) ;


inline doubleVec3 vRC2vR(const doubleVec3& RC,
                         const doubleVec3& vRC) ;


inline doubleVec3 R2RC(const doubleVec3& R) ;


inline doubleVec3 aR2aRC(const doubleVec3& aR,
                         const doubleVec3& RC, 
                         const doubleVec3& vRC) ;

inline const int sizeOf(const doubleVec3 &);
inline const int sizeOf(const doubleMat33 &);

#endif
