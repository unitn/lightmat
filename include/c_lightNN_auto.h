inline lightNNlm_complex& Set (const int i0, const int i1, const lm_complex val);
inline lightNNlm_complex& Set (const R r0, const int i1, const lightNlm_complex& arr);
inline lightNNlm_complex& Set (const R r0, const int i1, const light3lm_complex& arr);
inline lightNNlm_complex& Set (const R r0, const int i1, const light4lm_complex& arr);
inline lightNNlm_complex& Set (const R r0, const int i1, const lm_complex val);
inline lightNNlm_complex& Set (const int i0, const R r1, const lightNlm_complex& arr);
inline lightNNlm_complex& Set (const int i0, const R r1, const light3lm_complex& arr);
inline lightNNlm_complex& Set (const int i0, const R r1, const light4lm_complex& arr);
inline lightNNlm_complex& Set (const int i0, const R r1, const lm_complex val);
inline lightNNlm_complex& Set (const R r0, const R r1, const lightNNlm_complex& arr);
inline lightNNlm_complex& Set (const R r0, const R r1, const light33lm_complex& arr);
inline lightNNlm_complex& Set (const R r0, const R r1, const light44lm_complex& arr);
inline lightNNlm_complex& Set (const R r0, const R r1, const lm_complex val);
inline lightNlm_complex operator() (const R r0, const int i1) const;
inline lightNlm_complex operator() (const int i0, const R r1) const;
inline lightNNlm_complex operator() (const R r0, const R r1) const;

#ifdef IN_LIGHTNNdouble_C_H
inline lightNNdouble(const double e, const lightNNint &s1, const lightmat_atan2_enum);
inline lightNNdouble(const double e, const lightNNdouble &s1, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const double e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const double e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const lightNNint &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const lightNNdouble &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const lightNNint &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const lightNNdouble &s2, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNint &s1, const int e, const lightmat_atan2_enum);
inline lightNNdouble(const lightNNdouble &s1, const int e, const lightmat_atan2_enum);
inline lightNNdouble(const int e, const lightNNint &s1, const lightmat_atan2_enum);
inline lightNNdouble(const int e, const lightNNdouble &s1, const lightmat_atan2_enum);

#endif
