inline lightNlm_complex& Set (const int i0, const lm_complex val);
inline lightNlm_complex& Set (const R r0, const lightNlm_complex& arr);
inline lightNlm_complex& Set (const R r0, const light3lm_complex& arr);
inline lightNlm_complex& Set (const R r0, const light4lm_complex& arr);
inline lightNlm_complex& Set (const R r0, const lm_complex val);
inline lightNlm_complex operator() (const R r0) const;

#ifdef IN_LIGHTNdouble_C_H
inline lightNdouble(const double e, const lightNint &s1, const lightmat_atan2_enum);
inline lightNdouble(const double e, const lightNdouble &s1, const lightmat_atan2_enum);
inline lightNdouble(const lightNint &s1, const double e, const lightmat_atan2_enum);
inline lightNdouble(const lightNdouble &s1, const double e, const lightmat_atan2_enum);
inline lightNdouble(const lightNint &s1, const lightNint &s2, const lightmat_atan2_enum);
inline lightNdouble(const lightNint &s1, const lightNdouble &s2, const lightmat_atan2_enum);
inline lightNdouble(const lightNdouble &s1, const lightNint &s2, const lightmat_atan2_enum);
inline lightNdouble(const lightNdouble &s1, const lightNdouble &s2, const lightmat_atan2_enum);
inline lightNdouble(const lightNint &s1, const int e, const lightmat_atan2_enum);
inline lightNdouble(const lightNdouble &s1, const int e, const lightmat_atan2_enum);
inline lightNdouble(const int e, const lightNint &s1, const lightmat_atan2_enum);
inline lightNdouble(const int e, const lightNdouble &s1, const lightmat_atan2_enum);

#endif
