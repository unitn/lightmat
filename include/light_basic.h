//           -*- c++ -*-

#define IN_BASIC<T>_H
//
#ifdef COMPLEX_TOOLS
template<class T>
inline double LIGHTABS(const lm_complex m);
#else
template<class T>
inline T LIGHTABS(const T m);
#endif

//
// Basic routines with simple operations
//

template<class T>
inline void light_assign(const int n, const T *a, T *b);

template<class T>
inline void light_assign(const int i0, const int i1, const T *a, T *b);

template<class T>
inline void light_plus(const int n, const T *a, const T *b, T *c);

template<class T>
inline void light_minus(const int n, const T *a, const T *b, T *c);

template<class T>
inline void light_mult(const int n, const T *a, const T *b, T *c);

template<class T>
inline void light_divide(const int n, const T *a, const T *b, T *c);

template<class T>
inline void light_assign(const int n, const T a, T *b);

template<class T>
inline void light_assign(const int i0, const int i1, const T a, T *b);

template<class T>
inline void light_plus(const int n, const T a, const T *b, T *c);

#ifdef IN_BASICdouble_H
inline void light_plus(const int n, const double a, const int *b, double *c);
inline void light_plus(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_H
inline void light_plus(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_plus(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_plus(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_plus(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif

template<class T>
inline void light_minus(const int n, const T a, const T *b, T *c);

#ifdef IN_BASICdouble_H
inline void light_minus(const int n, const double a, const int *b, double *c);
inline void light_minus(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_H
inline void light_minus(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_minus(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_minus(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_minus(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif

template<class T>
inline void light_mult(const int n, const T a, const T *b, T *c);

#ifdef IN_BASICdouble_H
inline void light_mult(const int n, const double a, const int *b, double *c);
inline void light_mult(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_H
inline void light_mult(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_mult(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_mult(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_mult(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif

template<class T>
inline void light_divide(const int n, const T a, const T *b, T *c);

#ifdef IN_BASICdouble_H
inline void light_divide(const int n, const double a, const int *b, double *c);
inline void light_divide(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_H
inline void light_divide(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_divide(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_divide(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_divide(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif

template<class T>
inline void light_plus_same(const int n, T *a, const T *b);

template<class T>
inline void light_minus_same(const int n, T *a, const T *b);

template<class T>
inline void light_mult_same(const int n, T *a, const T *b);

template<class T>
inline void light_plus_same(const int n, T *a, const T b);

template<class T>
inline void light_mult_same(const int n, T *a, const T b);

template<class T>
inline T light_pow(T a, T b);


#ifdef IN_BASIC<double>_H
inline double light_atan2(double a, double b);
#endif

#ifdef IN_BASIC<lm_complex>_H
inline lm_complex light_atan2(lm_complex a, lm_complex b);
#endif

//
// Basic routines for vector and matrix calculation
//

// Dot product
//
// n : length of vectors
// x : vector 1
// y : vector 2
//
template<class T>
 inline T light_dot(const int n, const T *x, const T *y);

// Dot product
//
// n  : length of vectors
// x  : vector 1
// ix : step between elements in x
// y  : vector 2
//
template<class T>
 inline T light_dot(const int n,const T *x,
		    const int ix,const T *y);

// Matrix-vector product
//
// m : rows of matrix
// n : columns of matrix and no. elements in vector
// a : matrix
// x : vector
// y : result vector (length m)
template<class T>
inline void light_gemv(const int m, const int n,
		       const T *a, const T *x, T *y);

// Vector-matrix product
//
// m : rows of matrix and no. elements in vector
// n : columns of matrix
// x : vector
// a : matrix
// y : result vector (length n)
template<class T>
inline void light_gevm(const int m, const int n,
		       const T *x, const T *a, T *y);

// Vector-matrix product
//
// m  : rows of matrix and no. elements in vector
// n  : columns of matrix
// x  : vector
// a  : matrix
// y  : result vector (length n)
// iy : step between elements in y
template<class T>
inline void light_gevm(const int m, const int n,
		       const T *x, const T *a, T *y,
		const int iy);

// Matrix-Matrix product
//
// m : rows of matrix a and c
// n : columns of matrix b and c
// k : columns of matrix a and rows of matrix b
// a : matrix 1
// b : matrix 2
// c : result matrix
template<class T>
inline void light_gemm(const int m, const int n,
		       const int k, const T *a,
		       const T *b, T *c);

// Tensor3-Vector product
//
// m : size1 of a and rows of c
// n : size2 of a and columns of c
// k : size3 of a and no. elements in b
// a : tensor3
// b : vector
// c : result matrix
template<class T>
inline void light_ge3v(const int m, const int n,
		       const int k, const T *a,
		       const T *b, T *c);

// Vector-Tensor3 product
//
// m : size2 of b and rows of c
// n : size3 of a and columns of c
// k : no. elements in a and size1 of b
// a : vector
// b : tensor3
// c : result matrix
template<class T>
inline void light_gev3(const int m, const int n,
		       const int k, const T *a,
		       const T *b, T *c);

// Tensor3-Matrix product
//
// m : size1 of a and c
// n : size2 of a and c
// o : columns of b and size3 of c
// k : size3 of a and rows of b
// a : tensor3
// b : matrix
// c : result tensor3
template<class T>
inline void light_ge3m(const int m, const int n,
		       const int o, const int k,
		       const T *a, const T *b, T *c);

// Matrix-Tensor3 product
//
// m : rows of a and size1 of c
// n : size2 of b and c
// o : size3 of b and c
// k : columns of a and size1 of b
// a : matrix
// b : tensor3
// c : result tensor3
template<class T>
inline void light_gem3(const int m, const int n,
		       const int o, const int k,
		       const T *a, const T *b, T *c);

// Tensor3-Tensor3 product
//
// m : size1 of a c
// n : size2 of a and c
// o : size2 of b and size3 of c
// p : size3 of b and size4 of c
// k : size3 of a and size1 of b
// a : tensor3
// b : tensor3
// c : result tensor4
template<class T>
inline void light_ge33(const int m, const int n,
		       const int o, const int p,
		       const int k, const T *a,
		       const T *b, T *c);

// Tensor4-Vector product
//
// m : size1 of a and c
// n : size2 of a and c
// o : size3 of a and c
// k : size4 of a and no. elements in b
// a : tensor4
// b : vector
// c : result tensor3
template<class T>
inline void light_ge4v(const int m, const int n,
		       const int o, const int k,
		       const T *a, const T *b, T *c);

// Vector-Tensor4 product
//
// m : size2 of b and size1 of c
// n : size3 of b and size2 of c
// o : size4 of b and size3 of c
// k : no. elements in a and size1 of b
// a : vector
// b : tensor4
// c : result tensor3
template<class T>
inline void light_gev4(const int m, const int n,
		       const int o, const int k,
		       const T *a, const T *b, T *c);

// Tensor4-Matrix product
//
// m : size1 of a and c
// n : size2 of a and c
// o : size3 of a and c
// p : columns of b and size4 of c
// k : size4 of a and rows of b
// a : tensor4
// b : matrix
// c : result tensor4
template<class T>
inline void light_ge4m(const int m, const int n,
		       const int o, const int p,
		       const int k, const T *a,
		       const T *b, T *c);

// Matrix-Tensor4 product
//
// m : rows of a and size1 of c
// n : size2 of b and c
// o : size3 of b and c
// p : size4 of b and c
// k : columns of a and size1 of b
// a : matrix
// b : tensor4
// c : result tensor4
template<class T>
inline void light_gem4(const int m, const int n,
		       const int o, const int p,
		       const int k, const T *a,
		       const T *b, T *c);


template<class T>
inline lightN<T> light_join (const lightN<T> arr1, const lightN<T> arr2);

template<class T>
inline lightNN<T> light_join (const lightNN<T> arr1, const lightNN<T> arr2);

template<class T>
inline lightNNN<T> light_join (const lightNNN<T> arr1, const lightNNN<T> arr2);

template<class T>
inline lightNNNN<T> light_join (const lightNNNN<T> arr1, const lightNNNN<T> arr2);

template<class T>
inline lightN<T> light_append (const lightN<T> arr, const T el);

template<class T>
inline lightNN<T> light_append (const lightNN<T> arr, const lightN<T> el);

template<class T>
inline lightNNN<T> light_append (const lightNNN<T> arr, const lightNN<T> el);

template<class T>
inline lightNNNN<T> light_append (const lightNNNN<T> arr, const lightNNN<T> el);

template<class T>
inline lightN<T> light_prepend (const lightN<T> arr, const T el);

template<class T>
inline lightNN<T> light_prepend (const lightNN<T> arr, const lightN<T> el);

template<class T>
inline lightNNN<T> light_prepend (const lightNNN<T> arr, const lightNN<T> el);

template<class T>
inline lightNNNN<T> light_prepend (const lightNNNN<T> arr, const lightNNN<T> el);

template<class T>
inline lightN<T> light_drop (const lightN<T> arr, const R r);

template<class T>
inline lightNN<T> light_drop (const lightNN<T> arr, const R r);

template<class T>
inline lightNNN<T> light_drop (const lightNNN<T> arr, const R r);

template<class T>
inline lightNNNN<T> light_drop (const lightNNNN<T> arr, const R r);


//
// Join for N
//

template<class T>
inline lightN<T> Join (const lightN<T> arr1, const lightN<T> arr2);

template<class T>
inline lightN<T> Join (const light3<T> arr1, const lightN<T> arr2);

template<class T>
inline lightN<T> Join (const light4<T> arr1, const lightN<T> arr2);

template<class T>
inline lightN<T> Join (const light3<T> arr1, const light3<T> arr2);

template<class T>
inline lightN<T> Join (const light4<T> arr1, const light3<T> arr2);

template<class T>
inline lightN<T> Join (const lightN<T> arr1, const light3<T> arr2);

template<class T>
inline lightN<T> Join (const light3<T> arr1, const light4<T> arr2);

template<class T>
inline lightN<T> Join (const light4<T> arr1, const light4<T> arr2);

template<class T>
inline lightN<T> Join (const lightN<T> arr1, const light4<T> arr2);


//
// Join for NN
//

template<class T>
inline lightNN<T> Join (const lightNN<T> arr1, const lightNN<T> arr2);

template<class T>
inline lightNN<T> Join (const light33<T> arr1, const lightNN<T> arr2);

template<class T>
inline lightNN<T> Join (const light44<T> arr1, const lightNN<T> arr2);

template<class T>
inline lightNN<T> Join (const light33<T> arr1, const light33<T> arr2);

template<class T>
inline lightNN<T> Join (const light44<T> arr1, const light33<T> arr2);

template<class T>
inline lightNN<T> Join (const lightNN<T> arr1, const light33<T> arr2);

template<class T>
inline lightNN<T> Join (const light33<T> arr1, const light44<T> arr2);

template<class T>
inline lightNN<T> Join (const light44<T> arr1, const light44<T> arr2);

template<class T>
inline lightNN<T> Join (const lightNN<T> arr1, const light44<T> arr2);

//
// Join for NNN
//

template<class T>
inline lightNNN<T> Join (const lightNNN<T> arr1, const lightNNN<T> arr2);

//
// Join for NNNN
//

template<class T>
inline lightNNNN<T> Join (const lightNNNN<T> arr1, const lightNNNN<T> arr2);


//
// Append
//

template<class T>
inline lightN<T> Append (const light3<T> arr, const T el);

template<class T>
inline lightN<T> Append (const light4<T> arr, const T el);

template<class T>
inline lightN<T> Append (const lightN<T> arr, const T el);

template<class T>
inline lightNN<T> Append (const light33<T> arr, const light3<T> el);

template<class T>
inline lightNN<T> Append (const light44<T> arr, const light4<T> el);

template<class T>
inline lightNN<T> Append (const lightNN<T> arr, const lightN<T> el);

template<class T>
inline lightNNN<T> Append (const lightNNN<T> arr, const lightNN<T> el);

template<class T>
inline lightNNNN<T> Append (const lightNNNN<T> arr, const lightNNN<T> el);

//
// Prepend
//

template<class T>
inline lightN<T> Prepend (const light3<T> arr, const T el);

template<class T>
inline lightN<T> Prepend (const light4<T> arr, const T el);

template<class T>
inline lightN<T> Prepend (const lightN<T> arr, const T el);

template<class T>
inline lightNN<T> Prepend (const light33<T> arr, const light3<T> el);

template<class T>
inline lightNN<T> Prepend (const light44<T> arr, const light4<T> el);

template<class T>
inline lightNN<T> Prepend (const lightNN<T> arr, const lightN<T> el);

template<class T>
inline lightNNN<T> Prepend (const lightNNN<T> arr, const lightNN<T> el);

template<class T>
inline lightNNNN<T> Prepend (const lightNNNN<T> arr, const lightNNN<T> el);

//
// Drop
//

template<class T>
inline lightN<T> Drop (const lightN<T> arr, const R r);

template<class T>
inline lightN<T> Drop (const light3<T> arr, const R r);

template<class T>
inline lightN<T> Drop (const light4<T> arr, const R r);

template<class T>
inline lightNN<T> Drop (const lightNN<T> arr, const R r);

template<class T>
inline lightNN<T> Drop (const light33<T> arr, const R r);

template<class T>
inline lightNN<T> Drop (const light44<T> arr, const R r);

template<class T>
inline lightNNN<T> Drop (const lightNNN<T> arr, const R r);

template<class T>
inline lightNNNN<T> Drop (const lightNNNN<T> arr, const R r);


#undef IN_BASIC<T>_H
