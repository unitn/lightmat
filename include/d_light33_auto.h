inline light33double& Set (const int i0, const int i1, const double val);
inline light33double& Set (const R r0, const int i1, const lightNdouble& arr);
inline light33double& Set (const R r0, const int i1, const light3double& arr);
inline light33double& Set (const R r0, const int i1, const light4double& arr);
inline light33double& Set (const R r0, const int i1, const double val);
inline light33double& Set (const int i0, const R r1, const lightNdouble& arr);
inline light33double& Set (const int i0, const R r1, const light3double& arr);
inline light33double& Set (const int i0, const R r1, const light4double& arr);
inline light33double& Set (const int i0, const R r1, const double val);
inline light33double& Set (const R r0, const R r1, const lightNNdouble& arr);
inline light33double& Set (const R r0, const R r1, const light33double& arr);
inline light33double& Set (const R r0, const R r1, const light44double& arr);
inline light33double& Set (const R r0, const R r1, const double val);
inline lightNdouble operator() (const R r0, const int i1) const;
inline lightNdouble operator() (const int i0, const R r1) const;
inline lightNNdouble operator() (const R r0, const R r1) const;
friend inline light33double atan2 (const double e, const light33int &s1);
friend inline light33double atan2 (const double e, const light33double &s1);
friend inline light33double atan2 (const light33int &s1, const double e);
friend inline light33double atan2 (const light33double &s1, const double e);
friend inline light33double atan2 (const light33int &s1, const light33int &s2);
friend inline light33double atan2 (const light33int &s1, const light33double &s2);
friend inline light33double atan2 (const light33double &s1, const light33int &s2);
friend inline light33double atan2 (const light33double &s1, const light33double &s2);
friend inline light33double atan2 (const light33int &s1, const int e);
friend inline light33double atan2 (const light33double &s1, const int e);
friend inline light33double atan2 (const int e, const light33int &s1);
friend inline light33double atan2 (const int e, const light33double &s1);

#ifdef IN_LIGHT33double_H
inline light33double(const double e, const light33int &s1, const lightmat_atan2_enum);
inline light33double(const double e, const light33double &s1, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const double e, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const double e, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const light33int &s2, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const light33double &s2, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const light33int &s2, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const light33double &s2, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const int e, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const int e, const lightmat_atan2_enum);
inline light33double(const int e, const light33int &s1, const lightmat_atan2_enum);
inline light33double(const int e, const light33double &s1, const lightmat_atan2_enum);

#endif
