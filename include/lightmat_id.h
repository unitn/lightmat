//           -*- c++ -*-

#ifndef LIGHT_H
#define LIGHT_H
//
// (Un)comment the #defines according to your needs
//


//
// Define LIGHTMAT_TEMPLATES to use template syntax and template compilation
//
//#define  LIGHTMAT_TEMPLATES

//
// Define LIGHTMAT_LIMITS_CHECKING if limit checking should be enabled
//
#ifndef NO_RANGE_CHECKING
#define LIGHTMAT_LIMITS_CHECKING
#endif
//
// Define LIGHTMAT_INIT_ZERO if all elements should be initialized to zero
//
//#define LIGHTMAT_INIT_ZERO

//
// Define LIGHTMAT_DONT_USE_BLAS if BLAS should not be used
//
#define LIGHTMAT_DONT_USE_BLAS

//
// Define LIGHTMAT_OUTPUT_FUNCS if the string and output functions that use
// RWCString should be used.
//
#define LIGHTMAT_OUTPUT_FUNCS



#ifndef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE 1
#define _CRT_NONSTDC_NO_DEPRECATE 1
// This is needed for compiling without warning for functions
// which considered suspicious by Visual C++ 2005
// sprintf(), strncopy(), etc ...
#endif

// ***************************************************************************
// Things between the ***-lines should normally not be changed
//

// Some necessary include-files
#ifndef macintosh
#include <stdlib.h>
#endif


#if defined(OLDHEADER) || defined(macintosh) || (__GNUC__ && (__GNUC__ < 4))
// Old headers were removed in GNUC 4.3. We cannot not rely on __MINGW32__ or MINGW macro anymore.
#include <iostream.h>
#include <fstream.h>
#else
#include <iostream>
#include <fstream>
#endif


//*  #if ( _MSC_VER < 1300) || defined(macintosh)
//*#include <iostream.h>
//*#include <stdio.h>      // For sprintf only
//*#else


//*#endif


#include <math.h>
#include "mathdefs.h"

// This is because mathlink.h defines P; it is needed on the *tm.c files
// but nowhere else 

#ifdef P

// Was #undefine P
#undef P
#endif


#ifdef ANSI_STRING
#include <string>
#else
#include "rw/cstring.h"
#endif

#if defined(macintosh) || defined(MWCC)
// Probably necessary to access standard math functions...investigate later !!
using namespace std;
#endif

// <string> is the ANSI C++ standard string class
// rw/cstring.h should be defined here because there are "inline" directives.
// They should stay "inline" always.
// If we use RogueWave package for strings then __RWCSTRING_H__ will
// be defined here. Typically for the Beast project.
// Otherwise we defined a very simple RWString class and never use tostr.h
// 



#if defined(_MSC_VER)

 #if  (_MSC_VER<1300)
   #define MC_STDCALL _stdcall
   // MSC starting before 13.00 should  use _stdcall here
   // for linking with Digital 6.0-compiled Fortran code.
   
 #else
   #define MC_STDCALL 
   // MSC starting from 13.00 should not use _stdcall here
   // for linking with IFORT 8.0-compiled Fortran code.
 #endif

#else
#define MC_STDCALL 
#endif
// This is needed for calling Fortran external functions from 
// generated code.
// Defined first in MathCode 1.2, redefined in MathCode 1.2.2

void lighterror(const char msgstr[] ,const char filenam[]=0, int linenum=0);


#ifdef LIGHTMAT_LIMITS_CHECKING
# define limiterror(p) { if(p) lighterror("Limit check failed at " #p, __FILE__,__LINE__); }
#else
# define limiterror(p)
#endif

// Other nice defines
//#define LIGHTABS(x) (((x)<0)?(-(x)):(x))

#if defined(macintosh)
#define abs(x) abs(x)
#endif


// Dot for compatibility with old lightmat
// #define Dot(x,y) ((x)*(y))

// The following enums is used as arguments to constructors. They don't
// carry any real data, they are only used to tell the constructors apart.
//
enum lightmat_dont_zero_enum { lightmat_dont_zero };
enum lightmat_plus_enum { lightmat_plus };
enum lightmat_minus_enum { lightmat_minus };
enum lightmat_mult_enum { lightmat_mult };
enum lightmat_div_enum { lightmat_div };
enum lightmat_pow_enum { lightmat_pow };
enum lightmat_apply_enum { lightmat_apply };
enum lightmat_outer_enum { lightmat_outer };
enum lightmat_trans_enum { lightmat_trans };
enum lightmat_eprod_enum { lightmat_eprod };
enum lightmat_equot_enum { lightmat_equot };
enum lightmat_abs_enum { lightmat_abs };
enum lightmat_dummy_enum {  lightmat_dummy };
enum lightmat_atan2_enum { lightmat_atan2 };
//
// Define the Range class
//

class R {
public:
  int imin;
  int imax;
  int di;

  R (int a, int b) { imin = a; imax = b; di=1;};
  R (int a, int b, int c) { imin = a; imax = b; di = c;};
};

const R All = R(1, -1);

//
// Now it is time to include the actual code
//

#include "light_guard.h"

#ifdef NOT_INLINED
#define inline /* */
#endif
// All files were "inline" is mentioned must be below this define.

#include "lm_complex.h"

// and then all the classes


#ifdef LIGHTMAT_TEMPLATES

//CONV_DOUBLE + CONV_SELF replaced by CONV_INT_2_DOUBLE
//#define CONV_DOUBLE
//#define CONV_SELF
#define CONV_INT_2_DOUBLE
#define CONV_DOUBLE_2_COMPLEX

#ifdef LM_3
#include "light3.h"
#endif

#ifdef LM_4
#include "light4.h"
#endif

#ifdef LM_33
#include "light33.h"
#endif

#ifdef LM_44
#include "light44.h"
#endif

#ifdef LM_N
#include "lightN.h"
#endif

#ifdef LM_N3
#include "lightN3.h"
#endif
 
#ifdef LM_NN
#include "lightNN.h"
#endif

#ifdef LM_N33
#include "lightN33.h"
#endif
 
#ifdef LM_NNN
#include "lightNNN.h"
#endif

#ifdef LM_NNNN
#include "lightNNNN.h"
#endif

//CONV_DOUBLE + CONV_SELF replaced by CONV_INT_2_DOUBLE
//#undef CONV_DOUBLE
//#undef CONV_SELF
#undef CONV_INT_2_DOUBLE
#undef CONV_DOUBLE_2_COMPLEX

// #include "light_int.h"
// #include "light_double.h"
#include "light_basic.h"
#include "light_calc.h"

#ifdef __RWCSTRING_H__
#include "tostr.h"
#endif


typedef light3<double> light3double;
typedef light3<int>  light3int;
typedef light3<lm_complex> light3lm_complex;

typedef light4<double> light4double;
typedef light4<int>  light4int;
typedef light4<lm_complex> light4lm_complex;

typedef lightN<double> lightNdouble;
typedef lightN<int>  lightNint;
typedef lightN<lm_complex> lightNlm_complex;

typedef light33<double> light33double;
typedef light33<int>  light33int;
typedef light33<lm_complex> light33lm_complex;

typedef light44<double> light44double;
typedef light44<int>  light44int;
typedef light44<lm_complex> light44lm_complex;

typedef lightNN<double> lightNNdouble;
typedef lightNN<int>  lightNNint;
typedef lightNN<lm_complex>  lightNNlm_complex;

typedef lightN3<double> lightN3double;
typedef lightN3<int>  lightN3int;
typedef lightN3<lm_complex>  lightN3lm_complex;

typedef lightN33<double> lightN33double;
typedef lightN33<int>  lightN33int;
typedef lightN33<lm_complex>  lightN33lm_complex;
 
typedef lightNNN<double> lightNNNdouble;
typedef lightNNN<int>  lightNNNint;
typedef lightNNN<lm_complex>  lightNNNlm_complex;

typedef lightNNNN<double> lightNNNNdouble;
typedef lightNNNN<int>  lightNNNNint;
typedef lightNNNN<lm_complex>  lightNNNNlm_complex;

#include "transf.h"

// Functions that create strings and output them
#include "light_output.h"

// END OF LIGHTMAT_TEMPLATES


#else



// START OF NOT LIGHTMAT_TEMPLATES

class light3double;
class light4double;
class light33double;
class light44double;
class lightNdouble;
class lightN3double;
class lightNNdouble;
class lightN33double;
class lightNNNdouble;
class lightNNNNdouble;
class light3int;
class light4int;
class light33int;
class light44int;
class lightNint;
class lightN3int;
class lightNNint;
class lightN33int;
class lightNNNint;
class lightNNNNint;
class light3lm_complex;
class light4lm_complex;
class light33lm_complex;
class light44lm_complex;
class lightNlm_complex;
class lightN3lm_complex;
class lightNNlm_complex;
class lightN33lm_complex;
class lightNNNlm_complex;
class lightNNNNlm_complex;

#define CONV_DOUBLE_2_COMPLEX

#ifdef LM_3
#include "d_light3.h"
#endif

#ifdef LM_4
#include "d_light4.h"
#endif

#ifdef LM_33
#include "d_light33.h"
#endif

#ifdef LM_44
#include "d_light44.h"
#endif

#ifdef LM_N
#include "d_lightN.h"
#endif

#ifdef LM_N3
#include "d_lightN3.h"
#endif
 
#ifdef LM_NN
#include "d_lightNN.h"
#endif

#ifdef LM_N33
#include "d_lightN33.h"
#endif
 
#ifdef LM_NNN
#include "d_lightNNN.h"
#endif

#ifdef LM_NNNN
#include "d_lightNNNN.h"
#endif

#include "d_light_int.h"

#include "d_light_double.h"
#include "d_light_double_complex.h"
#include "d_light_basic.h"
#include "d_light_calc.h"

#undef CONV_DOUBLE_2_COMPLEX

//CONV_DOUBLE + CONV_SELF replaced by CONV_INT_2_DOUBLE
//#define CONV_DOUBLE
//#define CONV_SELF
#define CONV_INT_2_DOUBLE
#define CONV_INT_2_COMPLEX

#ifdef LM_3
#include "i_light3.h"
#endif

#ifdef LM_4
#include "i_light4.h"
#endif

#ifdef LM_33
#include "i_light33.h"
#endif

#ifdef LM_44
#include "i_light44.h"
#endif

#ifdef LM_N
#include "i_lightN.h"
#endif

#ifdef LM_N3
#include "i_lightN3.h"
#endif
 
#ifdef LM_NN
#include "i_lightNN.h"
#endif

#ifdef LM_N33
#include "i_lightN33.h"
#endif
 
#ifdef LM_NNN
#include "i_lightNNN.h"
#endif

#ifdef LM_NNNN
#include "i_lightNNNN.h"
#endif

#include "i_light_int.h"
#include "i_light_basic.h"
#include "i_light_calc.h"
//CONV_DOUBLE + CONV_SELF replaced by CONV_INT_2_DOUBLE
//#undef CONV_DOUBLE
//#undef CONV_SELF
#undef CONV_INT_2_DOUBLE
#undef CONV_INT_2_COMPLEX

#define COMPLEX_TOOLS

#ifdef LM_3
#include "c_light3.h"
#endif

#ifdef LM_4
#include "c_light4.h"
#endif

#ifdef LM_33
#include "c_light33.h"
#endif

#ifdef LM_44
#include "c_light44.h"
#endif

#ifdef LM_N
#include "c_lightN.h"
#endif

#ifdef LM_N3
#include "c_lightN3.h"
#endif
 
#ifdef LM_NN
#include "c_lightNN.h"
#endif

#ifdef LM_N33
#include "c_lightN33.h"
#endif
 
#ifdef LM_NNN
#include "c_lightNNN.h"
#endif

#ifdef LM_NNNN
#include "c_lightNNNN.h"
#endif

#include "c_light_int.h"

#define C_LIGHT_DOUBLE
// This is needed in order to remove some declarations from c_light_double.h
// which are otherwise duplicated in d_light_double.h
#include "c_light_complex.h"
#include "c_light_double_complex.h"

#include "c_light_basic.h"
#include "c_light_calc.h"

#undef COMPLEX_TOOLS 


#ifdef __RWCSTRING_H__
#include "tostr.h"
#endif

#include "transf.h"
// Functions that create strings and output them
#include "c_light_output.h"
#include "d_light_output.h"
#include "i_light_output.h"

#endif // END OF NOT LIGHTMAT_TEMPLATES

#ifndef NOT_INLINED
#include "light_icc.h"
#endif /* NOT_INLINED */

#undef  inline
#define inline inline

//
// ***************************************************************************
//
#endif /* LIGHT_H */
