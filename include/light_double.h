//
// routines which use BLAS
//
#ifndef LIGHTMAT_DONT_USE_BLAS
// These functions are overloaded variants of the ones in light_basic.h

<T> light_dot(const int n, const <T> *x, const <T> *y);

<T> light_dot(const int n, const <T> *x, const int ix, const <T> *y);

void light_gemv(const int m, const int n, const <T> *a, const <T> *x,
		<T> *y);

void light_gevm(const int m, const int n, const <T> *x, const <T> *a,
		<T> *y, const int iy = 1);

void light_gemm(const int m, const int n, const int k,
		const <T> *a, const <T> *b, <T> *c);

#endif // ! LIGHTMAT_DONT_USE_BLAS

//
// fractional part
//
inline <T> FractionalPart(const <T> s);
inline lightN<T>  FractionalPart(const lightN<T>&);
inline lightNN<T>  FractionalPart(const lightNN<T>&);
inline lightNNN<T>  FractionalPart(const lightNNN<T>&);
inline lightNNNN<T>  FractionalPart(const lightNNNN<T>&);

#ifndef C_LIGHT_DOUBLE
inline <T> FractionalPart(const int );
inline lightNint  FractionalPart(const lightNint&);
inline lightNNint  FractionalPart(const lightNNint&);
inline lightNNNint  FractionalPart(const lightNNNint&);
inline lightNNNNint  FractionalPart(const lightNNNNint&);
#endif

// IntegerPart
#ifndef C_LIGHT_DOUBLE
lightNint  IntegerPart(const lightNint&);
lightNNint  IntegerPart(const lightNNint&);
lightNNNint  IntegerPart(const lightNNNint&);
lightNNNNint  IntegerPart(const lightNNNNint&);
#endif

inline <T> IntegerPart(const <T> s); 
lightN<T> IntegerPart(const lightN<T>&);
lightNN<T>  IntegerPart(const lightNN<T>&);
lightNNN<T>  IntegerPart(const lightNNN<T>&);
lightNNNN<T>  IntegerPart(const lightNNNN<T>&);

//
// abs
//
template<class T>
lightN<T> abs(const lightN<T>& s);

template<class T>
lightN3<T> abs(const lightN3<T>& s);

template<class T>
lightNN<T> abs(const lightNN<T>& s);

template<class T>
lightNNN<T> abs(const lightNNN<T>& s);

template<class T>
lightNNNN<T> abs(const lightNNNN<T>& s);

// Mod

#ifndef C_LIGHT_DOUBLE
inline int Mod(const int m , const int n);
#endif

inline <T> Mod(const <T> m, const <T> n);
inline <T> Mod(const <T> m, const int n);
inline <T> Mod(const int m, const <T> n);

/*
lightN<T> Mod(const lightNint & i, const lightN<T> & d);
lightN<T> Mod(const lightN<T> & i, const lightNint & d);
lightNN<T> Mod(const lightNNint & i, const lightNN<T> & d);
lightNN<T> Mod(const lightNN<T> & i, const lightNNint & d);
lightNNN<T> Mod(const lightNNN<T> & i, const lightNNNint & d);
lightNNN<T> Mod(const lightNNNint & i, const lightNNN<T> & d);
lightNNNN<T> Mod(const lightNNNN<T> & i, const lightNNNNint & d);
lightNNNN<T> Mod(const lightNNNNint & i, const lightNNNN<T> & d);
*/




