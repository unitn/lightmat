//           -*- c++ -*-

#ifndef LIGHTN_H
#define LIGHTN_H
// <cd> lightNdouble
//
// .SS Functionality
//
// lightN is a template for classes that implement vectors with any
// number of elements. E.g. a vector v with 5 elements of type double
// can be instanciated with:
//
// <code>lightN&lt;double&gt; v(5);</code>
//
// The number of elements in the vector can change during
// execution. It changes its size to whatever is needed when it is
// assigned a new value with an assignment operator.
//
// When a lightN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightN object with four elements to one with five elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//
#define IN_LIGHTNdouble_H

#define LIGHTN_SIZE 10
// The default size.

 class light3;
 class light4;
 class lightN;
 class lightN3;
 class lightN33;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class lightNdouble {
public:

#ifdef IN_LIGHTNdouble_H
  friend class lightNint;
#else
  friend class lightNdouble;
#endif

#ifdef IN_LIGHTNint_H
#define TDtypeH  double
#define TDNtypeH lightNdouble
#else
#define TDtypeH double
#define TDNtypeH lightNdouble
#endif


#ifdef LIGHTMAT_TEMPLATES
  // friend class lightNint;
#endif

  #include "d_lightN_auto.h"

  inline lightNdouble();
  // Default constructor.

  inline lightNdouble(const lightNdouble&);
  // Copy constructor.
  
  inline lightNdouble(const light3double&);
  // Conversion.

  inline lightNdouble(const light4double&);
  // Conversion.

  inline lightNdouble(const int);
  // Construct a lightN of given size.

  inline lightNdouble(const int, const double *);
  // Construct a lightN of given size and initialize elements with values
  // from an array.

  inline lightNdouble(const int, const double);
  // Construct a lightN of given size and initialize all elements
  // with a value (the second argument).

  inline ~lightNdouble();
  // Destructor.


#ifdef CONV_INT_2_DOUBLE
  operator lightNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNlm_complex();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNlm_complex();
  // Convert to complex.
#endif


  lightNdouble& operator=(const lightNdouble&);
  // Assignment.

  lightNdouble& operator=(const light3double&);
  // Assignment, change size to 3 elements.

  lightNdouble& operator=(const light4double&);
  // Assignment, change size to 4 elements.

  lightNdouble& operator=(const double);
  // Assign one value to all elements.

  //
  // operator()
  //

  double operator() (const int x) const {
    limiterror((x<1) || (x>size1));
    return elem[x-1];
  };
  // Get the value of one element.

  double& operator()(const int x) {
    limiterror((x<1) || (x>size1));
    return elem[x-1];
  };
  // Get/Set the value of one element.
 
  inline double& takeElementReference(const int);
  // Same as above

  inline lightNdouble& SetShape(const int x=-1);
  // Sets the specified size. 

//REMOVE LATER
  lightNdouble SubVector(const int n1,const  int n2) const; 
  // Gets specified interval of the vector.

  const lightNdouble& SetSubVector(const int n1,const  int n2,const lightNdouble& x);
  // Puts all  elements of x into specified interval of  the current vector.
  // Returns the vector x.

  double SetSubVector(const int n1,const  int n2,const double x);
  // Puts the   element x into all elements of specified interval of  the current vector.
  // Returns x.

  int operator==(const lightNdouble&) const;
  // Equality.

  int operator!=(const lightNdouble&) const;
  // Inequality.
 
  lightNdouble& operator+=(const double);
  // Add a value to all elements.

  lightNdouble& operator+=(const lightNdouble&);
  // Elementwise addition.

  lightNdouble& operator-=(const double);
  // Subtract a value from all elements.

  lightNdouble& operator-=(const lightNdouble&);
  // Elementwise subtraction.

  lightNdouble& operator*=(const double);
  // Multiply all elements with a value.

  lightNdouble& operator/=(const double);
  // Divide all elements with a value.

#ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
#endif

  double Extract(const lightNint&) const;
  // Extract an element using given index.   

  int dimension(const int x = 1) const {
    if(x == 1)
      return size1;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == no. elements).

  #ifndef COMPLEX_TOOLS
  lightNdouble& normalize();
  // Normalize vector. Returns a reference to itself.
  #endif

  void Get(double *) const;
  // Get values of all elements and put them in an array.

  void Set(const double *);
  // Set values of all elements from array.

  double * data() const;
  // Direct access to the stored data. Use the result with care.

  lightNdouble operator+() const;
  // Unary plus.

  lightNdouble operator-() const;
  // Unary minus.


  //<ignore>
#ifdef IN_LIGHTNlm_complex_H
  friend class lightNint;
#endif
  friend class light3double;
  friend class light4double;
  friend class lightN3double;
  friend class lightNNdouble;
  friend class lightNNNdouble;
  friend class lightNNNNdouble;
  friend class lightN33double;
  //</ignore>

  friend inline lightNdouble operator+(const lightNdouble&, const lightNdouble&);
  // Elementwise addition.

  //--
  friend inline lightNdouble operator+(const lightNdouble&, const double);
  // Addition to all elements.

  friend inline lightNdouble operator+(const double, const lightNdouble&);
  // Addition to all elements.

#ifdef IN_LIGHTNdouble_H
  friend inline lightNdouble operator+(const lightNdouble&, const int);
  friend inline lightNdouble operator+(const lightNint&, const double);
  friend inline lightNdouble operator+(const double, const lightNint&);
  friend inline lightNdouble operator+(const int, const lightNdouble&);
#endif

#ifdef IN_LIGHTNlm_complex_H
friend inline lightNlm_complex operator+(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator+(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator+(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator+(const double e, const lightNlm_complex& s2);
#endif

  //--

  friend inline lightNdouble operator-(const lightNdouble&, const lightNdouble&);
  // Elementwise subtraction.

  friend inline lightNdouble operator-(const lightNdouble&, const double);
  // Subtraction from all elements.

  friend inline lightNdouble operator-(const double, const lightNdouble&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNdouble_H
  friend inline lightNdouble operator-(const lightNdouble&, const int);
  friend inline lightNdouble operator-(const lightNint&, const double);
  friend inline lightNdouble operator-(const double, const lightNint&);
  friend inline lightNdouble operator-(const int, const lightNdouble&);
#endif

  #ifdef IN_LIGHTNlm_complex_H
friend inline lightNlm_complex operator-(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator-(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator-(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator-(const double e, const lightNlm_complex& s2);
#endif


  friend inline double operator*(const lightNdouble&, const lightNdouble&);
  // Inner product.

  friend inline lightNdouble operator*(const lightNdouble&, const double);
  // Multiply all elements.

  friend inline lightNdouble operator*(const double, const lightNdouble&);
  // Multiply all elements.

#ifdef IN_LIGHTNdouble_H
  friend inline lightNdouble operator*(const lightNdouble&, const int);
  friend inline lightNdouble operator*(const lightNint&, const double);
  friend inline lightNdouble operator*(const double, const lightNint&);
  friend inline lightNdouble operator*(const int, const lightNdouble&);
#endif

#ifdef IN_LIGHTNlm_complex_H
friend inline lightNlm_complex operator*(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator*(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator*(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator*(const double e, const lightNlm_complex& s2);
#endif

  friend inline lightNdouble operator*(const lightNdouble&, const lightNNdouble&);
  // Inner product.

  friend inline lightNdouble operator*(const light3double&, const lightNNdouble&);
  // Inner product.

  friend inline lightNdouble operator*(const light4double&, const lightNNdouble&);
  // Inner product.

  friend inline lightNdouble operator*(const lightNNdouble&,const lightNdouble&);
  // Inner product.

  friend inline lightNdouble operator*(const lightNNdouble&, const light3double&);
  // Inner product.

  friend inline lightNdouble operator*(const lightNNdouble&, const light4double&);
  // Inner product.

  friend inline lightNdouble operator*(const lightN3double&, const light3double&);
  // Inner product.

  friend inline lightNdouble operator*(const lightN3double&, const lightNdouble&);
  // Inner product.

  friend inline lightNdouble operator/(const lightNdouble&, const double);
  // Divide all elements.

  friend inline lightNdouble operator/(const double, const lightNdouble&);
  // Divide all elements.

#ifdef IN_LIGHTNdouble_H
  friend inline lightNdouble operator/(const lightNdouble&, const int);
  friend inline lightNdouble operator/(const lightNint&, const double);
  friend inline lightNdouble operator/(const double, const lightNint&);
  friend inline lightNdouble operator/(const int, const lightNdouble&);
#endif

#ifdef IN_LIGHTNlm_complex_H
friend inline lightNlm_complex operator/(const lightNlm_complex& s1, const double e);
friend inline lightNlm_complex operator/(const lightNdouble& s1, const lm_complex e);
friend inline lightNlm_complex operator/(const lm_complex e, const lightNdouble& s2);
friend inline lightNlm_complex operator/(const double e, const lightNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNlm_complex_H
friend inline lightNdouble arg(const lightNlm_complex& a);
friend inline lightNdouble re(const lightNlm_complex& a);
friend inline lightNdouble im(const lightNlm_complex& a);
friend inline lightNlm_complex conjugate(const lightNlm_complex& a);
//#endif

  friend inline lightNdouble pow(const lightNdouble&, const lightNdouble&);
  // Raise to the power of-function, elementwise.

  friend inline lightNdouble pow(const lightNdouble&, const double);
  // Raise to the power of-function, for all elements.

  friend inline lightNdouble pow(const double, const lightNdouble&);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex pow(const lightNlm_complex& s1, const double e);
  // Raise to the power of-function, for all elements.

  friend inline lightNlm_complex pow( const double e,const lightNlm_complex& s1);
  // Raise to the power of-function, for all elements.

  friend inline lightNdouble ElemProduct(const lightNdouble&, const lightNdouble&);
  // Elementwise multiplication.

  friend inline lightNdouble ElemQuotient(const lightNdouble&, const lightNdouble&);
  // Elementwise division.

  friend inline lightNdouble Apply(const lightNdouble&, double f(double));
  // Apply the function elementwise all elements.

  friend inline lightNdouble Apply(const lightNdouble&, const lightNdouble&, double f(double, double));
  // Apply the function elementwise on all elements in the two vectors.


#ifdef IN_LIGHTNlm_complex_H
#else
  friend inline lightNdouble abs(const lightNdouble&);
#ifdef IN_LIGHTNdouble_H
  friend inline lightNdouble abs(const lightNlm_complex&);
#endif 
#endif

#ifndef COMPLEX_TOOLS
  // sign
  friend inline lightNint sign(const lightNint&);  
  friend inline lightNint sign(const lightNdouble&);
#else
  friend inline lightNlm_complex sign(const lightNlm_complex&);
#endif
  /// Added 2/2/98 

  friend inline lightNdouble  FractionalPart(const lightNdouble&);
  //  FractionalPart

  //old version
  //friend inline lightNint  IntegerPart(const lightNint&);
  //  IntegerPart

  //friend inline lightNint  IntegerPart(const lightNdouble&);
  //  IntegerPart

  //new version
  friend inline lightNdouble  IntegerPart(const lightNdouble&);
  //  IntegerPart

  friend inline lightNdouble Mod (const  lightNdouble&,const  lightNdouble&);
  // Mod
 
  friend inline lightNdouble Mod (const  lightNdouble&,const double);
  // Mod

  friend inline lightNdouble Mod (const double,const  lightNdouble&);
  // Mod

  friend inline double LightMax (const lightNdouble& );
  // Max

  friend inline double LightMin (const lightNdouble& );
  // Min

  friend inline double findLightMax (const double *,const int );
  // Find Max
 
  friend inline double findLightMin (const  double *,const  int );
  // Find Min
 
  /// End Added 

  friend inline lightNint ifloor(const lightNdouble&);
  // ifloor

  friend inline lightNint iceil(const lightNdouble&);
  // iceil

  friend inline lightNint irint(const lightNdouble&);
  // irint

  friend inline lightNdouble sqrt(const lightNdouble&);
  // sqrt

  friend inline lightNdouble exp(const lightNdouble&);
  // exp

  friend inline lightNdouble log(const lightNdouble&);
  // log

  friend inline lightNdouble sin(const lightNdouble&);
  // sin

  friend inline lightNdouble cos(const lightNdouble&);
  // cos

  friend inline lightNdouble tan(const lightNdouble&);
  // tan

  friend inline lightNdouble asin(const lightNdouble&);
  // asin

  friend inline lightNdouble acos(const lightNdouble&);
  // acos

  friend inline lightNdouble atan(const lightNdouble&);
  // atan

  friend inline lightNdouble sinh(const lightNdouble&);
  // sinh

  friend inline lightNdouble cosh(const lightNdouble&);
  // cosh

  friend inline lightNdouble tanh(const lightNdouble&);
  // tanh

  friend inline lightNdouble asinh(const lightNdouble&);
  // asinh

  friend inline lightNdouble acosh(const lightNdouble&);
  // acosh

  friend inline lightNdouble atanh(const lightNdouble&);
  // atanh

  friend inline lightNlm_complex sqrt(const lightNlm_complex&);
  // sqrt

  friend inline lightNlm_complex exp(const lightNlm_complex&);
  // exp

  friend inline lightNlm_complex ifloor(const lightNlm_complex&);
  // ifloor

  friend inline lightNlm_complex iceil(const lightNlm_complex&);
  // iceil

  friend inline lightNlm_complex irint(const lightNlm_complex&);
  // irint

  friend inline lightNlm_complex log(const lightNlm_complex&);
  // log

  friend inline lightNlm_complex sin(const lightNlm_complex&);
  // sin

  friend inline lightNlm_complex cos(const lightNlm_complex&);
  // cos

  friend inline lightNlm_complex tan(const lightNlm_complex&);
  // tan

  friend inline lightNlm_complex asin(const lightNlm_complex&);
  // asin

  friend inline lightNlm_complex acos(const lightNlm_complex&);
  // acos

  friend inline lightNlm_complex atan(const lightNlm_complex&);
  // atan

  friend inline lightNlm_complex sinh(const lightNlm_complex&);
  // sinh

  friend inline lightNlm_complex cosh(const lightNlm_complex&);
  // cosh

  friend inline lightNlm_complex tanh(const lightNlm_complex&);
  // tanh

  friend inline lightNlm_complex asinh(const lightNlm_complex&);
  // asinh

  friend inline lightNlm_complex acosh(const lightNlm_complex&);
  // acosh

  friend inline lightNlm_complex atanh(const lightNlm_complex&);
  // atanh


  friend inline lightNdouble Cross (const  lightNdouble&, const  lightNdouble&); 
  friend inline lightNdouble Cross (const  lightNdouble&, const  lightNdouble&, const  lightNdouble&); 
  friend inline lightNdouble Cross (const  lightNdouble&, const  lightNdouble&, const  lightNdouble&, const  lightNdouble&); 

  friend inline lightNdouble light_join (const lightNdouble arr1, const lightNdouble arr2);
  friend inline lightNdouble light_drop (const lightNdouble arr1, const R r);

#ifdef IN_LIGHTNint_H
  friend inline double light_mean     ( const  lightNdouble& );
  friend inline double light_standard_deviation( const  lightNdouble& );
  friend inline double light_variance ( const  lightNdouble& );
  friend inline double light_median   ( const  lightNdouble& );
#else
  friend inline double light_mean     ( const  lightNdouble& );
  friend inline double light_variance ( const  lightNdouble& );
  friend inline double light_standard_deviation( const  lightNdouble& );
  friend inline double light_median   ( const  lightNdouble& );
#endif
  
  friend inline double light_total    ( const  lightNdouble& );
 
 
  friend inline lightNdouble light_sort (const lightNdouble arr1);

  friend inline TDNtypeH light_quantile3L (const lightNdouble arr1, const lightNdouble q2, const lightNNdouble params);
  friend inline TDtypeH light_quantile3 (const lightNdouble arr1, double q,  const lightNNdouble params);
  friend inline TDNtypeH light_quantile2L (const lightNdouble arr1, const lightNdouble q2);
  friend inline TDtypeH light_quantile2 (const lightNdouble arr1, double q);

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNdouble::lightNdouble(const lightNint& s1, const double e, const lightmat_plus_enum)

  double *elem;
  // elem always points at the area where the elements are.


protected:
  double sarea[LIGHTN_SIZE];
  // The values are stored here if they fit into LIGHTN_SIZE
  // else a memory area is allocated.

  int size1;
  // The size of the vector (number of elements).

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNdouble(const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // argument is the size of the created vector. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNdouble(const lightNdouble&, const lightNdouble&, const lightmat_plus_enum);
  lightNdouble(const lightNdouble&, const double, const lightmat_plus_enum);
#ifdef IN_LIGHTNdouble_H
  lightNdouble(const lightNdouble&, const int, const lightmat_plus_enum);
  lightNdouble(const lightNint&, const double, const lightmat_plus_enum);
#endif

  lightNdouble(const lightNdouble&, const lightNdouble&, const lightmat_minus_enum);
  lightNdouble(const double, const lightNdouble&, const lightmat_minus_enum);
#ifdef IN_LIGHTNdouble_H
  lightNdouble(const int, const lightNdouble&, const lightmat_minus_enum);
  lightNdouble(const double, const lightNint&, const lightmat_minus_enum);
#endif

  lightNdouble(const lightNdouble&, const lightNdouble&, const lightmat_mult_enum);
  lightNdouble(const lightNdouble&, const double, const lightmat_mult_enum);
#ifdef IN_LIGHTNdouble_H
  lightNdouble(const lightNdouble&, const int, const lightmat_mult_enum);
  lightNdouble(const lightNint&, const double, const lightmat_mult_enum);
#endif

  lightNdouble(const lightNdouble&, const lightNNdouble&, const lightmat_mult_enum);
  lightNdouble(const lightNNdouble&, const lightNdouble&, const lightmat_mult_enum);
  lightNdouble(const light3double&, const lightNNdouble&, const lightmat_mult_enum);
  lightNdouble(const light4double&, const lightNNdouble&, const lightmat_mult_enum);
  lightNdouble(const lightN3double&, const light3double&, const lightmat_mult_enum);
  lightNdouble(const lightN3double&, const lightNdouble&, const lightmat_mult_enum);
  lightNdouble(const lightNNdouble&, const light3double&, const lightmat_mult_enum);
  lightNdouble(const lightNNdouble&, const light4double&, const lightmat_mult_enum);

  lightNdouble(const double, const lightNdouble&, const lightmat_div_enum);
#ifdef IN_LIGHTNdouble_H
  lightNdouble(const int, const lightNdouble&, const lightmat_div_enum);
  lightNdouble(const double, const lightNint&, const lightmat_div_enum);
#endif

  lightNdouble(const lightNdouble&, const lightNdouble&, const lightmat_pow_enum);
  lightNdouble(const lightNdouble&, const double, const lightmat_pow_enum);
  lightNdouble(const double, const lightNdouble&, const lightmat_pow_enum);

#ifdef IN_LIGHTNlm_complex_H
  lightNlm_complex(const lightNlm_complex& s1, const double e, const lightmat_pow_enum) ;
  lightNlm_complex(const double e,const lightNlm_complex& s1, const lightmat_pow_enum) ;
#endif

#ifdef IN_LIGHTNlm_complex_H
 #else
  #ifdef IN_LIGHTNdouble_H
   lightNdouble(const lightNdouble&, const lightmat_abs_enum);
   lightNdouble(const lightNlm_complex&, const lightmat_abs_enum);
  #else
  lightNdouble(const lightNdouble&, const lightmat_abs_enum);
  #endif
#endif

  lightNdouble(const lightNdouble&, const lightNdouble&, const lightmat_eprod_enum);
  lightNdouble(const lightNdouble&, const lightNdouble&, const lightmat_equot_enum);
  lightNdouble(const lightNdouble&, double f(double), const lightmat_apply_enum);
  lightNdouble(const lightNdouble&, const lightNdouble&, double f(double, double), const lightmat_apply_enum);

};





 inline double findLightMax (const double *,const int );
// Find Max

 inline double findLightMin (const  double *,const  int );
// Find Min








typedef lightNdouble doubleN;
typedef lightNint intN;
typedef lightNlm_complex lm_complexN;

#undef TDtypeH  
#undef TDNtypeH 

#undef IN_LIGHTNdouble_H

#endif
