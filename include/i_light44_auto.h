inline light44int& Set (const int i0, const int i1, const int val);
inline light44int& Set (const R r0, const int i1, const lightNint& arr);
inline light44int& Set (const R r0, const int i1, const light3int& arr);
inline light44int& Set (const R r0, const int i1, const light4int& arr);
inline light44int& Set (const R r0, const int i1, const int val);
inline light44int& Set (const int i0, const R r1, const lightNint& arr);
inline light44int& Set (const int i0, const R r1, const light3int& arr);
inline light44int& Set (const int i0, const R r1, const light4int& arr);
inline light44int& Set (const int i0, const R r1, const int val);
inline light44int& Set (const R r0, const R r1, const lightNNint& arr);
inline light44int& Set (const R r0, const R r1, const light33int& arr);
inline light44int& Set (const R r0, const R r1, const light44int& arr);
inline light44int& Set (const R r0, const R r1, const int val);
inline lightNint operator() (const R r0, const int i1) const;
inline lightNint operator() (const int i0, const R r1) const;
inline lightNNint operator() (const R r0, const R r1) const;
friend inline light44double atan2 (const double e, const light44int &s1);
friend inline light44double atan2 (const double e, const light44double &s1);
friend inline light44double atan2 (const light44int &s1, const double e);
friend inline light44double atan2 (const light44double &s1, const double e);
friend inline light44double atan2 (const light44int &s1, const light44int &s2);
friend inline light44double atan2 (const light44int &s1, const light44double &s2);
friend inline light44double atan2 (const light44double &s1, const light44int &s2);
friend inline light44double atan2 (const light44double &s1, const light44double &s2);
friend inline light44double atan2 (const light44int &s1, const int e);
friend inline light44double atan2 (const light44double &s1, const int e);
friend inline light44double atan2 (const int e, const light44int &s1);
friend inline light44double atan2 (const int e, const light44double &s1);

#ifdef IN_LIGHT44double_I_H
inline light44double(const double e, const light44int &s1, const lightmat_atan2_enum);
inline light44double(const double e, const light44double &s1, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const double e, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const double e, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const light44int &s2, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const light44double &s2, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const light44int &s2, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const light44double &s2, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const int e, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const int e, const lightmat_atan2_enum);
inline light44double(const int e, const light44int &s1, const lightmat_atan2_enum);
inline light44double(const int e, const light44double &s1, const lightmat_atan2_enum);

#endif
