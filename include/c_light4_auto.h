inline light4lm_complex& Set (const int i0, const lm_complex val);
inline light4lm_complex& Set (const R r0, const lightNlm_complex& arr);
inline light4lm_complex& Set (const R r0, const light3lm_complex& arr);
inline light4lm_complex& Set (const R r0, const light4lm_complex& arr);
inline light4lm_complex& Set (const R r0, const lm_complex val);
inline lightNlm_complex operator() (const R r0) const;
friend inline light4double atan2 (const double e, const light4int &s1);
friend inline light4double atan2 (const double e, const light4double &s1);
friend inline light4double atan2 (const light4int &s1, const double e);
friend inline light4double atan2 (const light4double &s1, const double e);
friend inline light4double atan2 (const light4int &s1, const light4int &s2);
friend inline light4double atan2 (const light4int &s1, const light4double &s2);
friend inline light4double atan2 (const light4double &s1, const light4int &s2);
friend inline light4double atan2 (const light4double &s1, const light4double &s2);
friend inline light4double atan2 (const light4int &s1, const int e);
friend inline light4double atan2 (const light4double &s1, const int e);
friend inline light4double atan2 (const int e, const light4int &s1);
friend inline light4double atan2 (const int e, const light4double &s1);

#ifdef IN_LIGHT4double_C_H
inline light4double(const double e, const light4int &s1, const lightmat_atan2_enum);
inline light4double(const double e, const light4double &s1, const lightmat_atan2_enum);
inline light4double(const light4int &s1, const double e, const lightmat_atan2_enum);
inline light4double(const light4double &s1, const double e, const lightmat_atan2_enum);
inline light4double(const light4int &s1, const light4int &s2, const lightmat_atan2_enum);
inline light4double(const light4int &s1, const light4double &s2, const lightmat_atan2_enum);
inline light4double(const light4double &s1, const light4int &s2, const lightmat_atan2_enum);
inline light4double(const light4double &s1, const light4double &s2, const lightmat_atan2_enum);
inline light4double(const light4int &s1, const int e, const lightmat_atan2_enum);
inline light4double(const light4double &s1, const int e, const lightmat_atan2_enum);
inline light4double(const int e, const light4int &s1, const lightmat_atan2_enum);
inline light4double(const int e, const light4double &s1, const lightmat_atan2_enum);

#endif
