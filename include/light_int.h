//
// sign
//


inline int sign(int    x);	

#ifndef __GNUG__

inline  int sign(double x);	

#else

// GNU has this symbol in library. We do not inline it.
        int sign(double x);	
#endif

// See details in light_int.icc
// These functions are defined once there.


#ifndef COMPLEX_TOOLS

light3int sign(const light3<T>& s);	


light4int sign(const light4<T>& s);	


light33int sign(const light33<T>& s);	


light44int sign(const light44<T>& s);	


lightNint sign(const lightN<T>& s);	


lightN3int sign(const lightN3<T>& s);	


lightNNint sign(const lightNN<T>& s);	


lightNNNint sign(const lightNNN<T>& s);	


lightNNNNint sign(const lightNNNN<T>& s);	

//
// abs
//
template<class T>
lightN<T> abs(const lightN<T>& s);

template<class T>
lightN3<T> abs(const lightN3<T>& s);

template<class T>
lightNN<T> abs(const lightNN<T>& s);

template<class T>
lightNNN<T> abs(const lightNNN<T>& s);

template<class T>
lightNNNN<T> abs(const lightNNNN<T>& s);


#else
light3lm_complex sign(const light3lm_complex& s);	


light4lm_complex sign(const light4lm_complex& s);	


light33lm_complex sign(const light33lm_complex& s);	


light44lm_complex sign(const light44lm_complex& s);	


lightNlm_complex sign(const lightNlm_complex& s);	


lightN3lm_complex sign(const lightN3lm_complex& s);	


lightNNlm_complex sign(const lightNNlm_complex& s);	


lightNNNlm_complex sign(const lightNNNlm_complex& s);	


lightNNNNlm_complex sign(const lightNNNNlm_complex& s);	
#endif

//
// Mod
//


lightN<T> Mod(const lightN<T>& v, const <T> s );


lightN<T> Mod(const <T> v, const lightN<T>& s);


lightN<T> Mod(const lightN<T>&v1, const lightN<T>&v2);




lightNN<T> Mod(const lightNN<T>& v, const <T> s );


lightNN<T> Mod(const <T> v, const lightNN<T>& s);


lightNN<T> Mod(const lightNN<T>&v1, const lightNN<T>&v2);




lightNNN<T> Mod(const lightNNN<T>& v, const <T> s );


lightNNN<T> Mod(const <T> v, const lightNNN<T>& s);


lightNNN<T> Mod(const lightNNN<T>&v1, const lightNNN<T>&v2);




lightNNNN<T> Mod(const lightNNNN<T>& v, const <T> s );


lightNNNN<T> Mod(const <T> v, const lightNNNN<T>& s);


lightNNNN<T> Mod(const lightNNNN<T>&v1, const lightNNNN<T>&v2);



