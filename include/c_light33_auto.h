inline light33lm_complex& Set (const int i0, const int i1, const lm_complex val);
inline light33lm_complex& Set (const R r0, const int i1, const lightNlm_complex& arr);
inline light33lm_complex& Set (const R r0, const int i1, const light3lm_complex& arr);
inline light33lm_complex& Set (const R r0, const int i1, const light4lm_complex& arr);
inline light33lm_complex& Set (const R r0, const int i1, const lm_complex val);
inline light33lm_complex& Set (const int i0, const R r1, const lightNlm_complex& arr);
inline light33lm_complex& Set (const int i0, const R r1, const light3lm_complex& arr);
inline light33lm_complex& Set (const int i0, const R r1, const light4lm_complex& arr);
inline light33lm_complex& Set (const int i0, const R r1, const lm_complex val);
inline light33lm_complex& Set (const R r0, const R r1, const lightNNlm_complex& arr);
inline light33lm_complex& Set (const R r0, const R r1, const light33lm_complex& arr);
inline light33lm_complex& Set (const R r0, const R r1, const light44lm_complex& arr);
inline light33lm_complex& Set (const R r0, const R r1, const lm_complex val);
inline lightNlm_complex operator() (const R r0, const int i1) const;
inline lightNlm_complex operator() (const int i0, const R r1) const;
inline lightNNlm_complex operator() (const R r0, const R r1) const;
friend inline light33double atan2 (const double e, const light33int &s1);
friend inline light33double atan2 (const double e, const light33double &s1);
friend inline light33double atan2 (const light33int &s1, const double e);
friend inline light33double atan2 (const light33double &s1, const double e);
friend inline light33double atan2 (const light33int &s1, const light33int &s2);
friend inline light33double atan2 (const light33int &s1, const light33double &s2);
friend inline light33double atan2 (const light33double &s1, const light33int &s2);
friend inline light33double atan2 (const light33double &s1, const light33double &s2);
friend inline light33double atan2 (const light33int &s1, const int e);
friend inline light33double atan2 (const light33double &s1, const int e);
friend inline light33double atan2 (const int e, const light33int &s1);
friend inline light33double atan2 (const int e, const light33double &s1);

#ifdef IN_LIGHT33double_C_H
inline light33double(const double e, const light33int &s1, const lightmat_atan2_enum);
inline light33double(const double e, const light33double &s1, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const double e, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const double e, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const light33int &s2, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const light33double &s2, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const light33int &s2, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const light33double &s2, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const int e, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const int e, const lightmat_atan2_enum);
inline light33double(const int e, const light33int &s1, const lightmat_atan2_enum);
inline light33double(const int e, const light33double &s1, const lightmat_atan2_enum);

#endif
