//           -*- c++ -*-

#ifndef LIGHTNN_C_H
#define LIGHTNN_C_H
// <cd> lightNNlm_complex
//
// .SS Functionality
//
// lightNN is a template for classes that implement matrices with any
// number of elements. E.g. a matrix v with 5x6 elements of type double
// can be instanciated with:
//
// <code>lightNN&lt;double&gt; v(5,6);</code>
//
// The number of rows and columns in the matrix can change during
// execution. It changes its size to whatever is needed when it is
// assigned a new value with an assignment operator.
//
// When a lightNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNN object with 5x5 elements to one with 5x6 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
// Vadim Engelson
//

#include <assert.h>

#define IN_LIGHTNNlm_complex_C_H


#define LIGHTNN_SIZE1 10
#define LIGHTNN_SIZE2 10
#define LIGHTNN_SIZE (LIGHTNN_SIZE1*LIGHTNN_SIZE2)
// The default size.

 class light3;
 class light4;
 class lightN;
 class light33;
 class light44;
 class lightN3;
 class lightNNN;


class lightNNlm_complex {
public:

#ifdef IN_LIGHTNNdouble_C_H
  friend class lightNNint;
#else
  friend class lightNNdouble;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNint;
#endif


  #include "c_lightNN_auto.h"

  inline lightNNlm_complex();
  // Default constructor.

  inline lightNNlm_complex(const lightNNlm_complex&);
  // Copy constructor.

  inline lightNNlm_complex(const light33lm_complex&);
  // Conversion.

  inline lightNNlm_complex(const light44lm_complex&);
  // Conversion.

  inline lightNNlm_complex(const lightN3lm_complex&);
  // Conversion.

  inline lightNNlm_complex(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNlm_complex(const int, const int);
  // Construct a lightNN of given size.

  inline lightNNlm_complex(const int, const int, const lm_complex *);
  // Construct a lightNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNlm_complex(const int, const int, const lm_complex);
  // Construct a lightNN of given size and initialize all elements
  // with a value (the third argument).

  inline ~lightNNlm_complex();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNlm_complex();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNlm_complex();
  // Convert to complex.
#endif



  lightNNlm_complex& operator=(const lightNNlm_complex&);
  // Assignment.

  lightNNlm_complex& operator=(const light33lm_complex&);
  // Assignment, change size to 3x3.

  lightNNlm_complex& operator=(const light44lm_complex&);
  // Assignment, change size to 4x4.

  lightNNlm_complex& operator=(const lightN3lm_complex&);
  // Assignment, change size to Nx3.

  lightNNlm_complex& operator=(const lm_complex);
  // Assign one value to all elements.

  lm_complex operator() (const int x, const int y) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2));
#ifdef ROWMAJOR
    return elem[y-1 + (x-1)*size2];
#else
    return elem[x-1 + (y-1)*size1];
#endif
  };
  // Get the value of one element.

  //
  // Set and Get
  //

  lm_complex& operator()(const int x, const int y) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2));
#ifdef ROWMAJOR
    return elem[y-1 + (x-1)*size2];
#else
    return elem[x-1 + (y-1)*size1];
#endif
  };
  // Get/Set the value of one element.

  //
  // Set
  //

  inline lightNNlm_complex& Set (const int i0, const lm_complex val);
  inline lightNNlm_complex& Set (const int i0, const lightNlm_complex& arr);
  inline lightNNlm_complex& Set (const int i0, const light3lm_complex& arr);
  inline lightNNlm_complex& Set (const int i0, const light4lm_complex& arr);


  lm_complex&  takeElementReference (const int x, const int y);
  // Same

  lightNlm_complex operator()(const int) const;
  // Get the value of one row.
 
  const lightNlm_complex& SetCol(const int n,const lightNlm_complex& x);
  // Put x into column n. Return x.

  const lightNlm_complex& SetRow(const int n,const lightNlm_complex& x);
  // Put x into row n. Return x.
  
  lm_complex SetCol(const int n,const lm_complex x);
  // Put x into all elements of  column n. Return x.

  lm_complex SetRow(const int n,const lm_complex x);
   // Put x into all elements of row n. Return x.

  const lightNNlm_complex& SetCols(const int n1,const int n2,const  lightNNlm_complex& x);
  // Put every  element of matrix x to the columns n1..n2. Return x. 
  // In this function and below:
  // n1=0 means to use  1. 
  // n2=0 means to use the largest possible value of matrix index.

  const lightNNlm_complex& SetRows(const int n1,const int n2,const  lightNNlm_complex& x);
  // Put every  element of matrix x to the rows n1..n2.  Return x. 
  
  lm_complex SetCols(const int n1, const int n2, const  lm_complex x);
  // Put x to very element of all columns n1..n2. Return x. 
  
  lm_complex SetRows(const int n1,const int n2,const  lm_complex x);
  // Put x to very eleme nt of all rows n1..n2. Return x. 
  
  lightNlm_complex  Col(const int n) const;
  // Get Column n.  

  lightNlm_complex  Row(const int n) const;
  // Get Row n.

// REMOVE LATER
  inline lightNNlm_complex   SubMatrix(const int r1, const int r2,const int c1, const int c2) const;
  // Get submatrix of rows r1..r2, columns c1..c2.

  inline const lightNNlm_complex& SetSubMatrix(const int r1,const int r2,const int c1,const int c2,
                  const  lightNNlm_complex& x);
  // Put matrix x into  submatrix  of rows r1..r2, columns c1..c2. Return x.

  inline  lm_complex SetSubMatrix(const int r1,const int r2,const int c1,const int c2,
                   const lm_complex x);
  // Put x into rows r1..r2, columns c1..c2. Return x.
  
  lightNNlm_complex Rows(const int x1,const int x2) const;
  // Get submstrix produced by rows x1..x2.
  
  
  lightNNlm_complex Cols(const int y1,const int y2) const;
  // Get submstrix produced by columns  y1..y2.
  
  int operator==(const lightNNlm_complex&) const;
  // Equality.

  int operator!=(const lightNNlm_complex&) const;
  // Inequality.
 
  lightNNlm_complex& operator+=(const lm_complex);
  // Add a value to all elements.

  lightNNlm_complex& operator+=(const lightNNlm_complex&);
  // Elementwise addition.

  lightNNlm_complex& operator-=(const lm_complex);
  // Subtract a value from all elements.

  lightNNlm_complex& operator-=(const lightNNlm_complex&);
  // Elementwise subtraction.

  lightNNlm_complex& operator*=(const lm_complex);
  // Muliply all elements with a value.

  lightNNlm_complex& operator/=(const lm_complex);
  // Divide all elements with a value.
 
  inline lightNNlm_complex& SetShape(const int x=-1, const int y=-1);
  // Sets specified shape for the matrix.

  lm_complex Extract(const lightNint&) const;
  // Extract an element using given index.   

  lightNNlm_complex& reshape(const int, const int, const lightNlm_complex&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. All vector-columns of the lightNN object will get
  // the value of the lightN-argument. The value of the first argument
  // must be the same as the number of elements in the lightN-vector.

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(lm_complex *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const lm_complex *);
  // Set values of all elements from array (row major order).
 
  lm_complex * data() const; 
  // Direct access to the stored data. Use the result with care.

  lightNNlm_complex operator+() const;
  // Unary plus.

  lightNNlm_complex operator-() const;
  // Unary minus.

  friend inline lightNNlm_complex operator+(const lightNNlm_complex&, const lightNNlm_complex&);
  // Elementwise addition.

  friend inline lightNNlm_complex operator+(const lightNNlm_complex&, const lm_complex);
  // Addition to all elements.

  friend inline lightNNlm_complex operator+(const lm_complex, const lightNNlm_complex&);
  // Addition to all elements.

#ifdef IN_LIGHTNNdouble_C_H
  friend inline lightNNdouble operator+(const lightNNdouble&, const int);
  friend inline lightNNdouble operator+(const lightNNint&, const double);
  friend inline lightNNdouble operator+(const double, const lightNNint&);
  friend inline lightNNdouble operator+(const int, const lightNNdouble&);
#endif

#ifdef IN_LIGHTNNlm_complex_C_H
friend inline lightNNlm_complex operator+(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator+(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator+(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator+(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNNlm_complex operator-(const lightNNlm_complex&, const lightNNlm_complex&);
  // Elementwise subtraction.

  friend inline lightNNlm_complex operator-(const lightNNlm_complex&, const lm_complex);
  // Subtraction from all elements.

  friend inline lightNNlm_complex operator-(const lm_complex, const lightNNlm_complex&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNdouble_C_H
  friend inline lightNNdouble operator-(const lightNNdouble&, const int);
  friend inline lightNNdouble operator-(const lightNNint&, const double);
  friend inline lightNNdouble operator-(const double, const lightNNint&);
  friend inline lightNNdouble operator-(const int, const lightNNdouble&);
#endif

  #ifdef IN_LIGHTNNlm_complex_C_H
friend inline lightNNlm_complex operator-(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator-(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator-(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator-(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNNlm_complex operator*(const lightNNlm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const lightNNlm_complex&, const lm_complex);
  // Multiply all elements.

  friend inline lightNNlm_complex operator*(const lm_complex, const lightNNlm_complex& s);
  // Multiply all elements.

#ifdef IN_LIGHTNNdouble_C_H
  friend inline lightNNdouble operator*(const lightNNdouble&, const int);
  friend inline lightNNdouble operator*(const lightNNint&, const double);
  friend inline lightNNdouble operator*(const double, const lightNNint&);
  friend inline lightNNdouble operator*(const int, const lightNNdouble&);
#endif

#ifdef IN_LIGHTNNlm_complex_C_H
friend inline lightNNlm_complex operator*(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator*(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator*(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator*(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNNlm_complex operator*(const lightNNlm_complex&, const light33lm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const lightNNlm_complex&, const light44lm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const lightN3lm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const light33lm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const light44lm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const lightNlm_complex&, const lightNNNlm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const lightNNNlm_complex&, const lightNlm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const light3lm_complex&, const lightNNNlm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const lightNNNlm_complex&, const light3lm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const light4lm_complex&, const lightNNNlm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator*(const lightNNNlm_complex&, const light4lm_complex&);
  // Inner product.

  friend inline lightNNlm_complex operator/(const lightNNlm_complex&, const lm_complex);
  // Divide all elements.

  friend inline lightNNlm_complex operator/(const lm_complex, const lightNNlm_complex&);
  // Divide all elements.

#ifdef IN_LIGHTNNdouble_C_H
  friend inline lightNNdouble operator/(const lightNNdouble&, const int);
  friend inline lightNNdouble operator/(const lightNNint&, const double);
  friend inline lightNNdouble operator/(const double, const lightNNint&);
  friend inline lightNNdouble operator/(const int, const lightNNdouble&);
#endif

#ifdef IN_LIGHTNNlm_complex_C_H
friend inline lightNNlm_complex operator/(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator/(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator/(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator/(const double e, const lightNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNlm_complex_C_H
friend inline lightNNdouble arg(const lightNNlm_complex& a);
friend inline lightNNdouble re(const lightNNlm_complex& a);
friend inline lightNNdouble im(const lightNNlm_complex& a);
friend inline lightNNlm_complex conjugate(const lightNNlm_complex& a);
//#endif

  friend inline lightNNlm_complex pow(const lightNNlm_complex&, const lightNNlm_complex&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNlm_complex pow(const lightNNlm_complex&, const lm_complex);
  // Raise to the power of-function, for all elements.

  friend inline lightNNlm_complex pow(const lm_complex, const lightNNlm_complex&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNlm_complex ElemProduct(const lightNNlm_complex&, const lightNNlm_complex&);
  // Elementwise multiplication.

  friend inline lightNNlm_complex ElemQuotient(const lightNNlm_complex&, const lightNNlm_complex&);
  // Elementwise division.

  friend inline lightNNlm_complex Apply(const lightNNlm_complex&, lm_complex f(lm_complex));
  // Apply the function elementwise all elements.

  friend inline lightNNlm_complex Apply(const lightNNlm_complex&, const lightNNlm_complex&, lm_complex f(lm_complex, lm_complex));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline lightNNlm_complex Transpose(const lightNNlm_complex&);
  // Transpose matrix.

  friend inline lightNNlm_complex Transpose(const lightN3lm_complex&);
  // Transpose matrix.

  friend inline lightNNlm_complex OuterProduct(const lightNlm_complex&, const lightNlm_complex&);
  // Outer product.


#ifdef IN_LIGHTNNlm_complex_C_H
#else
  friend inline lightNNlm_complex abs(const lightNNlm_complex&);
#ifdef IN_LIGHTNNdouble_C_H
  friend inline lightNNdouble abs(const lightNNlm_complex&);
#endif 
#endif


#ifndef COMPLEX_TOOLS
  friend inline lightNNint sign(const lightNNint&);
  // sign
#else
  // sign
  friend inline lightNNlm_complex sign(const lightNNlm_complex&);
#endif
  friend inline lightNNint sign(const lightNNdouble&);
  // sign

  /// Added 2/2/98 

  friend inline lightNNlm_complex  FractionalPart(const lightNNlm_complex&);
  //  FractionalPart

  friend inline lightNNlm_complex  IntegerPart(const lightNNlm_complex&);
  //  IntegerPart

  //friend inline lightNNint  IntegerPart(const lightNNdouble&);
  //  IntegerPart

  friend inline lightNNlm_complex Mod (const  lightNNlm_complex&,const  lightNNlm_complex&);
  // Mod
 
  friend inline lightNNlm_complex Mod (const  lightNNlm_complex&,const lm_complex);
  // Mod

  friend inline lightNNlm_complex Mod (const lm_complex,const  lightNNlm_complex&);
  // Mod

  friend inline lm_complex LightMax (const lightNNlm_complex& );
  // Max

  friend inline lm_complex LightMin (const lightNNlm_complex& );
  // Min

  friend inline lm_complex findLightMax (const  lm_complex *,const  int );
  // Find Max
 
  friend inline lm_complex findLightMin (const  lm_complex *,const  int );
  // Find Min
 
  /// End Added 



  friend inline lightNNint ifloor(const lightNNdouble&);
  // ifloor

  friend inline lightNNint iceil(const lightNNdouble&);
  // iceil

  friend inline lightNNint irint(const lightNNdouble&);
  // irint

  friend inline lightNNdouble sqrt(const lightNNdouble&);
  // sqrt

  friend inline lightNNdouble exp(const lightNNdouble&);
  // exp

  friend inline lightNNdouble log(const lightNNdouble&);
  // log

  friend inline lightNNdouble sin(const lightNNdouble&);
  // sin

  friend inline lightNNdouble cos(const lightNNdouble&);
  // cos

  friend inline lightNNdouble tan(const lightNNdouble&);
  // tan

  friend inline lightNNdouble asin(const lightNNdouble&);
  // asin

  friend inline lightNNdouble acos(const lightNNdouble&);
  // acos

  friend inline lightNNdouble atan(const lightNNdouble&);
  // atan

  friend inline lightNNdouble sinh(const lightNNdouble&);
  // sinh

  friend inline lightNNdouble cosh(const lightNNdouble&);
  // cosh

  friend inline lightNNdouble tanh(const lightNNdouble&);
  // tanh

  friend inline lightNNdouble asinh(const lightNNdouble&);
  // asinh

  friend inline lightNNdouble acosh(const lightNNdouble&);
  // acosh

  friend inline lightNNdouble atanh(const lightNNdouble&);
  // atanh
  
  friend inline lightNNlm_complex ifloor(const lightNNlm_complex&);
  // ifloor

  friend inline lightNNlm_complex iceil(const lightNNlm_complex&);
  // iceil

  friend inline lightNNlm_complex irint(const lightNNlm_complex&);
  // irint

  friend inline lightNNlm_complex sqrt(const lightNNlm_complex&);
  // sqrt

  friend inline lightNNlm_complex exp(const lightNNlm_complex&);
  // exp

  friend inline lightNNlm_complex log(const lightNNlm_complex&);
  // log

  friend inline lightNNlm_complex sin(const lightNNlm_complex&);
  // sin

  friend inline lightNNlm_complex cos(const lightNNlm_complex&);
  // cos

  friend inline lightNNlm_complex tan(const lightNNlm_complex&);
  // tan

  friend inline lightNNlm_complex asin(const lightNNlm_complex&);
  // asin

  friend inline lightNNlm_complex acos(const lightNNlm_complex&);
  // acos

  friend inline lightNNlm_complex atan(const lightNNlm_complex&);
  // atan

  friend inline lightNNlm_complex sinh(const lightNNlm_complex&);
  // sinh

  friend inline lightNNlm_complex cosh(const lightNNlm_complex&);
  // cosh

  friend inline lightNNlm_complex tanh(const lightNNlm_complex&);
  // tanh

  friend inline lightNNlm_complex asinh(const lightNNlm_complex&);
  // asinh

  friend inline lightNNlm_complex acosh(const lightNNlm_complex&);
  // acosh

  friend inline lightNNlm_complex atanh(const lightNNlm_complex&);
  // atanh



#ifdef IN_LIGHTNNint_C_H
  friend inline lightNdouble light_mean     ( const  lightNNlm_complex& );
  friend inline lightNdouble light_standard_deviation( const  lightNNlm_complex& );
  friend inline lightNdouble light_variance ( const  lightNNlm_complex& );
  friend inline lightNdouble light_median   ( const  lightNNlm_complex& );
#else
  friend inline lightNlm_complex light_mean     ( const  lightNNlm_complex& );
  friend inline lightNlm_complex light_variance ( const  lightNNlm_complex& );
  friend inline lightNlm_complex light_standard_deviation( const  lightNNlm_complex& );
  friend inline lightNlm_complex light_median   ( const  lightNNlm_complex& );
#endif
  
  friend inline lightNlm_complex light_total    ( const  lightNNlm_complex& );
 
 
  friend inline lightNNlm_complex light_sort (const lightNNlm_complex arr1);
  
  friend inline lightNlm_complex light_flatten (const lightNNlm_complex s);
  friend inline lightNlm_complex light_flatten (const lightNNlm_complex s, int level);
  friend inline lightNNlm_complex light_flatten0 (const lightNNlm_complex s);


  //<ignore>
#ifdef IN_LIGHTNNlm_complex_C_H  
  friend class lightNNint;
#endif  
  friend class light3lm_complex;
  friend class light4lm_complex;
  friend class lightNlm_complex;
  friend class light33lm_complex;
  friend class light44lm_complex;
  friend class lightN3lm_complex;
  friend class lightNNNlm_complex;
  friend class lightNNNNlm_complex;
  friend class lightN33lm_complex;

  friend inline lightNNlm_complex light_join (const lightNNlm_complex, const lightNNlm_complex);
  friend inline lightNNlm_complex light_drop (const lightNNlm_complex arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNdouble::lightNNlm_complex(const lightNNint& s1, const double e, const lightmat_plus_enum)
  lm_complex *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.


protected:
  lm_complex sarea[LIGHTNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNNlm_complex(const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // two arguments is the size of the created matrix. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNlm_complex(const lightNNlm_complex&, const lightNNlm_complex&, const lightmat_plus_enum);
  lightNNlm_complex(const lightNNlm_complex&, const lm_complex, const lightmat_plus_enum);

#ifdef IN_LIGHTNNdouble_C_H
  lightNNlm_complex(const lightNNdouble&, const int, const lightmat_plus_enum);
  lightNNlm_complex(const lightNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNlm_complex(const lightNNlm_complex&, const lightNNlm_complex&, const lightmat_minus_enum);
  lightNNlm_complex(const lm_complex, const lightNNlm_complex&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNdouble_C_H
  lightNNlm_complex(const int, const lightNNdouble&, const lightmat_minus_enum);
  lightNNlm_complex(const double, const lightNNint&, const lightmat_minus_enum);
#endif

  lightNNlm_complex(const lightNNlm_complex&, const lightNNlm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const lightNNlm_complex&, const lm_complex, const lightmat_mult_enum);

#ifdef IN_LIGHTNNdouble_C_H
  lightNNlm_complex(const lightNNdouble&, const int, const lightmat_mult_enum);
  lightNNlm_complex(const lightNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNlm_complex(const lightNNlm_complex&, const light33lm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const lightNNlm_complex&, const light44lm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const lightN3lm_complex&, const lightNNlm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const light33lm_complex&, const lightNNlm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const light44lm_complex&, const lightNNlm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const lightNlm_complex&, const lightNNNlm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const lightNNNlm_complex&, const lightNlm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const light3lm_complex&, const lightNNNlm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const lightNNNlm_complex&, const light3lm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const light4lm_complex&, const lightNNNlm_complex&, const lightmat_mult_enum);
  lightNNlm_complex(const lightNNNlm_complex&, const light4lm_complex&, const lightmat_mult_enum);

  lightNNlm_complex(const lm_complex, const lightNNlm_complex&, const lightmat_div_enum);
#ifdef IN_LIGHTNNdouble_C_H
  lightNNlm_complex(const int, const lightNNdouble&, const lightmat_div_enum);
  lightNNlm_complex(const double, const lightNNint&, const lightmat_div_enum);
#endif

  lightNNlm_complex(const lightNNlm_complex&, const lightNNlm_complex&, const lightmat_pow_enum);
  lightNNlm_complex(const lightNNlm_complex&, const lm_complex, const lightmat_pow_enum);
  lightNNlm_complex(const lm_complex, const lightNNlm_complex&, const lightmat_pow_enum);
  //lightNNlm_complex(const lightNNlm_complex&, const lightmat_abs_enum);

#ifdef IN_LIGHTNNlm_complex_C_H
 #else
  #ifdef IN_LIGHTNNdouble_C_H
   lightNNlm_complex(const lightNNlm_complex&, const lightmat_abs_enum);
   lightNNlm_complex(const lightNNlm_complex&, const lightmat_abs_enum);
  #else
  lightNNlm_complex(const lightNNlm_complex&, const lightmat_abs_enum);
  #endif
#endif

  lightNNlm_complex(const lightNNlm_complex&, const lightNNlm_complex&, const lightmat_eprod_enum);
  lightNNlm_complex(const lightNNlm_complex&, const lightNNlm_complex&, const lightmat_equot_enum);
  lightNNlm_complex(const lightNNlm_complex&, lm_complex f(lm_complex), const lightmat_apply_enum);
  lightNNlm_complex(const lightNNlm_complex&, const lightNNlm_complex&, lm_complex f(lm_complex, lm_complex), const lightmat_apply_enum);
  lightNNlm_complex(const lightN3lm_complex&, const lightmat_trans_enum);
  lightNNlm_complex(const lightNlm_complex&, const lightNlm_complex&, const lightmat_outer_enum);

};

typedef lightNNdouble doubleNN;
typedef lightNNint intNN;
typedef lightNNlm_complex lm_complexNN;

#undef IN_LIGHTNNlm_complex_C_H


#endif
