//           -*- c++ -*-

#define IN_BASICint_I_H
//
#ifdef COMPLEX_TOOLS

inline double LIGHTABS(const lm_complex m);
#else

inline int LIGHTABS(const int m);
#endif

//
// Basic routines with simple operations
//


inline void light_assign(const int n, const int *a, int *b);


inline void light_assign(const int i0, const int i1, const int *a, int *b);


inline void light_plus(const int n, const int *a, const int *b, int *c);


inline void light_minus(const int n, const int *a, const int *b, int *c);


inline void light_mult(const int n, const int *a, const int *b, int *c);


inline void light_divide(const int n, const int *a, const int *b, int *c);


inline void light_assign(const int n, const int a, int *b);


inline void light_assign(const int i0, const int i1, const int a, int *b);


inline void light_plus(const int n, const int a, const int *b, int *c);

#ifdef IN_BASICdouble_I_H
inline void light_plus(const int n, const double a, const int *b, double *c);
inline void light_plus(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_I_H
inline void light_plus(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_plus(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_plus(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_plus(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_minus(const int n, const int a, const int *b, int *c);

#ifdef IN_BASICdouble_I_H
inline void light_minus(const int n, const double a, const int *b, double *c);
inline void light_minus(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_I_H
inline void light_minus(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_minus(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_minus(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_minus(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_mult(const int n, const int a, const int *b, int *c);

#ifdef IN_BASICdouble_I_H
inline void light_mult(const int n, const double a, const int *b, double *c);
inline void light_mult(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_I_H
inline void light_mult(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_mult(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_mult(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_mult(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_divide(const int n, const int a, const int *b, int *c);

#ifdef IN_BASICdouble_I_H
inline void light_divide(const int n, const double a, const int *b, double *c);
inline void light_divide(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_I_H
inline void light_divide(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_divide(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_divide(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_divide(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_plus_same(const int n, int *a, const int *b);


inline void light_minus_same(const int n, int *a, const int *b);


inline void light_mult_same(const int n, int *a, const int *b);


inline void light_plus_same(const int n, int *a, const int b);


inline void light_mult_same(const int n, int *a, const int b);


inline int light_pow(int a, int b);


#ifdef IN_BASICdouble_I_H
inline double light_atan2(double a, double b);
#endif

#ifdef IN_BASIClm_complex_I_H
inline lm_complex light_atan2(lm_complex a, lm_complex b);
#endif

//
// Basic routines for vector and matrix calculation
//

// Dot product
//
// n : length of vectors
// x : vector 1
// y : vector 2
//

 inline int light_dot(const int n, const int *x, const int *y);

// Dot product
//
// n  : length of vectors
// x  : vector 1
// ix : step between elements in x
// y  : vector 2
//

 inline int light_dot(const int n,const int *x,
		    const int ix,const int *y);

// Matrix-vector product
//
// m : rows of matrix
// n : columns of matrix and no. elements in vector
// a : matrix
// x : vector
// y : result vector (length m)

inline void light_gemv(const int m, const int n,
		       const int *a, const int *x, int *y);

// Vector-matrix product
//
// m : rows of matrix and no. elements in vector
// n : columns of matrix
// x : vector
// a : matrix
// y : result vector (length n)

inline void light_gevm(const int m, const int n,
		       const int *x, const int *a, int *y);

// Vector-matrix product
//
// m  : rows of matrix and no. elements in vector
// n  : columns of matrix
// x  : vector
// a  : matrix
// y  : result vector (length n)
// iy : step between elements in y

inline void light_gevm(const int m, const int n,
		       const int *x, const int *a, int *y,
		const int iy);

// Matrix-Matrix product
//
// m : rows of matrix a and c
// n : columns of matrix b and c
// k : columns of matrix a and rows of matrix b
// a : matrix 1
// b : matrix 2
// c : result matrix

inline void light_gemm(const int m, const int n,
		       const int k, const int *a,
		       const int *b, int *c);

// Tensor3-Vector product
//
// m : size1 of a and rows of c
// n : size2 of a and columns of c
// k : size3 of a and no. elements in b
// a : tensor3
// b : vector
// c : result matrix

inline void light_ge3v(const int m, const int n,
		       const int k, const int *a,
		       const int *b, int *c);

// Vector-Tensor3 product
//
// m : size2 of b and rows of c
// n : size3 of a and columns of c
// k : no. elements in a and size1 of b
// a : vector
// b : tensor3
// c : result matrix

inline void light_gev3(const int m, const int n,
		       const int k, const int *a,
		       const int *b, int *c);

// Tensor3-Matrix product
//
// m : size1 of a and c
// n : size2 of a and c
// o : columns of b and size3 of c
// k : size3 of a and rows of b
// a : tensor3
// b : matrix
// c : result tensor3

inline void light_ge3m(const int m, const int n,
		       const int o, const int k,
		       const int *a, const int *b, int *c);

// Matrix-Tensor3 product
//
// m : rows of a and size1 of c
// n : size2 of b and c
// o : size3 of b and c
// k : columns of a and size1 of b
// a : matrix
// b : tensor3
// c : result tensor3

inline void light_gem3(const int m, const int n,
		       const int o, const int k,
		       const int *a, const int *b, int *c);

// Tensor3-Tensor3 product
//
// m : size1 of a c
// n : size2 of a and c
// o : size2 of b and size3 of c
// p : size3 of b and size4 of c
// k : size3 of a and size1 of b
// a : tensor3
// b : tensor3
// c : result tensor4

inline void light_ge33(const int m, const int n,
		       const int o, const int p,
		       const int k, const int *a,
		       const int *b, int *c);

// Tensor4-Vector product
//
// m : size1 of a and c
// n : size2 of a and c
// o : size3 of a and c
// k : size4 of a and no. elements in b
// a : tensor4
// b : vector
// c : result tensor3

inline void light_ge4v(const int m, const int n,
		       const int o, const int k,
		       const int *a, const int *b, int *c);

// Vector-Tensor4 product
//
// m : size2 of b and size1 of c
// n : size3 of b and size2 of c
// o : size4 of b and size3 of c
// k : no. elements in a and size1 of b
// a : vector
// b : tensor4
// c : result tensor3

inline void light_gev4(const int m, const int n,
		       const int o, const int k,
		       const int *a, const int *b, int *c);

// Tensor4-Matrix product
//
// m : size1 of a and c
// n : size2 of a and c
// o : size3 of a and c
// p : columns of b and size4 of c
// k : size4 of a and rows of b
// a : tensor4
// b : matrix
// c : result tensor4

inline void light_ge4m(const int m, const int n,
		       const int o, const int p,
		       const int k, const int *a,
		       const int *b, int *c);

// Matrix-Tensor4 product
//
// m : rows of a and size1 of c
// n : size2 of b and c
// o : size3 of b and c
// p : size4 of b and c
// k : columns of a and size1 of b
// a : matrix
// b : tensor4
// c : result tensor4

inline void light_gem4(const int m, const int n,
		       const int o, const int p,
		       const int k, const int *a,
		       const int *b, int *c);



inline lightNint light_join (const lightNint arr1, const lightNint arr2);


inline lightNNint light_join (const lightNNint arr1, const lightNNint arr2);


inline lightNNNint light_join (const lightNNNint arr1, const lightNNNint arr2);


inline lightNNNNint light_join (const lightNNNNint arr1, const lightNNNNint arr2);


inline lightNint light_append (const lightNint arr, const int el);


inline lightNNint light_append (const lightNNint arr, const lightNint el);


inline lightNNNint light_append (const lightNNNint arr, const lightNNint el);


inline lightNNNNint light_append (const lightNNNNint arr, const lightNNNint el);


inline lightNint light_prepend (const lightNint arr, const int el);


inline lightNNint light_prepend (const lightNNint arr, const lightNint el);


inline lightNNNint light_prepend (const lightNNNint arr, const lightNNint el);


inline lightNNNNint light_prepend (const lightNNNNint arr, const lightNNNint el);


inline lightNint light_drop (const lightNint arr, const R r);


inline lightNNint light_drop (const lightNNint arr, const R r);


inline lightNNNint light_drop (const lightNNNint arr, const R r);


inline lightNNNNint light_drop (const lightNNNNint arr, const R r);


//
// Join for N
//


inline lightNint Join (const lightNint arr1, const lightNint arr2);


inline lightNint Join (const light3int arr1, const lightNint arr2);


inline lightNint Join (const light4int arr1, const lightNint arr2);


inline lightNint Join (const light3int arr1, const light3int arr2);


inline lightNint Join (const light4int arr1, const light3int arr2);


inline lightNint Join (const lightNint arr1, const light3int arr2);


inline lightNint Join (const light3int arr1, const light4int arr2);


inline lightNint Join (const light4int arr1, const light4int arr2);


inline lightNint Join (const lightNint arr1, const light4int arr2);


//
// Join for NN
//


inline lightNNint Join (const lightNNint arr1, const lightNNint arr2);


inline lightNNint Join (const light33int arr1, const lightNNint arr2);


inline lightNNint Join (const light44int arr1, const lightNNint arr2);


inline lightNNint Join (const light33int arr1, const light33int arr2);


inline lightNNint Join (const light44int arr1, const light33int arr2);


inline lightNNint Join (const lightNNint arr1, const light33int arr2);


inline lightNNint Join (const light33int arr1, const light44int arr2);


inline lightNNint Join (const light44int arr1, const light44int arr2);


inline lightNNint Join (const lightNNint arr1, const light44int arr2);

//
// Join for NNN
//


inline lightNNNint Join (const lightNNNint arr1, const lightNNNint arr2);

//
// Join for NNNN
//


inline lightNNNNint Join (const lightNNNNint arr1, const lightNNNNint arr2);


//
// Append
//


inline lightNint Append (const light3int arr, const int el);


inline lightNint Append (const light4int arr, const int el);


inline lightNint Append (const lightNint arr, const int el);


inline lightNNint Append (const light33int arr, const light3int el);


inline lightNNint Append (const light44int arr, const light4int el);


inline lightNNint Append (const lightNNint arr, const lightNint el);


inline lightNNNint Append (const lightNNNint arr, const lightNNint el);


inline lightNNNNint Append (const lightNNNNint arr, const lightNNNint el);

//
// Prepend
//


inline lightNint Prepend (const light3int arr, const int el);


inline lightNint Prepend (const light4int arr, const int el);


inline lightNint Prepend (const lightNint arr, const int el);


inline lightNNint Prepend (const light33int arr, const light3int el);


inline lightNNint Prepend (const light44int arr, const light4int el);


inline lightNNint Prepend (const lightNNint arr, const lightNint el);


inline lightNNNint Prepend (const lightNNNint arr, const lightNNint el);


inline lightNNNNint Prepend (const lightNNNNint arr, const lightNNNint el);

//
// Drop
//


inline lightNint Drop (const lightNint arr, const R r);


inline lightNint Drop (const light3int arr, const R r);


inline lightNint Drop (const light4int arr, const R r);


inline lightNNint Drop (const lightNNint arr, const R r);


inline lightNNint Drop (const light33int arr, const R r);


inline lightNNint Drop (const light44int arr, const R r);


inline lightNNNint Drop (const lightNNNint arr, const R r);


inline lightNNNNint Drop (const lightNNNNint arr, const R r);


#undef IN_BASICint_I_H
