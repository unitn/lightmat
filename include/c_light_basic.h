//           -*- c++ -*-

#define IN_BASIClm_complex_C_H
//
#ifdef COMPLEX_TOOLS

inline double LIGHTABS(const lm_complex m);
#else

inline lm_complex LIGHTABS(const lm_complex m);
#endif

//
// Basic routines with simple operations
//


inline void light_assign(const int n, const lm_complex *a, lm_complex *b);


inline void light_assign(const int i0, const int i1, const lm_complex *a, lm_complex *b);


inline void light_plus(const int n, const lm_complex *a, const lm_complex *b, lm_complex *c);


inline void light_minus(const int n, const lm_complex *a, const lm_complex *b, lm_complex *c);


inline void light_mult(const int n, const lm_complex *a, const lm_complex *b, lm_complex *c);


inline void light_divide(const int n, const lm_complex *a, const lm_complex *b, lm_complex *c);


inline void light_assign(const int n, const lm_complex a, lm_complex *b);


inline void light_assign(const int i0, const int i1, const lm_complex a, lm_complex *b);


inline void light_plus(const int n, const lm_complex a, const lm_complex *b, lm_complex *c);

#ifdef IN_BASICdouble_C_H
inline void light_plus(const int n, const double a, const int *b, double *c);
inline void light_plus(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_C_H
inline void light_plus(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_plus(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_plus(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_plus(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_minus(const int n, const lm_complex a, const lm_complex *b, lm_complex *c);

#ifdef IN_BASICdouble_C_H
inline void light_minus(const int n, const double a, const int *b, double *c);
inline void light_minus(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_C_H
inline void light_minus(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_minus(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_minus(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_minus(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_mult(const int n, const lm_complex a, const lm_complex *b, lm_complex *c);

#ifdef IN_BASICdouble_C_H
inline void light_mult(const int n, const double a, const int *b, double *c);
inline void light_mult(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_C_H
inline void light_mult(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_mult(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_mult(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_mult(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_divide(const int n, const lm_complex a, const lm_complex *b, lm_complex *c);

#ifdef IN_BASICdouble_C_H
inline void light_divide(const int n, const double a, const int *b, double *c);
inline void light_divide(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_C_H
inline void light_divide(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_divide(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_divide(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_divide(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_plus_same(const int n, lm_complex *a, const lm_complex *b);


inline void light_minus_same(const int n, lm_complex *a, const lm_complex *b);


inline void light_mult_same(const int n, lm_complex *a, const lm_complex *b);


inline void light_plus_same(const int n, lm_complex *a, const lm_complex b);


inline void light_mult_same(const int n, lm_complex *a, const lm_complex b);


inline lm_complex light_pow(lm_complex a, lm_complex b);


#ifdef IN_BASICdouble_C_H
inline double light_atan2(double a, double b);
#endif

#ifdef IN_BASIClm_complex_C_H
inline lm_complex light_atan2(lm_complex a, lm_complex b);
#endif

//
// Basic routines for vector and matrix calculation
//

// Dot product
//
// n : length of vectors
// x : vector 1
// y : vector 2
//

 inline lm_complex light_dot(const int n, const lm_complex *x, const lm_complex *y);

// Dot product
//
// n  : length of vectors
// x  : vector 1
// ix : step between elements in x
// y  : vector 2
//

 inline lm_complex light_dot(const int n,const lm_complex *x,
		    const int ix,const lm_complex *y);

// Matrix-vector product
//
// m : rows of matrix
// n : columns of matrix and no. elements in vector
// a : matrix
// x : vector
// y : result vector (length m)

inline void light_gemv(const int m, const int n,
		       const lm_complex *a, const lm_complex *x, lm_complex *y);

// Vector-matrix product
//
// m : rows of matrix and no. elements in vector
// n : columns of matrix
// x : vector
// a : matrix
// y : result vector (length n)

inline void light_gevm(const int m, const int n,
		       const lm_complex *x, const lm_complex *a, lm_complex *y);

// Vector-matrix product
//
// m  : rows of matrix and no. elements in vector
// n  : columns of matrix
// x  : vector
// a  : matrix
// y  : result vector (length n)
// iy : step between elements in y

inline void light_gevm(const int m, const int n,
		       const lm_complex *x, const lm_complex *a, lm_complex *y,
		const int iy);

// Matrix-Matrix product
//
// m : rows of matrix a and c
// n : columns of matrix b and c
// k : columns of matrix a and rows of matrix b
// a : matrix 1
// b : matrix 2
// c : result matrix

inline void light_gemm(const int m, const int n,
		       const int k, const lm_complex *a,
		       const lm_complex *b, lm_complex *c);

// Tensor3-Vector product
//
// m : size1 of a and rows of c
// n : size2 of a and columns of c
// k : size3 of a and no. elements in b
// a : tensor3
// b : vector
// c : result matrix

inline void light_ge3v(const int m, const int n,
		       const int k, const lm_complex *a,
		       const lm_complex *b, lm_complex *c);

// Vector-Tensor3 product
//
// m : size2 of b and rows of c
// n : size3 of a and columns of c
// k : no. elements in a and size1 of b
// a : vector
// b : tensor3
// c : result matrix

inline void light_gev3(const int m, const int n,
		       const int k, const lm_complex *a,
		       const lm_complex *b, lm_complex *c);

// Tensor3-Matrix product
//
// m : size1 of a and c
// n : size2 of a and c
// o : columns of b and size3 of c
// k : size3 of a and rows of b
// a : tensor3
// b : matrix
// c : result tensor3

inline void light_ge3m(const int m, const int n,
		       const int o, const int k,
		       const lm_complex *a, const lm_complex *b, lm_complex *c);

// Matrix-Tensor3 product
//
// m : rows of a and size1 of c
// n : size2 of b and c
// o : size3 of b and c
// k : columns of a and size1 of b
// a : matrix
// b : tensor3
// c : result tensor3

inline void light_gem3(const int m, const int n,
		       const int o, const int k,
		       const lm_complex *a, const lm_complex *b, lm_complex *c);

// Tensor3-Tensor3 product
//
// m : size1 of a c
// n : size2 of a and c
// o : size2 of b and size3 of c
// p : size3 of b and size4 of c
// k : size3 of a and size1 of b
// a : tensor3
// b : tensor3
// c : result tensor4

inline void light_ge33(const int m, const int n,
		       const int o, const int p,
		       const int k, const lm_complex *a,
		       const lm_complex *b, lm_complex *c);

// Tensor4-Vector product
//
// m : size1 of a and c
// n : size2 of a and c
// o : size3 of a and c
// k : size4 of a and no. elements in b
// a : tensor4
// b : vector
// c : result tensor3

inline void light_ge4v(const int m, const int n,
		       const int o, const int k,
		       const lm_complex *a, const lm_complex *b, lm_complex *c);

// Vector-Tensor4 product
//
// m : size2 of b and size1 of c
// n : size3 of b and size2 of c
// o : size4 of b and size3 of c
// k : no. elements in a and size1 of b
// a : vector
// b : tensor4
// c : result tensor3

inline void light_gev4(const int m, const int n,
		       const int o, const int k,
		       const lm_complex *a, const lm_complex *b, lm_complex *c);

// Tensor4-Matrix product
//
// m : size1 of a and c
// n : size2 of a and c
// o : size3 of a and c
// p : columns of b and size4 of c
// k : size4 of a and rows of b
// a : tensor4
// b : matrix
// c : result tensor4

inline void light_ge4m(const int m, const int n,
		       const int o, const int p,
		       const int k, const lm_complex *a,
		       const lm_complex *b, lm_complex *c);

// Matrix-Tensor4 product
//
// m : rows of a and size1 of c
// n : size2 of b and c
// o : size3 of b and c
// p : size4 of b and c
// k : columns of a and size1 of b
// a : matrix
// b : tensor4
// c : result tensor4

inline void light_gem4(const int m, const int n,
		       const int o, const int p,
		       const int k, const lm_complex *a,
		       const lm_complex *b, lm_complex *c);



inline lightNlm_complex light_join (const lightNlm_complex arr1, const lightNlm_complex arr2);


inline lightNNlm_complex light_join (const lightNNlm_complex arr1, const lightNNlm_complex arr2);


inline lightNNNlm_complex light_join (const lightNNNlm_complex arr1, const lightNNNlm_complex arr2);


inline lightNNNNlm_complex light_join (const lightNNNNlm_complex arr1, const lightNNNNlm_complex arr2);


inline lightNlm_complex light_append (const lightNlm_complex arr, const lm_complex el);


inline lightNNlm_complex light_append (const lightNNlm_complex arr, const lightNlm_complex el);


inline lightNNNlm_complex light_append (const lightNNNlm_complex arr, const lightNNlm_complex el);


inline lightNNNNlm_complex light_append (const lightNNNNlm_complex arr, const lightNNNlm_complex el);


inline lightNlm_complex light_prepend (const lightNlm_complex arr, const lm_complex el);


inline lightNNlm_complex light_prepend (const lightNNlm_complex arr, const lightNlm_complex el);


inline lightNNNlm_complex light_prepend (const lightNNNlm_complex arr, const lightNNlm_complex el);


inline lightNNNNlm_complex light_prepend (const lightNNNNlm_complex arr, const lightNNNlm_complex el);


inline lightNlm_complex light_drop (const lightNlm_complex arr, const R r);


inline lightNNlm_complex light_drop (const lightNNlm_complex arr, const R r);


inline lightNNNlm_complex light_drop (const lightNNNlm_complex arr, const R r);


inline lightNNNNlm_complex light_drop (const lightNNNNlm_complex arr, const R r);


//
// Join for N
//


inline lightNlm_complex Join (const lightNlm_complex arr1, const lightNlm_complex arr2);


inline lightNlm_complex Join (const light3lm_complex arr1, const lightNlm_complex arr2);


inline lightNlm_complex Join (const light4lm_complex arr1, const lightNlm_complex arr2);


inline lightNlm_complex Join (const light3lm_complex arr1, const light3lm_complex arr2);


inline lightNlm_complex Join (const light4lm_complex arr1, const light3lm_complex arr2);


inline lightNlm_complex Join (const lightNlm_complex arr1, const light3lm_complex arr2);


inline lightNlm_complex Join (const light3lm_complex arr1, const light4lm_complex arr2);


inline lightNlm_complex Join (const light4lm_complex arr1, const light4lm_complex arr2);


inline lightNlm_complex Join (const lightNlm_complex arr1, const light4lm_complex arr2);


//
// Join for NN
//


inline lightNNlm_complex Join (const lightNNlm_complex arr1, const lightNNlm_complex arr2);


inline lightNNlm_complex Join (const light33lm_complex arr1, const lightNNlm_complex arr2);


inline lightNNlm_complex Join (const light44lm_complex arr1, const lightNNlm_complex arr2);


inline lightNNlm_complex Join (const light33lm_complex arr1, const light33lm_complex arr2);


inline lightNNlm_complex Join (const light44lm_complex arr1, const light33lm_complex arr2);


inline lightNNlm_complex Join (const lightNNlm_complex arr1, const light33lm_complex arr2);


inline lightNNlm_complex Join (const light33lm_complex arr1, const light44lm_complex arr2);


inline lightNNlm_complex Join (const light44lm_complex arr1, const light44lm_complex arr2);


inline lightNNlm_complex Join (const lightNNlm_complex arr1, const light44lm_complex arr2);

//
// Join for NNN
//


inline lightNNNlm_complex Join (const lightNNNlm_complex arr1, const lightNNNlm_complex arr2);

//
// Join for NNNN
//


inline lightNNNNlm_complex Join (const lightNNNNlm_complex arr1, const lightNNNNlm_complex arr2);


//
// Append
//


inline lightNlm_complex Append (const light3lm_complex arr, const lm_complex el);


inline lightNlm_complex Append (const light4lm_complex arr, const lm_complex el);


inline lightNlm_complex Append (const lightNlm_complex arr, const lm_complex el);


inline lightNNlm_complex Append (const light33lm_complex arr, const light3lm_complex el);


inline lightNNlm_complex Append (const light44lm_complex arr, const light4lm_complex el);


inline lightNNlm_complex Append (const lightNNlm_complex arr, const lightNlm_complex el);


inline lightNNNlm_complex Append (const lightNNNlm_complex arr, const lightNNlm_complex el);


inline lightNNNNlm_complex Append (const lightNNNNlm_complex arr, const lightNNNlm_complex el);

//
// Prepend
//


inline lightNlm_complex Prepend (const light3lm_complex arr, const lm_complex el);


inline lightNlm_complex Prepend (const light4lm_complex arr, const lm_complex el);


inline lightNlm_complex Prepend (const lightNlm_complex arr, const lm_complex el);


inline lightNNlm_complex Prepend (const light33lm_complex arr, const light3lm_complex el);


inline lightNNlm_complex Prepend (const light44lm_complex arr, const light4lm_complex el);


inline lightNNlm_complex Prepend (const lightNNlm_complex arr, const lightNlm_complex el);


inline lightNNNlm_complex Prepend (const lightNNNlm_complex arr, const lightNNlm_complex el);


inline lightNNNNlm_complex Prepend (const lightNNNNlm_complex arr, const lightNNNlm_complex el);

//
// Drop
//


inline lightNlm_complex Drop (const lightNlm_complex arr, const R r);


inline lightNlm_complex Drop (const light3lm_complex arr, const R r);


inline lightNlm_complex Drop (const light4lm_complex arr, const R r);


inline lightNNlm_complex Drop (const lightNNlm_complex arr, const R r);


inline lightNNlm_complex Drop (const light33lm_complex arr, const R r);


inline lightNNlm_complex Drop (const light44lm_complex arr, const R r);


inline lightNNNlm_complex Drop (const lightNNNlm_complex arr, const R r);


inline lightNNNNlm_complex Drop (const lightNNNNlm_complex arr, const R r);


#undef IN_BASIClm_complex_C_H
