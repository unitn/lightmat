//           -*- c++ -*-

#ifndef LIGHTN33_C_H
#define LIGHTN33_C_H
// <cd> lightN33lm_complex
//
// .SS Functionality
//
// lightN33 is a template for classes that implement tensors
// of size Nx3x3
//
// <code>lightN33&lt;double&gt; v(5);</code>
//
// The value of the first index can change during execution. It
// changes its size to whatever is needed when it is assigned a new
// value with an assignment operator.
//
// When a lightN33 object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightN33 object with 5x3x3 elements to one with 7x3x3 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#define LIGHTN33_SIZE 10


class lightN33lm_complex {
public:
  lightN33lm_complex();
  // Default constructor.

  lightN33lm_complex(const lightN33lm_complex&);
  // Copy constructor.

  lightN33lm_complex(const int n);
  // Construct a lightN33 with size nx3x3

  lightN33lm_complex(const int n, const lm_complex *);
  // Construct a lightN33 with size nx3x3 and initialize the
  // elements with values from an array (in row major order).
  
  lightN33lm_complex(const int n, const lm_complex);
  // Construct a lightN33 with size nx3x3 and initialize the
  // elements with a value (the second argument).

  ~lightN33lm_complex();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightN33double();
  // Convert to double
#else
  friend class lightN33int;  
#endif

  lightN33lm_complex& operator=(const lightN33lm_complex&);
  // Assignment.

  lightN33lm_complex& operator=(const lightNNNlm_complex&);
  // Assignment from a lightNNN with size Nx3x3.

  lightN33lm_complex& operator=(const lm_complex);
  // Assign one value to all elements.

  lm_complex operator()(const int, const int, const int) const;
  // Get the value of one element.

  lm_complex& operator()(const int, const int, const int);
  // Get/Set the value of one element.

  light3lm_complex operator()(const int, const int) const;
  // Get the value of a vector.

  const light33lm_complex& operator()(const int) const;
  // Get one 3x3 matrix.
  
  light33lm_complex& operator()(const int);
  // Get/Set one 3x3 matrix.
 
  int operator==(const lightN33lm_complex&) const;
  // Equality.

  int operator!=(const lightN33lm_complex&) const;
  // Inequality.

  lightN33lm_complex& operator+=(const lm_complex);
  // Add a value to all elements.

  lightN33lm_complex& operator+=(const lightN33lm_complex&);
  // Elementwise addition.

  lightN33lm_complex& operator-=(const lm_complex);
  // Subtract a value from all elements.

  lightN33lm_complex& operator-=(const lightN33lm_complex&);
  // Elementwise subtraction.

  lightN33lm_complex& operator*=(const lm_complex);
  // Mulitply all elements with a value.

  lightN33lm_complex& operator/=(const lm_complex);
  // Divide all elements with a value.

  lightN33lm_complex& reshape(const int, const int, const int, const lightNlm_complex& s);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c) will be set to s(a) in the lightN33lm_complex
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector and the second and third
  // argument must be 3. The program may dump core or behave strangely
  // if the arguments are incorrect.

  lightN33lm_complex& reshape(const int, const int, const int, const lightNNlm_complex& s);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c) will be set to s(a,b) in the
  // lightN33 object. The value of the first argument must be the same
  // as the number of rows in the lightNN-matrix and the second and
  // third argument must be 3. The number of columns in the
  // lightNN-matrix must also be 3. The program may dump core or
  // behave strangely if the arguments are incorrect.

  int dimension(const int) const;
  // Get size of some dimension (dimension(2) == 3 and dimension(3) ==
  // 3 for lightN3).

  void Get(lm_complex *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const lm_complex *);
  // Set values of all elements from array (row major order).

  lightN3lm_complex operator+() const;
  // Unary plus.

  lightN3lm_complex operator-() const;
  // Unary minus.

  friend inline lightN33lm_complex Apply(const lightN33lm_complex&, lm_complex f(lm_complex));
  // Apply the function elementwise all elements.

  friend inline lightN33lm_complex Apply(const lightN33lm_complex&, const lightN33lm_complex&, lm_complex f(lm_complex, lm_complex));
  // Apply the function elementwise on all elements in the two tensors.

  //<ignore>
  friend class light3lm_complex;
  friend class lightNlm_complex;
  friend class lightN3lm_complex;
  friend class lightNNlm_complex;
  friend class lightNNNlm_complex;
  //</ignore>
  
protected:
//FOOFOO    light33lm_complex sarea[LIGHTN33_SIZE];

  light33lm_complex *elem;
  // The matrixes are stored here.

  int size;
  // The size of the tensor.

  int alloc_size;
  // The size of the allocated area.

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  lightN33lm_complex(const int x, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // argument is the size of the tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  lightN33lm_complex(const lightN33lm_complex&, lm_complex f(lm_complex), const lightmat_apply_enum);
  lightN33lm_complex(const lightN33lm_complex&, const lightN33lm_complex&, lm_complex f(lm_complex, lm_complex), const lightmat_apply_enum);
  lightN33lm_complex(const lightN33lm_complex&, const lightmat_trans_enum);
};

typedef lightN33double doubleN33;
typedef lightN33int intN33;

#endif
