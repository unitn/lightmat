//           -*- c++ -*-

#ifdef LIGHTMAT_OUTPUT_FUNCS

#ifndef IN_OUTPUTdouble
#define IN_OUTPUTdouble

#include "rw/cstring.h"

//
// ToStr
//
#ifndef __RWCSTRING_H__
inline RWCString
ToStr(const double d);

inline RWCString
ToStr(const int d);

inline RWCString
ToStr(const lm_complex & d);
#endif


inline RWCString
ToStr(const lightNdouble& s);


inline RWCString
ToStr(const lightNNdouble& s, int depth=0);


inline RWCString
ToStr(const lightNNNdouble& s, int depth=0);


inline RWCString
ToStr(const lightNNNNdouble& s, int depth=0);


inline RWCString
light_export(const char* file, const lightNdouble& s, const char* format, const char* option, const char* optionval);


inline RWCString
light_export(const char* file, const lightNNdouble& s, const char* format, const char* option, const char* optionval);



inline RWCString
light_export1(const char* file, const lightNdouble& s, const char* option, const char* optionval);



inline RWCString
light_export1(const char* file, const lightNNdouble& s, const char* option, const char* optionval);

//
// operator<<
//

inline ostream& operator<<(ostream& o, const lm_complex& s);


inline ostream&
operator<<(ostream& o, const light3double& s);


inline ostream&
operator<<(ostream& o, const light4double& s);


inline ostream&
operator<<(ostream& o, const lightNdouble& s);


inline ostream&
operator<<(ostream& o, const light33double& s);


inline ostream&
operator<<(ostream& o, const light44double& s);


inline ostream&
operator<<(ostream& o, const lightN3double& s);


inline ostream&
operator<<(ostream& o, const lightNNdouble& s);


inline ostream&
operator<<(ostream& o, const lightNNNdouble& s);


inline ostream&
operator<<(ostream& o, const lightN33double& s);


inline ostream&
operator<<(ostream& o, const lightNNNNdouble& s);



inline lightNdouble 
light_importNdouble(const char* filename, const char* format, const char* opt1, const char* opt2);


inline lightNNdouble 
light_importNNdouble(const char* filename, const char* format, const char* opt1, const char* opt2);



inline lightNdouble 
light_import1Ndouble(const char* file);


inline lightNNdouble 
light_import1NNdouble(const char* file);

#endif

#endif // LIGHTMAT_OUTPUT_FUNCS
