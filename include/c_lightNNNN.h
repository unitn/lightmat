//           -*- c++ -*-

#ifndef LIGHTNNNN_C_H
#define LIGHTNNNN_C_H
// <cd> lightNNNlm_complex
//
// .SS Functionality
//
// lightNNNN is a template for classes that implement tensors of rank 4
// with any number of elements. E.g. a lightNNNN s with 5x6x7x8 elements of
// type double can be instanciated with:
//
// <code>lightNNNN&lt;double&gt; s(5,6,7,8);</code>
//
// The size of the tensor can change during execution. It changes its
// size to whatever is needed when it is assigned a new value with an
// assignment operator.
//
// When a lightNNNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNNN object with 5x6x5x5 elements to one with 5x5x6x5 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHTNNNNlm_complex_C_H


#define LIGHTNNNN_SIZE1 4
#define LIGHTNNNN_SIZE2 4
#define LIGHTNNNN_SIZE3 4
#define LIGHTNNNN_SIZE4 4
#define LIGHTNNNN_SIZE (LIGHTNNNN_SIZE1*LIGHTNNNN_SIZE2*LIGHTNNNN_SIZE3*LIGHTNNNN_SIZE4)
// The default size.

 class lightN;
 class lightNN;
 class lightNNN;


class lightNNNNlm_complex {
public:

#ifdef IN_LIGHTNNNNdouble_C_H
  friend class lightNNNNint;
#else
  friend class lightNNNNdouble;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNNNint;
#endif

  #include "c_lightNNNN_auto.h"

  inline lightNNNNlm_complex();
  // Default constructor.

  inline lightNNNNlm_complex(const lightNNNNlm_complex&);
  // Copy constructor.

  inline lightNNNNlm_complex(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNNNlm_complex(const int, const int, const int, const int);
  // Construct a lightNNNN of given size.

  inline lightNNNNlm_complex(const int, const int, const int, const int, const lm_complex *);
  // Construct a lightNNNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNNNlm_complex(const int, const int, const int, const int, const lm_complex);
  // Construct a lightNNNN of given size and initialize all elements
  // with a value (the last argument).

  inline ~lightNNNNlm_complex();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNNNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNNNlm_complex();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNNNlm_complex();
  // Convert to complex.
#endif

  lightNNNNlm_complex& operator=(const lightNNNNlm_complex&);
  // Assignment.

  lightNNNNlm_complex& operator=(const lm_complex);
  // Assign one value to all elements.

  //
  // Operator ().
  //
  
  lm_complex operator()(const int x, const int y, const int z, const int v) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3) || (v<1) || (v>size4));
#ifdef ROWMAJOR
    return elem[(((x-1)*size2+(y-1))*size3+(z-1))*size4+v-1];
#else
    return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
#endif
  };
  // Get the value of one element.


  lm_complex& operator()(const int x, const int y, const int z, const int v) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3) || (v<1) || (v>size4));
#ifdef ROWMAJOR
    return elem[(((x-1)*size2+(y-1))*size3+(z-1))*size4+v-1];
#else
    return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
#endif
  };
  // Get/Set the value of one element.

  inline lightNlm_complex operator()(const int, const int, const int) const;
  // Get the value of a vector in the tensor.

  inline lightNNlm_complex operator()(const int, const int) const;
  // Get the value of a matrix in the tensor.

  inline lightNNNlm_complex operator()(const int) const;
  // Get the value of a tensor of rank 3 in the tensor.


  //
  // Set
  //

  inline lightNNNNlm_complex& Set (const int i0, const int i1, const int i2,
			    const lm_complex val);
  inline lightNNNNlm_complex& Set (const int i0, const int i1, const int i2,
			    const lightNlm_complex& arr);
  inline lightNNNNlm_complex& Set (const int i0, const int i1, const int i2,
			    const light3lm_complex& arr);
  inline lightNNNNlm_complex& Set (const int i0, const int i1, const int i2,
			    const light4lm_complex& arr);
  inline lightNNNNlm_complex& Set (const int i0, const int i1, const lm_complex val);
  inline lightNNNNlm_complex& Set (const int i0, const int i1, const lightNNlm_complex& arr);
  inline lightNNNNlm_complex& Set (const int i0, const int i1, const light33lm_complex& arr);
  inline lightNNNNlm_complex& Set (const int i0, const int i1, const light44lm_complex& arr);
  inline lightNNNNlm_complex& Set (const int i0, const lm_complex val);
  inline lightNNNNlm_complex& Set (const int i0, const lightNNNlm_complex& arr);

 
  int operator==(const lightNNNNlm_complex&) const;
  // Equality.

  int operator!=(const lightNNNNlm_complex&) const;
  // Inequality.
 
  lightNNNNlm_complex& operator+=(const lm_complex);
  // Add a value to all elements.

  lightNNNNlm_complex& operator+=(const lightNNNNlm_complex&);
  // Elementwise addition.

  lightNNNNlm_complex& operator-=(const lm_complex);
  // Subtract a value from all elements.

  lightNNNNlm_complex& operator-=(const lightNNNNlm_complex&);
  // Elementwise subtraction.

  lightNNNNlm_complex& operator*=(const lm_complex);
  // Mulitply all elements with a value.

  lightNNNNlm_complex& operator/=(const lm_complex);
  // Divide all elements with a value.

  lightNNNNlm_complex& SetShape(const int x=-1, const int y=-1, 
			 const int z=-1, const int v=-1);
  // Sets specified shape to the tensor. 

  lm_complex Extract(const lightNint&) const;
  // Extract an element using given index. 

  lightNNNNlm_complex& reshape(const int, const int, const int, const int, const lightNlm_complex&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c,d) will be set to s(a) in the lightNNNNlm_complex
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNNlm_complex& reshape(const int, const int, const int, const int, const lightNNlm_complex&);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c,d) will be set to s(a,b) in the
  // lightNNNN object. The values of the first two arguments must be
  // the same as the size of the lightNN-matrix. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNNlm_complex& reshape(const int, const int, const int, const int, const lightNNNlm_complex&);
  // Convert the lightNNN-tensor to the given size and put the result
  // in this object. (*this)(a,b,c,d) will be set to s(a,b,c) in the
  // lightNNNN object. The values of the first three arguments must be
  // the same as the size of the lightNNN-tensor. The program may dump
  // core or behave strangely if the arguments are incorrect.

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else if(x == 3)
      return size3;
    else if(x == 4)
      return size4;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(lm_complex *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const lm_complex *);
  // Set values of all elements from array (row major order).
 
  lm_complex * data() const;
  // Direct access to the stored data. Use the result with care. 

  lightNNNNlm_complex operator+() const;
  // Unary plus.

  lightNNNNlm_complex operator-() const;
  // Unary minus.


  friend inline lightNNNNlm_complex operator+(const lightNNNNlm_complex&, const lightNNNNlm_complex&);
  // Elementwise addition.

  friend inline lightNNNNlm_complex operator+(const lightNNNNlm_complex&, const lm_complex);
  // Addition to all elements.

  friend inline lightNNNNlm_complex operator+(const lm_complex, const lightNNNNlm_complex&);
  // Addition to all elements.

#ifdef IN_LIGHTNNNNdouble_C_H
  friend inline lightNNNNdouble operator+(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator+(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator+(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator+(const int, const lightNNNNdouble&);
#endif

  #ifdef IN_LIGHTNNNNlm_complex_C_H
friend inline lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator+(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator+(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNNlm_complex operator-(const lightNNNNlm_complex&, const lightNNNNlm_complex&);
  // Elementwise subtraction.

  friend inline lightNNNNlm_complex operator-(const lightNNNNlm_complex&, const lm_complex);
  // Subtraction from all elements.

  friend inline lightNNNNlm_complex operator-(const lm_complex, const lightNNNNlm_complex&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNNNdouble_C_H
  friend inline lightNNNNdouble operator-(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator-(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator-(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator-(const int, const lightNNNNdouble&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_C_H
friend inline lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator-(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator-(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNNlm_complex operator*(const lightNNNNlm_complex&, const lm_complex);
  // Multiply all elements.

  friend inline lightNNNNlm_complex operator*(const lm_complex, const lightNNNNlm_complex&);
  // Multiply all elements.

#ifdef IN_LIGHTNNNNdouble_C_H
  friend inline lightNNNNdouble operator*(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator*(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator*(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator*(const int, const lightNNNNdouble&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_C_H
friend inline lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator*(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator*(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNNlm_complex operator*(const lightNNNNlm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline lightNNNNlm_complex operator*(const lightNNlm_complex&, const lightNNNNlm_complex&);
  // Inner product.

  friend inline lightNNNNlm_complex operator*(const lightNNNNlm_complex&, const light33lm_complex&);
  // Inner product.

  friend inline lightNNNNlm_complex operator*(const light33lm_complex&, const lightNNNNlm_complex&);
  // Inner product.

  friend inline lightNNNNlm_complex operator*(const lightNNNNlm_complex&, const light44lm_complex&);
  // Inner product.

  friend inline lightNNNNlm_complex operator*(const light44lm_complex&, const lightNNNNlm_complex&);
  // Inner product.

  friend inline lightNNNNlm_complex operator*(const lightNNNlm_complex&, const lightNNNlm_complex&);
  // Inner product.

  friend inline lightNNNNlm_complex operator/(const lightNNNNlm_complex&, const lm_complex);
  // Divide all elements.

  friend inline lightNNNNlm_complex operator/(const lm_complex, const lightNNNNlm_complex&);
  // Divide all elements.

#ifdef IN_LIGHTNNNNdouble_C_H
  friend inline lightNNNNdouble operator/(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator/(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator/(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator/(const int, const lightNNNNdouble&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_C_H
friend inline lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator/(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator/(const double e, const lightNNNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNNNlm_complex_C_H
friend inline lightNNNNdouble arg(const lightNNNNlm_complex& a);
friend inline lightNNNNdouble re(const lightNNNNlm_complex& a);
friend inline lightNNNNdouble im(const lightNNNNlm_complex& a);
friend inline lightNNNNlm_complex conjugate(const lightNNNNlm_complex& a);
//#endif

  friend inline lightNNNNlm_complex pow(const lightNNNNlm_complex&, const lightNNNNlm_complex&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNNNlm_complex pow(const lightNNNNlm_complex&, const lm_complex);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNNlm_complex pow(const lm_complex, const lightNNNNlm_complex&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNNlm_complex ElemProduct(const lightNNNNlm_complex&, const lightNNNNlm_complex&);
  // Elementwise multiplication.

  friend inline lightNNNNlm_complex ElemQuotient(const lightNNNNlm_complex&, const lightNNNNlm_complex&);
  // Elementwise division.

  friend inline lightNNNNlm_complex Apply(const lightNNNNlm_complex&, lm_complex f(lm_complex));
  // Apply the function elementwise all elements.

  friend inline lightNNNNlm_complex Apply(const lightNNNNlm_complex&, const lightNNNNlm_complex&, lm_complex f(lm_complex, lm_complex));
  // Apply the function elementwise on all elements in the two tensors.

  friend inline lightNNNNlm_complex Transpose(const lightNNNNlm_complex&);
  // Transpose.

  
#ifdef IN_LIGHTNNNNlm_complex_C_H
#else
  friend inline lightNNNNlm_complex abs(const lightNNNNlm_complex&);
#ifdef IN_LIGHTNNNNdouble_C_H
  friend inline lightNNNNdouble abs(const lightNNNNlm_complex&);
#endif 
#endif


  // abs

#ifndef COMPLEX_TOOLS
  friend inline lightNNNNint sign(const lightNNNNint&);
  // sign

  friend inline lightNNNNint sign(const lightNNNNdouble&);
  // sign
#else
  friend inline lightNNNNlm_complex sign(const lightNNNNlm_complex&);
#endif


/// Added 2/2/98 

  friend inline lightNNNNlm_complex  FractionalPart(const lightNNNNlm_complex&);
  //  FractionalPart

  friend inline lightNNNNlm_complex  IntegerPart(const lightNNNNlm_complex&);
  //  IntegerPart
 
  //friend inline lightNNNNint  IntegerPart(const lightNNNNdouble&);
  //  IntegerPart

  friend inline lightNNNNlm_complex Mod (const  lightNNNNlm_complex&,const  lightNNNNlm_complex&);
  // Mod
 
  friend inline lightNNNNlm_complex Mod (const  lightNNNNlm_complex&,const lm_complex);
  // Mod

  friend inline lightNNNNlm_complex Mod (const lm_complex,const  lightNNNNlm_complex&);
  // Mod

  friend inline lm_complex LightMax (const lightNNNNlm_complex& );
  // Max

  friend inline lm_complex LightMin (const lightNNNNlm_complex& );
  // Min

  friend inline lm_complex findLightMax (const  lm_complex *,const  int );
  // Find Max
 
  friend inline lm_complex findLightMin (const  lm_complex *,const  int );
  // Find Min
 
  /// End Added 

  friend inline lightNNNNint ifloor(const lightNNNNdouble&);
  // ifloor

  friend inline lightNNNNint iceil(const lightNNNNdouble&);
  // iceil

  friend inline lightNNNNint irint(const lightNNNNdouble&);
  // irint

  friend inline lightNNNNdouble sqrt(const lightNNNNdouble&);
  // sqrt

  friend inline lightNNNNdouble exp(const lightNNNNdouble&);
  // exp

  friend inline lightNNNNdouble log(const lightNNNNdouble&);
  // log

  friend inline lightNNNNdouble sin(const lightNNNNdouble&);
  // sin

  friend inline lightNNNNdouble cos(const lightNNNNdouble&);
  // cos

  friend inline lightNNNNdouble tan(const lightNNNNdouble&);
  // tan

  friend inline lightNNNNdouble asin(const lightNNNNdouble&);
  // asin

  friend inline lightNNNNdouble acos(const lightNNNNdouble&);
  // acos

  friend inline lightNNNNdouble atan(const lightNNNNdouble&);
  // atan

  friend inline lightNNNNdouble sinh(const lightNNNNdouble&);
  // sinh

  friend inline lightNNNNdouble cosh(const lightNNNNdouble&);
  // cosh

  friend inline lightNNNNdouble tanh(const lightNNNNdouble&);
  // tanh

  friend inline lightNNNNdouble asinh(const lightNNNNdouble&);
  // asinh

  friend inline lightNNNNdouble acosh(const lightNNNNdouble&);
  // acosh

  friend inline lightNNNNdouble atanh(const lightNNNNdouble&);
  // atanh

  friend inline lightNNNNlm_complex ifloor(const lightNNNNlm_complex&);
  // ifloor

  friend inline lightNNNNlm_complex iceil(const lightNNNNlm_complex&);
  // iceil

  friend inline lightNNNNlm_complex irint(const lightNNNNlm_complex&);
  // irint

  friend inline lightNNNNlm_complex sqrt(const lightNNNNlm_complex&);
  // sqrt

  friend inline lightNNNNlm_complex exp(const lightNNNNlm_complex&);
  // exp

  friend inline lightNNNNlm_complex log(const lightNNNNlm_complex&);
  // log

  friend inline lightNNNNlm_complex sin(const lightNNNNlm_complex&);
  // sin

  friend inline lightNNNNlm_complex cos(const lightNNNNlm_complex&);
  // cos

  friend inline lightNNNNlm_complex tan(const lightNNNNlm_complex&);
  // tan

  friend inline lightNNNNlm_complex asin(const lightNNNNlm_complex&);
  // asin

  friend inline lightNNNNlm_complex acos(const lightNNNNlm_complex&);
  // acos

  friend inline lightNNNNlm_complex atan(const lightNNNNlm_complex&);
  // atan

  friend inline lightNNNNlm_complex sinh(const lightNNNNlm_complex&);
  // sinh

  friend inline lightNNNNlm_complex cosh(const lightNNNNlm_complex&);
  // cosh

  friend inline lightNNNNlm_complex tanh(const lightNNNNlm_complex&);
  // tanh

  friend inline lightNNNNlm_complex asinh(const lightNNNNlm_complex&);
  // asinh

  friend inline lightNNNNlm_complex acosh(const lightNNNNlm_complex&);
  // acosh

  friend inline lightNNNNlm_complex atanh(const lightNNNNlm_complex&);
  // atanh

#ifdef IN_LIGHTNNNNint_C_H
  friend inline lightNNNdouble light_mean     ( const  lightNNNNlm_complex& );
  friend inline lightNNNdouble light_standard_deviation( const  lightNNNNlm_complex& );
  friend inline lightNNNdouble light_variance ( const  lightNNNNlm_complex& );
  friend inline lightNNNdouble light_median   ( const  lightNNNNlm_complex& );
#else
  friend inline lightNNNlm_complex light_mean     ( const  lightNNNNlm_complex& );
  friend inline lightNNNlm_complex light_variance ( const  lightNNNNlm_complex& );
  friend inline lightNNNlm_complex light_standard_deviation( const  lightNNNNlm_complex& );
  friend inline lightNNNlm_complex light_median   ( const  lightNNNNlm_complex& );
#endif
  
  friend inline lightNNNlm_complex light_total    ( const  lightNNNNlm_complex& );

  friend inline lightNNNNlm_complex light_sort (const lightNNNNlm_complex arr1);
  
  friend inline lightNlm_complex light_flatten (const lightNNNNlm_complex s);
  friend inline lightNlm_complex light_flatten (const lightNNNNlm_complex s, int level);
  friend inline lightNNNNlm_complex light_flatten0 (const lightNNNNlm_complex s);
  friend inline lightNNNlm_complex light_flatten1 (const lightNNNNlm_complex s);
  friend inline lightNNlm_complex light_flatten2 (const lightNNNNlm_complex s);

  //<ignore>
#ifdef IN_LIGHTNNNNlm_complex_C_H  
  friend class lightNNNNint;
#endif  
  friend class lightNlm_complex;
  friend class lightNNlm_complex;
  friend class lightNNNlm_complex;

  friend inline lightNNNNlm_complex light_join (const lightNNNNlm_complex, const lightNNNNlm_complex);
  friend inline lightNNNNlm_complex light_drop (const lightNNNNlm_complex arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNNNdouble caNNNot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNNNdouble::lightNNNNlm_complex(const lightNNNNint& s1, const double e, const lightmat_plus_enum)

  lm_complex *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // Size of the first index.

  int size2;
  // Size of the second index.

  int size3;
  // Size of the third index.

  int size4;
  // Size of the fourth index.


protected:
  lm_complex sarea[LIGHTNNNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNNNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  lightNNNNlm_complex(const int, const int, const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // four arguments is the size of the created tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNNNlm_complex(const lightNNNNlm_complex&, const lightNNNNlm_complex&, const lightmat_plus_enum);
  lightNNNNlm_complex(const lightNNNNlm_complex&, const lm_complex, const lightmat_plus_enum);

#ifdef IN_LIGHTNNNNdouble_C_H
  lightNNNNlm_complex(const lightNNNNdouble&, const int, const lightmat_plus_enum);
  lightNNNNlm_complex(const lightNNNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNNNlm_complex(const lightNNNNlm_complex&, const lightNNNNlm_complex&, const lightmat_minus_enum);
  lightNNNNlm_complex(const lm_complex, const lightNNNNlm_complex&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNNNdouble_C_H
  lightNNNNlm_complex(const int, const lightNNNNdouble&, const lightmat_minus_enum);
  lightNNNNlm_complex(const double, const lightNNNNint&, const lightmat_minus_enum);
#endif

  lightNNNNlm_complex(const lightNNNNlm_complex&, const lm_complex, const lightmat_mult_enum);
#ifdef IN_LIGHTNNNNdouble_C_H
  lightNNNNlm_complex(const lightNNNNdouble&, const int, const lightmat_mult_enum);
  lightNNNNlm_complex(const lightNNNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNNNlm_complex(const lightNNNNlm_complex&, const lightNNlm_complex&, const lightmat_mult_enum);
  lightNNNNlm_complex(const lightNNlm_complex&, const lightNNNNlm_complex&, const lightmat_mult_enum);
  lightNNNNlm_complex(const lightNNNNlm_complex&, const light33lm_complex&, const lightmat_mult_enum);
  lightNNNNlm_complex(const light33lm_complex&, const lightNNNNlm_complex&, const lightmat_mult_enum);
  lightNNNNlm_complex(const lightNNNNlm_complex&, const light44lm_complex&, const lightmat_mult_enum);
  lightNNNNlm_complex(const light44lm_complex&, const lightNNNNlm_complex&, const lightmat_mult_enum);
  lightNNNNlm_complex(const lightNNNlm_complex&, const lightNNNlm_complex&, const lightmat_mult_enum);

  lightNNNNlm_complex(const lm_complex, const lightNNNNlm_complex&, const lightmat_div_enum);
#ifdef IN_LIGHTNNNNdouble_C_H
  lightNNNNlm_complex(const int, const lightNNNNdouble&, const lightmat_div_enum);
  lightNNNNlm_complex(const double, const lightNNNNint&, const lightmat_div_enum);
#endif

  lightNNNNlm_complex(const lightNNNNlm_complex&, const lightNNNNlm_complex&, const lightmat_pow_enum);
  lightNNNNlm_complex(const lightNNNNlm_complex&, const lm_complex, const lightmat_pow_enum);
  lightNNNNlm_complex(const lm_complex, const lightNNNNlm_complex&, const lightmat_pow_enum);
 

#ifdef IN_LIGHTNNNNlm_complex_C_H
 #else
  #ifdef IN_LIGHTNNNNdouble_C_H
   lightNNNNlm_complex(const lightNNNNlm_complex&, const lightmat_abs_enum);
   lightNNNNlm_complex(const lightNNNNlm_complex&, const lightmat_abs_enum);
  #else
  lightNNNNlm_complex(const lightNNNNlm_complex&, const lightmat_abs_enum);
  #endif
#endif


  lightNNNNlm_complex(const lightNNNNlm_complex&, const lightNNNNlm_complex&, const lightmat_eprod_enum);
  lightNNNNlm_complex(const lightNNNNlm_complex&, const lightNNNNlm_complex&, const lightmat_equot_enum);
  lightNNNNlm_complex(const lightNNNNlm_complex&, lm_complex f(lm_complex), const lightmat_apply_enum);
  lightNNNNlm_complex(const lightNNNNlm_complex&, const lightNNNNlm_complex&, lm_complex f(lm_complex, lm_complex), const lightmat_apply_enum);
  lightNNNNlm_complex(const lightNNNNlm_complex&, const lightmat_trans_enum);
};

typedef lightNNNNdouble doubleNNNN;
typedef lightNNNNint intNNNN;
typedef lightNNNNlm_complex lm_complexNNNN;

#undef IN_LIGHTNNNNlm_complex_C_H

#endif
