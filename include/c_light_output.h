//           -*- c++ -*-

#ifdef LIGHTMAT_OUTPUT_FUNCS

#ifndef IN_OUTPUTlm_complex
#define IN_OUTPUTlm_complex

#include "rw/cstring.h"

//
// ToStr
//
#ifndef __RWCSTRING_C_H__
inline RWCString
ToStr(const double d);

inline RWCString
ToStr(const int d);

inline RWCString
ToStr(const lm_complex & d);
#endif


inline RWCString
ToStr(const lightNlm_complex& s);


inline RWCString
ToStr(const lightNNlm_complex& s, int depth=0);


inline RWCString
ToStr(const lightNNNlm_complex& s, int depth=0);


inline RWCString
ToStr(const lightNNNNlm_complex& s, int depth=0);


inline RWCString
light_export(const char* file, const lightNlm_complex& s, const char* format, const char* option, const char* optionval);


inline RWCString
light_export(const char* file, const lightNNlm_complex& s, const char* format, const char* option, const char* optionval);



inline RWCString
light_export1(const char* file, const lightNlm_complex& s, const char* option, const char* optionval);



inline RWCString
light_export1(const char* file, const lightNNlm_complex& s, const char* option, const char* optionval);

//
// operator<<
//

inline ostream& operator<<(ostream& o, const lm_complex& s);


inline ostream&
operator<<(ostream& o, const light3lm_complex& s);


inline ostream&
operator<<(ostream& o, const light4lm_complex& s);


inline ostream&
operator<<(ostream& o, const lightNlm_complex& s);


inline ostream&
operator<<(ostream& o, const light33lm_complex& s);


inline ostream&
operator<<(ostream& o, const light44lm_complex& s);


inline ostream&
operator<<(ostream& o, const lightN3lm_complex& s);


inline ostream&
operator<<(ostream& o, const lightNNlm_complex& s);


inline ostream&
operator<<(ostream& o, const lightNNNlm_complex& s);


inline ostream&
operator<<(ostream& o, const lightN33lm_complex& s);


inline ostream&
operator<<(ostream& o, const lightNNNNlm_complex& s);



inline lightNlm_complex 
light_importNlm_complex(const char* filename, const char* format, const char* opt1, const char* opt2);


inline lightNNlm_complex 
light_importNNlm_complex(const char* filename, const char* format, const char* opt1, const char* opt2);



inline lightNlm_complex 
light_import1Nlm_complex(const char* file);


inline lightNNlm_complex 
light_import1NNlm_complex(const char* file);

#endif

#endif // LIGHTMAT_OUTPUT_FUNCS
