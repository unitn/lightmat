//           -*- c++ -*-

#ifndef MATHDEFS_H
#define MATHDEFS_H

#define LEFT -1
#define RIGHT 1
#define NEAREST 0

#define False 0
#define True 1

// To be able to get C arrays from 1 to length 
#define CL(length) ((length + 1))            

#define even(x) ((x)%2 == 0 ? 1 : 0)
#define odd(x) ((x)%2 != 0 ? 1 : 0)

#define Sign(x) ((x) < 0 ? -1 : 1)
// The sign function. Returns -1 for negative numbers, otherwise +1.
// Note it returns +1 for zero! This behaviour is utilized.

//  #define sign(x) ((x) < 0 ? -1 : 1)
//  Vadim Removed this one !

#define NRCSign(a, b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
// --------- sign  -------------
//#ifndef signUndefined 
// int sign( int x);
// int sign( double x);
//#endif





// --------- Min(a, b) functions for base types -------------

// Extra (is this necessary?  >Problem because of rint returns double)
inline double Min(const int x, const double y) {
  return ((double) ( (x < y) ? x : y ));
}

inline double Min(const double x, const int y) {
  return ((double) ( (x < y) ? x : y ));
}


inline int Min(const int x, const int y) {
  return( (x < y) ? x : y );
}

inline unsigned int Min(const unsigned int x, const unsigned int y) {
  return( (x < y) ? x : y );
}
 
inline long Min(const long x, const long y) {
  return( (x < y) ? x : y );
}

inline unsigned long Min(const unsigned long x, const unsigned long y) {
  return( (x < y) ? x : y );
}

inline float Min(const float x, const float y) {
  return( (x < y) ? x : y );
}

inline double Min(const double x, const double y) {
	 return( (x < y) ? x : y );
}

// --------- Min(a, b, c) functions for base types -------------

inline int Min(const int x, const int y, const int z) {
    return( (x < y) ? (x < z ? x : z) : (y < z ? y : z));
}

inline unsigned int Min(const unsigned int x, const unsigned int y, const unsigned int z) {
   return( (x < y) ? (x < z ? x : z) : (y < z ? y : z));
}

inline long Min(const long x, const long y, const long z) {
    return( (x < y) ? (x < z ? x : z) : (y < z ? y : z));
}

inline unsigned long Min(const unsigned long x, const unsigned long y, const unsigned long z) {
   return( (x < y) ? (x < z ? x : z) : (y < z ? y : z));
}

inline float Min(const float x, const float y, const float z) {
    return( (x < y) ? (x < z ? x : z) : (y < z ? y : z));
}

inline double Min(const double x, const double y, const double z) {
    return( (x < y) ? (x < z ? x : z) : (y < z ? y : z));
}

// --------- Min(a, b, c) functions for base types -------------

inline int Min(const int w, const int x, const int y, const int z) {
    return Min(Min(w, x), Min(y, z));
}

inline unsigned int Min(const unsigned int w, const unsigned int x, 
			const unsigned int y, const unsigned int z) {
    return Min(Min(w, x), Min(y, z));
}

inline long Min(const long w, const long x, const long y, const long z) {
    return Min(Min(w, x), Min(y, z));
}

inline unsigned long Min(const unsigned long w, const unsigned long x, 
			 const unsigned long y, const unsigned long z) {
   return Min(Min(w, x), Min(y, z));
}

inline float Min(const float w, const float x, const float y, const float z) {
    return Min(Min(w, x), Min(y, z));
}

inline double Min(const double w, const double x, const double y, const double z) {
    return Min(Min(w, x), Min(y, z));
}

// --------- Max(a, b) functions for base types -------------

// Extra (is this necessary?  >Problem because of rint returns double)

inline double Max(const int x, const double y) {
	 return((double) ((x > y) ? x : y ));
}

inline double Max(const double x, const int y) {
	 return((double) ((x > y) ? x : y ));
}

inline int Max(const int x, const int y) {
	 return( (x > y) ? x : y );
}

inline unsigned int Max(const unsigned int x, const unsigned int y) {
	 return( (x > y) ? x : y );
}

inline long Max(const long x, const long y) {
	 return( (x > y) ? x : y );
}

inline unsigned long Max(const unsigned long x, const unsigned long y) {
	 return( (x > y) ? x : y );
}

inline float Max(const float x, const float y) {
	 return( (x > y) ? x : y );
}

inline double Max(const double x, const double y) {
	 return( (x > y) ? x : y );
}

// --------- Max(a, b, c) functions for base types -------------

inline int Max(const int x, const int y, const int z) {
    return( (x > y) ? (x > z ? x : z) : (y > z ? y : z));
}

inline unsigned int Max(const unsigned int x, const unsigned int y, const unsigned int z) {
    return( (x > y) ? (x > z ? x : z) : (y > z ? y : z));
}

inline long Max(const long x, const long y, const long z) {
    return( (x > y) ? (x > z ? x : z) : (y > z ? y : z));
}

inline unsigned long Max(const unsigned long x, const unsigned long y, const unsigned long z) {
    return( (x > y) ? (x > z ? x : z) : (y > z ? y : z));
}

inline float Max(const float x, const float y, const float z) {
    return( (x > y) ? (x > z ? x : z) : (y > z ? y : z));
}

inline double Max(const double x, const double y, const double z) {
    return( (x > y) ? (x > z ? x : z) : (y > z ? y : z));
}

// --------- Max(a, b, c, d) functions for base types -------------

inline int Max(const int w, const int x, const int y, const int z) {
    return Max(Max(w, x), Max(y, z));
}

inline unsigned int Max(const unsigned int w, const unsigned int x, 
			const unsigned int y, const unsigned int z) {
    return Max(Max(w, x), Max(y, z));
}

inline long Max(const long w, const long x, const long y, const long z) {
    return Max(Max(w, x), Max(y, z));
}

inline unsigned long Max(const unsigned long w, const unsigned long x, 
			 const unsigned long y, const unsigned long z) {
    return Max(Max(w, x), Max(y, z));
}

inline float Max(const float w, const float x, const float y, const float z) {
    return Max(Max(w, x), Max(y, z));
}

inline double Max(const double w, const double x, const double y, const double z) {
    return Max(Max(w, x), Max(y, z));
}

// --------- special double functions -----------------------

inline int sameSide(double x, double y, double z, double w) {
    if ( (x >= w && y >= w && z >= w) ||
	(x <= w && y <= w && z <= w) ) {
	return ( 1 );
    }
    else {
	return ( 0 );
    }
}

inline int sameSign(double x, double y, double w = 0.0) {
    if ( (x >= w && y >= w) || (x <= w && y <= w) ) {
	return ( 1 );
    }
    else {
	return ( 0 );
    }
}

#if 0

// We can not assume that compare operators are declared
// for all objects, therefore we build Min and Max functions
// only for base types.
// For other objects write special Min, Max class member functions.   

template <class Type>
inline Type Min(Type x, Type y) {
	 return( (x < y) ? x : y );
}

template <class Type>
inline Type Min(Type x, Type y, Type z) {
    return( (x < y) ? (x < z ? x : z) : (y < z ? y : z));
}

template <class Type>
inline Type Min(Type x1, Type x2, Type x3, Type x4) {
    return( Min(Min(x1, x2), Min(x3, x4)));
}
 
template <class Type>
inline Type Max(Type x, Type y) {
	 return( (x > y) ? x : y );
}

template <class Type>
inline Type Max(Type x, Type y, Type z) {
	 return( (x > y) ? (x > z ? x : z) : (y > z ? y : z));
}

template <class Type>
inline Type Max(Type x1, Type x2, Type x3, Type x4) {
	 return( Max(Max(x1, x2), Max(x3, x4)));
}

template <class Type>
inline int sameSide(Type x, Type y, Type z, Type w) {
	 if ( (x >= w && y >= w && z >= w) ||
	(x <= w && y <= w && z <= w) ) {
	return ( 1 );
	 }
	 else {
	return ( 0 );
	 }
}

template <class Type>
inline int sameSign(Type x, Type y, Type w) {
    if ( (x >= w && y >= w) || (x <= w && y <= w) ) {
	return ( 1 );
    }
    else {
	return ( 0 );
    }
}
#endif


//#define E		2.71828182845904523536029
//#define Pi		3.14159265358979323846264
//#define Degree	0.01745329251994329576924

const double E      = 2.71828182845904523536029;
const double pi     = 3.14159265358979323846264;
const double pi_2   = 3.14159265358979323846264 / 2;
const double Degree = 0.01745329251994329576924;

// A very small positive number.
// It should be possible to multiply it by itself without
// obtaining total underflow, i.e. value zero.
// So we choose sqrt(1.0e-300)=1.0e-150.
//#define smallPositiveNumber 1.0e-150
const double smallPositiveNumber = 1.0e-150;

const char nl = '\n';

// Used by slicefor.cc, matrixin.cc
//#define INFINITY       1.0e12
//#define TOLERANCE      1.0e-9
//#define INFINITESIMAL  1.0e-12

#if 0
// These are left from Beast compatibility mode, not in use anymore.

// use OUR "INFINITY" on macintosh / windows codewarrior
// as well as on g++ (3.3+ or may be even some earlier)
#if defined(macintosh) || defined (MWCC) || defined(__USE_ISOC99)
#undef INFINITY
#endif

const double INFINITY      = 1.0e12;
const double TOLERANCE     = 1.0e-9;
const double INFINITESIMAL = 1.0e-12;

#endif

#endif
