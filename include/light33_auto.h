inline light33<T>& Set (const int i0, const int i1, const T val);
inline light33<T>& Set (const R r0, const int i1, const lightN<T>& arr);
inline light33<T>& Set (const R r0, const int i1, const light3<T>& arr);
inline light33<T>& Set (const R r0, const int i1, const light4<T>& arr);
inline light33<T>& Set (const R r0, const int i1, const T val);
inline light33<T>& Set (const int i0, const R r1, const lightN<T>& arr);
inline light33<T>& Set (const int i0, const R r1, const light3<T>& arr);
inline light33<T>& Set (const int i0, const R r1, const light4<T>& arr);
inline light33<T>& Set (const int i0, const R r1, const T val);
inline light33<T>& Set (const R r0, const R r1, const lightNN<T>& arr);
inline light33<T>& Set (const R r0, const R r1, const light33<T>& arr);
inline light33<T>& Set (const R r0, const R r1, const light44<T>& arr);
inline light33<T>& Set (const R r0, const R r1, const T val);
inline lightN<T> operator() (const R r0, const int i1) const;
inline lightN<T> operator() (const int i0, const R r1) const;
inline lightNN<T> operator() (const R r0, const R r1) const;
friend inline light33double atan2 (const double e, const light33int &s1);
friend inline light33double atan2 (const double e, const light33double &s1);
friend inline light33double atan2 (const light33int &s1, const double e);
friend inline light33double atan2 (const light33double &s1, const double e);
friend inline light33double atan2 (const light33int &s1, const light33int &s2);
friend inline light33double atan2 (const light33int &s1, const light33double &s2);
friend inline light33double atan2 (const light33double &s1, const light33int &s2);
friend inline light33double atan2 (const light33double &s1, const light33double &s2);
friend inline light33double atan2 (const light33int &s1, const int e);
friend inline light33double atan2 (const light33double &s1, const int e);
friend inline light33double atan2 (const int e, const light33int &s1);
friend inline light33double atan2 (const int e, const light33double &s1);

#ifdef IN_LIGHT33double_H
inline light33double(const double e, const light33int &s1, const lightmat_atan2_enum);
inline light33double(const double e, const light33double &s1, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const double e, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const double e, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const light33int &s2, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const light33double &s2, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const light33int &s2, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const light33double &s2, const lightmat_atan2_enum);
inline light33double(const light33int &s1, const int e, const lightmat_atan2_enum);
inline light33double(const light33double &s1, const int e, const lightmat_atan2_enum);
inline light33double(const int e, const light33int &s1, const lightmat_atan2_enum);
inline light33double(const int e, const light33double &s1, const lightmat_atan2_enum);

#endif
