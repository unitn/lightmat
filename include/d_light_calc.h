//           -*- c++ -*-

#define IN_CALCdouble_H


//
// operator+
//

lightNdouble operator+(const lightNdouble& s1, const lightNdouble& s2);

//--

lightNdouble operator+(const lightNdouble& s1, const double e);


lightNdouble operator+(const double e, const lightNdouble& s2);

#ifdef IN_CALCdouble_H
lightNdouble operator+(const lightNdouble& s1, const int e);
lightNdouble operator+(const lightNint& s1, const double e);
lightNdouble operator+(const double e, const lightNint& s2);
lightNdouble operator+(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNlm_complex operator+(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator+(const lightNint& s1, const lm_complex e);
lightNlm_complex operator+(const lightNlm_complex& s1, const double e);
lightNlm_complex operator+(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator+(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator+(const int e, const lightNlm_complex& s2);
lightNlm_complex operator+(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator+(const double e, const lightNlm_complex& s2);
#endif

//--



lightN3double operator+(const lightN3double& s1, const lightN3double& s2);


lightN3double operator+(const lightN3double& s1, const double e);


lightN3double operator+(const double e, const lightN3double& s2);


lightNNdouble operator+(const lightNNdouble& s1, const lightNNdouble& s2);


lightNNdouble operator+(const lightNNdouble& s1, const double e);


lightNNdouble operator+(const double e, const lightNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNdouble operator+(const lightNNdouble& s1, const int e);
lightNNdouble operator+(const lightNNint& s1, const double e);
lightNNdouble operator+(const double e, const lightNNint& s2);
lightNNdouble operator+(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNlm_complex operator+(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator+(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator+(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator+(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator+(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator+(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator+(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator+(const double e, const lightNNlm_complex& s2);
#endif


lightNNNdouble operator+(const lightNNNdouble& s1, const lightNNNdouble& s2);


lightNNNdouble operator+(const lightNNNdouble& s1, const double e);


lightNNNdouble operator+(const double e, const lightNNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNNdouble operator+(const lightNNNdouble& s1, const int e);
lightNNNdouble operator+(const lightNNNint& s1, const double e);
lightNNNdouble operator+(const double e, const lightNNNint& s2);
lightNNNdouble operator+(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator+(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator+(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator+(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator+(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator+(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator+(const double e, const lightNNNlm_complex& s2);
#endif


lightNNNNdouble operator+(const lightNNNNdouble& s1, const lightNNNNdouble& s2);


lightNNNNdouble operator+(const lightNNNNdouble& s1, const double e);


lightNNNNdouble operator+(const double e, const lightNNNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNNNdouble operator+(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator+(const lightNNNNint& s1, const double e);
lightNNNNdouble operator+(const double e, const lightNNNNint& s2);
lightNNNNdouble operator+(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator+(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator+(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator+(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator+(const double e, const lightNNNNlm_complex& s2);
#endif

//
// operator-
//

lightNdouble operator-(const lightNdouble& s1, const lightNdouble& s2);


lightNdouble operator-(const lightNdouble& s1, const double e);


lightNdouble operator-(const double e, const lightNdouble& s2);

#ifdef IN_CALCdouble_H
lightNdouble operator-(const lightNdouble& s1, const int e);
lightNdouble operator-(const lightNint& s1, const double e);
lightNdouble operator-(const double e, const lightNint& s2);
lightNdouble operator-(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNlm_complex operator-(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator-(const lightNint& s1, const lm_complex e);
lightNlm_complex operator-(const lightNlm_complex& s1, const double e);
lightNlm_complex operator-(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator-(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator-(const int e, const lightNlm_complex& s2);
lightNlm_complex operator-(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator-(const double e, const lightNlm_complex& s2);
#endif


lightN3double operator-(const lightN3double& s1, const lightN3double& s2);


lightN3double operator-(const lightN3double& s1, const double e);


lightN3double operator-(const double e, const lightN3double& s2);


lightNNdouble operator-(const lightNNdouble& s1, const lightNNdouble& s2);


lightNNdouble operator-(const lightNNdouble& s1, const double e);


lightNNdouble operator-(const double e, const lightNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNdouble operator-(const lightNNdouble& s1, const int e);
lightNNdouble operator-(const lightNNint& s1, const double e);
lightNNdouble operator-(const double e, const lightNNint& s2);
lightNNdouble operator-(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNlm_complex operator-(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator-(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator-(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator-(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator-(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator-(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator-(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator-(const double e, const lightNNlm_complex& s2);
#endif


lightNNNdouble operator-(const lightNNNdouble& s1, const lightNNNdouble& s2);


lightNNNdouble operator-(const lightNNNdouble& s1, const double e);


lightNNNdouble operator-(const double e, const lightNNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNNdouble operator-(const lightNNNdouble& s1, const int e);
lightNNNdouble operator-(const lightNNNint& s1, const double e);
lightNNNdouble operator-(const double e, const lightNNNint& s2);
lightNNNdouble operator-(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator-(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator-(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator-(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator-(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator-(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator-(const double e, const lightNNNlm_complex& s2);

#endif



lightNNNNdouble operator-(const lightNNNNdouble& s1, const lightNNNNdouble& s2);


lightNNNNdouble operator-(const lightNNNNdouble& s1, const double e);


lightNNNNdouble operator-(const double e, const lightNNNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNNNdouble operator-(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator-(const lightNNNNint& s1, const double e);
lightNNNNdouble operator-(const double e, const lightNNNNint& s2);
lightNNNNdouble operator-(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator-(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator-(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator-(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator-(const double e, const lightNNNNlm_complex& s2);
#endif

//
// operator* (inner product)
//

lightNdouble operator*(const lightNdouble& s1, const double e);


lightNdouble operator*(const double e, const lightNdouble& s2);

#ifdef IN_CALCdouble_H
lightNdouble operator*(const lightNdouble& s1, const int e);
lightNdouble operator*(const lightNint& s1, const double e);
lightNdouble operator*(const double e, const lightNint& s2);
lightNdouble operator*(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNlm_complex operator*(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator*(const lightNint& s1, const lm_complex e);
lightNlm_complex operator*(const lightNlm_complex& s1, const double e);
lightNlm_complex operator*(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator*(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator*(const int e, const lightNlm_complex& s2);
lightNlm_complex operator*(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator*(const double e, const lightNlm_complex& s2);
#endif



lightNdouble operator*(const lightNdouble& s1, const lightNNdouble& s2);


lightNdouble operator*(const lightNNdouble& s1, const lightNdouble& s2);


lightNdouble operator*(const light3double& s1, const lightNNdouble& s2);


lightNdouble operator*(const light4double& s1, const lightNNdouble& s2);


lightNdouble operator*(const lightN3double& s1, const light3double& s2);


lightNdouble operator*(const lightN3double& s1, const lightNdouble& s2);


lightNdouble operator*(const lightNNdouble& s1, const light3double& s2);


lightNdouble operator*(const lightNNdouble& s1, const light4double& s2);


lightN3double operator*(const lightN3double& s1, const lightN3double& s2);


lightN3double operator*(const lightN3double& s1, const double e);


lightN3double operator*(const lightNNdouble& s1, const lightN3double& s2);


lightN3double operator*(const lightN3double& s1, const light33double& s2);


lightN3double operator*(const light44double& s1, const lightN3double& s2);


lightN3double operator*(const double e, const lightN3double& s2);


lightNNdouble operator*(const lightNNdouble& s1, const lightNNdouble& s2);


lightNNdouble operator*(const lightNNdouble& s1, const double e);


lightNNdouble operator*(const double e, const lightNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNdouble operator*(const lightNNdouble& s1, const int e);
lightNNdouble operator*(const lightNNint& s1, const double e);
lightNNdouble operator*(const double e, const lightNNint& s2);
lightNNdouble operator*(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNlm_complex operator*(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator*(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator*(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator*(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator*(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator*(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator*(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator*(const double e, const lightNNlm_complex& s2);
#endif


lightNNdouble operator*(const lightNNdouble& s1, const light33double& s2);


lightNNdouble operator*(const lightNNdouble& s1, const light44double& s2);


lightNNdouble operator*(const lightN3double& s1, const lightNNdouble& s2);


lightNNdouble operator*(const light33double& s1, const lightNNdouble& s2);


lightNNdouble operator*(const light44double& s1, const lightNNdouble& s2);


lightNNdouble operator*(const lightNdouble& s1, const lightNNNdouble& s2);


lightNNdouble operator*(const lightNNNdouble& s1, const lightNdouble& s2);


lightNNNdouble operator*(const lightNNNdouble& s1, const double e);


lightNNNdouble operator*(const double e, const lightNNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNNdouble operator*(const lightNNNdouble& s1, const int e);
lightNNNdouble operator*(const lightNNNint& s1, const double e);
lightNNNdouble operator*(const double e, const lightNNNint& s2);
lightNNNdouble operator*(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator*(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator*(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator*(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator*(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator*(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator*(const double e, const lightNNNlm_complex& s2);
#endif


lightNNNdouble operator*(const lightNNNdouble& s1, const lightNNdouble& s2);


lightNNNdouble operator*(const lightNNdouble& s1, const lightNNNdouble& s2);


lightNNNdouble operator*(const lightNNNNdouble& s1, const lightNdouble& s2);


lightNNNdouble operator*(const lightNdouble& s1, const lightNNNNdouble& s2);


lightNNNNdouble operator*(const lightNNNNdouble& s1, const double e);


lightNNNNdouble operator*(const double e, const lightNNNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNNNdouble operator*(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator*(const lightNNNNint& s1, const double e);
lightNNNNdouble operator*(const double e, const lightNNNNint& s2);
lightNNNNdouble operator*(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator*(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator*(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator*(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator*(const double e, const lightNNNNlm_complex& s2);
#endif


lightNNNNdouble operator*(const lightNNNNdouble& s1, const lightNNdouble& s2);


lightNNNNdouble operator*(const lightNNdouble& s1, const lightNNNNdouble& s2);


lightNNNNdouble operator*(const lightNNNdouble& s1, const lightNNNdouble& s2);

//
// operator/
//

lightNdouble operator/(const lightNdouble& s1, const double e);


lightNdouble operator/(const double e, const lightNdouble& s2);

#ifdef IN_CALCdouble_H
lightNdouble operator/(const lightNdouble& s1, const int e);
lightNdouble operator/(const lightNint& s1, const double e);
lightNdouble operator/(const double e, const lightNint& s2);
lightNdouble operator/(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNlm_complex operator/(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator/(const lightNint& s1, const lm_complex e);
lightNlm_complex operator/(const lightNlm_complex& s1, const double e);
lightNlm_complex operator/(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator/(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator/(const int e, const lightNlm_complex& s2);
lightNlm_complex operator/(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator/(const double e, const lightNlm_complex& s2);
#endif


lightN3double operator/(const lightN3double& s1, const double e);


lightN3double operator/(const double e, const lightN3double& s2);


lightNNdouble operator/(const lightNNdouble& s1, const double e);


lightNNdouble operator/(const double e, const lightNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNdouble operator/(const lightNNdouble& s1, const int e);
lightNNdouble operator/(const lightNNint& s1, const double e);
lightNNdouble operator/(const double e, const lightNNint& s2);
lightNNdouble operator/(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNlm_complex operator/(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator/(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator/(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator/(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator/(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator/(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator/(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator/(const double e, const lightNNlm_complex& s2);
#endif


lightNNNdouble operator/(const lightNNNdouble& s1, const double e);


lightNNNdouble operator/(const double e, const lightNNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNNdouble operator/(const lightNNNdouble& s1, const int e);
lightNNNdouble operator/(const lightNNNint& s1, const double e);
lightNNNdouble operator/(const double e, const lightNNNint& s2);
lightNNNdouble operator/(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALClm_complex_H
//lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator/(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator/(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator/(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator/(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator/(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator/(const double e, const lightNNNlm_complex& s2);
#endif


lightNNNNdouble operator/(const lightNNNNdouble& s1, const double e);


lightNNNNdouble operator/(const double e, const lightNNNNdouble& s2);

#ifdef IN_CALCdouble_H
lightNNNNdouble operator/(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator/(const lightNNNNint& s1, const double e);
lightNNNNdouble operator/(const double e, const lightNNNNint& s2);
lightNNNNdouble operator/(const int e, const lightNNNNdouble& s2);
#endif


#ifdef IN_CALClm_complex_H
//lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator/(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator/(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator/(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator/(const double e, const lightNNNNlm_complex& s2);
#endif


//
// ElemProduct
//

lightNdouble ElemProduct(const lightNdouble& s1, const lightNdouble& s2);


lightNNdouble ElemProduct(const lightNNdouble& s1, const lightNNdouble& s2);


lightN3double ElemProduct(const lightN3double& s1, const lightN3double& s2);


lightNNNdouble ElemProduct(const lightNNNdouble& s1, const lightNNNdouble& s2);


lightNNNNdouble ElemProduct(const lightNNNNdouble& s1, const lightNNNNdouble& s2);

//
// ElemQuotient
//

lightNdouble ElemQuotient(const lightNdouble& s1, const lightNdouble& s2);


lightNNdouble ElemQuotient(const lightNNdouble& s1, const lightNNdouble& s2);


lightN3double ElemQuotient(const lightN3double& s1, const lightN3double& s2);


lightNNNdouble ElemQuotient(const lightNNNdouble& s1, const lightNNNdouble& s2);


lightNNNNdouble ElemQuotient(const lightNNNNdouble& s1, const lightNNNNdouble& s2);

//
// Apply
//

lightNdouble Apply(const lightNdouble& s, double f(double));


lightNdouble Apply(const lightNdouble& s1, const lightNdouble& s2, double f(double, double));


lightNNdouble Apply(const lightNNdouble& s, double f(double));


lightNNdouble Apply(const lightNNdouble& s1, const lightNNdouble& s2, double f(double, double));


lightNNNdouble Apply(const lightNNNdouble& s, double f(double));


lightNNNdouble Apply(const lightNNNdouble& s1, const lightNNNdouble& s2, double f(double, double));


lightNNNNdouble Apply(const lightNNNNdouble& s, double f(double));


lightNNNNdouble Apply(const lightNNNNdouble& s1, const lightNNNNdouble& s2, double f(double, double));

//
// Transpose
//

lightNNdouble Transpose(const lightN3double& s);


lightNNdouble Transpose(const lightNNdouble& s);


lightNNNdouble Transpose(const lightNNNdouble& s);


lightNNNNdouble Transpose(const lightNNNNdouble& s);


lightNNdouble OuterProduct(const lightNdouble& s1, const lightNdouble& s2);

//
// pow
//

lightNdouble pow(const lightNdouble& s1, const lightNdouble& s2);


lightNdouble pow(const lightNdouble& s, const double e);


lightNdouble pow(const double e, const lightNdouble& s);

// Why this is needed for lightNlm_complex only ?
// This is because lightNlm_complex  accepts a (double) argument as
// constructor argument, via automatic conversion double=>integer.
// In constrast  lightNNlm_complex  accepts two (double) arguments as
// constructor argument, not just one,


lightNlm_complex pow(const lightNlm_complex& s1, const double e);

lightNlm_complex pow( const double e,const lightNlm_complex& s1);



lightN3double pow(const lightN3double& s1, const lightN3double& s2);


lightN3double pow(const lightN3double& s, const double e);


lightN3double pow(const double e, const lightN3double& s);


lightNNdouble pow(const lightNNdouble& s1, const lightNNdouble& s2);


lightNNdouble pow(const lightNNdouble& s, const double e);


lightNNdouble pow(const double e, const lightNNdouble& s);


lightNNNdouble pow(const lightNNNdouble& s1, const lightNNNdouble& s2);


lightNNNdouble pow(const lightNNNdouble& s, const double e);


lightNNNdouble pow(const double e, const lightNNNdouble& s);


lightNNNNdouble pow(const lightNNNNdouble& s1, const lightNNNNdouble& s2);


lightNNNNdouble pow(const lightNNNNdouble& s, const double e);


lightNNNNdouble pow(const double e, const lightNNNNdouble& s);



#include "d_light_calc_auto.h"

#undef IN_CALCdouble_H
