//           -*- c++ -*-

#define IN_BASICdouble_H
//
#ifdef COMPLEX_TOOLS

inline double LIGHTABS(const lm_complex m);
#else

inline double LIGHTABS(const double m);
#endif

//
// Basic routines with simple operations
//


inline void light_assign(const int n, const double *a, double *b);


inline void light_assign(const int i0, const int i1, const double *a, double *b);


inline void light_plus(const int n, const double *a, const double *b, double *c);


inline void light_minus(const int n, const double *a, const double *b, double *c);


inline void light_mult(const int n, const double *a, const double *b, double *c);


inline void light_divide(const int n, const double *a, const double *b, double *c);


inline void light_assign(const int n, const double a, double *b);


inline void light_assign(const int i0, const int i1, const double a, double *b);


inline void light_plus(const int n, const double a, const double *b, double *c);

#ifdef IN_BASICdouble_H
inline void light_plus(const int n, const double a, const int *b, double *c);
inline void light_plus(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_H
inline void light_plus(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_plus(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_plus(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_plus(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_minus(const int n, const double a, const double *b, double *c);

#ifdef IN_BASICdouble_H
inline void light_minus(const int n, const double a, const int *b, double *c);
inline void light_minus(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_H
inline void light_minus(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_minus(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_minus(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_minus(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_mult(const int n, const double a, const double *b, double *c);

#ifdef IN_BASICdouble_H
inline void light_mult(const int n, const double a, const int *b, double *c);
inline void light_mult(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_H
inline void light_mult(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_mult(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_mult(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_mult(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_divide(const int n, const double a, const double *b, double *c);

#ifdef IN_BASICdouble_H
inline void light_divide(const int n, const double a, const int *b, double *c);
inline void light_divide(const int n, const int a, const double *b, double *c);
#endif

#ifdef IN_BASIClm_complex_H
inline void light_divide(const int n, const lm_complex a, const int *b, lm_complex *c);
inline void light_divide(const int n, const lm_complex a, const double *b, lm_complex *c);
inline void light_divide(const int n, const int a, const lm_complex *b, lm_complex *c);
inline void light_divide(const int n, const double a, const lm_complex *b, lm_complex *c);
#endif


inline void light_plus_same(const int n, double *a, const double *b);


inline void light_minus_same(const int n, double *a, const double *b);


inline void light_mult_same(const int n, double *a, const double *b);


inline void light_plus_same(const int n, double *a, const double b);


inline void light_mult_same(const int n, double *a, const double b);


inline double light_pow(double a, double b);


#ifdef IN_BASICdouble_H
inline double light_atan2(double a, double b);
#endif

#ifdef IN_BASIClm_complex_H
inline lm_complex light_atan2(lm_complex a, lm_complex b);
#endif

//
// Basic routines for vector and matrix calculation
//

// Dot product
//
// n : length of vectors
// x : vector 1
// y : vector 2
//

 inline double light_dot(const int n, const double *x, const double *y);

// Dot product
//
// n  : length of vectors
// x  : vector 1
// ix : step between elements in x
// y  : vector 2
//

 inline double light_dot(const int n,const double *x,
		    const int ix,const double *y);

// Matrix-vector product
//
// m : rows of matrix
// n : columns of matrix and no. elements in vector
// a : matrix
// x : vector
// y : result vector (length m)

inline void light_gemv(const int m, const int n,
		       const double *a, const double *x, double *y);

// Vector-matrix product
//
// m : rows of matrix and no. elements in vector
// n : columns of matrix
// x : vector
// a : matrix
// y : result vector (length n)

inline void light_gevm(const int m, const int n,
		       const double *x, const double *a, double *y);

// Vector-matrix product
//
// m  : rows of matrix and no. elements in vector
// n  : columns of matrix
// x  : vector
// a  : matrix
// y  : result vector (length n)
// iy : step between elements in y

inline void light_gevm(const int m, const int n,
		       const double *x, const double *a, double *y,
		const int iy);

// Matrix-Matrix product
//
// m : rows of matrix a and c
// n : columns of matrix b and c
// k : columns of matrix a and rows of matrix b
// a : matrix 1
// b : matrix 2
// c : result matrix

inline void light_gemm(const int m, const int n,
		       const int k, const double *a,
		       const double *b, double *c);

// Tensor3-Vector product
//
// m : size1 of a and rows of c
// n : size2 of a and columns of c
// k : size3 of a and no. elements in b
// a : tensor3
// b : vector
// c : result matrix

inline void light_ge3v(const int m, const int n,
		       const int k, const double *a,
		       const double *b, double *c);

// Vector-Tensor3 product
//
// m : size2 of b and rows of c
// n : size3 of a and columns of c
// k : no. elements in a and size1 of b
// a : vector
// b : tensor3
// c : result matrix

inline void light_gev3(const int m, const int n,
		       const int k, const double *a,
		       const double *b, double *c);

// Tensor3-Matrix product
//
// m : size1 of a and c
// n : size2 of a and c
// o : columns of b and size3 of c
// k : size3 of a and rows of b
// a : tensor3
// b : matrix
// c : result tensor3

inline void light_ge3m(const int m, const int n,
		       const int o, const int k,
		       const double *a, const double *b, double *c);

// Matrix-Tensor3 product
//
// m : rows of a and size1 of c
// n : size2 of b and c
// o : size3 of b and c
// k : columns of a and size1 of b
// a : matrix
// b : tensor3
// c : result tensor3

inline void light_gem3(const int m, const int n,
		       const int o, const int k,
		       const double *a, const double *b, double *c);

// Tensor3-Tensor3 product
//
// m : size1 of a c
// n : size2 of a and c
// o : size2 of b and size3 of c
// p : size3 of b and size4 of c
// k : size3 of a and size1 of b
// a : tensor3
// b : tensor3
// c : result tensor4

inline void light_ge33(const int m, const int n,
		       const int o, const int p,
		       const int k, const double *a,
		       const double *b, double *c);

// Tensor4-Vector product
//
// m : size1 of a and c
// n : size2 of a and c
// o : size3 of a and c
// k : size4 of a and no. elements in b
// a : tensor4
// b : vector
// c : result tensor3

inline void light_ge4v(const int m, const int n,
		       const int o, const int k,
		       const double *a, const double *b, double *c);

// Vector-Tensor4 product
//
// m : size2 of b and size1 of c
// n : size3 of b and size2 of c
// o : size4 of b and size3 of c
// k : no. elements in a and size1 of b
// a : vector
// b : tensor4
// c : result tensor3

inline void light_gev4(const int m, const int n,
		       const int o, const int k,
		       const double *a, const double *b, double *c);

// Tensor4-Matrix product
//
// m : size1 of a and c
// n : size2 of a and c
// o : size3 of a and c
// p : columns of b and size4 of c
// k : size4 of a and rows of b
// a : tensor4
// b : matrix
// c : result tensor4

inline void light_ge4m(const int m, const int n,
		       const int o, const int p,
		       const int k, const double *a,
		       const double *b, double *c);

// Matrix-Tensor4 product
//
// m : rows of a and size1 of c
// n : size2 of b and c
// o : size3 of b and c
// p : size4 of b and c
// k : columns of a and size1 of b
// a : matrix
// b : tensor4
// c : result tensor4

inline void light_gem4(const int m, const int n,
		       const int o, const int p,
		       const int k, const double *a,
		       const double *b, double *c);



inline lightNdouble light_join (const lightNdouble arr1, const lightNdouble arr2);


inline lightNNdouble light_join (const lightNNdouble arr1, const lightNNdouble arr2);


inline lightNNNdouble light_join (const lightNNNdouble arr1, const lightNNNdouble arr2);


inline lightNNNNdouble light_join (const lightNNNNdouble arr1, const lightNNNNdouble arr2);


inline lightNdouble light_append (const lightNdouble arr, const double el);


inline lightNNdouble light_append (const lightNNdouble arr, const lightNdouble el);


inline lightNNNdouble light_append (const lightNNNdouble arr, const lightNNdouble el);


inline lightNNNNdouble light_append (const lightNNNNdouble arr, const lightNNNdouble el);


inline lightNdouble light_prepend (const lightNdouble arr, const double el);


inline lightNNdouble light_prepend (const lightNNdouble arr, const lightNdouble el);


inline lightNNNdouble light_prepend (const lightNNNdouble arr, const lightNNdouble el);


inline lightNNNNdouble light_prepend (const lightNNNNdouble arr, const lightNNNdouble el);


inline lightNdouble light_drop (const lightNdouble arr, const R r);


inline lightNNdouble light_drop (const lightNNdouble arr, const R r);


inline lightNNNdouble light_drop (const lightNNNdouble arr, const R r);


inline lightNNNNdouble light_drop (const lightNNNNdouble arr, const R r);


//
// Join for N
//


inline lightNdouble Join (const lightNdouble arr1, const lightNdouble arr2);


inline lightNdouble Join (const light3double arr1, const lightNdouble arr2);


inline lightNdouble Join (const light4double arr1, const lightNdouble arr2);


inline lightNdouble Join (const light3double arr1, const light3double arr2);


inline lightNdouble Join (const light4double arr1, const light3double arr2);


inline lightNdouble Join (const lightNdouble arr1, const light3double arr2);


inline lightNdouble Join (const light3double arr1, const light4double arr2);


inline lightNdouble Join (const light4double arr1, const light4double arr2);


inline lightNdouble Join (const lightNdouble arr1, const light4double arr2);


//
// Join for NN
//


inline lightNNdouble Join (const lightNNdouble arr1, const lightNNdouble arr2);


inline lightNNdouble Join (const light33double arr1, const lightNNdouble arr2);


inline lightNNdouble Join (const light44double arr1, const lightNNdouble arr2);


inline lightNNdouble Join (const light33double arr1, const light33double arr2);


inline lightNNdouble Join (const light44double arr1, const light33double arr2);


inline lightNNdouble Join (const lightNNdouble arr1, const light33double arr2);


inline lightNNdouble Join (const light33double arr1, const light44double arr2);


inline lightNNdouble Join (const light44double arr1, const light44double arr2);


inline lightNNdouble Join (const lightNNdouble arr1, const light44double arr2);

//
// Join for NNN
//


inline lightNNNdouble Join (const lightNNNdouble arr1, const lightNNNdouble arr2);

//
// Join for NNNN
//


inline lightNNNNdouble Join (const lightNNNNdouble arr1, const lightNNNNdouble arr2);


//
// Append
//


inline lightNdouble Append (const light3double arr, const double el);


inline lightNdouble Append (const light4double arr, const double el);


inline lightNdouble Append (const lightNdouble arr, const double el);


inline lightNNdouble Append (const light33double arr, const light3double el);


inline lightNNdouble Append (const light44double arr, const light4double el);


inline lightNNdouble Append (const lightNNdouble arr, const lightNdouble el);


inline lightNNNdouble Append (const lightNNNdouble arr, const lightNNdouble el);


inline lightNNNNdouble Append (const lightNNNNdouble arr, const lightNNNdouble el);

//
// Prepend
//


inline lightNdouble Prepend (const light3double arr, const double el);


inline lightNdouble Prepend (const light4double arr, const double el);


inline lightNdouble Prepend (const lightNdouble arr, const double el);


inline lightNNdouble Prepend (const light33double arr, const light3double el);


inline lightNNdouble Prepend (const light44double arr, const light4double el);


inline lightNNdouble Prepend (const lightNNdouble arr, const lightNdouble el);


inline lightNNNdouble Prepend (const lightNNNdouble arr, const lightNNdouble el);


inline lightNNNNdouble Prepend (const lightNNNNdouble arr, const lightNNNdouble el);

//
// Drop
//


inline lightNdouble Drop (const lightNdouble arr, const R r);


inline lightNdouble Drop (const light3double arr, const R r);


inline lightNdouble Drop (const light4double arr, const R r);


inline lightNNdouble Drop (const lightNNdouble arr, const R r);


inline lightNNdouble Drop (const light33double arr, const R r);


inline lightNNdouble Drop (const light44double arr, const R r);


inline lightNNNdouble Drop (const lightNNNdouble arr, const R r);


inline lightNNNNdouble Drop (const lightNNNNdouble arr, const R r);


#undef IN_BASICdouble_H
