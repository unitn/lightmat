#ifndef PrivPack_h
#define PrivPack_h
#include <stdlib.h>
#include <assert.h>
#include "lightmat.h"
#include "rw/cstring.h"


/*
 *
 $Id$
 *
 $Log$
 Revision 1.1  1997/03/14 20:09:03  vaden
 The LighMat hierarchy is necessary to
 compile geneated files from the codegenerator.
 The initial version is adapted for Win95 from Solaris version.

 Revision 1.2  1996/03/08 16:33:19  patno
 Some commenting of the code.

 Revision 1.1.1.1  1996/02/08 16:14:40  patno
 Imported misc

 Revision 1.6  1995/11/28 13:00:09  patno
 added direct_arr_(u)pk methods for (un)packing arrays. 
 *
 */

#define SKF_PACK

#ifdef SKF_PACK
enum PackType { Down, Up };
#endif


/*
   <cd> aPackBuf

   <s> Functionality
   <p>
   A class to pack integers doubles etc. into a buffer. It is 
   intended to be a <q>light</q> class, no extra features than 
   necessary is implemented.
   <p>
   An instance of this class can pack data into a buffer. The
   buffer is of type <f>buftyp</f> (must essentially be char, otherwise
   alignement problems will occur). The buffer is kept internally 
   within the object.
   It can pack integers, doubles, doubleVec3, doubleMat33, and 
   RWCString. Also packing and unpacking of arrays of integers and doubles
   are supported. A buffer will not allocate any memory for the application, 
   that is, if an array should be upacked, the pointer should point enough 
   memory to be unpacked.   
   
   <p>
   We align the pointer so that we can cast lvalue to the
   appropirate type. Due to the alignment we may have to allocate some
   <q>extra</q> memory. This is taken care of by the operators and the methods. 
   Note that to pack <code>int x; double y ;</code> will require 16 bytes 
   and not 12. 
   
   <s> Summary of usage.
   <p>
   The accual (un)packing is done with the operators <op> >> </op> and 
   <op> << </op>, or with 
   the methods <m>pk(...)</m> and <m>upk(...)</m>.
    <longcode>
    aPackBuf bp ;

    // For packing data
    bp << x ;
    for(i=0;i<10;i++)
         bp << Y[i] ;

    // alt.  
    bp.pk(x) ;
    for(i=0;i<10;i++)
         bp.pk(Y[i]);

    // and for the unpacking :

    bp >> x ;
    for(i=0;i<10;i++)
         bp >> Y[i] ;

    // alt.  
    bp.upk(x) ;
    for(i=0;i<10;i++)
         bp.upk(Y[i]);
    </longcode>
   <p>
   Performance : with alignment and inlining a doubleFuncCy2 spline
   object requires 1088 bytes and will be packed in 115 us on a 
   Sparc 10. with no alignment and no inlining the time becomes
   402 us, but the size of the buffer has shrink to 1044 bytes
   
<author>
<aname> Patrik Nordling

*/

///      Patrik Nordling PELAB L-ping 13/10-95
///          with thanks to Mikael Pettersson


/// the type of the buffer
typedef char    buftyp ;
typedef buftyp* bufptr  ;

#define ALIGN_DBL_INT 

#define DEFAULT_INC_SIZE 16

class aPackBuf {
    
    bufptr bpt_1 ;
    // Will point at the begining of the buffer.
    
    bufptr bpt ;          
    // Will be incremented for every byte that is packed, the <q>running</q>
    // pointer.
    
    size_t allocedTheSize ; 
    // Returns how much that is currently allocated for the buffer.
    
    size_t default_inc ;
    // The increment of the buffer when it owerflows and <f>realloc(3C)</f>
    // must be called.
    
    inline bufptr align(bufptr a, size_t size){
	return (bufptr) (((unsigned long)a + (size-1))&(~(size-1))) ;
    } ;
    // Aligns the pointer with respect of the size of the next object to be
    // packed next.
    
    inline bufptr alignoc(bufptr a, size_t size){	
	bufptr tmp = align(a, size) ;	
	if( ((size_t)(tmp - bpt_1) + size) > allocedTheSize ){
	    get_more_mem() ;
	    return align(bpt, size) ;
	} else {
	    return align(a, size) ;
	}
    } ;
    // Aligns the pointer with respect of the size to packed next and
    // allocates more memory if nessecary.
    
    inline bufptr alignoclen(bufptr a, size_t size, int len){	
	bufptr tmp = align(a, size) ;	
	if( ((size_t)(tmp - bpt_1) + size*len) > allocedTheSize ){
	    get_more_mem(size*len) ;
	    return align(bpt, size) ;
	} else {
	    return align(a, size) ;
	}
    } ;
    // Aligns the pointer with respect of the size to packed next and
    // allocates more memory if nessecary, this when packing an array.
    
    void get_more_mem() ;
    // Allocates more memory for the buffer.
    
    void get_more_mem(const int msize) ;
    // Allocates more memory for the buffer <a>msize</a> bytes is
    // allocated.
    
public:    
    
    aPackBuf() ;
    // Constructor.
    
    aPackBuf(const size_t size) ;
    // Constructor, will allocate <a>size</a> bytes for the buffer.
    
    aPackBuf(const size_t size, const size_t defsize) ;
    // Constructor, will allocate <a>size</a> bytes in the buffer, 
    // and sets the default increment size to <a>defsize</a>.
    
    ~aPackBuf() ;
    // Destructor.
    
    aPackBuf & operator=(const aPackBuf & a) ;
    // Copies the buffer and moves the copied running pointer. 
    
    inline char operator[](const int i){
	return ( bpt_1[i] ) ; }
    // For accesing the byte <a>i</a>.
    
    void print() ;
    // Prints the whole buffer.

    void useSize(const int) ;
    // Tell the buffer it will need a certain size.

    void reset() ;
    // Resets the <q>running</q> pointer, reuses the allocated memory.
    
    bufptr getPtr() ;
    // Obtain the buffer pointer.
    
    void setPtr(bufptr a) ;
    // Set the buffer pointer to <a>a</a>.
    
    int Packed() ;
    // Returns the number of bytes that have been packed so far.
    
    int AllocedSize() ;
    // The number of size that is allocated.
    
    inline void free_mem(){
	free(bpt_1) ; allocedTheSize = 0 ;
    } ;
    // Deallocates then memory allocated.

    /* <msd type=public> 
     Methods for packing data, pk means pack, and upk means
     unpack. The methods are overloaded to handle the basic types:
     int, double, doubleVec3, doubleMat33, and RWCString.
    */
	
    inline void pk(const int &x){ 	
	bpt = alignoc(bpt, sizeof(int)) ;	
	*(int *)bpt = x ;
	bpt += sizeof(int) ;
    } ;
    // Packs an integer.

    inline void upk(int &x){ 
	bpt = align(bpt, sizeof(int)) ;
	x = *(int *)bpt ;
	bpt += sizeof(int) ;
    } ;
    // Unpacks an integer.

    inline void pk(const double &x){ 
	bpt = alignoc(bpt, sizeof(double)) ;
	*(double *)bpt = x ;
	bpt += sizeof(double) ;
    } ;
    // Packs a double.    

    inline void upk(double &x){ 
	bpt = align(bpt, sizeof(double)) ;
	x = *(double *)bpt ;
	bpt += sizeof(double) ;
    } ;
    // Unpacks a double.    


    inline void pk(const RWCString &x){
	int N = x.length() ; 
	int i ;    
	
	if( ((int)(bpt - bpt_1) + N + 1) > (int)allocedTheSize )
	    get_more_mem(N+1) ;
	
	for(i=0; i<N; i++){
	    *bpt=x(i) ;
	    (void)(bpt)++ ;
	}
	
	*bpt= '\0' ;
	(void)(bpt)++ ;
    } ;
    // Packs an RWCString.    
    

    inline void upk(RWCString &x){
	x = bpt ;    
	bpt += x.length() + 1 ;
    } ;
    // Unpacks an RWCString.    
    
    inline void direct_arr_pk(const int *x, const int len) {
	if(len>0){
	    bpt = alignoclen(bpt, sizeof(int), len) ;	
	    for(int i=0; i<len; i++){
		*(int *)bpt = x[i] ;
		bpt += sizeof(int) ;		
	    }
	}
    } ;
    // Packs an array of integers, the <a>len</a> in not packed. 

    inline void direct_arr_upk(int *x, const int len) {
	if(len>0){
	    bpt = align(bpt, sizeof(int)) ; 
	    for(int i=0; i<len; i++){
		x[i] = *(int *)bpt ;
		bpt += sizeof(int) ;		
	    }
	}
    } ;
    // Unpacks an array of integers, the <a>len</a> in not unpacked. 
    
    inline void direct_arr_pk(const double *x, const int len) {
	if(len>0){
	    bpt = alignoclen(bpt, sizeof(double), len) ;
	    for(int i=0; i<len; i++){
		*(double *)bpt = x[i] ;
		bpt += sizeof(double) ; 
	    }
	}
    } ;
    // Packs an array of doubles, the <a>len</a> in not packed. 
    
    inline void direct_arr_upk(double *x, const int len) {
	if(len>0){
	    bpt = align(bpt, sizeof(double)) ; 
	    for(int i=0; i<len; i++){
		x[i] = *(double *)bpt ;
		bpt += sizeof(double) ; 
	    }
	}
    } ;
    // Unpacks an array of doubles, the <a>len</a> in not unpacked. 

    inline void direct_arr_pk(const char *x, const int len) {
	if(len>0){
	    bpt = alignoclen(bpt, sizeof(char), len) ;	
	    for(int i=0; i<len; i++){
		*bpt = x[i] ;
		bpt++;		
	    }
	}
    } ;
    // Packs an array of chars, the <a>len</a> in not packed. 
    
    inline void direct_arr_upk(char *x, const int len) {
	if(len>0){
	    bpt = align(bpt, sizeof(char)) ; 
	    for(int i=0; i<len; i++){
		x[i] = *bpt;
		bpt++;		
	    }
	}
    } ;
    // Unpacks an array of chars, the <a>len</a> in not unpacked. 
    
    inline void pk(const doubleVec3 &x){    
	pk(x(1)) ;
	pk(x(2)) ;
	pk(x(3)) ;
    } ;
    // Packs a doubleVec3.

    inline void upk(doubleVec3 &x){    
	upk(x(1)) ; 
	upk(x(2)) ; 
	upk(x(3)) ; 
    } ;
    // Unpacks a doubleVec3.
    
    inline void pk(const doubleMat33 &x){ 
	direct_arr_pk((double*)&x, 9) ;
    } ;
    // Packs a doubleMat33.
    
    inline void upk(doubleMat33 &x){    
	direct_arr_upk((double*)&x, 9) ;
    } ;
    // Unpacks a doubleMat33.
    
    /* <msd type=public> 
     Operators for packing and unpacking. */

    inline void operator<<(const int &x){
	pk(x) ; } ;
    // Packs an integer.

    inline void operator<<(const double &x){
	pk(x) ; } ;
    // Packs a double.
    
    inline void operator<<(const doubleVec3 &x){
    	pk(x) ; } ;
    // Packs a doubleVec3.
    
    inline void operator<<(const doubleMat33 &x){
    	pk(x) ; } ;
    // Packs a doubleMat33.
    
    inline void operator<<(const RWCString &x){
    	pk(x) ; } ;
    // Packs an RWCString.
    
    inline void operator>>(int &x){
	upk(x) ; } ;
    // Unpacks an integer.

    inline void operator>>(double &x){
	upk(x) ; } ;
    // Unpacks a double.
    
    inline void operator>>(doubleVec3 &x){
    	upk(x) ; } ;
    // Unpacks a doubleVec3.
    
    inline void operator>>(doubleMat33 &x){
    	upk(x) ; } ;
    // Unpacks a doubleMat33.
    
    inline void operator>>(RWCString &x){
    	upk(x) ; } ;
    // Unpacks an RWCString.

#ifdef SKF_PACK    
    inline void pack(const PackType p, double *x, const int len){
	if(p==Down)
	    direct_arr_pk(x, len) ;
	else
	    direct_arr_upk(x, len) ;
    } ;
    // Packs or unpacks an array of doubles, given a direction <a>p</a>.
    // The size <a>len</a> in not packed or unpacked.

    inline void pack(const PackType p, int *x, const int len){
	if(p==Down)
	    direct_arr_pk(x, len) ;
	else
	    direct_arr_upk(x, len) ;
    } ;
    // Packs or unpacks an array of integers, given a direction <a> p </a>.
    // The size <a>len</a> in not packed or unpacked.

    inline void pack(PackType p, int &x){
	if(p==Down)
	    pk(x) ;
	else upk(x) ; } ;
    // (Un)packs an integer.

    inline void pack(PackType p, double &x){
	if(p==Down)
	    pk(x) ;
	else upk(x) ; } ;
    // (Un)packs a double.
    
    inline void pack(PackType p, doubleVec3 &x){
	if(p==Down)
	    pk(x) ;
	else upk(x) ; } ;
    // (Un)packs a doubleVec3.
    
    inline void pack(PackType p, doubleMat33 &x){
	if(p==Down)
	    pk(x) ;
	else upk(x) ; } ;
    // (Un)packs a doubleMat33.
    
    inline void pack(PackType p, RWCString &x){
	if(p==Down)
	    pk(x) ;
	else upk(x) ; } ;
    // (Un)packs an RWCString.

    
    void checkPoint(PackType p){
	int tst ;
	if(p == Down) {
	    bufptr a = bpt ;
	    tst = (int)(align(a, sizeof(int)) - bpt_1) + (int) sizeof(int) ;
	    pk(tst) ;
	} else {
	    upk(tst) ;	    
	    assert(tst == Packed()) ;
	}	
    }
    // For checking consistency in the packing and unpacking. The
    // sending side packs a number (i.e. the number of bytes it has packed) 
    // On the reciving side then we ought to have the same number of bytes
    // when unpacking the buffer.
    
    
#endif    

};    
        
#endif  // PrivPack_h
