//           -*- c++ -*-

#ifndef LIGHT44_H
#define LIGHT44_H
// <cd> light44
//
// .SS Functionality
//
// light44 is a template for classes that implement matrices with 4x4
// elements. E.g. a matrix v with 4x4 elements of type double can be
// instanciated with:
//
// <code>light44&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHT44<T>_H

template<class T> class light3;
template<class T> class light4;
template<class T> class lightN;
template<class T> class lightNN;
template<class T> class lightNNN;
template<class T> class lightNNNN;

template<class T>
class light44 {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light44<double>();
  // Convert to double.
#else
#endif
#ifdef LIGHTMAT_TEMPLATES
  //  friend class light44<int>;
#endif

#ifdef IN_LIGHT44<double>_H
   friend class  light44<int>;
#else
   friend class  light44<double>;
#endif

  #include "light44_auto.h"

  light44();
  // Default constructor.

  light44(const light44<T>&);
  // Copy constructor.

  inline 
  light44(const T e11, const T e12, const T e13, const T e14,
	  const T e21, const T e22, const T e23, const T e24,
	  const T e31, const T e32, const T e33, const T e34,
	  const T e41, const T e42, const T e43, const T e44);
  // Initialize elements with values.

  light44(const T *);
  // Initialize elements with values from an array (in row major order).

  light44(const T);
  // Initialize all elements with the same value.

  light44<T>& operator=(const light44<T>&);
  // Assignment.

  light44<T>& operator=(const lightNN<T>&);
  // Assignment from a lightNN where NN=44.

  light44<T>& operator=(const T);
  // Assign one value to all elements.

  T operator() (const int x, const int y) const {
    limiterror((x<1) || (x>4) || (y<1) || (y>4));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*4];
#else
    return elem[x-1+(y-1)*4];
#endif
  };
  // Get the value of one element.

  T& operator()(const int x, const int y) {
    limiterror((x<1) || (x>4) || (y<1) || (y>4));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*4];
#else
    return elem[x-1+(y-1)*4];
#endif
  };
  // Get/Set the value of one element.

  light4<T> operator()(const int) const;
  // Get the value of one row.
 



  //
  // Set
  //

  inline light44<T>& Set (const int i0, const T val);
  inline light44<T>& Set (const int i0, const lightN<T>& arr);
  inline light44<T>& Set (const int i0, const light3<T>& arr);
  inline light44<T>& Set (const int i0, const light4<T>& arr);



  int operator==(const light44<T>&) const;
  // Equality.

  int operator!=(const light44<T>&) const;
  // Inequality.

  light44<T>& operator+=(const T);
  // Add a value to all elements.

  light44<T>& operator+=(const light44<T>&);
  // Elementwise addition.

  light44<T>& operator-=(const T);
  // Subtract a value from all elements.

  light44<T>& operator-=(const light44<T>&);
  // Elementwise subtraction

  light44<T>& operator*=(const T);
  // Muliply all elements with a value.

  light44<T>& operator/=(const T);
  // Divide all elements with a value.

  light44<T>& reshape(const int, const int, const light4<T>&);
  // Convert the light4-vector to the given size and put the result in
  // this object. All four vector-columns of the light44 object will
  // get the value of the light4-argument. The first two arguments
  // (the size of the matrix) must both have the value 4 since it is a
  // light44 object.

  int dimension(const int x) const {
    if((x==1) || (x==2))
      return 4;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 4 and dimension(2) == 4).

  void Get(T *) const;
  // Get values of all elements and put them in an array (9 elements long,
  // row major order).

  void Set(const T *data);
  // Set values of all elements from array (9 elements long, row major
  // order).

  light44<T> operator+() const;
  // Unary plus.

  light44<T> operator-() const;
  // Unary minus.

  friend inline light44<T> operator+(const light44<T>&, const light44<T>&);
  // Elementwise addition.

  friend inline light44<T> operator+(const light44<T>&, const T);
  // Addition to all elements.

  friend inline light44<T> operator+(const T, const light44<T>&);
  // Addition to all elements.

  friend inline light44<T> operator-(const light44<T>&, const light44<T>&);
  // Elementwise subtraction.

  friend inline light44<T> operator-(const light44<T>&, const T);
  // Subtraction from all elements.

  friend inline light44<T> operator-(const T, const light44<T>&);
  // Subtraction to all elements.

  friend inline light44<T> operator*(const light44<T>&, const light44<T>&);
  // Inner product.

  friend inline light44<T> operator*(const light44<T>&, const T);
  // Multiply all elements.

  friend inline light44<T> operator*(const T, const light44<T>&);
  // Multiply all elements.

  friend inline light4<T> operator*(const light44<T>&, const light4<T>&);
  // Inner product.

  friend inline light4<T> operator*(const light4<T>&, const light44<T>&);
  // Inner product.

  friend inline light44<T> operator/(const light44<T>&, const T);
  // Divide all elements.

  friend inline light44<T> operator/(const T, const light44<T>&);
  // Divide with all elements.

  friend inline light44<T> pow(const light44<T>&, const light44<T>&);
  // Raise to the power of-function, elementwise.

  friend inline light44<T> pow(const light44<T>&, const T);
  // Raise to the power of-function, for all elements.

  friend inline light44<T> pow(const T, const light44<T>&);
  // Raise to the power of-function, for all elements.

  friend inline light44<T> ElemProduct(const light44<T>&, const light44<T>&);
  // Elementwise multiplication.

  friend inline light44<T> ElemQuotient(const light44<T>&, const light44<T>&);
  // Elementwise division.

  friend inline light44<T> Apply(const light44<T>&, T f(T));
  // Apply the function elementwise on all elements.

  friend inline light44<T> Apply(const light44<T>&, const light44<T>&, T f(T, T));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline light44<T> Transpose(const light44<T>&);
  // Transpose matrix.

  friend inline light44<T> abs(const light44<T>&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light44<int> sign(const light44<T>&);
  // sign
#else
  friend inline light44lm_complex sign(const light44lm_complex&);
#endif
  friend inline light44<int> ifloor(const light44<double>&);
  // ifloor

  friend inline light44<int> iceil(const light44<double>&);
  // iceil

  friend inline light44<int> irint(const light44<double>&);
  // irint

  friend inline light44<double> sqrt(const light44<double>&);
  // sqrt

  friend inline light44<double> exp(const light44<double>&);
  // exp

  friend inline light44<double> log(const light44<double>&);
  // log

  friend inline light44<double> sin(const light44<double>&);
  // sin

  friend inline light44<double> cos(const light44<double>&);
  // cos

  friend inline light44<double> tan(const light44<double>&);
  // tan

  friend inline light44<double> asin(const light44<double>&);
  // asin

  friend inline light44<double> acos(const light44<double>&);
  // acos

  friend inline light44<double> atan(const light44<double>&);
  // atan

  friend inline light44<double> sinh(const light44<double>&);
  // sinh

  friend inline light44<double> cosh(const light44<double>&);
  // cosh

  friend inline light44<double> tanh(const light44<double>&);
  // tanh

  friend inline light44<double> asinh(const light44<double>&);
  // asinh

  friend inline light44<double> acosh(const light44<double>&);
  // acosh

  friend inline light44<double> atanh(const light44<double>&);
  // atanh

  friend inline light44<lm_complex> ifloor(const light44<lm_complex>&);
  // ifloor

  friend inline light44<lm_complex> iceil(const light44<lm_complex>&);
  // iceil

  friend inline light44<lm_complex> irint(const light44<lm_complex>&);
  // irint

  friend inline light44<lm_complex> sqrt(const light44<lm_complex>&);
  // sqrt

  friend inline light44<lm_complex> exp(const light44<lm_complex>&);
  // exp

  friend inline light44<lm_complex> log(const light44<lm_complex>&);
  // log

  friend inline light44<lm_complex> sin(const light44<lm_complex>&);
  // sin

  friend inline light44<lm_complex> cos(const light44<lm_complex>&);
  // cos

  friend inline light44<lm_complex> tan(const light44<lm_complex>&);
  // tan

  friend inline light44<lm_complex> asin(const light44<lm_complex>&);
  // asin

  friend inline light44<lm_complex> acos(const light44<lm_complex>&);
  // acos

  friend inline light44<lm_complex> atan(const light44<lm_complex>&);
  // atan

  friend inline light44<lm_complex> sinh(const light44<lm_complex>&);
  // sinh

  friend inline light44<lm_complex> cosh(const light44<lm_complex>&);
  // cosh

  friend inline light44<lm_complex> tanh(const light44<lm_complex>&);
  // tanh

  friend inline light44<lm_complex> asinh(const light44<lm_complex>&);
  // asinh

  friend inline light44<lm_complex> acosh(const light44<lm_complex>&);
  // acosh

  friend inline light44<lm_complex> atanh(const light44<lm_complex>&);
  // atanh


  //<ignore>
//    friend class light44<int>;
  friend class light4<T>;
  friend class lightN3<T>;
  friend class lightNN<T>;
  friend class lightNNN<T>;
  friend class lightNNNN<T>;
  //</ignore>

protected:
  T elem[16];
  // The values of the sixteen elements.

  light44(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.


};

typedef light44<double> double44;
typedef light44<int> int44;

#undef IN_LIGHT44<T>_H

#endif
