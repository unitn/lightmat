//           -*- c++ -*-

#ifdef LIGHTMAT_OUTPUT_FUNCS

#ifndef IN_OUTPUTint
#define IN_OUTPUTint

#include "rw/cstring.h"

//
// ToStr
//
#ifndef __RWCSTRING_I_H__
inline RWCString
ToStr(const double d);

inline RWCString
ToStr(const int d);

inline RWCString
ToStr(const lm_complex & d);
#endif


inline RWCString
ToStr(const lightNint& s);


inline RWCString
ToStr(const lightNNint& s, int depth=0);


inline RWCString
ToStr(const lightNNNint& s, int depth=0);


inline RWCString
ToStr(const lightNNNNint& s, int depth=0);


inline RWCString
light_export(const char* file, const lightNint& s, const char* format, const char* option, const char* optionval);


inline RWCString
light_export(const char* file, const lightNNint& s, const char* format, const char* option, const char* optionval);



inline RWCString
light_export1(const char* file, const lightNint& s, const char* option, const char* optionval);



inline RWCString
light_export1(const char* file, const lightNNint& s, const char* option, const char* optionval);

//
// operator<<
//

inline ostream& operator<<(ostream& o, const lm_complex& s);


inline ostream&
operator<<(ostream& o, const light3int& s);


inline ostream&
operator<<(ostream& o, const light4int& s);


inline ostream&
operator<<(ostream& o, const lightNint& s);


inline ostream&
operator<<(ostream& o, const light33int& s);


inline ostream&
operator<<(ostream& o, const light44int& s);


inline ostream&
operator<<(ostream& o, const lightN3int& s);


inline ostream&
operator<<(ostream& o, const lightNNint& s);


inline ostream&
operator<<(ostream& o, const lightNNNint& s);


inline ostream&
operator<<(ostream& o, const lightN33int& s);


inline ostream&
operator<<(ostream& o, const lightNNNNint& s);



inline lightNint 
light_importNint(const char* filename, const char* format, const char* opt1, const char* opt2);


inline lightNNint 
light_importNNint(const char* filename, const char* format, const char* opt1, const char* opt2);



inline lightNint 
light_import1Nint(const char* file);


inline lightNNint 
light_import1NNint(const char* file);

#endif

#endif // LIGHTMAT_OUTPUT_FUNCS
