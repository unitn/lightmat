






//
// fractional part
//
inline <T> FractionalPart(const <T> s);
inline lightN<T>  FractionalPart(const lightN<T>&);
inline lightNN<T>  FractionalPart(const lightNN<T>&);

inline lightNNN<T>  FractionalPart(const lightNNN<T>&);

inline lightNNNN<T>  FractionalPart(const lightNNNN<T>&);


// IntegerPart
inline <T> IntegerPart(const <T> s);
lightN<T>  IntegerPart(const lightN<T>&);

lightNN<T>  IntegerPart(const lightNN<T>&);

lightNNN<T>  IntegerPart(const lightNNN<T>&);

lightNNNN<T>  IntegerPart(const lightNNNN<T>&);


// Mod

inline <T> Mod(const <T> m, const <T> n);


// abs, arg, im, re, conjugate 

inline lightNdouble abs(const lightN<T>& a);
inline lightNdouble arg(const lightN<T>& a);
inline lightNdouble re(const lightN<T>& a);
inline lightNdouble im(const lightN<T>& a);
inline lightN<T> conjugate(const lightN<T>& a);

inline lightNNdouble abs(const lightNN<T>& a);
inline lightNNdouble arg(const lightNN<T>& a);
inline lightNNdouble re(const lightNN<T>& a);
inline lightNNdouble im(const lightNN<T>& a);
inline lightNN<T> conjugate(const lightNN<T>& a);

inline lightNNNdouble abs(const lightNNN<T>& a);
inline lightNNNdouble arg(const lightNNN<T>& a);
inline lightNNNdouble re(const lightNNN<T>& a);
inline lightNNNdouble im(const lightNNN<T>& a);
inline lightNNN<T> conjugate(const lightNNN<T>& a);

inline lightNNNNdouble abs(const lightNNNN<T>& a);
inline lightNNNNdouble arg(const lightNNNN<T>& a);
inline lightNNNNdouble re(const lightNNNN<T>& a);
inline lightNNNNdouble im(const lightNNNN<T>& a);
inline lightNNNN<T> conjugate(const lightNNNN<T>& a);




