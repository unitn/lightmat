//           -*- c++ -*-

#ifdef LM_ALL
#define  LM_3
#define  LM_N3
#define  LM_4
#define  LM_33
#define  LM_N33
#define  LM_44
#define LM_N
#define LM_NN
#define LM_NNN
#define LM_NNNN
#endif

#ifdef LM_N33
#define LM_3
#define LM_33
#endif

#ifdef LM_N3
#define LM_3
#endif

#ifdef LM_33
#define LM_3
#endif

#ifdef LM_44
#define LM_4
#endif

#ifdef LM_NNNN
#define LM_NNN
#endif

#ifdef LM_NNN
#define LM_NN
#endif

#ifdef LM_NN
#define LM_N
#endif
