//           -*- c++ -*-

#ifndef LIGHTNNNN_I_H
#define LIGHTNNNN_I_H
// <cd> lightNNNint
//
// .SS Functionality
//
// lightNNNN is a template for classes that implement tensors of rank 4
// with any number of elements. E.g. a lightNNNN s with 5x6x7x8 elements of
// type double can be instanciated with:
//
// <code>lightNNNN&lt;double&gt; s(5,6,7,8);</code>
//
// The size of the tensor can change during execution. It changes its
// size to whatever is needed when it is assigned a new value with an
// assignment operator.
//
// When a lightNNNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNNN object with 5x6x5x5 elements to one with 5x5x6x5 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHTNNNNint_I_H


#define LIGHTNNNN_SIZE1 4
#define LIGHTNNNN_SIZE2 4
#define LIGHTNNNN_SIZE3 4
#define LIGHTNNNN_SIZE4 4
#define LIGHTNNNN_SIZE (LIGHTNNNN_SIZE1*LIGHTNNNN_SIZE2*LIGHTNNNN_SIZE3*LIGHTNNNN_SIZE4)
// The default size.

 class lightN;
 class lightNN;
 class lightNNN;


class lightNNNNint {
public:

#ifdef IN_LIGHTNNNNdouble_I_H
  friend class lightNNNNint;
#else
  friend class lightNNNNdouble;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNNNint;
#endif

  #include "i_lightNNNN_auto.h"

  inline lightNNNNint();
  // Default constructor.

  inline lightNNNNint(const lightNNNNint&);
  // Copy constructor.

  inline lightNNNNint(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNNNint(const int, const int, const int, const int);
  // Construct a lightNNNN of given size.

  inline lightNNNNint(const int, const int, const int, const int, const int *);
  // Construct a lightNNNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNNNint(const int, const int, const int, const int, const int);
  // Construct a lightNNNN of given size and initialize all elements
  // with a value (the last argument).

  inline ~lightNNNNint();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNNNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNNNlm_complex();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNNNlm_complex();
  // Convert to complex.
#endif

  lightNNNNint& operator=(const lightNNNNint&);
  // Assignment.

  lightNNNNint& operator=(const int);
  // Assign one value to all elements.

  //
  // Operator ().
  //
  
  int operator()(const int x, const int y, const int z, const int v) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3) || (v<1) || (v>size4));
#ifdef ROWMAJOR
    return elem[(((x-1)*size2+(y-1))*size3+(z-1))*size4+v-1];
#else
    return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
#endif
  };
  // Get the value of one element.


  int& operator()(const int x, const int y, const int z, const int v) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3) || (v<1) || (v>size4));
#ifdef ROWMAJOR
    return elem[(((x-1)*size2+(y-1))*size3+(z-1))*size4+v-1];
#else
    return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
#endif
  };
  // Get/Set the value of one element.

  inline lightNint operator()(const int, const int, const int) const;
  // Get the value of a vector in the tensor.

  inline lightNNint operator()(const int, const int) const;
  // Get the value of a matrix in the tensor.

  inline lightNNNint operator()(const int) const;
  // Get the value of a tensor of rank 3 in the tensor.


  //
  // Set
  //

  inline lightNNNNint& Set (const int i0, const int i1, const int i2,
			    const int val);
  inline lightNNNNint& Set (const int i0, const int i1, const int i2,
			    const lightNint& arr);
  inline lightNNNNint& Set (const int i0, const int i1, const int i2,
			    const light3int& arr);
  inline lightNNNNint& Set (const int i0, const int i1, const int i2,
			    const light4int& arr);
  inline lightNNNNint& Set (const int i0, const int i1, const int val);
  inline lightNNNNint& Set (const int i0, const int i1, const lightNNint& arr);
  inline lightNNNNint& Set (const int i0, const int i1, const light33int& arr);
  inline lightNNNNint& Set (const int i0, const int i1, const light44int& arr);
  inline lightNNNNint& Set (const int i0, const int val);
  inline lightNNNNint& Set (const int i0, const lightNNNint& arr);

 
  int operator==(const lightNNNNint&) const;
  // Equality.

  int operator!=(const lightNNNNint&) const;
  // Inequality.
 
  lightNNNNint& operator+=(const int);
  // Add a value to all elements.

  lightNNNNint& operator+=(const lightNNNNint&);
  // Elementwise addition.

  lightNNNNint& operator-=(const int);
  // Subtract a value from all elements.

  lightNNNNint& operator-=(const lightNNNNint&);
  // Elementwise subtraction.

  lightNNNNint& operator*=(const int);
  // Mulitply all elements with a value.

  lightNNNNint& operator/=(const int);
  // Divide all elements with a value.

  lightNNNNint& SetShape(const int x=-1, const int y=-1, 
			 const int z=-1, const int v=-1);
  // Sets specified shape to the tensor. 

  int Extract(const lightNint&) const;
  // Extract an element using given index. 

  lightNNNNint& reshape(const int, const int, const int, const int, const lightNint&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c,d) will be set to s(a) in the lightNNNNint
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNNint& reshape(const int, const int, const int, const int, const lightNNint&);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c,d) will be set to s(a,b) in the
  // lightNNNN object. The values of the first two arguments must be
  // the same as the size of the lightNN-matrix. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNNint& reshape(const int, const int, const int, const int, const lightNNNint&);
  // Convert the lightNNN-tensor to the given size and put the result
  // in this object. (*this)(a,b,c,d) will be set to s(a,b,c) in the
  // lightNNNN object. The values of the first three arguments must be
  // the same as the size of the lightNNN-tensor. The program may dump
  // core or behave strangely if the arguments are incorrect.

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else if(x == 3)
      return size3;
    else if(x == 4)
      return size4;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(int *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const int *);
  // Set values of all elements from array (row major order).
 
  int * data() const;
  // Direct access to the stored data. Use the result with care. 

  lightNNNNint operator+() const;
  // Unary plus.

  lightNNNNint operator-() const;
  // Unary minus.


  friend inline lightNNNNint operator+(const lightNNNNint&, const lightNNNNint&);
  // Elementwise addition.

  friend inline lightNNNNint operator+(const lightNNNNint&, const int);
  // Addition to all elements.

  friend inline lightNNNNint operator+(const int, const lightNNNNint&);
  // Addition to all elements.

#ifdef IN_LIGHTNNNNdouble_I_H
  friend inline lightNNNNdouble operator+(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator+(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator+(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator+(const int, const lightNNNNdouble&);
#endif

  #ifdef IN_LIGHTNNNNlm_complex_I_H
friend inline lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator+(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator+(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNNint operator-(const lightNNNNint&, const lightNNNNint&);
  // Elementwise subtraction.

  friend inline lightNNNNint operator-(const lightNNNNint&, const int);
  // Subtraction from all elements.

  friend inline lightNNNNint operator-(const int, const lightNNNNint&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNNNdouble_I_H
  friend inline lightNNNNdouble operator-(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator-(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator-(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator-(const int, const lightNNNNdouble&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_I_H
friend inline lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator-(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator-(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNNint operator*(const lightNNNNint&, const int);
  // Multiply all elements.

  friend inline lightNNNNint operator*(const int, const lightNNNNint&);
  // Multiply all elements.

#ifdef IN_LIGHTNNNNdouble_I_H
  friend inline lightNNNNdouble operator*(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator*(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator*(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator*(const int, const lightNNNNdouble&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_I_H
friend inline lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator*(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator*(const double e, const lightNNNNlm_complex& s2);
#endif

  friend inline lightNNNNint operator*(const lightNNNNint&, const lightNNint&);
  // Inner product.

  friend inline lightNNNNint operator*(const lightNNint&, const lightNNNNint&);
  // Inner product.

  friend inline lightNNNNint operator*(const lightNNNNint&, const light33int&);
  // Inner product.

  friend inline lightNNNNint operator*(const light33int&, const lightNNNNint&);
  // Inner product.

  friend inline lightNNNNint operator*(const lightNNNNint&, const light44int&);
  // Inner product.

  friend inline lightNNNNint operator*(const light44int&, const lightNNNNint&);
  // Inner product.

  friend inline lightNNNNint operator*(const lightNNNint&, const lightNNNint&);
  // Inner product.

  friend inline lightNNNNint operator/(const lightNNNNint&, const int);
  // Divide all elements.

  friend inline lightNNNNint operator/(const int, const lightNNNNint&);
  // Divide all elements.

#ifdef IN_LIGHTNNNNdouble_I_H
  friend inline lightNNNNdouble operator/(const lightNNNNdouble&, const int);
  friend inline lightNNNNdouble operator/(const lightNNNNint&, const double);
  friend inline lightNNNNdouble operator/(const double, const lightNNNNint&);
  friend inline lightNNNNdouble operator/(const int, const lightNNNNdouble&);
#endif

#ifdef IN_LIGHTNNNNlm_complex_I_H
friend inline lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const double e);
friend inline lightNNNNlm_complex operator/(const lightNNNNdouble& s1, const lm_complex e);
friend inline lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNdouble& s2);
friend inline lightNNNNlm_complex operator/(const double e, const lightNNNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNNNlm_complex_I_H
friend inline lightNNNNdouble arg(const lightNNNNlm_complex& a);
friend inline lightNNNNdouble re(const lightNNNNlm_complex& a);
friend inline lightNNNNdouble im(const lightNNNNlm_complex& a);
friend inline lightNNNNlm_complex conjugate(const lightNNNNlm_complex& a);
//#endif

  friend inline lightNNNNint pow(const lightNNNNint&, const lightNNNNint&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNNNint pow(const lightNNNNint&, const int);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNNint pow(const int, const lightNNNNint&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNNint ElemProduct(const lightNNNNint&, const lightNNNNint&);
  // Elementwise multiplication.

  friend inline lightNNNNint ElemQuotient(const lightNNNNint&, const lightNNNNint&);
  // Elementwise division.

  friend inline lightNNNNint Apply(const lightNNNNint&, int f(int));
  // Apply the function elementwise all elements.

  friend inline lightNNNNint Apply(const lightNNNNint&, const lightNNNNint&, int f(int, int));
  // Apply the function elementwise on all elements in the two tensors.

  friend inline lightNNNNint Transpose(const lightNNNNint&);
  // Transpose.

  
#ifdef IN_LIGHTNNNNlm_complex_I_H
#else
  friend inline lightNNNNint abs(const lightNNNNint&);
#ifdef IN_LIGHTNNNNdouble_I_H
  friend inline lightNNNNdouble abs(const lightNNNNlm_complex&);
#endif 
#endif


  // abs

#ifndef COMPLEX_TOOLS
  friend inline lightNNNNint sign(const lightNNNNint&);
  // sign

  friend inline lightNNNNint sign(const lightNNNNdouble&);
  // sign
#else
  friend inline lightNNNNlm_complex sign(const lightNNNNlm_complex&);
#endif


/// Added 2/2/98 

  friend inline lightNNNNint  FractionalPart(const lightNNNNint&);
  //  FractionalPart

  friend inline lightNNNNint  IntegerPart(const lightNNNNint&);
  //  IntegerPart
 
  //friend inline lightNNNNint  IntegerPart(const lightNNNNdouble&);
  //  IntegerPart

  friend inline lightNNNNint Mod (const  lightNNNNint&,const  lightNNNNint&);
  // Mod
 
  friend inline lightNNNNint Mod (const  lightNNNNint&,const int);
  // Mod

  friend inline lightNNNNint Mod (const int,const  lightNNNNint&);
  // Mod

  friend inline int LightMax (const lightNNNNint& );
  // Max

  friend inline int LightMin (const lightNNNNint& );
  // Min

  friend inline int findLightMax (const  int *,const  int );
  // Find Max
 
  friend inline int findLightMin (const  int *,const  int );
  // Find Min
 
  /// End Added 

  friend inline lightNNNNint ifloor(const lightNNNNdouble&);
  // ifloor

  friend inline lightNNNNint iceil(const lightNNNNdouble&);
  // iceil

  friend inline lightNNNNint irint(const lightNNNNdouble&);
  // irint

  friend inline lightNNNNdouble sqrt(const lightNNNNdouble&);
  // sqrt

  friend inline lightNNNNdouble exp(const lightNNNNdouble&);
  // exp

  friend inline lightNNNNdouble log(const lightNNNNdouble&);
  // log

  friend inline lightNNNNdouble sin(const lightNNNNdouble&);
  // sin

  friend inline lightNNNNdouble cos(const lightNNNNdouble&);
  // cos

  friend inline lightNNNNdouble tan(const lightNNNNdouble&);
  // tan

  friend inline lightNNNNdouble asin(const lightNNNNdouble&);
  // asin

  friend inline lightNNNNdouble acos(const lightNNNNdouble&);
  // acos

  friend inline lightNNNNdouble atan(const lightNNNNdouble&);
  // atan

  friend inline lightNNNNdouble sinh(const lightNNNNdouble&);
  // sinh

  friend inline lightNNNNdouble cosh(const lightNNNNdouble&);
  // cosh

  friend inline lightNNNNdouble tanh(const lightNNNNdouble&);
  // tanh

  friend inline lightNNNNdouble asinh(const lightNNNNdouble&);
  // asinh

  friend inline lightNNNNdouble acosh(const lightNNNNdouble&);
  // acosh

  friend inline lightNNNNdouble atanh(const lightNNNNdouble&);
  // atanh

  friend inline lightNNNNlm_complex ifloor(const lightNNNNlm_complex&);
  // ifloor

  friend inline lightNNNNlm_complex iceil(const lightNNNNlm_complex&);
  // iceil

  friend inline lightNNNNlm_complex irint(const lightNNNNlm_complex&);
  // irint

  friend inline lightNNNNlm_complex sqrt(const lightNNNNlm_complex&);
  // sqrt

  friend inline lightNNNNlm_complex exp(const lightNNNNlm_complex&);
  // exp

  friend inline lightNNNNlm_complex log(const lightNNNNlm_complex&);
  // log

  friend inline lightNNNNlm_complex sin(const lightNNNNlm_complex&);
  // sin

  friend inline lightNNNNlm_complex cos(const lightNNNNlm_complex&);
  // cos

  friend inline lightNNNNlm_complex tan(const lightNNNNlm_complex&);
  // tan

  friend inline lightNNNNlm_complex asin(const lightNNNNlm_complex&);
  // asin

  friend inline lightNNNNlm_complex acos(const lightNNNNlm_complex&);
  // acos

  friend inline lightNNNNlm_complex atan(const lightNNNNlm_complex&);
  // atan

  friend inline lightNNNNlm_complex sinh(const lightNNNNlm_complex&);
  // sinh

  friend inline lightNNNNlm_complex cosh(const lightNNNNlm_complex&);
  // cosh

  friend inline lightNNNNlm_complex tanh(const lightNNNNlm_complex&);
  // tanh

  friend inline lightNNNNlm_complex asinh(const lightNNNNlm_complex&);
  // asinh

  friend inline lightNNNNlm_complex acosh(const lightNNNNlm_complex&);
  // acosh

  friend inline lightNNNNlm_complex atanh(const lightNNNNlm_complex&);
  // atanh

#ifdef IN_LIGHTNNNNint_I_H
  friend inline lightNNNdouble light_mean     ( const  lightNNNNint& );
  friend inline lightNNNdouble light_standard_deviation( const  lightNNNNint& );
  friend inline lightNNNdouble light_variance ( const  lightNNNNint& );
  friend inline lightNNNdouble light_median   ( const  lightNNNNint& );
#else
  friend inline lightNNNint light_mean     ( const  lightNNNNint& );
  friend inline lightNNNint light_variance ( const  lightNNNNint& );
  friend inline lightNNNint light_standard_deviation( const  lightNNNNint& );
  friend inline lightNNNint light_median   ( const  lightNNNNint& );
#endif
  
  friend inline lightNNNint light_total    ( const  lightNNNNint& );

  friend inline lightNNNNint light_sort (const lightNNNNint arr1);
  
  friend inline lightNint light_flatten (const lightNNNNint s);
  friend inline lightNint light_flatten (const lightNNNNint s, int level);
  friend inline lightNNNNint light_flatten0 (const lightNNNNint s);
  friend inline lightNNNint light_flatten1 (const lightNNNNint s);
  friend inline lightNNint light_flatten2 (const lightNNNNint s);

  //<ignore>
#ifdef IN_LIGHTNNNNlm_complex_I_H  
  friend class lightNNNNint;
#endif  
  friend class lightNint;
  friend class lightNNint;
  friend class lightNNNint;

  friend inline lightNNNNint light_join (const lightNNNNint, const lightNNNNint);
  friend inline lightNNNNint light_drop (const lightNNNNint arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNNNdouble caNNNot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNNNdouble::lightNNNNint(const lightNNNNint& s1, const double e, const lightmat_plus_enum)

  int *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // Size of the first index.

  int size2;
  // Size of the second index.

  int size3;
  // Size of the third index.

  int size4;
  // Size of the fourth index.


protected:
  int sarea[LIGHTNNNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNNNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  lightNNNNint(const int, const int, const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // four arguments is the size of the created tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNNNint(const lightNNNNint&, const lightNNNNint&, const lightmat_plus_enum);
  lightNNNNint(const lightNNNNint&, const int, const lightmat_plus_enum);

#ifdef IN_LIGHTNNNNdouble_I_H
  lightNNNNint(const lightNNNNdouble&, const int, const lightmat_plus_enum);
  lightNNNNint(const lightNNNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNNNint(const lightNNNNint&, const lightNNNNint&, const lightmat_minus_enum);
  lightNNNNint(const int, const lightNNNNint&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNNNdouble_I_H
  lightNNNNint(const int, const lightNNNNdouble&, const lightmat_minus_enum);
  lightNNNNint(const double, const lightNNNNint&, const lightmat_minus_enum);
#endif

  lightNNNNint(const lightNNNNint&, const int, const lightmat_mult_enum);
#ifdef IN_LIGHTNNNNdouble_I_H
  lightNNNNint(const lightNNNNdouble&, const int, const lightmat_mult_enum);
  lightNNNNint(const lightNNNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNNNint(const lightNNNNint&, const lightNNint&, const lightmat_mult_enum);
  lightNNNNint(const lightNNint&, const lightNNNNint&, const lightmat_mult_enum);
  lightNNNNint(const lightNNNNint&, const light33int&, const lightmat_mult_enum);
  lightNNNNint(const light33int&, const lightNNNNint&, const lightmat_mult_enum);
  lightNNNNint(const lightNNNNint&, const light44int&, const lightmat_mult_enum);
  lightNNNNint(const light44int&, const lightNNNNint&, const lightmat_mult_enum);
  lightNNNNint(const lightNNNint&, const lightNNNint&, const lightmat_mult_enum);

  lightNNNNint(const int, const lightNNNNint&, const lightmat_div_enum);
#ifdef IN_LIGHTNNNNdouble_I_H
  lightNNNNint(const int, const lightNNNNdouble&, const lightmat_div_enum);
  lightNNNNint(const double, const lightNNNNint&, const lightmat_div_enum);
#endif

  lightNNNNint(const lightNNNNint&, const lightNNNNint&, const lightmat_pow_enum);
  lightNNNNint(const lightNNNNint&, const int, const lightmat_pow_enum);
  lightNNNNint(const int, const lightNNNNint&, const lightmat_pow_enum);
 

#ifdef IN_LIGHTNNNNlm_complex_I_H
 #else
  #ifdef IN_LIGHTNNNNdouble_I_H
   lightNNNNint(const lightNNNNint&, const lightmat_abs_enum);
   lightNNNNint(const lightNNNNlm_complex&, const lightmat_abs_enum);
  #else
  lightNNNNint(const lightNNNNint&, const lightmat_abs_enum);
  #endif
#endif


  lightNNNNint(const lightNNNNint&, const lightNNNNint&, const lightmat_eprod_enum);
  lightNNNNint(const lightNNNNint&, const lightNNNNint&, const lightmat_equot_enum);
  lightNNNNint(const lightNNNNint&, int f(int), const lightmat_apply_enum);
  lightNNNNint(const lightNNNNint&, const lightNNNNint&, int f(int, int), const lightmat_apply_enum);
  lightNNNNint(const lightNNNNint&, const lightmat_trans_enum);
};

typedef lightNNNNdouble doubleNNNN;
typedef lightNNNNint intNNNN;
typedef lightNNNNlm_complex lm_complexNNNN;

#undef IN_LIGHTNNNNint_I_H

#endif
