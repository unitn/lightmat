inline light4int& Set (const int i0, const int val);
inline light4int& Set (const R r0, const lightNint& arr);
inline light4int& Set (const R r0, const light3int& arr);
inline light4int& Set (const R r0, const light4int& arr);
inline light4int& Set (const R r0, const int val);
inline lightNint operator() (const R r0) const;
friend inline light4double atan2 (const double e, const light4int &s1);
friend inline light4double atan2 (const double e, const light4double &s1);
friend inline light4double atan2 (const light4int &s1, const double e);
friend inline light4double atan2 (const light4double &s1, const double e);
friend inline light4double atan2 (const light4int &s1, const light4int &s2);
friend inline light4double atan2 (const light4int &s1, const light4double &s2);
friend inline light4double atan2 (const light4double &s1, const light4int &s2);
friend inline light4double atan2 (const light4double &s1, const light4double &s2);
friend inline light4double atan2 (const light4int &s1, const int e);
friend inline light4double atan2 (const light4double &s1, const int e);
friend inline light4double atan2 (const int e, const light4int &s1);
friend inline light4double atan2 (const int e, const light4double &s1);

#ifdef IN_LIGHT4double_I_H
inline light4double(const double e, const light4int &s1, const lightmat_atan2_enum);
inline light4double(const double e, const light4double &s1, const lightmat_atan2_enum);
inline light4double(const light4int &s1, const double e, const lightmat_atan2_enum);
inline light4double(const light4double &s1, const double e, const lightmat_atan2_enum);
inline light4double(const light4int &s1, const light4int &s2, const lightmat_atan2_enum);
inline light4double(const light4int &s1, const light4double &s2, const lightmat_atan2_enum);
inline light4double(const light4double &s1, const light4int &s2, const lightmat_atan2_enum);
inline light4double(const light4double &s1, const light4double &s2, const lightmat_atan2_enum);
inline light4double(const light4int &s1, const int e, const lightmat_atan2_enum);
inline light4double(const light4double &s1, const int e, const lightmat_atan2_enum);
inline light4double(const int e, const light4int &s1, const lightmat_atan2_enum);
inline light4double(const int e, const light4double &s1, const lightmat_atan2_enum);

#endif
