//           -*- c++ -*-

#ifndef LIGHTN3_I_H
#define LIGHTN3_I_H
// <cd> lightN3int
//
// .SS Functionality
//
// lightN3 is a template for classes that implement matrices with 3
// columns and any number of rows. E.g. a matrix v with 5x3 elements
// of type double can be instanciated with:
//
// <code>lightN3&lt;double&gt; v(5);</code>
//
// The number of rows in the matrix can change during execution. It
// changes its size to whatever is needed when it is assigned a new
// value with an assignment operator.
//
// When a lightN3 object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightN3 object with 5x3 elements to one with 7x3 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#define LIGHTN3_SIZE 10
// FOOFOO

 class lightN;
 class light33;
 class light44;
 class lightNN;


class lightN3int {
public:
  lightN3int();
  // Default constructor.

  lightN3int(const lightN3int&);
  // Copy constructor.

  lightN3int(const light33int&);
  // Conversion.

  lightN3int(const int);
  // Construct a lightN3 with a given number of rows.

  lightN3int(const int, const int *);
  // Construct a lightN3 with a given number of rows and initialize the
  // elements with values from an array (in row major order).

  lightN3int(const int, const int);
  // Construct a lightN3 with a given number of rows and initialize the
  // elements with a value (the second argument).

  ~lightN3int();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightN3double();
  // Convert to double.
#else
  friend class lightN3int;
#endif
#ifdef LIGHTMAT_TEMPLATES
  // friend class lightN3int;
#endif
  lightN3int& operator=(const lightN3int&);
  // Assignment.
  
  lightN3int& operator=(const light33int&);
  // Assignment, change size to 3x3.

  lightN3int& operator=(const lightNNint&);
  // Assignment from a lightNN with 3 columns.

  lightN3int& operator=(const int);
  // Assign one value to all elements.

  int operator()(const int, const int) const;
  // Get the value of one element.

  int& operator()(const int, const int);
  // Get/Set the value of one element.

  //<ignore>
//FOOFOO    light3int operator()(const int) const;
  //</ignore>

  const light3int& operator()(const int) const;
  // Get the value of one row.

  light3int& operator()(const int);
  // Get/Set the value of one row.
 
  int operator==(const lightN3int&) const;
  // Equality.

  int operator!=(const lightN3int&) const;
  // Inequality.

  lightN3int& operator+=(const int);
  // Add a value to all elements.

  lightN3int& operator+=(const lightN3int&);
  // Elementwise addition.

  lightN3int& operator-=(const int);
  // Subtract a value from all elements.

  lightN3int& operator-=(const lightN3int&);
  // Elementwise subtraction.

  lightN3int& operator*=(const int);
  // Mulitply all elements with a value.

  lightN3int& operator/=(const int);
  // Divide all elements with a value.

  lightN3int& reshape(const int, const int, const lightNint&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. All vector-columns of the lightNN object will get
  // the value of the lightN-argument. The value of the first argument
  // must be the same as the number of elements in the lightN-vector
  // and the second argument must be 3. The program may dump core or
  // behave strangely if the arguments are incorrect.

  int dimension(const int) const;
  // Get size of some dimension (dimension(2) == 3 for lightN3).

  void Get(int *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const int *);
  // Set values of all elements from array (row major order).

  lightN3int operator+() const;
  // Unary plus.

  lightN3int operator-() const;
  // Unary minus.

  //<ignore>
  // friend class lightN3int;
  friend class light3int;
  friend class lightNint;
  friend class light33int;
  friend class lightNNint;
  //</ignore>

  friend inline lightN3int operator+(const lightN3int&, const lightN3int&);
  // Elementwise addition.

  friend inline lightN3int operator+(const lightN3int&, const int);
  // Addition to all elements.

  friend inline lightN3int operator+(const int, const lightN3int&);
  // Addition to all elements.

  friend inline lightN3int operator-(const lightN3int&, const lightN3int&);
  // Elementwise subtraction.

  friend inline lightN3int operator-(const lightN3int&, const int);
  // Subtraction from all elements.

  friend inline lightN3int operator-(const int, const lightN3int&);
  // Subtraction to all elements.

  friend inline lightN3int operator*(const lightN3int&, const lightN3int&);
  // Inner product.

  friend inline lightN3int operator*(const lightN3int&, const int);
  // Multiply all elements.

  friend inline lightN3int operator*(const int, const lightN3int&);
  // Multiply all elements.

  friend inline lightN3int operator*(const lightN3int&, const light33int&);
  // Inner product.

  friend inline lightN3int operator*(const light44int&, const lightN3int&);
  // Inner product.

  friend inline lightN3int operator*(const lightNNint&, const lightN3int&);
  // Inner product.

  friend inline lightN3int operator/(const lightN3int&, const int);
  // Divide all elements.

  friend inline lightN3int operator/(const int, const lightN3int&);
  // Divide all elements.

  friend inline lightN3int pow(const lightN3int&, const lightN3int&);
  // Raise to the power of-function, elementwise.

  friend inline lightN3int pow(const lightN3int&, const int);
  // Raise to the power of-function, for all elements.

  friend inline lightN3int pow(const int, const lightN3int&);
  // Raise to the power of-function, for all elements.

  friend inline lightN3int ElemProduct(const lightN3int&, const lightN3int&);
  // Elementwise multiplication.

  friend inline lightN3int ElemQuotient(const lightN3int&, const lightN3int&);
  // Elementwise division.

  friend inline lightN3int Apply(const lightN3int&, int f(int));
  // Apply the function elementwise all elements.

  friend inline lightN3int Apply(const lightN3int&, const lightN3int&, int f(int, int));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline lightN3int abs(const lightN3int&);
  // abs

  friend inline lightN3int sign(const lightN3int&);
  // sign

  friend inline lightN3int sign(const lightN3double&);
  // sign

  friend inline lightN3int ifloor(const lightN3double&);
  // ifloor

  friend inline lightN3int iceil(const lightN3double&);
  // iceil

  friend inline lightN3int irint(const lightN3double&);
  // irint

  friend inline lightN3double sqrt(const lightN3double&);
  // sqrt

  friend inline lightN3double exp(const lightN3double&);
  // exp

  friend inline lightN3double log(const lightN3double&);
  // log

  friend inline lightN3double sin(const lightN3double&);
  // sin

  friend inline lightN3double cos(const lightN3double&);
  // cos

  friend inline lightN3double tan(const lightN3double&);
  // tan

  friend inline lightN3double asin(const lightN3double&);
  // asin

  friend inline lightN3double acos(const lightN3double&);
  // acos

  friend inline lightN3double atan(const lightN3double&);
  // atan

  friend inline lightN3double sinh(const lightN3double&);
  // sinh

  friend inline lightN3double cosh(const lightN3double&);
  // cosh

  friend inline lightN3double tanh(const lightN3double&);
  // tanh

  friend inline lightN3double asinh(const lightN3double&);
  // asinh

  friend inline lightN3double acosh(const lightN3double&);
  // acosh

  friend inline lightN3double atanh(const lightN3double&);
  // atanh
 
  friend inline lightN3lm_complex ifloor(const lightN3lm_complex&);
  // ifloor

  friend inline lightN3lm_complex iceil(const lightN3lm_complex&);
  // iceil

  friend inline lightN3lm_complex irint(const lightN3lm_complex&);
  // irint

  friend inline lightN3lm_complex sign(const lightN3lm_complex&);

  friend inline lightN3lm_complex sqrt(const lightN3lm_complex&);
  // sqrt

  friend inline lightN3lm_complex exp(const lightN3lm_complex&);
  // exp

  friend inline lightN3lm_complex log(const lightN3lm_complex&);
  // log

  friend inline lightN3lm_complex sin(const lightN3lm_complex&);
  // sin

  friend inline lightN3lm_complex cos(const lightN3lm_complex&);
  // cos

  friend inline lightN3lm_complex tan(const lightN3lm_complex&);
  // tan

  friend inline lightN3lm_complex asin(const lightN3lm_complex&);
  // asin

  friend inline lightN3lm_complex acos(const lightN3lm_complex&);
  // acos

  friend inline lightN3lm_complex atan(const lightN3lm_complex&);
  // atan

  friend inline lightN3lm_complex sinh(const lightN3lm_complex&);
  // sinh

  friend inline lightN3lm_complex cosh(const lightN3lm_complex&);
  // cosh

  friend inline lightN3lm_complex tanh(const lightN3lm_complex&);
  // tanh

  friend inline lightN3lm_complex asinh(const lightN3lm_complex&);
  // asinh

  friend inline lightN3lm_complex acosh(const lightN3lm_complex&);
  // acosh

  friend inline lightN3lm_complex atanh(const lightN3lm_complex&);
  // atanh

protected:
//FOOFOO    light3int sarea[LIGHTN3_SIZE];

  light3int *elem;
  // The rows are stored here.

  int size;
  // The number of rows.

  int alloc_size;
  // The size of the allocated area (number of rows)

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  lightN3int(const int x, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // argument is the number of rows in the matrix. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightN3int(const lightN3int&, const lightN3int&, const lightmat_plus_enum);
  lightN3int(const lightN3int&, const int, const lightmat_plus_enum);
  lightN3int(const lightN3int&, const lightN3int&, const lightmat_minus_enum);
  lightN3int(const int, const lightN3int&, const lightmat_minus_enum);
  lightN3int(const lightN3int&, const lightmat_minus_enum);
  lightN3int(const lightN3int&, const lightN3int&, const lightmat_mult_enum);
  lightN3int(const lightN3int&, const int, const lightmat_mult_enum);
  lightN3int(const lightNNint&, const lightN3int&, const lightmat_mult_enum);
  lightN3int(const lightN3int&, const light33int&, const lightmat_mult_enum);
  lightN3int(const light44int&, const lightN3int&, const lightmat_mult_enum);
  lightN3int(const int, const lightN3int&, const lightmat_div_enum);
  lightN3int(const lightN3int&, const lightN3int&, const lightmat_pow_enum);
  lightN3int(const lightN3int&, const int, const lightmat_pow_enum);
  lightN3int(const int, const lightN3int&, const lightmat_pow_enum);
  lightN3int(const lightN3int&, const lightmat_abs_enum);
  lightN3int(const lightN3int&, const lightN3int&, const lightmat_eprod_enum);
  lightN3int(const lightN3int&, const lightN3int&, const lightmat_equot_enum);
  lightN3int(const lightN3int&, int f(int), const lightmat_apply_enum);
  lightN3int(const lightN3int&, const lightN3int&, int f(int, int), const lightmat_apply_enum);
};

typedef lightN3double doubleN3;
typedef lightN3int intN3;

#endif
