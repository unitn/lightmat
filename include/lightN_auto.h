inline lightN<T>& Set (const int i0, const T val);
inline lightN<T>& Set (const R r0, const lightN<T>& arr);
inline lightN<T>& Set (const R r0, const light3<T>& arr);
inline lightN<T>& Set (const R r0, const light4<T>& arr);
inline lightN<T>& Set (const R r0, const T val);
inline lightN<T> operator() (const R r0) const;

#ifdef IN_LIGHTNdouble_H
inline lightNdouble(const double e, const lightNint &s1, const lightmat_atan2_enum);
inline lightNdouble(const double e, const lightNdouble &s1, const lightmat_atan2_enum);
inline lightNdouble(const lightNint &s1, const double e, const lightmat_atan2_enum);
inline lightNdouble(const lightNdouble &s1, const double e, const lightmat_atan2_enum);
inline lightNdouble(const lightNint &s1, const lightNint &s2, const lightmat_atan2_enum);
inline lightNdouble(const lightNint &s1, const lightNdouble &s2, const lightmat_atan2_enum);
inline lightNdouble(const lightNdouble &s1, const lightNint &s2, const lightmat_atan2_enum);
inline lightNdouble(const lightNdouble &s1, const lightNdouble &s2, const lightmat_atan2_enum);
inline lightNdouble(const lightNint &s1, const int e, const lightmat_atan2_enum);
inline lightNdouble(const lightNdouble &s1, const int e, const lightmat_atan2_enum);
inline lightNdouble(const int e, const lightNint &s1, const lightmat_atan2_enum);
inline lightNdouble(const int e, const lightNdouble &s1, const lightmat_atan2_enum);

#endif
