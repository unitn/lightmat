inline light3lm_complex& Set (const int i0, const lm_complex val);
inline light3lm_complex& Set (const R r0, const lightNlm_complex& arr);
inline light3lm_complex& Set (const R r0, const light3lm_complex& arr);
inline light3lm_complex& Set (const R r0, const light4lm_complex& arr);
inline light3lm_complex& Set (const R r0, const lm_complex val);
inline lightNlm_complex operator() (const R r0) const;
friend inline light3double atan2 (const double e, const light3int &s1);
friend inline light3double atan2 (const double e, const light3double &s1);
friend inline light3double atan2 (const light3int &s1, const double e);
friend inline light3double atan2 (const light3double &s1, const double e);
friend inline light3double atan2 (const light3int &s1, const light3int &s2);
friend inline light3double atan2 (const light3int &s1, const light3double &s2);
friend inline light3double atan2 (const light3double &s1, const light3int &s2);
friend inline light3double atan2 (const light3double &s1, const light3double &s2);
friend inline light3double atan2 (const light3int &s1, const int e);
friend inline light3double atan2 (const light3double &s1, const int e);
friend inline light3double atan2 (const int e, const light3int &s1);
friend inline light3double atan2 (const int e, const light3double &s1);

#ifdef IN_LIGHT3double_C_H
inline light3double(const double e, const light3int &s1, const lightmat_atan2_enum);
inline light3double(const double e, const light3double &s1, const lightmat_atan2_enum);
inline light3double(const light3int &s1, const double e, const lightmat_atan2_enum);
inline light3double(const light3double &s1, const double e, const lightmat_atan2_enum);
inline light3double(const light3int &s1, const light3int &s2, const lightmat_atan2_enum);
inline light3double(const light3int &s1, const light3double &s2, const lightmat_atan2_enum);
inline light3double(const light3double &s1, const light3int &s2, const lightmat_atan2_enum);
inline light3double(const light3double &s1, const light3double &s2, const lightmat_atan2_enum);
inline light3double(const light3int &s1, const int e, const lightmat_atan2_enum);
inline light3double(const light3double &s1, const int e, const lightmat_atan2_enum);
inline light3double(const int e, const light3int &s1, const lightmat_atan2_enum);
inline light3double(const int e, const light3double &s1, const lightmat_atan2_enum);

#endif
