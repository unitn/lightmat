//           -*- c++ -*-

#ifndef LIGHTNNN_C_H
#define LIGHTNNN_C_H
// <cd> lightNNNlm_complex
//
// .SS Functionality
//
// lightNNN is a template for classes that implement tensors of rank 3
// with any number of elements. E.g. a lightNNN s with 5x6x7 elements of
// type double can be instanciated with:
//
// <code>lightNNN&lt;double&gt; s(5,6,7);</code>
//
// The size of the tensor can change during execution. It changes its
// size to whatever is needed when it is assigned a new value with an
// assignment operator.
//
// When a lightNNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNNN object with 5x6x5 elements to one with 5x5x6 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHTNNNlm_complex_C_H

#define LIGHTNNN_SIZE1 5
#define LIGHTNNN_SIZE2 5
#define LIGHTNNN_SIZE3 5
#define LIGHTNNN_SIZE (LIGHTNNN_SIZE1*LIGHTNNN_SIZE2*LIGHTNNN_SIZE3)
// The default size.

 class lightN;
 class lightNN;
 class lightN33;
 class lightNNNN;


class lightNNNlm_complex {
public:

#ifdef IN_LIGHTNNNdouble_C_H
  friend class lightNNNint;
#else
  friend class lightNNNdouble;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNNint;
#endif

  #include "c_lightNNN_auto.h"

  inline lightNNNlm_complex();
  // Default constructor.

  inline lightNNNlm_complex(const lightNNNlm_complex&);
  // Copy constructor.

  inline lightNNNlm_complex(const lightN33lm_complex&);
  // Conversion.

  inline lightNNNlm_complex(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNNlm_complex(const int, const int, const int);
  // Construct a lightNNN of given size.

  inline lightNNNlm_complex(const int, const int, const int, const lm_complex *);
  // Construct a lightNNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNNlm_complex(const int, const int, const int, const lm_complex);
  // Construct a lightNNN of given size and initialize all elements
  // with a value (the last argument).

  inline ~lightNNNlm_complex();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNNlm_complex();
  // Convert to complex.
  
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNNlm_complex();
  // Convert to complex.
#endif

  lightNNNlm_complex& operator=(const lightNNNlm_complex&);
  // Assignment.

  lightNNNlm_complex& operator=(const lightN33lm_complex&);
  // Assignment, change size to Nx3x3.

  lightNNNlm_complex& operator=(const lm_complex);
  // Assign one value to all elements.

  lm_complex operator() (const int x, const int y, const int z) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3));
#ifdef ROWMAJOR
    return elem[((x-1)*size2+(y-1))*size3+z-1];
#else
    return elem[((z-1)*size2+(y-1))*size1+x-1];
#endif
  };
  // Get the value of one element.


  lm_complex& operator()(const int x, const int y, const int z) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) || (z<1) || (z>size3));
#ifdef ROWMAJOR
    return elem[((x-1)*size2+(y-1))*size3+z-1];
#else
    return elem[((z-1)*size2+(y-1))*size1+x-1];
#endif
  };
  // Get/Set the value of one element.

  lightNlm_complex operator()(const int, const int) const;
  // Get the value of a vector in the tensor.

  lightNNlm_complex operator()(const int) const;
  // Get the value of a matrix in the tensor.




  //
  // Set
  //

  inline lightNNNlm_complex& Set (const int i0, const int i1, const lm_complex val);
  inline lightNNNlm_complex& Set (const int i0, const int i1, const lightNlm_complex& arr);
  inline lightNNNlm_complex& Set (const int i0, const int i1, const light3lm_complex& arr);
  inline lightNNNlm_complex& Set (const int i0, const int i1, const light4lm_complex& arr);

  inline lightNNNlm_complex& Set (const int i0, const lm_complex val);
  inline lightNNNlm_complex& Set (const int i0, const lightNNlm_complex& arr);
  inline lightNNNlm_complex& Set (const int i0, const light33lm_complex& arr);
  inline lightNNNlm_complex& Set (const int i0, const light44lm_complex& arr);


 
  int operator==(const lightNNNlm_complex&) const;
  // Equality.

  int operator!=(const lightNNNlm_complex&) const;
  // Inequality.
 
  lightNNNlm_complex& operator+=(const lm_complex);
  // Add a value to all elements.

  lightNNNlm_complex& operator+=(const lightNNNlm_complex&);
  // Elementwise addition.

  lightNNNlm_complex& operator-=(const lm_complex);
  // Subtract a value from all elements.

  lightNNNlm_complex& operator-=(const lightNNNlm_complex&);
  // Elementwise subtraction.

  lightNNNlm_complex& operator*=(const lm_complex);
  // Mulitply all elements with a value.

  lightNNNlm_complex& operator/=(const lm_complex);
  // Divide all elements with a value.

  lm_complex Extract(const lightNint&) const;
  // Extract an element using given index. 

  inline lightNNNlm_complex& reshape(const int, const int, const int, const lightNlm_complex&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c) will be set to s(a) in the lightNNNlm_complex
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector. The program may dump
  // core or behave strangely if the arguments are incorrect.

  inline lightNNNlm_complex& reshape(const int, const int, const int, const lightNNlm_complex&);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c) will be set to s(a,b) in the
  // lightNNN object. The values of the first two arguments must be
  // the same as the size of the lightNN-matrix. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNlm_complex& SetShape(const int x=-1, const int y=-1, const int z=-1); 
  // Set new shape for the tensor.

  const  lightNNlm_complex& SetMatrix1(const int n,const lightNNlm_complex& x);
  // Assigns Tensor[n,_,_]=Matrix

  const  lightNlm_complex& SetVector12(const int n,const int m,const lightNlm_complex& x);
  // Assigns Tensor[n,m,_]=Vector 

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else if(x == 3)
      return size3;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(lm_complex *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const lm_complex *);
  // Set values of all elements from array (row major order).
 
  lm_complex * data() const;
  // Direct access to the stored data. Use the result with care.

  lightNNNlm_complex operator+() const;
  // Unary plus.

  lightNNNlm_complex operator-() const;
  // Unary minus.

  friend inline lightNNNlm_complex operator+(const lightNNNlm_complex&, const lightNNNlm_complex&);
  // Elementwise addition.

  friend inline lightNNNlm_complex operator+(const lightNNNlm_complex&, const lm_complex);
  // Addition to all elements.

  friend inline lightNNNlm_complex operator+(const lm_complex, const lightNNNlm_complex&);
  // Addition to all elements.

#ifdef IN_LIGHTNNNdouble_C_H
  friend inline lightNNNdouble operator+(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator+(const lightNNNint&, const double);
  friend inline lightNNNdouble operator+(const double, const lightNNNint&);
  friend inline lightNNNdouble operator+(const int, const lightNNNdouble&);
#endif

  #ifdef IN_LIGHTNNNlm_complex_C_H
friend inline lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator+(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator+(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator+(const double e, const lightNNNlm_complex& s2);
#endif

  friend inline lightNNNlm_complex operator-(const lightNNNlm_complex&, const lightNNNlm_complex&);
  // Elementwise subtraction.

  friend inline lightNNNlm_complex operator-(const lightNNNlm_complex&, const lm_complex);
  // Subtraction from all elements.

  friend inline lightNNNlm_complex operator-(const lm_complex, const lightNNNlm_complex&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNNdouble_C_H
  friend inline lightNNNdouble operator-(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator-(const lightNNNint&, const double);
  friend inline lightNNNdouble operator-(const double, const lightNNNint&);
  friend inline lightNNNdouble operator-(const int, const lightNNNdouble&);
#endif

  #ifdef IN_LIGHTNNNlm_complex_C_H
friend inline lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator-(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator-(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator-(const double e, const lightNNNlm_complex& s2);

#endif

  friend inline lightNNNlm_complex operator*(const lightNNNlm_complex&, const lm_complex);
  // Multiply all elements.

  friend inline lightNNNlm_complex operator*(const lm_complex, const lightNNNlm_complex&);
  // Multiply all elements.

#ifdef IN_LIGHTNNNdouble_C_H
  friend inline lightNNNdouble operator*(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator*(const lightNNNint&, const double);
  friend inline lightNNNdouble operator*(const double, const lightNNNint&);
  friend inline lightNNNdouble operator*(const int, const lightNNNdouble&);
#endif

#ifdef IN_LIGHTNNNlm_complex_C_H
friend inline lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator*(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator*(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator*(const double e, const lightNNNlm_complex& s2);
#endif

  friend inline lightNNNlm_complex operator*(const lightNNNlm_complex&, const lightNNlm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const lightNNlm_complex&, const lightNNNlm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const lightNNNNlm_complex&, const lightNlm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const lightNlm_complex&,	const lightNNNNlm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const lightNNNlm_complex&, const light33lm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const light33lm_complex&, const lightNNNlm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const lightNNNNlm_complex&, const light3lm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const light3lm_complex&, const lightNNNNlm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const lightNNNlm_complex&, const light44lm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const light44lm_complex&, const lightNNNlm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const lightNNNNlm_complex&, const light4lm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator*(const light4lm_complex&, const lightNNNNlm_complex&);
  // Inner product.

  friend inline lightNNNlm_complex operator/(const lightNNNlm_complex&, const lm_complex);
  // Divide all elements.

  friend inline lightNNNlm_complex operator/(const lm_complex, const lightNNNlm_complex&);
  // Divide all elements.

#ifdef IN_LIGHTNNNdouble_C_H
  friend inline lightNNNdouble operator/(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator/(const lightNNNint&, const double);
  friend inline lightNNNdouble operator/(const double, const lightNNNint&);
  friend inline lightNNNdouble operator/(const int, const lightNNNdouble&);
#endif

#ifdef IN_LIGHTNNNlm_complex_C_H
friend inline lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator/(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator/(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator/(const double e, const lightNNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNNlm_complex_C_H
friend inline lightNNNdouble arg(const lightNNNlm_complex& a);
friend inline lightNNNdouble re(const lightNNNlm_complex& a);
friend inline lightNNNdouble im(const lightNNNlm_complex& a);
friend inline lightNNNlm_complex conjugate(const lightNNNlm_complex& a);
//#endif

  friend inline lightNNNlm_complex pow(const lightNNNlm_complex&, const lightNNNlm_complex&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNNlm_complex pow(const lightNNNlm_complex&, const lm_complex);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNlm_complex pow(const lm_complex, const lightNNNlm_complex&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNlm_complex ElemProduct(const lightNNNlm_complex&, const lightNNNlm_complex&);
  // Elementwise multiplication.

  friend inline lightNNNlm_complex ElemQuotient(const lightNNNlm_complex&, const lightNNNlm_complex&);
  // Elementwise division.

  friend inline lightNNNlm_complex Apply(const lightNNNlm_complex&, lm_complex f(lm_complex));
  // Apply the function elementwise all elements.

  friend inline lightNNNlm_complex Apply(const lightNNNlm_complex&, const lightNNNlm_complex&, lm_complex f(lm_complex, lm_complex));
  // Apply the function elementwise on all elements in the two tensors.

  friend inline lightNNNlm_complex Transpose(const lightNNNlm_complex&);
  // Transpose.

#ifdef IN_LIGHTNNNlm_complex_C_H
#else
  friend inline lightNNNlm_complex abs(const lightNNNlm_complex&);
#ifdef IN_LIGHTNNNdouble_C_H
  friend inline lightNNNdouble abs(const lightNNNlm_complex&);
#endif 
#endif


#ifndef COMPLEX_TOOLS
  // sign
  friend inline lightNNNint sign(const lightNNNint&);
  // sign
  friend inline lightNNNint sign(const lightNNNdouble&);
  // abs
#else
  friend inline lightNNNlm_complex sign(const lightNNNlm_complex&);
#endif

 /// Added 2/2/98 

  friend inline lightNNNlm_complex  FractionalPart(const lightNNNlm_complex&);
  //  FractionalPart

  friend inline lightNNNlm_complex  IntegerPart(const lightNNNlm_complex&);
  //  IntegerPart

  //friend inline lightNNNint  IntegerPart(const lightNNNdouble&);
  //  IntegerPart

  friend inline lightNNNlm_complex Mod (const  lightNNNlm_complex&,const  lightNNNlm_complex&);
  // Mod
 
  friend inline lightNNNlm_complex Mod (const  lightNNNlm_complex&,const lm_complex);
  // Mod

  friend inline lightNNNlm_complex Mod (const lm_complex,const  lightNNNlm_complex&);
  // Mod

  friend inline lm_complex LightMax (const lightNNNlm_complex& );
  // Max

  friend inline lm_complex LightMin (const lightNNNlm_complex& );
  // Min

  friend inline lm_complex findLightMax (const  lm_complex *,const  int );
  // Find Max
 
  friend inline lm_complex findLightMin (const  lm_complex *,const  int );
  // Find Min
 
  /// End Added 



  friend inline lightNNNint ifloor(const lightNNNdouble&);
  // ifloor

  friend inline lightNNNint iceil(const lightNNNdouble&);
  // iceil

  friend inline lightNNNint irint(const lightNNNdouble&);
  // irint

  friend inline lightNNNdouble sqrt(const lightNNNdouble&);
  // sqrt

  friend inline lightNNNdouble exp(const lightNNNdouble&);
  // exp

  friend inline lightNNNdouble log(const lightNNNdouble&);
  // log

  friend inline lightNNNdouble sin(const lightNNNdouble&);
  // sin

  friend inline lightNNNdouble cos(const lightNNNdouble&);
  // cos

  friend inline lightNNNdouble tan(const lightNNNdouble&);
  // tan

  friend inline lightNNNdouble asin(const lightNNNdouble&);
  // asin

  friend inline lightNNNdouble acos(const lightNNNdouble&);
  // acos

  friend inline lightNNNdouble atan(const lightNNNdouble&);
  // atan

  friend inline lightNNNdouble sinh(const lightNNNdouble&);
  // sinh

  friend inline lightNNNdouble cosh(const lightNNNdouble&);
  // cosh

  friend inline lightNNNdouble tanh(const lightNNNdouble&);
  // tanh

  friend inline lightNNNdouble asinh(const lightNNNdouble&);
  // asinh

  friend inline lightNNNdouble acosh(const lightNNNdouble&);
  // acosh

  friend inline lightNNNdouble atanh(const lightNNNdouble&);
  // atanh

   friend inline lightNNNlm_complex ifloor(const lightNNNlm_complex&);
  // ifloor

  friend inline lightNNNlm_complex iceil(const lightNNNlm_complex&);
  // iceil

  friend inline lightNNNlm_complex irint(const lightNNNlm_complex&);
  // irint

  friend inline lightNNNlm_complex sqrt(const lightNNNlm_complex&);
  // sqrt

  friend inline lightNNNlm_complex exp(const lightNNNlm_complex&);
  // exp

  friend inline lightNNNlm_complex log(const lightNNNlm_complex&);
  // log

  friend inline lightNNNlm_complex sin(const lightNNNlm_complex&);
  // sin

  friend inline lightNNNlm_complex cos(const lightNNNlm_complex&);
  // cos

  friend inline lightNNNlm_complex tan(const lightNNNlm_complex&);
  // tan

  friend inline lightNNNlm_complex asin(const lightNNNlm_complex&);
  // asin

  friend inline lightNNNlm_complex acos(const lightNNNlm_complex&);
  // acos

  friend inline lightNNNlm_complex atan(const lightNNNlm_complex&);
  // atan

  friend inline lightNNNlm_complex sinh(const lightNNNlm_complex&);
  // sinh

  friend inline lightNNNlm_complex cosh(const lightNNNlm_complex&);
  // cosh

  friend inline lightNNNlm_complex tanh(const lightNNNlm_complex&);
  // tanh

  friend inline lightNNNlm_complex asinh(const lightNNNlm_complex&);
  // asinh

  friend inline lightNNNlm_complex acosh(const lightNNNlm_complex&);
  // acosh

  friend inline lightNNNlm_complex atanh(const lightNNNlm_complex&);
  // atanh

#ifdef IN_LIGHTNNNint_C_H
  friend inline lightNNdouble light_mean     ( const  lightNNNlm_complex& );
  friend inline lightNNdouble light_standard_deviation( const  lightNNNlm_complex& );
  friend inline lightNNdouble light_variance ( const  lightNNNlm_complex& );
  friend inline lightNNdouble light_median   ( const  lightNNNlm_complex& );
#else
  friend inline lightNNlm_complex light_mean     ( const  lightNNNlm_complex& );
  friend inline lightNNlm_complex light_variance ( const  lightNNNlm_complex& );
  friend inline lightNNlm_complex light_standard_deviation( const  lightNNNlm_complex& );
  friend inline lightNNlm_complex light_median   ( const  lightNNNlm_complex& );
#endif
  
  friend inline lightNNlm_complex light_total    ( const  lightNNNlm_complex& );
  
  friend inline lightNNNlm_complex light_sort (const lightNNNlm_complex arr1);
  
  friend inline lightNlm_complex light_flatten (const lightNNNlm_complex s);
  friend inline lightNlm_complex light_flatten (const lightNNNlm_complex s, int level);
  friend inline lightNNNlm_complex light_flatten0 (const lightNNNlm_complex s);
  friend inline lightNNlm_complex light_flatten1 (const lightNNNlm_complex s);

  //<ignore>
#ifdef IN_LIGHTNNNlm_complex_C_H
  friend class lightNNNint;
#endif

  friend class lightNlm_complex;
  friend class lightNNlm_complex;
  friend class lightN33lm_complex;
  friend class lightNNNNlm_complex;

  friend inline lightNNNlm_complex light_join (const lightNNNlm_complex, const lightNNNlm_complex);
  friend inline lightNNNlm_complex light_drop (const lightNNNlm_complex arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNNdouble::lightNNNlm_complex(const lightNNNint& s1, const double e, const lightmat_plus_enum)

  lm_complex *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // Size of the first index.

  int size2;
  // Size of the second index.

  int size3;
  // Size of the third index.

protected:
  lm_complex sarea[LIGHTNNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNNNlm_complex(const int, const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // three arguments is the size of the created tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNNlm_complex(const lightNNNlm_complex&, const lightNNNlm_complex&, const lightmat_plus_enum);
  lightNNNlm_complex(const lightNNNlm_complex&, const lm_complex, const lightmat_plus_enum);

#ifdef IN_LIGHTNNNdouble_C_H
  lightNNNlm_complex(const lightNNNdouble&, const int, const lightmat_plus_enum);
  lightNNNlm_complex(const lightNNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNNlm_complex(const lightNNNlm_complex&, const lightNNNlm_complex&, const lightmat_minus_enum);
  lightNNNlm_complex(const lm_complex, const lightNNNlm_complex&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNNdouble_C_H
  lightNNNlm_complex(const int, const lightNNNdouble&, const lightmat_minus_enum);
  lightNNNlm_complex(const double, const lightNNNint&, const lightmat_minus_enum);
#endif

  lightNNNlm_complex(const lightNNNlm_complex&, const lm_complex, const lightmat_mult_enum);

#ifdef IN_LIGHTNNNdouble_C_H
  lightNNNlm_complex(const lightNNNdouble&, const int, const lightmat_mult_enum);
  lightNNNlm_complex(const lightNNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNNlm_complex(const lightNNNlm_complex&, const lightNNlm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const lightNNlm_complex&, const lightNNNlm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const lightNNNNlm_complex&, const lightNlm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const lightNlm_complex&, const lightNNNNlm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const lightNNNlm_complex&, const light33lm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const light33lm_complex&, const lightNNNlm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const lightNNNNlm_complex&, const light3lm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const light3lm_complex&, const lightNNNNlm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const lightNNNlm_complex&, const light44lm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const light44lm_complex&, const lightNNNlm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const lightNNNNlm_complex&, const light4lm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const light4lm_complex&, const lightNNNNlm_complex&, const lightmat_mult_enum);
  lightNNNlm_complex(const lm_complex, const lightNNNlm_complex&, const lightmat_div_enum);

#ifdef IN_LIGHTNNNdouble_C_H
  lightNNNlm_complex(const int, const lightNNNdouble&, const lightmat_div_enum);
  lightNNNlm_complex(const double, const lightNNNint&, const lightmat_div_enum);
#endif

  lightNNNlm_complex(const lightNNNlm_complex&, const lightNNNlm_complex&, const lightmat_pow_enum);
  lightNNNlm_complex(const lightNNNlm_complex&, const lm_complex, const lightmat_pow_enum);
  lightNNNlm_complex(const lm_complex, const lightNNNlm_complex&, const lightmat_pow_enum);

#ifdef IN_LIGHTNNNlm_complex_C_H
 #else
  #ifdef IN_LIGHTNNNdouble_C_H
   lightNNNlm_complex(const lightNNNlm_complex&, const lightmat_abs_enum);
   lightNNNlm_complex(const lightNNNlm_complex&, const lightmat_abs_enum);
  #else
  lightNNNlm_complex(const lightNNNlm_complex&, const lightmat_abs_enum);
  #endif
#endif


  lightNNNlm_complex(const lightNNNlm_complex&, const lightNNNlm_complex&, const lightmat_eprod_enum);
  lightNNNlm_complex(const lightNNNlm_complex&, const lightNNNlm_complex&, const lightmat_equot_enum);
  lightNNNlm_complex(const lightNNNlm_complex&, lm_complex f(lm_complex), const lightmat_apply_enum);
  lightNNNlm_complex(const lightNNNlm_complex&, const lightNNNlm_complex&, lm_complex f(lm_complex, lm_complex), const lightmat_apply_enum);
  lightNNNlm_complex(const lightNNNlm_complex&, const lightmat_trans_enum);
};

typedef lightNNNdouble doubleNNN;
typedef lightNNNint intNNN;
typedef lightNNNlm_complex lm_complexNNN;

#undef IN_LIGHTNNNlm_complex_C_H

#endif
