#ifndef COMPLEX_H
#define COMPLEX_H

class lm_complex {
public:
    double re;
    double im;
    inline lm_complex ();
    inline lm_complex (int re);
    inline lm_complex (double re);
    inline lm_complex (double re,  double im); 
    inline lm_complex& operator+=(const lm_complex a);
    inline lm_complex& operator*=(const lm_complex a);
    inline lm_complex& operator-=(const lm_complex a);
    inline lm_complex& operator/=(const lm_complex a);
    inline lm_complex operator+() const;
    inline lm_complex operator-() const;
	inline int operator== (const lm_complex a) const; // Equality.
    inline int operator!= (const lm_complex a)const; // Inequality.
	
};

inline double arg(const lm_complex& a);
inline double re(const lm_complex& a);
inline double im(const lm_complex& a);
inline lm_complex conjugate(const lm_complex& a);

inline lm_complex operator+(const lm_complex& a,const lm_complex& b);
inline lm_complex operator+(const lm_complex& a,double b);
inline lm_complex operator+(double a,const lm_complex& b);


inline lm_complex operator-(const lm_complex& a,const lm_complex& b);
inline lm_complex operator-(const lm_complex& a,double b);
inline lm_complex operator-(double a,const lm_complex& b);


inline lm_complex operator*(const lm_complex& a,const lm_complex& b);
inline lm_complex operator*(const lm_complex& a,double b);
inline lm_complex operator*(double a,const lm_complex& b);


inline lm_complex operator/(const lm_complex& a,const lm_complex& b);
inline lm_complex operator/(const lm_complex& a,double b);
inline lm_complex operator/(double a,const lm_complex& b);


//inline lm_complex operator lm_complex(double b);

#endif
