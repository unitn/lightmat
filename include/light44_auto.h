inline light44<T>& Set (const int i0, const int i1, const T val);
inline light44<T>& Set (const R r0, const int i1, const lightN<T>& arr);
inline light44<T>& Set (const R r0, const int i1, const light3<T>& arr);
inline light44<T>& Set (const R r0, const int i1, const light4<T>& arr);
inline light44<T>& Set (const R r0, const int i1, const T val);
inline light44<T>& Set (const int i0, const R r1, const lightN<T>& arr);
inline light44<T>& Set (const int i0, const R r1, const light3<T>& arr);
inline light44<T>& Set (const int i0, const R r1, const light4<T>& arr);
inline light44<T>& Set (const int i0, const R r1, const T val);
inline light44<T>& Set (const R r0, const R r1, const lightNN<T>& arr);
inline light44<T>& Set (const R r0, const R r1, const light33<T>& arr);
inline light44<T>& Set (const R r0, const R r1, const light44<T>& arr);
inline light44<T>& Set (const R r0, const R r1, const T val);
inline lightN<T> operator() (const R r0, const int i1) const;
inline lightN<T> operator() (const int i0, const R r1) const;
inline lightNN<T> operator() (const R r0, const R r1) const;
friend inline light44double atan2 (const double e, const light44int &s1);
friend inline light44double atan2 (const double e, const light44double &s1);
friend inline light44double atan2 (const light44int &s1, const double e);
friend inline light44double atan2 (const light44double &s1, const double e);
friend inline light44double atan2 (const light44int &s1, const light44int &s2);
friend inline light44double atan2 (const light44int &s1, const light44double &s2);
friend inline light44double atan2 (const light44double &s1, const light44int &s2);
friend inline light44double atan2 (const light44double &s1, const light44double &s2);
friend inline light44double atan2 (const light44int &s1, const int e);
friend inline light44double atan2 (const light44double &s1, const int e);
friend inline light44double atan2 (const int e, const light44int &s1);
friend inline light44double atan2 (const int e, const light44double &s1);

#ifdef IN_LIGHT44double_H
inline light44double(const double e, const light44int &s1, const lightmat_atan2_enum);
inline light44double(const double e, const light44double &s1, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const double e, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const double e, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const light44int &s2, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const light44double &s2, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const light44int &s2, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const light44double &s2, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const int e, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const int e, const lightmat_atan2_enum);
inline light44double(const int e, const light44int &s1, const lightmat_atan2_enum);
inline light44double(const int e, const light44double &s1, const lightmat_atan2_enum);

#endif
