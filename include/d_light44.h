//           -*- c++ -*-

#ifndef LIGHT44_H
#define LIGHT44_H
// <cd> light44double
//
// .SS Functionality
//
// light44 is a template for classes that implement matrices with 4x4
// elements. E.g. a matrix v with 4x4 elements of type double can be
// instanciated with:
//
// <code>light44&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHT44double_H

 class light3;
 class light4;
 class lightN;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light44double {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light44double();
  // Convert to double.
#else
#endif
#ifdef LIGHTMAT_TEMPLATES
  //  friend class light44int;
#endif

#ifdef IN_LIGHT44double_H
   friend class  light44int;
#else
   friend class  light44double;
#endif

  #include "d_light44_auto.h"

  light44double();
  // Default constructor.

  light44double(const light44double&);
  // Copy constructor.

  inline 
  light44double(const double e11, const double e12, const double e13, const double e14,
	  const double e21, const double e22, const double e23, const double e24,
	  const double e31, const double e32, const double e33, const double e34,
	  const double e41, const double e42, const double e43, const double e44);
  // Initialize elements with values.

  light44double(const double *);
  // Initialize elements with values from an array (in row major order).

  light44double(const double);
  // Initialize all elements with the same value.

  light44double& operator=(const light44double&);
  // Assignment.

  light44double& operator=(const lightNNdouble&);
  // Assignment from a lightNN where NN=44.

  light44double& operator=(const double);
  // Assign one value to all elements.

  double operator() (const int x, const int y) const {
    limiterror((x<1) || (x>4) || (y<1) || (y>4));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*4];
#else
    return elem[x-1+(y-1)*4];
#endif
  };
  // Get the value of one element.

  double& operator()(const int x, const int y) {
    limiterror((x<1) || (x>4) || (y<1) || (y>4));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*4];
#else
    return elem[x-1+(y-1)*4];
#endif
  };
  // Get/Set the value of one element.

  light4double operator()(const int) const;
  // Get the value of one row.
 



  //
  // Set
  //

  inline light44double& Set (const int i0, const double val);
  inline light44double& Set (const int i0, const lightNdouble& arr);
  inline light44double& Set (const int i0, const light3double& arr);
  inline light44double& Set (const int i0, const light4double& arr);



  int operator==(const light44double&) const;
  // Equality.

  int operator!=(const light44double&) const;
  // Inequality.

  light44double& operator+=(const double);
  // Add a value to all elements.

  light44double& operator+=(const light44double&);
  // Elementwise addition.

  light44double& operator-=(const double);
  // Subtract a value from all elements.

  light44double& operator-=(const light44double&);
  // Elementwise subtraction

  light44double& operator*=(const double);
  // Muliply all elements with a value.

  light44double& operator/=(const double);
  // Divide all elements with a value.

  light44double& reshape(const int, const int, const light4double&);
  // Convert the light4-vector to the given size and put the result in
  // this object. All four vector-columns of the light44 object will
  // get the value of the light4-argument. The first two arguments
  // (the size of the matrix) must both have the value 4 since it is a
  // light44 object.

  int dimension(const int x) const {
    if((x==1) || (x==2))
      return 4;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 4 and dimension(2) == 4).

  void Get(double *) const;
  // Get values of all elements and put them in an array (9 elements long,
  // row major order).

  void Set(const double *data);
  // Set values of all elements from array (9 elements long, row major
  // order).

  light44double operator+() const;
  // Unary plus.

  light44double operator-() const;
  // Unary minus.

  friend inline light44double operator+(const light44double&, const light44double&);
  // Elementwise addition.

  friend inline light44double operator+(const light44double&, const double);
  // Addition to all elements.

  friend inline light44double operator+(const double, const light44double&);
  // Addition to all elements.

  friend inline light44double operator-(const light44double&, const light44double&);
  // Elementwise subtraction.

  friend inline light44double operator-(const light44double&, const double);
  // Subtraction from all elements.

  friend inline light44double operator-(const double, const light44double&);
  // Subtraction to all elements.

  friend inline light44double operator*(const light44double&, const light44double&);
  // Inner product.

  friend inline light44double operator*(const light44double&, const double);
  // Multiply all elements.

  friend inline light44double operator*(const double, const light44double&);
  // Multiply all elements.

  friend inline light4double operator*(const light44double&, const light4double&);
  // Inner product.

  friend inline light4double operator*(const light4double&, const light44double&);
  // Inner product.

  friend inline light44double operator/(const light44double&, const double);
  // Divide all elements.

  friend inline light44double operator/(const double, const light44double&);
  // Divide with all elements.

  friend inline light44double pow(const light44double&, const light44double&);
  // Raise to the power of-function, elementwise.

  friend inline light44double pow(const light44double&, const double);
  // Raise to the power of-function, for all elements.

  friend inline light44double pow(const double, const light44double&);
  // Raise to the power of-function, for all elements.

  friend inline light44double ElemProduct(const light44double&, const light44double&);
  // Elementwise multiplication.

  friend inline light44double ElemQuotient(const light44double&, const light44double&);
  // Elementwise division.

  friend inline light44double Apply(const light44double&, double f(double));
  // Apply the function elementwise on all elements.

  friend inline light44double Apply(const light44double&, const light44double&, double f(double, double));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline light44double Transpose(const light44double&);
  // Transpose matrix.

  friend inline light44double abs(const light44double&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light44int sign(const light44double&);
  // sign
#else
  friend inline light44lm_complex sign(const light44lm_complex&);
#endif
  friend inline light44int ifloor(const light44double&);
  // ifloor

  friend inline light44int iceil(const light44double&);
  // iceil

  friend inline light44int irint(const light44double&);
  // irint

  friend inline light44double sqrt(const light44double&);
  // sqrt

  friend inline light44double exp(const light44double&);
  // exp

  friend inline light44double log(const light44double&);
  // log

  friend inline light44double sin(const light44double&);
  // sin

  friend inline light44double cos(const light44double&);
  // cos

  friend inline light44double tan(const light44double&);
  // tan

  friend inline light44double asin(const light44double&);
  // asin

  friend inline light44double acos(const light44double&);
  // acos

  friend inline light44double atan(const light44double&);
  // atan

  friend inline light44double sinh(const light44double&);
  // sinh

  friend inline light44double cosh(const light44double&);
  // cosh

  friend inline light44double tanh(const light44double&);
  // tanh

  friend inline light44double asinh(const light44double&);
  // asinh

  friend inline light44double acosh(const light44double&);
  // acosh

  friend inline light44double atanh(const light44double&);
  // atanh

  friend inline light44lm_complex ifloor(const light44lm_complex&);
  // ifloor

  friend inline light44lm_complex iceil(const light44lm_complex&);
  // iceil

  friend inline light44lm_complex irint(const light44lm_complex&);
  // irint

  friend inline light44lm_complex sqrt(const light44lm_complex&);
  // sqrt

  friend inline light44lm_complex exp(const light44lm_complex&);
  // exp

  friend inline light44lm_complex log(const light44lm_complex&);
  // log

  friend inline light44lm_complex sin(const light44lm_complex&);
  // sin

  friend inline light44lm_complex cos(const light44lm_complex&);
  // cos

  friend inline light44lm_complex tan(const light44lm_complex&);
  // tan

  friend inline light44lm_complex asin(const light44lm_complex&);
  // asin

  friend inline light44lm_complex acos(const light44lm_complex&);
  // acos

  friend inline light44lm_complex atan(const light44lm_complex&);
  // atan

  friend inline light44lm_complex sinh(const light44lm_complex&);
  // sinh

  friend inline light44lm_complex cosh(const light44lm_complex&);
  // cosh

  friend inline light44lm_complex tanh(const light44lm_complex&);
  // tanh

  friend inline light44lm_complex asinh(const light44lm_complex&);
  // asinh

  friend inline light44lm_complex acosh(const light44lm_complex&);
  // acosh

  friend inline light44lm_complex atanh(const light44lm_complex&);
  // atanh


  //<ignore>
//    friend class light44int;
  friend class light4double;
  friend class lightN3double;
  friend class lightNNdouble;
  friend class lightNNNdouble;
  friend class lightNNNNdouble;
  //</ignore>

protected:
  double elem[16];
  // The values of the sixteen elements.

  light44double(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.


};

typedef light44double double44;
typedef light44int int44;

#undef IN_LIGHT44double_H

#endif
