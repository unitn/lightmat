//           -*- c++ -*-

#ifndef LIGHT33_I_H
#define LIGHT33_I_H
// <cd> light33int
//
// .SS Functionality
//
// light33 is a template for classes that implement matrices with 3x3
// elements. E.g. a matrix v with 3x3 elements of type double can be
// instanciated with:
//
// <code>light33&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHT33int_I_H

 class light3;
 class lightN;
 class lightN3;
 class lightN33;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light33int {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light33double();
  // Convert to double.
#else
   friend class  lightN33int;
#endif

#ifdef IN_LIGHT33double_I_H
   friend class  light33int;
#else
   friend class  light33double;
#endif

  #include "i_light33_auto.h"

  inline light33int();
  // Default constructor.

  inline light33int(const light33int&); 
  // Copy constructor.

  inline  light33int(const int e11, const int e12, const int e13, const int e21, const int e22, const int e23, const int e31, const int e32, const int e33);
  // Initialize elements with values.

  inline light33int(const int *);
  // Initialize elements with values from an array (in row major order).

  inline light33int(const int);
  // Initialize all elements with the same value.

  light33int& operator=(const light33int&);
    // Assignment.

  light33int& operator=(const lightN3int&);
  // Assignment from a lightN3 where N=3.

  light33int& operator=(const lightNNint&);
  // Assignment from a lightNN where NN=33.

  light33int& operator=(const int);
  // Assign one value to all elements.

  int operator()(const int x, const int y) const {
    limiterror((x<1) || (x>3) || (y<1) || (y>3));

#ifdef ROWMAJOR
    return elem[y-1+(x-1)*3];
#else
    return elem[x-1+(y-1)*3];
#endif
  };
  // Get the value of one element.


  int& operator()(const int x, const int y) {
    limiterror((x<1) || (x>3) || (y<1) || (y>3));
#ifdef ROWMAJOR
    return elem[y-1+(x-1)*3];
#else
    return elem[x-1+(y-1)*3];
#endif
  };
  // Get/Set the value of one element.

  light3int operator()(const int) const;
  // Get the value of one row.


  //
  // Set
  //

  inline light33int& Set (const int i0, const int val);
  inline light33int& Set (const int i0, const lightNint& arr);
  inline light33int& Set (const int i0, const light3int& arr);
  inline light33int& Set (const int i0, const light4int& arr);


  int operator==(const light33int&) const;
  // Equality.

  int operator!=(const light33int&) const;
  // Inequality.

  light33int& operator+=(const int);
  // Add a value to all elements.

  light33int& operator+=(const light33int&);
  // Elementwise addition.

  light33int& operator-=(const int);
  // Subtract a value from all elements.

  light33int& operator-=(const light33int&);
  // Elementwise subtraction.

  light33int& operator*=(const int);
  // Muliply all elements with a value.

  light33int& operator/=(const int);
  // Divide all elements with a value.

  light33int& reshape(const int, const int, const light3int&);
  // Convert the light3-vector to the given size and put the result in
  // this object. All three vector-columns of the light33 object will
  // get the value of the light3-argument. The first two arguments
  // (the size of the matrix) must both have the value 3 since it is a
  // light33 object.

  int dimension(const int x) const {
    if((x==1) || (x==2))
      return 3;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 3 and dimension(2) == 3).
 
  void Get(int *) const;
  // Get values of all elements and put them in an array (9 elements long,
  // row major order).

  void Set(const int *);
  // Set values of all elements from array (9 elements long, row major
  // order).

  void Set(const int e11, const int e12, const int e13,
	   const int e21, const int e22, const int e23,
	   const int e31, const int e32, const int e33);
  // Set the values of all the elements.

  void Get( int& e11, int& e12, int& e13,
	    int& e21, int& e22, int& e23,
	    int& e31, int& e32, int& e33) const;
  // Get the values of all the elements.

  light33int operator+() const;
  // Unary plus.

  light33int operator-() const;
  // Unary minus.

  friend inline light33int operator+(const light33int&, const light33int&);
  // Elementwise addition.

  friend inline light33int operator+(const light33int&, const int);
  // Addition to all elements.

  friend inline light33int operator+(const int, const light33int&);
  // Addition to all elements.

  friend inline light33int operator-(const light33int&, const light33int&);
  // Elementwise subtraction.

  friend inline light33int operator-(const light33int&, const int);
  // Subtraction from all elements.

  friend inline light33int operator-(const int, const light33int&);
  // Subtraction to all elements.

  friend inline light33int operator*(const light33int&, const light33int&);
  // Inner product.

  friend inline light33int Dot(const light33int&, const light33int&);
  // Inner product.

  friend inline light33int operator*(const light33int&, const int);
  // Multiply all elements.

  friend inline light33int operator*(const int, const light33int&);
  // Multiply all elements.

  friend inline light3int operator*(const light33int&, const light3int&);
  // Inner product.

  friend inline light3int operator*(const light3int&, const light33int&);
  // Inner product.

  friend inline light33int operator/(const light33int&, const int);
  // Divide all elements.

  friend inline light33int operator/(const int, const light33int&);
  // Divide with all elements.

  friend inline light33int pow(const light33int&, const light33int&);
  // Raise to the power of-function, elementwise.

  friend inline light33int pow(const light33int&, const int);
  // Raise to the power of-function, for all elements.

  friend inline light33int pow(const int, const light33int&);
  // Raise to the power of-function, for all elements.

  friend inline light33int ElemProduct(const light33int&, const light33int&);
  // Elementwise multiplication.

  friend inline light33int ElemQuotient(const light33int&, const light33int&);
  // Elementwise division.

  friend inline light33int Apply(const light33int&, int f(int));
  // Apply the function elementwise on all elements.

  friend inline light33int Apply(const light33int&, const light33int& s, int f(int, int));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline light33int Transpose(const light33int&);
  // Transpose matrix.

  friend inline light33int abs(const light33int&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light33int sign(const light33int&);
  // sign
#else
  friend inline light33lm_complex sign(const light33lm_complex&);
#endif
  friend inline light33int ifloor(const light33double&);
  // ifloor

  friend inline light33int iceil(const light33double&);
  // iceil

  friend inline light33int irint(const light33double&);
  // irint

  friend inline light33double sqrt(const light33double&);
  // sqrt

  friend inline light33double exp(const light33double&);
  // exp

  friend inline light33double log(const light33double&);
  // log

  friend inline light33double sin(const light33double&);
  // sin

  friend inline light33double cos(const light33double&);
  // cos

  friend inline light33double tan(const light33double&);
  // tan

  friend inline light33double asin(const light33double&);
  // asin

  friend inline light33double acos(const light33double&);
  // acos

  friend inline light33double atan(const light33double&);
  // atan

  friend inline light33double sinh(const light33double&);
  // sinh

  friend inline light33double cosh(const light33double&);
  // cosh

  friend inline light33double tanh(const light33double&);
  // tanh

  friend inline light33double asinh(const light33double&);
  // asinh

  friend inline light33double acosh(const light33double&);
  // acosh

  friend inline light33double atanh(const light33double&);
  // atanh

  friend inline light33lm_complex ifloor(const light33lm_complex&);
  // ifloor

  friend inline light33lm_complex iceil(const light33lm_complex&);
  // iceil

  friend inline light33lm_complex irint(const light33lm_complex&);
  // irint

  friend inline light33lm_complex sqrt(const light33lm_complex&);
  // sqrt

  friend inline light33lm_complex exp(const light33lm_complex&);
  // exp

  friend inline light33lm_complex log(const light33lm_complex&);
  // log

  friend inline light33lm_complex sin(const light33lm_complex&);
  // sin

  friend inline light33lm_complex cos(const light33lm_complex&);
  // cos

  friend inline light33lm_complex tan(const light33lm_complex&);
  // tan

  friend inline light33lm_complex asin(const light33lm_complex&);
  // asin

  friend inline light33lm_complex acos(const light33lm_complex&);
  // acos

  friend inline light33lm_complex atan(const light33lm_complex&);
  // atan

  friend inline light33lm_complex sinh(const light33lm_complex&);
  // sinh

  friend inline light33lm_complex cosh(const light33lm_complex&);
  // cosh

  friend inline light33lm_complex tanh(const light33lm_complex&);
  // tanh

  friend inline light33lm_complex asinh(const light33lm_complex&);
  // asinh

  friend inline light33lm_complex acosh(const light33lm_complex&);
  // acosh

  friend inline light33lm_complex atanh(const light33lm_complex&);
  // atanh



  //<ignore>
//    friend class light33int;
  friend class light3int;
  friend class lightN3int;
  friend class lightNNint;
  friend class lightN33int;
  friend class lightNNNint;
  friend class lightNNNNint;


  //    friend ostream & operator<<( ostream & ost,const  light33double & v);
  //</ignore>

protected:
  int elem[9];
  // The values of the nine elements.

  light33int(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.

};



typedef light33double double33;
typedef light33int int33;

//*********************************
typedef light33double doubleMat33;
extern  const  doubleMat33   zeroMat33;
//********************************

#undef IN_LIGHT33int_I_H

#endif
