//           -*- c++ -*-

#ifndef LIGHTNN_H
#define LIGHTNN_H
// <cd> lightNN
//
// .SS Functionality
//
// lightNN is a template for classes that implement matrices with any
// number of elements. E.g. a matrix v with 5x6 elements of type double
// can be instanciated with:
//
// <code>lightNN&lt;double&gt; v(5,6);</code>
//
// The number of rows and columns in the matrix can change during
// execution. It changes its size to whatever is needed when it is
// assigned a new value with an assignment operator.
//
// When a lightNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNN object with 5x5 elements to one with 5x6 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
// Vadim Engelson
//

#include <assert.h>

#define IN_LIGHTNN<T>_H


#define LIGHTNN_SIZE1 10
#define LIGHTNN_SIZE2 10
#define LIGHTNN_SIZE (LIGHTNN_SIZE1*LIGHTNN_SIZE2)
// The default size.

template<class T> class light3;
template<class T> class light4;
template<class T> class lightN;
template<class T> class light33;
template<class T> class light44;
template<class T> class lightN3;
template<class T> class lightNNN;

template<class T>
class lightNN {
public:

#ifdef IN_LIGHTNNdouble_H
  friend class lightNN<int>;
#else
  friend class lightNN<double>;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNN<int>;
#endif


  #include "lightNN_auto.h"

  inline lightNN();
  // Default constructor.

  inline lightNN(const lightNN<T>&);
  // Copy constructor.

  inline lightNN(const light33<T>&);
  // Conversion.

  inline lightNN(const light44<T>&);
  // Conversion.

  inline lightNN(const lightN3<T>&);
  // Conversion.

  inline lightNN(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNN(const int, const int);
  // Construct a lightNN of given size.

  inline lightNN(const int, const int, const T *);
  // Construct a lightNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNN(const int, const int, const T);
  // Construct a lightNN of given size and initialize all elements
  // with a value (the third argument).

  inline ~lightNN();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNN<double>();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNN<lm_complex>();
  // Convert to complex.
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNN<lm_complex>();
  // Convert to complex.
#endif



  lightNN<T>& operator=(const lightNN<T>&);
  // Assignment.

  lightNN<T>& operator=(const light33<T>&);
  // Assignment, change size to 3x3.

  lightNN<T>& operator=(const light44<T>&);
  // Assignment, change size to 4x4.

  lightNN<T>& operator=(const lightN3<T>&);
  // Assignment, change size to Nx3.

  lightNN<T>& operator=(const T);
  // Assign one value to all elements.

  T operator() (const int x, const int y) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2));
#ifdef ROWMAJOR
    return elem[y-1 + (x-1)*size2];
#else
    return elem[x-1 + (y-1)*size1];
#endif
  };
  // Get the value of one element.

  //
  // Set and Get
  //

  T& operator()(const int x, const int y) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2));
#ifdef ROWMAJOR
    return elem[y-1 + (x-1)*size2];
#else
    return elem[x-1 + (y-1)*size1];
#endif
  };
  // Get/Set the value of one element.

  //
  // Set
  //

  inline lightNN<T>& Set (const int i0, const T val);
  inline lightNN<T>& Set (const int i0, const lightN<T>& arr);
  inline lightNN<T>& Set (const int i0, const light3<T>& arr);
  inline lightNN<T>& Set (const int i0, const light4<T>& arr);


  T&  takeElementReference (const int x, const int y);
  // Same

  lightN<T> operator()(const int) const;
  // Get the value of one row.
 
  const lightN<T>& SetCol(const int n,const lightN<T>& x);
  // Put x into column n. Return x.

  const lightN<T>& SetRow(const int n,const lightN<T>& x);
  // Put x into row n. Return x.
  
  T SetCol(const int n,const T x);
  // Put x into all elements of  column n. Return x.

  T SetRow(const int n,const T x);
   // Put x into all elements of row n. Return x.

  const lightNN<T>& SetCols(const int n1,const int n2,const  lightNN<T>& x);
  // Put every  element of matrix x to the columns n1..n2. Return x. 
  // In this function and below:
  // n1=0 means to use  1. 
  // n2=0 means to use the largest possible value of matrix index.

  const lightNN<T>& SetRows(const int n1,const int n2,const  lightNN<T>& x);
  // Put every  element of matrix x to the rows n1..n2.  Return x. 
  
  T SetCols(const int n1, const int n2, const  T x);
  // Put x to very element of all columns n1..n2. Return x. 
  
  T SetRows(const int n1,const int n2,const  T x);
  // Put x to very eleme nt of all rows n1..n2. Return x. 
  
  lightN<T>  Col(const int n) const;
  // Get Column n.  

  lightN<T>  Row(const int n) const;
  // Get Row n.

// REMOVE LATER
  inline lightNN<T>   SubMatrix(const int r1, const int r2,const int c1, const int c2) const;
  // Get submatrix of rows r1..r2, columns c1..c2.

  inline const lightNN<T>& SetSubMatrix(const int r1,const int r2,const int c1,const int c2,
                  const  lightNN<T>& x);
  // Put matrix x into  submatrix  of rows r1..r2, columns c1..c2. Return x.

  inline  T SetSubMatrix(const int r1,const int r2,const int c1,const int c2,
                   const T x);
  // Put x into rows r1..r2, columns c1..c2. Return x.
  
  lightNN<T> Rows(const int x1,const int x2) const;
  // Get submstrix produced by rows x1..x2.
  
  
  lightNN<T> Cols(const int y1,const int y2) const;
  // Get submstrix produced by columns  y1..y2.
  
  int operator==(const lightNN<T>&) const;
  // Equality.

  int operator!=(const lightNN<T>&) const;
  // Inequality.
 
  lightNN<T>& operator+=(const T);
  // Add a value to all elements.

  lightNN<T>& operator+=(const lightNN<T>&);
  // Elementwise addition.

  lightNN<T>& operator-=(const T);
  // Subtract a value from all elements.

  lightNN<T>& operator-=(const lightNN<T>&);
  // Elementwise subtraction.

  lightNN<T>& operator*=(const T);
  // Muliply all elements with a value.

  lightNN<T>& operator/=(const T);
  // Divide all elements with a value.
 
  inline lightNN<T>& SetShape(const int x=-1, const int y=-1);
  // Sets specified shape for the matrix.

  T Extract(const lightN<int>&) const;
  // Extract an element using given index.   

  lightNN<T>& reshape(const int, const int, const lightN<T>&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. All vector-columns of the lightNN object will get
  // the value of the lightN-argument. The value of the first argument
  // must be the same as the number of elements in the lightN-vector.

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(T *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const T *);
  // Set values of all elements from array (row major order).
 
  T * data() const; 
  // Direct access to the stored data. Use the result with care.

  lightNN<T> operator+() const;
  // Unary plus.

  lightNN<T> operator-() const;
  // Unary minus.

  friend inline lightNN<T> operator+(const lightNN<T>&, const lightNN<T>&);
  // Elementwise addition.

  friend inline lightNN<T> operator+(const lightNN<T>&, const T);
  // Addition to all elements.

  friend inline lightNN<T> operator+(const T, const lightNN<T>&);
  // Addition to all elements.

#ifdef IN_LIGHTNNdouble_H
  friend inline lightNN<double> operator+(const lightNN<double>&, const int);
  friend inline lightNN<double> operator+(const lightNN<int>&, const double);
  friend inline lightNN<double> operator+(const double, const lightNN<int>&);
  friend inline lightNN<double> operator+(const int, const lightNN<double>&);
#endif

#ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNlm_complex operator+(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator+(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator+(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator+(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNN<T> operator-(const lightNN<T>&, const lightNN<T>&);
  // Elementwise subtraction.

  friend inline lightNN<T> operator-(const lightNN<T>&, const T);
  // Subtraction from all elements.

  friend inline lightNN<T> operator-(const T, const lightNN<T>&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNdouble_H
  friend inline lightNN<double> operator-(const lightNN<double>&, const int);
  friend inline lightNN<double> operator-(const lightNN<int>&, const double);
  friend inline lightNN<double> operator-(const double, const lightNN<int>&);
  friend inline lightNN<double> operator-(const int, const lightNN<double>&);
#endif

  #ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNlm_complex operator-(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator-(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator-(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator-(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNN<T> operator*(const lightNN<T>&, const lightNN<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const lightNN<T>&, const T);
  // Multiply all elements.

  friend inline lightNN<T> operator*(const T, const lightNN<T>& s);
  // Multiply all elements.

#ifdef IN_LIGHTNNdouble_H
  friend inline lightNN<double> operator*(const lightNN<double>&, const int);
  friend inline lightNN<double> operator*(const lightNN<int>&, const double);
  friend inline lightNN<double> operator*(const double, const lightNN<int>&);
  friend inline lightNN<double> operator*(const int, const lightNN<double>&);
#endif

#ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNlm_complex operator*(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator*(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator*(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator*(const double e, const lightNNlm_complex& s2);
#endif

  friend inline lightNN<T> operator*(const lightNN<T>&, const light33<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const lightNN<T>&, const light44<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const lightN3<T>&, const lightNN<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const light33<T>&, const lightNN<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const light44<T>&, const lightNN<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const lightN<T>&, const lightNNN<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const lightNNN<T>&, const lightN<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const light3<T>&, const lightNNN<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const lightNNN<T>&, const light3<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const light4<T>&, const lightNNN<T>&);
  // Inner product.

  friend inline lightNN<T> operator*(const lightNNN<T>&, const light4<T>&);
  // Inner product.

  friend inline lightNN<T> operator/(const lightNN<T>&, const T);
  // Divide all elements.

  friend inline lightNN<T> operator/(const T, const lightNN<T>&);
  // Divide all elements.

#ifdef IN_LIGHTNNdouble_H
  friend inline lightNN<double> operator/(const lightNN<double>&, const int);
  friend inline lightNN<double> operator/(const lightNN<int>&, const double);
  friend inline lightNN<double> operator/(const double, const lightNN<int>&);
  friend inline lightNN<double> operator/(const int, const lightNN<double>&);
#endif

#ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNlm_complex operator/(const lightNNlm_complex& s1, const double e);
friend inline lightNNlm_complex operator/(const lightNNdouble& s1, const lm_complex e);
friend inline lightNNlm_complex operator/(const lm_complex e, const lightNNdouble& s2);
friend inline lightNNlm_complex operator/(const double e, const lightNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNlm_complex_H
friend inline lightNNdouble arg(const lightNNlm_complex& a);
friend inline lightNNdouble re(const lightNNlm_complex& a);
friend inline lightNNdouble im(const lightNNlm_complex& a);
friend inline lightNNlm_complex conjugate(const lightNNlm_complex& a);
//#endif

  friend inline lightNN<T> pow(const lightNN<T>&, const lightNN<T>&);
  // Raise to the power of-function, elementwise.

  friend inline lightNN<T> pow(const lightNN<T>&, const T);
  // Raise to the power of-function, for all elements.

  friend inline lightNN<T> pow(const T, const lightNN<T>&);
  // Raise to the power of-function, for all elements.

  friend inline lightNN<T> ElemProduct(const lightNN<T>&, const lightNN<T>&);
  // Elementwise multiplication.

  friend inline lightNN<T> ElemQuotient(const lightNN<T>&, const lightNN<T>&);
  // Elementwise division.

  friend inline lightNN<T> Apply(const lightNN<T>&, T f(T));
  // Apply the function elementwise all elements.

  friend inline lightNN<T> Apply(const lightNN<T>&, const lightNN<T>&, T f(T, T));
  // Apply the function elementwise on all elements in the two matrices.

  friend inline lightNN<T> Transpose(const lightNN<T>&);
  // Transpose matrix.

  friend inline lightNN<T> Transpose(const lightN3<T>&);
  // Transpose matrix.

  friend inline lightNN<T> OuterProduct(const lightN<T>&, const lightN<T>&);
  // Outer product.


#ifdef IN_LIGHTNNlm_complex_H
#else
  friend inline lightNN<T> abs(const lightNN<T>&);
#ifdef IN_LIGHTNNdouble_H
  friend inline lightNN<double> abs(const lightNN<lm_complex>&);
#endif 
#endif


#ifndef COMPLEX_TOOLS
  friend inline lightNN<int> sign(const lightNN<int>&);
  // sign
#else
  // sign
  friend inline lightNNlm_complex sign(const lightNNlm_complex&);
#endif
  friend inline lightNN<int> sign(const lightNN<double>&);
  // sign

  /// Added 2/2/98 

  friend inline lightNN<T>  FractionalPart(const lightNN<T>&);
  //  FractionalPart

  friend inline lightNN<T>  IntegerPart(const lightNN<T>&);
  //  IntegerPart

  //friend inline lightNN<int>  IntegerPart(const lightNN<double>&);
  //  IntegerPart

  friend inline lightNN<T> Mod (const  lightNN<T>&,const  lightNN<T>&);
  // Mod
 
  friend inline lightNN<T> Mod (const  lightNN<T>&,const T);
  // Mod

  friend inline lightNN<T> Mod (const T,const  lightNN<T>&);
  // Mod

  friend inline T LightMax (const lightNN<T>& );
  // Max

  friend inline T LightMin (const lightNN<T>& );
  // Min

  friend inline T findLightMax (const  T *,const  int );
  // Find Max
 
  friend inline T findLightMin (const  T *,const  int );
  // Find Min
 
  /// End Added 



  friend inline lightNN<int> ifloor(const lightNN<double>&);
  // ifloor

  friend inline lightNN<int> iceil(const lightNN<double>&);
  // iceil

  friend inline lightNN<int> irint(const lightNN<double>&);
  // irint

  friend inline lightNN<double> sqrt(const lightNN<double>&);
  // sqrt

  friend inline lightNN<double> exp(const lightNN<double>&);
  // exp

  friend inline lightNN<double> log(const lightNN<double>&);
  // log

  friend inline lightNN<double> sin(const lightNN<double>&);
  // sin

  friend inline lightNN<double> cos(const lightNN<double>&);
  // cos

  friend inline lightNN<double> tan(const lightNN<double>&);
  // tan

  friend inline lightNN<double> asin(const lightNN<double>&);
  // asin

  friend inline lightNN<double> acos(const lightNN<double>&);
  // acos

  friend inline lightNN<double> atan(const lightNN<double>&);
  // atan

  friend inline lightNN<double> sinh(const lightNN<double>&);
  // sinh

  friend inline lightNN<double> cosh(const lightNN<double>&);
  // cosh

  friend inline lightNN<double> tanh(const lightNN<double>&);
  // tanh

  friend inline lightNN<double> asinh(const lightNN<double>&);
  // asinh

  friend inline lightNN<double> acosh(const lightNN<double>&);
  // acosh

  friend inline lightNN<double> atanh(const lightNN<double>&);
  // atanh
  
  friend inline lightNN<lm_complex> ifloor(const lightNN<lm_complex>&);
  // ifloor

  friend inline lightNN<lm_complex> iceil(const lightNN<lm_complex>&);
  // iceil

  friend inline lightNN<lm_complex> irint(const lightNN<lm_complex>&);
  // irint

  friend inline lightNN<lm_complex> sqrt(const lightNN<lm_complex>&);
  // sqrt

  friend inline lightNN<lm_complex> exp(const lightNN<lm_complex>&);
  // exp

  friend inline lightNN<lm_complex> log(const lightNN<lm_complex>&);
  // log

  friend inline lightNN<lm_complex> sin(const lightNN<lm_complex>&);
  // sin

  friend inline lightNN<lm_complex> cos(const lightNN<lm_complex>&);
  // cos

  friend inline lightNN<lm_complex> tan(const lightNN<lm_complex>&);
  // tan

  friend inline lightNN<lm_complex> asin(const lightNN<lm_complex>&);
  // asin

  friend inline lightNN<lm_complex> acos(const lightNN<lm_complex>&);
  // acos

  friend inline lightNN<lm_complex> atan(const lightNN<lm_complex>&);
  // atan

  friend inline lightNN<lm_complex> sinh(const lightNN<lm_complex>&);
  // sinh

  friend inline lightNN<lm_complex> cosh(const lightNN<lm_complex>&);
  // cosh

  friend inline lightNN<lm_complex> tanh(const lightNN<lm_complex>&);
  // tanh

  friend inline lightNN<lm_complex> asinh(const lightNN<lm_complex>&);
  // asinh

  friend inline lightNN<lm_complex> acosh(const lightNN<lm_complex>&);
  // acosh

  friend inline lightNN<lm_complex> atanh(const lightNN<lm_complex>&);
  // atanh



#ifdef IN_LIGHTNNint_H
  friend inline lightN<double> light_mean     ( const  lightNN<T>& );
  friend inline lightN<double> light_standard_deviation( const  lightNN<T>& );
  friend inline lightN<double> light_variance ( const  lightNN<T>& );
  friend inline lightN<double> light_median   ( const  lightNN<T>& );
#else
  friend inline lightN<T> light_mean     ( const  lightNN<T>& );
  friend inline lightN<T> light_variance ( const  lightNN<T>& );
  friend inline lightN<T> light_standard_deviation( const  lightNN<T>& );
  friend inline lightN<T> light_median   ( const  lightNN<T>& );
#endif
  
  friend inline lightN<T> light_total    ( const  lightNN<T>& );
 
 
  friend inline lightNN<T> light_sort (const lightNN<T> arr1);
  
  friend inline lightN<T> light_flatten (const lightNN<T> s);
  friend inline lightN<T> light_flatten (const lightNN<T> s, int level);
  friend inline lightNN<T> light_flatten0 (const lightNN<T> s);


  //<ignore>
#ifdef IN_LIGHTNNlm_complex_H  
  friend class lightNN<int>;
#endif  
  friend class light3<T>;
  friend class light4<T>;
  friend class lightN<T>;
  friend class light33<T>;
  friend class light44<T>;
  friend class lightN3<T>;
  friend class lightNNN<T>;
  friend class lightNNNN<T>;
  friend class lightN33<T>;

  friend inline lightNN<T> light_join (const lightNN<T>, const lightNN<T>);
  friend inline lightNN<T> light_drop (const lightNN<T> arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNdouble::lightNN(const lightNNint& s1, const double e, const lightmat_plus_enum)
  T *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // The number of rows.

  int size2;
  // The number of columns.


protected:
  T sarea[LIGHTNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNN(const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // two arguments is the size of the created matrix. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNN(const lightNN<T>&, const lightNN<T>&, const lightmat_plus_enum);
  lightNN(const lightNN<T>&, const T, const lightmat_plus_enum);

#ifdef IN_LIGHTNNdouble_H
  lightNN(const lightNNdouble&, const int, const lightmat_plus_enum);
  lightNN(const lightNNint&, const double, const lightmat_plus_enum);
#endif

  lightNN(const lightNN<T>&, const lightNN<T>&, const lightmat_minus_enum);
  lightNN(const T, const lightNN<T>&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNdouble_H
  lightNN(const int, const lightNNdouble&, const lightmat_minus_enum);
  lightNN(const double, const lightNNint&, const lightmat_minus_enum);
#endif

  lightNN(const lightNN<T>&, const lightNN<T>&, const lightmat_mult_enum);
  lightNN(const lightNN<T>&, const T, const lightmat_mult_enum);

#ifdef IN_LIGHTNNdouble_H
  lightNN(const lightNNdouble&, const int, const lightmat_mult_enum);
  lightNN(const lightNNint&, const double, const lightmat_mult_enum);
#endif

  lightNN(const lightNN<T>&, const light33<T>&, const lightmat_mult_enum);
  lightNN(const lightNN<T>&, const light44<T>&, const lightmat_mult_enum);
  lightNN(const lightN3<T>&, const lightNN<T>&, const lightmat_mult_enum);
  lightNN(const light33<T>&, const lightNN<T>&, const lightmat_mult_enum);
  lightNN(const light44<T>&, const lightNN<T>&, const lightmat_mult_enum);
  lightNN(const lightN<T>&, const lightNNN<T>&, const lightmat_mult_enum);
  lightNN(const lightNNN<T>&, const lightN<T>&, const lightmat_mult_enum);
  lightNN(const light3<T>&, const lightNNN<T>&, const lightmat_mult_enum);
  lightNN(const lightNNN<T>&, const light3<T>&, const lightmat_mult_enum);
  lightNN(const light4<T>&, const lightNNN<T>&, const lightmat_mult_enum);
  lightNN(const lightNNN<T>&, const light4<T>&, const lightmat_mult_enum);

  lightNN(const T, const lightNN<T>&, const lightmat_div_enum);
#ifdef IN_LIGHTNNdouble_H
  lightNN(const int, const lightNNdouble&, const lightmat_div_enum);
  lightNN(const double, const lightNNint&, const lightmat_div_enum);
#endif

  lightNN(const lightNN<T>&, const lightNN<T>&, const lightmat_pow_enum);
  lightNN(const lightNN<T>&, const T, const lightmat_pow_enum);
  lightNN(const T, const lightNN<T>&, const lightmat_pow_enum);
  //lightNN(const lightNN<T>&, const lightmat_abs_enum);

#ifdef IN_LIGHTNNlm_complex_H
 #else
  #ifdef IN_LIGHTNNdouble_H
   lightNN(const lightNN<T>&, const lightmat_abs_enum);
   lightNN(const lightNN<lm_complex>&, const lightmat_abs_enum);
  #else
  lightNN(const lightNN<T>&, const lightmat_abs_enum);
  #endif
#endif

  lightNN(const lightNN<T>&, const lightNN<T>&, const lightmat_eprod_enum);
  lightNN(const lightNN<T>&, const lightNN<T>&, const lightmat_equot_enum);
  lightNN(const lightNN<T>&, T f(T), const lightmat_apply_enum);
  lightNN(const lightNN<T>&, const lightNN<T>&, T f(T, T), const lightmat_apply_enum);
  lightNN(const lightN3<T>&, const lightmat_trans_enum);
  lightNN(const lightN<T>&, const lightN<T>&, const lightmat_outer_enum);

};

typedef lightNN<double> doubleNN;
typedef lightNN<int> intNN;
typedef lightNN<lm_complex> lm_complexNN;

#undef IN_LIGHTNN<T>_H


#endif
