//           -*- c++ -*-

#ifndef LIGHT4_I_H
#define LIGHT4_I_H
// <cd> light4int
//
// .SS Functionality
//
// light4 is a template for classes that implement vectors with 4
// elements. E.g. a vector v with 4 elements of type double can be
// instanciated with:
//
// <code>light4&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#define IN_LIGHT4int_I_H

 class lightN;
 class light44;
 class lightNN;
 class lightNNN;
 class lightNNNN;


class light4int {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light4double();
  // Convert to double.
#else
#endif
#ifdef LIGHTMAT_TEMPLATES
  friend class light4int;
#endif

#ifdef IN_LIGHT4double_I_H
  friend class light4int;
#else
  friend class light4double;
#endif

  #include "i_light4_auto.h"

  light4int();
  // Default constructor.

  light4int(const light4int&);
  // Copy constructor.
  
  light4int(const int, const int, const int, const int);
  // Initialize elements with values.

  light4int(const int *);
  // Initialize elements with values from an array.

  light4int(const int);
  // Initialize all elements with the same value.

  light4int& operator=(const light4int& s);
  // Assignment.
  
  light4int& operator=(const lightNint& s);
  // Assignment from a lightN where N=3.

  light4int& operator=(const int);
  // Assign one value to all elements.

  int operator()(const int x) const {
    limiterror((x<1) || (x>4));
    return elem[x-1];
  };
  // Get the value of one element.

  int& operator()(const int x) {
    limiterror((x<1) || (x>4));
    return elem[x-1];
  };
  // Get/Set the value of one element.
  int operator==(const light4int&) const;
  // Equality.

  int operator!=(const light4int&) const;
  // Inequality.

  light4int& operator+=(const int);
  // Add a value to all elements.

  light4int& operator+=(const light4int&);
  // Elementwise addition.

  light4int& operator-=(const int);
  // Subtract a value from all elements.

  light4int& operator-=(const light4int&);
  // Elementwise subtraction.

  light4int& operator*=(const int);
  // Muliply all elements with a value.

  light4int& operator/=(const int);
  // Divide all elements with a value.
 
  #ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
  #endif
  int dimension(const int x = 1) const {
    if(x == 1)
      return 4;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 4). If this was a lightNint
  // then dimension(1) would return the number of elements.
 
  #ifndef COMPLEX_TOOLS
  light4int& normalize();
  // Normalize vector.
  #endif

  void Get(int *) const;
  // Get values of all elements and put them in an array (4 elements long).

  void Set(const int *);
  // Set values of all elements from array (4 elements long).
 
  light4int operator+() const;
  // Unary plus.

  light4int operator-() const;
  // Unary minus.

  friend inline light4int operator+(const light4int&, const light4int&);
  // Elementwise addition.

  friend inline light4int operator+(const light4int&, const int);
  // Addition to all elements.

  friend inline light4int operator+(const int, const light4int&);
  // Addition to all elements.

  friend inline light4int operator-(const light4int&, const light4int&);
  // Elementwise subtraction.

  friend inline light4int operator-(const light4int&, const int);
  // Subtraction from all elements.

  friend inline light4int operator-(const int, const light4int&);
  // Subtraction to all elements.

  friend inline int operator*(const light4int&, const light4int&);
  // Inner product.

  friend inline light4int operator*(const light4int&, const int);
  // Multiply all elements.

  friend inline light4int operator*(const int, const light4int&);
  // Multiply all elements.

  friend inline light4int operator*(const light4int&, const light44int&);
  // Inner product.

  friend inline light4int operator*(const light44int&, const light4int&);
  // Inner product.

  friend inline lightNint operator*(const light4int&, const lightNNint&);
  // Inner product.

  friend inline light4int operator/(const light4int&, const int);
  // Divide all elements.

  friend inline light4int operator/(const int, const light4int&);
  // Divide with all elements.

  friend inline light4int pow(const light4int&, const light4int&);
  // Raise to the power of-function, elementwise.

  friend inline light4int pow(const light4int&, const int);
  // Raise to the power of-function, for all elements.

  friend inline light4int pow(const int, const light4int&);
  // Raise to the power of-function, for all elements.

  friend inline light4int ElemProduct(const light4int&, const light4int&);
  // Elementwise multiplication.

  friend inline light4int ElemQuotient(const light4int&, const light4int&);
  // Elementwise division.

  friend inline light4int Apply(const light4int&, int f(int));
  // Apply the function elementwise on all four elements.

  friend inline light4int Apply(const light4int&, const light4int& ,int f(int, int));
  // Apply the function elementwise on all four elements in the two
  // vecors.

  friend inline light44int OuterProduct(const light4int&, const light4int&);
  // Outer product.

  friend inline light4int abs(const light4int&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light4int sign(const light4int&);
  // sign
#else
  friend inline light4lm_complex sign(const light4lm_complex&);
#endif
  friend inline light4int ifloor(const light4double&);
  // ifloor

  friend inline light4int iceil(const light4double&);
  // iceil

  friend inline light4int irint(const light4double&);
  // irint

  friend inline light4double sqrt(const light4double&);
  // sqrt

  friend inline light4double exp(const light4double&);
  // exp

  friend inline light4double log(const light4double&);
  // log

  friend inline light4double sin(const light4double&);
  // sin

  friend inline light4double cos(const light4double&);
  // cos

  friend inline light4double tan(const light4double&);
  // tan

  friend inline light4double asin(const light4double&);
  // asin

  friend inline light4double acos(const light4double&);
  // acos

  friend inline light4double atan(const light4double&);
  // atan

  friend inline light4double sinh(const light4double&);
  // sinh

  friend inline light4double cosh(const light4double&);
  // cosh

  friend inline light4double tanh(const light4double&);
  // tanh

  friend inline light4double asinh(const light4double&);
  // asinh

  friend inline light4double acosh(const light4double&);
  // acosh

  friend inline light4double atanh(const light4double&);
  // atanh

  friend inline light4lm_complex ifloor(const light4lm_complex&);
  // ifloor

  friend inline light4lm_complex iceil(const light4lm_complex&);
  // iceil

  friend inline light4lm_complex irint(const light4lm_complex&);
  // irint

  friend inline light4lm_complex sqrt(const light4lm_complex&);
  // sqrt

  friend inline light4lm_complex exp(const light4lm_complex&);
  // exp

  friend inline light4lm_complex log(const light4lm_complex&);
  // log

  friend inline light4lm_complex sin(const light4lm_complex&);
  // sin

  friend inline light4lm_complex cos(const light4lm_complex&);
  // cos

  friend inline light4lm_complex tan(const light4lm_complex&);
  // tan

  friend inline light4lm_complex asin(const light4lm_complex&);
  // asin

  friend inline light4lm_complex acos(const light4lm_complex&);
  // acos

  friend inline light4lm_complex atan(const light4lm_complex&);
  // atan

  friend inline light4lm_complex sinh(const light4lm_complex&);
  // sinh

  friend inline light4lm_complex cosh(const light4lm_complex&);
  // cosh

  friend inline light4lm_complex tanh(const light4lm_complex&);
  // tanh

  friend inline light4lm_complex asinh(const light4lm_complex&);
  // asinh

  friend inline light4lm_complex acosh(const light4lm_complex&);
  // acosh

  friend inline light4lm_complex atanh(const light4lm_complex&);
  // atanh


  //<ignore>
//    friend class light4int;
  friend class lightNint;
  friend class light44int;
  friend class lightNNint;
  friend class lightNNNint;
  friend class lightNNNNint;

  friend inline lightNint light_join (const lightNint arr1, const lightNint arr2);

  //</ignore>

protected:
  int elem[4];
  // The values of the four elements.

  light4int(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The size of the vector (number of elements).


};

typedef light4double double4;
typedef light4int int4;

#undef IN_LIGHT4int_I_H

#endif
