//
// sign
//


inline int sign(int    x);	

#ifndef __GNUG__

inline  int sign(double x);	

#else

// GNU has this symbol in library. We do not inline it.
        int sign(double x);	
#endif

// See details in light_int.icc
// These functions are defined once there.


#ifndef COMPLEX_TOOLS

light3int sign(const light3int& s);	


light4int sign(const light4int& s);	


light33int sign(const light33int& s);	


light44int sign(const light44int& s);	


lightNint sign(const lightNint& s);	


lightN3int sign(const lightN3int& s);	


lightNNint sign(const lightNNint& s);	


lightNNNint sign(const lightNNNint& s);	


lightNNNNint sign(const lightNNNNint& s);	

//
// abs
//

lightNint abs(const lightNint& s);


lightN3int abs(const lightN3int& s);


lightNNint abs(const lightNNint& s);


lightNNNint abs(const lightNNNint& s);


lightNNNNint abs(const lightNNNNint& s);


#else
light3lm_complex sign(const light3lm_complex& s);	


light4lm_complex sign(const light4lm_complex& s);	


light33lm_complex sign(const light33lm_complex& s);	


light44lm_complex sign(const light44lm_complex& s);	


lightNlm_complex sign(const lightNlm_complex& s);	


lightN3lm_complex sign(const lightN3lm_complex& s);	


lightNNlm_complex sign(const lightNNlm_complex& s);	


lightNNNlm_complex sign(const lightNNNlm_complex& s);	


lightNNNNlm_complex sign(const lightNNNNlm_complex& s);	
#endif

//
// Mod
//


lightNint Mod(const lightNint& v, const int s );


lightNint Mod(const int v, const lightNint& s);


lightNint Mod(const lightNint&v1, const lightNint&v2);




lightNNint Mod(const lightNNint& v, const int s );


lightNNint Mod(const int v, const lightNNint& s);


lightNNint Mod(const lightNNint&v1, const lightNNint&v2);




lightNNNint Mod(const lightNNNint& v, const int s );


lightNNNint Mod(const int v, const lightNNNint& s);


lightNNNint Mod(const lightNNNint&v1, const lightNNNint&v2);




lightNNNNint Mod(const lightNNNNint& v, const int s );


lightNNNNint Mod(const int v, const lightNNNNint& s);


lightNNNNint Mod(const lightNNNNint&v1, const lightNNNNint&v2);



