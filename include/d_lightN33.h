//           -*- c++ -*-

#ifndef LIGHTN33_H
#define LIGHTN33_H
// <cd> lightN33double
//
// .SS Functionality
//
// lightN33 is a template for classes that implement tensors
// of size Nx3x3
//
// <code>lightN33&lt;double&gt; v(5);</code>
//
// The value of the first index can change during execution. It
// changes its size to whatever is needed when it is assigned a new
// value with an assignment operator.
//
// When a lightN33 object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightN33 object with 5x3x3 elements to one with 7x3x3 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#define LIGHTN33_SIZE 10


class lightN33double {
public:
  lightN33double();
  // Default constructor.

  lightN33double(const lightN33double&);
  // Copy constructor.

  lightN33double(const int n);
  // Construct a lightN33 with size nx3x3

  lightN33double(const int n, const double *);
  // Construct a lightN33 with size nx3x3 and initialize the
  // elements with values from an array (in row major order).
  
  lightN33double(const int n, const double);
  // Construct a lightN33 with size nx3x3 and initialize the
  // elements with a value (the second argument).

  ~lightN33double();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightN33double();
  // Convert to double
#else
  friend class lightN33int;  
#endif

  lightN33double& operator=(const lightN33double&);
  // Assignment.

  lightN33double& operator=(const lightNNNdouble&);
  // Assignment from a lightNNN with size Nx3x3.

  lightN33double& operator=(const double);
  // Assign one value to all elements.

  double operator()(const int, const int, const int) const;
  // Get the value of one element.

  double& operator()(const int, const int, const int);
  // Get/Set the value of one element.

  light3double operator()(const int, const int) const;
  // Get the value of a vector.

  const light33double& operator()(const int) const;
  // Get one 3x3 matrix.
  
  light33double& operator()(const int);
  // Get/Set one 3x3 matrix.
 
  int operator==(const lightN33double&) const;
  // Equality.

  int operator!=(const lightN33double&) const;
  // Inequality.

  lightN33double& operator+=(const double);
  // Add a value to all elements.

  lightN33double& operator+=(const lightN33double&);
  // Elementwise addition.

  lightN33double& operator-=(const double);
  // Subtract a value from all elements.

  lightN33double& operator-=(const lightN33double&);
  // Elementwise subtraction.

  lightN33double& operator*=(const double);
  // Mulitply all elements with a value.

  lightN33double& operator/=(const double);
  // Divide all elements with a value.

  lightN33double& reshape(const int, const int, const int, const lightNdouble& s);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c) will be set to s(a) in the lightN33double
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector and the second and third
  // argument must be 3. The program may dump core or behave strangely
  // if the arguments are incorrect.

  lightN33double& reshape(const int, const int, const int, const lightNNdouble& s);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c) will be set to s(a,b) in the
  // lightN33 object. The value of the first argument must be the same
  // as the number of rows in the lightNN-matrix and the second and
  // third argument must be 3. The number of columns in the
  // lightNN-matrix must also be 3. The program may dump core or
  // behave strangely if the arguments are incorrect.

  int dimension(const int) const;
  // Get size of some dimension (dimension(2) == 3 and dimension(3) ==
  // 3 for lightN3).

  void Get(double *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const double *);
  // Set values of all elements from array (row major order).

  lightN3double operator+() const;
  // Unary plus.

  lightN3double operator-() const;
  // Unary minus.

  friend inline lightN33double Apply(const lightN33double&, double f(double));
  // Apply the function elementwise all elements.

  friend inline lightN33double Apply(const lightN33double&, const lightN33double&, double f(double, double));
  // Apply the function elementwise on all elements in the two tensors.

  //<ignore>
  friend class light3double;
  friend class lightNdouble;
  friend class lightN3double;
  friend class lightNNdouble;
  friend class lightNNNdouble;
  //</ignore>
  
protected:
//FOOFOO    light33double sarea[LIGHTN33_SIZE];

  light33double *elem;
  // The matrixes are stored here.

  int size;
  // The size of the tensor.

  int alloc_size;
  // The size of the allocated area.

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  lightN33double(const int x, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // argument is the size of the tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  lightN33double(const lightN33double&, double f(double), const lightmat_apply_enum);
  lightN33double(const lightN33double&, const lightN33double&, double f(double, double), const lightmat_apply_enum);
  lightN33double(const lightN33double&, const lightmat_trans_enum);
};

typedef lightN33double doubleN33;
typedef lightN33int intN33;

#endif
