//           -*- c++ -*-

#ifdef LIGHTMAT_OUTPUT_FUNCS

#ifndef IN_OUTPUT<T>
#define IN_OUTPUT<T>

#include "rw/cstring.h"

//
// ToStr
//
#ifndef __RWCSTRING_H__
inline RWCString
ToStr(const double d);

inline RWCString
ToStr(const int d);

inline RWCString
ToStr(const lm_complex & d);
#endif

template<class T>
inline RWCString
ToStr(const lightN<T>& s);

template<class T>
inline RWCString
ToStr(const lightNN<T>& s, int depth=0);

template<class T>
inline RWCString
ToStr(const lightNNN<T>& s, int depth=0);

template<class T>
inline RWCString
ToStr(const lightNNNN<T>& s, int depth=0);

template<class T>
inline RWCString
light_export(const char* file, const lightN<T>& s, const char* format, const char* option, const char* optionval);

template<class T>
inline RWCString
light_export(const char* file, const lightNN<T>& s, const char* format, const char* option, const char* optionval);


template<class T>
inline RWCString
light_export1(const char* file, const lightN<T>& s, const char* option, const char* optionval);


template<class T>
inline RWCString
light_export1(const char* file, const lightNN<T>& s, const char* option, const char* optionval);

//
// operator<<
//

inline ostream& operator<<(ostream& o, const lm_complex& s);

template<class T>
inline ostream&
operator<<(ostream& o, const light3<T>& s);

template<class T>
inline ostream&
operator<<(ostream& o, const light4<T>& s);

template<class T>
inline ostream&
operator<<(ostream& o, const lightN<T>& s);

template<class T>
inline ostream&
operator<<(ostream& o, const light33<T>& s);

template<class T>
inline ostream&
operator<<(ostream& o, const light44<T>& s);

template<class T>
inline ostream&
operator<<(ostream& o, const lightN3<T>& s);

template<class T>
inline ostream&
operator<<(ostream& o, const lightNN<T>& s);

template<class T>
inline ostream&
operator<<(ostream& o, const lightNNN<T>& s);

template<class T>
inline ostream&
operator<<(ostream& o, const lightN33<T>& s);

template<class T>
inline ostream&
operator<<(ostream& o, const lightNNNN<T>& s);


template<class T>
inline lightN<T> 
light_importN<T>(const char* filename, const char* format, const char* opt1, const char* opt2);

template<class T>
inline lightNN<T> 
light_importNN<T>(const char* filename, const char* format, const char* opt1, const char* opt2);


template<class T>
inline lightN<T> 
light_import1N<T>(const char* file);

template<class T>
inline lightNN<T> 
light_import1NN<T>(const char* file);

#endif

#endif // LIGHTMAT_OUTPUT_FUNCS
