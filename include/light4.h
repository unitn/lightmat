//           -*- c++ -*-

#ifndef LIGHT4_H
#define LIGHT4_H
// <cd> light4
//
// .SS Functionality
//
// light4 is a template for classes that implement vectors with 4
// elements. E.g. a vector v with 4 elements of type double can be
// instanciated with:
//
// <code>light4&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#define IN_LIGHT4<T>_H

template<class T> class lightN;
template<class T> class light44;
template<class T> class lightNN;
template<class T> class lightNNN;
template<class T> class lightNNNN;

template<class T>
class light4 {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light4<double>();
  // Convert to double.
#else
#endif
#ifdef LIGHTMAT_TEMPLATES
  friend class light4<int>;
#endif

#ifdef IN_LIGHT4<double>_H
  friend class light4<int>;
#else
  friend class light4<double>;
#endif

  #include "light4_auto.h"

  light4();
  // Default constructor.

  light4(const light4<T>&);
  // Copy constructor.
  
  light4(const T, const T, const T, const T);
  // Initialize elements with values.

  light4(const T *);
  // Initialize elements with values from an array.

  light4(const T);
  // Initialize all elements with the same value.

  light4<T>& operator=(const light4<T>& s);
  // Assignment.
  
  light4<T>& operator=(const lightN<T>& s);
  // Assignment from a lightN where N=3.

  light4<T>& operator=(const T);
  // Assign one value to all elements.

  T operator()(const int x) const {
    limiterror((x<1) || (x>4));
    return elem[x-1];
  };
  // Get the value of one element.

  T& operator()(const int x) {
    limiterror((x<1) || (x>4));
    return elem[x-1];
  };
  // Get/Set the value of one element.
  int operator==(const light4<T>&) const;
  // Equality.

  int operator!=(const light4<T>&) const;
  // Inequality.

  light4<T>& operator+=(const T);
  // Add a value to all elements.

  light4<T>& operator+=(const light4<T>&);
  // Elementwise addition.

  light4<T>& operator-=(const T);
  // Subtract a value from all elements.

  light4<T>& operator-=(const light4<T>&);
  // Elementwise subtraction.

  light4<T>& operator*=(const T);
  // Muliply all elements with a value.

  light4<T>& operator/=(const T);
  // Divide all elements with a value.
 
  #ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
  #endif
  int dimension(const int x = 1) const {
    if(x == 1)
      return 4;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 4). If this was a lightN
  // then dimension(1) would return the number of elements.
 
  #ifndef COMPLEX_TOOLS
  light4<T>& normalize();
  // Normalize vector.
  #endif

  void Get(T *) const;
  // Get values of all elements and put them in an array (4 elements long).

  void Set(const T *);
  // Set values of all elements from array (4 elements long).
 
  light4<T> operator+() const;
  // Unary plus.

  light4<T> operator-() const;
  // Unary minus.

  friend inline light4<T> operator+(const light4<T>&, const light4<T>&);
  // Elementwise addition.

  friend inline light4<T> operator+(const light4<T>&, const T);
  // Addition to all elements.

  friend inline light4<T> operator+(const T, const light4<T>&);
  // Addition to all elements.

  friend inline light4<T> operator-(const light4<T>&, const light4<T>&);
  // Elementwise subtraction.

  friend inline light4<T> operator-(const light4<T>&, const T);
  // Subtraction from all elements.

  friend inline light4<T> operator-(const T, const light4<T>&);
  // Subtraction to all elements.

  friend inline T operator*(const light4<T>&, const light4<T>&);
  // Inner product.

  friend inline light4<T> operator*(const light4<T>&, const T);
  // Multiply all elements.

  friend inline light4<T> operator*(const T, const light4<T>&);
  // Multiply all elements.

  friend inline light4<T> operator*(const light4<T>&, const light44<T>&);
  // Inner product.

  friend inline light4<T> operator*(const light44<T>&, const light4<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const light4<T>&, const lightNN<T>&);
  // Inner product.

  friend inline light4<T> operator/(const light4<T>&, const T);
  // Divide all elements.

  friend inline light4<T> operator/(const T, const light4<T>&);
  // Divide with all elements.

  friend inline light4<T> pow(const light4<T>&, const light4<T>&);
  // Raise to the power of-function, elementwise.

  friend inline light4<T> pow(const light4<T>&, const T);
  // Raise to the power of-function, for all elements.

  friend inline light4<T> pow(const T, const light4<T>&);
  // Raise to the power of-function, for all elements.

  friend inline light4<T> ElemProduct(const light4<T>&, const light4<T>&);
  // Elementwise multiplication.

  friend inline light4<T> ElemQuotient(const light4<T>&, const light4<T>&);
  // Elementwise division.

  friend inline light4<T> Apply(const light4<T>&, T f(T));
  // Apply the function elementwise on all four elements.

  friend inline light4<T> Apply(const light4<T>&, const light4<T>& ,T f(T, T));
  // Apply the function elementwise on all four elements in the two
  // vecors.

  friend inline light44<T> OuterProduct(const light4<T>&, const light4<T>&);
  // Outer product.

  friend inline light4<T> abs(const light4<T>&);
  // abs

#ifndef COMPLEX_TOOLS
  friend inline light4<int> sign(const light4<T>&);
  // sign
#else
  friend inline light4lm_complex sign(const light4lm_complex&);
#endif
  friend inline light4<int> ifloor(const light4<double>&);
  // ifloor

  friend inline light4<int> iceil(const light4<double>&);
  // iceil

  friend inline light4<int> irint(const light4<double>&);
  // irint

  friend inline light4<double> sqrt(const light4<double>&);
  // sqrt

  friend inline light4<double> exp(const light4<double>&);
  // exp

  friend inline light4<double> log(const light4<double>&);
  // log

  friend inline light4<double> sin(const light4<double>&);
  // sin

  friend inline light4<double> cos(const light4<double>&);
  // cos

  friend inline light4<double> tan(const light4<double>&);
  // tan

  friend inline light4<double> asin(const light4<double>&);
  // asin

  friend inline light4<double> acos(const light4<double>&);
  // acos

  friend inline light4<double> atan(const light4<double>&);
  // atan

  friend inline light4<double> sinh(const light4<double>&);
  // sinh

  friend inline light4<double> cosh(const light4<double>&);
  // cosh

  friend inline light4<double> tanh(const light4<double>&);
  // tanh

  friend inline light4<double> asinh(const light4<double>&);
  // asinh

  friend inline light4<double> acosh(const light4<double>&);
  // acosh

  friend inline light4<double> atanh(const light4<double>&);
  // atanh

  friend inline light4<lm_complex> ifloor(const light4<lm_complex>&);
  // ifloor

  friend inline light4<lm_complex> iceil(const light4<lm_complex>&);
  // iceil

  friend inline light4<lm_complex> irint(const light4<lm_complex>&);
  // irint

  friend inline light4<lm_complex> sqrt(const light4<lm_complex>&);
  // sqrt

  friend inline light4<lm_complex> exp(const light4<lm_complex>&);
  // exp

  friend inline light4<lm_complex> log(const light4<lm_complex>&);
  // log

  friend inline light4<lm_complex> sin(const light4<lm_complex>&);
  // sin

  friend inline light4<lm_complex> cos(const light4<lm_complex>&);
  // cos

  friend inline light4<lm_complex> tan(const light4<lm_complex>&);
  // tan

  friend inline light4<lm_complex> asin(const light4<lm_complex>&);
  // asin

  friend inline light4<lm_complex> acos(const light4<lm_complex>&);
  // acos

  friend inline light4<lm_complex> atan(const light4<lm_complex>&);
  // atan

  friend inline light4<lm_complex> sinh(const light4<lm_complex>&);
  // sinh

  friend inline light4<lm_complex> cosh(const light4<lm_complex>&);
  // cosh

  friend inline light4<lm_complex> tanh(const light4<lm_complex>&);
  // tanh

  friend inline light4<lm_complex> asinh(const light4<lm_complex>&);
  // asinh

  friend inline light4<lm_complex> acosh(const light4<lm_complex>&);
  // acosh

  friend inline light4<lm_complex> atanh(const light4<lm_complex>&);
  // atanh


  //<ignore>
//    friend class light4<int>;
  friend class lightN<T>;
  friend class light44<T>;
  friend class lightNN<T>;
  friend class lightNNN<T>;
  friend class lightNNNN<T>;

  friend inline lightN<T> light_join (const lightN<T> arr1, const lightN<T> arr2);

  //</ignore>

protected:
  T elem[4];
  // The values of the four elements.

  light4(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The size of the vector (number of elements).


};

typedef light4<double> double4;
typedef light4<int> int4;

#undef IN_LIGHT4<T>_H

#endif
