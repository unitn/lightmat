//           -*- c++ -*-

#define IN_CALC<T>_H


//
// operator+
//
template<class T>
lightN<T> operator+(const lightN<T>& s1, const lightN<T>& s2);

//--
template<class T>
lightN<T> operator+(const lightN<T>& s1, const T e);

template<class T>
lightN<T> operator+(const T e, const lightN<T>& s2);

#ifdef IN_CALC<double>_H
lightNdouble operator+(const lightNdouble& s1, const int e);
lightNdouble operator+(const lightNint& s1, const double e);
lightNdouble operator+(const double e, const lightNint& s2);
lightNdouble operator+(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNlm_complex operator+(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator+(const lightNint& s1, const lm_complex e);
lightNlm_complex operator+(const lightNlm_complex& s1, const double e);
lightNlm_complex operator+(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator+(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator+(const int e, const lightNlm_complex& s2);
lightNlm_complex operator+(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator+(const double e, const lightNlm_complex& s2);
#endif

//--


template<class T>
lightN3<T> operator+(const lightN3<T>& s1, const lightN3<T>& s2);

template<class T>
lightN3<T> operator+(const lightN3<T>& s1, const T e);

template<class T>
lightN3<T> operator+(const T e, const lightN3<T>& s2);

template<class T>
lightNN<T> operator+(const lightNN<T>& s1, const lightNN<T>& s2);

template<class T>
lightNN<T> operator+(const lightNN<T>& s1, const T e);

template<class T>
lightNN<T> operator+(const T e, const lightNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNdouble operator+(const lightNNdouble& s1, const int e);
lightNNdouble operator+(const lightNNint& s1, const double e);
lightNNdouble operator+(const double e, const lightNNint& s2);
lightNNdouble operator+(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNlm_complex operator+(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator+(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator+(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator+(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator+(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator+(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator+(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator+(const double e, const lightNNlm_complex& s2);
#endif

template<class T>
lightNNN<T> operator+(const lightNNN<T>& s1, const lightNNN<T>& s2);

template<class T>
lightNNN<T> operator+(const lightNNN<T>& s1, const T e);

template<class T>
lightNNN<T> operator+(const T e, const lightNNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNNdouble operator+(const lightNNNdouble& s1, const int e);
lightNNNdouble operator+(const lightNNNint& s1, const double e);
lightNNNdouble operator+(const double e, const lightNNNint& s2);
lightNNNdouble operator+(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator+(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator+(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator+(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator+(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator+(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator+(const double e, const lightNNNlm_complex& s2);
#endif

template<class T>
lightNNNN<T> operator+(const lightNNNN<T>& s1, const lightNNNN<T>& s2);

template<class T>
lightNNNN<T> operator+(const lightNNNN<T>& s1, const T e);

template<class T>
lightNNNN<T> operator+(const T e, const lightNNNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNNNdouble operator+(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator+(const lightNNNNint& s1, const double e);
lightNNNNdouble operator+(const double e, const lightNNNNint& s2);
lightNNNNdouble operator+(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator+(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator+(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator+(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator+(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator+(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator+(const double e, const lightNNNNlm_complex& s2);
#endif

//
// operator-
//
template<class T>
lightN<T> operator-(const lightN<T>& s1, const lightN<T>& s2);

template<class T>
lightN<T> operator-(const lightN<T>& s1, const T e);

template<class T>
lightN<T> operator-(const T e, const lightN<T>& s2);

#ifdef IN_CALC<double>_H
lightNdouble operator-(const lightNdouble& s1, const int e);
lightNdouble operator-(const lightNint& s1, const double e);
lightNdouble operator-(const double e, const lightNint& s2);
lightNdouble operator-(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNlm_complex operator-(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator-(const lightNint& s1, const lm_complex e);
lightNlm_complex operator-(const lightNlm_complex& s1, const double e);
lightNlm_complex operator-(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator-(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator-(const int e, const lightNlm_complex& s2);
lightNlm_complex operator-(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator-(const double e, const lightNlm_complex& s2);
#endif

template<class T>
lightN3<T> operator-(const lightN3<T>& s1, const lightN3<T>& s2);

template<class T>
lightN3<T> operator-(const lightN3<T>& s1, const T e);

template<class T>
lightN3<T> operator-(const T e, const lightN3<T>& s2);

template<class T>
lightNN<T> operator-(const lightNN<T>& s1, const lightNN<T>& s2);

template<class T>
lightNN<T> operator-(const lightNN<T>& s1, const T e);

template<class T>
lightNN<T> operator-(const T e, const lightNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNdouble operator-(const lightNNdouble& s1, const int e);
lightNNdouble operator-(const lightNNint& s1, const double e);
lightNNdouble operator-(const double e, const lightNNint& s2);
lightNNdouble operator-(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNlm_complex operator-(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator-(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator-(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator-(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator-(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator-(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator-(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator-(const double e, const lightNNlm_complex& s2);
#endif

template<class T>
lightNNN<T> operator-(const lightNNN<T>& s1, const lightNNN<T>& s2);

template<class T>
lightNNN<T> operator-(const lightNNN<T>& s1, const T e);

template<class T>
lightNNN<T> operator-(const T e, const lightNNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNNdouble operator-(const lightNNNdouble& s1, const int e);
lightNNNdouble operator-(const lightNNNint& s1, const double e);
lightNNNdouble operator-(const double e, const lightNNNint& s2);
lightNNNdouble operator-(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator-(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator-(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator-(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator-(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator-(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator-(const double e, const lightNNNlm_complex& s2);

#endif


template<class T>
lightNNNN<T> operator-(const lightNNNN<T>& s1, const lightNNNN<T>& s2);

template<class T>
lightNNNN<T> operator-(const lightNNNN<T>& s1, const T e);

template<class T>
lightNNNN<T> operator-(const T e, const lightNNNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNNNdouble operator-(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator-(const lightNNNNint& s1, const double e);
lightNNNNdouble operator-(const double e, const lightNNNNint& s2);
lightNNNNdouble operator-(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator-(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator-(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator-(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator-(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator-(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator-(const double e, const lightNNNNlm_complex& s2);
#endif

//
// operator* (inner product)
//
template<class T>
lightN<T> operator*(const lightN<T>& s1, const T e);

template<class T>
lightN<T> operator*(const T e, const lightN<T>& s2);

#ifdef IN_CALC<double>_H
lightNdouble operator*(const lightNdouble& s1, const int e);
lightNdouble operator*(const lightNint& s1, const double e);
lightNdouble operator*(const double e, const lightNint& s2);
lightNdouble operator*(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNlm_complex operator*(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator*(const lightNint& s1, const lm_complex e);
lightNlm_complex operator*(const lightNlm_complex& s1, const double e);
lightNlm_complex operator*(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator*(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator*(const int e, const lightNlm_complex& s2);
lightNlm_complex operator*(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator*(const double e, const lightNlm_complex& s2);
#endif


template<class T>
lightN<T> operator*(const lightN<T>& s1, const lightNN<T>& s2);

template<class T>
lightN<T> operator*(const lightNN<T>& s1, const lightN<T>& s2);

template<class T>
lightN<T> operator*(const light3<T>& s1, const lightNN<T>& s2);

template<class T>
lightN<T> operator*(const light4<T>& s1, const lightNN<T>& s2);

template<class T>
lightN<T> operator*(const lightN3<T>& s1, const light3<T>& s2);

template<class T>
lightN<T> operator*(const lightN3<T>& s1, const lightN<T>& s2);

template<class T>
lightN<T> operator*(const lightNN<T>& s1, const light3<T>& s2);

template<class T>
lightN<T> operator*(const lightNN<T>& s1, const light4<T>& s2);

template<class T>
lightN3<T> operator*(const lightN3<T>& s1, const lightN3<T>& s2);

template<class T>
lightN3<T> operator*(const lightN3<T>& s1, const T e);

template<class T>
lightN3<T> operator*(const lightNN<T>& s1, const lightN3<T>& s2);

template<class T>
lightN3<T> operator*(const lightN3<T>& s1, const light33<T>& s2);

template<class T>
lightN3<T> operator*(const light44<T>& s1, const lightN3<T>& s2);

template<class T>
lightN3<T> operator*(const T e, const lightN3<T>& s2);

template<class T>
lightNN<T> operator*(const lightNN<T>& s1, const lightNN<T>& s2);

template<class T>
lightNN<T> operator*(const lightNN<T>& s1, const T e);

template<class T>
lightNN<T> operator*(const T e, const lightNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNdouble operator*(const lightNNdouble& s1, const int e);
lightNNdouble operator*(const lightNNint& s1, const double e);
lightNNdouble operator*(const double e, const lightNNint& s2);
lightNNdouble operator*(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNlm_complex operator*(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator*(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator*(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator*(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator*(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator*(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator*(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator*(const double e, const lightNNlm_complex& s2);
#endif

template<class T>
lightNN<T> operator*(const lightNN<T>& s1, const light33<T>& s2);

template<class T>
lightNN<T> operator*(const lightNN<T>& s1, const light44<T>& s2);

template<class T>
lightNN<T> operator*(const lightN3<T>& s1, const lightNN<T>& s2);

template<class T>
lightNN<T> operator*(const light33<T>& s1, const lightNN<T>& s2);

template<class T>
lightNN<T> operator*(const light44<T>& s1, const lightNN<T>& s2);

template<class T>
lightNN<T> operator*(const lightN<T>& s1, const lightNNN<T>& s2);

template<class T>
lightNN<T> operator*(const lightNNN<T>& s1, const lightN<T>& s2);

template<class T>
lightNNN<T> operator*(const lightNNN<T>& s1, const T e);

template<class T>
lightNNN<T> operator*(const T e, const lightNNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNNdouble operator*(const lightNNNdouble& s1, const int e);
lightNNNdouble operator*(const lightNNNint& s1, const double e);
lightNNNdouble operator*(const double e, const lightNNNint& s2);
lightNNNdouble operator*(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator*(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator*(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator*(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator*(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator*(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator*(const double e, const lightNNNlm_complex& s2);
#endif

template<class T>
lightNNN<T> operator*(const lightNNN<T>& s1, const lightNN<T>& s2);

template<class T>
lightNNN<T> operator*(const lightNN<T>& s1, const lightNNN<T>& s2);

template<class T>
lightNNN<T> operator*(const lightNNNN<T>& s1, const lightN<T>& s2);

template<class T>
lightNNN<T> operator*(const lightN<T>& s1, const lightNNNN<T>& s2);

template<class T>
lightNNNN<T> operator*(const lightNNNN<T>& s1, const T e);

template<class T>
lightNNNN<T> operator*(const T e, const lightNNNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNNNdouble operator*(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator*(const lightNNNNint& s1, const double e);
lightNNNNdouble operator*(const double e, const lightNNNNint& s2);
lightNNNNdouble operator*(const int e, const lightNNNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator*(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator*(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator*(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator*(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator*(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator*(const double e, const lightNNNNlm_complex& s2);
#endif

template<class T>
lightNNNN<T> operator*(const lightNNNN<T>& s1, const lightNN<T>& s2);

template<class T>
lightNNNN<T> operator*(const lightNN<T>& s1, const lightNNNN<T>& s2);

template<class T>
lightNNNN<T> operator*(const lightNNN<T>& s1, const lightNNN<T>& s2);

//
// operator/
//
template<class T>
lightN<T> operator/(const lightN<T>& s1, const T e);

template<class T>
lightN<T> operator/(const T e, const lightN<T>& s2);

#ifdef IN_CALC<double>_H
lightNdouble operator/(const lightNdouble& s1, const int e);
lightNdouble operator/(const lightNint& s1, const double e);
lightNdouble operator/(const double e, const lightNint& s2);
lightNdouble operator/(const int e, const lightNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNlm_complex operator/(const lightNlm_complex& s1, const int e);
//lightNlm_complex operator/(const lightNint& s1, const lm_complex e);
lightNlm_complex operator/(const lightNlm_complex& s1, const double e);
lightNlm_complex operator/(const lightNdouble& s1, const lm_complex e);
//lightNlm_complex operator/(const lm_complex e, const lightNint& s2);
//lightNlm_complex operator/(const int e, const lightNlm_complex& s2);
lightNlm_complex operator/(const lm_complex e, const lightNdouble& s2);
lightNlm_complex operator/(const double e, const lightNlm_complex& s2);
#endif

template<class T>
lightN3<T> operator/(const lightN3<T>& s1, const T e);

template<class T>
lightN3<T> operator/(const T e, const lightN3<T>& s2);

template<class T>
lightNN<T> operator/(const lightNN<T>& s1, const T e);

template<class T>
lightNN<T> operator/(const T e, const lightNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNdouble operator/(const lightNNdouble& s1, const int e);
lightNNdouble operator/(const lightNNint& s1, const double e);
lightNNdouble operator/(const double e, const lightNNint& s2);
lightNNdouble operator/(const int e, const lightNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNlm_complex operator/(const lightNNlm_complex& s1, const int e);
//lightNNlm_complex operator/(const lightNNint& s1, const lm_complex e);
lightNNlm_complex operator/(const lightNNlm_complex& s1, const double e);
lightNNlm_complex operator/(const lightNNdouble& s1, const lm_complex e);
//lightNNlm_complex operator/(const lm_complex e, const lightNNint& s2);
//lightNNlm_complex operator/(const int e, const lightNNlm_complex& s2);
lightNNlm_complex operator/(const lm_complex e, const lightNNdouble& s2);
lightNNlm_complex operator/(const double e, const lightNNlm_complex& s2);
#endif

template<class T>
lightNNN<T> operator/(const lightNNN<T>& s1, const T e);

template<class T>
lightNNN<T> operator/(const T e, const lightNNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNNdouble operator/(const lightNNNdouble& s1, const int e);
lightNNNdouble operator/(const lightNNNint& s1, const double e);
lightNNNdouble operator/(const double e, const lightNNNint& s2);
lightNNNdouble operator/(const int e, const lightNNNdouble& s2);
#endif

#ifdef IN_CALC<lm_complex>_H
//lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const int e);
//lightNNNlm_complex operator/(const lightNNNint& s1, const lm_complex e);
lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const double e);
lightNNNlm_complex operator/(const lightNNNdouble& s1, const lm_complex e);
//lightNNNlm_complex operator/(const lm_complex e, const lightNNNint& s2);
//lightNNNlm_complex operator/(const int e, const lightNNNlm_complex& s2);
lightNNNlm_complex operator/(const lm_complex e, const lightNNNdouble& s2);
lightNNNlm_complex operator/(const double e, const lightNNNlm_complex& s2);
#endif

template<class T>
lightNNNN<T> operator/(const lightNNNN<T>& s1, const T e);

template<class T>
lightNNNN<T> operator/(const T e, const lightNNNN<T>& s2);

#ifdef IN_CALC<double>_H
lightNNNNdouble operator/(const lightNNNNdouble& s1, const int e);
lightNNNNdouble operator/(const lightNNNNint& s1, const double e);
lightNNNNdouble operator/(const double e, const lightNNNNint& s2);
lightNNNNdouble operator/(const int e, const lightNNNNdouble& s2);
#endif


#ifdef IN_CALC<lm_complex>_H
//lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const int e);
//lightNNNNlm_complex operator/(const lightNNNNint& s1, const lm_complex e);
lightNNNNlm_complex operator/(const lightNNNNlm_complex& s1, const double e);
lightNNNNlm_complex operator/(const lightNNNNdouble& s1, const lm_complex e);
//lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNint& s2);
//lightNNNNlm_complex operator/(const int e, const lightNNNNlm_complex& s2);
lightNNNNlm_complex operator/(const lm_complex e, const lightNNNNdouble& s2);
lightNNNNlm_complex operator/(const double e, const lightNNNNlm_complex& s2);
#endif


//
// ElemProduct
//
template<class T>
lightN<T> ElemProduct(const lightN<T>& s1, const lightN<T>& s2);

template<class T>
lightNN<T> ElemProduct(const lightNN<T>& s1, const lightNN<T>& s2);

template<class T>
lightN3<T> ElemProduct(const lightN3<T>& s1, const lightN3<T>& s2);

template<class T>
lightNNN<T> ElemProduct(const lightNNN<T>& s1, const lightNNN<T>& s2);

template<class T>
lightNNNN<T> ElemProduct(const lightNNNN<T>& s1, const lightNNNN<T>& s2);

//
// ElemQuotient
//
template<class T>
lightN<T> ElemQuotient(const lightN<T>& s1, const lightN<T>& s2);

template<class T>
lightNN<T> ElemQuotient(const lightNN<T>& s1, const lightNN<T>& s2);

template<class T>
lightN3<T> ElemQuotient(const lightN3<T>& s1, const lightN3<T>& s2);

template<class T>
lightNNN<T> ElemQuotient(const lightNNN<T>& s1, const lightNNN<T>& s2);

template<class T>
lightNNNN<T> ElemQuotient(const lightNNNN<T>& s1, const lightNNNN<T>& s2);

//
// Apply
//
template<class T>
lightN<T> Apply(const lightN<T>& s, T f(T));

template<class T>
lightN<T> Apply(const lightN<T>& s1, const lightN<T>& s2, T f(T, T));

template<class T>
lightNN<T> Apply(const lightNN<T>& s, T f(T));

template<class T>
lightNN<T> Apply(const lightNN<T>& s1, const lightNN<T>& s2, T f(T, T));

template<class T>
lightNNN<T> Apply(const lightNNN<T>& s, T f(T));

template<class T>
lightNNN<T> Apply(const lightNNN<T>& s1, const lightNNN<T>& s2, T f(T, T));

template<class T>
lightNNNN<T> Apply(const lightNNNN<T>& s, T f(T));

template<class T>
lightNNNN<T> Apply(const lightNNNN<T>& s1, const lightNNNN<T>& s2, T f(T, T));

//
// Transpose
//
template<class T>
lightNN<T> Transpose(const lightN3<T>& s);

template<class T>
lightNN<T> Transpose(const lightNN<T>& s);

template<class T>
lightNNN<T> Transpose(const lightNNN<T>& s);

template<class T>
lightNNNN<T> Transpose(const lightNNNN<T>& s);

template<class T>
lightNN<T> OuterProduct(const lightN<T>& s1, const lightN<T>& s2);

//
// pow
//
template<class T>
lightN<T> pow(const lightN<T>& s1, const lightN<T>& s2);

template<class T>
lightN<T> pow(const lightN<T>& s, const T e);

template<class T>
lightN<T> pow(const T e, const lightN<T>& s);

// Why this is needed for lightNlm_complex only ?
// This is because lightNlm_complex  accepts a (double) argument as
// constructor argument, via automatic conversion double=>integer.
// In constrast  lightNNlm_complex  accepts two (double) arguments as
// constructor argument, not just one,


lightNlm_complex pow(const lightNlm_complex& s1, const double e);

lightNlm_complex pow( const double e,const lightNlm_complex& s1);


template<class T>
lightN3<T> pow(const lightN3<T>& s1, const lightN3<T>& s2);

template<class T>
lightN3<T> pow(const lightN3<T>& s, const T e);

template<class T>
lightN3<T> pow(const T e, const lightN3<T>& s);

template<class T>
lightNN<T> pow(const lightNN<T>& s1, const lightNN<T>& s2);

template<class T>
lightNN<T> pow(const lightNN<T>& s, const T e);

template<class T>
lightNN<T> pow(const T e, const lightNN<T>& s);

template<class T>
lightNNN<T> pow(const lightNNN<T>& s1, const lightNNN<T>& s2);

template<class T>
lightNNN<T> pow(const lightNNN<T>& s, const T e);

template<class T>
lightNNN<T> pow(const T e, const lightNNN<T>& s);

template<class T>
lightNNNN<T> pow(const lightNNNN<T>& s1, const lightNNNN<T>& s2);

template<class T>
lightNNNN<T> pow(const lightNNNN<T>& s, const T e);

template<class T>
lightNNNN<T> pow(const T e, const lightNNNN<T>& s);



#include "light_calc_auto.h"

#undef IN_CALC<T>_H
