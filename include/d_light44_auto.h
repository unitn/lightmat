inline light44double& Set (const int i0, const int i1, const double val);
inline light44double& Set (const R r0, const int i1, const lightNdouble& arr);
inline light44double& Set (const R r0, const int i1, const light3double& arr);
inline light44double& Set (const R r0, const int i1, const light4double& arr);
inline light44double& Set (const R r0, const int i1, const double val);
inline light44double& Set (const int i0, const R r1, const lightNdouble& arr);
inline light44double& Set (const int i0, const R r1, const light3double& arr);
inline light44double& Set (const int i0, const R r1, const light4double& arr);
inline light44double& Set (const int i0, const R r1, const double val);
inline light44double& Set (const R r0, const R r1, const lightNNdouble& arr);
inline light44double& Set (const R r0, const R r1, const light33double& arr);
inline light44double& Set (const R r0, const R r1, const light44double& arr);
inline light44double& Set (const R r0, const R r1, const double val);
inline lightNdouble operator() (const R r0, const int i1) const;
inline lightNdouble operator() (const int i0, const R r1) const;
inline lightNNdouble operator() (const R r0, const R r1) const;
friend inline light44double atan2 (const double e, const light44int &s1);
friend inline light44double atan2 (const double e, const light44double &s1);
friend inline light44double atan2 (const light44int &s1, const double e);
friend inline light44double atan2 (const light44double &s1, const double e);
friend inline light44double atan2 (const light44int &s1, const light44int &s2);
friend inline light44double atan2 (const light44int &s1, const light44double &s2);
friend inline light44double atan2 (const light44double &s1, const light44int &s2);
friend inline light44double atan2 (const light44double &s1, const light44double &s2);
friend inline light44double atan2 (const light44int &s1, const int e);
friend inline light44double atan2 (const light44double &s1, const int e);
friend inline light44double atan2 (const int e, const light44int &s1);
friend inline light44double atan2 (const int e, const light44double &s1);

#ifdef IN_LIGHT44double_H
inline light44double(const double e, const light44int &s1, const lightmat_atan2_enum);
inline light44double(const double e, const light44double &s1, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const double e, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const double e, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const light44int &s2, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const light44double &s2, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const light44int &s2, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const light44double &s2, const lightmat_atan2_enum);
inline light44double(const light44int &s1, const int e, const lightmat_atan2_enum);
inline light44double(const light44double &s1, const int e, const lightmat_atan2_enum);
inline light44double(const int e, const light44int &s1, const lightmat_atan2_enum);
inline light44double(const int e, const light44double &s1, const lightmat_atan2_enum);

#endif
