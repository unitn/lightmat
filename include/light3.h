//           -*- c++ -*-

#ifndef LIGHT3_H
#define LIGHT3_H
// <cd> light3
//
// .SS Functionality
//
// light3 is a template for classes that implement vectors with 3
// elements. E.g. a vector v with 3 elements of type double can be
// instanciated with:
//
// <code>light3&lt;double&gt; v;</code>
//
// .SS Author
//
// Anders Gertz
//

#define IN_LIGHT3<T>_H

template<class T> class light4;

template<class T> class lightN;
template<class T> class light33;
template<class T> class lightN3;
template<class T> class lightN33;
template<class T> class lightNN;
template<class T> class lightNNN;
template<class T> class lightNNNN;

template<class T>
class light3 {
public:

#ifdef CONV_INT_2_DOUBLE
  operator light3<double>();
  // Convert to double.
#else
  friend class lightN3<int>;
  friend class lightN33<int>;
#endif
#ifdef LIGHTMAT_TEMPLATES
  //  friend class light3<int>; 
  friend class lightN3<int>;
  friend class lightN33<int>;
#endif

#ifdef IN_LIGHT3<double>_H
  friend class light3<int>; 
#else
  friend class light3<double>; 
#endif

  #include "light3_auto.h"

  light3();
  // Default constructor.

  light3(const light3<T>&);
  // Copy constructor.

  light3(const T, const T, const T);
  // Initialize elements with values.

  light3(const T *);
  // Initialize elements with values from an array.

  light3(const T);
  // Initialize all elements with the same value.

  light3<T>& operator=(const light3<T>&);
  // Assignment.

  light3<T>& operator=(const lightN<T>&);
  // Assignment from a lightN where N=3.

  light3<T>& operator=(const T);
  // Assign one value to all elements.

  T operator()(const int x) const {
    limiterror((x<1) || (x>3));
    return elem[x-1];
  };
  // Get the value of one element.

  T& operator()(const int x) {
    limiterror((x<1) || (x>3));
    return elem[x-1];
  };
  // Get/Set the value of one element.


  int operator==(const light3<T>&) const;
  // Equality.

  int operator!=(const light3<T>&) const;
  // Inequality.

  light3<T>& operator+=(const T);
  // Add a value to all elements.

  light3<T>& operator+=(const light3<T>&);
  // Elementwise addition.

  light3<T>& operator-=(const T);
  // Subtract a value from all elements.

  light3<T>& operator-=(const light3<T>&);
  // Elementwise subtraction.

  light3<T>& operator*=(const T);
  // Muliply all elements with a value.

  light3<T>& operator/=(const T);
  // Divide all elements with a value.

  #ifndef COMPLEX_TOOLS
  double length() const;
  // Get length of vector.
  #endif
  int dimension(const int x = 1) const {
    if (x == 1)
      return 3;
    else
      return 1;
  };
  // Get size of some dimension (dimension(1) == 3). If this was a lightN
  // then dimension(1) would return the number of elements.
 
  #ifndef COMPLEX_TOOLS
  light3<T>& normalize();
  // Normalize vector.
  #endif

  void Get(T *) const;
  // Get values of all elements and put them in an array (3 elements long).

  void Set(const T *);
  // Set values of all elements from array (3 elements long).

  void Get( T&, T&, T&) const;
  // Get the value of the three elements.

  void Set(const T, const T, const T);
  // Set the value of the three elements.

  light3<T> operator+() const;
  // Unary plus.

  light3<T> operator-() const;
  // Unary minus.

  friend inline light3<T> operator+(const light3<T>&, const light3<T>&);
  // Elementwise addition.

  friend inline light3<T> operator+(const light3<T>&, const T);
  // Addition to all elements.

  friend inline light3<T> operator+(const T, const light3<T>&);
  // Addition to all elements.

  friend inline light3<T> operator-(const light3<T>&, const light3<T>&);
  // Elementwise subtraction.

  friend inline light3<T> operator-(const light3<T>&, const T);
  // Subtraction from all elements.

  friend inline light3<T> operator-(const T, const light3<T>&);
  // Subtraction to all elements.

  friend inline T operator*(const light3<T>&, const light3<T>&);
  // Inner product.

  friend inline T Dot(const light3<T>&, const light3<T>&);
  // Inner product.

  friend inline light3<T> operator*(const light3<T>&, const T);
  // Multiply all elements.

  friend inline light3<T> operator*(const T, const light3<T>&);
  // Multiply all elements.

  friend inline light3<T> operator*(const light3<T>&, const light33<T>&);
  // Inner product.

  friend inline light3<T> operator*(const light33<T>&, const light3<T>&);
  // Inner product.

  friend inline lightN<T> operator*(const light3<T>&, const lightNN<T>&);
  // Inner product.

  friend inline light3<T> Dot(const light3<T>&, const light33<T>&);
  // Inner product.

  friend inline light3<T> Dot(const light33<T>&, const light3<T>&);
  // Inner product.

  friend inline light3<T> operator/(const light3<T>&, const T);
  // Divide all elements.

  friend inline light3<T> operator/(const T, const light3<T>&);
  // Divide with all elements.

  friend inline light3<T> pow(const light3<T>&, const light3<T>&);
  // Raise to the power of-function, elementwise.

  friend inline light3<T> pow(const light3<T>&, const T);
  // Raise to the power of-function, for all elements.

  friend inline light3<T> pow(const T, const light3<T>&);
  // Raise to the power of-function, for all elements.

  friend inline light3<T> ElemProduct(const light3<T>&, const light3<T>&);
  // Elementwise multiplication.

  friend inline light3<T> ElemQuotient(const light3<T>&, const light3<T>&);
  // Elementwise division.

  friend inline light3<T> Apply(const light3<T>&, T f(T));
  // Apply the function elementwise on all three elements.

  friend inline light3<T> Apply(const light3<T>&, const light3<T>&, T f(T, T));
  // Apply the function elementwise on all three elements in the two
  // vecors.

  friend inline light3<T> Cross(const light3<T>&, const light3<T>&);
  // Cross product.

  friend inline light33<T> OuterProduct(const light3<T>&, const light3<T>&);
  // Outer product.

  friend  inline  light3<T> abs(const light3<T>&);
  // abs

#ifndef COMPLEX_TOOLS
  friend  inline  light3<int> sign(const light3<int>&);
  // sign

  friend  inline  light3<int> sign(const light3<double>&);
  // sign
#else //for complex numbers
  friend  inline  light3lm_complex sign(const light3lm_complex&);

#endif
  friend  inline  light3<int> ifloor(const light3<double>&);
  // ifloor

  friend  inline  light3<int> iceil(const light3<double>&);
  // iceil

  friend  inline  light3<int> irint(const light3<double>&);
  // irint

  friend  inline  light3<double> sqrt(const light3<double>&);
  // sqrt

  friend  inline  light3<double> exp(const light3<double>&);
  // exp

  friend  inline  light3<double> log(const light3<double>&);
  // log

  friend  inline  light3<double> sin(const light3<double>&);
  // sin

  friend  inline  light3<double> cos(const light3<double>&);
  // cos

  friend  inline  light3<double> tan(const light3<double>&);
  // tan

  friend  inline  light3<double> asin(const light3<double>&);
  // asin

  friend  inline  light3<double> acos(const light3<double>&);
  // acos

  friend  inline  light3<double> atan(const light3<double>&);
  // atan

  friend  inline  light3<double> sinh(const light3<double>&);
  // sinh

  friend  inline  light3<double> cosh(const light3<double>&);
  // cosh

  friend  inline  light3<double> tanh(const light3<double>&);
  // tanh

  friend  inline  light3<double> asinh(const light3<double>&);
  // asinh

  friend  inline  light3<double> acosh(const light3<double>&);
  // acosh

  friend  inline  light3<double> atanh(const light3<double>&);
  // atanh

  friend  inline  light3<lm_complex> ifloor(const light3<lm_complex>&);
  // ifloor

  friend  inline  light3<lm_complex> iceil(const light3<lm_complex>&);
  // iceil

  friend  inline  light3<lm_complex> irint(const light3<lm_complex>&);
  // irint

  friend  inline  light3<lm_complex> sqrt(const light3<lm_complex>&);
  // sqrt

  friend  inline  light3<lm_complex> exp(const light3<lm_complex>&);
  // exp

  friend  inline  light3<lm_complex> log(const light3<lm_complex>&);
  // log

  friend  inline  light3<lm_complex> sin(const light3<lm_complex>&);
  // sin

  friend  inline  light3<lm_complex> cos(const light3<lm_complex>&);
  // cos

  friend  inline  light3<lm_complex> tan(const light3<lm_complex>&);
  // tan

  friend  inline  light3<lm_complex> asin(const light3<lm_complex>&);
  // asin

  friend  inline  light3<lm_complex> acos(const light3<lm_complex>&);
  // acos

  friend  inline  light3<lm_complex> atan(const light3<lm_complex>&);
  // atan

  friend  inline  light3<lm_complex> sinh(const light3<lm_complex>&);
  // sinh

  friend  inline  light3<lm_complex> cosh(const light3<lm_complex>&);
  // cosh

  friend  inline  light3<lm_complex> tanh(const light3<lm_complex>&);
  // tanh

  friend  inline  light3<lm_complex> asinh(const light3<lm_complex>&);
  // asinh

  friend  inline  light3<lm_complex> acosh(const light3<lm_complex>&);
  // acosh

  friend  inline  light3<lm_complex> atanh(const light3<lm_complex>&);
  // atanh
  //<ignore>
  // friend class light3<int>;
  friend class lightN<T>;
  friend class light33<T>;
  friend class lightN3<T>;
  friend class lightNN<T>;
  friend class lightNNN<T>;
  friend class lightNNNN<T>;
  
#ifndef COMPLEX_TOOLS
  friend  inline  lightN3<int> sign(const lightN3<int>&);
  friend  inline  lightN3<int> sign(const lightN3<double>&);
#else
  friend  inline  lightN3lm_complex sign(const lightN3lm_complex&);
#endif
  friend  inline  lightN3<int> ifloor(const lightN3<double>&);
  friend  inline  lightN3<int> iceil(const lightN3<double>&);
  friend  inline  lightN3<int> irint(const lightN3<double>&);

  friend inline lightN<T> light_join (const lightN<T> arr1, const lightN<T> arr2);

  //</ignore>

protected:
  T elem[3];
  // The values of the three elements.

  light3(const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. Used
  // for speed.

  int size1;
  // The size of the vector (number of elements).


};

//#include "light3.icc"


inline  light3<double> atanh(const light3<double>&);
inline  light3<double> atanh(const light3<double>&);

//*******************************
typedef light3<double> doubleVec3;
typedef light3<int> int3;
extern const doubleVec3 zeroVec3;
//*******************************

#undef IN_LIGHT3<T>_H

#endif
