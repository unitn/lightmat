// (c) MathCore Engineering
// This is a non-optimized variant of RWCString class normally supplied
// in RWTOOLS library.
// The only reason to use it is to keep common LIGHTMAT library within two projects -
// OMATH5 project ( this class is linked with code generated from ObjectMath compiler )
// and
// BEARS project. (This class is not used at all there, but LIGHTMAT is used there with RWTOOLS
// library)
//**************************************************************************************
// Note: Potentially RWCString is memory leak. We ignore this because it will be used for
// testing data output  and small string operations.
//***************************************************************************************
#ifndef cstring_h
#define cstring_h

#ifndef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE 1
#define _CRT_NONSTDC_NO_DEPRECATE 1
// This is needed for compiling without warning for functions
// which considered suspicious by Visual C++ 2005
// sprintf(), strncopy(), etc ...
#endif

#if defined(OLDHEADER) || defined(macintosh) || (__GNUC__ && (__GNUC__ < 4))

#include <iostream.h>
#include <string.h>
#else

#include <iostream>
#include <string.h>


//using namespace std;
// Danger ! this using namespace std causes conflicts
// regarding abs() at least in g++ 3 and later.

// Instead these are the only these symbols used from std:
using std::ostream;
using std::cout;
using std::cin;
using std::cerr;
using std::endl;
using std::ws;
#endif





class  RWCString  {
public:
  char * data;   // Potentially we use byte string, not 0-terminated character strings,
                 // But we always keep the 0-byte after the end. 
  int    length; // Length of byte string

  RWCString ()
    {
      data=new char[1];
      data[0]=0;
      length=0;
    }

 RWCString (const char * a)
     {data = new char [strlen(a)+1];
      length= (int)strlen(a);
      strcpy(data,a);};


 operator char*() {return data;} 
 // user-defined conversion

 void set(const unsigned char * a, const int len)
    {
      length=len;
      data=new char[length+1];
      memcpy( data, a,  len);
      data[len]='\0';      // Potentially 0-terminated.
    }

 RWCString & operator+=(const char * cs)
     { return append (cs);}

 RWCString & operator+=(const RWCString & cs)
     { return append (cs.data);}

  char * getdata() const
    { return data ; }
  int   getlength() const
    { return length ;}
 
  RWCString & append(const  char * cs) // 0-terminated append !
     {  char * nd = new char[strlen(data)+strlen(cs) +1 ];
         //strncpy(nd,data,strlen(data));
         strncpy(nd,data,strlen(data)+1);
         strcat(nd,cs);
         delete [] data;
         data=nd;
         length=(int)strlen(data);
         return *this;  
     }
 friend RWCString operator + (const RWCString & s1, const RWCString & s2);
 friend ostream & operator << (ostream & o, const RWCString s);
  };


#endif
