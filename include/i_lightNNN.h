//           -*- c++ -*-

#ifndef LIGHTNNN_I_H
#define LIGHTNNN_I_H
// <cd> lightNNNint
//
// .SS Functionality
//
// lightNNN is a template for classes that implement tensors of rank 3
// with any number of elements. E.g. a lightNNN s with 5x6x7 elements of
// type double can be instanciated with:
//
// <code>lightNNN&lt;double&gt; s(5,6,7);</code>
//
// The size of the tensor can change during execution. It changes its
// size to whatever is needed when it is assigned a new value with an
// assignment operator.
//
// When a lightNNN object is used as a input data in a calculation then
// its size must be a valid one in that context. E.g. one can't add a
// lightNNN object with 5x6x5 elements to one with 5x5x6 elements. If this
// happens then the application will dump core.
//
// .SS Author
//
// Anders Gertz
//

#include <assert.h>

#define IN_LIGHTNNNint_I_H

#define LIGHTNNN_SIZE1 5
#define LIGHTNNN_SIZE2 5
#define LIGHTNNN_SIZE3 5
#define LIGHTNNN_SIZE (LIGHTNNN_SIZE1*LIGHTNNN_SIZE2*LIGHTNNN_SIZE3)
// The default size.

 class lightN;
 class lightNN;
 class lightN33;
 class lightNNNN;


class lightNNNint {
public:

#ifdef IN_LIGHTNNNdouble_I_H
  friend class lightNNNint;
#else
  friend class lightNNNdouble;
#endif

#ifdef LIGHTMAT_TEMPLATES
  friend class lightNNNint;
#endif

  #include "i_lightNNN_auto.h"

  inline lightNNNint();
  // Default constructor.

  inline lightNNNint(const lightNNNint&);
  // Copy constructor.

  inline lightNNNint(const lightN33int&);
  // Conversion.

  inline lightNNNint(const lightmat_dummy_enum);
  // Intentionally does nothing

  inline lightNNNint(const int, const int, const int);
  // Construct a lightNNN of given size.

  inline lightNNNint(const int, const int, const int, const int *);
  // Construct a lightNNN of given size and initialize elements with values
  // from an array (in row major order).

  inline lightNNNint(const int, const int, const int, const int);
  // Construct a lightNNN of given size and initialize all elements
  // with a value (the last argument).

  inline ~lightNNNint();
  // Destructor.

#ifdef CONV_INT_2_DOUBLE
  operator lightNNNdouble();
  // Convert to double.
#endif

#ifdef CONV_DOUBLE_2_COMPLEX
  operator lightNNNlm_complex();
  // Convert to complex.
  
#endif

#ifdef CONV_INT_2_COMPLEX
  operator lightNNNlm_complex();
  // Convert to complex.
#endif

  lightNNNint& operator=(const lightNNNint&);
  // Assignment.

  lightNNNint& operator=(const lightN33int&);
  // Assignment, change size to Nx3x3.

  lightNNNint& operator=(const int);
  // Assign one value to all elements.

  int operator() (const int x, const int y, const int z) const {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3));
#ifdef ROWMAJOR
    return elem[((x-1)*size2+(y-1))*size3+z-1];
#else
    return elem[((z-1)*size2+(y-1))*size1+x-1];
#endif
  };
  // Get the value of one element.


  int& operator()(const int x, const int y, const int z) {
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) || (z<1) || (z>size3));
#ifdef ROWMAJOR
    return elem[((x-1)*size2+(y-1))*size3+z-1];
#else
    return elem[((z-1)*size2+(y-1))*size1+x-1];
#endif
  };
  // Get/Set the value of one element.

  lightNint operator()(const int, const int) const;
  // Get the value of a vector in the tensor.

  lightNNint operator()(const int) const;
  // Get the value of a matrix in the tensor.




  //
  // Set
  //

  inline lightNNNint& Set (const int i0, const int i1, const int val);
  inline lightNNNint& Set (const int i0, const int i1, const lightNint& arr);
  inline lightNNNint& Set (const int i0, const int i1, const light3int& arr);
  inline lightNNNint& Set (const int i0, const int i1, const light4int& arr);

  inline lightNNNint& Set (const int i0, const int val);
  inline lightNNNint& Set (const int i0, const lightNNint& arr);
  inline lightNNNint& Set (const int i0, const light33int& arr);
  inline lightNNNint& Set (const int i0, const light44int& arr);


 
  int operator==(const lightNNNint&) const;
  // Equality.

  int operator!=(const lightNNNint&) const;
  // Inequality.
 
  lightNNNint& operator+=(const int);
  // Add a value to all elements.

  lightNNNint& operator+=(const lightNNNint&);
  // Elementwise addition.

  lightNNNint& operator-=(const int);
  // Subtract a value from all elements.

  lightNNNint& operator-=(const lightNNNint&);
  // Elementwise subtraction.

  lightNNNint& operator*=(const int);
  // Mulitply all elements with a value.

  lightNNNint& operator/=(const int);
  // Divide all elements with a value.

  int Extract(const lightNint&) const;
  // Extract an element using given index. 

  inline lightNNNint& reshape(const int, const int, const int, const lightNint&);
  // Convert the lightN-vector to the given size and put the result in
  // this object. (*this)(a,b,c) will be set to s(a) in the lightNNNint
  // object. The value of the first argument must be the same as the
  // number of elements in the lightN-vector. The program may dump
  // core or behave strangely if the arguments are incorrect.

  inline lightNNNint& reshape(const int, const int, const int, const lightNNint&);
  // Convert the lightNN-matrix to the given size and put the result
  // in this object. (*this)(a,b,c) will be set to s(a,b) in the
  // lightNNN object. The values of the first two arguments must be
  // the same as the size of the lightNN-matrix. The program may dump
  // core or behave strangely if the arguments are incorrect.

  lightNNNint& SetShape(const int x=-1, const int y=-1, const int z=-1); 
  // Set new shape for the tensor.

  const  lightNNint& SetMatrix1(const int n,const lightNNint& x);
  // Assigns Tensor[n,_,_]=Matrix

  const  lightNint& SetVector12(const int n,const int m,const lightNint& x);
  // Assigns Tensor[n,m,_]=Vector 

  int dimension(const int x) const {
    if(x == 1)
      return size1;
    else if(x == 2)
      return size2;
    else if(x == 3)
      return size3;
    else
      return 1;
  };
  // Get size of some dimension.
 
  void Get(int *) const;
  // Get values of all elements and put them in an array (row major
  // order).

  void Set(const int *);
  // Set values of all elements from array (row major order).
 
  int * data() const;
  // Direct access to the stored data. Use the result with care.

  lightNNNint operator+() const;
  // Unary plus.

  lightNNNint operator-() const;
  // Unary minus.

  friend inline lightNNNint operator+(const lightNNNint&, const lightNNNint&);
  // Elementwise addition.

  friend inline lightNNNint operator+(const lightNNNint&, const int);
  // Addition to all elements.

  friend inline lightNNNint operator+(const int, const lightNNNint&);
  // Addition to all elements.

#ifdef IN_LIGHTNNNdouble_I_H
  friend inline lightNNNdouble operator+(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator+(const lightNNNint&, const double);
  friend inline lightNNNdouble operator+(const double, const lightNNNint&);
  friend inline lightNNNdouble operator+(const int, const lightNNNdouble&);
#endif

  #ifdef IN_LIGHTNNNlm_complex_I_H
friend inline lightNNNlm_complex operator+(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator+(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator+(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator+(const double e, const lightNNNlm_complex& s2);
#endif

  friend inline lightNNNint operator-(const lightNNNint&, const lightNNNint&);
  // Elementwise subtraction.

  friend inline lightNNNint operator-(const lightNNNint&, const int);
  // Subtraction from all elements.

  friend inline lightNNNint operator-(const int, const lightNNNint&);
  // Subtraction to all elements.

#ifdef IN_LIGHTNNNdouble_I_H
  friend inline lightNNNdouble operator-(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator-(const lightNNNint&, const double);
  friend inline lightNNNdouble operator-(const double, const lightNNNint&);
  friend inline lightNNNdouble operator-(const int, const lightNNNdouble&);
#endif

  #ifdef IN_LIGHTNNNlm_complex_I_H
friend inline lightNNNlm_complex operator-(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator-(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator-(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator-(const double e, const lightNNNlm_complex& s2);

#endif

  friend inline lightNNNint operator*(const lightNNNint&, const int);
  // Multiply all elements.

  friend inline lightNNNint operator*(const int, const lightNNNint&);
  // Multiply all elements.

#ifdef IN_LIGHTNNNdouble_I_H
  friend inline lightNNNdouble operator*(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator*(const lightNNNint&, const double);
  friend inline lightNNNdouble operator*(const double, const lightNNNint&);
  friend inline lightNNNdouble operator*(const int, const lightNNNdouble&);
#endif

#ifdef IN_LIGHTNNNlm_complex_I_H
friend inline lightNNNlm_complex operator*(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator*(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator*(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator*(const double e, const lightNNNlm_complex& s2);
#endif

  friend inline lightNNNint operator*(const lightNNNint&, const lightNNint&);
  // Inner product.

  friend inline lightNNNint operator*(const lightNNint&, const lightNNNint&);
  // Inner product.

  friend inline lightNNNint operator*(const lightNNNNint&, const lightNint&);
  // Inner product.

  friend inline lightNNNint operator*(const lightNint&,	const lightNNNNint&);
  // Inner product.

  friend inline lightNNNint operator*(const lightNNNint&, const light33int&);
  // Inner product.

  friend inline lightNNNint operator*(const light33int&, const lightNNNint&);
  // Inner product.

  friend inline lightNNNint operator*(const lightNNNNint&, const light3int&);
  // Inner product.

  friend inline lightNNNint operator*(const light3int&, const lightNNNNint&);
  // Inner product.

  friend inline lightNNNint operator*(const lightNNNint&, const light44int&);
  // Inner product.

  friend inline lightNNNint operator*(const light44int&, const lightNNNint&);
  // Inner product.

  friend inline lightNNNint operator*(const lightNNNNint&, const light4int&);
  // Inner product.

  friend inline lightNNNint operator*(const light4int&, const lightNNNNint&);
  // Inner product.

  friend inline lightNNNint operator/(const lightNNNint&, const int);
  // Divide all elements.

  friend inline lightNNNint operator/(const int, const lightNNNint&);
  // Divide all elements.

#ifdef IN_LIGHTNNNdouble_I_H
  friend inline lightNNNdouble operator/(const lightNNNdouble&, const int);
  friend inline lightNNNdouble operator/(const lightNNNint&, const double);
  friend inline lightNNNdouble operator/(const double, const lightNNNint&);
  friend inline lightNNNdouble operator/(const int, const lightNNNdouble&);
#endif

#ifdef IN_LIGHTNNNlm_complex_I_H
friend inline lightNNNlm_complex operator/(const lightNNNlm_complex& s1, const double e);
friend inline lightNNNlm_complex operator/(const lightNNNdouble& s1, const lm_complex e);
friend inline lightNNNlm_complex operator/(const lm_complex e, const lightNNNdouble& s2);
friend inline lightNNNlm_complex operator/(const double e, const lightNNNlm_complex& s2);
#endif

//#ifdef IN_LIGHTNNNlm_complex_I_H
friend inline lightNNNdouble arg(const lightNNNlm_complex& a);
friend inline lightNNNdouble re(const lightNNNlm_complex& a);
friend inline lightNNNdouble im(const lightNNNlm_complex& a);
friend inline lightNNNlm_complex conjugate(const lightNNNlm_complex& a);
//#endif

  friend inline lightNNNint pow(const lightNNNint&, const lightNNNint&);
  // Raise to the power of-function, elementwise.

  friend inline lightNNNint pow(const lightNNNint&, const int);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNint pow(const int, const lightNNNint&);
  // Raise to the power of-function, for all elements.

  friend inline lightNNNint ElemProduct(const lightNNNint&, const lightNNNint&);
  // Elementwise multiplication.

  friend inline lightNNNint ElemQuotient(const lightNNNint&, const lightNNNint&);
  // Elementwise division.

  friend inline lightNNNint Apply(const lightNNNint&, int f(int));
  // Apply the function elementwise all elements.

  friend inline lightNNNint Apply(const lightNNNint&, const lightNNNint&, int f(int, int));
  // Apply the function elementwise on all elements in the two tensors.

  friend inline lightNNNint Transpose(const lightNNNint&);
  // Transpose.

#ifdef IN_LIGHTNNNlm_complex_I_H
#else
  friend inline lightNNNint abs(const lightNNNint&);
#ifdef IN_LIGHTNNNdouble_I_H
  friend inline lightNNNdouble abs(const lightNNNlm_complex&);
#endif 
#endif


#ifndef COMPLEX_TOOLS
  // sign
  friend inline lightNNNint sign(const lightNNNint&);
  // sign
  friend inline lightNNNint sign(const lightNNNdouble&);
  // abs
#else
  friend inline lightNNNlm_complex sign(const lightNNNlm_complex&);
#endif

 /// Added 2/2/98 

  friend inline lightNNNint  FractionalPart(const lightNNNint&);
  //  FractionalPart

  friend inline lightNNNint  IntegerPart(const lightNNNint&);
  //  IntegerPart

  //friend inline lightNNNint  IntegerPart(const lightNNNdouble&);
  //  IntegerPart

  friend inline lightNNNint Mod (const  lightNNNint&,const  lightNNNint&);
  // Mod
 
  friend inline lightNNNint Mod (const  lightNNNint&,const int);
  // Mod

  friend inline lightNNNint Mod (const int,const  lightNNNint&);
  // Mod

  friend inline int LightMax (const lightNNNint& );
  // Max

  friend inline int LightMin (const lightNNNint& );
  // Min

  friend inline int findLightMax (const  int *,const  int );
  // Find Max
 
  friend inline int findLightMin (const  int *,const  int );
  // Find Min
 
  /// End Added 



  friend inline lightNNNint ifloor(const lightNNNdouble&);
  // ifloor

  friend inline lightNNNint iceil(const lightNNNdouble&);
  // iceil

  friend inline lightNNNint irint(const lightNNNdouble&);
  // irint

  friend inline lightNNNdouble sqrt(const lightNNNdouble&);
  // sqrt

  friend inline lightNNNdouble exp(const lightNNNdouble&);
  // exp

  friend inline lightNNNdouble log(const lightNNNdouble&);
  // log

  friend inline lightNNNdouble sin(const lightNNNdouble&);
  // sin

  friend inline lightNNNdouble cos(const lightNNNdouble&);
  // cos

  friend inline lightNNNdouble tan(const lightNNNdouble&);
  // tan

  friend inline lightNNNdouble asin(const lightNNNdouble&);
  // asin

  friend inline lightNNNdouble acos(const lightNNNdouble&);
  // acos

  friend inline lightNNNdouble atan(const lightNNNdouble&);
  // atan

  friend inline lightNNNdouble sinh(const lightNNNdouble&);
  // sinh

  friend inline lightNNNdouble cosh(const lightNNNdouble&);
  // cosh

  friend inline lightNNNdouble tanh(const lightNNNdouble&);
  // tanh

  friend inline lightNNNdouble asinh(const lightNNNdouble&);
  // asinh

  friend inline lightNNNdouble acosh(const lightNNNdouble&);
  // acosh

  friend inline lightNNNdouble atanh(const lightNNNdouble&);
  // atanh

   friend inline lightNNNlm_complex ifloor(const lightNNNlm_complex&);
  // ifloor

  friend inline lightNNNlm_complex iceil(const lightNNNlm_complex&);
  // iceil

  friend inline lightNNNlm_complex irint(const lightNNNlm_complex&);
  // irint

  friend inline lightNNNlm_complex sqrt(const lightNNNlm_complex&);
  // sqrt

  friend inline lightNNNlm_complex exp(const lightNNNlm_complex&);
  // exp

  friend inline lightNNNlm_complex log(const lightNNNlm_complex&);
  // log

  friend inline lightNNNlm_complex sin(const lightNNNlm_complex&);
  // sin

  friend inline lightNNNlm_complex cos(const lightNNNlm_complex&);
  // cos

  friend inline lightNNNlm_complex tan(const lightNNNlm_complex&);
  // tan

  friend inline lightNNNlm_complex asin(const lightNNNlm_complex&);
  // asin

  friend inline lightNNNlm_complex acos(const lightNNNlm_complex&);
  // acos

  friend inline lightNNNlm_complex atan(const lightNNNlm_complex&);
  // atan

  friend inline lightNNNlm_complex sinh(const lightNNNlm_complex&);
  // sinh

  friend inline lightNNNlm_complex cosh(const lightNNNlm_complex&);
  // cosh

  friend inline lightNNNlm_complex tanh(const lightNNNlm_complex&);
  // tanh

  friend inline lightNNNlm_complex asinh(const lightNNNlm_complex&);
  // asinh

  friend inline lightNNNlm_complex acosh(const lightNNNlm_complex&);
  // acosh

  friend inline lightNNNlm_complex atanh(const lightNNNlm_complex&);
  // atanh

#ifdef IN_LIGHTNNNint_I_H
  friend inline lightNNdouble light_mean     ( const  lightNNNint& );
  friend inline lightNNdouble light_standard_deviation( const  lightNNNint& );
  friend inline lightNNdouble light_variance ( const  lightNNNint& );
  friend inline lightNNdouble light_median   ( const  lightNNNint& );
#else
  friend inline lightNNint light_mean     ( const  lightNNNint& );
  friend inline lightNNint light_variance ( const  lightNNNint& );
  friend inline lightNNint light_standard_deviation( const  lightNNNint& );
  friend inline lightNNint light_median   ( const  lightNNNint& );
#endif
  
  friend inline lightNNint light_total    ( const  lightNNNint& );
  
  friend inline lightNNNint light_sort (const lightNNNint arr1);
  
  friend inline lightNint light_flatten (const lightNNNint s);
  friend inline lightNint light_flatten (const lightNNNint s, int level);
  friend inline lightNNNint light_flatten0 (const lightNNNint s);
  friend inline lightNNint light_flatten1 (const lightNNNint s);

  //<ignore>
#ifdef IN_LIGHTNNNlm_complex_I_H
  friend class lightNNNint;
#endif

  friend class lightNint;
  friend class lightNNint;
  friend class lightN33int;
  friend class lightNNNNint;

  friend inline lightNNNint light_join (const lightNNNint, const lightNNNint);
  friend inline lightNNNint light_drop (const lightNNNint arr1, const R r);

  //</ignore>

  //--
  // This should be protected, but until the friendship between lightNint
  // and lightNNNdouble cannot be established this will do.
  // If not public problems will occur with the definition of
  //lightNNNdouble::lightNNNint(const lightNNNint& s1, const double e, const lightmat_plus_enum)

  int *elem;
  // A pointer to where the elements are stored (column major order).

  int size1;
  // Size of the first index.

  int size2;
  // Size of the second index.

  int size3;
  // Size of the third index.

protected:
  int sarea[LIGHTNNN_SIZE];
  // The values are stored here (column major order) if they fit into
  // LIGHTNNN_SIZE else a memory area is allocated.

  int alloc_size;
  // The size of the allocated area (number of elements).

  void init(const int);
  // Used to initialize vector with a given size. Allocates memory if needed.

  inline lightNNNint(const int, const int, const int, const lightmat_dont_zero_enum);
  // Constructor that leave the values of elements as undefined. The first
  // three arguments is the size of the created tensor. Used for speed.
  //
  //
  //
  // The following constructors are used internally for doing the
  // calculation as indicated by the name of the enum. The value of the
  // enum argument is ignored by the constructors, it is only used by
  // the compiler to tell the constructors apart.
  lightNNNint(const lightNNNint&, const lightNNNint&, const lightmat_plus_enum);
  lightNNNint(const lightNNNint&, const int, const lightmat_plus_enum);

#ifdef IN_LIGHTNNNdouble_I_H
  lightNNNint(const lightNNNdouble&, const int, const lightmat_plus_enum);
  lightNNNint(const lightNNNint&, const double, const lightmat_plus_enum);
#endif

  lightNNNint(const lightNNNint&, const lightNNNint&, const lightmat_minus_enum);
  lightNNNint(const int, const lightNNNint&, const lightmat_minus_enum);

#ifdef IN_LIGHTNNNdouble_I_H
  lightNNNint(const int, const lightNNNdouble&, const lightmat_minus_enum);
  lightNNNint(const double, const lightNNNint&, const lightmat_minus_enum);
#endif

  lightNNNint(const lightNNNint&, const int, const lightmat_mult_enum);

#ifdef IN_LIGHTNNNdouble_I_H
  lightNNNint(const lightNNNdouble&, const int, const lightmat_mult_enum);
  lightNNNint(const lightNNNint&, const double, const lightmat_mult_enum);
#endif

  lightNNNint(const lightNNNint&, const lightNNint&, const lightmat_mult_enum);
  lightNNNint(const lightNNint&, const lightNNNint&, const lightmat_mult_enum);
  lightNNNint(const lightNNNNint&, const lightNint&, const lightmat_mult_enum);
  lightNNNint(const lightNint&, const lightNNNNint&, const lightmat_mult_enum);
  lightNNNint(const lightNNNint&, const light33int&, const lightmat_mult_enum);
  lightNNNint(const light33int&, const lightNNNint&, const lightmat_mult_enum);
  lightNNNint(const lightNNNNint&, const light3int&, const lightmat_mult_enum);
  lightNNNint(const light3int&, const lightNNNNint&, const lightmat_mult_enum);
  lightNNNint(const lightNNNint&, const light44int&, const lightmat_mult_enum);
  lightNNNint(const light44int&, const lightNNNint&, const lightmat_mult_enum);
  lightNNNint(const lightNNNNint&, const light4int&, const lightmat_mult_enum);
  lightNNNint(const light4int&, const lightNNNNint&, const lightmat_mult_enum);
  lightNNNint(const int, const lightNNNint&, const lightmat_div_enum);

#ifdef IN_LIGHTNNNdouble_I_H
  lightNNNint(const int, const lightNNNdouble&, const lightmat_div_enum);
  lightNNNint(const double, const lightNNNint&, const lightmat_div_enum);
#endif

  lightNNNint(const lightNNNint&, const lightNNNint&, const lightmat_pow_enum);
  lightNNNint(const lightNNNint&, const int, const lightmat_pow_enum);
  lightNNNint(const int, const lightNNNint&, const lightmat_pow_enum);

#ifdef IN_LIGHTNNNlm_complex_I_H
 #else
  #ifdef IN_LIGHTNNNdouble_I_H
   lightNNNint(const lightNNNint&, const lightmat_abs_enum);
   lightNNNint(const lightNNNlm_complex&, const lightmat_abs_enum);
  #else
  lightNNNint(const lightNNNint&, const lightmat_abs_enum);
  #endif
#endif


  lightNNNint(const lightNNNint&, const lightNNNint&, const lightmat_eprod_enum);
  lightNNNint(const lightNNNint&, const lightNNNint&, const lightmat_equot_enum);
  lightNNNint(const lightNNNint&, int f(int), const lightmat_apply_enum);
  lightNNNint(const lightNNNint&, const lightNNNint&, int f(int, int), const lightmat_apply_enum);
  lightNNNint(const lightNNNint&, const lightmat_trans_enum);
};

typedef lightNNNdouble doubleNNN;
typedef lightNNNint intNNN;
typedef lightNNNlm_complex lm_complexNNN;

#undef IN_LIGHTNNNint_I_H

#endif
