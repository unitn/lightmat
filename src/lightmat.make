# File: lightmat.make
# Created: 1998-07-15 20:08
# Creator: Peter Johansson
# Comment: This is a make file for compiling the LightMat library
#          on a Power Macintosh.

# --------------------------------------------------------------------
#  Compiler and linker
# --------------------------------------------------------------------

CC = MWCPPC
CCOPT = -w off # -w nounusedvar,nounusedarg,nopossible # -d NDEBUG

LINK = MWLINKPPC
LINKOPT =

# --------------------------------------------------------------------
#  LightMat
# --------------------------------------------------------------------

LM = ::

# --------------------------------------------------------------------
#  Include paths
# --------------------------------------------------------------------

CodeWarrior=Macintosh HD:CodeWarrior Pro 4:MetroWerks CodeWarrior:

#STDINC = -i- �
#	-i "{CodeWarrior}Metrowerks Standard Library:MSL C:MSL Common:Public Includes:" �
#	-i "{CodeWarrior}Metrowerks Standard Library:MSL C++:Include:Language Support:" �
#	-i "{CodeWarrior}Metrowerks Standard Library:MSL C++:Include:Library Support:"

STDINC = -i- �
	-i "{CodeWarrior}Metrowerks Standard Library:MSL C:MSL Mac:Public Includes:" �
	-i "{CodeWarrior}Metrowerks Standard Library:MSL C:MSL Common:Public Includes:" �
	-i "{CodeWarrior}Metrowerks Standard Library:MSL C:MSL Common:Private Includes:" �
	-i "{CodeWarrior}Metrowerks Standard Library:MSL C++:Include:" #�
#	-i "{CodeWarrior}Metrowerks Standard Library:MSL C++:Include:"

INCDIR = {LM}include:
INC = {STDINC} -i {INCDIR} -i {LM}icc:


# --------------------------------------------------------------------
#  Libraries
# --------------------------------------------------------------------

# These libraries makes the test programs stand-alone applications
# that run outside the MPW shell.

CWLIBDIR = {CodeWarrior}MacOS Support:Libraries:
MSLDIR = {CodeWarrior}Metrowerks Standard Library:
MPWLIBDIR = {MPW}:Interfaces&Libraries:Libraries:

STDLIBS = �
	"{MSLDIR}MSL C:MSL C.PPC.Lib" �
	"{MSLDIR}MSL C++:MSL C++.PPC.Lib" �
	"{CWLIBDIR}MacOS Common:InterfaceLib" �
	"{CWLIBDIR}MacOS Common:MathLib" �
	"{CWLIBDIR}Runtime:Runtime PPC:MSL RuntimePPC.Lib" �
	"{CWLIBDIR}MacOS PPC:PPCToolLibs.o"

# --------------------------------------------------------------------
#  Compiling and linking
# --------------------------------------------------------------------

# Fix the dot problem

fixdot � $OutOfDate
	Set Exit 0
	Open -t "::include:light_icc.h"
	Find �
	Replace -c � /"../icc/"/ "::icc:"
	Close -y "::include:light_icc.h"
	Set Exit 1
	
# Object files

OBJDIR = ::obj:
AOBJDIR = {OBJDIR}{CC}:
LMOBJS = {AOBJDIR}not_inlined.o {AOBJDIR}cstring.o {AOBJDIR}extwin.o
LMOBJS_I =  {AOBJDIR}cstring.o {AOBJDIR}extwin.o

# Source files

SRC = �
				::include:classes.h �
                ::include:d_light3.h �
                ::include:d_light33.h �
                ::include:d_lightN33.h �
                ::include:d_lightN3.h �
                ::include:d_light4.h �
                ::include:d_light44.h �
                ::include:d_lightN.h �
                ::include:d_lightN3.h �
                ::include:d_lightNN.h �
                ::include:d_lightNNN.h �
                ::include:d_lightNNNN.h �
                ::include:d_light_output.h �
                ::include:d_light_calc.h �
                ::include:d_light_basic.h �
                ::include:i_light3.h �
                ::include:i_light33.h �
                ::include:i_lightN33.h �
                ::include:i_lightN3.h �
                ::include:i_light4.h �
                ::include:i_light44.h �
                ::include:rw:cstring.h �
                ::include:i_lightN.h �
                ::include:i_lightN3.h �
                ::include:i_lightNN.h �
                ::include:i_lightNNN.h �
                ::include:i_lightNNNN.h �
                ::include:i_light_output.h �
                ::include:i_light_calc.h �
                ::include:i_light_basic.h �
                ::include:light3.h �
                ::include:light33.h �
                ::include:lightN33.h �
                ::include:lightN3.h �
                ::include:light4.h �
                ::include:light44.h �
                ::include:lightN.h �
                ::include:lightN3.h �
                ::include:lightNN.h �
                ::include:lightNNN.h �
                ::include:lightNNNN.h �
                ::include:light_output.h �
                ::include:light_calc.h �
                ::include:light_basic.h �
                ::include:lightmat.h �
                ::include:lightmat_id.h �
                ::include:light_icc.h �
                ::include:mathinter.h �
                ::include:mathdefs.h �
                ::include:tostr.h �
                ::include:transf.h

AUTO = �
				::include:light3_auto.h �
                ::include:light33_auto.h �
                ::include:light4_auto.h �
                ::include:light44_auto.h �
                ::include:lightN_auto.h �
                ::include:lightNN_auto.h �
                ::include:lightNNN_auto.h �
                ::include:lightNNNN_auto.h


lightmat � normal

normal � fixdot objdir {LMOBJS}

all � normal lmtest-run

{AOBJDIR}not_inlined.o � not_inlined.cpp {INCDIR}lightmat.h {SRC} {AUTO}
    # Assume that a "generate" has already been made
	{CC} {CCOPT} {INCEXTRA} not_inlined.cpp {INC} -o {AOBJDIR}not_inlined.o

{AOBJDIR}cstring.o � {INCDIR}rw:cstring.cpp {INCDIR}rw:cstring.h
	{CC} {CCOPT} {INCEXTRA} {INCDIR}rw:cstring.cpp {INC} -o {AOBJDIR}cstring.o

{AOBJDIR}extwin.o � extwin.cpp {INCDIR}lightmat.h {SRC}
	{CC} {CCOPT} {INCEXTRA} extwin.cpp {INC} -o {AOBJDIR}extwin.o

# Make sure that the object directory exists

objdir �
	if `exists {OBJDIR}` != {OBJDIR}
	  newfolder {OBJDIR}
	end
	if `exists {AOBJDIR}` != {AOBJDIR}
	  newfolder {AOBJDIR}
	end

# --------------------------------------------------------------------
#  Testing
# --------------------------------------------------------------------

# This doesn't seem to be working. The compilation of test.cpp
# generates an 'internal compiler error' for some unknown reason.

lmtest-run � lmtest2_i lmtest2 lmtest lmtest_i
	:lmtest > Dev:Null
	:lmtest2 > Dev:Null
	:lmtest_i > Dev:Null
	:lmtest2_i > Dev:Null

lmtest � {OBJDIR}test.o {LMOBJS}
	MWLINKPPC -o {Targ} {OBJDIR}test.o {LMOBJS} {STDLIBS} # -t 'MPST' -c 'MPS '

lmtest2 � {OBJDIR}test2.o {LMOBJS}
	MWLINKPPC -o {Targ} {OBJDIR}test2.o {LMOBJS} {STDLIBS} # -t 'MPST' -c 'MPS '

lmtest_i � {OBJDIR}test_i.o {LMOBJS_I}
	MWLINKPPC -o {Targ} {OBJDIR}test_i.o {LMOBJS} {STDLIBS} # -t 'MPST' -c 'MPS '

lmtest2_i � {OBJDIR}test2_i.o {LMOBJS_I}
	MWLINKPPC -o {Targ} {OBJDIR}test2_i.o {LMOBJS} {STDLIBS} # -t 'MPST' -c 'MPS '
	
{OBJDIR}test.o �  test.cpp {INCDIR}lightmat.h  {SRC}
	{CC} {OPT} {INCEXTRA} -d NOT_INLINED test.cpp {INC} -o {OBJDIR}test.o

{OBJDIR}test2.o �  test2.cpp {INCDIR}lightmat.h {SRC}
	{CC} {OPT} {INCEXTRA} -d NOT_INLINED test2.cpp {INC} -o {OBJDIR}test2.o

{OBJDIR}test_i.o �  test.cpp {INCDIR}lightmat.h  {SRC}
	{CCBIG} {INCEXTRA} test.cpp {INC} -o {OBJDIR}test_i.o

{OBJDIR}test2_i.o �  test2.cpp {INCDIR}lightmat.h  {SRC}
	{CC} {OPT} {INCEXTRA} test2.cpp {INC} -o {OBJDIR}test2_i.o

# --------------------------------------------------------------------
#  Cleaning up
# --------------------------------------------------------------------

clean �
	Delete {OBJDIR}�.o {AOBJDIR}�.o lmtest lmtest2 lmtest_i lmtest2_i Templates.DB
	
