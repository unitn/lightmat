#include "tostr.h"
#include <stdio.h>
#include "mathdefs.h"
#include <math.h>
#include <iostream.h>


RWCString ToStr(const int val)
{
    // Convert to string without leading or trailing spaces.
    char buf[100];
    sprintf(buf,"%d", val);
    
    return (RWCString(buf));

}

RWCString ToStr(const double val)
{
    char buf[100];
    if ((val>999)||(val<-999)) {
        // Convert to string without leading or trailing spaces.
        // Also elliminate trailing zeros.
        sprintf(buf,"%1.15e", val);
        RWCString ValStr = RWCString(buf);
        size_t epos = 17; // ValStr.index("e",17);
        size_t len = ValStr.length();
        RWCString man = ValStr(0,epos-1);
        return (man.strip(RWCString::trailing, '0') + ValStr(epos,len-epos));
    } else {
        // Convert to string without leading or trailing spaces.
        // Also elliminate trailing zeros.
        sprintf(buf,"%1.15g", val);
        return (RWCString(buf));
        
    }

}

RWCString ToStrS(const int val)
{
    // Convert to string without leading or trailing spaces
    // except space for plus sign.
    char buf[100];
    sprintf(buf,"% d", val);
    
    return (RWCString(buf));

}

RWCString ToStrS(const double val)
{
    char buf[100];
    if ((val>999)||(val<-999)) {
        // Convert to string without leading or trailing spaces
        // except space for plus sign.
        // Also elliminate trailing zeros.
        sprintf(buf,"% 1.15e", val);
        RWCString ValStr = RWCString(buf);
        size_t epos = 18; // ValStr.index("e",18);
        size_t len = ValStr.length();
        RWCString man = ValStr(0,epos-1);
        return (man.strip(RWCString::trailing, '0') + ValStr(epos,len-epos));
    } else {
        // Convert to string without leading or trailing spaces
        // except space for plus sign.
        // Also elliminate trailing zeros.
        sprintf(buf,"% 1.15g", val);
        return (RWCString(buf));
    }

}



RWCString ToStrSA(const double val)
{
    double val2 = fabs(val/pi);
    RWCString ppp;
    if (Sign(val)==1) {
        ppp =" pi";
    } else {
        ppp ="-pi";
    }

    if (val2<1.0) {

        if (fabs(val2 - 1.0/2) < 1.0e-16) {
            return (ppp + "/2");
        }

        if (fabs(val2 - 1.0/3)  < 1.0e-16) {
            return (ppp + "/3");
        }

        if (fabs(val2 - 1.0/4)  < 1.0e-16) {
            return (ppp + "/4");
        }

        return (ppp + "*" + ToStr(val2));

    } else {
        return (ppp + "*" + ToStr(val2));

    }


}

RWCString ToStrA(const double val)
{
    double val2 = fabs(val/pi);
    RWCString ppp;
    if (Sign(val)==1) {
        ppp ="pi";
    } else {
        ppp ="-pi";
    }

    if (val2<1.0) {

        if (fabs(val2 - 1.0/2) < 1.0e-16) {
            return (ppp + "/2");
        }

        if (fabs(val2 - 1.0/3)  < 1.0e-16) {
            return (ppp + "/3");
        }

        if (fabs(val2 - 1.0/4)  < 1.0e-16) {
            return (ppp + "/4");
        }

        return (ppp + "*" + ToStr(val2));

    } else {
        return (ppp + "*" + ToStr(val2));

    }


}

