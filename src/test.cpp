
#define  LM_ALL
#include "lightmat.h"
#include <assert.h>
#include <math.h>

#ifdef __SUNPRO_CC
// Sun CC wants this wrapper
inline double sin_wrap(double x);
#endif

int Global_Tequal1DArrayOfComplex ( const lm_complexN &a, const lm_complexN &b)
{
    const lm_complex & s=a(1);
    double d=4;
    lm_complex u=s+d;
    lm_complex v=d+s;
    
    return a==b;
};


lightNNint get_intNN (int y, int x);
lightNNNint get_intNNN (int x, int y, int z);
lightNNNNint get_intNNNN (int x, int y, int z, int w);

light3double l3d(const double a,const double  b,const double c) {
  light3double res(a,b,c);
  return res;
}

lightNdouble l2d(const double a,const double  b) {
  lightNdouble res(2);
  res(1)=a;
  res(2)=b;
  return res;
}

light4double l4d(const double a,const double  b,const double c, const double d) {
  light4double res(a,b,c,d);
  return res;
}

lightNdouble l4Nd(const double a,const double  b,const double c, const double d) {
  light4double res1(a,b,c,d);
  lightNdouble res(res1);
  return res;
}

lightNint l4Ni(const int a,const int  b,const int c, const int d) {
  light4int res1(a,b,c,d);
  lightNint res(res1);
  return res;
}

lightNdouble l3Nd(const double a,const double  b,const double c) {
  light3double res1(a,b,c);
  lightNdouble res(res1);
  return res;
}

lightNNdouble l22NNd(const  double a,const double  b,const double c, const double d) {
  lightNNdouble res(2,2);
  res(1,1)=a;
  res(1,2)=b;
  res(2,1)=c;
  res(2,2)=d;
  return res;
}

int boo( int  a, int  b){
    return 0.5; 
}

int testConversion(){
	lightNdouble a=l2d(5,6);
        int s=5.5;
        boo(6.3,5.6);
        Max(5.7 ,5.5);
        a(1.2)=a(1.3);
	lightNdouble b=l2d(5,6);
	int res= Global_Tequal1DArrayOfComplex(a,b);
    assert(res);
	
	return res;
}


const int sxx=2;

double next (double a) {
  return a+1;
}

double sum (double a, double b) {
  return a+b;
}

#include "genres.cc"

void f1()
  {
    assert(abs((double)0.5)==0.5);
    assert(abs(-0.5)==0.5);
    light3double a0(1);
    assert (a0 == l3d(1,1,1)); 
    light3double a1;
    //  cout << "Initial (non-initialized)" << a1 << endl;
    light3double a2 (3,4,5);

    assert (a2 == l3d(3,4,5)); 

    light3double a3 (a2);
    assert (a3 == l3d(3,4,5)); 

    double data[3];
    data[0]=6; data[1]=7; data[2]=8; 
    light3double a4 (data);
    assert (a4 == l3d(6,7,8)); 

    light3double a5 (5);
    assert (a5 == l3d(5,5,5)); 

    a4=a4;
    assert (a4 == l3d(6,7,8)); 

    a3=a4;
    assert (a3 == l3d(6,7,8)); 

    lightNdouble ax1(4); 
    lightNdouble ax2(3); 
    ax1(1)=11; ax1(2)=12; ax1(3)=13; ax1(4)=14;
    ax2(1)=51; ax2(2)=52; ax2(3)=53; 
    // a3=ax1; // should fail
    a3=ax2; // OK
    assert (a3 == l3d(51,52,53)); 

    assert(a3(1)==51);
    assert(a3(2)==52);
    assert(a3(3)==53);

    a3(1)=61;
    a3(2)=62;
    a3(3)=63;
    assert (a3 == l3d(61,62,63)); 
    assert( ! ( a3 != l3d(61,62,63))); 
    a3(2)=77;
    assert (a3 != l3d(61,62,63)); 
    assert( ! (a3 == l3d(61,62,63))); 

    a2=l3d(2,3,4);
    a3=a2+=6;
    assert ( a2 ==  l3d(8,9,10));
    assert(a3==a2);
    a3=a2+=a2;
    assert ( a2 ==  l3d(16,18,20));
    assert(a3==a2);
    a3=a2-=6;
    assert ( a2 ==  l3d(10,12,14));assert(a3==a2);
    a3=a2-=a2;
    assert ( a2 ==  l3d(0,0,0));assert(a3==a2);

    a2=l3d(2,3,4);
    a3=a2*=2;
    assert ( a2 ==  l3d(4,6,8)); assert(a3==a2);
    a3=a2/=2;
    assert ( a2 ==  l3d(2,3,4)); assert(a3==a2);

    assert ( abs(a2.length() - sqrt(29))< 1e-6 );
    assert ( a2.dimension(1) == 3);
    assert ( a2.dimension(2) == 1);

    a2.normalize();
    cout << a2 << endl;
    cout << l3d(2/sqrt(29.0),3/sqrt(29.0),4/sqrt(29.0)) << endl;
    if (a2 !=  l3d(2/sqrt(29.0),3/sqrt(29.0),4/sqrt(29.0)))
      cerr << "WARNING: Equal test failed in f1: _LINE_" << endl;
    a2=l3d(3,4,0);
    a2.normalize();
    a3=a2;
    a3-=l3d(0.6,0.8,0);
    assert(a3.length()< 1e-14);



    a2=l3d( 0.6,0.8,0);
    a2.Get(data);
    assert( data[0]==0.6); assert( data[1]==0.8); assert( data[2]==0  );
    data[2]=0.3;
    a2.Set(data);
    assert( a2==  l3d(0.6,0.8,0.3));

    a2.Set(5,6,7);
    assert ( a2 ==  l3d(5,6,7));
    double p5,p6,p7;
    a2.Get(p5,p6,p7);
    assert(p5==5);
    assert(p6==6);
    assert(p7==7);
 


    a3=5;
    assert (a3 == l3d(5,5,5)); 

    assert (+a3 == a3);
    assert (-a3 == l3d (-5,-5,-5));
  
    a3 = l3d (2,3,4);
    assert (a3+a3 == l3d(4,6,8));
    assert (a3+ 3. == l3d(5,6,7));
    assert (3.+ a3 == l3d(5,6,7));
    a4 = l3d (1,2,1);
    assert (a3-a4 == l3d(1,1,3));
    assert (a3-4. == l3d(-2,-1,0));
    assert (4.-a3 == l3d(2,1,0));
    assert (a3*a4 == 2+6+4);    // Dot product, .
    assert (Dot(a3,a4) == a3*a4);
    assert (a3*2. == l3d(4,6,8));
    assert (2.*a3 == l3d(4,6,8));
 
    light33double m1(11,12,13,21,22,23,31,32,33);
    // m1 = { { 11,12,13 },{ 21,22,23},{31,32,33 } }
    // a3 = { 2,3,4}
    // a3.m1 = {209, 218, 227}

    light33double m2=m1;
    cout <<  m1 << endl;
    cout << a3 << endl;
    cout << a3*m1 << endl;
      
    assert (a3*m1==l3d(209, 218, 227));
    assert (Dot(a3,m1) == a3*m1);
    assert (a3/2. == l3d(1,1.5,2));
    assert (12./a3 == l3d(6,4,3));
    assert (pow (a3,a3) == l3d (2*2,3*3*3, 4*4*4*4));
    assert (pow (a3,3.) == l3d (8,27,64));
    assert (pow (2.,a3) == l3d (4,8,16));
    assert (abs(l3d(-5,3,0))==l3d(5,3,0));
    assert (ElemProduct(a3,a3)==l3d(4,9,16)); // Times or * in Mathematica
    assert (ElemQuotient(a3,a3)==l3d(1,1,1));

    assert (Apply(a3,next) == l3d (3,4,5));

  
    a3=l3d(3,4,5);
    // a3= {3,4,5}

    assert (Apply(a3,sin_wrap_double) == l3d (sin(3),sin(4),sin(5)));
 
    assert (Apply(a3,a3,sum) == l3d(6,8,10));
  
    // <<Calculus`VectorAnalysis`
    // CrossProduct [a3,a3+1]
    assert (Cross(a3,a3+1.) == l3d(-1,2,-1)); 

 
    light33double m3(12 , 15  ,18,
                     16,  20,  24,
                     20 , 25,  30);
    // MatrixForm[Outer[Times,a3,a3+1]]
    assert (OuterProduct(a3,a3+1.) == m3);
 

    light3double a;
    a(1)=1;   
    light3int  b;
    b(1)=1;
    b(2)=2;
    b(3)=3;
    cout << pow(b,b)(1)  <<" " <<  pow(b,b)(2)  <<" " << pow(b,b)(3) << endl   ;

    cout << pow(b,b) << endl;

    cout << atan2 (b, b) << endl;
    cout << atan2 (b, 17.5) << endl;
    cout << atan2 (17.5, b) << endl;
 
    lightNNdouble s(2,2,1);
    s(1,2)=2;
    s(2,1)=3;
    s(2,2)=4;
    cout << s  << endl << Transpose(s) << endl;
  
    lightNNNdouble k(2,2,2,1);
    {  
      for (int i1=1;i1<=2;i1++)
        for (int i2=1;i2<=2;i2++)
          for (int i3=1;i3<=2;i3++)
            k(i1,i2,i3)=i1*100+i2*10+i3;
      cout << k << endl<<  Transpose(k) << endl; 
    }

    cout << k << endl << endl;
    cout << s << endl << endl;
    lightNNNdouble x;
    x=k*s;
    /*
      k= Table [i1*100+i2*10+i3,{i1,1,2},{i2,1,2},{i3,1,2}]
      s=Table [i*2+j+1, {i,0,1}, {j,0,1} ]
      MatrixForm[k.s]
      */


    cout << "------" << endl << x << endl << "-----"<<endl;


    lightNNNNdouble z(2,2,2,2,1);
    {
      for (int i1=1;i1<=2;i1++)
        for (int i2=1;i2<=2;i2++)
          for (int i3=1;i3<=2;i3++)  
            for (int i4=1;i4<=2;i4++)
              z(i1,i2,i3,i4)=i1*1000+i2*100+i3*10+i4;
      cout << z << endl<<  Transpose(z) << endl; 
    }

    /*
      Must be
      k= Table [i*4+j*2+k,  {i,0,1}, {j,0,1}, {k,0,1}]
      MatrixForm[Transpose[k]]

      Out[33]//MatrixForm= 0   4
      1   5
 
      2   6
      3   7
      */

    cerr << "Size of light3double: " << sizeof(light3double);
    cerr << " double: " << sizeof(double);
    cerr << " int: " << sizeof(int);
    cerr << " abs(-3.5)=" << abs(-3.5) << endl; 
#if 0
    // very compiler-dependent assertions in my opinion /Pontus
    #ifdef i386
    assert(sizeof(light3double) == (3*sizeof(double) + sizeof(int))); // extra size1, alignment therefore + 4
    #else
    assert(sizeof(light3double) == (3*sizeof(double) + sizeof(int) + 4)); // extra size1, alignment therefore + 4
    #endif
#endif
  }
  /*****************************************/

void f2()
  {
   
    light4double a0(1);
    assert (a0 == l4d(1,1,1,1)); 
    light4double a1;
    //  cout << "Initial (non-initialized)" << a1 << endl;
    light4double a2 (3,4,5,6);

    assert (a2 == l4d(3,4,5,6)); 

    light4double a3 (a2);
    assert (a3 == l4d(3,4,5,6)); 

    double data[4];
    data[0]=6; data[1]=7; data[2]=8;data[3]=9; 
    light4double a4 (data);
    assert (a4 == l4d(6,7,8,9)); 

    light4double a5 (5);
    assert (a5 == l4d(5,5,5,5)); 

    a4=a4;
    assert (a4 == l4d(6,7,8,9)); 

    a3=a4;
    assert (a4 == l4d(6,7,8,9)); 

    lightNdouble ax1(4); 
    lightNdouble ax2(3); 
    ax1(1)=11; ax1(2)=12; ax1(3)=13; ax1(4)=14;
    ax2(1)=51; ax2(2)=52; ax2(3)=53; 
    a3=ax1;// OK
    // a3=ax2; // should fail
    assert (a3 == l4d(11,12,13,14)); 

    assert(a3(1)=11);
    assert(a3(2)=12);
    assert(a3(3)=13);
    assert(a3(4)=14);

    a3(1)=61;
    a3(2)=62;
    a3(3)=63;
    a3(4)=64;

    assert (a3 == l4d(61,62,63,64)); 
    assert( ! ( a3 != l4d(61,62,63,64))); 
    a3(2)=77;
    assert (a3 != l4d(61,62,63,64)); 
    assert( ! (a3 == l4d(61,62,63,64))); 

    a2=l4d(2,3,4,5);
    a3=a2+=6;
    assert ( a2 ==  l4d(8,9,10,11));
    assert(a3==a2);
    a3=a2+=a2;
    assert ( a2 ==  l4d(16,18,20,22));
    assert(a3==a2);
    a3=a2-=6;
    assert ( a2 ==  l4d(10,12,14,16));assert(a3==a2);
    a3=a2-=a2;
    assert ( a2 ==  l4d(0,0,0,0));assert(a3==a2);

    a2=l4d(2,3,4,5);
    a3=a2*=2;
    assert ( a2 ==  l4d(4,6,8,10)); assert(a3==a2);
    a3=a2/=2;
    assert ( a2 ==  l4d(2,3,4,5)); assert(a3==a2);

    assert ( abs(a2.length() - sqrt(54))< 1e-6 );
    assert ( a2.dimension(1) == 4);
    assert ( a2.dimension(2) == 1);

    a2.normalize();
    a2 = a2 -  l4d(2/sqrt(54.0),3/sqrt(54.0),4/sqrt(54.0),5/ sqrt(54.0) );
    assert(a2.length()< 1e-6);
    a2=l4d(0,3,4,0);
    a2.normalize();
    a3=a2;
    a3-=l4d(0,0.6,0.8,0);
    assert(a3.length()< 1e-14);



    a2=l4d( 0.6,0.8,0,0.77);
    a2.Get(data);
    assert( data[0]==0.6); assert( data[1]==0.8);
    assert( data[2]==0  );assert( data[3]==0.77  );
    data[2]=0.3;
    a2.Set(data);
    assert( a2==  l4d(0.6,0.8,0.3,0.77));

  
 


    a3=5;
    assert (a3 == l4d(5,5,5,5)); 

    assert (+a3 == a3);
    assert (-a3 == l4d (-5,-5,-5,-5));
  
    a3 = l4d (2,3,4,5);
    assert (a3+a3 == l4d(4,6,8,10));
    assert (a3+ 3. == l4d(5,6,7,8));
    assert (3.+ a3 == l4d(5,6,7,8));
    a4 = l4d (1,2,1,7);
    assert (a3-a4 == l4d(1,1,3,-2));
    assert (a3- 4. == l4d(-2,-1,0,1));
    assert (4.-a3 == l4d(2,1,0,-1));
    assert (a3*a4 == 2+6+4+35);    // Dot product, .
    assert (a3*2. == l4d(4,6,8,10));
    assert (2.*a3 == l4d(4,6,8,10));
 
    light44double m1(11,12,13,14,21,22,23,24,31,32,33,34,41,42,43,44);
    // m1 = { { 11,12,13,14 },{ 21,22,23,24},{31,32,33,34 },{41,42,43,44} }
    // a3 = { 2,3,4,5}
    // a3.m1 = {414, 428, 442, 456}


    light44double m2=m1;
    cout <<  m1;
    assert (a3*m1==l4d(414, 428, 442, 456  ));
 
    assert (a3/2. == l4d(1,1.5,2,2.5));
    assert (12./a3 == l4d(6,4,3,2.4));
    assert (pow (a3,a3) == l4d (2*2,3*3*3, 4*4*4*4,5*5*5*5*5));
    assert (pow (a3,3.) == l4d (8,27,64,125));
    assert (pow (2.,a3) == l4d (4,8,16,32));
    assert (abs(l4d(-5,3,0,5))==l4d(5,3,0,5));
    assert (ElemProduct(a3,a3)==l4d(4,9,16,25)); // Times or * in Mathematica
    assert (ElemQuotient(a3,a3)==l4d(1,1,1,1));

    assert (Apply(a3,next) == l4d (3,4,5,6));

  
    a3=l4d(3,4,5,6);
    // a3= {3,4,5,6}

    assert (Apply(a3,sin_wrap_double) == l4d (sin(3),sin(4),sin(5),sin(6)));
 
    assert (Apply(a3,a3,sum) == l4d(6,8,10,12));
  
    // !! NO SUCH HERE ! 
    // <<Calculus`VectorAnalysis`
    // CrossProduct [a3,a3+1]
    // assert (Cross(a3,a3+1) == l4d(-1,2,-1,77777)); 

 
    light44double m3(12 , 15  ,18, 21,
                     16,  20,  24, 28,
                     20 , 25,  30, 35,
                     24,  30,  36, 42);




    // MatrixForm[Outer[Times,a3,a3+1]]
    assert (OuterProduct(a3,a3+1.) == m3);
 

    light4double a;
    a(1)=1;   
    light4int  b;
    b(1)=1;
    b(2)=2;
    b(3)=3;
    cout << pow(b,b)(1)  <<" " <<  pow(b,b)(2)  <<" " << pow(b,b)(3) << endl   ;

    cout << pow(b,b) << endl;
  }


void f3()
  {
   
    lightNdouble a0(4,1);
    assert (a0 == l4Nd(1,1,1,1)); 

    lightNdouble a0x(3,1);
    assert (a0x == l3Nd(1,1,1)); 

    lightNdouble a1;
    // cout << "Initial (non-initialized)(a1)" << a1 << endl;
    lightNdouble a15(5,15);
    cout << "Initial (initialized)(a15)" << a15 << endl;
    light4double a22 (3,4,5,6);
    lightNdouble a2 (a22);
    assert (a2 == l4Nd(3,4,5,6)); 

    lightNdouble a3 (a2);
    assert (a3 == l4Nd(3,4,5,6)); 

    double data[4];
    data[0]=6; data[1]=7; data[2]=8;data[3]=9; 
    lightNdouble a4 (4,data);
    assert (a4 == l4Nd(6,7,8,9)); 

    a4=a4;
    assert (a4 == l4Nd(6,7,8,9)); 

    a3=a4;
    assert (a4 == l4d(6,7,8,9)); 

    lightNdouble ax1(4); 
    lightNdouble ax2(3); 
    ax1(1)=11; ax1(2)=12; ax1(3)=13; ax1(4)=14;
    ax2(1)=51; ax2(2)=52; ax2(3)=53; 
    a3=ax1;// OK
  
    assert (a3 == l4Nd(11,12,13,14)); 
    a3=ax2; 
    assert (a3 == l3Nd(51,52,53));
    a3= l4d   (11,12,13,14);
    assert (a3 == l4Nd(11,12,13,14)); 
    a3= 6;
    assert (a3 == l4Nd(6,6,6,6));
    a3= l3d   (51,52,53);  
    assert (a3 == l3Nd(51,52,53));

    assert(a3(1)==51);
    assert(a3(2)==52);
    assert(a3(3)==53);
    // assert(a3(4)==14); // should fail

    a3(1)=61;
    a3(2)=62;
    a3(3)=63;
    // a3(4)=64;// should fail
    a3= l4d   (61,62,63,64);
    

    assert (a3 == l4Nd(61,62,63,64)); 
    assert( ! ( a3 != l4Nd(61,62,63,64))); 
    a3(2)=77;
    assert (a3 != l4Nd(61,62,63,64)); 
    assert( ! (a3 == l4Nd(61,62,63,64))); 

    a2=l4Nd(2,3,4,5);
    a3=a2+=6;
    assert ( a2 ==  l4Nd(8,9,10,11));
    assert(a3==a2);
    a3=a2+=a2;
    assert ( a2 ==  l4Nd(16,18,20,22));
    assert(a3==a2);
    a3=a2-=6;
    assert ( a2 ==  l4Nd(10,12,14,16));assert(a3==a2);
    a3=a2-=a2;
    assert ( a2 ==  l4Nd(0,0,0,0));assert(a3==a2);

    a2=l4Nd(2,3,4,5);
    a3=a2*=2;
    assert ( a2 ==  l4Nd(4,6,8,10)); assert(a3==a2);
    a3=a2/=2;
    assert ( a2 ==  l4Nd(2,3,4,5)); assert(a3==a2);

    assert ( abs(a2.length() - sqrt(54))< 1e-6 );
    assert ( a2.dimension(1) == 4);
    assert ( a2.dimension(2) == 1);

    a2.normalize();
    a2=a2-l4d(2/sqrt(54.0),3/sqrt(54.0),4/sqrt(54.0),5/ sqrt(54.0));
    assert(a2.length()< 1e-6);

    a2=l4d(0,3,4,0);
    a2.normalize();
    a3=a2;
    a3-=l4d(0,0.6,0.8,0);
    assert(a3.length()< 1e-14);



    a2=l4Nd( 0.6,0.8,0,0.77);
    a2.Get(data); // "size" must be correct before 
    assert( data[0]==0.6); assert( data[1]==0.8);
    assert( data[2]==0  );assert( data[3]==0.77  );
    data[2]=0.3;
    a2.Set(data);
    assert( a2==  l4Nd(0.6,0.8,0.3,0.77));

  
 


    a3=5;
    assert (a3 == l4Nd(5,5,5,5)); 

    assert (+a3 == a3);
    assert (-a3 == l4Nd (-5,-5,-5,-5));
  
    a3 = l4Nd (2,3,4,5);
    assert (a3+a3 == l4Nd(4,6,8,10));
    assert (a3+ 3 == l4Nd(5,6,7,8));
    assert (3+ a3 == l4Nd(5,6,7,8));
    a4 = l4Nd (1,2,1,7);

    lightNint signus=sign(a4);
    assert(signus(1)==1);
    signus=sign(signus);
    assert(signus(1)==1);


    assert (a3-a4 == l4Nd(1,1,3,-2));
   
    assert (a3- 4 == l4Nd(-2,-1,0,1));
    assert (4-a3 == l4Nd(2,1,0,-1));
    assert (a3*a4 == 2+6+4+35);    // Dot product, .
    assert (a3*2 == l4Nd(4,6,8,10));
    assert (2*a3 == l4Nd(4,6,8,10));
 
    
    light44double m1(11,12,13,14,21,22,23,24,31,32,33,34,41,42,43,44);
    // m1 = { { 11,12,13,14 },{ 21,22,23,24},{31,32,33,34 },{41,42,43,44} }
    // a3 = { 2,3,4,5}
    // a3.m1 = {414, 428, 442, 456}


    lightNNdouble m2=m1;

    // temorarily removed
    // assert (a3*m1==l4Nd(414, 428, 442, 456  ));
 
    assert (a3/2 == l4Nd(1,1.5,2,2.5));
    assert (12/a3 == l4Nd(6,4,3,2.4));
    assert (pow (a3,a3) == l4Nd (2*2,3*3*3, 4*4*4*4,5*5*5*5*5));
    assert (pow (a3,3.) == l4Nd (8,27,64,125));
    assert (pow (2.,a3) == l4Nd (4,8,16,32));
    assert (abs(l4Nd(-5,3,0,5))==l4Nd(5,3,0,5));
    assert (ElemProduct(a3,a3)==l4Nd(4,9,16,25)); // Times or * in Mathematica
    assert (ElemQuotient(a3,a3)==l4Nd(1,1,1,1));

    assert (Apply(a3,next) == l4Nd (3,4,5,6));

  
    a3=l4Nd(3,4,5,6);
    // a3= {3,4,5,6}

    assert (Apply(a3,sin_wrap_double) == l4Nd (sin(3),sin(4),sin(5),sin(6)));
 
    assert (Apply(a3,a3,sum) == l4Nd(6,8,10,12));
  
    // !! NO SUCH HERE ! 
    // <<Calculus`VectorAnalysis`
    // CrossProduct [a3,a3+1]
    // assert (Cross(a3,a3+1) == l4d(-1,2,-1,77777)); 

    light44double m3(12 , 15  ,18, 21,
                     16,  20,  24, 28,
                     20 , 25,  30, 35,
                     24,  30,  36, 42);
    m2=m3;
    // MatrixForm[Outer[Times,a3,a3+1]]
    assert (OuterProduct(a3,a3+1) == m2);


  }


void f4()
{
  {
    lightNNdouble s(2,2,1);
    s(1,2)=2;
    s(2,1)=3;
    s(2,2)=4;
    cout << s  << endl << Transpose(s) << endl;
  
    lightNNNdouble k(2,2,2,1);
    {  
      for (int i1=1;i1<=2;i1++)
        for (int i2=1;i2<=2;i2++)
          for (int i3=1;i3<=2;i3++)
            k(i1,i2,i3)=i1*100+i2*10+i3;
      cout << k << endl<<  Transpose(k) << endl; 
    }
    lightNNNdouble x;
    x=k*s;
    /*
      k= Table [i1*100+i2*10+i3,{i1,1,2},{i2,1,2},{i3,1,2}]
      s=Table [i*2+j+1, {i,0,1}, {j,0,1} ]
      MatrixForm[k.s]
      */


    cout << "------" << endl << x << endl << "-----"<<endl;


    lightNNNNdouble z(2,2,2,2,1);
    {
      for (int i1=1;i1<=2;i1++)
        for (int i2=1;i2<=2;i2++)
          for (int i3=1;i3<=2;i3++)  
            for (int i4=1;i4<=2;i4++)
              z(i1,i2,i3,i4)=i1*1000+i2*100+i3*10+i4;
      cout << z << endl<<  Transpose(z) << endl; 
    }

    /*
      Must be
      k= Table [i*4+j*2+k,  {i,0,1}, {j,0,1}, {k,0,1}]
      MatrixForm[Transpose[k]]

      Out[33]//MatrixForm= 0   4
      1   5
 
      2   6
      3   7
      */

    lightNNNdouble k1(2,2,3,1);
    {  
      for (int i1=1;i1<=2;i1++)
        for (int i2=1;i2<=2;i2++)
          for (int i3=1;i3<=3;i3++)
            k1(i1,i2,i3)=i1*100+i2*10+i3;
    }

    lightNNNdouble k2(3,5,1,1);
    {  
      for (int i1=1;i1<=3;i1++)
        for (int i2=1;i2<=5;i2++)
          for (int i3=1;i3<=1;i3++)
            k2(i1,i2,i3)=i1*100+i2*10+i3;
    }

    lightNNNNdouble zz(k1*k2);
    cout << "ZZ(" << 
      zz.dimension(1) << "," <<
      zz.dimension(2) << "," <<
      zz.dimension(3) << "," <<
      zz.dimension(4) << ")\n" <<
 
      " " << zz << endl;

    /*
      k1=Table[i1*100+i2*10+i3,{i1,1,2},{i2,1,2},{i3,1,3}]
      k2=Table[i1*100+i2*10+i3,{i1,1,3},{i2,1,5},{i3,1,1}]
      MatrixForm[k1.k2]
 
      Out[3]//MatrixForm= 71096    77426
      74456    81086
      77816    84746
      81176    88406
      84536    92066
 
      134396   140726
      140756   147386
      147116   154046
      153476   160706
      159836   167366
      */


  }
  {
    lightN33double f(2);
    // cout << f << endl; 

    double u;
    u=1;
    double x=sin(u);



  }

  {
    lightNdouble p; 
    lightNdouble r;
    p=l4d(4,5,6,7);
    r=p;
    cout << "r="<<r<<endl;
    cout << "p="<<p<<endl;
    p=l3d(14,15,16);
    cout << "p="<<p<<endl;
    p=l4d(4,5,6,7); 
    cout << "p="<<p<<endl;
  }
}

void f42()
  { 
    lightNdouble p; 
    p=l3d(1,2,3);
    lightNNdouble s(3,2);
    for (int i1=1;i1<=3;i1++)
      for (int i2=1;i2<=2;i2++)
        s(i1,i2)=i1*10+i2;
    cout << "p="<<p<<endl;
    cout << "s="<<s<<endl;
     cout << "s(1)="<<s(1)<<endl;
     cout << "s(2)="<<s(2)<<endl;
     cout << "s(3)="<<s(3)<<endl;
     assert(s(1)==l2d(11,12));
    assert(s(2)==l2d(21,22));
    assert(s(3)==l2d(31,32));
    //    assert(s.Row(2)==l2d(21,22));
    //    assert(s.Col(2)==l3d(12,22,32));

    assert(s(2,All)==l2d(21,22));
    assert(s(All,2)==l3d(12,22,32));

    lightNNdouble mm(2,2);
    //    mm.SetRow(1,l2d(11,12));
    //    mm.SetRow(2,l2d(21,22));
    mm.Set(1, All, l2d(11,12));
    mm.Set(2, All, l2d(21,22));
    double dx[4];
    mm.Get(dx); 
    cout <<"dx=" << dx[0] << " " <<   dx[1] << " " << 
      dx[2] << " " <<    dx[3] << " " <<endl  ;
    //    assert(mm.Col(2)==l2d(12,22));
    assert(mm(All, 2)==l2d(12,22));
    
    assert(s.SubMatrix(1,2,1,2)==mm);
    s.SetSubMatrix(2,3,1,2,mm);
    //assert(s.Col(2)==l3d(12,12,22));
    //assert(s.Col(1)==l3d(11,11,21));
    assert(s(All, 2)==l3d(12,12,22));
    assert(s(All, 1)==l3d(11,11,21));

    //assert(s.Rows(1,3)==s);
    //assert(s.Cols(1,2)==s);
    assert(s(R(1,3), All)==s);
    assert(s(All, R(1,2))==s);

    doubleN ff(l4Nd(12,13,14,15));
    cout << ff << endl;
    cout << ff.SubVector(2,4)<< endl;
    assert(ff.SubVector(2,4)==l3Nd(13,14,15));
    assert(ff.SubVector(2,0)==l3Nd(13,14,15));
    ff.SetSubVector(2,0,l3Nd(23,24,25));
    assert(ff==l4Nd(12,23,24,25));

    cout << " Checks multiplications " << endl;
    {
    for (int xi1=1;xi1<=3;xi1++)
      for (int xi2=1;xi2<=2;xi2++)
        s(xi1,xi2)=xi1*10+xi2;
    }
    cout << "p*s=" <<"["<< p<< "]*["<<s<<"]=" <<p*s<<endl;   // 3 * 32 = 2  (13*32=12)  32=3 rows, 2 columns
    // 2*22= 12*22 = 12 = 2    
    if (0)    cout << "s*p=" << s*p<<endl;   // 32 * 3  (32*31)   = NOT allowed at runtime
    assert(p*s==l2d(146,152));


    // For every single operation the vector is considered as vertical.
    lightNdouble k(2); 
    k(1)=1; k(2)=2;
    cout << "k=" << k << endl;
    
    if (0)    cout << "k*s="<<"["<< k<< "]*["<<s<<"]="  << k*s<<endl;   // 2 * 32  (12*32) = NOT ALLOWED
    cout << "s*k="<<"["<< s<< "]*["<<k<<"]="  << s*k<<endl;   // 32 * 2 = 3  (32*21=3) OK
    
    assert( s*k==l3d(35,65,95));
    // So in Mathematica and in Lightmat vectors are row-vectors if they are
    // to the left from *-sign and column-vectors if they are to the right from *-sign.

    lightNNdouble sss(2,4);
    {
    for (int xi1=1;xi1<=2;xi1++)
      for (int  xi2=1;xi2<=4;xi2++)
        sss(xi1,xi2)=xi1*4+xi2-4;
    }
    cout << "s*sss" << s << "*" << sss << "=" << s*sss << endl ; // 32*24=34
    sss=s*sss;
    assert(sss.Row(1)==l4Nd(71,94,117,140   ));
    assert(sss.Row(2)==l4Nd(131,174,217,260 ));
    assert(sss.Row(3)==l4Nd(191,254,317,380 ));


 



    assert( make_lightN(3,7.0,2.0,9.0)==l3Nd(7,2,9));

    // testing setshape
    lightNNdouble d;
    // default size.
    assert(d.dimension(1)==10);
    assert(d.dimension(2)==10);
    // (NO)    assert(d(1,1)==0);
    
    // forced size 
    d.SetShape(3,3);
    assert(d.dimension(1)==3);
    assert(d.dimension(2)==3);

    d=7;
    //d.SetCol(2,2);
    //d.SetRow(3,3);
    d.Set(All, 2, 2);
    d.Set(3, All, 3);
    // "Col2 = 2, col3=3 
    assert(d==
           make_lightN(3,
                       make_lightN(3,7.0,2.0,7.0),
                       make_lightN(3,7.0,2.0,7.0),
                       make_lightN(3,3.0,3.0,3.0)));

    d.SetSubMatrix(0,0,0,0,8);
    d.SetSubMatrix(1,2,1,2,12);
    //    cout<<"After d.SetSubMatrix(1,2,1,2,12);" <<endl  <<d<<endl;
    assert(d==
           make_lightN(3,
                       make_lightN(3,12.0, 12.0, 8.0),
                       make_lightN(3,12.0, 12.0, 8.0),
                       make_lightN(3,8.0, 8.0, 8.0)));



    // d.SetRows(0,0,5);
    // d.SetCols(1,2,6);
    d.Set(All, All, 5);
    d.Set(All, R(1,2), 6);

    //    cout <<"After d.SetRows(0,0,5);, d.SetCols(1,2,6);" <<endl<<d<<endl;

    assert(d==
           make_lightN(3,
                       make_lightN(3,6.0, 6.0, 5.0),
                       make_lightN(3, 6.0,  6.0, 5.0),
                       make_lightN(3,6.0, 6.0, 5.0)));
  

    lightNdouble  e(4);
    e.SetSubVector(0,0,8);
    e.SetSubVector(2,3,7);
    assert(e==l4Nd(8,7,7,8));     


    double dd=3; int ii=2;

    // These shouldwork for overloading
    //cout << make_lightN(4,2,3,4,5) << endl;

    assert( make_lightN(4,2.0,3.0,4.0,5.0)==l4Nd(2.0,3.0,4.0,5.0));
    assert( make_lightN(4,2.1,3.1,4.1,5.1)==l4Nd(2.1,3.1,4.1,5.1));
    //cout << make_lightN(2,ii,ii) <<endl; 
    assert( make_lightN(2,dd,dd)==l2d(dd,dd));

  }

void f5()
  {
    // New test for lightNNdouble

    lightNNdouble a=l22NNd(5,6,7,8);
    assert(a==l22NNd(5,6,7,8));
    lightNNdouble b(2,2,7);
    cout <<"["<< b<<"]";   cout << endl;
 
    cout <<"[" << l22NNd(7,7,7,7)<<"]";
    cout << endl;
    cout << endl;
    cout << endl;
    assert(b==l22NNd(7,7,7,7));
    b=a;
    assert(b==a);
    double s[4];
    s[0]=5;
    s[1]=6;
    s[2]=7;
    s[3]=8;
    lightNNdouble c(2,2,s);
    assert(c==l22NNd(5,6,7,8));
    c=3;
    assert(c==l22NNd(3,3,3,3));
    assert(  l22NNd(5,6,7,8) + 1 == l22NNd(6,7,8,9));
    c=l22NNd(5,6,7,8);
    assert(c(1,2)==6);
    assert(c(2,1)==7);
    c(2,1)=77;
    assert(c==l22NNd(5,6,77,8));
    lightNdouble crow;
    crow=  c(2);
    assert(crow==make_lightN(2,77.0, 8.0));
    c.SetCol(1,55);
    assert(c==  l22NNd(55,6,55,8));
    c.SetRow(2,88);
    assert(c==  l22NNd(55,6,88,88));
    c.SetCols(1,2,99);
    assert(c==  l22NNd(99,99,99,99));
    //c.SetRows(1,2,88);
    c.Set(R(1,2), All, 88);
    assert(c==  l22NNd(88,88,88,88));
    assert(c!= l22NNd(88,88,88,0));
    lightNNdouble d;
    d = (c+=2);
    assert(d ==  l22NNd(90,90,90,90));
    assert(c ==  l22NNd(90,90,90,90));
    d = (c-=2);
    assert(c==  l22NNd(88,88,88,88));
    assert(d==  l22NNd(88,88,88,88));
    d = (c/=8);
  
    assert(c==  l22NNd(11,11,11,11));
    assert(d==  l22NNd(11,11,11,11));
    lightNNdouble e=ElemProduct(c,d);
    assert(e== l22NNd(121,121,121,121));
    c=l22NNd(5,6,7,8);
    assert(Transpose(c)==l22NNd(5,7,6,8));
  }

void f51()
  {
    // TEST added 27/11/97.

    cout << "TEST  27/11/97";
    lightNNNdouble k1(4,2,3);
    { 
    for (int i1=1;i1<=4;i1++)
      for (int i2=1;i2<=2;i2++)
        for (int i3=1;i3<=3;i3++)
          k1(i1,i2,i3)=i1*100+i2*10+i3;
    }

  
    lightNNdouble k2(3,5);
    {
    for (int i1=1;i1<=3;i1++)
      for (int i2=1;i2<=5;i2++)
        k2(i1,i2)=i1*10+i2;
    }

    cout << "[" << k1 << "]*[" << k2 << "]=" << "[" << k1*k2 << "]" << endl;  
    lightNNNdouble k12= k1*k2;
    cout << "K12(1)==" <<k12(1)<<endl;

    assert( k12.dimension(1)==4);
    assert( k12.dimension(2)==2);
    assert( k12.dimension(3)==5);
    cout << "K12[2,1,3]="<< k12(2,1,3)<< endl;
    assert( k12(2,1,3)== 14648 );

    // DIMENSIONS: 423*35=425 

    /* Mathematica:
       Dot[Table [ i1*100+i2*10+i3,  {i1,1,4}, {i2,1,2}, {i3,1,3}],
       Table [ i1*10+i2,  {i1,1,3}, {i2,1,5}]]//MatrixForm
       */

    /* Should be:
       7076 7706 
       7412 8072 
       7748 8438 
       8084 8804 
       8420 9170 
  
       13376 14006 
       14012 14672 
       14648 15338 
       15284 16004 
       15920 16670 
  
       19676 20306 
       20612 21272 
       21548 22238 
       22484 23204 
       23420 24170 
  
       25976 26606 
       27212 27872 
       28448 29138 
       29684 30404 
       30920 31670
       */


    // ****************
    // 423*3=42
    lightNdouble k3=l3Nd(2,3,4);
    cout << "[" << k1 << "]*[" << k3 << "]=" << "[" << k1*k3 << "]" << endl; 
    lightNNdouble k13=  k1*k3;
    assert(k13.dimension(1)==4);
    assert(k13.dimension(2)==2);
    /* Mma:
       Dot[Table [ i1*100+i2*10+i3,  {i1,1,4}, {i2,1,2}, {i3,1,3}],{2,3,4}]//MatrixForm
       */
   
    /*
      1010 1100 
      1910 2000 
      2810 2900 
      3710 3800 
      */


    // ************
    // 34*425=325

    lightNNdouble kk1(3,4);
    {
    for (int i1=1;i1<=3;i1++)
      for (int i2=1;i2<=4;i2++)
        kk1(i1,i2)=i1*10+i2;
  
    }
    lightNNNdouble kk2(4,2,5);
    {
    for (int i1=1;i1<=4;i1++)
      for (int i2=1;i2<=2;i2++)
        for (int i3=1;i3<=5;i3++)
          kk2(i1,i2,i3)=i1*100+i2*10+i3;
    }

    cout << "[" << kk1 << "]*[" << kk2 << "]=" << "[" << kk1*kk2 << "]" << endl;  
    lightNNNdouble kk12= kk1*kk2;
    cout << "Kk12(1)==" <<kk12(1)<<endl;

    assert( kk12.dimension(1)==3);
    assert( kk12.dimension(2)==2);
    assert( kk12.dimension(3)==5);
    cout << "Kk12[2,1,3]="<< kk12(2,1,3)<< endl;
    assert( kk12(2,1,3)== 24170 );
    /* Result 
       13550 14050 
       13600 14100 
       13650 14150 
       13700 14200 
       13750 14250 
  
       23990 24890 
       24080 24980 
       24170 25070 
       24260 25160 
       24350 25250 
  
       34430 35730 
       34560 35860 
       34690 35990 
       34820 36120 
       34950 36250
       */

    /* Mathematica:
       Dot[
       Table [ i1*10+i2,  {i1,1,3}, {i2,1,4}]
       ,
       Table [ i1*100+i2*10+i3,  {i1,1,4}, {i2,1,2}, {i3,1,5}]
       ]//MatrixForm
       */


    /******** */
    // 4*425=25
    lightNdouble kk3=l4Nd(1,2,3,4);
    cout << "[" << kk3 << "]*[" << kk2 << "]=" << "[" << kk3*kk2 << "]" << endl; 
    lightNNdouble kk32=  kk3*kk2;
    assert(kk32.dimension(1)==2);
    assert(kk32.dimension(2)==5);
    /* Mma:
       Dot[{1,2,3,4},Table [ i1*100+i2*10+i3,  {i1,1,4}, {i2,1,2}, {i3,1,5}]]//MatrixForm
       */
   
    /*
      [3110 3120 3130 3140 3150 
      3210 3220 3230 3240 3250 ]
      */

    /****** */
    // 236*654=2354
    lightNNNdouble kkk1(2,3,6);
    lightNNNdouble kkk2(6,5,4);
    {    
      int i1;
      for ( i1=1;i1<=2;i1++)
         for (int i2=1;i2<=3;i2++)
           for (int i3=1;i3<=6;i3++)
             kkk1 (i1,i2,i3)=i1*100+i2*10+i3;
      for ( i1=1;i1<=6;i1++)
        for (int i2=1;i2<=5;i2++)
          for (int i3=1;i3<=4;i3++)
            kkk2 (i1,i2,i3)=i1*100+i2*10+i3;
    } 
    lightNNNNdouble  kkk12=kkk1*kkk2;
    cout << "\nTEST 236*654=2354\n";
    cout << "[" << kkk1 << "]\n*\n[" << kkk2 << "]\n=\n[" <<  kkk12 << "\n]";
     
 /* Mma:
       Dot[
        Table [ i1*100+i2*10+i3,  {i1,1,2}, {i2,1,3}, {i3,1,6}],
        Table [ i1*100+i2*10+i3,  {i1,1,6}, {i2,1,5}, {i3,1,4}]
          ]//MatrixForm
          */

     /*

[(2.47591e+05 2.48272e+05 2.48953e+05 2.49634e+05    2.69251e+05 2.69992e+05 2.70733e+05 2.71474e+05    2.90911e+05 2.91712e+05 2.92513e+05 2.93314e+05 
  2.54401e+05 2.55082e+05 2.55763e+05 2.56444e+05    2.76661e+05 2.77402e+05 2.78143e+05 2.78884e+05    2.98921e+05 2.99722e+05 3.00523e+05 3.01324e+05 
  2.61211e+05 2.61892e+05 2.62573e+05 2.63254e+05    2.84071e+05 2.84812e+05 2.85553e+05 2.86294e+05    3.06931e+05 3.07732e+05 3.08533e+05 3.09334e+05 
  2.68021e+05 2.68702e+05 2.69383e+05 2.70064e+05    2.91481e+05 2.92222e+05 2.92963e+05 2.93704e+05    3.14941e+05 3.15742e+05 3.16543e+05 3.17344e+05 
  2.74831e+05 2.75512e+05 2.76193e+05 2.76874e+05    2.98891e+05 2.99632e+05 3.00373e+05 3.01114e+05    3.22951e+05 3.23752e+05 3.24553e+05 3.25354e+05 
  
  4.64191e+05 4.65472e+05 4.66753e+05 4.68034e+05    4.85851e+05 4.87192e+05 4.88533e+05 4.89874e+05    5.07511e+05 5.08912e+05 5.10313e+05 5.11714e+05 
  4.77001e+05 4.78282e+05 4.79563e+05 4.80844e+05    4.99261e+05 5.00602e+05 5.01943e+05 5.03284e+05    5.21521e+05 5.22922e+05 5.24323e+05 5.25724e+05 
  4.89811e+05 4.91092e+05 4.92373e+05 4.93654e+05    5.12671e+05 5.14012e+05 5.15353e+05 5.16694e+05    5.35531e+05 5.36932e+05 5.38333e+05 5.39734e+05 
  5.02621e+05 5.03902e+05 5.05183e+05 5.06464e+05    5.26081e+05 5.27422e+05 5.28763e+05 5.30104e+05    5.49541e+05 5.50942e+05 5.52343e+05 5.53744e+05 
  5.15431e+05 5.16712e+05 5.17993e+05 5.19274e+05    5.39491e+05 5.40832e+05 5.42173e+05 5.43514e+05    5.63551e+05 5.64952e+05 5.66353e+05 5.67754e+05 )
]

      */

     assert( kkk12.dimension(1)==2);
     assert( kkk12.dimension(2)==3);
     assert( kkk12.dimension(3)==5);
     assert( kkk12.dimension(4)==4);
     assert (kkk12(2,1,3,2)==491092);
  }


void f6()
  {

    // TEST Added 12/1/98

    cout << "TEST 12/1/98";
    lightNNNNdouble kbig4(4,2,5,3);
    {
    for (int i1=1;i1<=4;i1++)
      for (int i2=1;i2<=2;i2++)
        for (int i3=1;i3<=5;i3++)
          for (int i4=1;i4<=3;i4++)
            kbig4(i1,i2,i3,i4)=i1*1000+i2*100+i3*10+i4;
    }
    // TEST: 4253*3=425
 
    lightNdouble k_small_1=l3Nd(2,3,4);
      
    lightNNNdouble k_res_3= kbig4 * k_small_1;
    assert( k_res_3.dimension(1)==4);
    assert( k_res_3.dimension(2)==2);
    assert( k_res_3.dimension(3)==5);
    assert( k_res_3(2,1,3)==19190); 
    /* Mma:
       Dot[Table [ i1*1000+i2*100+i3*10+i4,
       {i1,1,4 }, {i2,1,2}, {i3,1,5}, {i4,1,3} ],{2,3,4}]//MatrixForm
       */
    /*
      10010 10910 
      10100 11000 
      10190 11090 
      10280 11180 
      10370 11270 
  
      19010 19910 
      19100 20000 
      19190 20090 
      19280 20180 
      19370 20270 
  
      28010 28910 
      28100 29000 
      28190 29090 
      28280 29180 
      28370 29270 
  
      37010 37910 
      37100 38000 
      37190 38090 
      37280 38180 
      37370 38270
      */
    cout <<  k_res_3;


    // ***********************************************
       
    // TEST: 2*2222=222

    cout << "\n TEST: 2*2211=211 \n";
    lightNNNNdouble kbig4_2(2,2,2,2) ;
    {
    for (int i1=1;i1<=2;i1++)
      for (int i2=1;i2<=2;i2++)
        for (int i3=1;i3<=2;i3++)
          for (int i4=1;i4<=2;i4++)
            kbig4_2(i1,i2,i3,i4)=i1*1000+i2*100+i3*10+i4;
    }
    cout << "\n" << kbig4_2 <<"\n";
      
    lightNdouble k_small_3= l2d(5,7);
    lightNNNdouble   k_res_32 = k_small_3*kbig4_2;
   
    /* Mma:
       Dot[ { 5,7 },  Table[i1*1000+i2*100+i3*10+i4,
       {i1,1,2 }, {i2,1,2}, {i3,1,2}, {i4,1,2} ]]//MatrixForm
       */
    cout  << "\n" <<  k_res_32 <<"\n";


    // TEST: 4*4253=253
    cout << "\n TEST: 4*4253=253\n";

    lightNdouble k_small_2=l4Nd(2,3,4,5);
    k_res_3=  k_small_2*kbig4;
    assert( k_res_3.dimension(1)==2);
    assert( k_res_3.dimension(2)==5);
    assert( k_res_3.dimension(3)==3);
    cout <<  k_res_3 << "\n" ;
    assert( k_res_3(2,1,3)==42982 ); 
    /* Mma:
       Dot[ { 2,3,4,5 },  Table[i1*1000+i2*100+i3*10+i4,
       {i1,1,4 }, {i2,1,2}, {i3,1,5}, {i4,1,3} ]]//MatrixForm
       */

    /*
      41554 41694 41834 41974 42114 
      41568 41708 41848 41988 42128 
      41582 41722 41862 42002 42142 
  
      42954 43094 43234 43374 43514 
      42968 43108 43248 43388 43528 
      42982 43122 43262 43402 43542
      */


    // TEST: 34*4253=3253
    cout << "\n TEST: 34*4253=3253\n";
    lightNNdouble k_mid_2(3,4);
    {
    for (int i1=1;i1<=3;i1++)
      for (int i2=1;i2<=4;i2++)
        k_mid_2(i1,i2)=i1*10+i2;
    }  
  lightNNNNdouble k_res_4 = k_mid_2*kbig4;
    assert( k_res_4.dimension(1)==3);
    assert( k_res_4.dimension(2)==2);
    assert( k_res_4.dimension(3)==5);
    assert( k_res_4.dimension(4)==3);
    assert( k_res_4(2,1,3,2)== 241880  ); 
    cout << "\n" <<k_res_4 << endl;
    /* Mma:
       Dot[ 
       Table[i1*10+i2, {i1,1,3},{i2,1,4}]
       , 
       Table[i1*1000+i2*100+i3*10+i4,
       {i1,1,4 }, {i2,1,2}, {i3,1,5}, {i4,1,3} ]]
       //MatrixForm
       */
    /*

      135550 135600 135650    140550 140600 140650 
      136050 136100 136150    141050 141100 141150 
      136550 136600 136650    141550 141600 141650 
      137050 137100 137150    142050 142100 142150 
      137550 137600 137650    142550 142600 142650 
  
      239990 240080 240170    248990 249080 249170 
      240890 240980 241070    249890 249980 250070 
      241790 241880 241970    250790 250880 250970 
      242690 242780 242870    251690 251780 251870 
      243590 243680 243770    252590 252680 252770 
  
      344430 344560 344690    357430 357560 357690 
      345730 345860 345990    358730 358860 358990 
      347030 347160 347290    360030 360160 360290 
      348330 348460 348590    361330 361460 361590 
      349630 349760 349890    362630 362760 362890 

      */
       

    // TEST: 4253*32=4252
    cout << "\n TEST: 4253*32=4252\n";
    lightNNdouble k_mid2_2(3,2);
    {
    for (int i1=1;i1<=3;i1++)
      for (int i2=1;i2<=2;i2++)
        k_mid2_2(i1,i2)=i1*10+i2;
    }
    k_res_4 = kbig4 *  k_mid2_2 ;
    assert( k_res_4.dimension(1)==4);
    assert( k_res_4.dimension(2)==2);
    assert( k_res_4.dimension(3)==5);
    assert( k_res_4.dimension(4)==2);

    assert( k_res_4(2,1,3,2)== 140732  ); 
    cout << "\n" <<k_res_4 << endl;

    /* Mma:
       Dot[ 
       Table[i1*1000+i2*100+i3*10+i4,
       {i1,1,4 }, {i2,1,2}, {i3,1,5}, {i4,1,3} ]
       ,
       Table[i1*10+i2, {i1,1,3},{i2,1,2}]]
       //MatrixForm
       */
    /*
      70076 73412    76376 80012 
      70706 74072    77006 80672 
      71336 74732    77636 81332 
      71966 75392    78266 81992 
      72596 76052    78896 82652 
  
      133076 139412    139376 146012 
      133706 140072    140006 146672 
      134336 140732    140636 147332 
      134966 141392    141266 147992 
      135596 142052    141896 148652 
  
      196076 205412    202376 212012 
      196706 206072    203006 212672 
      197336 206732    203636 213332 
      197966 207392    204266 213992 
      198596 208052    204896 214652 
  
      259076 271412    265376 278012 
      259706 272072    266006 278672 
      260336 272732    266636 279332 
      260966 273392    267266 279992 
      261596 274052    267896 280652 

      */



  }
 
void f7()
  {
    cout << "Functions introduced at 3/2/98 **** "<< endl;
    {

      //  irint(X). Rounds the integer.
      // Changed to irint to solve problem with Min(int, double). /TP
      
      // Note that rint(light*) returns light*int
      //           rint(double) returns double, not int!.
      // This fact is important in code generation. Type derivation shoud be
      // carefully used there.
 
    lightNint n=irint(l4Nd(-5.6,-5.5,-5.4,5.4));
    lightNdouble n2=l4Nd(irint(-5.6),irint(-5.5),irint(-5.4),irint(5.4));
    cout << n2 << endl;
    cout << l4Nd(-6, -6, -5, 5) << endl;
    assert(n2==l4Nd(-6, -6, -5, 5)); 
    lightNdouble n3=n;
    assert(n3==n2);
    assert (irint (5.6) == 6);
    }
    {
      // IntegerPart. Returns (int)x always as int.

      lightNdouble n=IntegerPart(l4Nd(-5.6,-5.5,-5.4,5.4));
      assert(n==l4Ni(-5,-5,-5,5));
      assert(IntegerPart(n)==n);
      assert(IntegerPart(5.5)==5);
      assert(IntegerPart(5)==5);

    }
    {
      // FractionalPart. Returns x-(int)x as type of(x)
      lightNdouble n=l4Nd(-5.4,0,5.0,5.9);
      cout << FractionalPart(n) << endl;
      lightNint n1=l4Ni(-5,-4,0,99);
      assert(FractionalPart(n1)==l4Ni(0,0,0,0));
      assert(FractionalPart(-5)==0);

    }

    {
      // Quotient[m,n] is generated as floor(n/m)
      // if n or m are scalars;
      // floor(ElemQuotient(n,m)) if both are non-scalars.
      // 
       lightNdouble n=l4Nd(-5.4,0,5.0,5.9);
       lightNdouble m=l4Nd(2.1,2.0,1.1,5.0);
       cout << ifloor(ElemQuotient(n,m))<<endl;
       cout << ifloor(n/(-1.2))<<endl;
       assert(ifloor(-1.9)==-2);
    }


    {
      {
      lightNint n1=l4Ni(8,8,-8,-8);
      lightNint n2=l4Ni(3,-3,3,-3);
      lightNint n3=l4Ni(2,-2,2,-2);
      cout <<"Mod(n1,n3)=" <<Mod(n1,n3)<< endl;
      assert(Mod(n1,n2)==l4Ni(2,-1,1,-2));
      assert(Mod(n2,n1)==l4Ni(3,5,-5,-3));
      assert( Mod(n2,2)==l4Ni(1,1,1,1));
      assert( Mod(n2,-2)==l4Ni(-1,-1,-1,-1));
      assert(  Mod(13,n2)==l4Ni(1,-2,1,-2));
      assert(  Mod(-13,n2)==l4Ni(2,-1,2,-1));

      }
      {
      lightNdouble n1=l4Nd(8,8,-8,-8);
      lightNdouble n2=l4Nd(3,-3,3,-3);
      cout <<"Mod(8,-3)="<< Mod(8,-3) << endl; 
      assert(Mod(n1,n2)==l4Ni(2,-1,1,-2));
      assert(Mod(n2,n1)==l4Ni(3,5,-5,-3));
      assert( Mod(n2,2)==l4Ni(1,1,1,1));
      assert( Mod(n2,-2)==l4Ni(-1,-1,-1,-1));
      assert(  Mod(13,n2)==l4Ni(1,-2,1,-2));
      assert(  Mod(-13,n2)==l4Ni(2,-1,2,-1));
      }
      {
      lightNdouble d1=l4Nd(8.2,8.2,-8.2,-8.2);
      lightNdouble d2=l4Nd(3.1,-3.1,3.1,-3.1);
 
      assert( (Mod(d1,d2)-l4Nd(2.,-1.1,1.1,-2.)).length()<1e-13);
      assert( (Mod(d2,d1)-l4Nd(3.1,5.1,-5.1,-3.1)).length()<1e-13);
                       
      assert( (Mod(d2,2)-l4Nd(1.1,0.9,1.1,0.9)).length()<1e-13);
    
      assert( (Mod(d2,-2)-l4Nd(-0.9,-1.1,-0.9,-1.1)).length()<1e-13);
      assert( (Mod(13,d2)-l4Nd(0.6,-2.5,0.6,-2.5)).length()<1e-13);
  
      assert( (Mod(-13,d2)-l4Nd(2.5,-0.6,2.5,-0.6)).length()<1e-13);

      lightNdouble d3=l4Ni(2,3,4,5);
      lightNint i3=l4Ni(2,3,4,5);
      d3=i3;
      }
    }


    {
      lightNint n1=l4Ni(8,8,2,13);
      assert(LightMax(n1)==13);
      assert(LightMin(n1)==2);
      lightNint n2=l4Ni(1,8,2,13);
      assert(LightMax(n2)==13);
      assert(LightMin(n2)==1);
      lightNdouble n3=l4Nd(8.4,8.6,2.3,13.4);
      assert(LightMax(n3)==13.4);
      assert(LightMin(n3)==2.3);
       
 
    }


    {
      lightNNNNint s=
        make_lightN(2,
                    make_lightN(2,
                                make_lightN(2,
                                            make_lightN(2, 11,12),
                                            make_lightN(2, 13,14)
                                            ),
                                make_lightN(2,
                                            make_lightN(2, 15,16),
                                            make_lightN(2, 17,18)
                                            )
                                ),

                   make_lightN(2,
                                make_lightN(2,
                                            make_lightN(2, 19,20),
                                            make_lightN(2, 21,22)
                                            ),
                                make_lightN(2,
                                            make_lightN(2, 23,24),
                                            make_lightN(2, 25,26)
                                            )
                                )
                    );

      cout << s;
    assert(s.dimension(1)==2);
    assert( s.dimension(2)==2);
    assert( s.dimension(3)==2);
    assert( s.dimension(4)==2);

     


    }
  
    {
      lightNNNint kkk1(2,3,6);
      for (int i1=1;i1<=2;i1++)
         for (int i2=1;i2<=3;i2++)
           for (int i3=1;i3<=6;i3++)
             kkk1 (i1,i2,i3)=i1*100+i2*10+i3;
     lightNNNint kkk2=kkk1;
     lightNint p(6,990);
     cout << kkk1(2,1);
     kkk1.SetVector12(2,1,p);
     assert(kkk1(2,1)==p);
     lightNNint pp(3,6,880);
     kkk1.SetMatrix1(1,pp);
         assert(kkk1(1)==pp);

    }

  }

void f8() {
  {
   intN a   =make_lightN(3, 2,3,5);
   intN b   =make_lightN(3, 4,2,3);
   intN c=Cross(a,b);
   assert (c==make_lightN(3,-1,14,-8));
   }

 {
   intN a   =make_lightN(4, 2,3,5,1);
   intN b   =make_lightN(4, 4,2,3,2);
   intN c   =make_lightN(4, 5,2,7,4);
   intN d=Cross(a,b,c);
   assert (d==make_lightN(4,-18, 21, -12, 33));
   }
 {
   intN a   =make_lightN(5, 2,3,5,1,3);
   intN b   =make_lightN(5, 4,2,3,2,7);
   intN c   =make_lightN(5, 5,2,7,4,1);
   intN d   =make_lightN(5, 4,1,4,2,6);
   intN e=Cross(a,b,c,d);
   assert (e==make_lightN(5, -126, -18, 15, 132, 33));
   }

}

//
// Testing of () with ranges in lightN, light3, light4, lightNN, light33, light44
//

void f9() {
  {
   intN a = make_lightN(5, 1, 2, 3, 4, 5);
   intN b = make_lightN(3, 10, 10, 10);
   b = a(R(2, 4));
   cout << b << endl;
   assert (b==make_lightN(3, 2, 3, 4));
  }
  {
    light4int a (1, 2, 3, 4);
    intN b(7);
    b = a(R(3, 4));
    assert (b==make_lightN(2, 3, 4));
  }
  {
    light3int a (1, 2, 3);
    lightNint b = make_lightN (3, 11, 11, 12);
    b = a(R(2, 3));
    assert (b==make_lightN (2, 2, 3));
  }
  {
    cout << "Testing range in NN" << endl;
    intNN a = get_intNN (7, 5);
    intNN b = a(R(2, 4), R(3, 5));
    cout << a << endl;
    cout << b << endl;
    assert (b==make_lightN(3, 
			   make_lightN (3, 7, 8, 9),
			   make_lightN (3, 12, 13, 14),
			   make_lightN (3, 17, 18, 19)
			   ));
  }
  {
    light33int a (1, 2, 3, 4, 5, 6, 7, 8, 9);
    lightNNint b;
    b = a(R(2, 3), R(1,2));
    cout << b << endl;
    assert (b==make_lightN(2, make_lightN(2, 4, 5), make_lightN (2, 7, 8)));
  }
  {
    light44int a (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
    light33int b (1, 2, 3, 4, 5, 6, 7, 8, 9);
    light33int c (6, 7, 8, 10, 11, 12, 14, 15, 16);
    b = a(R(2, 4), R(2,4));
    cout << a << endl << endl;
    cout << b << endl << endl;
    cout << c << endl;
    assert (b==c);
  }

  {
    cout << "Testing range in NNN" << endl;
    intNNN a = get_intNNN (4, 5, 3);
    intNNN b = a(R(2, 4), R(3, 5), R(1,2));
    cout << a << endl;
    cout << b << endl;
    assert (b==
	    make_lightN(3,
			make_lightN(3, 
				    make_lightN (2, 21, 22),
				    make_lightN (2, 24, 25),
				    make_lightN (2, 27, 28)),
			make_lightN(3, 
				    make_lightN (2, 36, 37),
				    make_lightN (2, 39, 40),
				    make_lightN (2, 42, 43)),
			make_lightN(3, 
				    make_lightN (2, 51, 52),
				    make_lightN (2, 54, 55),
				    make_lightN (2, 57, 58))));
  }

  {
    cout << "Testing range in NNNN" << endl;
    intNNNN a = get_intNNNN (2, 4, 5, 3);
    intNNNN b = a(R(1,1), R(2, 4), R(3, 5), R(1,2));
    intNNNN c = make_lightN(1, make_lightN(3,
					   make_lightN(3, 
						       make_lightN (2, 21, 22),
						       make_lightN (2, 24, 25),
						       make_lightN (2, 27, 28)),
					   make_lightN(3, 
						       make_lightN (2, 36, 37),
						       make_lightN (2, 39, 40),
						       make_lightN (2, 42, 43)),
					   make_lightN(3, 
						       make_lightN (2, 51, 52),
						       make_lightN (2, 54, 55),
						       make_lightN (2, 57, 58))));

    cout << a << endl << endl;
    cout << b << endl << endl;
    cout << c << endl << endl;
    
    assert (b==c);
  }
}


void f10() {
  //
  // Join
  //

  {
   intN a = make_lightN(5, 1, 2, 3, 4, 5);
   intN b = make_lightN(3, 10, 10, 10);
   intN c = Join (a, b);
   cout << c << endl;
   assert (c==make_lightN(8, 1, 2, 3, 4, 5, 10, 10, 10));
  }
  {
    intN a = make_lightN(5, 1, 2, 3, 4, 5);
    intN b = a;
    a = Join (a(R(1,-1))(R(1,0)), a);
    a = Join (a, a(R(6,0)));
    cout << a << endl;
    assert (a==b);
  }
  {
    intNN a = get_intNN (3, 2);
    intNN b = get_intNN (2, 2);
    intNN c = Join (a, b);
    cout << c << endl;
    assert (c==make_lightN(5, 
			   make_lightN (2, 0, 1),
			   make_lightN (2, 2, 3),
			   make_lightN (2, 4, 5),
			   make_lightN (2, 0, 1),
			   make_lightN (2, 2, 3)
			   ));
  }
  {
    cout << "Testing join in NNN" << endl;
    intNNN a = get_intNNN (4, 2, 3);
    intNNN b = get_intNNN (2, 2, 3);
    intNNN c = Join (a, b);
    cout << a << endl << endl;
    cout << b << endl << endl;
    cout << c << endl << endl;
    assert (c==
	    make_lightN(6,
			make_lightN(2, 
				    make_lightN (3, 0, 1, 2),
				    make_lightN (3, 3, 4, 5)),
			make_lightN(2, 
				    make_lightN (3, 6, 7, 8),
				    make_lightN (3, 9, 10, 11)),
			make_lightN(2, 
				    make_lightN (3, 12, 13, 14),
				    make_lightN (3, 15, 16, 17)),
			make_lightN(2, 
				    make_lightN (3, 18, 19, 20),
				    make_lightN (3, 21, 22, 23)),
			make_lightN(2, 
				    make_lightN (3, 0, 1, 2),
				    make_lightN (3, 3, 4, 5)),
			make_lightN(2, 
				    make_lightN (3, 6, 7, 8),
				    make_lightN (3, 9, 10, 11))));

  }
  {
    cout << "Testing join in NNNN" << endl;
    intNNNN a = get_intNNNN (1, 2, 2, 2);
    intNNNN b = get_intNNNN (3, 2, 2, 2);
    intNNNN c = Join (a, b);
    intNNNN d = make_lightN(4, 
			    make_lightN(2,
					make_lightN(2, 
						    make_lightN (2, 0, 1),
						    make_lightN (2, 2, 3)),
					make_lightN(2, 
						    make_lightN (2, 4, 5),
						    make_lightN (2, 6, 7))),
			    make_lightN(2,
					make_lightN(2, 
						    make_lightN (2, 0, 1),
						    make_lightN (2, 2, 3)),
					make_lightN(2, 
						    make_lightN (2, 4, 5),
						    make_lightN (2, 6, 7))),
			    make_lightN(2,
					make_lightN(2, 
						    make_lightN (2, 8, 9),
						    make_lightN (2, 10, 11)),
					make_lightN(2, 
						    make_lightN (2, 12, 13),
						    make_lightN (2, 14, 15))),
			    make_lightN(2,
					make_lightN(2, 
						    make_lightN (2, 16, 17),
						    make_lightN (2, 18, 19)),
					make_lightN(2, 
						    make_lightN (2, 20, 21),
						    make_lightN (2, 22, 23))));

    cout << a << endl << endl;
    cout << b << endl << endl;
    cout << c << endl << endl;
    cout << d << endl << endl;
    
    assert (d==c);
  }

  //
  // Append and prepend
  //

  {
   intN a = make_lightN(5, 1, 2, 3, 4, 5);
   intN c = Append (a, 17);
   cout << c << endl;
   assert (c==make_lightN(6, 1, 2, 3, 4, 5, 17));
   c = Prepend (c, 42);
   cout << c << endl;
   assert (c==make_lightN(7, 42, 1, 2, 3, 4, 5, 17));
  }
  {
    intNN a = get_intNN (3, 2);
    intNN c = Append (a, make_lightN (2, 17, 18));
    cout << c << endl;
    assert (c==make_lightN(4, 
			   make_lightN (2, 0, 1),
			   make_lightN (2, 2, 3),
			   make_lightN (2, 4, 5),
			   make_lightN (2, 17, 18)
			   ));
    c = Prepend (c, make_lightN (2, 42, 43));
    cout << c << endl;
    assert (c==make_lightN(5, 
			   make_lightN (2, 42, 43),
			   make_lightN (2, 0, 1),
			   make_lightN (2, 2, 3),
			   make_lightN (2, 4, 5),
			   make_lightN (2, 17, 18)
			   ));
  }
}

void f11() {
  {
   intN a = make_lightN(5, 1, 2, 3, 4, 5);
   a = Drop (a, R(2,3));
   cout << a << endl;
   assert (a==make_lightN(3, 1, 4, 5));
   a = Drop (a, R(1,1));
   cout << a << endl;
   assert (a==make_lightN(2, 4, 5));
   a = Drop (a, R(2,2));
   cout << a << endl;
   assert (a==make_lightN(1, 4));
  }
  {
    intNN a = get_intNN (7, 2);
    a = Drop (a, R(1,2));
    cout << a << endl;
    assert (a==make_lightN(5, 
			   make_lightN (2, 4, 5),
			   make_lightN (2, 6, 7),
			   make_lightN (2, 8, 9),
			   make_lightN (2, 10, 11),
			   make_lightN (2, 12, 13)
			   ));
    a = Drop (a, R(2,3));
    cout << a << endl;
    assert (a==make_lightN(3, 
			   make_lightN (2, 4, 5),
			   make_lightN (2, 10, 11),
			   make_lightN (2, 12, 13)
			   ));
    a = Drop (a, R(3,3));
    cout << a << endl;
    assert (a==make_lightN(2, 
			   make_lightN (2, 4, 5),
			   make_lightN (2, 10, 11)
			   ));
  }
}

lightNNint get_intNN (int y, int x) {
  lightNNint res(y, x);
  for (int i=0; i<y; i++) {
    for (int j=0; j<x; j++) {
      res(i+1, j+1) = i*x+j;
    }
  }
  return res;
}

lightNNNint get_intNNN (int x, int y, int z) {
  lightNNNint res(x, y, z);
  for (int i=0; i<x; i++) {
    for (int j=0; j<y; j++) {
      for (int k=0; k<z; k++) {
	res(i+1, j+1, k+1) = i*y*z+j*z+k;
      }
    }
  }
  return res;
}

lightNNNNint get_intNNNN (int x, int y, int z, int w) {
  lightNNNNint res(x, y, z, w);
  for (int i=0; i<x; i++) {
    for (int j=0; j<y; j++) {
      for (int k=0; k<z; k++) {
	for (int m=0; m<w; m++) {
	  res(i+1, j+1, k+1, m+1) = i*y*z*w + j*z*w + k*w + m;
	}
      }
    }
  }
  return res;
}


void f12()
{
 lightNdouble p;
 p=lightNdouble(make_lightN(2,3,4));
 assert(p==make_lightN(2,3.0,4.0));
}


void f13()
{
    /*
 lightNdouble p,p1,p2;
 p=lightNdouble(make_lightN(6,1.,2.,3.,4.,5.,6.));
 p1=p(R(1,4));
 p2=p1(R(1,2));
 assert(p2==make_lightN(2,1.,2.));
*/
doubleN UTeil(0);
    doubleN Span(0);
    doubleN Ut14(0);
    int ULang;



    Span.SetShape(6 );
    Span(1) = 1.;
    Span(2) = 2.;
    Span(3) = 3.;
    Span(4) = 4.;
    Span(5) = 5.;
    Span(6) = 6.;

    cout << (Span);
    cout << "\n";
    UTeil = Span(R(1,4));
    cout << "UTeil=";
    cout << (UTeil);
    cout << "\n";
    Ut14 = UTeil(R(1,2));
    cout << "Ut14=";
    cout << (Ut14);
    cout << "\n";
    ULang = Ut14.dimension(1);
    assert(ULang==2);

}

void f14() {
   int i=2;
   intN in=make_lightN(4, 5,6,7,8);

   lm_complexN c(4);
   lm_complexN c2(4);
   double ddd=7.77;
   
   doubleN d(4);
   double d0=6;
   c(1)=lm_complex(1,3);
   c(2)=lm_complex(-4,3);
   c(3)=lm_complex(5,-7);
   c(4)=lm_complex(-3,-6);

   d=abs(c);
   doubleN d2=make_lightN(4, 3.16228, 5., 8.60233, 6.7082);
   cout << "d-d2=" << abs(d-d2)<<"\n";
   c2=sqrt(c);
//   c2=pow(c2,2.);
   cout << "abs(c2-c)=" << abs(c2-c)<<"\n";
//   c2=pow(2.,c2);
   c2=pow(d,c2);
   c2=pow(c2,d);
   c2=pow(c2,i);
   c2=pow(c2,in);
   c2=pow(i,c2);
   c2=pow(in,c2);

   intNN inn(2,2,1);
   
   doubleNN dd(2,2);
   lm_complexNN cc(2,2);
   lm_complexNN cc2(2,2);
   dd=1;
   cc=lm_complex(1.,0);
   cc2=pow(dd,cc);
   cc2=pow(i,cc);
   cc2=pow(inn,cc);

   cc2=pow(cc,dd);
   cc2=pow(cc,i);
   cc2=pow(cc,inn);
   
   cc2=pow(2.,cc);
   
   cc2=pow(cc,2.);
  
}


void f15() {
     doubleN d(4);
     d(1)=5;
   d(2)=4;
   d(3)=7;
   d(4)=1;
   doubleN p=light_sort(d);
   cout << "Sorted " << p << endl;
   assert(p(1)==1);
   assert(p(2)==4);
   assert(p(3)==5);
   assert(p(4)==7);

doubleNN y(4,1);
y(1,1)=5;
y(2,1)=4;
y(3,1)=7;
y(4,1)=1;
cout << "y=" << y << endl;
doubleNN z(y);
z=light_sort(y);
cout << "z=" << z << endl;



lm_complexN f(4);
f(1)= lm_complex(1,-3);
f(2)= lm_complex(1,-2);
f(3)= lm_complex(1,-4);
f(4)= lm_complex(2,-3);
   cout << "single f(1)= " << f(1) << endl;
   cout << "single lm_complex(1,-3)= " << lm_complex(1,-3) << endl;

   cout << "UnSorted " << f << endl;
   lm_complexN fs=light_sort(f);
   cout << "Sorted " << fs << endl;
 
}


void f16(){

    intNN in=make_lightN(2,
        make_lightN(4, 5,6,7,8),
        make_lightN(4, 10,11,12,13)
        );
    RWCString stri;
    stri="f16a.csv";
    light_export1("f16a.csv",in,NULL,NULL);
    light_export1(stri,in,NULL,NULL);

    cout<< "light_importNNint(f16a.csv)" <<endl;
    intNN in2=light_importNNint("f16a.csv","csv",NULL,NULL);

        cout<<" in="<< in <<endl;
        cout<<" in2="<< in2 <<endl;
cout<< "light_importNNint done" <<endl;
    assert(in==in2);


lm_complexNN f(2,2);
f(1,2)= lm_complex(0,-8);
f(1,1)= lm_complex(8,0);
f(2,1)= lm_complex(7,-4);
f(2,2)= lm_complex(0,8);
    
    light_export1("f16c.csv",f,NULL,NULL);
    lm_complexNN coin2=light_importNNlm_complex("f16c.csv","csv",NULL,NULL);
    assert(f==coin2);
}

void f17(){
lm_complexN f(4);
f(1)= lm_complex(0,-8);
f(2)= lm_complex(8,0);
f(3)= lm_complex(7,-4);
f(4)= lm_complex(0,8);
    
lm_complex d17=light_variance(f);
cout << "d17=" <<d17 <<endl;
}

int
main()
{
 cerr<<"Library check ..."<<endl;
 f17();
 f16();



 testConversion();
 f1();
 f2();
 f3();
 f4(); f42();
 f5();
 f51();
 f6();
 f7();
 f8();
 f9();
 f10();
 f11();
 f12();
 f13();
 f14();
 f15();
 g1();
 cerr<<"Library check ok."<<endl;
 return 0;
}


/* Multiplications allowed:
   
    3*3  = s
    3*s  = 3
    3*33 = 3
    33*3 = 3
    3*NN = N
    3N*3N=3N
    3N*s = 3N
    s*3N = 3N
    3N*N = 3
    3N*NN = 3N
    33*33 = 33
    33*s  = 33
    s*33  = 33
    33*3  = 3
    3*33  = 3 
    3N*s  = 3N
    s*3N  = 3N
    NN*3N = NN
    N*NN  = N
    N*s   = N
    s*N   = N
    NN*N  = N
    N*N   = s
    3*NN  = N
    NN*3  = N
    N3*3  = N
    
    */ 
