//  Extra Win/dos functions.
#include <math.h>

#if defined(OLDHEADER) || defined(macintosh) || ( __GNUC__  && (__GNUC__ < 4))
  #include <iostream.h>
#else
  #include <ostream>
  #include <iostream>
  #include <fstream>


  using namespace std;
#endif


#include <stdlib.h>



// Some defines for error-reporting
#ifdef _MSC_VER
// Under MathCode/MathLink abort causes complete and unclear stop.
#include "windows.h"
void  lighterror(const char str[],const char file[], int line) {
 if (file) cerr << "LIGHTMAT: " << str << " " <<file<< ":" <<line<< endl;
 else cerr << "LIGHTMAT: " << str << endl;
                                       //  MessageBox(0,"LightMat check failed, task aborted.","LightMat check failed",MB_OK);
                                         DebugBreak();
                                         abort(); }
#else
void  lighterror(const char str[],const char file[],int line) {
 char c;
 if (file)cerr << "LIGHTMAT: " << str << " " <<file<< ":" <<line<< endl;
 else cerr << "LIGHTMAT: " << str << endl;
 cin >> c >> ws;
 abort(); }
#endif



