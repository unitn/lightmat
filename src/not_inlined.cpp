#ifndef NOT_INLINED
#define NOT_INLINED
#endif

#define  LM_ALL

// #define NO_RANGE_CHECKING
// We need range checking for not-inlined version.
// This version is normally used for debugging of lightmat, too.

#include "lightmat.h"
#undef  inline 
#define inline /* */

// All the LightMat code but the "inline" directive 
// is deactivated. 
#include "light_icc.h"

// void dummy_tompe () {
// 
//   lightNdouble a, b;
//   double c;
//   
//   lightNdouble& (*dc1)(const lightNdouble &, double);
//   lightNdouble& (*dc2)(double, const lightNdouble &);
//   lightNdouble& (*dc3)(const lightNdouble &, const lightNdouble &);
// 
//   dc1 (a, c);
//   dc2 (c, b);
//   dc3 (a, b);
// 
// }
