typedef lm_complex tcomplex;
typedef lm_complexN tcomplexN;
typedef lm_complexNN tcomplexNN;
typedef lm_complexNNN tcomplexNNN;
typedef lm_complexNNNN tcomplexNNNN;
void g1(){
    int aint, bint, dint;
    intN aintN, bintN, dintN;
    intNN aintNN, bintNN, dintNN;
    intNNN aintNNN, bintNNN, dintNNN;
    intNNNN aintNNNN, bintNNNN, dintNNNN;
    double adouble, bdouble, ddouble;
    doubleN adoubleN, bdoubleN, ddoubleN;
    doubleNN adoubleNN, bdoubleNN, ddoubleNN;
    doubleNNN adoubleNNN, bdoubleNNN, ddoubleNNN;
    doubleNNNN adoubleNNNN, bdoubleNNNN, ddoubleNNNN;
    tcomplex atcomplex, btcomplex, dtcomplex;
    tcomplexN atcomplexN, btcomplexN, dtcomplexN;
    tcomplexNN atcomplexNN, btcomplexNN, dtcomplexNN;
    tcomplexNNN atcomplexNNN, btcomplexNNN, dtcomplexNNN;
    tcomplexNNNN atcomplexNNNN, btcomplexNNNN, dtcomplexNNNN;
    {  aint=(int)2;bint=(int)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( aint, bint);assert(ctcomplex==dtcomplex);}
    {  aint=(int)2;bdouble=(double)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( aint, bdouble);assert(ctcomplex==dtcomplex);}
    {  aint=(int)2;btcomplex=(tcomplex)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( aint, btcomplex);assert(ctcomplex==dtcomplex);}
    {  adouble=(double)2;bint=(int)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( adouble, bint);assert(ctcomplex==dtcomplex);}
    {  adouble=(double)2;bdouble=(double)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( adouble, bdouble);assert(ctcomplex==dtcomplex);}
    {  adouble=(double)2;btcomplex=(tcomplex)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( adouble, btcomplex);assert(ctcomplex==dtcomplex);}
    {  atcomplex=(tcomplex)2;bint=(int)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( atcomplex, bint);assert(ctcomplex==dtcomplex);}
    {  atcomplex=(tcomplex)2;bdouble=(double)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( atcomplex, bdouble);assert(ctcomplex==dtcomplex);}
    {  atcomplex=(tcomplex)2;btcomplex=(tcomplex)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( atcomplex, btcomplex);assert(ctcomplex==dtcomplex);}
    {  aintN=(int)2;bintN=(int)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( aintN, bintN);assert(ctcomplexN==dtcomplexN);}
    {  aintN=(int)2;bdoubleN=(double)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( aintN, bdoubleN);assert(ctcomplexN==dtcomplexN);}
    {  aintN=(int)2;btcomplexN=(tcomplex)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( aintN, btcomplexN);assert(ctcomplexN==dtcomplexN);}
    {  adoubleN=(double)2;bintN=(int)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( adoubleN, bintN);assert(ctcomplexN==dtcomplexN);}
    {  adoubleN=(double)2;bdoubleN=(double)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( adoubleN, bdoubleN);assert(ctcomplexN==dtcomplexN);}
    {  adoubleN=(double)2;btcomplexN=(tcomplex)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( adoubleN, btcomplexN);assert(ctcomplexN==dtcomplexN);}
    {  atcomplexN=(tcomplex)2;bintN=(int)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( atcomplexN, bintN);assert(ctcomplexN==dtcomplexN);}
    {  atcomplexN=(tcomplex)2;bdoubleN=(double)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( atcomplexN, bdoubleN);assert(ctcomplexN==dtcomplexN);}
    {  atcomplexN=(tcomplex)2;btcomplexN=(tcomplex)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( atcomplexN, btcomplexN);assert(ctcomplexN==dtcomplexN);}
    {  aintNN=(int)2;bintNN=(int)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( aintNN, bintNN);assert(ctcomplexNN==dtcomplexNN);}
    {  aintNN=(int)2;bdoubleNN=(double)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( aintNN, bdoubleNN);assert(ctcomplexNN==dtcomplexNN);}
    {  aintNN=(int)2;btcomplexNN=(tcomplex)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( aintNN, btcomplexNN);assert(ctcomplexNN==dtcomplexNN);}
    {  adoubleNN=(double)2;bintNN=(int)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( adoubleNN, bintNN);assert(ctcomplexNN==dtcomplexNN);}
    {  adoubleNN=(double)2;bdoubleNN=(double)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( adoubleNN, bdoubleNN);assert(ctcomplexNN==dtcomplexNN);}
    {  adoubleNN=(double)2;btcomplexNN=(tcomplex)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( adoubleNN, btcomplexNN);assert(ctcomplexNN==dtcomplexNN);}
    {  atcomplexNN=(tcomplex)2;bintNN=(int)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( atcomplexNN, bintNN);assert(ctcomplexNN==dtcomplexNN);}
    {  atcomplexNN=(tcomplex)2;bdoubleNN=(double)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( atcomplexNN, bdoubleNN);assert(ctcomplexNN==dtcomplexNN);}
    {  atcomplexNN=(tcomplex)2;btcomplexNN=(tcomplex)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( atcomplexNN, btcomplexNN);assert(ctcomplexNN==dtcomplexNN);}
    {  aintNNN=(int)2;bintNNN=(int)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( aintNNN, bintNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    {  aintNNN=(int)2;bdoubleNNN=(double)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( aintNNN, bdoubleNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    {  aintNNN=(int)2;btcomplexNNN=(tcomplex)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( aintNNN, btcomplexNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    {  adoubleNNN=(double)2;bintNNN=(int)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( adoubleNNN, bintNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    {  adoubleNNN=(double)2;bdoubleNNN=(double)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( adoubleNNN, bdoubleNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    {  adoubleNNN=(double)2;btcomplexNNN=(tcomplex)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( adoubleNNN, btcomplexNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    {  atcomplexNNN=(tcomplex)2;bintNNN=(int)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( atcomplexNNN, bintNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    {  atcomplexNNN=(tcomplex)2;bdoubleNNN=(double)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( atcomplexNNN, bdoubleNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    {  atcomplexNNN=(tcomplex)2;btcomplexNNN=(tcomplex)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( atcomplexNNN, btcomplexNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    {  aintNNNN=(int)2;bintNNNN=(int)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( aintNNNN, bintNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    {  aintNNNN=(int)2;bdoubleNNNN=(double)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( aintNNNN, bdoubleNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    {  aintNNNN=(int)2;btcomplexNNNN=(tcomplex)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( aintNNNN, btcomplexNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    {  adoubleNNNN=(double)2;bintNNNN=(int)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( adoubleNNNN, bintNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    {  adoubleNNNN=(double)2;bdoubleNNNN=(double)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( adoubleNNNN, bdoubleNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    {  adoubleNNNN=(double)2;btcomplexNNNN=(tcomplex)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( adoubleNNNN, btcomplexNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    {  atcomplexNNNN=(tcomplex)2;bintNNNN=(int)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( atcomplexNNNN, bintNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    {  atcomplexNNNN=(tcomplex)2;bdoubleNNNN=(double)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( atcomplexNNNN, bdoubleNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    {  atcomplexNNNN=(tcomplex)2;btcomplexNNNN=(tcomplex)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( atcomplexNNNN, btcomplexNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}

    { aint=(int)2;bint=(int)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( aint, bint);assert(ctcomplex==dtcomplex);}
    { aint=(int)2;bint=(int)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( aint, bint);assert(ctcomplex==dtcomplex);}
    { adouble=(double)2;bdouble=(double)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( adouble, bdouble);assert(ctcomplex==dtcomplex);}
    { adouble=(double)2;bdouble=(double)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( adouble, bdouble);assert(ctcomplex==dtcomplex);}
    { atcomplex=(tcomplex)2;btcomplex=(tcomplex)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( atcomplex, btcomplex);assert(ctcomplex==dtcomplex);}
    { atcomplex=(tcomplex)2;btcomplex=(tcomplex)3;dtcomplex=(tcomplex)8;tcomplex ctcomplex=pow( atcomplex, btcomplex);assert(ctcomplex==dtcomplex);}
    { aintN=(int)2;bint=(int)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( aintN, bint);assert(ctcomplexN==dtcomplexN);}
    { aint=(int)2;bintN=(int)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( aint, bintN);assert(ctcomplexN==dtcomplexN);}
    { adoubleN=(double)2;bdouble=(double)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( adoubleN, bdouble);assert(ctcomplexN==dtcomplexN);}
    { adouble=(double)2;bdoubleN=(double)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( adouble, bdoubleN);assert(ctcomplexN==dtcomplexN);}
    { atcomplexN=(tcomplex)2;btcomplex=(tcomplex)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( atcomplexN, btcomplex);assert(ctcomplexN==dtcomplexN);}
    { atcomplex=(tcomplex)2;btcomplexN=(tcomplex)3;dtcomplexN=(tcomplex)8;tcomplexN ctcomplexN=pow( atcomplex, btcomplexN);assert(ctcomplexN==dtcomplexN);}
    { aintNN=(int)2;bint=(int)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( aintNN, bint);assert(ctcomplexNN==dtcomplexNN);}
    { aint=(int)2;bintNN=(int)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( aint, bintNN);assert(ctcomplexNN==dtcomplexNN);}
    { adoubleNN=(double)2;bdouble=(double)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( adoubleNN, bdouble);assert(ctcomplexNN==dtcomplexNN);}
    { adouble=(double)2;bdoubleNN=(double)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( adouble, bdoubleNN);assert(ctcomplexNN==dtcomplexNN);}
    { atcomplexNN=(tcomplex)2;btcomplex=(tcomplex)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( atcomplexNN, btcomplex);assert(ctcomplexNN==dtcomplexNN);}
    { atcomplex=(tcomplex)2;btcomplexNN=(tcomplex)3;dtcomplexNN=(tcomplex)8;tcomplexNN ctcomplexNN=pow( atcomplex, btcomplexNN);assert(ctcomplexNN==dtcomplexNN);}
    { aintNNN=(int)2;bint=(int)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( aintNNN, bint);assert(ctcomplexNNN==dtcomplexNNN);}
    { aint=(int)2;bintNNN=(int)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( aint, bintNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    { adoubleNNN=(double)2;bdouble=(double)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( adoubleNNN, bdouble);assert(ctcomplexNNN==dtcomplexNNN);}
    { adouble=(double)2;bdoubleNNN=(double)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( adouble, bdoubleNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    { atcomplexNNN=(tcomplex)2;btcomplex=(tcomplex)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( atcomplexNNN, btcomplex);assert(ctcomplexNNN==dtcomplexNNN);}
    { atcomplex=(tcomplex)2;btcomplexNNN=(tcomplex)3;dtcomplexNNN=(tcomplex)8;tcomplexNNN ctcomplexNNN=pow( atcomplex, btcomplexNNN);assert(ctcomplexNNN==dtcomplexNNN);}
    { aintNNNN=(int)2;bint=(int)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( aintNNNN, bint);assert(ctcomplexNNNN==dtcomplexNNNN);}
    { aint=(int)2;bintNNNN=(int)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( aint, bintNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    { adoubleNNNN=(double)2;bdouble=(double)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( adoubleNNNN, bdouble);assert(ctcomplexNNNN==dtcomplexNNNN);}
    { adouble=(double)2;bdoubleNNNN=(double)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( adouble, bdoubleNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
    { atcomplexNNNN=(tcomplex)2;btcomplex=(tcomplex)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( atcomplexNNNN, btcomplex);assert(ctcomplexNNNN==dtcomplexNNNN);}
    { atcomplex=(tcomplex)2;btcomplexNNNN=(tcomplex)3;dtcomplexNNNN=(tcomplex)8;tcomplexNNNN ctcomplexNNNN=pow( atcomplex, btcomplexNNNN);assert(ctcomplexNNNN==dtcomplexNNNN);}
};
