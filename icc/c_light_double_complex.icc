//
// sqrt
//

inline light3lm_complex
sqrt(const light3lm_complex& s)
{
    return light3lm_complex(sqrt(s.elem[0]), sqrt(s.elem[1]), sqrt(s.elem[2]));
}


inline light4lm_complex
sqrt(const light4lm_complex& s)
{
    return light4lm_complex(sqrt(s.elem[0]), sqrt(s.elem[1]), sqrt(s.elem[2]),
			  sqrt(s.elem[3]));
}


inline light33lm_complex
sqrt(const light33lm_complex& s)
{
    return light33lm_complex(sqrt(s.elem[0]), sqrt(s.elem[3]), sqrt(s.elem[6]),
			   sqrt(s.elem[1]), sqrt(s.elem[4]), sqrt(s.elem[7]),
			   sqrt(s.elem[2]), sqrt(s.elem[5]), sqrt(s.elem[8]));
}


inline light44lm_complex
sqrt(const light44lm_complex& s)
{
    return light44lm_complex(sqrt(s.elem[0]), sqrt(s.elem[4]), sqrt(s.elem[8]),
			   sqrt(s.elem[12]),
			   sqrt(s.elem[1]), sqrt(s.elem[5]), sqrt(s.elem[9]),
			   sqrt(s.elem[13]),
			   sqrt(s.elem[2]), sqrt(s.elem[6]), sqrt(s.elem[10]),
			   sqrt(s.elem[14]),
			   sqrt(s.elem[3]), sqrt(s.elem[7]), sqrt(s.elem[11]),
			   sqrt(s.elem[15]));
}

inline lightNlm_complex
sqrt(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sqrt_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
sqrt(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sqrt_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
sqrt(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = sqrt_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
sqrt(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sqrt_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
sqrt(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sqrt_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// exp
//

inline light3lm_complex
exp(const light3lm_complex& s)
{
    return light3lm_complex(exp(s.elem[0]), exp(s.elem[1]), exp(s.elem[2]));
}


inline light4lm_complex
exp(const light4lm_complex& s)
{
    return light4lm_complex(exp(s.elem[0]), exp(s.elem[1]), exp(s.elem[2]),
			  exp(s.elem[3]));
}


inline light33lm_complex
exp(const light33lm_complex& s)
{
    return light33lm_complex(exp(s.elem[0]), exp(s.elem[3]), exp(s.elem[6]),
			   exp(s.elem[1]), exp(s.elem[4]), exp(s.elem[7]),
			   exp(s.elem[2]), exp(s.elem[5]), exp(s.elem[8]));
}


inline light44lm_complex
exp(const light44lm_complex& s)
{
    return light44lm_complex(exp(s.elem[0]), exp(s.elem[4]), exp(s.elem[8]),
			   exp(s.elem[12]),
			   exp(s.elem[1]), exp(s.elem[5]), exp(s.elem[9]),
			   exp(s.elem[13]),
			   exp(s.elem[2]), exp(s.elem[6]), exp(s.elem[10]),
			   exp(s.elem[14]),
			   exp(s.elem[3]), exp(s.elem[7]), exp(s.elem[11]),
			   exp(s.elem[15]));
}


inline lightNlm_complex
exp(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = exp_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
exp(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = exp_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
exp(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = exp_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
exp(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = exp_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
exp(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = exp_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// log
//


inline light3lm_complex
log(const light3lm_complex& s)
{
    return light3lm_complex(log(s.elem[0]), log(s.elem[1]), log(s.elem[2]));
}


inline light4lm_complex
log(const light4lm_complex& s)
{
    return light4lm_complex(log(s.elem[0]), log(s.elem[1]), log(s.elem[2]),
			  log(s.elem[3]));
}


inline light33lm_complex
log(const light33lm_complex& s)
{
    return light33lm_complex(log(s.elem[0]), log(s.elem[3]), log(s.elem[6]),
			   log(s.elem[1]), log(s.elem[4]), log(s.elem[7]),
			   log(s.elem[2]), log(s.elem[5]), log(s.elem[8]));
}


inline light44lm_complex
log(const light44lm_complex& s)
{
    return light44lm_complex(log(s.elem[0]), log(s.elem[4]), log(s.elem[8]),
			   log(s.elem[12]),
			   log(s.elem[1]), log(s.elem[5]), log(s.elem[9]),
			   log(s.elem[13]),
			   log(s.elem[2]), log(s.elem[6]), log(s.elem[10]),
			   log(s.elem[14]),
			   log(s.elem[3]), log(s.elem[7]), log(s.elem[11]),
			   log(s.elem[15]));
}


inline lightNlm_complex
log(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = log_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
log(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = log_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
log(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = log_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
log(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = log_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
log(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = log_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// sin
//


inline light3lm_complex
sin(const light3lm_complex& s)
{
    return light3lm_complex(sin(s.elem[0]), sin(s.elem[1]), sin(s.elem[2]));
}


inline light4lm_complex
sin(const light4lm_complex& s)
{
    return light4lm_complex(sin(s.elem[0]), sin(s.elem[1]), sin(s.elem[2]),
			  sin(s.elem[3]));
}


inline light33lm_complex
sin(const light33lm_complex& s)
{
    return light33lm_complex(sin(s.elem[0]), sin(s.elem[3]), sin(s.elem[6]),
			   sin(s.elem[1]), sin(s.elem[4]), sin(s.elem[7]),
			   sin(s.elem[2]), sin(s.elem[5]), sin(s.elem[8]));
}


inline light44lm_complex
sin(const light44lm_complex& s)
{
    return light44lm_complex(sin(s.elem[0]), sin(s.elem[4]), sin(s.elem[8]),
			   sin(s.elem[12]),
			   sin(s.elem[1]), sin(s.elem[5]), sin(s.elem[9]),
			   sin(s.elem[13]),
			   sin(s.elem[2]), sin(s.elem[6]), sin(s.elem[10]),
			   sin(s.elem[14]),
			   sin(s.elem[3]), sin(s.elem[7]), sin(s.elem[11]),
			   sin(s.elem[15]));
}


inline lightNlm_complex
sin(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sin_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
sin(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sin_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
sin(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = sin_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
sin(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sin_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
sin(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sin_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// cos
//


inline light3lm_complex
cos(const light3lm_complex& s)
{
    return light3lm_complex(cos(s.elem[0]), cos(s.elem[1]), cos(s.elem[2]));
}


inline light4lm_complex
cos(const light4lm_complex& s)
{
    return light4lm_complex(cos(s.elem[0]), cos(s.elem[1]), cos(s.elem[2]),
			  cos(s.elem[3]));
}


inline light33lm_complex
cos(const light33lm_complex& s)
{
    return light33lm_complex(cos(s.elem[0]), cos(s.elem[3]), cos(s.elem[6]),
			   cos(s.elem[1]), cos(s.elem[4]), cos(s.elem[7]),
			   cos(s.elem[2]), cos(s.elem[5]), cos(s.elem[8]));
}


inline light44lm_complex
cos(const light44lm_complex& s)
{
    return light44lm_complex(cos(s.elem[0]), cos(s.elem[4]), cos(s.elem[8]),
			   cos(s.elem[12]),
			   cos(s.elem[1]), cos(s.elem[5]), cos(s.elem[9]),
			   cos(s.elem[13]),
			   cos(s.elem[2]), cos(s.elem[6]), cos(s.elem[10]),
			   cos(s.elem[14]),
			   cos(s.elem[3]), cos(s.elem[7]), cos(s.elem[11]),
			   cos(s.elem[15]));
}


inline lightNlm_complex
cos(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = cos_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
cos(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = cos_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
cos(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = cos_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
cos(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = cos_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
cos(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = cos_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// tan
//


inline light3lm_complex
tan(const light3lm_complex& s)
{
    return light3lm_complex(tan(s.elem[0]), tan(s.elem[1]), tan(s.elem[2]));
}


inline light4lm_complex
tan(const light4lm_complex& s)
{
    return light4lm_complex(tan(s.elem[0]), tan(s.elem[1]), tan(s.elem[2]),
			  tan(s.elem[3]));
}


inline light33lm_complex
tan(const light33lm_complex& s)
{
    return light33lm_complex(tan(s.elem[0]), tan(s.elem[3]), tan(s.elem[6]),
			   tan(s.elem[1]), tan(s.elem[4]), tan(s.elem[7]),
			   tan(s.elem[2]), tan(s.elem[5]), tan(s.elem[8]));
}


inline light44lm_complex
tan(const light44lm_complex& s)
{
    return light44lm_complex(tan(s.elem[0]), tan(s.elem[4]), tan(s.elem[8]),
			   tan(s.elem[12]),
			   tan(s.elem[1]), tan(s.elem[5]), tan(s.elem[9]),
			   tan(s.elem[13]),
			   tan(s.elem[2]), tan(s.elem[6]), tan(s.elem[10]),
			   tan(s.elem[14]),
			   tan(s.elem[3]), tan(s.elem[7]), tan(s.elem[11]),
			   tan(s.elem[15]));
}


inline lightNlm_complex
tan(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = tan_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
tan(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = tan_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
tan(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = tan_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
tan(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = tan_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
tan(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = tan_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// asin
//


inline light3lm_complex
asin(const light3lm_complex& s)
{
    return light3lm_complex(asin(s.elem[0]), asin(s.elem[1]), asin(s.elem[2]));
}


inline light4lm_complex
asin(const light4lm_complex& s)
{
    return light4lm_complex(asin(s.elem[0]), asin(s.elem[1]), asin(s.elem[2]),
			  asin(s.elem[3]));
}


inline light33lm_complex
asin(const light33lm_complex& s)
{
    return light33lm_complex(asin(s.elem[0]), asin(s.elem[3]), asin(s.elem[6]),
			   asin(s.elem[1]), asin(s.elem[4]), asin(s.elem[7]),
			   asin(s.elem[2]), asin(s.elem[5]), asin(s.elem[8]));
}


inline light44lm_complex
asin(const light44lm_complex& s)
{
    return light44lm_complex(asin(s.elem[0]), asin(s.elem[4]), asin(s.elem[8]),
			   asin(s.elem[12]),
			   asin(s.elem[1]), asin(s.elem[5]), asin(s.elem[9]),
			   asin(s.elem[13]),
			   asin(s.elem[2]), asin(s.elem[6]), asin(s.elem[10]),
			   asin(s.elem[14]),
			   asin(s.elem[3]), asin(s.elem[7]), asin(s.elem[11]),
			   asin(s.elem[15]));
}


inline lightNlm_complex
asin(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = asin_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
asin(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = asin_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
asin(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = asin_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
asin(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = asin_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
asin(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = asin_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// acos
//


inline light3lm_complex
acos(const light3lm_complex& s)
{
    return light3lm_complex(acos(s.elem[0]), acos(s.elem[1]), acos(s.elem[2]));
}


inline light4lm_complex
acos(const light4lm_complex& s)
{
    return light4lm_complex(acos(s.elem[0]), acos(s.elem[1]), acos(s.elem[2]),
			  acos(s.elem[3]));
}


inline light33lm_complex
acos(const light33lm_complex& s)
{
    return light33lm_complex(acos(s.elem[0]), acos(s.elem[3]), acos(s.elem[6]),
			   acos(s.elem[1]), acos(s.elem[4]), acos(s.elem[7]),
			   acos(s.elem[2]), acos(s.elem[5]), acos(s.elem[8]));
}


inline light44lm_complex
acos(const light44lm_complex& s)
{
    return light44lm_complex(acos(s.elem[0]), acos(s.elem[4]), acos(s.elem[8]),
			   acos(s.elem[12]),
			   acos(s.elem[1]), acos(s.elem[5]), acos(s.elem[9]),
			   acos(s.elem[13]),
			   acos(s.elem[2]), acos(s.elem[6]), acos(s.elem[10]),
			   acos(s.elem[14]),
			   acos(s.elem[3]), acos(s.elem[7]), acos(s.elem[11]),
			   acos(s.elem[15]));
}


inline lightNlm_complex
acos(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = acos_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
acos(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = acos_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
acos(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = acos_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
acos(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = acos_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
acos(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = acos_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// atan
//


inline light3lm_complex
atan(const light3lm_complex& s)
{
    return light3lm_complex(atan(s.elem[0]), atan(s.elem[1]), atan(s.elem[2]));
}


inline light4lm_complex
atan(const light4lm_complex& s)
{
    return light4lm_complex(atan(s.elem[0]), atan(s.elem[1]), atan(s.elem[2]),
			  atan(s.elem[3]));
}


inline light33lm_complex
atan(const light33lm_complex& s)
{
    return light33lm_complex(atan(s.elem[0]), atan(s.elem[3]), atan(s.elem[6]),
			   atan(s.elem[1]), atan(s.elem[4]), atan(s.elem[7]),
			   atan(s.elem[2]), atan(s.elem[5]), atan(s.elem[8]));
}


inline light44lm_complex
atan(const light44lm_complex& s)
{
    return light44lm_complex(atan(s.elem[0]), atan(s.elem[4]), atan(s.elem[8]),
			   atan(s.elem[12]),
			   atan(s.elem[1]), atan(s.elem[5]), atan(s.elem[9]),
			   atan(s.elem[13]),
			   atan(s.elem[2]), atan(s.elem[6]), atan(s.elem[10]),
			   atan(s.elem[14]),
			   atan(s.elem[3]), atan(s.elem[7]), atan(s.elem[11]),
			   atan(s.elem[15]));
}


inline lightNlm_complex
atan(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = atan_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
atan(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = atan_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
atan(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = atan_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
atan(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = atan_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
atan(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = atan_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// sinh
//


inline light3lm_complex
sinh(const light3lm_complex& s)
{
    return light3lm_complex(sinh(s.elem[0]), sinh(s.elem[1]), sinh(s.elem[2]));
}


inline light4lm_complex
sinh(const light4lm_complex& s)
{
    return light4lm_complex(sinh(s.elem[0]), sinh(s.elem[1]), sinh(s.elem[2]),
			  sinh(s.elem[3]));
}


inline light33lm_complex
sinh(const light33lm_complex& s)
{
    return light33lm_complex(sinh(s.elem[0]), sinh(s.elem[3]), sinh(s.elem[6]),
			   sinh(s.elem[1]), sinh(s.elem[4]), sinh(s.elem[7]),
			   sinh(s.elem[2]), sinh(s.elem[5]), sinh(s.elem[8]));
}


inline light44lm_complex
sinh(const light44lm_complex& s)
{
    return light44lm_complex(sinh(s.elem[0]), sinh(s.elem[4]), sinh(s.elem[8]),
			   sinh(s.elem[12]),
			   sinh(s.elem[1]), sinh(s.elem[5]), sinh(s.elem[9]),
			   sinh(s.elem[13]),
			   sinh(s.elem[2]), sinh(s.elem[6]), sinh(s.elem[10]),
			   sinh(s.elem[14]),
			   sinh(s.elem[3]), sinh(s.elem[7]), sinh(s.elem[11]),
			   sinh(s.elem[15]));
}


inline lightNlm_complex
sinh(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sinh_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
sinh(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sinh_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
sinh(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = sinh_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
sinh(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sinh_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
sinh(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = sinh_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// cosh
//


inline light3lm_complex
cosh(const light3lm_complex& s)
{
    return light3lm_complex(cosh(s.elem[0]), cosh(s.elem[1]), cosh(s.elem[2]));
}


inline light4lm_complex
cosh(const light4lm_complex& s)
{
    return light4lm_complex(cosh(s.elem[0]), cosh(s.elem[1]), cosh(s.elem[2]),
			  cosh(s.elem[3]));
}


inline light33lm_complex
cosh(const light33lm_complex& s)
{
    return light33lm_complex(cosh(s.elem[0]), cosh(s.elem[3]), cosh(s.elem[6]),
			   cosh(s.elem[1]), cosh(s.elem[4]), cosh(s.elem[7]),
			   cosh(s.elem[2]), cosh(s.elem[5]), cosh(s.elem[8]));
}


inline light44lm_complex
cosh(const light44lm_complex& s)
{
    return light44lm_complex(cosh(s.elem[0]), cosh(s.elem[4]), cosh(s.elem[8]),
			   cosh(s.elem[12]),
			   cosh(s.elem[1]), cosh(s.elem[5]), cosh(s.elem[9]),
			   cosh(s.elem[13]),
			   cosh(s.elem[2]), cosh(s.elem[6]), cosh(s.elem[10]),
			   cosh(s.elem[14]),
			   cosh(s.elem[3]), cosh(s.elem[7]), cosh(s.elem[11]),
			   cosh(s.elem[15]));
}


inline lightNlm_complex
cosh(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = cosh_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
cosh(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = cosh_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
cosh(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = cosh_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
cosh(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = cosh_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
cosh(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = cosh_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// tanh
//


inline light3lm_complex
tanh(const light3lm_complex& s)
{
    return light3lm_complex(tanh(s.elem[0]), tanh(s.elem[1]), tanh(s.elem[2]));
}


inline light4lm_complex
tanh(const light4lm_complex& s)
{
    return light4lm_complex(tanh(s.elem[0]), tanh(s.elem[1]), tanh(s.elem[2]),
			  tanh(s.elem[3]));
}


inline light33lm_complex
tanh(const light33lm_complex& s)
{
    return light33lm_complex(tanh(s.elem[0]), tanh(s.elem[3]), tanh(s.elem[6]),
			   tanh(s.elem[1]), tanh(s.elem[4]), tanh(s.elem[7]),
			   tanh(s.elem[2]), tanh(s.elem[5]), tanh(s.elem[8]));
}


inline light44lm_complex
tanh(const light44lm_complex& s)
{
    return light44lm_complex(tanh(s.elem[0]), tanh(s.elem[4]), tanh(s.elem[8]),
			   tanh(s.elem[12]),
			   tanh(s.elem[1]), tanh(s.elem[5]), tanh(s.elem[9]),
			   tanh(s.elem[13]),
			   tanh(s.elem[2]), tanh(s.elem[6]), tanh(s.elem[10]),
			   tanh(s.elem[14]),
			   tanh(s.elem[3]), tanh(s.elem[7]), tanh(s.elem[11]),
			   tanh(s.elem[15]));
}


inline lightNlm_complex
tanh(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = tanh_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
tanh(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = tanh_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
tanh(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = tanh_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
tanh(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = tanh_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
tanh(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = tanh_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// asinh
//


inline light3lm_complex
asinh(const light3lm_complex& s)
{
    return light3lm_complex(asinh(s.elem[0]), asinh(s.elem[1]),
			  asinh(s.elem[2]));
}


inline light4lm_complex
asinh(const light4lm_complex& s)
{
    return light4lm_complex(asinh(s.elem[0]), asinh(s.elem[1]), asinh(s.elem[2]),
			  asinh(s.elem[3]));
}


inline light33lm_complex
asinh(const light33lm_complex& s)
{
    return light33lm_complex(asinh(s.elem[0]), asinh(s.elem[3]),
			   asinh(s.elem[6]),
			   asinh(s.elem[1]), asinh(s.elem[4]),
			   asinh(s.elem[7]),
			   asinh(s.elem[2]), asinh(s.elem[5]),
			   asinh(s.elem[8]));
}


inline light44lm_complex
asinh(const light44lm_complex& s)
{
    return light44lm_complex(asinh(s.elem[0]), asinh(s.elem[4]),
			   asinh(s.elem[8]), asinh(s.elem[12]),
			   asinh(s.elem[1]), asinh(s.elem[5]),
			   asinh(s.elem[9]), asinh(s.elem[13]),
			   asinh(s.elem[2]), asinh(s.elem[6]),
			   asinh(s.elem[10]), asinh(s.elem[14]),
			   asinh(s.elem[3]), asinh(s.elem[7]),
			   asinh(s.elem[11]), asinh(s.elem[15]));
}


inline lightNlm_complex
asinh(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = asinh_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
asinh(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = asinh_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
asinh(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = asinh_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
asinh(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = asinh_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
asinh(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = asinh_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// acosh
//


inline light3lm_complex
acosh(const light3lm_complex& s)
{
    return light3lm_complex(acosh(s.elem[0]), acosh(s.elem[1]),
			  acosh(s.elem[2]));
}


inline light4lm_complex
acosh(const light4lm_complex& s)
{
    return light4lm_complex(acosh(s.elem[0]), acosh(s.elem[1]), acosh(s.elem[2]),
			  acosh(s.elem[3]));
}


inline light33lm_complex
acosh(const light33lm_complex& s)
{
    return light33lm_complex(acosh(s.elem[0]), acosh(s.elem[3]),
			   acosh(s.elem[6]),
			   acosh(s.elem[1]), acosh(s.elem[4]),
			   acosh(s.elem[7]),
			   acosh(s.elem[2]), acosh(s.elem[5]),
			   acosh(s.elem[8]));
}


inline light44lm_complex
acosh(const light44lm_complex& s)
{
    return light44lm_complex(acosh(s.elem[0]), acosh(s.elem[4]),
			   acosh(s.elem[8]), acosh(s.elem[12]),
			   acosh(s.elem[1]), acosh(s.elem[5]),
			   acosh(s.elem[9]), acosh(s.elem[13]),
			   acosh(s.elem[2]), acosh(s.elem[6]),
			   acosh(s.elem[10]), acosh(s.elem[14]),
			   acosh(s.elem[3]), acosh(s.elem[7]),
			   acosh(s.elem[11]), acosh(s.elem[15]));
}


inline lightNlm_complex
acosh(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = acosh_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
acosh(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = acosh_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
acosh(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = acosh_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
acosh(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = acosh_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
acosh(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = acosh_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}

//
// atanh
//


inline light3lm_complex
atanh(const light3lm_complex& s)
{
    return light3lm_complex(atanh(s.elem[0]), atanh(s.elem[1]),
			  atanh(s.elem[2]));
}


inline light4lm_complex
atanh(const light4lm_complex& s)
{
    return light4lm_complex(atanh(s.elem[0]), atanh(s.elem[1]), atanh(s.elem[2]),
			  atanh(s.elem[3]));
}


inline light33lm_complex
atanh(const light33lm_complex& s)
{
    return light33lm_complex(atanh(s.elem[0]), atanh(s.elem[3]),
			   atanh(s.elem[6]),
			   atanh(s.elem[1]), atanh(s.elem[4]),
			   atanh(s.elem[7]),
			   atanh(s.elem[2]), atanh(s.elem[5]),
			   atanh(s.elem[8]));
}


inline light44lm_complex
atanh(const light44lm_complex& s)
{
    return light44lm_complex(atanh(s.elem[0]), atanh(s.elem[4]),
			   atanh(s.elem[8]), atanh(s.elem[12]),
			   atanh(s.elem[1]), atanh(s.elem[5]),
			   atanh(s.elem[9]), atanh(s.elem[13]),
			   atanh(s.elem[2]), atanh(s.elem[6]),
			   atanh(s.elem[10]), atanh(s.elem[14]),
			   atanh(s.elem[3]), atanh(s.elem[7]),
			   atanh(s.elem[11]), atanh(s.elem[15]));
}


inline lightNlm_complex
atanh(const lightNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = atanh_wrap_lm_complex;
    return lightNlm_complex(s, f, lightmat_apply);
}


inline lightNNlm_complex
atanh(const lightNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = atanh_wrap_lm_complex;
    return lightNNlm_complex(s, f, lightmat_apply);
}


inline lightN3lm_complex
atanh(const lightN3lm_complex& s)
{
    lm_complex (*f)(lm_complex) = atanh_wrap_lm_complex;
    return lightN3lm_complex(s, f, lightmat_apply);
}


inline lightNNNlm_complex
atanh(const lightNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = atanh_wrap_lm_complex;
    return lightNNNlm_complex(s, f, lightmat_apply);
}


inline lightNNNNlm_complex
atanh(const lightNNNNlm_complex& s)
{
    lm_complex (*f)(lm_complex) = atanh_wrap_lm_complex;
    return lightNNNNlm_complex(s, f, lightmat_apply);
}
