//           -*- c++ -*-

//
// routines which use BLAS
//
#ifndef LIGHTMAT_DONT_USE_BLAS
extern "C"
{
    double ddot_(const int *n, const double *dx, const int *incx,
		 const double *dy, const int *incy);
    void dgemm_(const char *transa, const char *transb, const int *m,
		const int *n, const int *k, const double *alpha,
		const double *a, const int *lda, const double *b,
		const int *ldb, const double *beta, double *c,
		const int *ldc);
    void dgemv_(const char *trans, const int *m, const int *n,
		const double *alpha, const double *a, const int *lda,
		const double *x, const int *incx, const double *beta,
		double *y, const int *incy);
}

// These functions are overloaded variants of the ones in light_basic.icc

inline double
light_dot(const int n, const double *x, const double *y)
{
    const int i1 = 1;

    return ddot_(&n, x, &i1, y, &i1);
}


inline double
light_dot(const int n, const double *x, const int ix, const double *y)
{
    const int i1 = 1;

    return ddot_(&n, x, &ix, y, &i1);
}


inline void
light_gemv(const int m, const int n, const double *a, const double *x,
	   double *y)
{
    const char cN = 'N';
    const double d0 = 0.0;
    const double d1 = 1.0;
    const int i1 = 1;

    dgemv_(&cN, &m, &n, &d1, a, &m, x, &i1, &d0, y, &i1);
}


inline void
light_gevm(const int m, const int n, const double *x, const double *a,
	   double *y, const int iy = 1)
{
    // V * M == Transpose(M) * V
    const char cT = 'T';
    const double d0 = 0.0;
    const double d1 = 1.0;
    const int i1 = 1;

    dgemv_(&cT, &m, &n, &d1, a, &m, x, &i1, &d0, y, &iy);
}


inline void
light_gemm(const int m, const int n, const int k,
	   const double *a, const double *b, double *c)
{
    const char cN = 'N';
    const double d0 = 0.0;
    const double d1 = 1.0;

    dgemm_(&cN, &cN, &m, &n, &k, &d1, a, &m, b, &k, &d0, c, &m);
}
#endif // LIGHTMAT_DONT_USE_BLAS

// *************************************************************************************
// FractionalPart

inline 
double FractionalPart(const double s)
{
  return s-(int)s;
}

inline 
double FractionalPart(const int )
{
  return 0;
}



inline lightNdouble
FractionalPart(const  lightNdouble & s)
{
  return s-IntegerPart(s);
}
 

inline lightNint
FractionalPart(const  lightNint & s)
{
  lightNint x=s;  x=0;  return x; 
}


inline lightNNdouble
FractionalPart(const  lightNNdouble & s)
{
  return s-IntegerPart(s);
}
 

inline lightNNint
FractionalPart(const  lightNNint & s)
{
  lightNNint x=s;  x=0;  return x; 
}


inline lightNNNdouble
FractionalPart(const  lightNNNdouble & s)
{
  return s-IntegerPart(s);
}
 

inline lightNNNint
FractionalPart(const  lightNNNint & s)
{
  lightNNNint x=s;  x=0;  return x; 
}

inline lightNNNNdouble
FractionalPart(const  lightNNNNdouble & s)
{
  return s-IntegerPart(s);
}
 

inline lightNNNNint
FractionalPart(const  lightNNNNint & s)
{
  lightNNNNint x=s;  x=0;  return x; 
}





// IntegerPart

inline double IntegerPart(const double s)
{
   return (int)s;
}

inline int IntegerPart(const int s)
{
   return s;
}

inline lightNdouble
IntegerPart(const  lightNdouble & s)
{
  lightNdouble res(s.size1, lightmat_dont_zero);
  double *em = s.elem;
   double *re = res.elem;
   
   for(int i=0; i<s.size1; i++)
      re[i] = (int)(em[i]);

    return res;
}

inline lightNNdouble
IntegerPart(const  lightNNdouble & s)
{
  lightNNdouble res(s.size1,s.size2, lightmat_dont_zero);
  double *em = s.elem;
   double *re = res.elem;
   
   for(int i=0; i<s.size1*s.size2; i++)
      re[i] = (int)(em[i]);

    return res;
}

inline lightNNNdouble
IntegerPart(const  lightNNNdouble & s)
{
  lightNNNdouble res(s.size1,s.size2,s.size3, lightmat_dont_zero);
  double *em = s.elem;
   double *re = res.elem;
   
   for(int i=0; i<s.size1*s.size2*s.size3 ; i++)
      re[i] = (int)(em[i]);

    return res;
}

inline lightNNNNdouble
IntegerPart(const  lightNNNNdouble & s)
{
  lightNNNNdouble res(s.size1,s.size2,s.size3,s.size4, lightmat_dont_zero);
  double *em = s.elem;
   double *re = res.elem;
   
   for(int i=0; i<s.size1*s.size2*s.size3*s.size4 ; i++)
      re[i] = (int)(em[i]);

    return res;
}

inline lightNint
IntegerPart(const  lightNint & s)
{
  return s;
}

inline lightNNint
IntegerPart(const  lightNNint & s)
{
  return s;
}

inline lightNNNint
IntegerPart(const  lightNNNint & s)
{
  return s;
}

inline lightNNNNint
IntegerPart(const  lightNNNNint & s)
{
  return s;
}



// The same for all other types....
// This FractionalPartBase - see in beginning.



//
// abs
//



inline lightNdouble
abs(const lightNdouble& s)
{
    return lightNdouble(s, lightmat_abs);
}



inline lightN3double
abs(const lightN3double& s)
{
    return lightN3double(s, lightmat_abs);
}



inline lightNNdouble
abs(const lightNNdouble& s)
{
    return lightNNdouble(s, lightmat_abs);
}



inline lightNNNdouble
abs(const lightNNNdouble& s)
{
    return lightNNNdouble(s, lightmat_abs);
}



inline lightNNNNdouble
abs(const lightNNNNdouble& s)
{
    return lightNNNNdouble(s, lightmat_abs);
}


// **************************************************************************************
// Mod (the functions valid for double only
/////////////////////////////////////////////////


inline int Mod(const int m ,const int n)
{
if (m>=0)  {
   if (n>0) 
     return m%n;
   else
   if (n<0)
     return m%n+n; // Use Mathematica's definition
   else
     { 
   lighterror("Bad 2nd argument to Mod");
   return 0;
     }
  }
else {
  if (n>0)
    return m%n+n; // Use Mathematica's definition
  else if (n<0)
    return m%n;
  else {
    lighterror("Bad 2nd argument to Mod");
    return 0;
 }
}
}

inline double Mod(const double m,const double n)
{
 if (n==0) lighterror("Bad 2nd argument to Mod");
  return m-n*floor(m/n);
}

inline double Mod(const double m,const int n)
{return Mod(m,(double)n);}

inline double Mod(const int m,const double n)
{return Mod((double)m,n);}

/*
lightNdouble  Mod(const lightNint & i,const lightNdouble &  d)
{ return Mod(i,d);}

lightNdouble  Mod(const lightNdouble &  i,const lightNint & d)
{ return Mod(i,d);}

lightNNdouble  Mod(const lightNNint & i,const lightNNdouble &  d)
{ return Mod(i,d);}

lightNNdouble  Mod(const lightNNdouble &  i,const lightNNint & d)
{ return Mod(i,d);}


lightNNNdouble  Mod(const lightNNNdouble &  i,const lightNNNint & d)
{ return Mod(i,d);}

lightNNNdouble  Mod(const lightNNNint & i,const lightNNNdouble &  d)
{ return Mod(i,d);}



lightNNNNdouble  Mod(const lightNNNNdouble &  i,const lightNNNNint & d)
{ return Mod(i,d);}

lightNNNNdouble  Mod(const lightNNNNint & i,const lightNNNNdouble &  d)
{ return Mod(i,d);}

*/
