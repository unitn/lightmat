//           -*- c++ -*-


#define IN_LIGHT3lm_complex_ICC


inline
light3lm_complex::light3lm_complex()
{
#ifdef LIGHTMAT_INIT_ZERO
	elem[0] = 0; elem[1] = 0; elem[2] = 0;
#endif
	size1 = 3;
}



inline
light3lm_complex::light3lm_complex(const light3lm_complex& s)
{
    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];
    size1 = 3;
}



inline
light3lm_complex::light3lm_complex(const lm_complex e1, const lm_complex e2, const lm_complex e3)
{
    elem[0] = e1; elem[1] = e2; elem[2] = e3;
    size1 = 3;
}



inline
light3lm_complex::light3lm_complex(const lm_complex *data)
{
    elem[0] = data[0]; elem[1] = data[1]; elem[2] = data[2];
    size1 = 3;
}



inline
light3lm_complex::light3lm_complex(const lm_complex e)
{
    elem[0] = e; elem[1] = e; elem[2] = e;
    size1 = 3;
}




inline
light3lm_complex::light3lm_complex(const lightmat_dont_zero_enum)
{
    size1 = 3;
}

#ifdef CONV_INT_2_DOUBLE


inline
light3lm_complex::operator light3double()
{

    return light3double(elem[0], elem[1], elem[2]);

}


#endif


inline light3lm_complex&
light3lm_complex::operator=(const light3lm_complex& s)
{
    if(this == &s)
	return *this;

    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];

    return *this;
}



inline light3lm_complex&
light3lm_complex::operator=(const lightNlm_complex& s)
{
    limiterror(s.size1 != 3);

    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];

    return *this;
}



inline light3lm_complex&
light3lm_complex::operator=(const lm_complex e)
{
    elem[0] = e; elem[1] = e; elem[2] = e;

    return *this;
}


// 
// inline lm_complex
// light3lm_complex::operator()(const int x) const
// {
//     limiterror((x<1) || (x>3));
//     return elem[x-1];
// }
 


// 
// inline lm_complex&
// light3lm_complex::operator()(const int x) 
// {
//     limiterror((x<1) || (x>3));
//     return elem[x-1];
// }
 




inline int
light3lm_complex::operator==(const light3lm_complex& s) const
{
    return (elem[0] == s.elem[0]) &&
	   (elem[1] == s.elem[1]) &&
	   (elem[2] == s.elem[2]);
}



inline int
light3lm_complex::operator!=(const light3lm_complex& s) const
{
    return (elem[0] != s.elem[0]) ||
	   (elem[1] != s.elem[1]) ||
	   (elem[2] != s.elem[2]);
}




inline light3lm_complex&
light3lm_complex::operator+=(const lm_complex e)
{
    elem[0] += e; elem[1] += e; elem[2] += e;

    return *this;
}



inline light3lm_complex&
light3lm_complex::operator+=(const light3lm_complex& s)
{
    elem[0] += s.elem[0]; elem[1] += s.elem[1]; elem[2] += s.elem[2];

    return *this;
}



inline light3lm_complex&
light3lm_complex::operator-=(const lm_complex e)
{
    elem[0] -= e; elem[1] -= e; elem[2] -= e;

    return *this;
}



inline light3lm_complex&
light3lm_complex::operator-=(const light3lm_complex& s)
{
    elem[0] -= s.elem[0]; elem[1] -= s.elem[1]; elem[2] -= s.elem[2];

    return *this;
}



inline light3lm_complex&
light3lm_complex::operator*=(const lm_complex e)
{
    elem[0] *= e; elem[1] *= e; elem[2] *= e;

    return *this;
}



inline light3lm_complex&
light3lm_complex::operator/=(const lm_complex e)
{
    *this *= 1/e;

    return *this;
}

#ifndef COMPLEX_TOOLS

inline double
light3lm_complex::length() const
{
    return sqrt(double(elem[0]*elem[0] + elem[1]*elem[1] + elem[2]*elem[2]));
}
#endif

// 
// inline int
// light3lm_complex::dimension(const int x) const
// {
//     if(x == 1)
// 	   return 3;
//     else
// 	   return 1;
// }
 
#ifndef COMPLEX_TOOLS

inline light3lm_complex&
light3lm_complex::normalize()
{
  //    double len = length();
  // For some obscure reason in template noatation SparcCompiler
  // cannot call length() here.
    double len = sqrt(double(elem[0]*elem[0] + elem[1]*elem[1] + elem[2]*elem[2]));
    if(len == 0)
	lighterror("Can't normalize 0-length vector");

    *this *= lm_complex(1/len);

    return *this;
}
#endif


inline void
light3lm_complex::Get(lm_complex *data) const
{
    data[0] = elem[0]; data[1] = elem[1]; data[2] = elem[2];
}



inline void
light3lm_complex::Set(const lm_complex *data)
{
    elem[0] = data[0]; elem[1] = data[1]; elem[2] = data[2];
}


inline void
light3lm_complex::Get( lm_complex& d0, lm_complex& d1 , lm_complex& d2 ) const
{
    d0 = elem[0]; d1 = elem[1]; d2 = elem[2];
}



inline void
light3lm_complex::Set(const lm_complex d0,const lm_complex d1,const lm_complex d2)
{
    elem[0] = d0; elem[1] = d1; elem[2] = d2;
}






inline light3lm_complex
light3lm_complex::operator+() const
{
    return *this;
}



inline light3lm_complex
light3lm_complex::operator-() const
{
    return light3lm_complex(-elem[0], -elem[1], -elem[2]);
}


//
// **** friends ****
//



inline  light3lm_complex
operator+(const light3lm_complex& s1, const light3lm_complex& s2)
{    
    return light3lm_complex(s1.elem[0] + s2.elem[0],
		     s1.elem[1] + s2.elem[1],
		     s1.elem[2] + s2.elem[2]);
}



inline light3lm_complex
operator+(const light3lm_complex& s1, const lm_complex e)
{
    return light3lm_complex(s1.elem[0] + e,
		     s1.elem[1] + e,
		     s1.elem[2] + e);
}



inline light3lm_complex
operator+(const lm_complex e, const light3lm_complex& s1)
{
    return s1 + e;
}



inline light3lm_complex
operator-(const light3lm_complex& s1, const light3lm_complex& s2)
{
    return light3lm_complex(s1.elem[0] - s2.elem[0],
		     s1.elem[1] - s2.elem[1],
		     s1.elem[2] - s2.elem[2]);
}



inline light3lm_complex
operator-(const light3lm_complex& s1, const lm_complex e)
{
    return light3lm_complex(s1.elem[0] - e,
		     s1.elem[1] - e,
		     s1.elem[2] - e);
}



inline light3lm_complex
operator-(const lm_complex e, const light3lm_complex& s1)
{
    return light3lm_complex(e - s1.elem[0],
		     e - s1.elem[1],
		     e - s1.elem[2]);
}



inline lm_complex
operator*(const light3lm_complex& s1, const light3lm_complex& s2)
{
    return s1.elem[0]*s2.elem[0] +
	   s1.elem[1]*s2.elem[1] +
	   s1.elem[2]*s2.elem[2];
}



inline lm_complex
Dot(const light3lm_complex& s1, const light3lm_complex& s2)
{
    return s1*s2;
}





inline light3lm_complex
operator*(const light3lm_complex& s1, const lm_complex e)
{
    return light3lm_complex(s1.elem[0] * e,
		     s1.elem[1] * e,
		     s1.elem[2] * e);
}



inline light3lm_complex
operator*(const lm_complex e, const light3lm_complex& s1)
{
    return s1 * e;
}



inline light3lm_complex
operator*(const light3lm_complex& s1, const light33lm_complex& s2)
{
#ifdef ROWMAJOR
    return light3lm_complex(s1.elem[0] * s2.elem[0] +
		     s1.elem[1] * s2.elem[3] +
		     s1.elem[2] * s2.elem[6],
		     s1.elem[0] * s2.elem[1] +
		     s1.elem[1] * s2.elem[4] +
		     s1.elem[2] * s2.elem[7],
		     s1.elem[0] * s2.elem[2] +
		     s1.elem[1] * s2.elem[5] +
		     s1.elem[2] * s2.elem[8]);
#else
    return light3lm_complex(s1.elem[0] * s2.elem[0] +
		     s1.elem[1] * s2.elem[1] +
		     s1.elem[2] * s2.elem[2],
		     s1.elem[0] * s2.elem[3] +
		     s1.elem[1] * s2.elem[4] +
		     s1.elem[2] * s2.elem[5],
		     s1.elem[0] * s2.elem[6] +
		     s1.elem[1] * s2.elem[7] +
		     s1.elem[2] * s2.elem[8]);
#endif
}



inline light3lm_complex
Dot(const light3lm_complex& s1, const light33lm_complex& s2)
{ 
 return s1*s2;
}


inline light3lm_complex
operator/(const light3lm_complex& s1, const lm_complex e)
{
    lm_complex n = 1/e;
    return s1 * n;
}



inline light3lm_complex
operator/(const lm_complex e, const light3lm_complex& s1)
{
    return light3lm_complex(e / s1.elem[0],
		     e / s1.elem[1],
		     e / s1.elem[2]);
}



inline light3lm_complex
pow(const light3lm_complex& s1, const light3lm_complex& s2)
{
    return light3lm_complex(lm_complex(light_pow(s1.elem[0], s2.elem[0])),
		     lm_complex(light_pow(s1.elem[1], s2.elem[1])),
		     lm_complex(light_pow(s1.elem[2], s2.elem[2])));
}



inline light3lm_complex
pow(const light3lm_complex& s, const lm_complex e)
{
    return light3lm_complex(lm_complex(light_pow(s.elem[0], e)),
		     lm_complex(light_pow(s.elem[1], e)),
		     lm_complex(light_pow(s.elem[2], e)));
}



inline light3lm_complex
pow(const lm_complex e, const light3lm_complex& s)
{
    return light3lm_complex(lm_complex(light_pow(e, s.elem[0])),
		     lm_complex(light_pow(e, s.elem[1])),
		     lm_complex(light_pow(e, s.elem[2])));
}



inline light3lm_complex
abs(const light3lm_complex& s)
{
    return light3lm_complex(LIGHTABS(s.elem[0]),
		     LIGHTABS(s.elem[1]),
		     LIGHTABS(s.elem[2]));
}



inline light3lm_complex
ElemProduct(const light3lm_complex& s1, const light3lm_complex& s2)
{
    return light3lm_complex(s1.elem[0] * s2.elem[0],
		     s1.elem[1] * s2.elem[1],
		     s1.elem[2] * s2.elem[2]);
}



inline light3lm_complex
ElemQuotient(const light3lm_complex& s1, const light3lm_complex& s2)
{
    return light3lm_complex(s1.elem[0] / s2.elem[0],
		     s1.elem[1] / s2.elem[1],
		     s1.elem[2] / s2.elem[2]);
}



inline light3lm_complex
Apply(const light3lm_complex& s, lm_complex f(lm_complex))
{
    return light3lm_complex(f(s.elem[0]), f(s.elem[1]), f(s.elem[2]));
}



inline light3lm_complex
Apply(const light3lm_complex& s1, const light3lm_complex& s2, lm_complex f(lm_complex, lm_complex))
{
    return light3lm_complex(f(s1.elem[0], s2.elem[0]),
		     f(s1.elem[1], s2.elem[1]),
		     f(s1.elem[2], s2.elem[2]));
}



inline light3lm_complex
Cross(const light3lm_complex& s1, const light3lm_complex& s2)
{
    return light3lm_complex(s1.elem[1]*s2.elem[2] - s2.elem[1]*s1.elem[2],
		     s1.elem[2]*s2.elem[0] - s2.elem[2]*s1.elem[0],
		     s1.elem[0]*s2.elem[1] - s2.elem[0]*s1.elem[1]);
}



inline light33lm_complex
OuterProduct(const light3lm_complex& s1, const light3lm_complex& s2)
{
    return light33lm_complex(s1.elem[0]*s2.elem[0], s1.elem[0]*s2.elem[1],
		      s1.elem[0]*s2.elem[2],
		      s1.elem[1]*s2.elem[0], s1.elem[1]*s2.elem[1],
		      s1.elem[1]*s2.elem[2],
		      s1.elem[2]*s2.elem[0], s1.elem[2]*s2.elem[1],
		      s1.elem[2]*s2.elem[2]);
}

#include "c_light3_auto.icc"

#undef IN_LIGHT3lm_complex_ICC
