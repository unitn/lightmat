//           -*- c++ -*-

//
// ifloor
//


inline light3int
ifloor(const light3double& s)
{
    return light3int(int(floor(s.elem[0])), int(floor(s.elem[1])),
		       int(floor(s.elem[2])));
}

inline light3lm_complex
ifloor(const light3lm_complex& s)
{
    return light3lm_complex(lm_complex(floor(s.elem[0].re),floor(s.elem[0].im )), 
							  lm_complex(floor(s.elem[1].re),floor(s.elem[1].im )),
		                      lm_complex(floor(s.elem[2].re),floor(s.elem[2].im)));
}

inline light4int
ifloor(const light4double& s)
{
    return light4int(int(floor(s.elem[0])), int(floor(s.elem[1])),
		       int(floor(s.elem[2])), int(floor(s.elem[3])));
}

inline light4lm_complex
ifloor(const light4lm_complex& s)
{
    return light4lm_complex(lm_complex(floor(s.elem[0].re),floor(s.elem[0].im )), 
							  lm_complex(floor(s.elem[1].re),floor(s.elem[1].im )),
		                      lm_complex(floor(s.elem[2].re),floor(s.elem[2].im )),
		                      lm_complex(floor(s.elem[3].re),floor(s.elem[3].im )));
}

inline light33int
ifloor(const light33double& s)
{
    return light33int(int(floor(s.elem[0])), int(floor(s.elem[3])),
			int(floor(s.elem[6])),
			int(floor(s.elem[1])), int(floor(s.elem[4])),
			int(floor(s.elem[7])),
			int(floor(s.elem[2])), int(floor(s.elem[5])),
			int(floor(s.elem[8])));
}

inline light33lm_complex
ifloor(const light33lm_complex& s)
{
    return light33lm_complex(lm_complex(floor(s.elem[0].re),floor(s.elem[0].im )), 
							  lm_complex(floor(s.elem[3].re),floor(s.elem[3].im )),
		                      lm_complex(floor(s.elem[6].re),floor(s.elem[6].im )),
		                      lm_complex(floor(s.elem[1].re),floor(s.elem[1].im )),
		                      lm_complex(floor(s.elem[4].re),floor(s.elem[4].im )),
		                      lm_complex(floor(s.elem[7].re),floor(s.elem[7].im )),
		                      lm_complex(floor(s.elem[2].re),floor(s.elem[2].im )),
		                      lm_complex(floor(s.elem[5].re),floor(s.elem[5].im )),
		                      lm_complex(floor(s.elem[8].re),floor(s.elem[8].im ))
		                      );
}

inline light44int
ifloor(const light44double& s)
{
    return light44int(int(floor(s.elem[0])), int(floor(s.elem[4])),
			int(floor(s.elem[8])), int(floor(s.elem[12])),
			int(floor(s.elem[1])), int(floor(s.elem[5])),
			int(floor(s.elem[9])), int(floor(s.elem[13])),
			int(floor(s.elem[2])), int(floor(s.elem[6])),
			int(floor(s.elem[10])), int(floor(s.elem[14])),
			int(floor(s.elem[3])), int(floor(s.elem[7])),
			int(floor(s.elem[11])), int(floor(s.elem[15])));
}

inline light44lm_complex
ifloor(const light44lm_complex& s)
{
    return light44lm_complex(lm_complex(floor(s.elem[0].re),floor(s.elem[0].im )), 
							  lm_complex(floor(s.elem[4].re),floor(s.elem[4].im )),
		                      lm_complex(floor(s.elem[8].re),floor(s.elem[8].im )),
		                      lm_complex(floor(s.elem[12].re),floor(s.elem[12].im )),
		                      lm_complex(floor(s.elem[1].re),floor(s.elem[1].im )),
		                      lm_complex(floor(s.elem[5].re),floor(s.elem[5].im )),
		                      lm_complex(floor(s.elem[9].re),floor(s.elem[9].im )),
		                      lm_complex(floor(s.elem[13].re),floor(s.elem[13].im )),
		                      lm_complex(floor(s.elem[2].re),floor(s.elem[2].im )),
		                      lm_complex(floor(s.elem[6].re),floor(s.elem[6].im )),
		                      lm_complex(floor(s.elem[10].re),floor(s.elem[10].im )),
		                      lm_complex(floor(s.elem[14].re),floor(s.elem[14].im )),
		                      lm_complex(floor(s.elem[3].re),floor(s.elem[3].im )),
		                      lm_complex(floor(s.elem[7].re),floor(s.elem[7].im )),
		                      lm_complex(floor(s.elem[11].re),floor(s.elem[11].im )),
		                      lm_complex(floor(s.elem[15].re),floor(s.elem[15].im ))
		                      );
}

inline lightNint
ifloor(const lightNdouble& s)
{
    lightNint res(s.size1, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;

    for(int i=0; i<s.size1; i++)
	re[i] = int(floor(em[i]));

    return res;
}

inline lightNlm_complex
ifloor(const lightNlm_complex& s)
{
    lightNlm_complex res(s.size1, lightmat_dont_zero);	
    for(int i=0; i<s.size1; i++)
    {
		res.elem[i] = ifloor(s.elem[i]);
    }
    return res;
}
inline lightN3int
ifloor(const lightN3double& s)
{
    lightN3int res(s.size, lightmat_dont_zero);

    light3double *em = s.elem;
    light3int *re = res.elem;

    for(int i=0; i<s.size; i++)
    {
	re[i].elem[0] = int(floor(em[i].elem[0]));
	re[i].elem[1] = int(floor(em[i].elem[1]));
	re[i].elem[2] = int(floor(em[i].elem[2]));
    }

    return res;
}

inline lightN3lm_complex
ifloor(const lightN3lm_complex& s)
{
    lightN3lm_complex res(s.size, lightmat_dont_zero);

    for(int i=1; i<=s.size; i++)
    {
		res(i,0) = ifloor(s(i,0));
		res(i,1) = ifloor(s(i,1));
		res(i,2) = ifloor(s(i,2));
    }

    return res;
}

inline lightNNint
ifloor(const lightNNdouble& s)
{
    lightNNint res(s.size1, s.size2, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;
    const int size = s.size1*s.size2;

    for(int i=0; i<size; i++)
	re[i] = int(floor(em[i]));

    return res;
}

inline lightNNlm_complex
ifloor(const lightNNlm_complex& s)
{
    lightNNlm_complex res(s.size1, s.size2, lightmat_dont_zero);

    const int size = s.size1*s.size2;
 
    for(int i=0; i<size; i++){
		  res.elem[i]= ifloor(s.elem[i]);
    }
	
    return res;
}

inline lightNNNint
ifloor(const lightNNNdouble& s)
{
    lightNNNint res(s.size1, s.size2, s.size3, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;
    const int size = s.size1*s.size2*s.size3;

    for(int i=0; i<size; i++)
	re[i] = int(floor(em[i]));

    return res;
}

inline lightNNNlm_complex
ifloor(const lightNNNlm_complex& s)
{
    lightNNNlm_complex res(s.size1, s.size2, s.size3, lightmat_dont_zero);

    const int size = s.size1*s.size2*s.size3;

    for(int i=0; i<size; i++){
		res.elem[i] = ifloor(s.elem[i]);
	}
    return res;
}

inline lightNNNNint
ifloor(const lightNNNNdouble& s)
{
    lightNNNNint res(s.size1, s.size2, s.size3, s.size4, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;
    const int size = s.size1*s.size2*s.size3*s.size4;

    for(int i=0; i<size; i++)
	re[i] = int(floor(em[i]));

    return res;
}

inline lightNNNNlm_complex
ifloor(const lightNNNNlm_complex& s)
{
    lightNNNNlm_complex res(s.size1, s.size2, s.size3, s.size4, lightmat_dont_zero);

    const int size = s.size1*s.size2*s.size3*s.size4;

    for(int i=0; i<size; i++){
		res.elem[i] = ifloor(s.elem[i]);
	}
    return res;
}

//
// iceil
//


inline light3int
iceil(const light3double& s)
{
    return light3int(int(ceil(s.elem[0])), int(ceil(s.elem[1])),
		       int(ceil(s.elem[2])));
}

inline light3lm_complex
iceil(const light3lm_complex& s)
{
    return light3lm_complex(lm_complex(ceil(s.elem[0].re), ceil(s.elem[0].im)), 
							  lm_complex(ceil(s.elem[1].re), ceil(s.elem[1].im)),
		                      lm_complex(ceil(s.elem[2].re), ceil(s.elem[2].im)));
}

inline light4int
iceil(const light4double& s)
{
    return light4int(int(ceil(s.elem[0])), int(ceil(s.elem[1])),
		       int(ceil(s.elem[2])), int(ceil(s.elem[3])));
}

inline light4lm_complex
iceil(const light4lm_complex& s)
{
    return light4lm_complex(lm_complex(ceil(s.elem[0].re), ceil(s.elem[0].im)), 
							  lm_complex(ceil(s.elem[1].re), ceil(s.elem[1].im)),
		                      lm_complex(ceil(s.elem[2].re), ceil(s.elem[2].im)),
		                      lm_complex(ceil(s.elem[3].re), ceil(s.elem[3].im))
		                      );
}

inline light33int
iceil(const light33double& s)
{
    return light33int(int(ceil(s.elem[0])), int(ceil(s.elem[3])),
			int(ceil(s.elem[6])),
			int(ceil(s.elem[1])), int(ceil(s.elem[4])),
			int(ceil(s.elem[7])),
			int(ceil(s.elem[2])), int(ceil(s.elem[5])),
			int(ceil(s.elem[8])));
}

inline light33lm_complex
iceil(const light33lm_complex& s)
{
    return light33lm_complex(lm_complex(ceil(s.elem[0].re), ceil(s.elem[0].im)), 
							  lm_complex(ceil(s.elem[3].re), ceil(s.elem[3].im)),
		                      lm_complex(ceil(s.elem[6].re), ceil(s.elem[6].im)),
		                      lm_complex(ceil(s.elem[1].re), ceil(s.elem[1].im)),
		                      lm_complex(ceil(s.elem[4].re), ceil(s.elem[4].im)),
		                      lm_complex(ceil(s.elem[7].re), ceil(s.elem[7].im)),
		                      lm_complex(ceil(s.elem[2].re), ceil(s.elem[2].im)),
		                      lm_complex(ceil(s.elem[5].re), ceil(s.elem[5].im)),
		                      lm_complex(ceil(s.elem[8].re), ceil(s.elem[8].im))		                     
		                      );
}

inline light44int
iceil(const light44double& s)
{
    return light44int(int(ceil(s.elem[0])), int(ceil(s.elem[4])),
			int(ceil(s.elem[8])), int(ceil(s.elem[12])),
			int(ceil(s.elem[1])), int(ceil(s.elem[5])),
			int(ceil(s.elem[9])), int(ceil(s.elem[13])),
			int(ceil(s.elem[2])), int(ceil(s.elem[6])),
			int(ceil(s.elem[10])), int(ceil(s.elem[14])),
			int(ceil(s.elem[3])), int(ceil(s.elem[7])),
			int(ceil(s.elem[11])), int(ceil(s.elem[15])));
}

inline light44lm_complex
iceil(const light44lm_complex& s)
{
    return light44lm_complex(lm_complex(ceil(s.elem[0].re), ceil(s.elem[0].im)), 
							  lm_complex(ceil(s.elem[4].re), ceil(s.elem[4].im)),
		                      lm_complex(ceil(s.elem[8].re), ceil(s.elem[8].im)),
		                      lm_complex(ceil(s.elem[12].re), ceil(s.elem[12].im)),
		                      lm_complex(ceil(s.elem[1].re), ceil(s.elem[1].im)),
		                      lm_complex(ceil(s.elem[5].re), ceil(s.elem[5].im)),
		                      lm_complex(ceil(s.elem[9].re), ceil(s.elem[9].im)),
		                      lm_complex(ceil(s.elem[13].re), ceil(s.elem[13].im)),
		                      lm_complex(ceil(s.elem[2].re), ceil(s.elem[2].im)), 
		                      lm_complex(ceil(s.elem[6].re), ceil(s.elem[6].im)),
		                      lm_complex(ceil(s.elem[10].re), ceil(s.elem[10].im)),
		                      lm_complex(ceil(s.elem[14].re), ceil(s.elem[14].im)),
		                      lm_complex(ceil(s.elem[3].re), ceil(s.elem[3].im)),
		                      lm_complex(ceil(s.elem[7].re), ceil(s.elem[7].im)),
		                      lm_complex(ceil(s.elem[11].re), ceil(s.elem[11].im)),
		                      lm_complex(ceil(s.elem[15].re), ceil(s.elem[15].im))		                     
		                      );
}

inline lightNint
iceil(const lightNdouble& s)
{
    lightNint res(s.size1, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;

    for(int i=0; i<s.size1; i++)
	re[i] = int(ceil(em[i]));

    return res;
}

inline lightNlm_complex
iceil(const lightNlm_complex& s)
{
    lightNlm_complex res(s.size1, lightmat_dont_zero);

    for(int i=1; i<=s.size1; i++)
		res(i) = iceil(s(i));
    return res;
}

inline lightN3int
iceil(const lightN3double& s)
{
    lightN3int res(s.size, lightmat_dont_zero);

    light3double *em = s.elem;
    light3int *re = res.elem;

    for(int i=0; i<s.size; i++)
    {
	re[i].elem[0] = int(ceil(em[i].elem[0]));
	re[i].elem[1] = int(ceil(em[i].elem[1]));
	re[i].elem[2] = int(ceil(em[i].elem[2]));
    }

    return res;
}

inline lightN3lm_complex
iceil(const lightN3lm_complex& s)
{
    lightN3lm_complex res(s.size, lightmat_dont_zero);

    for(int i=1; i<=s.size; i++)
    {
		res(i,0) = iceil(s(i,0));
		res(i,1) = iceil(s(i,1));
		res(i,2) = iceil(s(i,2));
    }

    return res;
}

inline lightNNint
iceil(const lightNNdouble& s)
{
    lightNNint res(s.size1, s.size2, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;
    const int size = s.size1*s.size2;

    for(int i=0; i<size; i++)
	re[i] = int(ceil(em[i]));

    return res;
}

inline lightNNlm_complex
iceil(const lightNNlm_complex& s)
{
    lightNNlm_complex res(s.size1, s.size2, lightmat_dont_zero);

    const int size = s.size1*s.size2;

    for(int i=0; i<size; i++){
		res.elem[i] = iceil(s.elem[i]);
	}
    return res;
}


inline lightNNNint
iceil(const lightNNNdouble& s)
{
    lightNNNint res(s.size1, s.size2, s.size3, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;
    const int size = s.size1*s.size2*s.size3;

    for(int i=0; i<size; i++)
	re[i] = int(ceil(em[i]));

    return res;
}

inline lightNNNlm_complex
iceil(const lightNNNlm_complex& s)
{
    lightNNNlm_complex res(s.size1, s.size2, s.size3, lightmat_dont_zero);

    const int size = s.size1*s.size2*s.size3;

    for(int i=0; i<size; i++){
		res.elem[i] = iceil(s.elem[i]);
	}
    return res;
}

inline lightNNNNint
iceil(const lightNNNNdouble& s)
{
    lightNNNNint res(s.size1, s.size2, s.size3, s.size4, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;
    const int size = s.size1*s.size2*s.size3*s.size4;

    for(int i=0; i<size; i++)
	re[i] = int(ceil(em[i]));

    return res;
}

inline lightNNNNlm_complex
iceil(const lightNNNNlm_complex& s)
{
    lightNNNNlm_complex res(s.size1, s.size2, s.size3, s.size4, lightmat_dont_zero);

    const int size = s.size1*s.size2*s.size3*s.size4;

    for(int i=0; i<size; i++){
		res.elem[i] = iceil(s.elem[i]);
    }
    return res;
}

//
// irint (to avoid confusion with rint that returns a double)
//


inline light3int
irint(const light3double& s)
{
    return light3int(int(rint(s.elem[0])), int(rint(s.elem[1])),
		       int(rint(s.elem[2])));
}

inline light3lm_complex
irint(const light3lm_complex& s)
{
    return light3lm_complex(lm_complex(rint(s.elem[0].re),rint(s.elem[0].im )), 
                              lm_complex(rint(s.elem[1].re),rint(s.elem[1].im)),
		                      lm_complex(rint(s.elem[2].re),rint(s.elem[2].im)));
}

inline light4int
irint(const light4double& s)
{
    return light4int(int(rint(s.elem[0])), int(rint(s.elem[1])),
		       int(rint(s.elem[2])), int(rint(s.elem[3])));
}

inline light4lm_complex
irint(const light4lm_complex& s)
{
    return light4lm_complex(lm_complex(rint(s.elem[0].re),rint(s.elem[0].im )), 
                              lm_complex(rint(s.elem[1].re),rint(s.elem[1].im)),
		                      lm_complex(rint(s.elem[2].re),rint(s.elem[2].im)),
		                      lm_complex(rint(s.elem[3].re),rint(s.elem[3].im))
		                      );
}

inline light33int
irint(const light33double& s)
{
    return light33int(int(rint(s.elem[0])), int(rint(s.elem[3])),
			int(rint(s.elem[6])),
			int(rint(s.elem[1])), int(rint(s.elem[4])),
			int(rint(s.elem[7])),
			int(rint(s.elem[2])), int(rint(s.elem[5])),
			int(rint(s.elem[8])));
}

inline light33lm_complex
irint(const light33lm_complex& s)
{
    return light33lm_complex(lm_complex(rint(s.elem[0].re),rint(s.elem[0].im )), 
                              lm_complex(rint(s.elem[3].re),rint(s.elem[3].im)),
		                      lm_complex(rint(s.elem[6].re),rint(s.elem[6].im)),
		                      lm_complex(rint(s.elem[1].re),rint(s.elem[1].im)),
		                      lm_complex(rint(s.elem[4].re),rint(s.elem[4].im)),
		                      lm_complex(rint(s.elem[7].re),rint(s.elem[7].im)),
		                      lm_complex(rint(s.elem[2].re),rint(s.elem[2].im)),
		                      lm_complex(rint(s.elem[5].re),rint(s.elem[5].im)),
		                      lm_complex(rint(s.elem[8].re),rint(s.elem[8].im))
		                      );
}

inline light44int
irint(const light44double& s)
{
    return light44int(int(rint(s.elem[0])), int(rint(s.elem[4])),
			int(rint(s.elem[8])), int(rint(s.elem[12])),
			int(rint(s.elem[1])), int(rint(s.elem[5])),
			int(rint(s.elem[9])), int(rint(s.elem[13])),
			int(rint(s.elem[2])), int(rint(s.elem[6])),
			int(rint(s.elem[10])), int(rint(s.elem[14])),
			int(rint(s.elem[3])), int(rint(s.elem[7])),
			int(rint(s.elem[11])), int(rint(s.elem[15])));
}

inline light44lm_complex
irint(const light44lm_complex& s)
{
    return light44lm_complex(lm_complex(rint(s.elem[0].re),rint(s.elem[0].im )), 
                              lm_complex(rint(s.elem[4].re),rint(s.elem[4].im)),
		                      lm_complex(rint(s.elem[8].re),rint(s.elem[8].im)),
		                      lm_complex(rint(s.elem[12].re),rint(s.elem[12].im)),
		                      lm_complex(rint(s.elem[1].re),rint(s.elem[1].im)),
		                      lm_complex(rint(s.elem[5].re),rint(s.elem[5].im)),
		                      lm_complex(rint(s.elem[9].re),rint(s.elem[9].im)),
		                      lm_complex(rint(s.elem[13].re),rint(s.elem[13].im)),
		                      lm_complex(rint(s.elem[2].re),rint(s.elem[2].im)),
		                      lm_complex(rint(s.elem[6].re),rint(s.elem[6].im)),
		                      lm_complex(rint(s.elem[10].re),rint(s.elem[10].im)),
		                      lm_complex(rint(s.elem[14].re),rint(s.elem[14].im)),
		                      lm_complex(rint(s.elem[3].re),rint(s.elem[3].im)),
		                      lm_complex(rint(s.elem[7].re),rint(s.elem[7].im)),
		                      lm_complex(rint(s.elem[11].re),rint(s.elem[11].im)),
		                      lm_complex(rint(s.elem[15].re),rint(s.elem[15].im))
		                      );
}

inline lightNint
irint(const lightNdouble& s)
{
    lightNint res(s.size1, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;

    for(int i=0; i<s.size1; i++)
	re[i] = int(rint(em[i]));

    return res;
}

inline lightNlm_complex
irint(const lightNlm_complex& s)
{
    lightNlm_complex res(s.size1, lightmat_dont_zero);

    for(int i=1; i<=s.size1; i++){
		res(i) = irint(s(i));
    }
    return res;
}

inline lightN3int
irint(const lightN3double& s)
{
    lightN3int res(s.size, lightmat_dont_zero);

    light3double *em = s.elem;
    light3int *re = res.elem;

    for(int i=0; i<s.size; i++)
    {
	re[i].elem[0] = int(rint(em[i].elem[0]));
	re[i].elem[1] = int(rint(em[i].elem[1]));
	re[i].elem[2] = int(rint(em[i].elem[2]));
    }

    return res;
}

inline lightN3lm_complex
irint(const lightN3lm_complex& s)
{
    lightN3lm_complex res(s.size, lightmat_dont_zero);

    for(int i=1; i<=s.size; i++)
    {
	res(i,0) = irint(s(i,0));
	res(i,1) = irint(s(i,1));
	res(i,2) = irint(s(i,2));
	}

    return res;
}

inline lightNNint
irint(const lightNNdouble& s)
{
    lightNNint res(s.size1, s.size2, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;
    const int size = s.size1*s.size2;

    for(int i=0; i<size; i++)
	re[i] = int(rint(em[i]));

    return res;
}

inline lightNNlm_complex
irint(const lightNNlm_complex& s)
{
    lightNNlm_complex res(s.size1, s.size2, lightmat_dont_zero);

    const int size = s.size1*s.size2;

    for(int i=1; i<=size; i++){
		res(i) = irint(s(i));
	}
    return res;
}

inline lightNNNint
irint(const lightNNNdouble& s)
{
    lightNNNint res(s.size1, s.size2, s.size3, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;
    const int size = s.size1*s.size2*s.size3;

    for(int i=0; i<size; i++)
	re[i] = int(rint(em[i]));

    return res;
}

inline lightNNNlm_complex
irint(const lightNNNlm_complex& s)
{
    lightNNNlm_complex res(s.size1, s.size2, s.size3, lightmat_dont_zero);

    const int size = s.size1*s.size2*s.size3;

    for(int i=1; i<=size; i++){
		res(i) = irint(s(i));
	 }
    return res;
}

inline lightNNNNint
irint(const lightNNNNdouble& s)
{
    lightNNNNint res(s.size1, s.size2, s.size3, s.size4, lightmat_dont_zero);

    double *em = s.elem;
    int *re = res.elem;
    const int size = s.size1*s.size2*s.size3*s.size4;

    for(int i=1; i<=size; i++)
	re[i] = int(rint(em[i]));

    return res;
}

inline lightNNNNlm_complex
irint(const lightNNNNlm_complex& s)
{
    lightNNNNlm_complex res(s.size1, s.size2, s.size3, s.size4, lightmat_dont_zero);

    const int size = s.size1*s.size2*s.size3*s.size4;

    for(int i=1; i<=size; i++){
		res(i) = irint(s(i));
	}
    return res;
}
