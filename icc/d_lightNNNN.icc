//           -*- c++ -*-

//
// **** members ****
//

#define IN_LIGHTNNNNdouble_ICC 


inline void
lightNNNNdouble::init(const int size)
{
    if(LIGHTNNNN_SIZE < size)
    {
        alloc_size = size;
        elem = new double[size];
    }
    else
    {
        alloc_size = LIGHTNNNN_SIZE;
        elem = sarea;
    }
}


	
inline
lightNNNNdouble::lightNNNNdouble()
  : elem(sarea), size1(LIGHTNNNN_SIZE1), size2(LIGHTNNNN_SIZE2),
    size3(LIGHTNNNN_SIZE3), size4(LIGHTNNNN_SIZE4), alloc_size(LIGHTNNNN_SIZE)
{
#ifdef LIGHTMAT_INIT_ZERO
    light_assign(LIGHTNNNN_SIZE, double(0), sarea);
#endif
}


inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s)
{
    const int size = s.size1*s.size2*s.size3*s.size4;
    init(size);
  
    size1 = s.size1;
    size2 = s.size2;
    size3 = s.size3;
    size4 = s.size4;

    light_assign(size, s.elem, elem);
}


inline
lightNNNNdouble::lightNNNNdouble(const lightmat_dummy_enum)
{
   init(0);
}




inline
lightNNNNdouble::lightNNNNdouble(const int x, const int y, const int z, const int v)
{
    const int size = x*y*z*v;
    init(size);

    size1 = x;
    size2 = y;
    size3 = z;
    size4 = v;

#ifdef LIGHTMAT_INIT_ZERO    
    light_assign(size, double(0), elem);
#endif
}



inline
lightNNNNdouble::lightNNNNdouble(const int x, const int y, const int z, const int v,
			const double *data)
{
    const int size = x*y*z*v;
    init(size);

    size1 = x;
    size2 = y;
    size3 = z;
    size4 = v;

    const int size432 = size4*size3*size2;

#ifdef ROWMAJOR
    double *em = elem;
    for(int i=0; i<size; i++) em[i] = data[i];    
#else
    for(int i4=0; i4<size4; i4++)
    {
	 double *em1 = &elem[i4*size1*size2*size3];
	const double *de1 = &data[i4];

	for(int i3=0; i3<size3; i3++)
	{
	    double *em2 = &em1[i3*size1*size2];
	    const double *de2 = &de1[i3*size4];

	    for(int i2=0; i2<size2; i2++)
	    {
		 double *em3 = &em2[i2*size1];
		const double *de3 = &de2[i2*size4*size3];

		for(int i1=0; i1<size1; i1++)
		    em3[i1] = de3[i1*size432];
	    }
	}
    }
#endif
}



inline
lightNNNNdouble::lightNNNNdouble(const int x, const int y, const int z, const int v,
			const double e)
{
    const int size = x*y*z*v;
    init(size);

    size1 = x;
    size2 = y;
    size3 = z;
    size4 = v;

    light_assign(size, e, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const int x, const int y, const int z, const int v,
			const lightmat_dont_zero_enum)
{
    const int size = x*y*z*v;
    init(size);

    size1 = x;
    size2 = y;
    size3 = z;
    size4 = v;
}



inline
lightNNNNdouble::~lightNNNNdouble()
{
    if(sarea != elem)
	delete [] elem;
}

#ifdef CONV_INT_2_DOUBLE

inline
lightNNNNdouble::operator lightNNNNdouble()
{
    lightNNNNdouble res(size1, size2, size3, size4, lightmat_dont_zero);
    const int size = size1*size2*size3*size4;
    for(int i=0; i<size; i++) res.elem[i] = elem[i];
    return res;
}
#endif

#ifdef CONV_DOUBLE_2_COMPLEX

inline
lightNNNNdouble::operator lightNNNNlm_complex()
{
    lightNNNNlm_complex res(size1, size2,size3,size4,lightmat_dont_zero);
    const int size = size1*size2*size3*size4;
    for(int i=0; i<size; i++) res.elem[i] = elem[i];
    return res;
}
#endif

#ifdef CONV_INT_2_COMPLEX

inline
lightNNNNdouble::operator lightNNNNlm_complex()
{
    lightNNNNlm_complex res(size1, size2,size3,size4,lightmat_dont_zero);
    const int size = size1*size2*size3*size4;
    for(int i=0; i<size; i++) res.elem[i] = elem[i];
    return res;
}
#endif




inline lightNNNNdouble&
lightNNNNdouble::operator=(const lightNNNNdouble& s)
{
    if(this == &s)
	return *this;

    const int size = s.size1*s.size2*s.size3*s.size4;

    if(alloc_size < size)
    {
	if(elem != sarea)
	    delete [] elem;

	alloc_size = size;
	elem = new double[alloc_size];
    }

    size1 = s.size1;
    size2 = s.size2;
    size3 = s.size3;
    size4 = s.size4;

    light_assign(size, s.elem, elem);

    return *this;
}



inline lightNNNNdouble&
lightNNNNdouble::operator=(const double e)
{
    light_assign(size1*size2*size3*size4, e, elem);

    return *this;
}


inline lightNNNNdouble&
lightNNNNdouble::SetShape(const int xx, const int yy, const int zz,  const int vv )
{

  int x=xx;
  int y=yy;
  int z=zz;
  int v=vv;
  if (x==-1) x=size1;
  if (y==-1) y=size2;
  if (z==-1) z=size3;
  if (v==-1) v=size4;

 limiterror((x<0) || (y<0) || (z<0) || (v<0));

 if(alloc_size < x*y*z*v)
    {
	if(elem != sarea)
	    delete [] elem;

	alloc_size = x*y*z*v;
	elem = new double[alloc_size];
    }

 size1 = x;
 size2 = y;
 size3 = z;
 size4 = v;
 return *this;
}





inline lightNNNNdouble&
lightNNNNdouble::reshape(const int x, const int y, const int z, const int v,
		      const lightNdouble& s)
{
    limiterror(x != s.size1);

    const int size = x*y*z*v;

    if(alloc_size < size)
    {
	if(elem != sarea)
	    delete [] elem;

	alloc_size = size1;
	elem = new double[alloc_size];
    }

    size1 = x;
    size2 = y;
    size3 = z;
    size4 = v;

    double *em = elem;
    double *se = s.elem;
    const int num = y*z*v;

#ifdef ROWMAJOR
    for(int i=1; i<=v; i++)
      for(int j=1; j<=z; j++)
	for(int k=1; k<=y; k++)
	  for(int l=1; l<=x; l++)
	    operator()(l, k, j, i) = s(l);
#else
    for(int i=0; i<num; i++)
	light_assign(x, se, &em[i*x]);
#endif

    return *this;
}



inline 
lightNNNNdouble & lightNNNNdouble::reshape(const int x, const int y, const int z, const int v,const lightNNdouble& s) {
    limiterror((x != s.size1) || (y != s.size2));

    const int size = x*y*z*v;

    if(alloc_size < size) {
        if(elem != sarea)
            delete [] elem;
        alloc_size = size;
        elem = new double[alloc_size];
    }

    size1 = x;
    size2 = y;
    size3 = z;
    size4 = v;

    double *em = elem;
    double *se = s.elem;

#ifdef ROWMAJOR
    for(int i=1; i<=v; i++)
      for(int j=1; j<=z; j++)
	for(int k=1; k<=y; k++)
	  for(int l=1; l<=x; l++)
	    operator()(l, k, j, i) = s(l, k);
#else
    {
        const int nsize = z*v;
        const int len = x*y;
        for(int i=0; i<nsize; i++)
            light_assign(len, se, &em[i*len]);
        return *this;
    }
#endif
}



inline lightNNNNdouble&
lightNNNNdouble::reshape(const int x, const int y, const int z, const int v,
		      const lightNNNdouble& s)
{
    limiterror((x != s.size1) || (y != s.size2) ||
	       (z != s.size3));

    const int size = x*y*z*v;

    if(alloc_size < size)
    {
	if(elem != sarea)
	    delete [] elem;

	alloc_size = size;
	elem = new double[alloc_size];
    }

    size1 = x;
    size2 = y;
    size3 = z;
    size4 = v;

    double *em = elem;
    double *se = s.elem;
    const int len = x*y*z;

#ifdef ROWMAJOR
    for(int i=1; i<=v; i++)
      for(int j=1; j<=z; j++)
	for(int k=1; k<=y; k++)
	  for(int l=1; l<=x; l++)
	    operator()(l, k, j, i) = s(l, k, j);
#else
    for(int i=0; i<v; i++)
	light_assign(len, se, &em[i*len]);
#endif

    return *this;
}


//
//inline double
//lightNNNNdouble::operator()(const int x, const int y, const int z,
//			   const int v) const
//{
//    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
//		 (z<1) || (z>size3) || (v<1) || (v>size4));
//    return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
//}


//
// Set
//


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const int i1, const int i2,
					const double val) {
  return Set (i0, i1, i2, All, val);
}


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const int i1, const int i2,
					const lightNdouble& arr) {
  return Set (i0, i1, i2, All, arr);
}


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const int i1, const int i2,
					const light3double& arr) {
  return Set (i0, i1, i2, All, arr);
}


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const int i1, const int i2,
					const light4double& arr) {
  return Set (i0, i1, i2, All, arr);
}


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const int i1, 
					const double val) {
  return Set (i0, i1, All, All, val);
}


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const int i1, 
					const lightNNdouble& arr) {
  return Set (i0, i1, All, All, arr);
}


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const int i1, 
					const light33double& arr) {
  return Set (i0, i1, All, All, arr);
}


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const int i1, 
					const light44double& arr) {
  return Set (i0, i1, All, All, arr);
}


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const double val) {
  return Set (i0, All, All, All, val);
}


inline lightNNNNdouble& lightNNNNdouble::Set (const int i0, const lightNNNdouble& arr) {
  return Set (i0, All, All, All, arr);
}


//
// Something else
// 

// 
// inline double&
// lightNNNNdouble::operator()(const int x, const int y, const int z, const int v)
// {
//     limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
// 		  (z<1) || (z>size3) || (v<1) || (v>size4));
//     return elem[(((v-1)*size3+(z-1))*size2+(y-1))*size1+x-1];
// }
 



inline lightNNNdouble 
// VADIM - cannot compile in DEC ! 
lightNNNNdouble::operator()(const int x) const
{
    limiterror((x<1) || (x>size1));

    lightNNNdouble res(size2, size3, size4, lightmat_dont_zero);

    const int size = size2*size3*size4;
    double *re = res.elem;
    double *em = elem;

#ifdef ROWMAJOR
    assert(0);
#else
    for(int i=x-1, el=0; el<size; i+=size1, el++)
	re[el] = em[i];
#endif

    return res;
}



inline lightNNdouble
lightNNNNdouble::operator()(const int x, const int y) const
{
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2));

    lightNNdouble res(size3, size4, lightmat_dont_zero);

    const int size = size3*size4;
    const int step = size1*size2;
    double *re = res.elem;
    double *em = elem;

#ifdef ROWMAJOR
    assert(0);
#else
    for(int i=(y-1)*size1+x-1, el=0; el<size; i+=step, el++)
	re[el] = em[i];
#endif

    return res;
}



inline lightNdouble
lightNNNNdouble::operator()(const int x, const int y, const int z) const
{
    limiterror((x<1) || (x>size1) || (y<1) || (y>size2) ||
	       (z<1) || (z>size3));

    lightNdouble res(size4, lightmat_dont_zero);

    const int step = size1*size2*size3;
    double *re = res.elem;
    double *em = elem;

#ifdef ROWMAJOR
    assert(0);
#else
    for(int i=((z-1)*size2+(y-1))*size1+x-1, el=0; el<size4; i+=step, el++)
	re[el] = em[i];
#endif

    return res;
}



inline int
lightNNNNdouble::operator==(const lightNNNNdouble& s) const
{
    if((size1 != s.size1) || (size2 != s.size2) || (size3 != s.size3) ||
       (size4 != s.size4))
	return 0;

    const int size = size1*size2*size3*size4;

    for(int i=0; i<size; i++)
	if(elem[i] != s.elem[i])
	    return 0;

    return 1;
}



inline int
lightNNNNdouble::operator!=(const lightNNNNdouble& s) const
{
    if((size1 != s.size1) || (size2 != s.size2) || (size3 != s.size3) ||
       (size4 != s.size4))
	return 1;

    const int size = size1*size2*size3*size4;

    for(int i=0; i<size; i++)
	if(elem[i] != s.elem[i])
	    return 1;

    return 0;
}


inline lightNNNNdouble&
lightNNNNdouble::operator+=(const double e)
{
    light_plus_same(size1*size2*size3*size4, elem, e);

    return *this;
}



inline lightNNNNdouble&
lightNNNNdouble::operator+=(const lightNNNNdouble& s)
{
    light_plus_same(size1*size2*size3*size4, elem, s.elem);

    return *this;
}



inline lightNNNNdouble&
lightNNNNdouble::operator-=(const double e)
{
    light_plus_same(size1*size2*size3*size4, elem, -e);

    return *this;
}



inline lightNNNNdouble&
lightNNNNdouble::operator-=(const lightNNNNdouble& s)
{
    light_minus_same(size1*size2*size3*size4, elem, s.elem);

    return *this;
}



inline lightNNNNdouble&
lightNNNNdouble::operator*=(const double e)
{
    light_mult_same(size1*size2*size3*size4, elem, e);

    return *this;
}



inline lightNNNNdouble&
lightNNNNdouble::operator/=(const double e)
{
    light_mult_same(size1*size2*size3*size4, elem, 1/e);

    return *this;
}



inline double
lightNNNNdouble::Extract(const lightNint&s) const
{
   if (s.dimension(1) != 3) 
     lighterror("Cannot do Extract");
   return (*this)(s(1),s(2),s(3),s(4));
}


// 
// inline int
// lightNNNNdouble::dimension(const int x) const
// {
//     if(x == 1)
// 	   return size1;
//     else if(x == 2)
// 	   return size2;
//     else if(x == 3)
// 	   return size3;
//     else if(x == 4)
// 	   return size4;
//     else
// 	   return 1;
// }

 

inline void
lightNNNNdouble::Get(double *data) const
{
    const int size432 = size4*size3*size2;

#ifdef ROWMAJOR
    for(int i=0; i<(size1*size2*size3*size4); i++)
      data[i] = elem[i];
#else
    for(int i4=0; i4<size4; i4++)
    {
	 double *em1 = &elem[i4*size1*size2*size3];
	 double *de1 = &data[i4];

	for(int i3=0; i3<size3; i3++)
	{
	    double *em2 = &em1[i3*size1*size2];
	    double *de2 = &de1[i3*size4];

	    for(int i2=0; i2<size2; i2++)
	    {
		 double *em3 = &em2[i2*size1];
		 double *de3 = &de2[i2*size4*size3];

		for(int i1=0; i1<size1; i1++)
		    de3[i1*size432] = em3[i1];
	    }
	}
    }
#endif
}



inline void
lightNNNNdouble::Set(const double *data)
{
    const int size432 = size4*size3*size2;

#ifdef ROWMAJOR
    for(int i=0; i<(size1*size2*size3*size4); i++)
      elem[i] = data[i];
#else
    for(int i4=0; i4<size4; i4++)
    {
	 double *em1 = &elem[i4*size1*size2*size3];
	const double *de1 = &data[i4];

	for(int i3=0; i3<size3; i3++)
	{
	    double *em2 = &em1[i3*size1*size2];
	    const double *de2 = &de1[i3*size4];

	    for(int i2=0; i2<size2; i2++)
	    {
		 double *em3 = &em2[i2*size1];
		const double *de3 = &de2[i2*size4*size3];

		for(int i1=0; i1<size1; i1++)
		    em3[i1] = de3[i1*size432];
	    }
	}
    }
#endif
}


inline double *
lightNNNNdouble::data() const
{
  return elem;
}


inline lightNNNNdouble
lightNNNNdouble::operator+() const
{
    return *this;
}



inline lightNNNNdouble
lightNNNNdouble::operator-() const
{
    return *this * -1;
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const lightNNNNdouble& s2,
			const lightmat_plus_enum)
{
    limiterror((s1.size1 != s2.size1) || (s1.size2 != s2.size2) ||
	       (s1.size3 != s2.size3) || (s1.size4 != s2.size4));

    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_plus(size, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const double e,
			const lightmat_plus_enum)
{
    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_plus(size, e, s1.elem, elem);
}

#ifdef IN_LIGHTNNNNdouble_ICC
//
inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const int e, const lightmat_plus_enum)
{
    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_plus(size, e, s1.elem, elem);
}

//
inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNint& s1, const double e, const lightmat_plus_enum)
{
    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_plus(size, e, s1.elem, elem);
}
#endif


inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const lightNNNNdouble& s2,
			const lightmat_minus_enum)
{
    limiterror((s1.size1 != s2.size1) || (s1.size2 != s2.size2) ||
	       (s1.size3 != s2.size3) || (s1.size4 != s2.size4));

    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_minus(size, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const double e, const lightNNNNdouble& s2,
			const lightmat_minus_enum)
{
    const int size = s2.size1*s2.size2*s2.size3*s2.size4;
    init(size);

    size1 = s2.size1;
    size2 = s2.size2;
    size3 = s2.size3;
    size4 = s2.size4;

    light_minus(size, e, s2.elem, elem);
}

#ifdef IN_LIGHTNNNNdouble_ICC
inline
lightNNNNdouble::lightNNNNdouble(const int e, const lightNNNNdouble& s2, const lightmat_minus_enum)
{
    const int size = s2.size1*s2.size2*s2.size3*s2.size4;
    init(size);

    size1 = s2.size1;
    size2 = s2.size2;
    size3 = s2.size3;
    size4 = s2.size4;

    light_minus(size, e, s2.elem, elem);
}

inline
lightNNNNdouble::lightNNNNdouble(const double e, const lightNNNNint& s2, const lightmat_minus_enum)
{
    const int size = s2.size1*s2.size2*s2.size3*s2.size4;
    init(size);

    size1 = s2.size1;
    size2 = s2.size2;
    size3 = s2.size3;
    size4 = s2.size4;

    light_minus(size, e, s2.elem, elem);
}
#endif



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const double e,
			const lightmat_mult_enum)
{
    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_mult(size, e, s1.elem, elem);
}

#ifdef IN_LIGHTNNNNdouble_ICC
inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const int e, const lightmat_mult_enum)
{
    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_mult(size, e, s1.elem, elem);
}

inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNint& s1, const double e, const lightmat_mult_enum)
{
    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_mult(size, e, s1.elem, elem);
}
#endif


inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const lightNNdouble& s2,
		      const lightmat_mult_enum)
{
    limiterror(s1.size4 != s2.size1);

    const int size = s1.size1*s1.size2*s1.size3*s2.size2;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s2.size2;

    light_ge4m(size1, size2, size3, size4, s1.size4, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNdouble& s1, const lightNNNNdouble& s2,
			const lightmat_mult_enum)
{
    limiterror(s1.size2 != s2.size1);

    const int size = s1.size1*s2.size2*s2.size3*s2.size4;
    init(size);

    size1 = s1.size1;
    size2 = s2.size2;
    size3 = s2.size3;
    size4 = s2.size4;

    light_gem4(size1, size2, size3, size4, s1.size2, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const light33double& s2,
		      const lightmat_mult_enum)
{
    limiterror(s1.size4 != 3);

    const int size = s1.size1*s1.size2*s1.size3*3;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = 3;

    light_ge4m(size1, size2, size3, size4, 3, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const light33double& s1, const lightNNNNdouble& s2,
			const lightmat_mult_enum)
{
    limiterror(33 != s2.size1);

    const int size = 3*s2.size2*s2.size3*s2.size4;
    init(size);

    size1 = 3;
    size2 = s2.size2;
    size3 = s2.size3;
    size4 = s2.size4;

    light_gem4(size1, size2, size3, size4, 3, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const light44double& s2,
		      const lightmat_mult_enum)
{
    limiterror(s1.size4 != 4);

    const int size = s1.size1*s1.size2*s1.size3*4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = 4;

    light_ge4m(size1, size2, size3, size4, 4, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const light44double& s1, const lightNNNNdouble& s2,
			const lightmat_mult_enum)
{
    limiterror(4 != s2.size1);

    const int size = 4*s2.size2*s2.size3*s2.size4;
    init(size);

    size1 = 4;
    size2 = s2.size2;
    size3 = s2.size3;
    size4 = s2.size4;

    light_gem4(size1, size2, size3, size4, 4, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNdouble& s1, const lightNNNdouble& s2,
			const lightmat_mult_enum)
{
    limiterror(s1.size3 != s2.size1);

    const int size = s1.size1*s1.size2*s2.size2*s2.size3;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s2.size2;
    size4=  s2.size3;

    light_ge33(size1, size2, size3, size4, s1.size3, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const double e, const lightNNNNdouble& s2,
			const lightmat_div_enum)
{
    const int size = s2.size1*s2.size2*s2.size3*s2.size4;
    init(size);

    size1 = s2.size1;
    size2 = s2.size2;
    size3 = s2.size3;
    size4 = s2.size4;

    light_divide(size, e, s2.elem, elem);
}

#ifdef IN_LIGHTNNNNdouble_ICC

inline
lightNNNNdouble::lightNNNNdouble(const double e, const lightNNNNint& s2, const lightmat_div_enum)
{
    const int size = s2.size1*s2.size2*s2.size3*s2.size4;
    init(size);

    size1 = s2.size1;
    size2 = s2.size2;
    size3 = s2.size3;
    size4 = s2.size4;

    light_divide(size, e, s2.elem, elem);
}

inline
lightNNNNdouble::lightNNNNdouble(const int e, const lightNNNNdouble& s2, const lightmat_div_enum)
{
    const int size = s2.size1*s2.size2*s2.size3*s2.size4;
    init(size);

    size1 = s2.size1;
    size2 = s2.size2;
    size3 = s2.size3;
    size4 = s2.size4;

    light_divide(size, e, s2.elem, elem);

}
#endif


inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const lightNNNNdouble& s2,
			const lightmat_pow_enum)
{
    limiterror((s1.size1 != s2.size1) || (s1.size2 != s2.size2) ||
	       (s1.size3 != s2.size3) || (s1.size4 != s2.size4));

    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);
 
    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    double *em = elem;
    const  double *s1e = s1.elem;
    const  double *s2e = s2.elem;

    for(int i=0; i<size; i++)

	em[i] = double(light_pow(s1e[i], s2e[i]));
// VADIM_03 (changed) from pow() !
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s, const double e,
			const lightmat_pow_enum)
{
    const int size = s.size1*s.size2*s.size3*s.size4;
    init(size);
 
    size1 = s.size1;
    size2 = s.size2;
    size3 = s.size3;
    size4 = s.size4;

    double *em = elem;
    const double *se = s.elem;

    for(int i=0; i<size; i++)
	em[i] = double(light_pow(se[i], e));
}



inline
lightNNNNdouble::lightNNNNdouble(const double e, const lightNNNNdouble& s,
			const lightmat_pow_enum)
{
    const int size = s.size1*s.size2*s.size3*s.size4;
    init(size);
 
    size1 = s.size1;
    size2 = s.size2;
    size3 = s.size3;
    size4 = s.size4;

    double *em = elem;
    const double *se = s.elem;

    for(int i=0; i<size; i++)
	em[i] = double(light_pow(e, se[i]));
}






#ifdef IN_LIGHTNNNNlm_complex_ICC
#else


inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s, const lightmat_abs_enum)
{
    const int size = s.size1*s.size2*s.size3*s.size4;
    init(size);
    size1 = s.size1;
    size2 = s.size2;
    size3 = s.size3;
    size4 = s.size4;
    for(int i=0; i<size; i++)
	elem[i] = LIGHTABS(s.elem[i]);
}

#ifdef IN_LIGHTNNNNdouble_ICC

inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNlm_complex& s, const lightmat_abs_enum)
{
    const int size = s.size1*s.size2*s.size3*s.size4;
    init(size);
    size1 = s.size1;
    size2 = s.size2;
    size3 = s.size3;
    size4 = s.size4;
    
    for(int i=0; i<size; i++)
	elem[i] = LIGHTABS(s.elem[i]);
}
#endif
#endif





inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const lightNNNNdouble& s2,
			const lightmat_eprod_enum)
{
    limiterror((s1.size1 != s2.size1) || (s1.size2 != s2.size2) ||
	       (s1.size3 != s2.size3) || (s1.size4 != s2.size4));

    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);
 
    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_mult(size, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const lightNNNNdouble& s2,
			const lightmat_equot_enum)
{
    limiterror((s1.size1 != s2.size1) || (s1.size2 != s2.size2) ||
	       (s1.size3 != s2.size3) || (s1.size4 != s2.size4));

    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);
 
    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    light_divide(size, s1.elem, s2.elem, elem);
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s, double f(double), const lightmat_apply_enum)
{
    const int size = s.size1*s.size2*s.size3*s.size4;
    init(size);

    size1 = s.size1;
    size2 = s.size2;
    size3 = s.size3;
    size4 = s.size4;

    double *em = elem;
    const double *se = s.elem;

    for(int i=0; i<size; i++)
	em[i] = f(se[i]);
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s1, const lightNNNNdouble& s2,
			 double f(double, double), const lightmat_apply_enum)
{
    limiterror((s1.size1 != s2.size1) || (s1.size2 != s2.size2) ||
	       (s1.size3 != s2.size3) || (s1.size4 != s2.size4));

    const int size = s1.size1*s1.size2*s1.size3*s1.size4;
    init(size);

    size1 = s1.size1;
    size2 = s1.size2;
    size3 = s1.size3;
    size4 = s1.size4;

    double *em = elem;
    const double *e1 = s1.elem;
    const double *e2 = s2.elem;

    for(int i=0; i<size; i++)
	em[i] = f(e1[i], e2[i]);
}



inline
lightNNNNdouble::lightNNNNdouble(const lightNNNNdouble& s, const lightmat_trans_enum)
{
    init(s.size1*s.size2*s.size3*s.size4);

    size1 = s.size2;
    size2 = s.size1;
    size3 = s.size3;
    size4 = s.size4;

    for(int i3=0; i3<size3*size4; i3++)
    {
	 double *r3 = &elem[i3*size2*size1];
	const double *d3 = &s.elem[i3*size2*size1];

	for(int col=0; col<s.size1; col++)
	{
	    int start = col*size1;

	    for(int row=0, step=0; row<size1; row++, step+=s.size1)
		r3[start+row] = d3[step+col];
	}
    }
}

#ifdef IN_LIGHTNNNNint_ICC

 inline lightNNNdouble light_mean     ( const  lightNNNNdouble& s){
     lightNNNdouble res(s.size2, s.size3, s.size4);
     for (int i=1;i<=s.size2;i++) {
     	for (int j=1;j<=s.size3;j++) {
     	  for (int k=1;k<=s.size4;k++) {
            res(i,j,k)=light_mean(s(All,i,j,k));
        }
       }
     }
      return res;
  };
  
   inline lightNNNdouble light_variance     ( const  lightNNNNdouble& s){
     lightNNNdouble res(s.size2, s.size3, s.size4);
     for (int i=1;i<=s.size2;i++) {
     	for (int j=1;j<=s.size3;j++) {
     	  for (int k=1;k<=s.size4;k++) {
            res(i,j,k)=light_variance(s(All,i,j,k));
        }
       }
     }
      return res;
  };
  
   inline lightNNNdouble light_standard_deviation     ( const  lightNNNNdouble& s){
     lightNNNdouble res(s.size2, s.size3, s.size4);
     for (int i=1;i<=s.size2;i++) {
     	for (int j=1;j<=s.size3;j++) {
     	  for (int k=1;k<=s.size4;k++) {
            res(i,j,k)=light_standard_deviation(s(All,i,j,k));
        }
       }
     }
      return res;
  };
  
     inline lightNNNdouble light_median     ( const  lightNNNNdouble& s){
     lightNNNdouble res(s.size2, s.size3, s.size4);
     for (int i=1;i<=s.size2;i++) {
     	for (int j=1;j<=s.size3;j++) {
     	  for (int k=1;k<=s.size4;k++) {
            res(i,j,k)=light_median(s(All,i,j,k));
        }
       }
     }
      return res;
  };
  
#else
 inline lightNNNdouble light_mean     ( const  lightNNNNdouble& s){
  
     lightNNNdouble res(s.size2,s.size3,s.size4);
     for (int i=1;i<=s.size2;i++) {
     	for (int j=1;j<=s.size3;j++) {
     	  for (int k=1;k<=s.size4;k++) {
            res(i,j,k)=light_mean(s(All,i,j,k));
        }
       }
     }
     return res;
  };
  
   inline lightNNNdouble light_variance     ( const  lightNNNNdouble& s){
  
     lightNNNdouble res(s.size2,s.size3,s.size4);
     for (int i=1;i<=s.size2;i++) {
     	for (int j=1;j<=s.size3;j++) {
     	  for (int k=1;k<=s.size4;k++) {
            res(i,j,k)=light_variance(s(All,i,j,k));
        }
       }
     }
     return res;
  };
  
   inline lightNNNdouble light_standard_deviation     ( const  lightNNNNdouble& s){
  
     lightNNNdouble res(s.size2,s.size3,s.size4);
     for (int i=1;i<=s.size2;i++) {
     	for (int j=1;j<=s.size3;j++) {
     	  for (int k=1;k<=s.size4;k++) {
            res(i,j,k)=light_standard_deviation(s(All,i,j,k));
        }
       }
     }
     return res;
  };

 inline lightNNNdouble light_median     ( const  lightNNNNdouble& s){
  
     lightNNNdouble res(s.size2,s.size3,s.size4);
     for (int i=1;i<=s.size2;i++) {
     	for (int j=1;j<=s.size3;j++) {
     	  for (int k=1;k<=s.size4;k++) {
            res(i,j,k)=light_median(s(All,i,j,k));
        }
       }
     }
     return res;
  };

#endif

 
inline lightNNNdouble light_total    ( const  lightNNNNdouble& s){
      lightNNNdouble res(s.size2,s.size3,s.size4);
      for (int i=1;i<=s.size2;i++) {
     	for (int j=1;j<=s.size3;j++) {
     	  for (int k=1;k<=s.size4;k++) {
            res(i,j,k)=light_total(s(All,i,j,k));
        }
       }
     }
     return res;
};


inline
bool greater_or_equal(lightNNNdouble a,lightNNNdouble b) {
/* This is called lexicographical comparison for arrays */
  for (int i = 1; i <= a.dimension(1); i++) {
      
      bool greater1= greater_or_equal(a(i),b(i));
      bool greater2= greater_or_equal(b(i),a(i));
      /* More efficent would be to use a threevalued function here */
      
      if (greater1 && greater2) continue; /* here equal */
      if (greater1) return true;
      if (greater2) return false;
      
   }
   return true; /* all were equal */

}
 
 
 
 

inline
int partition(lightNNNNdouble & a, int p, int r) {
  lightNNNdouble x = a(r);
  int j = p - 1;
  for (int i = p; i < r; i++) {

    if (greater_or_equal (x , a(i))) {
      j = j + 1;
      lightNNNdouble temp = a(j);
      a.Set(j,a(i)); //a(j) = a(i);
      a.Set(i,temp); //a(i) = temp;
    }
  }
  a.Set(r,a(j + 1)); //a(r) = a(j + 1);
  a.Set(j+1,x);  //a(j + 1) = x;

  return (j + 1);
}


inline
void quickSort(lightNNNNdouble & a, int p, int r) {
  if (p < r) {
    int q = partition(a, p, r);
    quickSort(a, p, q - 1);
    quickSort(a, q + 1, r);
  }
}

 
 
   inline lightNNNNdouble light_sort (const lightNNNNdouble s){
       lightNNNNdouble copy=s;
       quickSort(copy,1,copy.dimension(1)); // first and last index to use.
       return copy; 
 };

  inline lightNdouble light_flatten (const lightNNNNdouble s){
 	int size = s.size1*s.size2*s.size3*s.size4;
 	lightNdouble res(size);
 	for (int i=1; i<=s.size1; i++){
 		for (int j=1; j<=s.size2; j++){
 			for (int k=1; k<=s.size3; k++){
 				for (int m=1; m<=s.size4; m++){
 				     res.Set((i-1)*s.size2*s.size3*s.size4 + (j-1)*s.size3*s.size4 + (k-1)*s.size4 + m,s(i,j,k,m)); 				 
 				}
 			}
 		}
 	}
        return res;
 }

  inline lightNdouble light_flatten (const lightNNNNdouble s, int level){
 	int size = s.size1*s.size2*s.size3*s.size4;
 	lightNdouble res(size);
 	res = light_flatten(s);
 	return res;
 }
 
  inline lightNNNNdouble light_flatten0 (const lightNNNNdouble s){
       return s;
}

 inline lightNNNdouble light_flatten1 (const lightNNNNdouble s){
       int size = s.size1*s.size2;
       lightNNNdouble res(size, s.size3, s.size4); 
       for (int i=1; i<=s.size1; i++){
        	for(int j=1; j<= s.size2; j++){
        		res.Set((i-1)*s.size2 + j,s(i,j));
        	}
        } 
        return res;
       
}

 inline lightNNdouble light_flatten2 (const lightNNNNdouble s){
	int size= s.size1*s.size2*s.size3;
	lightNNdouble res(size,s.size4);
	for (int i=1; i<= s.size1; i++){
		for (int j=1; j<=s.size2; j++) {
			for( int k=1; k<=s.size3; k++){
			    res.Set((i-1)*s.size2*s.size3 + (j-1)*s.size3 + k,s(i,j,k));	
			}
		}
	}
	return res;
}

#include "d_lightNNNN_auto.icc"

#undef IN_LIGHTNNNNdouble_ICC 

