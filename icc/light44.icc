//           -*- c++ -*-

//
// **** members ****
//

#define IN_LIGHT44<T>_ICC

template<class T>
inline
light44<T>::light44()
{
#ifdef LIGHTMAT_INIT_ZERO
        elem[0] = 0; elem[1] = 0; elem[2] = 0; elem[3] = 0;
        elem[4] = 0; elem[5] = 0; elem[6] = 0; elem[7] = 0;
        elem[8] = 0; elem[9] = 0; elem[10] = 0; elem[11] = 0;
        elem[12] = 0; elem[13] = 0; elem[14] = 0; elem[15] = 0;
#endif
	size1 = 4;
	size2 = 4;
}


template<class T>
inline
light44<T>::light44(const light44<T>& s)
{
    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];
    elem[3] = s.elem[3];
    elem[4] = s.elem[4]; elem[5] = s.elem[5]; elem[6] = s.elem[6];
    elem[7] = s.elem[7];
    elem[8] = s.elem[8]; elem[9] = s.elem[9]; elem[10] = s.elem[10];
    elem[11] = s.elem[11];
    elem[12] = s.elem[12]; elem[13] = s.elem[13]; elem[14] = s.elem[14];
    elem[15] = s.elem[15];
    size1 = 4;
    size2 = 4;

}
 
 
template<class T>
inline
light44<T>::light44(const T e11, const T e12, const T e13, const T e14,
		    const T e21, const T e22, const T e23, const T e24,
		    const T e31, const T e32, const T e33, const T e34,
		    const T e41, const T e42, const T e43, const T e44)
{
#ifdef ROWMAJOR
    elem[0] = e11; elem[4] = e21; elem[8] = e31; elem[12] = e41;
    elem[1] = e12; elem[5] = e22; elem[9] = e32; elem[13] = e42;
    elem[2] = e13; elem[6] = e23; elem[10] = e33; elem[14] = e43;
    elem[3] = e14; elem[7] = e24; elem[11] = e34; elem[15] = e44;
#else
    elem[0] = e11; elem[4] = e12; elem[8] = e13; elem[12] = e14;
    elem[1] = e21; elem[5] = e22; elem[9] = e23; elem[13] = e24;
    elem[2] = e31; elem[6] = e32; elem[10] = e33; elem[14] = e34;
    elem[3] = e41; elem[7] = e42; elem[11] = e43; elem[15] = e44;
#endif
    size1 = 4;
    size2 = 4;
}
 
 
template<class T>
inline
light44<T>::light44(const T *data)
{
#ifdef ROWMAJOR
    elem[0] = data[0]; elem[1] = data[1]; elem[2] = data[2];
    elem[3] = data[3];
    elem[4] = data[4]; elem[5] = data[5]; elem[6] = data[6];
    elem[7] = data[7];
    elem[8] = data[8]; elem[9] = data[9]; elem[10] = data[10];
    elem[11] = data[11];
    elem[12] = data[12]; elem[13] = data[13]; elem[14] = data[14];
    elem[15] = data[15];
#else
    elem[0] = data[0]; elem[1] = data[4]; elem[2] = data[8];
    elem[3] = data[12];
    elem[4] = data[1]; elem[5] = data[5]; elem[6] = data[9];
    elem[7] = data[13];
    elem[8] = data[2]; elem[9] = data[6]; elem[10] = data[10];
    elem[11] = data[14];
    elem[12] = data[3]; elem[13] = data[7]; elem[14] = data[11];
    elem[15] = data[15];
#endif
    size1 = 4;
    size2 = 4;
}
 
 
template<class T>
inline
light44<T>::light44(const T e)
{
    elem[0] = e; elem[1] = e; elem[2] = e; elem[3] = e;
    elem[4] = e; elem[5] = e; elem[6] = e; elem[7] = e;
    elem[8] = e; elem[9] = e; elem[10] = e; elem[11] = e;
    elem[12] = e; elem[13] = e; elem[14] = e; elem[15] = e;
    size1 = 4;
    size2 = 4;
}


template<class T>
inline
light44<T>::light44(const lightmat_dont_zero_enum)
{
    size1 = 4;
    size2 = 4;
}

#ifdef CONV_INT_2_DOUBLE
template<class T>
inline
light44<T>::operator light44<double>()
{
#ifdef ROWMAJOR
    return light44<double>(elem[0], elem[1], elem[2], elem[3],
			   elem[4], elem[5], elem[6], elem[7],
			   elem[8], elem[9], elem[10], elem[11],
			   elem[12], elem[13], elem[14], elem[15]);
#else
    return light44<double>(elem[0], elem[4], elem[8], elem[12],
			   elem[1], elem[5], elem[9], elem[13],
			   elem[2], elem[6], elem[10], elem[14],
			   elem[3], elem[7], elem[11], elem[15]);
#endif
}
#endif

template<class T>
inline light44<T>&
light44<T>::operator=(const light44<T>& s)
{
    if(this == &s)
	return *this;

    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];
    elem[3] = s.elem[3];
    elem[4] = s.elem[4]; elem[5] = s.elem[5]; elem[6] = s.elem[6];
    elem[7] = s.elem[7];
    elem[8] = s.elem[8]; elem[9] = s.elem[9]; elem[10] = s.elem[10];
    elem[11] = s.elem[11];
    elem[12] = s.elem[12]; elem[13] = s.elem[13]; elem[14] = s.elem[14];
    elem[15] = s.elem[15];

    return *this;
}    


template<class T>
inline light44<T>&
light44<T>::operator=(const lightNN<T>& s)
{
    limiterror((s.size1 != 4) || (s.size2 != 4));

    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];
    elem[3] = s.elem[3];
    elem[4] = s.elem[4]; elem[5] = s.elem[5]; elem[6] = s.elem[6];
    elem[7] = s.elem[7];
    elem[8] = s.elem[8]; elem[9] = s.elem[9]; elem[10] = s.elem[10];
    elem[11] = s.elem[11];
    elem[12] = s.elem[12]; elem[13] = s.elem[13]; elem[14] = s.elem[14];
    elem[15] = s.elem[15];

    return *this;
}


template<class T>
inline light44<T>&
light44<T>::operator=(const T e)
{
    elem[0] = e; elem[1] = e; elem[2] = e; elem[3] = e;
    elem[4] = e; elem[5] = e; elem[6] = e; elem[7] = e;
    elem[8] = e; elem[9] = e; elem[10] = e; elem[11] = e;
    elem[12] = e; elem[13] = e; elem[14] = e; elem[15] = e;

    return *this;
}


template<class T>
inline light44<T>&
light44<T>::reshape(const int x, const int y, const light4<T>& s)
{
    limiterror((x != 4) || (y != 4));
#ifdef ROWMAJOR
    elem[0] = s.elem[0]; elem[1] = s.elem[0]; elem[2] = s.elem[0]; elem[3] = s.elem[0];
    elem[4] = s.elem[1]; elem[5] = s.elem[1]; elem[6] = s.elem[1]; elem[7] = s.elem[1];
    elem[8] = s.elem[2]; elem[9] = s.elem[2]; elem[10] = s.elem[2]; elem[11] = s.elem[2];
    elem[12] = s.elem[3]; elem[13] = s.elem[3]; elem[14] = s.elem[3]; elem[15] = s.elem[3];
#else
    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];
    elem[3] = s.elem[3];
    elem[4] = s.elem[0]; elem[5] = s.elem[1]; elem[6] = s.elem[2];
    elem[7] = s.elem[3];
    elem[8] = s.elem[0]; elem[9] = s.elem[1]; elem[10] = s.elem[2];
    elem[11] = s.elem[3];
    elem[12] = s.elem[0]; elem[13] = s.elem[1]; elem[14] = s.elem[2];
    elem[15] = s.elem[3];
#endif
    return *this;
}


// template<class T>
// inline T
// light44<T>::operator()(const int x, const int y) const
// {
//     limiterror((x<1) || (x>4) || (y<1) || (y>4));
// 
//     return elem[x-1+(y-1)*4];
// }
 



// template<class T>
// inline T&
// light44<T>::operator()(const int x, const int y)
// {
//     limiterror((x<1) || (x>4) || (y<1) || (y>4));
// 
//     return elem[x-1+(y-1)*4];
// }
 

template<class T>
inline light4<T>
light44<T>::operator()(const int x) const
{
    limiterror((x<1) || (x>4));

#ifdef ROWMAJOR
    return light4<T>(elem[(x-1)*4], elem[(x-1)*4+1], elem[(x-1)*4+2], elem[(x-1)*4+3]);
#else
    return light4<T>(elem[x-1], elem[x+3], elem[x+7], elem[x+11]);
#endif
}


//
// Set
//


template<class T>
inline light44<T>& light44<T>::Set (const int i0, const lightN<T>& arr) {
  return Set (i0, All, arr);
}

template<class T>
inline light44<T>& light44<T>::Set (const int i0, const light3<T>& arr) {
  return Set (i0, All, arr);
}

template<class T>
inline light44<T>& light44<T>::Set (const int i0, const light4<T>& arr) {
  return Set (i0, All, arr);
}






template<class T>
inline int
light44<T>::operator==(const light44<T>& s) const
{
    return (elem[0] == s.elem[0]) && (elem[1] == s.elem[1]) &&
	   (elem[2] == s.elem[2]) && (elem[3] == s.elem[3]) &&
	   (elem[4] == s.elem[4]) && (elem[5] == s.elem[5]) &&
	   (elem[6] == s.elem[6]) && (elem[7] == s.elem[7]) &&
	   (elem[8] == s.elem[8]) && (elem[9] == s.elem[9]) &&
	   (elem[10] == s.elem[10]) && (elem[11] == s.elem[11]) &&
	   (elem[12] == s.elem[12]) && (elem[13] == s.elem[13]) &&
	   (elem[14] == s.elem[14]) && (elem[15] == s.elem[15]);
}


template<class T>
inline int
light44<T>::operator!=(const light44<T>& s) const
{
    return (elem[0] != s.elem[0]) || (elem[1] != s.elem[1]) ||
	   (elem[2] != s.elem[2]) || (elem[3] != s.elem[3]) ||
	   (elem[4] != s.elem[4]) || (elem[5] != s.elem[5]) ||
	   (elem[6] != s.elem[6]) || (elem[7] != s.elem[7]) ||
	   (elem[8] != s.elem[8]) || (elem[9] != s.elem[9]) ||
	   (elem[10] != s.elem[10]) || (elem[11] != s.elem[11]) ||
	   (elem[12] != s.elem[12]) || (elem[13] != s.elem[13]) ||
	   (elem[14] != s.elem[14]) || (elem[15] != s.elem[15]);

}



template<class T>
inline light44<T>&
light44<T>::operator+=(const T e)
{
    elem[0] += e; elem[1] += e; elem[2] += e; elem[3] += e;
    elem[4] += e; elem[5] += e; elem[6] += e; elem[7] += e;
    elem[8] += e; elem[9] += e; elem[10] += e; elem[11] += e;
    elem[12] += e; elem[13] += e; elem[14] += e; elem[15] += e;

    return *this;
}


template<class T>
inline light44<T>&
light44<T>::operator+=(const light44<T>& s)
{
    elem[0] += s.elem[0]; elem[1] += s.elem[1]; elem[2] += s.elem[2];
    elem[3] += s.elem[3];
    elem[4] += s.elem[4]; elem[5] += s.elem[5]; elem[6] += s.elem[6];
    elem[7] += s.elem[7];
    elem[8] += s.elem[8]; elem[9] += s.elem[9]; elem[10] += s.elem[10];
    elem[11] += s.elem[11];
    elem[12] += s.elem[12]; elem[13] += s.elem[13]; elem[14] += s.elem[14];
    elem[15] += s.elem[15];

    return *this;
}


template<class T>
inline light44<T>&
light44<T>::operator-=(const T e)
{
    elem[0] -= e; elem[1] -= e; elem[2] -= e; elem[3] -= e;
    elem[4] -= e; elem[5] -= e; elem[6] -= e; elem[7] -= e;
    elem[8] -= e; elem[9] -= e; elem[10] -= e; elem[11] -= e;
    elem[12] -= e; elem[13] -= e; elem[14] -= e; elem[15] -= e;

    return *this;
}


template<class T>
inline light44<T>&
light44<T>::operator-=(const light44<T>& s)
{
    elem[0] -= s.elem[0]; elem[1] -= s.elem[1]; elem[2] -= s.elem[2];
    elem[3] -= s.elem[3];
    elem[4] -= s.elem[4]; elem[5] -= s.elem[5]; elem[6] -= s.elem[6];
    elem[7] -= s.elem[7];
    elem[8] -= s.elem[8]; elem[9] -= s.elem[9]; elem[10] -= s.elem[10];
    elem[11] -= s.elem[11];
    elem[12] -= s.elem[12]; elem[13] -= s.elem[13]; elem[14] -= s.elem[14];
    elem[15] -= s.elem[15];

    return *this;
}


template<class T>
inline light44<T>&
light44<T>::operator*=(const T e)
{
    elem[0] *= e; elem[1] *= e; elem[2] *= e; elem[3] *= e;
    elem[4] *= e; elem[5] *= e; elem[6] *= e; elem[7] *= e;
    elem[8] *= e; elem[9] *= e; elem[10] *= e; elem[11] *= e;
    elem[12] *= e; elem[13] *= e; elem[14] *= e; elem[15] *= e;

    return *this;
}


template<class T>
inline light44<T>&
light44<T>::operator/=(const T e)
{
    *this *= 1/e;

    return *this;
}


// template<class T>
// inline int
// light44<T>::dimension(const int x) const
// {
//     if((x==1) || (x==2))
// 	   return 4;
//     else
// 	   return 1;
// }


template<class T>
inline void
light44<T>::Get(T *data) const
{
#ifdef ROWMAJOR
  data[0] = elem[0]; data[1] = elem[1]; data[2] = elem[2];
  data[3] = elem[3];
  data[4] = elem[4]; data[5] = elem[5]; data[6] = elem[6];
  data[7] = elem[7];
  data[8] = elem[8]; data[9] = elem[9]; data[10] = elem[10];
  data[11] = elem[11];
  data[12] = elem[12]; data[13] = elem[13]; data[14] = elem[14];
  data[15] = elem[15];
#else
  data[0] = elem[0]; data[1] = elem[4]; data[2] = elem[8];
  data[3] = elem[12];
  data[4] = elem[1]; data[5] = elem[5]; data[6] = elem[9];
  data[7] = elem[13];
  data[8] = elem[2]; data[9] = elem[6]; data[10] = elem[10];
  data[11] = elem[14];
  data[12] = elem[3]; data[13] = elem[7]; data[14] = elem[11];
  data[15] = elem[15];
#endif
}


template<class T>
inline void
light44<T>::Set(const T *data)
{
#ifdef ROWMAJOR
    elem[0] = data[0]; elem[1] = data[1]; elem[2] = data[2];
    elem[3] = data[3];
    elem[4] = data[4]; elem[5] = data[5]; elem[6] = data[6];
    elem[7] = data[7];
    elem[8] = data[8]; elem[9] = data[9]; elem[10] = data[10];
    elem[11] = data[11];
    elem[12] = data[12]; elem[13] = data[13]; elem[14] = data[14];
    elem[15] = data[15];
#else
    elem[0] = data[0]; elem[4] = data[1]; elem[8] = data[2];
    elem[12] = data[3];
    elem[1] = data[4]; elem[5] = data[5]; elem[9] = data[6];
    elem[13] = data[7];
    elem[2] = data[8]; elem[6] = data[9]; elem[10] = data[10];
    elem[14] = data[11];
    elem[3] = data[12]; elem[7] = data[13]; elem[11] = data[14];
    elem[15] = data[15];
#endif
}


template<class T>
inline light44<T>
light44<T>::operator+() const
{
    return *this;
}


template<class T>
inline light44<T>
light44<T>::operator-() const
{
#ifdef ROWMAJOR
    return light44<T>(-elem[0], -elem[1], -elem[2], -elem[3],
		      -elem[4], -elem[5], -elem[6], -elem[7],
		      -elem[8], -elem[9], -elem[10], -elem[11],
		      -elem[12], -elem[13], -elem[14], -elem[15]);
#else
    return light44<T>(-elem[0], -elem[4], -elem[8], -elem[12],
		      -elem[1], -elem[5], -elem[9], -elem[13],
		      -elem[2], -elem[6], -elem[10], -elem[14],
		      -elem[3], -elem[7], -elem[11], -elem[15]);
#endif
}


//
// **** friends ****
//


template<class T>
inline light44<T>
operator+(const light44<T>& s1, const light44<T>& s2)
{
#ifdef ROWMAJOR
    return light44<T>(s1.elem[0] + s2.elem[0], s1.elem[1] + s2.elem[1],
		      s1.elem[2] + s2.elem[2], s1.elem[3] + s2.elem[3],
		      s1.elem[4] + s2.elem[4], s1.elem[5] + s2.elem[5],
		      s1.elem[6] + s2.elem[6], s1.elem[7] + s2.elem[7],
		      s1.elem[8] + s2.elem[8], s1.elem[9] + s2.elem[9],
		      s1.elem[10] + s2.elem[10], s1.elem[11] + s2.elem[11],
		      s1.elem[12] + s2.elem[12], s1.elem[13] + s2.elem[13],
		      s1.elem[14] + s2.elem[14], s1.elem[15] + s2.elem[15]);
#else
    return light44<T>(s1.elem[0] + s2.elem[0], s1.elem[4] + s2.elem[4],
		      s1.elem[8] + s2.elem[8], s1.elem[12] + s2.elem[12],
		      s1.elem[1] + s2.elem[1], s1.elem[5] + s2.elem[5],
		      s1.elem[9] + s2.elem[9], s1.elem[13] + s2.elem[13],
		      s1.elem[2] + s2.elem[2], s1.elem[6] + s2.elem[6],
		      s1.elem[10] + s2.elem[10], s1.elem[14] + s2.elem[14],
		      s1.elem[3] + s2.elem[3], s1.elem[7] + s2.elem[7],
		      s1.elem[11] + s2.elem[11], s1.elem[15] + s2.elem[15]);
#endif
}


template<class T>
inline light44<T>
operator+(const light44<T>& s1, const T e)
{
#ifdef ROWMAJOR
    return light44<T>(s1.elem[0] + e, s1.elem[1] + e, s1.elem[2] + e,
		      s1.elem[3] + e,
		      s1.elem[4] + e, s1.elem[5] + e, s1.elem[6] + e,
		      s1.elem[7] + e,
		      s1.elem[8] + e, s1.elem[9] + e, s1.elem[10] + e,
		      s1.elem[11] + e,
		      s1.elem[12] + e, s1.elem[13] + e, s1.elem[14] + e,
		      s1.elem[15] + e);
#else
    return light44<T>(s1.elem[0] + e, s1.elem[4] + e, s1.elem[8] + e,
		      s1.elem[12] + e,
		      s1.elem[1] + e, s1.elem[5] + e, s1.elem[9] + e,
		      s1.elem[13] + e,
		      s1.elem[2] + e, s1.elem[6] + e, s1.elem[10] + e,
		      s1.elem[14] + e,
		      s1.elem[3] + e, s1.elem[7] + e, s1.elem[11] + e,
		      s1.elem[15] + e);
#endif
}


template<class T>
inline light44<T>
operator+(const T e, const light44<T>& s1)
{
    return s1 + e;
}


template<class T>
inline light44<T>
operator-(const light44<T>& s1, const light44<T>& s2)
{
#ifdef ROWMAJOR
    return light44<T>(s1.elem[0] - s2.elem[0], s1.elem[1] - s2.elem[1],
		      s1.elem[2] - s2.elem[2], s1.elem[3] - s2.elem[3],
		      s1.elem[4] - s2.elem[4], s1.elem[5] - s2.elem[5],
		      s1.elem[6] - s2.elem[6], s1.elem[7] - s2.elem[7],
		      s1.elem[8] - s2.elem[8], s1.elem[9] - s2.elem[9],
		      s1.elem[10] - s2.elem[10], s1.elem[11] - s2.elem[11],
		      s1.elem[12] - s2.elem[12], s1.elem[13] - s2.elem[13],
		      s1.elem[14] - s2.elem[14], s1.elem[15] - s2.elem[15]);
#else
    return light44<T>(s1.elem[0] - s2.elem[0], s1.elem[4] - s2.elem[4],
		      s1.elem[8] - s2.elem[8], s1.elem[12] - s2.elem[12],
		      s1.elem[1] - s2.elem[1], s1.elem[5] - s2.elem[5],
		      s1.elem[9] - s2.elem[9], s1.elem[13] - s2.elem[13],
		      s1.elem[2] - s2.elem[2], s1.elem[6] - s2.elem[6],
		      s1.elem[10] - s2.elem[10], s1.elem[14] - s2.elem[14],
		      s1.elem[3] - s2.elem[3], s1.elem[7] - s2.elem[7],
		      s1.elem[11] - s2.elem[11], s1.elem[15] - s2.elem[15]);
#endif
}


template<class T>
inline light44<T>
operator-(const light44<T>& s1, const T e)
{
#ifdef ROWMAJOR
    return light44<T>(s1.elem[0] - e, s1.elem[1] - e, s1.elem[2] - e,
		      s1.elem[3] - e,
		      s1.elem[4] - e, s1.elem[5] - e, s1.elem[6] - e,
		      s1.elem[7] - e,
		      s1.elem[8] - e, s1.elem[9] - e, s1.elem[10] - e,
		      s1.elem[11] - e,
		      s1.elem[12] - e, s1.elem[13] - e, s1.elem[14] - e,
		      s1.elem[15] - e);
#else
    return light44<T>(s1.elem[0] - e, s1.elem[4] - e, s1.elem[8] - e,
		      s1.elem[12] - e,
		      s1.elem[1] - e, s1.elem[5] - e, s1.elem[9] - e,
		      s1.elem[13] - e,
		      s1.elem[2] - e, s1.elem[6] - e, s1.elem[10] - e,
		      s1.elem[14] - e,
		      s1.elem[3] - e, s1.elem[7] - e, s1.elem[11] - e,
		      s1.elem[15] - e);
#endif
}


template<class T>
inline light44<T>
operator-(const T e, const light44<T>& s1)
{
#ifdef ROWMAJOR
    return light44<T>(e - s1.elem[0], e - s1.elem[1], e - s1.elem[2],
		      e - s1.elem[3],
		      e - s1.elem[4], e - s1.elem[5], e - s1.elem[6],
		      e - s1.elem[7],
		      e - s1.elem[8], e - s1.elem[9], e - s1.elem[10],
		      e - s1.elem[11],
		      e - s1.elem[12], e - s1.elem[13], e - s1.elem[14],
		      e - s1.elem[15]);
#else
    return light44<T>(e - s1.elem[0], e - s1.elem[4], e - s1.elem[8],
		      e - s1.elem[12],
		      e - s1.elem[1], e - s1.elem[5], e - s1.elem[9],
		      e - s1.elem[13],
		      e - s1.elem[2], e - s1.elem[6], e - s1.elem[10],
		      e - s1.elem[14],
		      e - s1.elem[3], e - s1.elem[7], e - s1.elem[11],
		      e - s1.elem[15]);
#endif
}


template<class T>
inline light44<T>
operator*(const light44<T>& s1, const light44<T>& s2)
{
#ifdef ROWMAJOR
  assert(0);
    return light44<T>(
       s1.elem[0]*s2.elem[0] + s1.elem[1]*s2.elem[4] + s1.elem[2]*s2.elem[8] + s1.elem[3]*s2.elem[12],
       s1.elem[0]*s2.elem[1] + s1.elem[1]*s2.elem[5] + s1.elem[2]*s2.elem[9] + s1.elem[3]*s2.elem[13],
       s1.elem[0]*s2.elem[2] + s1.elem[1]*s2.elem[6] + s1.elem[2]*s2.elem[10] + s1.elem[3]*s2.elem[14],
       s1.elem[0]*s2.elem[3] + s1.elem[1]*s2.elem[7] + s1.elem[2]*s2.elem[11] + s1.elem[3]*s2.elem[15],

       s1.elem[4]*s2.elem[0] + s1.elem[5]*s2.elem[4] + s1.elem[6]*s2.elem[8] + s1.elem[7]*s2.elem[12],
       s1.elem[4]*s2.elem[1] + s1.elem[5]*s2.elem[5] + s1.elem[6]*s2.elem[9] + s1.elem[7]*s2.elem[13],
       s1.elem[4]*s2.elem[2] + s1.elem[5]*s2.elem[6] + s1.elem[6]*s2.elem[10] + s1.elem[7]*s2.elem[14],
       s1.elem[4]*s2.elem[3] + s1.elem[5]*s2.elem[7] + s1.elem[6]*s2.elem[11] + s1.elem[7]*s2.elem[15],

       s1.elem[8]*s2.elem[0] + s1.elem[9]*s2.elem[4] + s1.elem[10]*s2.elem[8] + s1.elem[11]*s2.elem[12],
       s1.elem[8]*s2.elem[1] + s1.elem[9]*s2.elem[5] + s1.elem[10]*s2.elem[9] + s1.elem[11]*s2.elem[13],
       s1.elem[8]*s2.elem[2] + s1.elem[9]*s2.elem[6] + s1.elem[10]*s2.elem[10] + s1.elem[11]*s2.elem[14],
       s1.elem[8]*s2.elem[3] + s1.elem[9]*s2.elem[7] + s1.elem[10]*s2.elem[11] + s1.elem[11]*s2.elem[15],

       s1.elem[12]*s2.elem[0] + s1.elem[13]*s2.elem[4] + s1.elem[14]*s2.elem[8] + s1.elem[15]*s2.elem[12],
       s1.elem[12]*s2.elem[1] + s1.elem[13]*s2.elem[5] + s1.elem[14]*s2.elem[9] + s1.elem[15]*s2.elem[13],
       s1.elem[12]*s2.elem[2] + s1.elem[13]*s2.elem[6] + s1.elem[14]*s2.elem[10] + s1.elem[15]*s2.elem[14],
       s1.elem[12]*s2.elem[3] + s1.elem[13]*s2.elem[7] + s1.elem[14]*s2.elem[11] + s1.elem[15]*s2.elem[15]);
#else
    return light44<T>(
       s1.elem[0]*s2.elem[0] + s1.elem[4]*s2.elem[1] + s1.elem[8]*s2.elem[2] +
       s1.elem[12]*s2.elem[3],
       s1.elem[0]*s2.elem[4] + s1.elem[4]*s2.elem[5] + s1.elem[8]*s2.elem[6] +
       s1.elem[12]*s2.elem[7],
       s1.elem[0]*s2.elem[8] + s1.elem[4]*s2.elem[9] + s1.elem[8]*s2.elem[10] +
       s1.elem[12]*s2.elem[11],
       s1.elem[0]*s2.elem[12] + s1.elem[4]*s2.elem[13] +
       s1.elem[8]*s2.elem[14] + s1.elem[12]*s2.elem[15],

       s1.elem[1]*s2.elem[0] + s1.elem[5]*s2.elem[1] + s1.elem[9]*s2.elem[2] +
       s1.elem[13]*s2.elem[3],
       s1.elem[1]*s2.elem[4] + s1.elem[5]*s2.elem[5] + s1.elem[9]*s2.elem[6] +
       s1.elem[13]*s2.elem[7],
       s1.elem[1]*s2.elem[8] + s1.elem[5]*s2.elem[9] + s1.elem[9]*s2.elem[10] +
       s1.elem[13]*s2.elem[11],
       s1.elem[1]*s2.elem[12] + s1.elem[5]*s2.elem[13] +
       s1.elem[9]*s2.elem[14] + s1.elem[13]*s2.elem[15],

       s1.elem[2]*s2.elem[0] + s1.elem[6]*s2.elem[1] + s1.elem[10]*s2.elem[2] +
       s1.elem[14]*s2.elem[3],
       s1.elem[2]*s2.elem[4] + s1.elem[6]*s2.elem[5] + s1.elem[10]*s2.elem[6] +
       s1.elem[14]*s2.elem[7],
       s1.elem[2]*s2.elem[8] + s1.elem[6]*s2.elem[9] +
       s1.elem[10]*s2.elem[10] + s1.elem[14]*s2.elem[11],
       s1.elem[2]*s2.elem[12] + s1.elem[6]*s2.elem[13] +
       s1.elem[10]*s2.elem[14] + s1.elem[14]*s2.elem[15],

       s1.elem[3]*s2.elem[0] + s1.elem[7]*s2.elem[1] + s1.elem[11]*s2.elem[2] +
       s1.elem[15]*s2.elem[3],
       s1.elem[3]*s2.elem[4] + s1.elem[7]*s2.elem[5] + s1.elem[11]*s2.elem[6] +
       s1.elem[15]*s2.elem[7],
       s1.elem[3]*s2.elem[8] + s1.elem[7]*s2.elem[9] +
       s1.elem[11]*s2.elem[10] + s1.elem[15]*s2.elem[11],
       s1.elem[3]*s2.elem[12] + s1.elem[7]*s2.elem[13] +
       s1.elem[11]*s2.elem[14] + s1.elem[15]*s2.elem[15]);
#endif
}


template<class T>
inline light44<T>
operator*(const light44<T>& s1, const T e)
{
#ifdef ROWMAJOR
    return light44<T>(s1.elem[0]*e, s1.elem[1]*e, s1.elem[2]*e, s1.elem[3]*e,
		      s1.elem[4]*e, s1.elem[5]*e, s1.elem[6]*e, s1.elem[7]*e,
		      s1.elem[8]*e, s1.elem[9]*e, s1.elem[10]*e, s1.elem[11]*e,
		      s1.elem[12]*e, s1.elem[13]*e, s1.elem[14]*e, s1.elem[15]*e
		      );
#else
    return light44<T>(s1.elem[0]*e, s1.elem[4]*e, s1.elem[8]*e, s1.elem[12]*e,
		      s1.elem[1]*e, s1.elem[5]*e, s1.elem[9]*e, s1.elem[13]*e,
		      s1.elem[2]*e, s1.elem[6]*e, s1.elem[10]*e, s1.elem[14]*e,
		      s1.elem[3]*e, s1.elem[7]*e, s1.elem[11]*e, s1.elem[15]*e
		      );
#endif
}


template<class T>
inline light44<T>
operator*(const T e, const light44<T>& s1)
{
    return s1 * e;
}


template<class T>
inline light4<T>
operator*(const light44<T>& s1, const light4<T>& s2)
{
#ifdef ROWMAJOR
  assert(0);
#else
    return light4<T>(s1.elem[0] * s2.elem[0] +
		     s1.elem[4] * s2.elem[1] +
		     s1.elem[8] * s2.elem[2] +
		     s1.elem[12] * s2.elem[3] ,
		     s1.elem[1] * s2.elem[0] +
		     s1.elem[5] * s2.elem[1] +
		     s1.elem[9] * s2.elem[2] +
		     s1.elem[13] * s2.elem[3],
		     s1.elem[2] * s2.elem[0] +
		     s1.elem[6] * s2.elem[1] +
		     s1.elem[10] * s2.elem[2] +
		     s1.elem[14] * s2.elem[3],
		     s1.elem[3] * s2.elem[0] +
		     s1.elem[7] * s2.elem[1] +
		     s1.elem[11] * s2.elem[2] +
		     s1.elem[15] * s2.elem[3]);		     
#endif
}


template<class T>
inline light44<T>
operator/(const light44<T>& s1, const T e)
{
#ifdef ROWMAJOR
    return light44<T>(s1.elem[0] / e, s1.elem[1] / e, s1.elem[2] / e,
		      s1.elem[3] / e,
		      s1.elem[4] / e, s1.elem[5] / e, s1.elem[6] / e,
		      s1.elem[7] / e,
		      s1.elem[8] / e, s1.elem[9] / e, s1.elem[10] / e,
		      s1.elem[11] / e,
		      s1.elem[12] / e, s1.elem[13] / e, s1.elem[14] / e,
		      s1.elem[15] / e);
#else
    return light44<T>(s1.elem[0] / e, s1.elem[4] / e, s1.elem[8] / e,
		      s1.elem[12] / e,
		      s1.elem[1] / e, s1.elem[5] / e, s1.elem[9] / e,
		      s1.elem[13] / e,
		      s1.elem[2] / e, s1.elem[6] / e, s1.elem[10] / e,
		      s1.elem[14] / e,
		      s1.elem[3] / e, s1.elem[7] / e, s1.elem[11] / e,
		      s1.elem[15] / e);
#endif
}


template<class T>
inline light44<T>
operator/(const T e, const light44<T>& s1)
{
#ifdef ROWMAJOR
    return light44<T>(e / s1.elem[0], e / s1.elem[1], e / s1.elem[2],
		      e / s1.elem[3],
		      e / s1.elem[4], e / s1.elem[5], e / s1.elem[6],
		      e / s1.elem[7],
		      e / s1.elem[8], e / s1.elem[9], e / s1.elem[10],
		      e / s1.elem[11],
		      e / s1.elem[12], e / s1.elem[13], e / s1.elem[14],
		      e / s1.elem[15]);
#else
    return light44<T>(e / s1.elem[0], e / s1.elem[4], e / s1.elem[8],
		      e / s1.elem[12],
		      e / s1.elem[1], e / s1.elem[5], e / s1.elem[9],
		      e / s1.elem[13],
		      e / s1.elem[2], e / s1.elem[6], e / s1.elem[10],
		      e / s1.elem[14],
		      e / s1.elem[3], e / s1.elem[7], e / s1.elem[11],
		      e / s1.elem[15]);
#endif
}


template<class T>
inline light44<T>
pow(const light44<T>& s1, const light44<T>& s2)
{
#ifdef ROWMAJOR
    return light44<T>(T(light_pow(s1.elem[0], s2.elem[0])),
		      T(light_pow(s1.elem[1], s2.elem[1])),
		      T(light_pow(s1.elem[2], s2.elem[2])),
		      T(light_pow(s1.elem[3], s2.elem[3])),
		      T(light_pow(s1.elem[4], s2.elem[4])),
		      T(light_pow(s1.elem[5], s2.elem[5])),
		      T(light_pow(s1.elem[6], s2.elem[6])),
		      T(light_pow(s1.elem[7], s2.elem[7])),
		      T(light_pow(s1.elem[8], s2.elem[8])),
		      T(light_pow(s1.elem[9], s2.elem[9])),
		      T(light_pow(s1.elem[10], s2.elem[10])),
		      T(light_pow(s1.elem[11], s2.elem[11])),
		      T(light_pow(s1.elem[12], s2.elem[12])),
		      T(light_pow(s1.elem[13], s2.elem[13])),
		      T(light_pow(s1.elem[14], s2.elem[14])),
		      T(light_pow(s1.elem[15], s2.elem[15])));
#else
    return light44<T>(T(light_pow(s1.elem[0], s2.elem[0])),
		      T(light_pow(s1.elem[4], s2.elem[4])),
		      T(light_pow(s1.elem[8], s2.elem[8])),
		      T(light_pow(s1.elem[12], s2.elem[12])),
		      T(light_pow(s1.elem[1], s2.elem[1])),
		      T(light_pow(s1.elem[5], s2.elem[5])),
		      T(light_pow(s1.elem[9], s2.elem[9])),
		      T(light_pow(s1.elem[13], s2.elem[13])),
		      T(light_pow(s1.elem[2], s2.elem[2])),
		      T(light_pow(s1.elem[6], s2.elem[6])),
		      T(light_pow(s1.elem[10], s2.elem[10])),
		      T(light_pow(s1.elem[14], s2.elem[14])),
		      T(light_pow(s1.elem[3], s2.elem[3])),
		      T(light_pow(s1.elem[7], s2.elem[7])),
		      T(light_pow(s1.elem[11], s2.elem[11])),
		      T(light_pow(s1.elem[15], s2.elem[15])));
#endif
}


template<class T>
inline light44<T>
pow(const light44<T>& s, const T e)
{
#ifdef ROWMAJOR
    return light44<T>(T(light_pow(s.elem[0], e)), T(light_pow(s.elem[1], e)),
		      T(light_pow(s.elem[2], e)), T(light_pow(s.elem[3], e)),
		      T(light_pow(s.elem[4], e)), T(light_pow(s.elem[5], e)),
		      T(light_pow(s.elem[6], e)), T(light_pow(s.elem[7], e)),
		      T(light_pow(s.elem[8], e)), T(light_pow(s.elem[9], e)),
		      T(light_pow(s.elem[10], e)), T(light_pow(s.elem[11], e)),
		      T(light_pow(s.elem[12], e)), T(light_pow(s.elem[13], e)),
		      T(light_pow(s.elem[14], e)), T(light_pow(s.elem[15], e)));
#else
    return light44<T>(T(light_pow(s.elem[0], e)), T(light_pow(s.elem[4], e)),
		      T(light_pow(s.elem[8], e)), T(light_pow(s.elem[12], e)),
		      T(light_pow(s.elem[1], e)), T(light_pow(s.elem[5], e)),
		      T(light_pow(s.elem[9], e)), T(light_pow(s.elem[13], e)),
		      T(light_pow(s.elem[2], e)), T(light_pow(s.elem[6], e)),
		      T(light_pow(s.elem[10], e)), T(light_pow(s.elem[14], e)),
		      T(light_pow(s.elem[3], e)), T(light_pow(s.elem[7], e)),
		      T(light_pow(s.elem[11], e)), T(light_pow(s.elem[15], e)));
#endif
}


template<class T>
inline light44<T>
pow(const T e, const light44<T>& s)
{
#ifdef ROWMAJOR
    return light44<T>(T(light_pow(e,s.elem[0])), T(light_pow(e,s.elem[1])),
		      T(light_pow(e,s.elem[2])), T(light_pow(e,s.elem[3])),
		      T(light_pow(e,s.elem[4])), T(light_pow(e,s.elem[5])),
		      T(light_pow(e,s.elem[6])), T(light_pow(e,s.elem[7])),
		      T(light_pow(e,s.elem[8])), T(light_pow(e,s.elem[9])),
		      T(light_pow(e,s.elem[10])), T(light_pow(e,s.elem[11])),
		      T(light_pow(e,s.elem[12])), T(light_pow(e,s.elem[13])),
		      T(light_pow(e,s.elem[14])), T(light_pow(e,s.elem[15])));
#else
    return light44<T>(T(light_pow(e,s.elem[0])), T(light_pow(e,s.elem[4])),
		      T(light_pow(e,s.elem[8])), T(light_pow(e,s.elem[12])),
		      T(light_pow(e,s.elem[1])), T(light_pow(e,s.elem[5])),
		      T(light_pow(e,s.elem[9])), T(light_pow(e,s.elem[13])),
		      T(light_pow(e,s.elem[2])), T(light_pow(e,s.elem[6])),
		      T(light_pow(e,s.elem[10])), T(light_pow(e,s.elem[14])),
		      T(light_pow(e,s.elem[3])), T(light_pow(e,s.elem[7])),
		      T(light_pow(e,s.elem[11])), T(light_pow(e,s.elem[15])));
#endif
}


template<class T>
inline light44<T>
abs(const light44<T>& s)
{
#ifdef ROWMAJOR
    return light44<T>(LIGHTABS(s.elem[0]), LIGHTABS(s.elem[1]),
		      LIGHTABS(s.elem[2]), LIGHTABS(s.elem[3]),
		      LIGHTABS(s.elem[4]), LIGHTABS(s.elem[5]),
		      LIGHTABS(s.elem[6]), LIGHTABS(s.elem[7]),
		      LIGHTABS(s.elem[8]), LIGHTABS(s.elem[9]),
		      LIGHTABS(s.elem[10]), LIGHTABS(s.elem[11]),
		      LIGHTABS(s.elem[12]), LIGHTABS(s.elem[13]),
		      LIGHTABS(s.elem[14]), LIGHTABS(s.elem[15]));
#else
    return light44<T>(LIGHTABS(s.elem[0]), LIGHTABS(s.elem[4]),
		      LIGHTABS(s.elem[8]), LIGHTABS(s.elem[12]),
		      LIGHTABS(s.elem[1]), LIGHTABS(s.elem[5]),
		      LIGHTABS(s.elem[9]), LIGHTABS(s.elem[13]),
		      LIGHTABS(s.elem[2]), LIGHTABS(s.elem[6]),
		      LIGHTABS(s.elem[10]), LIGHTABS(s.elem[14]),
		      LIGHTABS(s.elem[3]), LIGHTABS(s.elem[7]),
		      LIGHTABS(s.elem[11]), LIGHTABS(s.elem[15]));
#endif
}


template<class T>
inline light44<T>
ElemProduct(const light44<T>& s1, const light44<T>& s2)
{
#ifdef ROWMAJOR
    return light44<T>(s1.elem[0] * s2.elem[0], s1.elem[1] * s2.elem[1],
		      s1.elem[2] * s2.elem[2], s1.elem[3] * s2.elem[3],
		      s1.elem[4] * s2.elem[4], s1.elem[5] * s2.elem[5],
		      s1.elem[6] * s2.elem[6], s1.elem[7] * s2.elem[7],
		      s1.elem[8] * s2.elem[8], s1.elem[9] * s2.elem[9],
		      s1.elem[10] * s2.elem[10], s1.elem[11] * s2.elem[11],
		      s1.elem[12] * s2.elem[12], s1.elem[13] * s2.elem[13],
		      s1.elem[14] * s2.elem[14], s1.elem[15] * s2.elem[15]);
#else
    return light44<T>(s1.elem[0] * s2.elem[0], s1.elem[4] * s2.elem[4],
		      s1.elem[8] * s2.elem[8], s1.elem[12] * s2.elem[12],
		      s1.elem[1] * s2.elem[1], s1.elem[5] * s2.elem[5],
		      s1.elem[9] * s2.elem[9], s1.elem[13] * s2.elem[13],
		      s1.elem[2] * s2.elem[2], s1.elem[6] * s2.elem[6],
		      s1.elem[10] * s2.elem[10], s1.elem[14] * s2.elem[14],
		      s1.elem[3] * s2.elem[3], s1.elem[7] * s2.elem[7],
		      s1.elem[11] * s2.elem[11], s1.elem[15] * s2.elem[15]);
#endif
}


template<class T>
inline light44<T>
ElemQuotient(const light44<T>& s1, const light44<T>& s2)
{
#ifdef ROWMAJOR
    return light44<T>(s1.elem[0] / s2.elem[0], s1.elem[1] / s2.elem[1],
		      s1.elem[2] / s2.elem[2], s1.elem[3] / s2.elem[3],
		      s1.elem[4] / s2.elem[4], s1.elem[5] / s2.elem[5],
		      s1.elem[6] / s2.elem[6], s1.elem[7] / s2.elem[7],
		      s1.elem[8] / s2.elem[8], s1.elem[9] / s2.elem[9],
		      s1.elem[10] / s2.elem[10], s1.elem[11] / s2.elem[11],
		      s1.elem[12] / s2.elem[12], s1.elem[13] / s2.elem[13],
		      s1.elem[14] / s2.elem[14], s1.elem[15] / s2.elem[15]);
#else
    return light44<T>(s1.elem[0] / s2.elem[0], s1.elem[4] / s2.elem[4],
		      s1.elem[8] / s2.elem[8], s1.elem[12] / s2.elem[12],
		      s1.elem[1] / s2.elem[1], s1.elem[5] / s2.elem[5],
		      s1.elem[9] / s2.elem[9], s1.elem[13] / s2.elem[13],
		      s1.elem[2] / s2.elem[2], s1.elem[6] / s2.elem[6],
		      s1.elem[10] / s2.elem[10], s1.elem[14] / s2.elem[14],
		      s1.elem[3] / s2.elem[3], s1.elem[7] / s2.elem[7],
		      s1.elem[11] / s2.elem[11], s1.elem[15] / s2.elem[15]);
#endif
}


template<class T>
inline light44<T>
Apply(const light44<T>& s, T f(T))
{
#ifdef ROWMAJOR
    return light44<T>(f(s.elem[0]), f(s.elem[1]), f(s.elem[2]), f(s.elem[3]),
		      f(s.elem[4]), f(s.elem[5]), f(s.elem[6]), f(s.elem[7]),
		      f(s.elem[8]), f(s.elem[9]), f(s.elem[10]), f(s.elem[11]),
		      f(s.elem[12]), f(s.elem[13]), f(s.elem[14]), f(s.elem[15])
		      );
#else
    return light44<T>(f(s.elem[0]), f(s.elem[4]), f(s.elem[8]), f(s.elem[12]),
		      f(s.elem[1]), f(s.elem[5]), f(s.elem[9]), f(s.elem[13]),
		      f(s.elem[2]), f(s.elem[6]), f(s.elem[10]), f(s.elem[14]),
		      f(s.elem[3]), f(s.elem[7]), f(s.elem[11]), f(s.elem[15])
		      );
#endif
}


template<class T>
inline light44<T>
Apply(const light44<T>& s1, const light44<T>& s2, T f(T, T))
{
#ifdef ROWMAJOR
    return light44<T>(f(s1.elem[0], s2.elem[0]), f(s1.elem[1], s2.elem[1]),
		      f(s1.elem[2], s2.elem[2]), f(s1.elem[3], s2.elem[3]),
		      f(s1.elem[4], s2.elem[4]), f(s1.elem[5], s2.elem[5]),
		      f(s1.elem[6], s2.elem[6]), f(s1.elem[7], s2.elem[7]),
		      f(s1.elem[8], s2.elem[8]), f(s1.elem[9], s2.elem[9]),
		      f(s1.elem[10], s2.elem[10]), f(s1.elem[11], s2.elem[11]),
		      f(s1.elem[12], s2.elem[12]), f(s1.elem[13], s2.elem[13]),
		      f(s1.elem[14], s2.elem[14]), f(s1.elem[15], s2.elem[15])
		      );
#else
    return light44<T>(f(s1.elem[0], s2.elem[0]), f(s1.elem[4], s2.elem[4]),
		      f(s1.elem[8], s2.elem[8]), f(s1.elem[12], s2.elem[12]),
		      f(s1.elem[1], s2.elem[1]), f(s1.elem[5], s2.elem[5]),
		      f(s1.elem[9], s2.elem[9]), f(s1.elem[13], s2.elem[13]),
		      f(s1.elem[2], s2.elem[2]), f(s1.elem[6], s2.elem[6]),
		      f(s1.elem[10], s2.elem[10]), f(s1.elem[14], s2.elem[14]),
		      f(s1.elem[3], s2.elem[3]), f(s1.elem[7], s2.elem[7]),
		      f(s1.elem[11], s2.elem[11]), f(s1.elem[15], s2.elem[15])
		      );
#endif
}


template<class T>
inline light44<T>
Transpose(const light44<T>& s)
{
    return light44(s.elem);
}

#include "light44_auto.icc"
 
#undef IN_LIGHT44<T>_ICC
