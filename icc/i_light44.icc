//           -*- c++ -*-

//
// **** members ****
//

#define IN_LIGHT44int_ICC


inline
light44int::light44int()
{
#ifdef LIGHTMAT_INIT_ZERO
        elem[0] = 0; elem[1] = 0; elem[2] = 0; elem[3] = 0;
        elem[4] = 0; elem[5] = 0; elem[6] = 0; elem[7] = 0;
        elem[8] = 0; elem[9] = 0; elem[10] = 0; elem[11] = 0;
        elem[12] = 0; elem[13] = 0; elem[14] = 0; elem[15] = 0;
#endif
	size1 = 4;
	size2 = 4;
}



inline
light44int::light44int(const light44int& s)
{
    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];
    elem[3] = s.elem[3];
    elem[4] = s.elem[4]; elem[5] = s.elem[5]; elem[6] = s.elem[6];
    elem[7] = s.elem[7];
    elem[8] = s.elem[8]; elem[9] = s.elem[9]; elem[10] = s.elem[10];
    elem[11] = s.elem[11];
    elem[12] = s.elem[12]; elem[13] = s.elem[13]; elem[14] = s.elem[14];
    elem[15] = s.elem[15];
    size1 = 4;
    size2 = 4;

}
 
 

inline
light44int::light44int(const int e11, const int e12, const int e13, const int e14,
		    const int e21, const int e22, const int e23, const int e24,
		    const int e31, const int e32, const int e33, const int e34,
		    const int e41, const int e42, const int e43, const int e44)
{
#ifdef ROWMAJOR
    elem[0] = e11; elem[4] = e21; elem[8] = e31; elem[12] = e41;
    elem[1] = e12; elem[5] = e22; elem[9] = e32; elem[13] = e42;
    elem[2] = e13; elem[6] = e23; elem[10] = e33; elem[14] = e43;
    elem[3] = e14; elem[7] = e24; elem[11] = e34; elem[15] = e44;
#else
    elem[0] = e11; elem[4] = e12; elem[8] = e13; elem[12] = e14;
    elem[1] = e21; elem[5] = e22; elem[9] = e23; elem[13] = e24;
    elem[2] = e31; elem[6] = e32; elem[10] = e33; elem[14] = e34;
    elem[3] = e41; elem[7] = e42; elem[11] = e43; elem[15] = e44;
#endif
    size1 = 4;
    size2 = 4;
}
 
 

inline
light44int::light44int(const int *data)
{
#ifdef ROWMAJOR
    elem[0] = data[0]; elem[1] = data[1]; elem[2] = data[2];
    elem[3] = data[3];
    elem[4] = data[4]; elem[5] = data[5]; elem[6] = data[6];
    elem[7] = data[7];
    elem[8] = data[8]; elem[9] = data[9]; elem[10] = data[10];
    elem[11] = data[11];
    elem[12] = data[12]; elem[13] = data[13]; elem[14] = data[14];
    elem[15] = data[15];
#else
    elem[0] = data[0]; elem[1] = data[4]; elem[2] = data[8];
    elem[3] = data[12];
    elem[4] = data[1]; elem[5] = data[5]; elem[6] = data[9];
    elem[7] = data[13];
    elem[8] = data[2]; elem[9] = data[6]; elem[10] = data[10];
    elem[11] = data[14];
    elem[12] = data[3]; elem[13] = data[7]; elem[14] = data[11];
    elem[15] = data[15];
#endif
    size1 = 4;
    size2 = 4;
}
 
 

inline
light44int::light44int(const int e)
{
    elem[0] = e; elem[1] = e; elem[2] = e; elem[3] = e;
    elem[4] = e; elem[5] = e; elem[6] = e; elem[7] = e;
    elem[8] = e; elem[9] = e; elem[10] = e; elem[11] = e;
    elem[12] = e; elem[13] = e; elem[14] = e; elem[15] = e;
    size1 = 4;
    size2 = 4;
}



inline
light44int::light44int(const lightmat_dont_zero_enum)
{
    size1 = 4;
    size2 = 4;
}

#ifdef CONV_INT_2_DOUBLE

inline
light44int::operator light44double()
{
#ifdef ROWMAJOR
    return light44double(elem[0], elem[1], elem[2], elem[3],
			   elem[4], elem[5], elem[6], elem[7],
			   elem[8], elem[9], elem[10], elem[11],
			   elem[12], elem[13], elem[14], elem[15]);
#else
    return light44double(elem[0], elem[4], elem[8], elem[12],
			   elem[1], elem[5], elem[9], elem[13],
			   elem[2], elem[6], elem[10], elem[14],
			   elem[3], elem[7], elem[11], elem[15]);
#endif
}
#endif


inline light44int&
light44int::operator=(const light44int& s)
{
    if(this == &s)
	return *this;

    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];
    elem[3] = s.elem[3];
    elem[4] = s.elem[4]; elem[5] = s.elem[5]; elem[6] = s.elem[6];
    elem[7] = s.elem[7];
    elem[8] = s.elem[8]; elem[9] = s.elem[9]; elem[10] = s.elem[10];
    elem[11] = s.elem[11];
    elem[12] = s.elem[12]; elem[13] = s.elem[13]; elem[14] = s.elem[14];
    elem[15] = s.elem[15];

    return *this;
}    



inline light44int&
light44int::operator=(const lightNNint& s)
{
    limiterror((s.size1 != 4) || (s.size2 != 4));

    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];
    elem[3] = s.elem[3];
    elem[4] = s.elem[4]; elem[5] = s.elem[5]; elem[6] = s.elem[6];
    elem[7] = s.elem[7];
    elem[8] = s.elem[8]; elem[9] = s.elem[9]; elem[10] = s.elem[10];
    elem[11] = s.elem[11];
    elem[12] = s.elem[12]; elem[13] = s.elem[13]; elem[14] = s.elem[14];
    elem[15] = s.elem[15];

    return *this;
}



inline light44int&
light44int::operator=(const int e)
{
    elem[0] = e; elem[1] = e; elem[2] = e; elem[3] = e;
    elem[4] = e; elem[5] = e; elem[6] = e; elem[7] = e;
    elem[8] = e; elem[9] = e; elem[10] = e; elem[11] = e;
    elem[12] = e; elem[13] = e; elem[14] = e; elem[15] = e;

    return *this;
}



inline light44int&
light44int::reshape(const int x, const int y, const light4int& s)
{
    limiterror((x != 4) || (y != 4));
#ifdef ROWMAJOR
    elem[0] = s.elem[0]; elem[1] = s.elem[0]; elem[2] = s.elem[0]; elem[3] = s.elem[0];
    elem[4] = s.elem[1]; elem[5] = s.elem[1]; elem[6] = s.elem[1]; elem[7] = s.elem[1];
    elem[8] = s.elem[2]; elem[9] = s.elem[2]; elem[10] = s.elem[2]; elem[11] = s.elem[2];
    elem[12] = s.elem[3]; elem[13] = s.elem[3]; elem[14] = s.elem[3]; elem[15] = s.elem[3];
#else
    elem[0] = s.elem[0]; elem[1] = s.elem[1]; elem[2] = s.elem[2];
    elem[3] = s.elem[3];
    elem[4] = s.elem[0]; elem[5] = s.elem[1]; elem[6] = s.elem[2];
    elem[7] = s.elem[3];
    elem[8] = s.elem[0]; elem[9] = s.elem[1]; elem[10] = s.elem[2];
    elem[11] = s.elem[3];
    elem[12] = s.elem[0]; elem[13] = s.elem[1]; elem[14] = s.elem[2];
    elem[15] = s.elem[3];
#endif
    return *this;
}


// 
// inline int
// light44int::operator()(const int x, const int y) const
// {
//     limiterror((x<1) || (x>4) || (y<1) || (y>4));
// 
//     return elem[x-1+(y-1)*4];
// }
 



// 
// inline int&
// light44int::operator()(const int x, const int y)
// {
//     limiterror((x<1) || (x>4) || (y<1) || (y>4));
// 
//     return elem[x-1+(y-1)*4];
// }
 


inline light4int
light44int::operator()(const int x) const
{
    limiterror((x<1) || (x>4));

#ifdef ROWMAJOR
    return light4int(elem[(x-1)*4], elem[(x-1)*4+1], elem[(x-1)*4+2], elem[(x-1)*4+3]);
#else
    return light4int(elem[x-1], elem[x+3], elem[x+7], elem[x+11]);
#endif
}


//
// Set
//



inline light44int& light44int::Set (const int i0, const lightNint& arr) {
  return Set (i0, All, arr);
}


inline light44int& light44int::Set (const int i0, const light3int& arr) {
  return Set (i0, All, arr);
}


inline light44int& light44int::Set (const int i0, const light4int& arr) {
  return Set (i0, All, arr);
}







inline int
light44int::operator==(const light44int& s) const
{
    return (elem[0] == s.elem[0]) && (elem[1] == s.elem[1]) &&
	   (elem[2] == s.elem[2]) && (elem[3] == s.elem[3]) &&
	   (elem[4] == s.elem[4]) && (elem[5] == s.elem[5]) &&
	   (elem[6] == s.elem[6]) && (elem[7] == s.elem[7]) &&
	   (elem[8] == s.elem[8]) && (elem[9] == s.elem[9]) &&
	   (elem[10] == s.elem[10]) && (elem[11] == s.elem[11]) &&
	   (elem[12] == s.elem[12]) && (elem[13] == s.elem[13]) &&
	   (elem[14] == s.elem[14]) && (elem[15] == s.elem[15]);
}



inline int
light44int::operator!=(const light44int& s) const
{
    return (elem[0] != s.elem[0]) || (elem[1] != s.elem[1]) ||
	   (elem[2] != s.elem[2]) || (elem[3] != s.elem[3]) ||
	   (elem[4] != s.elem[4]) || (elem[5] != s.elem[5]) ||
	   (elem[6] != s.elem[6]) || (elem[7] != s.elem[7]) ||
	   (elem[8] != s.elem[8]) || (elem[9] != s.elem[9]) ||
	   (elem[10] != s.elem[10]) || (elem[11] != s.elem[11]) ||
	   (elem[12] != s.elem[12]) || (elem[13] != s.elem[13]) ||
	   (elem[14] != s.elem[14]) || (elem[15] != s.elem[15]);

}




inline light44int&
light44int::operator+=(const int e)
{
    elem[0] += e; elem[1] += e; elem[2] += e; elem[3] += e;
    elem[4] += e; elem[5] += e; elem[6] += e; elem[7] += e;
    elem[8] += e; elem[9] += e; elem[10] += e; elem[11] += e;
    elem[12] += e; elem[13] += e; elem[14] += e; elem[15] += e;

    return *this;
}



inline light44int&
light44int::operator+=(const light44int& s)
{
    elem[0] += s.elem[0]; elem[1] += s.elem[1]; elem[2] += s.elem[2];
    elem[3] += s.elem[3];
    elem[4] += s.elem[4]; elem[5] += s.elem[5]; elem[6] += s.elem[6];
    elem[7] += s.elem[7];
    elem[8] += s.elem[8]; elem[9] += s.elem[9]; elem[10] += s.elem[10];
    elem[11] += s.elem[11];
    elem[12] += s.elem[12]; elem[13] += s.elem[13]; elem[14] += s.elem[14];
    elem[15] += s.elem[15];

    return *this;
}



inline light44int&
light44int::operator-=(const int e)
{
    elem[0] -= e; elem[1] -= e; elem[2] -= e; elem[3] -= e;
    elem[4] -= e; elem[5] -= e; elem[6] -= e; elem[7] -= e;
    elem[8] -= e; elem[9] -= e; elem[10] -= e; elem[11] -= e;
    elem[12] -= e; elem[13] -= e; elem[14] -= e; elem[15] -= e;

    return *this;
}



inline light44int&
light44int::operator-=(const light44int& s)
{
    elem[0] -= s.elem[0]; elem[1] -= s.elem[1]; elem[2] -= s.elem[2];
    elem[3] -= s.elem[3];
    elem[4] -= s.elem[4]; elem[5] -= s.elem[5]; elem[6] -= s.elem[6];
    elem[7] -= s.elem[7];
    elem[8] -= s.elem[8]; elem[9] -= s.elem[9]; elem[10] -= s.elem[10];
    elem[11] -= s.elem[11];
    elem[12] -= s.elem[12]; elem[13] -= s.elem[13]; elem[14] -= s.elem[14];
    elem[15] -= s.elem[15];

    return *this;
}



inline light44int&
light44int::operator*=(const int e)
{
    elem[0] *= e; elem[1] *= e; elem[2] *= e; elem[3] *= e;
    elem[4] *= e; elem[5] *= e; elem[6] *= e; elem[7] *= e;
    elem[8] *= e; elem[9] *= e; elem[10] *= e; elem[11] *= e;
    elem[12] *= e; elem[13] *= e; elem[14] *= e; elem[15] *= e;

    return *this;
}



inline light44int&
light44int::operator/=(const int e)
{
    *this *= 1/e;

    return *this;
}


// 
// inline int
// light44int::dimension(const int x) const
// {
//     if((x==1) || (x==2))
// 	   return 4;
//     else
// 	   return 1;
// }



inline void
light44int::Get(int *data) const
{
#ifdef ROWMAJOR
  data[0] = elem[0]; data[1] = elem[1]; data[2] = elem[2];
  data[3] = elem[3];
  data[4] = elem[4]; data[5] = elem[5]; data[6] = elem[6];
  data[7] = elem[7];
  data[8] = elem[8]; data[9] = elem[9]; data[10] = elem[10];
  data[11] = elem[11];
  data[12] = elem[12]; data[13] = elem[13]; data[14] = elem[14];
  data[15] = elem[15];
#else
  data[0] = elem[0]; data[1] = elem[4]; data[2] = elem[8];
  data[3] = elem[12];
  data[4] = elem[1]; data[5] = elem[5]; data[6] = elem[9];
  data[7] = elem[13];
  data[8] = elem[2]; data[9] = elem[6]; data[10] = elem[10];
  data[11] = elem[14];
  data[12] = elem[3]; data[13] = elem[7]; data[14] = elem[11];
  data[15] = elem[15];
#endif
}



inline void
light44int::Set(const int *data)
{
#ifdef ROWMAJOR
    elem[0] = data[0]; elem[1] = data[1]; elem[2] = data[2];
    elem[3] = data[3];
    elem[4] = data[4]; elem[5] = data[5]; elem[6] = data[6];
    elem[7] = data[7];
    elem[8] = data[8]; elem[9] = data[9]; elem[10] = data[10];
    elem[11] = data[11];
    elem[12] = data[12]; elem[13] = data[13]; elem[14] = data[14];
    elem[15] = data[15];
#else
    elem[0] = data[0]; elem[4] = data[1]; elem[8] = data[2];
    elem[12] = data[3];
    elem[1] = data[4]; elem[5] = data[5]; elem[9] = data[6];
    elem[13] = data[7];
    elem[2] = data[8]; elem[6] = data[9]; elem[10] = data[10];
    elem[14] = data[11];
    elem[3] = data[12]; elem[7] = data[13]; elem[11] = data[14];
    elem[15] = data[15];
#endif
}



inline light44int
light44int::operator+() const
{
    return *this;
}



inline light44int
light44int::operator-() const
{
#ifdef ROWMAJOR
    return light44int(-elem[0], -elem[1], -elem[2], -elem[3],
		      -elem[4], -elem[5], -elem[6], -elem[7],
		      -elem[8], -elem[9], -elem[10], -elem[11],
		      -elem[12], -elem[13], -elem[14], -elem[15]);
#else
    return light44int(-elem[0], -elem[4], -elem[8], -elem[12],
		      -elem[1], -elem[5], -elem[9], -elem[13],
		      -elem[2], -elem[6], -elem[10], -elem[14],
		      -elem[3], -elem[7], -elem[11], -elem[15]);
#endif
}


//
// **** friends ****
//



inline light44int
operator+(const light44int& s1, const light44int& s2)
{
#ifdef ROWMAJOR
    return light44int(s1.elem[0] + s2.elem[0], s1.elem[1] + s2.elem[1],
		      s1.elem[2] + s2.elem[2], s1.elem[3] + s2.elem[3],
		      s1.elem[4] + s2.elem[4], s1.elem[5] + s2.elem[5],
		      s1.elem[6] + s2.elem[6], s1.elem[7] + s2.elem[7],
		      s1.elem[8] + s2.elem[8], s1.elem[9] + s2.elem[9],
		      s1.elem[10] + s2.elem[10], s1.elem[11] + s2.elem[11],
		      s1.elem[12] + s2.elem[12], s1.elem[13] + s2.elem[13],
		      s1.elem[14] + s2.elem[14], s1.elem[15] + s2.elem[15]);
#else
    return light44int(s1.elem[0] + s2.elem[0], s1.elem[4] + s2.elem[4],
		      s1.elem[8] + s2.elem[8], s1.elem[12] + s2.elem[12],
		      s1.elem[1] + s2.elem[1], s1.elem[5] + s2.elem[5],
		      s1.elem[9] + s2.elem[9], s1.elem[13] + s2.elem[13],
		      s1.elem[2] + s2.elem[2], s1.elem[6] + s2.elem[6],
		      s1.elem[10] + s2.elem[10], s1.elem[14] + s2.elem[14],
		      s1.elem[3] + s2.elem[3], s1.elem[7] + s2.elem[7],
		      s1.elem[11] + s2.elem[11], s1.elem[15] + s2.elem[15]);
#endif
}



inline light44int
operator+(const light44int& s1, const int e)
{
#ifdef ROWMAJOR
    return light44int(s1.elem[0] + e, s1.elem[1] + e, s1.elem[2] + e,
		      s1.elem[3] + e,
		      s1.elem[4] + e, s1.elem[5] + e, s1.elem[6] + e,
		      s1.elem[7] + e,
		      s1.elem[8] + e, s1.elem[9] + e, s1.elem[10] + e,
		      s1.elem[11] + e,
		      s1.elem[12] + e, s1.elem[13] + e, s1.elem[14] + e,
		      s1.elem[15] + e);
#else
    return light44int(s1.elem[0] + e, s1.elem[4] + e, s1.elem[8] + e,
		      s1.elem[12] + e,
		      s1.elem[1] + e, s1.elem[5] + e, s1.elem[9] + e,
		      s1.elem[13] + e,
		      s1.elem[2] + e, s1.elem[6] + e, s1.elem[10] + e,
		      s1.elem[14] + e,
		      s1.elem[3] + e, s1.elem[7] + e, s1.elem[11] + e,
		      s1.elem[15] + e);
#endif
}



inline light44int
operator+(const int e, const light44int& s1)
{
    return s1 + e;
}



inline light44int
operator-(const light44int& s1, const light44int& s2)
{
#ifdef ROWMAJOR
    return light44int(s1.elem[0] - s2.elem[0], s1.elem[1] - s2.elem[1],
		      s1.elem[2] - s2.elem[2], s1.elem[3] - s2.elem[3],
		      s1.elem[4] - s2.elem[4], s1.elem[5] - s2.elem[5],
		      s1.elem[6] - s2.elem[6], s1.elem[7] - s2.elem[7],
		      s1.elem[8] - s2.elem[8], s1.elem[9] - s2.elem[9],
		      s1.elem[10] - s2.elem[10], s1.elem[11] - s2.elem[11],
		      s1.elem[12] - s2.elem[12], s1.elem[13] - s2.elem[13],
		      s1.elem[14] - s2.elem[14], s1.elem[15] - s2.elem[15]);
#else
    return light44int(s1.elem[0] - s2.elem[0], s1.elem[4] - s2.elem[4],
		      s1.elem[8] - s2.elem[8], s1.elem[12] - s2.elem[12],
		      s1.elem[1] - s2.elem[1], s1.elem[5] - s2.elem[5],
		      s1.elem[9] - s2.elem[9], s1.elem[13] - s2.elem[13],
		      s1.elem[2] - s2.elem[2], s1.elem[6] - s2.elem[6],
		      s1.elem[10] - s2.elem[10], s1.elem[14] - s2.elem[14],
		      s1.elem[3] - s2.elem[3], s1.elem[7] - s2.elem[7],
		      s1.elem[11] - s2.elem[11], s1.elem[15] - s2.elem[15]);
#endif
}



inline light44int
operator-(const light44int& s1, const int e)
{
#ifdef ROWMAJOR
    return light44int(s1.elem[0] - e, s1.elem[1] - e, s1.elem[2] - e,
		      s1.elem[3] - e,
		      s1.elem[4] - e, s1.elem[5] - e, s1.elem[6] - e,
		      s1.elem[7] - e,
		      s1.elem[8] - e, s1.elem[9] - e, s1.elem[10] - e,
		      s1.elem[11] - e,
		      s1.elem[12] - e, s1.elem[13] - e, s1.elem[14] - e,
		      s1.elem[15] - e);
#else
    return light44int(s1.elem[0] - e, s1.elem[4] - e, s1.elem[8] - e,
		      s1.elem[12] - e,
		      s1.elem[1] - e, s1.elem[5] - e, s1.elem[9] - e,
		      s1.elem[13] - e,
		      s1.elem[2] - e, s1.elem[6] - e, s1.elem[10] - e,
		      s1.elem[14] - e,
		      s1.elem[3] - e, s1.elem[7] - e, s1.elem[11] - e,
		      s1.elem[15] - e);
#endif
}



inline light44int
operator-(const int e, const light44int& s1)
{
#ifdef ROWMAJOR
    return light44int(e - s1.elem[0], e - s1.elem[1], e - s1.elem[2],
		      e - s1.elem[3],
		      e - s1.elem[4], e - s1.elem[5], e - s1.elem[6],
		      e - s1.elem[7],
		      e - s1.elem[8], e - s1.elem[9], e - s1.elem[10],
		      e - s1.elem[11],
		      e - s1.elem[12], e - s1.elem[13], e - s1.elem[14],
		      e - s1.elem[15]);
#else
    return light44int(e - s1.elem[0], e - s1.elem[4], e - s1.elem[8],
		      e - s1.elem[12],
		      e - s1.elem[1], e - s1.elem[5], e - s1.elem[9],
		      e - s1.elem[13],
		      e - s1.elem[2], e - s1.elem[6], e - s1.elem[10],
		      e - s1.elem[14],
		      e - s1.elem[3], e - s1.elem[7], e - s1.elem[11],
		      e - s1.elem[15]);
#endif
}



inline light44int
operator*(const light44int& s1, const light44int& s2)
{
#ifdef ROWMAJOR
  assert(0);
    return light44int(
       s1.elem[0]*s2.elem[0] + s1.elem[1]*s2.elem[4] + s1.elem[2]*s2.elem[8] + s1.elem[3]*s2.elem[12],
       s1.elem[0]*s2.elem[1] + s1.elem[1]*s2.elem[5] + s1.elem[2]*s2.elem[9] + s1.elem[3]*s2.elem[13],
       s1.elem[0]*s2.elem[2] + s1.elem[1]*s2.elem[6] + s1.elem[2]*s2.elem[10] + s1.elem[3]*s2.elem[14],
       s1.elem[0]*s2.elem[3] + s1.elem[1]*s2.elem[7] + s1.elem[2]*s2.elem[11] + s1.elem[3]*s2.elem[15],

       s1.elem[4]*s2.elem[0] + s1.elem[5]*s2.elem[4] + s1.elem[6]*s2.elem[8] + s1.elem[7]*s2.elem[12],
       s1.elem[4]*s2.elem[1] + s1.elem[5]*s2.elem[5] + s1.elem[6]*s2.elem[9] + s1.elem[7]*s2.elem[13],
       s1.elem[4]*s2.elem[2] + s1.elem[5]*s2.elem[6] + s1.elem[6]*s2.elem[10] + s1.elem[7]*s2.elem[14],
       s1.elem[4]*s2.elem[3] + s1.elem[5]*s2.elem[7] + s1.elem[6]*s2.elem[11] + s1.elem[7]*s2.elem[15],

       s1.elem[8]*s2.elem[0] + s1.elem[9]*s2.elem[4] + s1.elem[10]*s2.elem[8] + s1.elem[11]*s2.elem[12],
       s1.elem[8]*s2.elem[1] + s1.elem[9]*s2.elem[5] + s1.elem[10]*s2.elem[9] + s1.elem[11]*s2.elem[13],
       s1.elem[8]*s2.elem[2] + s1.elem[9]*s2.elem[6] + s1.elem[10]*s2.elem[10] + s1.elem[11]*s2.elem[14],
       s1.elem[8]*s2.elem[3] + s1.elem[9]*s2.elem[7] + s1.elem[10]*s2.elem[11] + s1.elem[11]*s2.elem[15],

       s1.elem[12]*s2.elem[0] + s1.elem[13]*s2.elem[4] + s1.elem[14]*s2.elem[8] + s1.elem[15]*s2.elem[12],
       s1.elem[12]*s2.elem[1] + s1.elem[13]*s2.elem[5] + s1.elem[14]*s2.elem[9] + s1.elem[15]*s2.elem[13],
       s1.elem[12]*s2.elem[2] + s1.elem[13]*s2.elem[6] + s1.elem[14]*s2.elem[10] + s1.elem[15]*s2.elem[14],
       s1.elem[12]*s2.elem[3] + s1.elem[13]*s2.elem[7] + s1.elem[14]*s2.elem[11] + s1.elem[15]*s2.elem[15]);
#else
    return light44int(
       s1.elem[0]*s2.elem[0] + s1.elem[4]*s2.elem[1] + s1.elem[8]*s2.elem[2] +
       s1.elem[12]*s2.elem[3],
       s1.elem[0]*s2.elem[4] + s1.elem[4]*s2.elem[5] + s1.elem[8]*s2.elem[6] +
       s1.elem[12]*s2.elem[7],
       s1.elem[0]*s2.elem[8] + s1.elem[4]*s2.elem[9] + s1.elem[8]*s2.elem[10] +
       s1.elem[12]*s2.elem[11],
       s1.elem[0]*s2.elem[12] + s1.elem[4]*s2.elem[13] +
       s1.elem[8]*s2.elem[14] + s1.elem[12]*s2.elem[15],

       s1.elem[1]*s2.elem[0] + s1.elem[5]*s2.elem[1] + s1.elem[9]*s2.elem[2] +
       s1.elem[13]*s2.elem[3],
       s1.elem[1]*s2.elem[4] + s1.elem[5]*s2.elem[5] + s1.elem[9]*s2.elem[6] +
       s1.elem[13]*s2.elem[7],
       s1.elem[1]*s2.elem[8] + s1.elem[5]*s2.elem[9] + s1.elem[9]*s2.elem[10] +
       s1.elem[13]*s2.elem[11],
       s1.elem[1]*s2.elem[12] + s1.elem[5]*s2.elem[13] +
       s1.elem[9]*s2.elem[14] + s1.elem[13]*s2.elem[15],

       s1.elem[2]*s2.elem[0] + s1.elem[6]*s2.elem[1] + s1.elem[10]*s2.elem[2] +
       s1.elem[14]*s2.elem[3],
       s1.elem[2]*s2.elem[4] + s1.elem[6]*s2.elem[5] + s1.elem[10]*s2.elem[6] +
       s1.elem[14]*s2.elem[7],
       s1.elem[2]*s2.elem[8] + s1.elem[6]*s2.elem[9] +
       s1.elem[10]*s2.elem[10] + s1.elem[14]*s2.elem[11],
       s1.elem[2]*s2.elem[12] + s1.elem[6]*s2.elem[13] +
       s1.elem[10]*s2.elem[14] + s1.elem[14]*s2.elem[15],

       s1.elem[3]*s2.elem[0] + s1.elem[7]*s2.elem[1] + s1.elem[11]*s2.elem[2] +
       s1.elem[15]*s2.elem[3],
       s1.elem[3]*s2.elem[4] + s1.elem[7]*s2.elem[5] + s1.elem[11]*s2.elem[6] +
       s1.elem[15]*s2.elem[7],
       s1.elem[3]*s2.elem[8] + s1.elem[7]*s2.elem[9] +
       s1.elem[11]*s2.elem[10] + s1.elem[15]*s2.elem[11],
       s1.elem[3]*s2.elem[12] + s1.elem[7]*s2.elem[13] +
       s1.elem[11]*s2.elem[14] + s1.elem[15]*s2.elem[15]);
#endif
}



inline light44int
operator*(const light44int& s1, const int e)
{
#ifdef ROWMAJOR
    return light44int(s1.elem[0]*e, s1.elem[1]*e, s1.elem[2]*e, s1.elem[3]*e,
		      s1.elem[4]*e, s1.elem[5]*e, s1.elem[6]*e, s1.elem[7]*e,
		      s1.elem[8]*e, s1.elem[9]*e, s1.elem[10]*e, s1.elem[11]*e,
		      s1.elem[12]*e, s1.elem[13]*e, s1.elem[14]*e, s1.elem[15]*e
		      );
#else
    return light44int(s1.elem[0]*e, s1.elem[4]*e, s1.elem[8]*e, s1.elem[12]*e,
		      s1.elem[1]*e, s1.elem[5]*e, s1.elem[9]*e, s1.elem[13]*e,
		      s1.elem[2]*e, s1.elem[6]*e, s1.elem[10]*e, s1.elem[14]*e,
		      s1.elem[3]*e, s1.elem[7]*e, s1.elem[11]*e, s1.elem[15]*e
		      );
#endif
}



inline light44int
operator*(const int e, const light44int& s1)
{
    return s1 * e;
}



inline light4int
operator*(const light44int& s1, const light4int& s2)
{
#ifdef ROWMAJOR
  assert(0);
#else
    return light4int(s1.elem[0] * s2.elem[0] +
		     s1.elem[4] * s2.elem[1] +
		     s1.elem[8] * s2.elem[2] +
		     s1.elem[12] * s2.elem[3] ,
		     s1.elem[1] * s2.elem[0] +
		     s1.elem[5] * s2.elem[1] +
		     s1.elem[9] * s2.elem[2] +
		     s1.elem[13] * s2.elem[3],
		     s1.elem[2] * s2.elem[0] +
		     s1.elem[6] * s2.elem[1] +
		     s1.elem[10] * s2.elem[2] +
		     s1.elem[14] * s2.elem[3],
		     s1.elem[3] * s2.elem[0] +
		     s1.elem[7] * s2.elem[1] +
		     s1.elem[11] * s2.elem[2] +
		     s1.elem[15] * s2.elem[3]);		     
#endif
}



inline light44int
operator/(const light44int& s1, const int e)
{
#ifdef ROWMAJOR
    return light44int(s1.elem[0] / e, s1.elem[1] / e, s1.elem[2] / e,
		      s1.elem[3] / e,
		      s1.elem[4] / e, s1.elem[5] / e, s1.elem[6] / e,
		      s1.elem[7] / e,
		      s1.elem[8] / e, s1.elem[9] / e, s1.elem[10] / e,
		      s1.elem[11] / e,
		      s1.elem[12] / e, s1.elem[13] / e, s1.elem[14] / e,
		      s1.elem[15] / e);
#else
    return light44int(s1.elem[0] / e, s1.elem[4] / e, s1.elem[8] / e,
		      s1.elem[12] / e,
		      s1.elem[1] / e, s1.elem[5] / e, s1.elem[9] / e,
		      s1.elem[13] / e,
		      s1.elem[2] / e, s1.elem[6] / e, s1.elem[10] / e,
		      s1.elem[14] / e,
		      s1.elem[3] / e, s1.elem[7] / e, s1.elem[11] / e,
		      s1.elem[15] / e);
#endif
}



inline light44int
operator/(const int e, const light44int& s1)
{
#ifdef ROWMAJOR
    return light44int(e / s1.elem[0], e / s1.elem[1], e / s1.elem[2],
		      e / s1.elem[3],
		      e / s1.elem[4], e / s1.elem[5], e / s1.elem[6],
		      e / s1.elem[7],
		      e / s1.elem[8], e / s1.elem[9], e / s1.elem[10],
		      e / s1.elem[11],
		      e / s1.elem[12], e / s1.elem[13], e / s1.elem[14],
		      e / s1.elem[15]);
#else
    return light44int(e / s1.elem[0], e / s1.elem[4], e / s1.elem[8],
		      e / s1.elem[12],
		      e / s1.elem[1], e / s1.elem[5], e / s1.elem[9],
		      e / s1.elem[13],
		      e / s1.elem[2], e / s1.elem[6], e / s1.elem[10],
		      e / s1.elem[14],
		      e / s1.elem[3], e / s1.elem[7], e / s1.elem[11],
		      e / s1.elem[15]);
#endif
}



inline light44int
pow(const light44int& s1, const light44int& s2)
{
#ifdef ROWMAJOR
    return light44int(int(light_pow(s1.elem[0], s2.elem[0])),
		      int(light_pow(s1.elem[1], s2.elem[1])),
		      int(light_pow(s1.elem[2], s2.elem[2])),
		      int(light_pow(s1.elem[3], s2.elem[3])),
		      int(light_pow(s1.elem[4], s2.elem[4])),
		      int(light_pow(s1.elem[5], s2.elem[5])),
		      int(light_pow(s1.elem[6], s2.elem[6])),
		      int(light_pow(s1.elem[7], s2.elem[7])),
		      int(light_pow(s1.elem[8], s2.elem[8])),
		      int(light_pow(s1.elem[9], s2.elem[9])),
		      int(light_pow(s1.elem[10], s2.elem[10])),
		      int(light_pow(s1.elem[11], s2.elem[11])),
		      int(light_pow(s1.elem[12], s2.elem[12])),
		      int(light_pow(s1.elem[13], s2.elem[13])),
		      int(light_pow(s1.elem[14], s2.elem[14])),
		      int(light_pow(s1.elem[15], s2.elem[15])));
#else
    return light44int(int(light_pow(s1.elem[0], s2.elem[0])),
		      int(light_pow(s1.elem[4], s2.elem[4])),
		      int(light_pow(s1.elem[8], s2.elem[8])),
		      int(light_pow(s1.elem[12], s2.elem[12])),
		      int(light_pow(s1.elem[1], s2.elem[1])),
		      int(light_pow(s1.elem[5], s2.elem[5])),
		      int(light_pow(s1.elem[9], s2.elem[9])),
		      int(light_pow(s1.elem[13], s2.elem[13])),
		      int(light_pow(s1.elem[2], s2.elem[2])),
		      int(light_pow(s1.elem[6], s2.elem[6])),
		      int(light_pow(s1.elem[10], s2.elem[10])),
		      int(light_pow(s1.elem[14], s2.elem[14])),
		      int(light_pow(s1.elem[3], s2.elem[3])),
		      int(light_pow(s1.elem[7], s2.elem[7])),
		      int(light_pow(s1.elem[11], s2.elem[11])),
		      int(light_pow(s1.elem[15], s2.elem[15])));
#endif
}



inline light44int
pow(const light44int& s, const int e)
{
#ifdef ROWMAJOR
    return light44int(int(light_pow(s.elem[0], e)), int(light_pow(s.elem[1], e)),
		      int(light_pow(s.elem[2], e)), int(light_pow(s.elem[3], e)),
		      int(light_pow(s.elem[4], e)), int(light_pow(s.elem[5], e)),
		      int(light_pow(s.elem[6], e)), int(light_pow(s.elem[7], e)),
		      int(light_pow(s.elem[8], e)), int(light_pow(s.elem[9], e)),
		      int(light_pow(s.elem[10], e)), int(light_pow(s.elem[11], e)),
		      int(light_pow(s.elem[12], e)), int(light_pow(s.elem[13], e)),
		      int(light_pow(s.elem[14], e)), int(light_pow(s.elem[15], e)));
#else
    return light44int(int(light_pow(s.elem[0], e)), int(light_pow(s.elem[4], e)),
		      int(light_pow(s.elem[8], e)), int(light_pow(s.elem[12], e)),
		      int(light_pow(s.elem[1], e)), int(light_pow(s.elem[5], e)),
		      int(light_pow(s.elem[9], e)), int(light_pow(s.elem[13], e)),
		      int(light_pow(s.elem[2], e)), int(light_pow(s.elem[6], e)),
		      int(light_pow(s.elem[10], e)), int(light_pow(s.elem[14], e)),
		      int(light_pow(s.elem[3], e)), int(light_pow(s.elem[7], e)),
		      int(light_pow(s.elem[11], e)), int(light_pow(s.elem[15], e)));
#endif
}



inline light44int
pow(const int e, const light44int& s)
{
#ifdef ROWMAJOR
    return light44int(int(light_pow(e,s.elem[0])), int(light_pow(e,s.elem[1])),
		      int(light_pow(e,s.elem[2])), int(light_pow(e,s.elem[3])),
		      int(light_pow(e,s.elem[4])), int(light_pow(e,s.elem[5])),
		      int(light_pow(e,s.elem[6])), int(light_pow(e,s.elem[7])),
		      int(light_pow(e,s.elem[8])), int(light_pow(e,s.elem[9])),
		      int(light_pow(e,s.elem[10])), int(light_pow(e,s.elem[11])),
		      int(light_pow(e,s.elem[12])), int(light_pow(e,s.elem[13])),
		      int(light_pow(e,s.elem[14])), int(light_pow(e,s.elem[15])));
#else
    return light44int(int(light_pow(e,s.elem[0])), int(light_pow(e,s.elem[4])),
		      int(light_pow(e,s.elem[8])), int(light_pow(e,s.elem[12])),
		      int(light_pow(e,s.elem[1])), int(light_pow(e,s.elem[5])),
		      int(light_pow(e,s.elem[9])), int(light_pow(e,s.elem[13])),
		      int(light_pow(e,s.elem[2])), int(light_pow(e,s.elem[6])),
		      int(light_pow(e,s.elem[10])), int(light_pow(e,s.elem[14])),
		      int(light_pow(e,s.elem[3])), int(light_pow(e,s.elem[7])),
		      int(light_pow(e,s.elem[11])), int(light_pow(e,s.elem[15])));
#endif
}



inline light44int
abs(const light44int& s)
{
#ifdef ROWMAJOR
    return light44int(LIGHTABS(s.elem[0]), LIGHTABS(s.elem[1]),
		      LIGHTABS(s.elem[2]), LIGHTABS(s.elem[3]),
		      LIGHTABS(s.elem[4]), LIGHTABS(s.elem[5]),
		      LIGHTABS(s.elem[6]), LIGHTABS(s.elem[7]),
		      LIGHTABS(s.elem[8]), LIGHTABS(s.elem[9]),
		      LIGHTABS(s.elem[10]), LIGHTABS(s.elem[11]),
		      LIGHTABS(s.elem[12]), LIGHTABS(s.elem[13]),
		      LIGHTABS(s.elem[14]), LIGHTABS(s.elem[15]));
#else
    return light44int(LIGHTABS(s.elem[0]), LIGHTABS(s.elem[4]),
		      LIGHTABS(s.elem[8]), LIGHTABS(s.elem[12]),
		      LIGHTABS(s.elem[1]), LIGHTABS(s.elem[5]),
		      LIGHTABS(s.elem[9]), LIGHTABS(s.elem[13]),
		      LIGHTABS(s.elem[2]), LIGHTABS(s.elem[6]),
		      LIGHTABS(s.elem[10]), LIGHTABS(s.elem[14]),
		      LIGHTABS(s.elem[3]), LIGHTABS(s.elem[7]),
		      LIGHTABS(s.elem[11]), LIGHTABS(s.elem[15]));
#endif
}



inline light44int
ElemProduct(const light44int& s1, const light44int& s2)
{
#ifdef ROWMAJOR
    return light44int(s1.elem[0] * s2.elem[0], s1.elem[1] * s2.elem[1],
		      s1.elem[2] * s2.elem[2], s1.elem[3] * s2.elem[3],
		      s1.elem[4] * s2.elem[4], s1.elem[5] * s2.elem[5],
		      s1.elem[6] * s2.elem[6], s1.elem[7] * s2.elem[7],
		      s1.elem[8] * s2.elem[8], s1.elem[9] * s2.elem[9],
		      s1.elem[10] * s2.elem[10], s1.elem[11] * s2.elem[11],
		      s1.elem[12] * s2.elem[12], s1.elem[13] * s2.elem[13],
		      s1.elem[14] * s2.elem[14], s1.elem[15] * s2.elem[15]);
#else
    return light44int(s1.elem[0] * s2.elem[0], s1.elem[4] * s2.elem[4],
		      s1.elem[8] * s2.elem[8], s1.elem[12] * s2.elem[12],
		      s1.elem[1] * s2.elem[1], s1.elem[5] * s2.elem[5],
		      s1.elem[9] * s2.elem[9], s1.elem[13] * s2.elem[13],
		      s1.elem[2] * s2.elem[2], s1.elem[6] * s2.elem[6],
		      s1.elem[10] * s2.elem[10], s1.elem[14] * s2.elem[14],
		      s1.elem[3] * s2.elem[3], s1.elem[7] * s2.elem[7],
		      s1.elem[11] * s2.elem[11], s1.elem[15] * s2.elem[15]);
#endif
}



inline light44int
ElemQuotient(const light44int& s1, const light44int& s2)
{
#ifdef ROWMAJOR
    return light44int(s1.elem[0] / s2.elem[0], s1.elem[1] / s2.elem[1],
		      s1.elem[2] / s2.elem[2], s1.elem[3] / s2.elem[3],
		      s1.elem[4] / s2.elem[4], s1.elem[5] / s2.elem[5],
		      s1.elem[6] / s2.elem[6], s1.elem[7] / s2.elem[7],
		      s1.elem[8] / s2.elem[8], s1.elem[9] / s2.elem[9],
		      s1.elem[10] / s2.elem[10], s1.elem[11] / s2.elem[11],
		      s1.elem[12] / s2.elem[12], s1.elem[13] / s2.elem[13],
		      s1.elem[14] / s2.elem[14], s1.elem[15] / s2.elem[15]);
#else
    return light44int(s1.elem[0] / s2.elem[0], s1.elem[4] / s2.elem[4],
		      s1.elem[8] / s2.elem[8], s1.elem[12] / s2.elem[12],
		      s1.elem[1] / s2.elem[1], s1.elem[5] / s2.elem[5],
		      s1.elem[9] / s2.elem[9], s1.elem[13] / s2.elem[13],
		      s1.elem[2] / s2.elem[2], s1.elem[6] / s2.elem[6],
		      s1.elem[10] / s2.elem[10], s1.elem[14] / s2.elem[14],
		      s1.elem[3] / s2.elem[3], s1.elem[7] / s2.elem[7],
		      s1.elem[11] / s2.elem[11], s1.elem[15] / s2.elem[15]);
#endif
}



inline light44int
Apply(const light44int& s, int f(int))
{
#ifdef ROWMAJOR
    return light44int(f(s.elem[0]), f(s.elem[1]), f(s.elem[2]), f(s.elem[3]),
		      f(s.elem[4]), f(s.elem[5]), f(s.elem[6]), f(s.elem[7]),
		      f(s.elem[8]), f(s.elem[9]), f(s.elem[10]), f(s.elem[11]),
		      f(s.elem[12]), f(s.elem[13]), f(s.elem[14]), f(s.elem[15])
		      );
#else
    return light44int(f(s.elem[0]), f(s.elem[4]), f(s.elem[8]), f(s.elem[12]),
		      f(s.elem[1]), f(s.elem[5]), f(s.elem[9]), f(s.elem[13]),
		      f(s.elem[2]), f(s.elem[6]), f(s.elem[10]), f(s.elem[14]),
		      f(s.elem[3]), f(s.elem[7]), f(s.elem[11]), f(s.elem[15])
		      );
#endif
}



inline light44int
Apply(const light44int& s1, const light44int& s2, int f(int, int))
{
#ifdef ROWMAJOR
    return light44int(f(s1.elem[0], s2.elem[0]), f(s1.elem[1], s2.elem[1]),
		      f(s1.elem[2], s2.elem[2]), f(s1.elem[3], s2.elem[3]),
		      f(s1.elem[4], s2.elem[4]), f(s1.elem[5], s2.elem[5]),
		      f(s1.elem[6], s2.elem[6]), f(s1.elem[7], s2.elem[7]),
		      f(s1.elem[8], s2.elem[8]), f(s1.elem[9], s2.elem[9]),
		      f(s1.elem[10], s2.elem[10]), f(s1.elem[11], s2.elem[11]),
		      f(s1.elem[12], s2.elem[12]), f(s1.elem[13], s2.elem[13]),
		      f(s1.elem[14], s2.elem[14]), f(s1.elem[15], s2.elem[15])
		      );
#else
    return light44int(f(s1.elem[0], s2.elem[0]), f(s1.elem[4], s2.elem[4]),
		      f(s1.elem[8], s2.elem[8]), f(s1.elem[12], s2.elem[12]),
		      f(s1.elem[1], s2.elem[1]), f(s1.elem[5], s2.elem[5]),
		      f(s1.elem[9], s2.elem[9]), f(s1.elem[13], s2.elem[13]),
		      f(s1.elem[2], s2.elem[2]), f(s1.elem[6], s2.elem[6]),
		      f(s1.elem[10], s2.elem[10]), f(s1.elem[14], s2.elem[14]),
		      f(s1.elem[3], s2.elem[3]), f(s1.elem[7], s2.elem[7]),
		      f(s1.elem[11], s2.elem[11]), f(s1.elem[15], s2.elem[15])
		      );
#endif
}



inline light44int
Transpose(const light44int& s)
{
    return light44int(s.elem);
}

#include "i_light44_auto.icc"
 
#undef IN_LIGHT44int_ICC
