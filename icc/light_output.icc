//           -*- c++ -*-

#ifdef LIGHTMAT_OUTPUT_FUNCS
#define C_IN_OUTPUT<T>
#include <stdio.h>
#include <strstream>
#include <fstream>

#include <ctype.h>
// for tolower()

#include "rw/cstring.h"


#ifndef simple_printout_defined
#define simple_printout_defined

#ifndef __RWCSTRING_H__

//
// ToStr
//
inline RWCString
ToStr(const double d)
{
    char buf[20];
    sprintf(buf, "%g", d);
    return RWCString(buf);
}


inline RWCString
ToStr(const int d)
{
    char buf[20];
    sprintf(buf, "%d", d);
    return RWCString(buf);
}


inline RWCString
ToStr(const lm_complex & d)
{
    char buf[40];
    if (d.re==0 && d.im!=0) {
      sprintf(buf, "%g*I",  d.im);
    } 
    else 
    {
         
      if (d.im>0)
		sprintf(buf, "%g+%g*I", d.re, d.im);
      else if (d.im==0)
		sprintf(buf, "%g", d.re);
      else
		sprintf(buf, "%g-%g*I", d.re, -d.im);
    }    
    return RWCString(buf);
}

#endif

inline std::ostream& operator<<(std::ostream& o, const lm_complex& s){
    o<<s.re;
    if (s.im>0) o<<"+";
    if (s.im!=0) {
        o<<s.im; 
        o<<" I";
    }
    
    return o;
};


#endif


#ifndef  startendString
// Guarantee that these functions are defined only once.
#define  startendString
#ifndef NOPAR

#ifdef OLD_ARRAY_PRINTING
inline void startString(RWCString &s)
{
   s = "(";
}

inline void endString(RWCString &s)
{
   s += ")";
}

inline void commaString(RWCString &s, int index)
{}

#else

inline void startString(RWCString &s)
{
   s = "{";
}

inline void endString(RWCString &s)
{
   s += "}";
}

inline void commaString(RWCString &s, int index)
{
  if (index!=1) s += ", ";
}


#endif

 
#else
inline void startString(RWCString &){}
inline void endString(RWCString &s){s+="   ";}
inline void commaString(RWCString &s, int index){s+=" ";}
#endif
#endif

template<class T>
inline RWCString
ToStr(const lightN<T>& s)
{
    RWCString res;
    startString(res);
    for(int i=1; i<=s.dimension(1); i++) {
	    commaString(res,i);
	    res += ToStr(s(i));
    }
    endString(res);
    return res;
}


template<class T>
inline RWCString
ToStr(const lightNN<T>& s, int depth)
{
    RWCString res;

    startString(res);
    for(int i=1; i<=s.dimension(1); i++) {
       commaString(res,i);
       if(i!=1) {
         res += "\n ";
         for (int j=1;j<=depth;j++) res+=" ";
       }   
       res+=ToStr(s(i));
     }

    endString(res);
    return res;
}


template<class T>
inline RWCString
ToStr(const lightNNN<T>& s, int depth)
{
    RWCString res;

    startString(res);
    for(int i=1; i<=s.dimension(1); i++) {
       commaString(res,i);
       if(i!=1) { 
          res += "\n ";
          for (int j=1;j<=depth;j++) res+=" ";
       }
       res+=ToStr(s(i),depth+1);
    }
    endString(res);
    return res;
}


template<class T>
inline RWCString
ToStr(const lightNNNN<T>& s, int depth)
{
    RWCString res;

    startString(res);
    for(int i=1; i<=s.dimension(1); i++) {
       commaString(res,i);
       if(i!=1) {
         res += "\n ";
         for (int j=1;j<=depth;j++) res+=" ";
       }
       res+=ToStr(s(i),depth+1);
    }
    endString(res);
    return res;
}

/*
Export formats:
CSV format is intended for 2D arrays.
Each row of the array is on separate line, separated by "newline".
There is no newline after the last row. Newline is OS-dependent, 
i.e. as printed "\n" to a text file. There are no spaces between numbers.
Floating point nubers can be printed in any notation (decimal or exponentional),
there is no strict definition. Complex numbers are written as 66.+8.12*I
Numbers within the same row are separated by a comma ","
 
 
If 1D array is passed , each line contains one number
 
List format
If 1D array is passed , each line contains one number
*/

template<class T>
inline RWCString
light_export(const char* file, const lightN<T>& s, const char* format, const char* option, const char* optionval)
{
	RWCString res;
	for (int i=1; i<= s.dimension(1); i++) {
		res += ToStr(s(i));
		if (i<s.dimension(1) ) res +="\n";
	}
	std::ofstream ofs(file);
	ofs << res;
	
	return file;
}

template<class T>
inline RWCString
light_export(const char* file, const lightNN<T>& s, const char* format, const char* option, const char* optionval)
{
      RWCString res;
      for (int i=1; i<= s.dimension(1); i++) {
            for(int j = 1; j<= s.dimension(2); j++){
            	res+= ToStr(s(i,j));
            	if (j<s.dimension(2) ) res +=",";
            }
           if (i<s.dimension(1) ) res +="\n";          
      }
      
      std::ofstream ofs(file);
      ofs << res;
      
      return file; 
}

template<class T>
inline RWCString
light_export1(const char* file, const lightN<T>& s, const char* option, const char* optionval)
{
	RWCString res;
	const char* suffix_tmp =strrchr(file,'.');
	if (suffix_tmp!=NULL) {
            char* suffix=strdup(suffix_tmp);
            char* pos=suffix;  /* convert suffix to lowercase */
            while(*pos){
              *pos=tolower(*pos);
              pos++;
            };
        
        if (!strcmp(suffix,".csv")) 
        	res= light_export(file, s, suffix, option, optionval);
        else 
        	lighterror("Unknown export format");
        }			           
        return res;
}

template<class T>
inline RWCString
light_export1(const char* file, const lightNN<T>& s, const char* option, const char* optionval)
{
	RWCString res;
	const char* suffix_tmp =strrchr(file,'.');
	if (suffix_tmp!=NULL) {
            char* suffix=strdup(suffix_tmp);
            char* pos=suffix;  /* convert suffix to lowercase */
            while(*pos){
              *pos=tolower(*pos);
              pos++;
            };
        
        if (!strcmp(suffix,".csv")) 
        	res= light_export(file, s, suffix, option, optionval);
        else
        	lighterror("Unknown export format");	
        			
        }    
        return res;
}

//
// import
//

#ifndef MAXLINESIZE_DEF
#define MAXLINESIZE_DEF
const int MAXLINESIZE=32000;
#endif

#ifdef C_IN_OUTPUTint

inline int read_from_stream_int( std::istrstream & istr, int hasI){
     int tmp;
     istr >> tmp;
     return tmp;
}

#endif

#ifdef  C_IN_OUTPUTdouble

inline double read_from_stream_double(std::istrstream & istr, int hasI){
     double tmp;
     istr >> tmp;
     return tmp;
}

#endif

#ifdef  C_IN_OUTPUTlm_complex

inline lm_complex  read_from_stream_lm_complex( std::istrstream & istr, int hasI){
     
     double   tmpre=0;
     double   tmpim=0;
     istr >> tmpre;
     istr >> tmpim;  // may fail for complex numbers with im=0, case "7"
     if (hasI && tmpim==0 && tmpre!=0) {
        // For the case  "8 I"
        tmpim=tmpre;
        tmpre=0;

     } 
     lm_complex tmp(tmpre,tmpim);
     char isym;     // normally should be "I"
     istr >> isym;  // may fail for complex numbers with im=0
     return tmp;
}

#endif


template<class T>
inline lightN<T> 
light_importN<T>(const char* filename, const char* format, const char* opt1, const char* opt2){
	lightN<T> res;
	
	char* fm = strdup(format);
	char* pos=fm;  /* convert format to lowercase */
            while(*pos){
              *pos=tolower(*pos);
              pos++;
            };
        if ((!strcmp(fm,"csv"))||(!strcmp(fm,"list")) ) {
        
        char line[MAXLINESIZE];
        std::ifstream ifs(filename);
        int number_of_lines=0;
        while(ifs.getline(line,MAXLINESIZE)) {

           if (strlen(line)>0) number_of_lines++;
        }	
        ifs.close();
        res.SetShape(number_of_lines);
         ifs.clear();
        ifs.open( filename);
         number_of_lines=0;
        int hasI=0;
        while(ifs.getline(line,MAXLINESIZE)) {

           if (strlen(line)>0) {
             std::istrstream istr(line);
             number_of_lines++;
             hasI=(strchr(line,'I')!=NULL);
             T temp=read_from_stream_<T>(istr,hasI);
             res(number_of_lines)=temp;
           }
           
        }	
         ifs.close();

        
    } // if csv
    else
    {
      lighterror("Unknown import format");	
    }
    return res;
}

template<class T>
inline lightNN<T> 
light_importNN<T>(const char* filename, const char* format, const char* opt1, const char* opt2){

    lightNN<T> res;

    char* fm = strdup(format);
    char* pos=fm;  /* convert format to lowercase */
    while(*pos){
        *pos=tolower(*pos);
        pos++;
    };
    if (!strcmp(fm,"csv"))  {
        
        char line[MAXLINESIZE];
        std::ifstream ifs(filename);
        int number_of_lines=0;
        char * commapos=NULL;
        int max_number_of_commas=0;
        int number_of_commas=0;
        while(ifs.getline(line,MAXLINESIZE)) {

            if (strlen(line)>0) {
                commapos=line;
                number_of_commas=0;
                while(*commapos) {
                    if (*commapos==',') number_of_commas++;
                    commapos++;
                }
                if (number_of_commas>max_number_of_commas) {
                    max_number_of_commas=number_of_commas;
                }

                number_of_lines++;
            }
        }
        ifs.close();
        res.SetShape(number_of_lines,max_number_of_commas+1);
        // +1 because there is no comma after the last number in the line.
        // We compute maximal number because even if the file is broken,
        // all numbers will be read in.
        ifs.clear();
        ifs.open( filename);
        number_of_lines=0;
        char local_line[MAXLINESIZE];
        int number_of_local_lines=0;
        while(ifs.getline(line,MAXLINESIZE)) {

            if (strlen(line)>0) {
                std::istrstream istr(line);
                number_of_lines++;
                number_of_local_lines=0;
                int hasI=0;
                while(istr.getline(local_line,MAXLINESIZE,',')) {
                    number_of_local_lines++;


                    if (strlen(local_line)>0) {
                        std::istrstream istr2(local_line);
                        hasI=(strchr(local_line,'I')!=NULL);
                        T temp=read_from_stream_<T>(istr2,hasI);
                        res(number_of_lines,number_of_local_lines)=temp;
                    }

                } // while(istr)

            } // if



        } // while (ifs)
        
    } // if
    else
    {
        lighterror("Unknown import format");
    }
    return res;
}





template<class T>
inline lightN<T> 
light_import1N<T>(const char* file)
{
	lightN<T> res;
	const char* suffix_tmp =strrchr(file,'.');
	if (suffix_tmp!=NULL) {
	        suffix_tmp++; // to skip "." 
            char* suffix=strdup(suffix_tmp);
            char* pos=suffix;  /* convert suffix to lowercase */
            while(*pos){
              *pos=tolower(*pos);
              pos++;
            };
        
        if (!strcmp(suffix,"csv")) 
        	res= light_importN<T>(file, suffix, NULL, NULL);
        else
        	lighterror("Unknown import format");	
        			
        }    
        return res;
}


template<class T>
inline lightNN<T> 
light_import1NN<T>(const char* file)
{
	lightNN<T> res;
	const char* suffix_tmp =strrchr(file,'.');
	if (suffix_tmp!=NULL) {
	        suffix_tmp++; // to skip "." 
            char* suffix=strdup(suffix_tmp);
            char* pos=suffix;  /* convert suffix to lowercase */
            while(*pos){
              *pos=tolower(*pos);
              pos++;
            };
        
        if (!strcmp(suffix,"csv")) 
        	res= light_importNN<T>(file, suffix, NULL, NULL);
        else
        	lighterror("Unknown import format");	
        			
        }    
        return res;
}


//
// operator<<
//

template<class T>
inline ostream&
operator<<(ostream& o, const light3<T>& s)
{
    return o << ToStr(lightN<T>(s));
}


template<class T>
inline ostream&
operator<<(ostream& o, const light4<T>& s)
{
    return o << ToStr(lightN<T>(s));
}


template<class T>
inline ostream&
operator<<(ostream& o, const lightN<T>& s)
{
    return o << ToStr(s);
}


template<class T>
inline ostream&
operator<<(ostream& o, const light33<T>& s)
{
    return o << ToStr(lightNN<T>(s));
}


template<class T>
inline ostream&
operator<<(ostream& o, const light44<T>& s)
{
    return o << ToStr(lightNN<T>(s));
}


template<class T>
inline ostream&
operator<<(ostream& o, const lightN3<T>& s)
{
    return o << ToStr(lightNN<T>(s));
}


template<class T>
inline ostream&
operator<<(ostream& o, const lightNN<T>& s)
{
    return o << ToStr(s);
}


template<class T>
inline ostream&
operator<<(ostream& o, const lightNNN<T>& s)
{
    return o << ToStr(s);
}

template<class T>
inline ostream&
operator<<(ostream& o, const lightN33<T>& s)
{
    return o << ToStr(lightNNN<T>(s));
}


template<class T>
inline ostream&
operator<<(ostream& o, const lightNNNN<T>& s)
{
    return o << ToStr(s);
}
#undef C_IN_OUTPUT<T>
#endif // LIGHTMAT_OUTPUT_FUNCS
