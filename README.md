# BUILD INSTRUCTIONS
> **Note:** These instructions have been prepared by Paolo Bosetti and Alessandro Mazzalai (UniTN) on Jul. 30th 2015.

## Get the package

Download the compressed package [here](https://bitbucket.org/unitn/lightmat/get/master.zip).

## On DEBIAN machines

### Prerequisites

The following packages must be installed:

- `$ sudo apt-get install build-essential`
- `$ sudo apt-get install clang`

### Building the static library:

    $ make all

> **Note:** The `lmlib.a` file is created in `lib` folder.


## On Mac OS X
   
Same procedure than Debian, with the exception that the only prerequisite is having Xcode installed.


## On MS Windows

### Prerequisites

You're going to need VisualStudio 2012 or later.

### Building the static library

- Open a command pront 
- run vcvars64.bat to build the for x64 or run vcvars32.bat for build x86. 
      (for visual studio 2017 the path is Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build)
- Open the src path in the rootpath of the lightmat project
- Type nmake -f make.win all

> **Note:** The `lmlib.lib` file is created in `lib` folder.