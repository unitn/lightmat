#define LM_ALL
#include <lightmat.h>
int main(){
doubleNNN f(5,6,7,3.11);
cout << f; 
return 0;
}
// INLINED:
//  g++ foo.cpp -I../include ../obj/cstring.o ../obj/extwin.o -lm
//
// NOT INLINED:
//  g++ foo.cpp -I../include  -DNOT_INLINED ../obj/cstring.o ../obj/extwin.o ../obj/not_inlined.o -lm
//
