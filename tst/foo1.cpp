#define LM_N
#include <lightmat.h>
int main(){
doubleN x(5,2.0);
cout << x; 
//doubleNN y(3,4);- Forbidden
// This is not allowed because this class requires LM_NN.
// Compiler reports an error.
return 0;
}
// ONLY NOT INLINED:
//  g++ foo1.cpp -I../include  -DNOT_INLINED ../obj/cstring.o ../obj/extwin.o ../obj/not_inlined.o -lm

